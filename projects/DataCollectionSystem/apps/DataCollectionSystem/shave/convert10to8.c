
#include <svuCommonShave.h>
#include <moviVectorUtils.h>
#include <lookupTable10to8.h>

#include <swcFrameTypes.h>
#include <mvMacros.h>
#include <mv_types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <swcCdma.h>
//#include <aeStats.h>

dmaTransactionList_t __attribute__((section(".cmx.cdmaDescriptors"))) tasks0;
dmaTransactionList_t *ref0;

#define LINE_BUFFS 6
u16 inLine[2][640];
u8 outLine[1][640];

u8 outLine2[LINE_BUFFS][640];
u8 lut[1024];
u32 lutInitialized;

int convert10to8bpp(frameBuffer *in, frameBuffer *out)
{
    u32 id;
    u8* outptr;
    u16* inptr;
    u32 cl = 0;
    // Initialize tasks handler
    id = dmaInitRequester(1);
    ref0 = dmaCreateTransaction(id, &tasks0,
                                (u8*)(in->p1  + cl * in->spec.stride),
                                (u8*)inLine[0],
                                in->spec.width * in->spec.bytesPP);
    dmaStartListTask(ref0);
    dmaWaitTask(ref0);
    if (!lutInitialized)
    {
        for (cl = 0; cl < 1024; cl ++)
        {
            lut[cl] = cl >> 2;
        }
        lutInitialized = 1;
    }
    for(cl = 0 ; cl < in->spec.height - 1; cl++)
    {
        ref0 = dmaCreateTransaction(id, &tasks0,
                                    (u8*)(in->p1  + (cl + 1) * in->spec.stride),
                                    (u8*)inLine[(cl + 1) & 1],
                                    in->spec.width * in->spec.bytesPP);
        dmaStartListTask(ref0);
        inptr = inLine[cl & 1];
        outptr = &outLine2[cl % LINE_BUFFS][0];
        mvcvLUT10to8_asm(&inptr, &outptr, lut, in->spec.width, 1);
        dmaWaitTask(ref0);
        ref0 = dmaCreateTransaction(id, &tasks0,
                                    (u8*)outLine2[cl % LINE_BUFFS],
                                    (u8*)(out->p1 + cl * out->spec.stride),
                                    out->spec.width);
        dmaStartListTask(ref0);
        dmaWaitTask(ref0);
    }
    inptr = inLine[cl & 1];
    outptr = &outLine2[cl % LINE_BUFFS][0];
    mvcvLUT10to8_asm(&inptr, &outptr, lut, in->spec.width, 1);
    ref0 = dmaCreateTransaction(id, &tasks0,
                                (u8*)outLine2[cl % LINE_BUFFS],
                                (u8*)(out->p1 + cl * out->spec.stride),
                                out->spec.width);
    dmaStartListTask(ref0);
    dmaWaitTask(ref0);
    SHAVE_HALT;
    return 0;
}
