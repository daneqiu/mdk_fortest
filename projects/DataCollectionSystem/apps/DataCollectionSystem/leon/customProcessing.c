#include <pthread.h>
#include <string.h>
#include <mvMacros.h>

#include "swcFrameTypes.h"
#include "OsDrvTimer.h"
#include "swcLeonUtils.h"
#include "swcShaveLoader.h"


#include <DrvLeonL2C.h>
#include <DrvShaveL2Cache.h>

#include <DrvIcbDefines.h>

#include "customProcThread.h"

#define NUMBER_OF_SHAVE_THREADS 2

#define CMX_NOCACHE __attribute__((section(".cmx_direct.data")))
#define CMX_DATA_S4 __attribute__((section(".shv4.S.data")))
#define CMX_DATA_S7 __attribute__((section(".shv7.S.data")))

#define DDR_AREA __attribute__((section(".ddr.bss")))

extern u32 cvrt5_convert10to8bpp[];
extern u32 cvrt6_convert10to8bpp[];

static const u32 cvrt_convert10to8bpp[NUMBER_OF_SHAVE_THREADS] =
{
    (u32)cvrt5_convert10to8bpp,
    (u32)cvrt6_convert10to8bpp,
};
static const u32 convertShaveNo[NUMBER_OF_SHAVE_THREADS] =
{
    (u32)5,
    (u32)6,
};
static const u32 convertShaveCachePart[NUMBER_OF_SHAVE_THREADS] =
{
    (u32)0,
    (u32)1,
};

void* customProcessInit()
{
    return NULL;
}
void* customProcessFrame(processMessage_t* message)
{
    u32 i;
    u64 elapsed;
    for(i = 0; i < NUMBER_OF_SHAVE_THREADS; i++)
    {
        // get buffers for output from the circular buffer
        frameBuffer outFb;
        outFb.p1 = (u8*) message->metaBuffers[i]->customMetadata;
        outFb.p2 = NULL;
        outFb.p3 = NULL;
        outFb.spec = message->inFrames[i].fb.spec;
        outFb.spec.bytesPP = 1;
        outFb.spec.stride = message->inFrames[i].fb.spec.width;
        outFb.spec.type = RAW8;
        {///// TO BE REMOVED
            tyTimeStamp stamp;
            DrvTimerStartTicksCount(&stamp);
            DrvShaveL2CachePartitionInvalidate(convertShaveCachePart[i]);
            swcResetShave(convertShaveNo[i]);
            swcSetAbsoluteDefaultStack(convertShaveNo[i]);
            swcStartShaveCC(convertShaveNo[i], (u32)cvrt_convert10to8bpp[i],
                            "ii", (u32)&message->inFrames[i].fb,
                            (u32)&outFb);
            swcWaitShave(convertShaveNo[i]);
            DrvTimerGetElapsedTicks(&stamp, &elapsed);
        }
    }
    for(i = 0; i < NUMBER_OF_SHAVE_THREADS; i++)
    {
        message->outFrames[i].fb.spec = message->inFrames[i].fb.spec;
        // TODO: use dma
        memcpy(message->outFrames[i].fb.p1, message->inFrames[i].fb.p1, message->inFrames[i].fb.spec.stride *
               message->inFrames[i].fb.spec.height);

        message->metaSizes[i] = message->inFrames[i].fb.spec.width *
                        message->inFrames[i].fb.spec.height;
    }
    return NULL;
}
