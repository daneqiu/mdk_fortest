///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///


// 1: Includes
// ----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <rtems.h>
#include <rtems/status-checks.h>
#include <rtems/libio.h>
#include <rtems/shell.h>
#include <sched.h>
#include <sys/stat.h>        /* For mode constants */
#include <mqueue.h>
#include <fcntl.h>
#include <OsDrvCpr.h>
#include <OsDrvTimer.h>
#include <DrvLeon.h>
#include <DrvShaveL2Cache.h>

#include <brdRtems.h>


#include "DataCollector.h"

// 2:  Source Specific #defines and types  (typedef, enum, struct)
// -----------------------------------------------------------------------------
// 3: Global Data (Only if absolutely necessary)
// -----------------------------------------------------------------------------
// 4: Static Local Data
// -----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------

void startShell(void)
{

    rtems_shell_init(
                    "SHLL", /* task name */
                    RTEMS_MINIMUM_STACK_SIZE * 8, /* task stack size */
                    200, /* task priority */
                    "/dev/console", /* device name */
                    false, /* run forever */
                    false, /* wait for shell to terminate */
                    NULL /* login check function,
                use NULL to disable a login check */
    );
}



static dataCollectorContext dcc =
{
    .camName =
    {
        (char*)"/dev/video_tracking_left",
        (char*)"/dev/video_tracking_right"
    },
    .camCnt = 2,
    .processingthreadCnt = 2,
    .camFrameRate = 30,
    .latencyControl = 0,
    .outputFrameDisable = 0,
    .processingPlugin = {NULL}
};

void initializeCache()
{
    DrvShaveL2CacheSetMode(SHAVEL2C_MODE_NORMAL);
    //Set Shave L2 cache partitions
    DrvShaveL2CacheSetupPartition(SHAVEPART128KB);
    DrvShaveL2CacheSetupPartition(SHAVEPART128KB);

    //Allocate Shave L2 cache set partitions
    DrvShaveL2CacheAllocateSetPartitions();
    //Assign the one partition defined to all shaves
    DrvShaveL2CacheSetLSUPartId(5, 0);
    DrvShaveL2CacheSetLSUPartId(6, 1);

    DrvShaveL2CachePartitionInvalidate(0);
    DrvShaveL2CachePartitionInvalidate(1);

}
extern "C" void *POSIX_Init(void *args)
{
    UNUSED(args);
    rtems_status_code sc;

    sc = brdInitializeBoard();
    if (sc != RTEMS_SUCCESSFUL) {
        RTEMS_SYSLOG_ERROR_WITH_SC( sc, "Failure to initialize board driver !");
        return NULL;
    }
    initializeCache();
    startShell();
    DataCollectorStart(&dcc);
    return NULL;
}
