#include <stdio.h>
#include <unistd.h>

#include <fstream>
#include <iostream>
#include <opencv/cv.hpp>
#include <opencv/highgui.h>
#include <sstream>
#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <time.h>
#include "superFrame.h"
#include "XLink.h"

using namespace std;

static IplImage* final_output[2];

//#define USB_ENDPOINT_IN     (0x81)   /* endpoint address */
//#define USB_ENDPOINT_OUT (0x01)   /* endpoint address */
//#define USB_TIMEOUT         4000        /* Connection timeout (in ms) */

uint32_t FRAME_WIDTH  = 640;
uint32_t FRAME_HEIGHT = 480;

#define X_LINK_STREAM_NAME "DCStream"
static streamId_t usbLinkStream;

// note that you will need ~MAX_FRAME_COUNT * 1.3 MB
#define MAX_FRAME_COUNT    (30 * 30 * 60) // 30 mins

typedef enum exitReason
{
    MALLOC_FAIL,
    MAX_FRAMES,
    USER_EXIT
}exitReason;

#define CHECK_MALLOC(x) \
    if(!x){ \
        saveAndExit(MALLOC_FAIL); \
    }


#define ROUND_UP(x,y) ((x + y - 1) / y * y)

#define OUTPUT_DIR          "output"
//#define TRIGGERED_CAPTURE
#define TRIGGER_KEY 32 /*space*/

float ms1, ms2, ms3, ms4, ms5, ms6, time1;
struct timespec spec;

uint8_t outf[3][640 * 480 * 3]; //TODO: carefull for max size here

int frameCount = 0;
uint16_t** frames[2];
frameMetadataGeneric** metas[2];
imuFrame** imus;

static void cv_init_display()
{
    cvNamedWindow("stream1", CV_WINDOW_AUTOSIZE);
    cvNamedWindow("stream2", CV_WINDOW_AUTOSIZE);

    final_output[0] = cvCreateImage( cvSize(FRAME_WIDTH, FRAME_HEIGHT), IPL_DEPTH_8U, 1);
    final_output[1] = cvCreateImage( cvSize(FRAME_WIDTH, FRAME_HEIGHT), IPL_DEPTH_8U, 1);
    final_output[0]->imageData = (char*)outf[0];
    final_output[1]->imageData = (char*)outf[1];
}

static void cv_stop_display()
{
    cvDestroyWindow("stream1");
    cvDestroyWindow("stream2");
}


// implements a simple "protocol". Each message needs a 4K ACK
void fullRead(struct fastmemDevice* a, void* buffer, int cnt)
{
    streamPacketDesc_t * packet;
    XLinkReadData(usbLinkStream,&packet);
    assert(cnt == packet->length);
    memcpy(buffer, packet->data, cnt);
    XLinkReleaseData(usbLinkStream);
}

void process(uint16_t* frame1, uint16_t* frame2)
{
    char key;
    int i;
    for(i = 0; i < FRAME_WIDTH*FRAME_HEIGHT; i++)
    {
        outf[0][i] = (uint8_t)(frame1[i] >> 8 );
        outf[1][i] = (uint8_t)(frame2[i] >> 8 );
    }
    cvShowImage("stream1", final_output[0]);
    cvShowImage("stream2", final_output[1]);
}


int key;

void writeFrame(uint16_t* imgBuffer, int width, int height, string filename)
{
    cv::Mat TempMat(height, width, CV_16UC1, imgBuffer);
    imwrite(filename, TempMat);
}

void writeMeta(frameMetadataGeneric* metaBuffer, string filename)
{
    ofstream metaFile(filename.c_str(), ios::binary);
    metaFile.write((const char*)metaBuffer, sizeof(frameMetadataGeneric));
    metaFile.close();
}

void writeImu(imuFrame* imuBuffer, string filename)
{
    ofstream imuFile(filename.c_str(), ios::binary);
    imuFile.write((const char*)imuBuffer, sizeof(imuFrame));
    imuFile.close();
}

void readImageAndMeta(fastmemDevice* a, uint16_t* imageData, frameMetadataGeneric* metaData)
{
    fullRead(a, (uint8_t*)&FRAME_WIDTH,  sizeof(uint32_t));
    fullRead(a, (uint8_t*)&FRAME_HEIGHT, sizeof(uint32_t));

    uint8_t tmpMeta[ROUND_UP(FRAME_WIDTH * FRAME_HEIGHT + sizeof(frameMetadataGeneric), 4096)];
    fullRead(a, (uint8_t*) imageData, FRAME_WIDTH * FRAME_HEIGHT * 2);
    for (int i = 0; i < FRAME_WIDTH * FRAME_HEIGHT; i++)
    {
        imageData[i] <<= 6;
    }
    int metaSize;
    fullRead(a, (uint8_t*)&metaSize, sizeof(int));
    fullRead(a, (uint8_t*)tmpMeta, metaSize);
    memcpy(metaData, tmpMeta, sizeof(frameMetadataGeneric));
}


void saveAndExit(exitReason reason)
{
    printf("Exiting because ");
    switch (reason)
    {
    case USER_EXIT:
        printf("user requested halt\n");
        break;
    case MALLOC_FAIL:
        printf("not enough memory\n");
        break;
    case MAX_FRAMES:
        printf("reached maximum number of frames\n");
        break;

    }
    int frameId;
    for(frameId = 0; frameId < frameCount; frameId++)
    {
        if (frameId % 100 == 0)
            printf("writing frame %d\n", frameId);
        stringstream f_ss;
        f_ss << frameId;
        string idxStr = f_ss.str();

        writeFrame(frames[0][frameId], FRAME_WIDTH, FRAME_HEIGHT, OUTPUT_DIR "/image0_" + idxStr + ".png");
        writeFrame(frames[1][frameId], FRAME_WIDTH, FRAME_HEIGHT, OUTPUT_DIR "/image1_" + idxStr + ".png");
        writeMeta(metas[0][frameId], OUTPUT_DIR "/image1_" + idxStr + "_meta.bin");
        writeMeta(metas[1][frameId], OUTPUT_DIR "/image1_" + idxStr + "_meta.bin");
        writeImu(imus[frameId], OUTPUT_DIR "/image1_" + idxStr + "_imu.bin");
        if (metas[0][frameId]->frameCount != metas[1][frameId]->frameCount)
            printf("Metadata corrupted at frame %d\n", frameId);
        if ((frameId + 1 < frameCount) &&
                        (metas[0][frameId + 1]->timestampNs - metas[0][frameId]->timestampNs > 34*1000*1000))
            printf("timestamp difference too big at frame %d: %d ns\n", frameId, metas[0][frameId]->timestampNs - metas[0][frameId + 1]->timestampNs);
        if ((frameId + 1 < frameCount) &&
                        (metas[0][frameId + 1]->frameCount - metas[0][frameId]->frameCount > 1))
            printf("frame count difference too big at frame %d: %d\n", frameId, metas[0][frameId]->frameCount - metas[0][frameId + 1]->frameCount);
        free(frames[0][frameId]);
        free(frames[1][frameId]);
        free(metas[0][frameId]);
        free(metas[1][frameId]);
        free(imus[frameId]);
    }
    printf("done, exiting\n");
    exit(0);
}

int main()
{
    cv_init_display();

    XLinkHandler_t handler;
    handler.devicePath = "/dev/ttyACM0";
    handler.devicePath2 = "/dev/ttyACM1";
    XLinkInitialize(&handler);
    usbLinkStream = XLinkOpenStream(X_LINK_STREAM_NAME, 5*1024*1024);
    int f = 0;
    FILE *fp;
    int lt1ns,lt2ns;
    int t1ns,t2ns;
    float et1,et2;

    system("mkdir " OUTPUT_DIR);

    frames[0] = (uint16_t**) malloc(MAX_FRAME_COUNT * sizeof(uint16_t**));
    frames[1] = (uint16_t**) malloc(MAX_FRAME_COUNT * sizeof(uint16_t**));
    metas[0] = (frameMetadataGeneric**) malloc(MAX_FRAME_COUNT * sizeof(frameMetadataGeneric**));
    metas[1] = (frameMetadataGeneric**) malloc(MAX_FRAME_COUNT * sizeof(frameMetadataGeneric**));
    imus = (imuFrame**) malloc(MAX_FRAME_COUNT * sizeof(imuFrame**));

    while (frameCount < MAX_FRAME_COUNT)
    {
        int i;
        // allocate pointers for data structures
        frames[0][frameCount] = (uint16_t*) malloc(FRAME_WIDTH * FRAME_HEIGHT * sizeof(uint16_t));
        CHECK_MALLOC(frames[0][frameCount]);
        frames[1][frameCount] = (uint16_t*) malloc(FRAME_WIDTH * FRAME_HEIGHT * sizeof(uint16_t));
        CHECK_MALLOC(frames[1][frameCount]);
        metas[0][frameCount] = (frameMetadataGeneric*) malloc(sizeof(frameMetadataGeneric));
        CHECK_MALLOC(metas[0][frameCount]);
        metas[1][frameCount] = (frameMetadataGeneric*) malloc(sizeof(frameMetadataGeneric));
        CHECK_MALLOC(metas[1][frameCount]);
        imus[frameCount] = (imuFrame*) malloc(sizeof(imuFrame));
        CHECK_MALLOC(imus[frameCount]);
        // get current time
        //printf("-----------------------\n");
        clock_gettime(CLOCK_REALTIME, &spec);
        ms1 = spec.tv_nsec/1000000;

        //read first image
        readImageAndMeta(NULL, frames[0][frameCount], metas[0][frameCount]);
        //read IMU
        readImageAndMeta(NULL, frames[1][frameCount], metas[1][frameCount]);
        //read IMU
        int imuSize;
        fullRead(NULL, (uint8_t*) &imuSize, sizeof(int));
        fullRead(NULL, (uint8_t*) imus[frameCount], imuSize);
        process(frames[0][frameCount], frames[1][frameCount]);
        frameCount++;
        key = cv::waitKey(1);
        if(key == 27)
            saveAndExit(USER_EXIT);
    }
    saveAndExit(MAX_FRAMES);
    return 0;
}
