#This Makefile should be included two times: before and after including generic.mk

ifeq ($(MODULES_MK_FIRST_PART),)
MODULES_MK_FIRST_PART:=1

LEON_COMPONENT_PATHS         += $(patsubst %,$(MODULES_DIR)/%/leon,$(AppModuleList))
LEON_COMPONENT_PATHS         += $(patsubst %,$(MODULES_DIR)/%/shared,$(AppModuleList))
LEON_COMPONENT_PATHS         += $(patsubst %,$(MODULES_DIR)/%/shared*,$(AppModuleList))

LEON_COMPONENT_PATHS_BM     += $(patsubst %,$(MODULES_DIR)/%/leon/bm,$(AppModuleList))
LEON_COMPONENT_PATHS_RTEMS     += $(patsubst %,$(MODULES_DIR)/%/leon/rtems,$(AppModuleList))

LEON_COMPONENT_HEADERS_PATHS += $(patsubst %,$(MODULES_DIR)/%/*include*,$(AppModuleList))
LEON_COMPONENT_HEADERS_PATHS += $(patsubst %,$(MODULES_DIR)/%/*,$(AppModuleList))

SH_COMPONENTS_HEADERS_PATHS  += $(patsubst %,$(MODULES_DIR)/%/shared*,$(AppModuleList))

CCOPT_EXTRA           +=  -D'VCSHOOKS_USE_SOFTWARE=1'

else

endif
