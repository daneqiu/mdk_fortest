#include <stdint.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <semaphore.h>
//project specific
#include "ipipe.h"
#include "ic_main.h"

#include "ipipeUtils.h"
#include "sendOutApi.h"

#define PRINT_LOS_SENT_MESSAGES(...)
#define PRINT_LOS_RECEIVED_MESSAGES(...)

#define DDR_BUFFER_ALOCATED_MEM_SIZE (4208*3140*78/8)


uint32_t frmCounterTotal[IPIPE_MAX_OUTPUTS_ALlOWED];


uint8_t ddrStaticAlocatedMemory[DDR_BUFFER_ALOCATED_MEM_SIZE] DDR_BUF_DATA;
static icCtrl       *ctrl = (icCtrl*)0;
static pthread_t    eventThread;
int32_t             ip_ready = 0;
sem_t               semWaitForLrtReady;
sem_t               semWaitForSourceCommit;
sem_t               semWaitForSourceReady;
sem_t               semWaitForSourceStoped;


extern void incCamSendFrameEnd  (int32_t sourceInstance, int32_t seqNo, uint64_t ts);
extern void incCamSendFrameStart(int32_t sourceInstance, int32_t seqNo, uint64_t ts);
extern void incCamIspStart     (int32_t ispInstance, int32_t seqNo, uint32_t userData);
extern void incCamIspEnd       (int32_t ispInstance, int32_t seqNo, uint32_t userData);
extern void incCamSendOuputData(FrameT *dataBufStruct, uint32_t outputId);

static void *eventLoop(void *vCtrl);

void los_start(void *arg)
{
    UNUSED(arg);
    PRINT_LOS_SENT_MESSAGES("%s:\n",__func__);
    int        rc;

    /// Initialize the semaphore for waiting for lrt ready
    if((rc = sem_init(&semWaitForLrtReady, 0, 0)) == -1)
        printf("pthread_create: %s\n", strerror(rc));
    ctrl = icSetup(1,(uint32_t)ddrStaticAlocatedMemory,sizeof(ddrStaticAlocatedMemory));

    //Create the Event thread.  This thread blocks waiting for events
    //from the IPIPE client API.
    if ((rc = pthread_create(&eventThread, NULL, eventLoop, (void*)ctrl)) != 0) {
        printf("pthread_create: %s\n", strerror(rc));
        exit(1);
    }
    // wait for lrt ready event, this function is blocking, waiting for that
    if(sem_wait(&semWaitForLrtReady) == -1) {
        printf("sem_wait error\n");
    }
    sem_destroy(&semWaitForLrtReady);

    sendOutInit();
}

void los_stop(void) {
    PRINT_LOS_SENT_MESSAGES("%s:\n",__func__);
    icTeardown((icCtrl*)ctrl);
    /* Stop the event loop thread and wait for it to exit */
    pthread_join(eventThread, NULL);
    sendOutFini();
}

void los_ConfigureSource(int srcIdx, icSourceConfig *sconf)
{
    PRINT_LOS_SENT_MESSAGES("%s:\n",__func__);
    icConfigureSource (ctrl, (icSourceInstance)srcIdx, sconf);
}

void los_SetupSource(int srcIdx, icSourceSetup *sconf) {
    PRINT_LOS_SENT_MESSAGES("%s:\n",__func__);
    icSetupSource (ctrl, (icSourceInstance)srcIdx, sconf);
}

void los_SetupSourceCommit(void) {
    int        rc;
    PRINT_LOS_SENT_MESSAGES("%s:\n",__func__);
    /// Initialize the semaphore for waiting for source commit response
    if((rc = sem_init(&semWaitForSourceCommit, 0, 0)) == -1)
        printf("pthread_create: %s\n", strerror(rc));
    icSetupSourcesCommit(ctrl);
    // wait for lrt ready event, this function is blocking, waiting for that
    if(sem_wait(&semWaitForSourceCommit) == -1) {
        printf("sem_wait error\n");
    }
    sem_destroy(&semWaitForSourceCommit);
}

void los_configIsp(void *iconf, int ispIdx) {
    PRINT_LOS_SENT_MESSAGES("%s:\n",__func__);
    icConfigureIsp(ctrl, ispIdx, iconf);
}

int  los_startSource (int srcIdx) {
    int        rc;
    PRINT_LOS_SENT_MESSAGES("%s:\n",__func__);
    /// Initialize the semaphore for waiting for source commit response
    if((rc = sem_init(&semWaitForSourceReady, 0, 0)) == -1)
        printf("pthread_create: %s\n", strerror(rc));
    icStartSource(ctrl, (icSourceInstance)srcIdx);
    // wait for source to be ready. Otherwise will not be possible other
    // configuration
    if(sem_wait(&semWaitForSourceReady) == -1) {
        printf("sem_wait error\n");
    }
    sem_destroy(&semWaitForSourceReady);
    return 0;
}

int  los_stopSource (int srcIdx) {
    int        rc;
    PRINT_LOS_SENT_MESSAGES("%s:\n",__func__);
    /// Initialize the semaphore for waiting for source commit response
    if((rc = sem_init(&semWaitForSourceStoped, 0, 0)) == -1)
        printf("pthread_create: %s\n", strerror(rc));
    icStopSource(ctrl, (icSourceInstance)srcIdx);
    // wait for source to be stopped
    if(sem_wait(&semWaitForSourceStoped) == -1) {
        printf("sem_wait error\n");
    }
    sem_destroy(&semWaitForSourceStoped);
    return 0;
}

void los_dataWasSent (FrameT *dataBufStruct, uint32_t outputId, uint32_t frmType) {
    UNUSED(outputId);
    UNUSED(frmType);
    PRINT_LOS_SENT_MESSAGES("%s:\n",__func__);
    icDataReceived(ctrl, dataBufStruct);
}

static void *eventLoop(void *vCtrl) {
    icEvent	ev;
    unsigned int     evno;
    icCtrl * lpCtrl = (icCtrl *)vCtrl;
    if(0 == vCtrl) {
        return NULL;
    }
    while (1) {
        if(!icGetEvent(lpCtrl, &ev)) {
            evno = ev.ctrl & IC_EVENT_CTRL_TYPE_MASK;
            switch (evno) {
                case IC_EVENT_TYPE_LEON_RT_READY:
                    PRINT_LOS_RECEIVED_MESSAGES("IC_EVENT_TYPE_LEON_RT_READY \n");
                    if(sem_post(&semWaitForLrtReady) == -1)  {/// Inform that lrt is ready
                        printf("sem_post error\n");
                    }
                    break;
                case IC_EVENT_TYPE_SETUP_SOURCES_RESULT:
                    PRINT_LOS_RECEIVED_MESSAGES("IC_EVENT_TYPE_CONFIG_SOURCES_RESULT \n");
                    if(sem_post(&semWaitForSourceCommit) == -1)  {/// Inform that lrt is ready
                        printf("sem_post error\n");
                    }
                    break;
                case IC_EVENT_TYPE_SOURCE_READY:
                    PRINT_LOS_RECEIVED_MESSAGES("IC_EVENT_TYPE_SOURCE_READY \n");
                    if(sem_post(&semWaitForSourceReady) == -1)  {/// Inform that lrt is ready
                        printf("sem_post error\n");
                    }
                    break;
                case IC_EVENT_TYPE_READOUT_START:
                    // not implemented
                    //incCamSendFrameStart(ev.u.lineEvent.sourceInstance, ev.u.lineEvent.seqNo, ev.u.lineEvent.ts);
                    break;
                case IC_EVENT_TYPE_READOUT_END:
                    //PRINT_LOS_RECEIVED_MESSAGES("IC_EVENT_TYPE_READOUT_END \n");
                    // triger updating isp parameters is call at every eof event
                    incCamSendFrameEnd(ev.u.lineEvent.sourceInstance, ev.u.lineEvent.seqNo, ev.u.lineEvent.ts);
                    break;

                case IC_EVENT_TYPE_SOURCE_STOPPED:
                    PRINT_LOS_RECEIVED_MESSAGES("IC_EVENT_TYPE_SOURCE_STOPPED \n");
                    if(sem_post(&semWaitForSourceStoped) == -1)  {/// Inform that lrt is ready
                        printf("sem_post error\n");
                    }
                    break;
                case IC_EVENT_TYPE_SEND_OUTPUT_DATA: {
                    FrameT *frame;
                    uint32_t outId;
                    uint32_t frmType;
                    frame   = ev.u.sentOutput.dataBufStruct;
                    // guzzi create a dependency between source id and output id, so here will be the source id
                    // output comming from an isp instance
                    if (ctrl->icPipelineDescription.icQueryOutput[ev.u.sentOutput.outputId]->attrs <= IC_OUTPUT_FRAME_DATA_TYPE_METADATA_STILL) {
                        outId   = ctrl->icPipelineDescription.icQueryOutput[ev.u.sentOutput.outputId]->dependentSources;
                        frmType = ctrl->icPipelineDescription.icQueryOutput[ev.u.sentOutput.outputId]->attrs; //type is still, video ...
                    }
                    else {
                      //it is a app specific output type. In this case as not directly dependent output id by source is not supported,
                      //will be made a hack, depth output will be register as  output id 2, FRAME_DATA_TYPE_STILL, FRAME_DATA_FORMAT_YUV420
                        outId = 2;
                      //frmFmt = FRAME_T_FORMAT_YUV420;
                        frmType = IC_OUTPUT_FRAME_DATA_TYPE_STILL;
                    }
//                    printf("\n\n%d %d",frame->stride[0], frame->height[0]);
                    sendOutSend(frame, outId, frmType, los_dataWasSent);
                    frmCounterTotal[ev.u.sentOutput.outputId]++;
                    }
                    break;
                case IC_EVENT_TYPE_TORN_DOWN:
                    PRINT_LOS_RECEIVED_MESSAGES("IC_EVENT_TYPE_TORN_DOWN \n");
                    //printf("tm: %llu  \n",lpCtrl->curTime);
                    return NULL;
                    break;
                default:
                    PRINT_LOS_RECEIVED_MESSAGES ("%s: Unhandled evt %d:\n", __func__, evno);
                    break;
            }
        }
        else { // no activity in last period, lrt crash or lrt is started, but no camera connected
               // in this case cut down lrt
            printf("Error X\n");
        }
    }
    return NULL;
}