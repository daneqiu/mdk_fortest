///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief
///

// 1: Includes
// ----------------------------------------------------------------------------
#ifndef SOURCE_PARAM_AR0330_H
#define SOURCE_PARAM_AR0330_H
#include "ipipe.h"
#include "Board182Api.h"
#include "CamSensor.h"
#include "ar0330_2L_1920x1080_Raw10_30Hz.h"
#include "DrvGpio.h"


// 2:  Source specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------


enum {

    AR0330_30FPS_CAMAL              = 0,
    AR0330_30FPS_CAMAR              = 1,
    AR0330_30FPS_CAMBL              = 2,
    AR0330_30FPS_CAMBR              = 3,
    AR0330_30FPS_CAMCL              = 4,
    AR0330_30FPS_CAMCR              = 5,

    MAX_SENSOR_CONFIG               = 6
};

// same reset gpio for all cams, in consequence will not be anymore individual reset
const GpioConfigDescriptor ar0330CamAllGpios[] =
{
        {
                .configType = RESET_PIN,
                .gpioNumber = 59,
                .activeLevel = 1,
                .delayMs = 300,   // 3.41 at 24MHz = 8192 MCLK's
        },
        {
                .configType = RESET_PIN,
                .gpioNumber = 59,
                .activeLevel = 0,
                .delayMs = 300,   // 3.41 at 24MHz = 8192 MCLK's
        },
        {
                .configType = END, 0, 0, 0
        },
};

// same reset gpio for all cams, in consequence will not be anymore individual reset
const GpioConfigDescriptor ar0330CamGpiosNothing[] = {
        {
                .configType = END, 0, 0, 0
        },
};


SensorConfiguration sensorConfigurations[MAX_SENSOR_CONFIG] = {
        {&ar0330_2L_1920x1080_RAW10_30Hz_camCfg , &gAppDevHndls.i2c1Handle, 0, ar0330CamGpiosNothing},
        {&ar0330_2L_1920x1080_RAW10_30Hz_camCfg , &gAppDevHndls.i2c1Handle, 1, ar0330CamGpiosNothing},
        {&ar0330_2L_1920x1080_RAW10_30Hz_camCfg , &gAppDevHndls.i2c2Handle, 0, ar0330CamGpiosNothing},
        {&ar0330_2L_1920x1080_RAW10_30Hz_camCfg , &gAppDevHndls.i2c2Handle, 1, ar0330CamGpiosNothing},
        {&ar0330_2L_1920x1080_RAW10_30Hz_camCfg , &gAppDevHndls.i2c0Handle, 0, ar0330CamGpiosNothing},
        {&ar0330_2L_1920x1080_RAW10_30Hz_camCfg , &gAppDevHndls.i2c0Handle, 1, ar0330CamGpiosNothing},
};

#define CRAR 0
// it is repeated 3 times, in order fit with sensor configuration, no difference between 30fps and 15 fos for mipi configuration
icSourceConfig srcCfg[MAX_SENSOR_CONFIG] = {

        {{1920,1080},  {0, 0   ,1920,1080-CRAR}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_0, 2, 490, IC_IPIPE_RAW_10, 1, IC_CIF0_DEVICE4}, ""},
        {{1920,1080},  {0, CRAR,1920,1080-   0}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_1, 2, 490, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE0}, ""},
        {{1920,1080},  {0, CRAR,1920,1080-   0}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_2, 2, 490, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE1}, ""},
        {{1920,1080},  {0, 0   ,1920,1080-CRAR}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_3, 2, 490, IC_IPIPE_RAW_10, 1, IC_CIF1_DEVICE5}, ""},
        {{1920,1080},  {0, CRAR,1920,1080-   0}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_4, 2, 490, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE3}, ""},
        {{1920,1080},  {0, CRAR,1920,1080-   0}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_5, 2, 490, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE2}, ""},


};

#define CBPP 10 //bits per pixel
#define CBPP_CIF 16 //bits per pixel

icSourceSetup sourceSetup[MAX_SENSOR_CONFIG] = {
        //IMX208_15FPS

        {1920, 1080, CBPP_CIF, (1920 * 1080), 1,1,1,1,0},
        {1920, 1080, CBPP, (1920 * 1080), 1,1,1,1,0},
        {1920, 1080, CBPP, (1920 * 1080), 1,1,1,1,0},
        {1920, 1080, CBPP_CIF, (1920 * 1080), 1,1,1,1,0},
        {1920, 1080, CBPP, (1920 * 1080), 1,1,1,1,0},
        {1920, 1080, CBPP, (1920 * 1080), 1,1,1,1,0},
};
#undef CBPP

#endif //SOURCE_PARAM_AR0330_H

