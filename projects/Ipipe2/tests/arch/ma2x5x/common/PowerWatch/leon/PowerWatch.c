
/**************************************************************************************************

 @File         : PowerWatch.c
 @Author       : MT
 @Brief        : Contains Interface for capture power
 Date          : 01 - October - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : � Movidius Srl 2014, � Movidius Ltd 2015

 Description :


 **************************************************************************************************/

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
// stl
#include <stdlib.h>
#include "assert.h"
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <semaphore.h>
#include "rtems.h"
#include "brdMv0198.h"
#include "VcsHooksApi.h"
#include "DrvTempSensor.h"

#ifdef MV0212
#include "MV0212.h"
#else
#include "Board182Api.h"
#endif
#include "PowerWatch.h"
#include "app_config.h"

/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/


/**************************************************************************************************
 ~~~  Local variables
 **************************************************************************************************/
typedef struct
{
    tyAdcResultAllRails power;
    fp32 avgmW;
    fp32 avgmA;
} Prof;

// ~~~~~~~~~~~ Power measure variable ~~~~~~~~~~~~~~~~~~~~~~~~~
static tyBrd198Handle powerMonHandle;
static Prof test;
fp32 temperature    = 0.0f;
fp32 totalPower     = 0.0f;
fp32 nrOfCapture    = 0.0f;
fp32 maxPower       = 0.0f;

pthread_t       thrDisplayPowerThr;
pthread_t       thrPowerMeasureThr;

int showPowerNow = 0;
/**************************************************************************************************
 ~~~ Imported Function Declaration
 **************************************************************************************************/

/**************************************************************************************************
 ~~~ Local File function declarations
 **************************************************************************************************/
static void* displayPowerThr(void *unused);
static void* powerMeasureThr(void *unused);
/**************************************************************************************************
 ~~~ Functions Implementation
 **************************************************************************************************/
void initPowerMeasurement(void)
{
    int rc;
#ifdef MV0212
    I2CM_Device* i2cBus = i2c2Handle;
#else
    I2CM_Device* i2cBus = gAppDevHndls.i2c2Handle;
#endif
    s32 retVal;

    I2CM_Device* i2c = i2cBus;
    if (i2c == NULL)
    {
        printf("I2C not initialized for MV0198\n");
    }

    retVal = Brd198Init(&powerMonHandle, i2c, NULL);
    if (retVal != DRV_BRD198_DRV_SUCCESS)
    {
        printf("Board 198 init error\n");
        exit(-1);
    }
    if ((rc = pthread_create(&thrPowerMeasureThr, NULL, powerMeasureThr, (void*)NULL)) != 0) {
        printf("pthread_create: %s\n", strerror(rc));
        exit(1);
    }
    sleep(1);
    //showPower();
    //sleep(1);
}

void showPower(void) {
    struct timespec tp;
    DrvTempSensorGetTemp(&temperature);
    clock_gettime(CLOCK_REALTIME, &tp);
    printf("\nTime = %5lds %8ldus \n", tp.tv_sec, tp.tv_nsec/1000);
    sleep(1);
    printf("AvgPower mW = %7.03f, MaxPower mW = %7.03f, CrtTemp=%2.1fC\n", totalPower/nrOfCapture, maxPower, temperature);
    sleep(1);
}

/**************************************************************************************************
 ~~~ Local Functions Implementation
 **************************************************************************************************/

static void* displayPowerThr(void *unused) {
    while(1) {
        showPower();
        sleep(1*60);
    }
}

static void* powerMeasureThr(void *unused) {
    int rc;
    DrvTempSensorInit();
    if ((rc = pthread_create(&thrDisplayPowerThr, NULL, displayPowerThr, (void*)NULL)) != 0) {
        printf("pthread_create: %s\n", strerror(rc));
        exit(1);
    }
    while(1) {
        showPowerNow = swcLeonReadNoCacheU32((uint32_t)&showPowerNow);
        Brd198SampleAllRails(&powerMonHandle,&(test.power));
        //fp32 temperature    = 0.0f;
        totalPower     = totalPower + test.power.totalMilliWatts;
        nrOfCapture    = nrOfCapture + 1.0f;
        if(maxPower < test.power.totalMilliWatts) maxPower = test.power.totalMilliWatts;
        sleep(1);
        if(showPowerNow) {
            showPower();
            sleep(1);
            showPowerNow = 0;
        }
    }
}
