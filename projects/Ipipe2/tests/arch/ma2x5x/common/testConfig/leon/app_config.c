
/**************************************************************************************************

 @File         : app_config.c
 @Author       : xx
 @Brief        : Application configuration
 Date          : 01 - May - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : � Movidius Srl 2014, � Movidius Ltd 2015

 Description :

 **************************************************************************************************/

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include <OsDrvCpr.h>
#include <DrvShaveL2Cache.h>
#include "DrvTimer.h"
//#include <osal_stdlib.h>

#include "DrvDdr.h"

#ifdef MV0212
#include "MV0212.h"
#include "brdGpioCfgs/brdMv0182R5GpioDefaults.h"
#else
#include "Board182Api.h"
#endif

#include "DrvCDCEL.h"
#include "app_config.h"
#include "stdio.h"

/**************************************************************************************************
 ~~~  Specific #defines and types (typedef,enum,struct)
**************************************************************************************************/
#define CMX_CONFIG_SLICE_7_0       (0x11111111)
#define CMX_CONFIG_SLICE_15_8      (0x11111111)
#define L2CACHE_CFG                (SHAVE_L2CACHE_NORMAL_MODE)

#define CLOCKS_MIPICFG (AUX_CLK_MASK_MIPI_ECFG | AUX_CLK_MASK_MIPI_CFG)
#define REM_CLOCKS        (    AUX_CLK_MASK_I2S0  |     \
                                AUX_CLK_MASK_I2S1  |     \
                                AUX_CLK_MASK_I2S2  )

/**************************************************************************************************
 ~~~  Global Data (Only if absolutely necessary)
**************************************************************************************************/
CmxRamLayoutCfgType __attribute__((section(".cmx.ctrl"))) __cmx_config = {
        CMX_CONFIG_SLICE_7_0, CMX_CONFIG_SLICE_15_8};

#ifdef MV0212
#define NUM_I2C_DEVS 3
I2CM_Device * i2c0Handle = NULL;
I2CM_Device * i2c1Handle = NULL;
I2CM_Device * i2c2Handle = NULL;
#endif

/**************************************************************************************************
 ~~~  Static Local Data
 **************************************************************************************************/
tyAuxClkDividerCfg auxClkAllOn[] =
{
    {AUX_CLK_MASK_UART, CLK_SRC_REFCLK0, 96, 625},   // Give the UART an SCLK that allows it to generate an output baud rate of of 115200 Hz (the uart divides by 16)
    {
        .auxClockEnableMask     = REM_CLOCKS,                // all rest of the necessary clocks
        .auxClockSource         = CLK_SRC_SYS_CLK,            //
        .auxClockDivNumerator   = 1,                          //
        .auxClockDivDenominator = 1,                          //
    },
    {
        .auxClockEnableMask     = AUX_CLK_MASK_LCD ,          // LCD clock
        .auxClockSource         = CLK_SRC_SYS_CLK,
        .auxClockDivNumerator   = 1,
        .auxClockDivDenominator = 1
    },
    {
        .auxClockEnableMask     = AUX_CLK_MASK_MEDIA,         // SIPP Clock
        .auxClockSource         = CLK_SRC_SYS_CLK ,      //
        .auxClockDivNumerator   = 1,                          //
        .auxClockDivDenominator = 1,                          //
    },
    {
        .auxClockEnableMask     = (AUX_CLK_MASK_CIF0 | AUX_CLK_MASK_CIF1 ),  // CIFs Clock
        .auxClockSource         = CLK_SRC_SYS_CLK,            //
        .auxClockDivNumerator   = 1,                          //
        .auxClockDivDenominator = 1,                          //
    },
    {
        .auxClockEnableMask     = CLOCKS_MIPICFG,             // MIPI CFG + ECFG Clock
        .auxClockSource         = CLK_SRC_SYS_CLK     ,       //
        .auxClockDivNumerator   = 1,                          //
        .auxClockDivDenominator = (uint32_t)(DEFAULT_APP_CLOCK_KHZ/24000),                         //
    },
    {
        .auxClockEnableMask = AUX_CLK_MASK_MIPI_TX0 | AUX_CLK_MASK_MIPI_TX1,
        .auxClockSource = CLK_SRC_REFCLK0,
        .auxClockDivNumerator = 1,
        .auxClockDivDenominator = 1
    }, // ref clocks for MIPI PLL,
    {
        .auxClockEnableMask = (u32)(1 << CSS_AUX_TSENS),
        .auxClockSource = CLK_SRC_REFCLK0,
        .auxClockDivNumerator = 1,
        .auxClockDivDenominator = 10,
    },
    {
            .auxClockEnableMask     = AUX_CLK_MASK_USB_PHY_EXTREFCLK,
            .auxClockSource         = CLK_SRC_PLL0,
            .auxClockDivNumerator   = 1,
            .auxClockDivDenominator = 24
    },
    {
            .auxClockEnableMask     = AUX_CLK_MASK_USB_PHY_REF_ALT_CLK,
            .auxClockSource         = CLK_SRC_PLL0,
            .auxClockDivNumerator   = 1,
            .auxClockDivDenominator = 24
    },
    {
            .auxClockEnableMask     = AUX_CLK_MASK_USB_CTRL_REF_CLK,
            .auxClockSource         = CLK_SRC_PLL0,
            .auxClockDivNumerator   = 1,
            .auxClockDivDenominator = 24
    },
    {
            .auxClockEnableMask     = AUX_CLK_MASK_USB_CTRL_SUSPEND_CLK,
            .auxClockSource         = CLK_SRC_PLL0,
            .auxClockDivNumerator   = 1,
            .auxClockDivDenominator = 24
    },
    {0,0,0,0}, // Null Terminated List
};

#ifdef MV0212
static s32 BoardInitialization(void);
#endif

//#############################################################################
#include <DrvLeonL2C.h>
void leonL2CacheInitWrThrough()
{
    LL2CConfig_t ll2Config;

    // Invalidate entire L2Cache before enabling it
    DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_INVALIDATE, /*disable cache?:*/ 0);

    ll2Config.LL2CEnable = 1;
    ll2Config.LL2CLockedWaysNo = 0;
    ll2Config.LL2CWayToReplace = 0;
    ll2Config.busUsage = BUS_WRAPPING_MODE;
    ll2Config.hitRate = HIT_WRAPPING_MODE;
    ll2Config.replacePolicy = LRU;
    ll2Config.writePolicy = WRITE_THROUGH;

    DrvLL2CInitialize(&ll2Config);
}


/**************************************************************************************************
 ~~~ Functions Implementation
**************************************************************************************************/
int initClocksAndMemory(void)
{
    // Configure the system
    OsDrvCprInit();
    OsDrvCprOpen();
    //OsDrvCprSetupClocks(&clockConfigs);
    DrvTimerInit();
    OsDrvCprAuxClockArrayConfig(auxClkAllOn);
    leonL2CacheInitWrThrough();
    // Set the shave L2 Cache mode
    DrvShaveL2CacheSetMode(L2CACHE_CFG);
    // Enable PMB subsystem gated clocks and resets
    SET_REG_WORD   (CMX_RSTN_CTRL,0x00000000);               // engage reset
    SET_REG_WORD   (CMX_CLK_CTRL, 0xffffffff);               // turn on clocks
    SET_REG_WORD   (CMX_RSTN_CTRL,0xffffffff);               // dis-engage reset
    // Enable media subsystem gated clocks and resets
    SET_REG_WORD(MSS_CLK_CTRL_ADR,      0xffffffff);
    SET_REG_WORD(MSS_RSTN_CTRL_ADR,     0xffffffff);
    SET_REG_WORD(MSS_SIPP_CLK_CTRL_ADR, 0xffffffff);
    DrvCprStartAllClocks();

    DrvCprSysDeviceAction(MSS_DOMAIN, ASSERT_RESET,  DEV_MSS_LCD |  DEV_MSS_CIF0 | DEV_MSS_CIF1 | DEV_MSS_SIPP);
    DrvCprSysDeviceAction(MSS_DOMAIN, DEASSERT_RESET, -1);
    DrvCprSysDeviceAction(CSS_DOMAIN, DEASSERT_RESET, -1);
    DrvCprSysDeviceAction(UPA_DOMAIN, DEASSERT_RESET, -1);
    DrvDdrInitialise(NULL);
    DrvCprStartAllClocks();
#ifdef MV0212
    (void)BoardInitialization();
#else
    assert(0 == BoardInitialise(EXT_PLL_CFG_148_24_24MHZ));
#endif

    return 0;
}

#ifdef MV0212
static s32 BoardInitialization(void)
{
    s32 boardStatus;
//#ifdef MV0212
    int32_t rc;
    uint32_t rev;
    BoardI2CInfo info[NUM_I2C_DEVS];
    BoardConfigDesc config[] =
    {
        {
            BRDCONFIG_GPIO,
            // use this for the sake of testing as it has the same gpio config as the MV0212 R0
            (void *)brdMV0182R5GpioCfgDefault
        },
        {
            BRDCONFIG_END,
            NULL
        }
    };

    rc = BoardInit(config);
    if (rc!=BRDCONFIG_SUCCESS)
    {
    	printf("Error: board initialization failed with %ld status\n",
    	    			rc);
        return rc;
    }

    rc = BoardGetPCBRevision(&rev);
    if (rc!=BRDCONFIG_SUCCESS)
    {
    	printf("Error: board configuration read failed with %ld status\n",
    	    			rc);
        return rc;
    }
    printf("Board Mv0212 initialized, revision = %lu \n", rev);

    boardStatus = BoardInitExtPll(EXT_PLL_CFG_148_24_24MHZ);
    if (boardStatus != BRDCONFIG_SUCCESS)
    {
    	printf("Error: board initialization failed with %ld status\n",
    			boardStatus);
    	return -1;
    }
    rc = BoardGetI2CInfo(info, NUM_I2C_DEVS);
    i2c0Handle=info[0].handler;
    i2c1Handle=info[1].handler;
    i2c2Handle=info[2].handler;
//#elif defined(MV0182)
//    boardStatus = BoardInitialise(EXT_PLL_CFG_148_24_24MHZ);
//    i2c0Handle=gAppDevHndls.i2c0Handle;
//    i2c1Handle=gAppDevHndls.i2c1Handle;
//    i2c2Handle=gAppDevHndls.i2c2Handle;
//    if (boardStatus != B_SUCCESS)
//    {
//    	printf("Error: board initialization failed with %ld status\n",
//    			boardStatus);
//    	return -1;
//    }
//#endif
}
#endif
