
#include <stdio.h>
#include <stdint.h>

#define I_IMG_W 816
#define I_IMG_H 616

#define OFF_Y    68
#define OFF_X     88

#define O_IMG_W  640
#define O_IMG_H   480

uint16_t iPack8[I_IMG_W*I_IMG_H];


//###################################################
int main(void)
{
  FILE *f = fopen("in_raw10_svga_816x616_P400_10b.raw", "rb");
  int y;

printf("f: %p \n", f);
printf("sz: %d \n", sizeof(iPack8));

fread(iPack8, 1, sizeof(iPack8), f); 
fclose(f);

printf("read \n");
  f = fopen("./in_raw10_vga_640x480_P400_10b.raw", "wb");

  for(y=0; y<O_IMG_H; y++)
  {
printf("%d \n",y);
     fwrite(&iPack8[(y+OFF_Y) * (I_IMG_W) + (OFF_X)], 1, O_IMG_W *2,   f);
printf("%d \n",y);
  }

  fclose(f);
return 0;
}
