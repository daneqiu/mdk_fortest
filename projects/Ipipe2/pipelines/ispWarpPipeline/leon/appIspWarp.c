/**************************************************************************************************

 @File         : app3CamIsp.c
 @Author       : MT
 @Brief        : Pipeline with 1x ISP and PlgWarp
 Date          : 01 - March - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : � Movidius Srl 2014, � Movidius Ltd 2015

 Description :


 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include "DrvLeonL2C.h"
#include "ipipe.h"
#include "ipipeDbg.h"
#include "FrameMgrUtils.h"
#include "ipipeUtils.h"
#include "ipipeOpipeUtils.h"
#include "FrameMgrApi.h"
#include "IpipeServerApi.h"
#include "ipipeMsgQueue.h"
#include "PlgSourceApi.h"
#include "PlgFifoApi.h"
#include "PlgIspFullApi.h"
#include "PlgWarpApi.h"
#include "appIspWarp.h"
#include <DrvShaveL2Cache.h>

//#include "fisheye_mesh.h"
#include "init_mesh_y.h"
#include "init_mesh_uv.h"
/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/
#define MAX_NR_OF_CAMS              1
#define NR_OF_BUFFERS_PER_SOURCE    2
#define NR_OF_BUFFERS_PER_ISP_OUT   2
#define NR_OF_BUFFERS_PER_WARP_OUT  3

#define IMG_W_IN_WARP (2880)
#define IMG_H_IN_WARP (2160)

#define IMG_W_OUT_WARP (1920)
#define IMG_H_OUT_WARP (1088)

#define MESH_CELL_SIZE 16
#define MESH_WIDTH     MESH_WIDTH_F
#define MESH_HEIGHT    MESH_HEIGHT_F
#define MESH_WIDTH_C   MESH_WIDTH_F_CHROMA
#define MESH_HEIGHT_C  MESH_HEIGHT_F_CHROMA

/**************************************************************************************************
 ~~~  Local variables
 **************************************************************************************************/
static PlgSource      plgSource[MAX_NR_OF_CAMS] SECTION(".cmx_direct.data") ALIGNED(8);
static PlgIspFull     plgVdo1  [MAX_NR_OF_CAMS] SECTION(".cmx_direct.data") ALIGNED(8);
static PlgFifo        plgFifo  SECTION(".cmx_direct.data") ALIGNED(8);
static PlgWarp        plgWarp  [MAX_NR_OF_CAMS] SECTION(".cmx_direct.data") ALIGNED(8);

static FramePool frameMgrPoolC    [MAX_NR_OF_CAMS];
static FramePool frameMgrPoolFifo [MAX_NR_OF_CAMS];
static FramePool frameMgrPoolVideo[MAX_NR_OF_CAMS];
static FramePool frameMgrPoolWarp [MAX_NR_OF_CAMS];

static FrameT *frameMgrFrameC    [MAX_NR_OF_CAMS];
static FrameT *frameMgrFrameVideo[MAX_NR_OF_CAMS];
static FrameT *frameMgrFrameWarp [MAX_NR_OF_CAMS];

static uint32_t        startSrcState[MAX_NR_OF_CAMS];
static uint32_t        stopSrcState [MAX_NR_OF_CAMS];
static icSourceConfig  *startSrcLocConfig[MAX_NR_OF_CAMS];
static uint32_t        tearDownEnable;
static void            *nextCfg [MAX_NR_OF_CAMS];
static uint32_t        previewAbleMode;
static FrameProducedCB cbOutputList[MAX_NR_OF_CAMS*2];

SECTION(".cmx_direct.data") static volatile meshStruct init_mesh_Y0 =
{
    .meshWidth  = MESH_WIDTH,
    .meshHeight = MESH_HEIGHT,
    .meshX      = (float*)initMeshY_x,
    .meshY      = (float*)initMeshY_y,
    .coord_min_x = 0,
    .coord_max_x = 960,
    .coord_min_y = 0,
    .coord_max_y = 1088
};
SECTION(".cmx_direct.data") static volatile meshStruct init_mesh_Y1 =
{
    .meshWidth  = MESH_WIDTH,
    .meshHeight = MESH_HEIGHT,
    .meshX      = (float*)initMeshY_x,
    .meshY      = (float*)initMeshY_y,
    .coord_min_x = 960,
    .coord_max_x = 1920,
    .coord_min_y = 0,
    .coord_max_y = 1088
};
SECTION(".cmx_direct.data") static volatile meshStruct init_mesh_UV =
{
    .meshWidth  = MESH_WIDTH_C,
    .meshHeight = MESH_HEIGHT_C,
    .meshX      = (float*)initMeshChroma_x,
    .meshY      = (float*)initMeshChroma_y,
    .coord_min_x = 0,
    .coord_max_x = 960,
    .coord_min_y = 0,
    .coord_max_y = 544
};

/**************************************************************************************************
 ~~~ Local File function declarations
 **************************************************************************************************/
static void     turnOfapp3Cam(void);
static uint32_t checkTurnOfFinalStop(uint32_t *updateVal);
static void     startSourcesLocal(icSourceConfig  *sourceConfig, uint32_t sourceId);
static uint32_t getSourcePluginId(void *plg);
static uint32_t getIspPluginId(void *plg);
static void     app3CamIspStartSrc(uint32_t sourceInstance, icSourceConfig  *sourceConfig);
static void     app3CamIspStopSrc (uint32_t sourceInstance) ;
static void     cbEofSourceEvent(void *plg, FrameT *frame);
static void     cbSofSourceEvent(void *plg, FrameT *frame);
static void     cbConfigIsp    (uint32_t ispInstance, void *iconf);
static void     cbStartIspEvent(void *plg, uint32_t seqNr, void *userData);
static void     cbEndIspEvent  (void *plg, uint32_t seqNr, void *userData);
static void     cbOutput(FrameT *frame, void *pluginObj);

static void     cbErrorIspEvent(void* plg, icSeverity severity, icError errorNo, void *userData);
/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/

void app3CamIspCbIcSetup(icCtrl *ctrl) {
    uint32_t i;
    uint32_t shv;
    OpipeReset(); //general inits

    memset((void*)startSrcState, 0, sizeof(startSrcState));
    memset((void*)stopSrcState, 0, sizeof(stopSrcState));
    memset((void*)startSrcLocConfig, 0, sizeof(startSrcLocConfig));
    memset((void*)nextCfg, 0, sizeof(nextCfg));
    tearDownEnable = 0;
    previewAbleMode = 1;
    gServerInfo.cbDataWasSent   = NULL;

    // create plug-ins
    PlgFifoCreate   ((void*)&plgFifo);
    PlgFifoConfig   ((void*)&plgFifo, (uint32_t)MAX_NR_OF_CAMS);
    plgFifo.plg.init(frameMgrPoolFifo, 3, (void*)&plgFifo);

    icSize frameSizeIn = {IMG_W_IN_WARP, IMG_H_IN_WARP};
    icSize frameSizeOut  = {IMG_W_OUT_WARP, IMG_H_OUT_WARP};

    for(i = 0; i < MAX_NR_OF_CAMS; i++) {
        gServerInfo.sourceServerCtrl[i].cbStartSource  = NULL;
        gServerInfo.sourceServerCtrl[i].cbStopSource   = NULL;
        gServerInfo.pluginServerCtrl[i].cbConfigPlugin = NULL;
        PlgSourceCreate ((void*)&plgSource[i], i);
        PlgIspFullCreate((void*)&plgVdo1[i]);
        PlgWarpCreate   ((void*)&plgWarp[i]);
        PlgWarpConfig(&plgWarp[i], 
                        frameSizeIn, frameSizeOut,
                        &init_mesh_Y0, &init_mesh_Y1, &init_mesh_UV,
                        0x80 , i);

        plgSource[0].plg.init(&frameMgrPoolC[i],     1, (void*)&plgSource[i]);
        plgVdo1[0].plg.init  (&frameMgrPoolVideo[i], 1, (void*)&plgVdo1[i]);
        plgWarp[0].plg.init (&frameMgrPoolWarp[i],   1, (void*)&plgWarp[i]);
        // source that have associated trigger capture capability
        gServerInfo.sourceServerCtrl[i].pool = plgSource[i].outputPools;
        frameMgrFrameC[i]       = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_SOURCE);
        frameMgrFrameVideo[i]   = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_ISP_OUT);
        frameMgrFrameWarp[i]    = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_WARP_OUT);
        // init callback output, this pluginObj not refer this time to output plugin, refere to source
        // plugin, in order to identify the source
        cbOutputList[i  ].callback  = cbOutput;
        cbOutputList[i  ].pluginObj = &plgWarp[i];

        // Source output pool
        FrameMgrCreatePool(&frameMgrPoolC[i], frameMgrFrameC[i], &plgFifo.plg.callbacks[i], 1);
        FrameMgrCreatePool(&frameMgrPoolFifo[i],  NULL,    plgVdo1[i].plg.callbacks, 1);
        FrameMgrCreatePool(&frameMgrPoolVideo[i], frameMgrFrameVideo[i], &plgWarp[i].plg.callbacks[0],         1);
        FrameMgrCreatePool(&frameMgrPoolWarp[i],  frameMgrFrameWarp[i],  &cbOutputList[i], 1);

        // create descriptions for available functionality regarding isp. Los isp side, base on this
        // informations, will properly update parameters and config sensors.
        ipServerRegSourceQuery(i,
                "Source",
                IC_SOURCE_ATTR_HAS_VIDEO_ISP | 
                IC_SOURCE_ATTR_HAS_VIDEO_OUT ,
                NR_OF_BUFFERS_PER_SOURCE,
                i, i + MAX_NR_OF_CAMS*1, i + MAX_NR_OF_CAMS*2, 0);

        ipServerRegIspQuery(i      , "IspVdo",     IC_ISP_ATTR_VIDEO_LINK, i);
        ipServerRegOutputQuery(i   , "Out",        IC_OUTPUT_FRAME_DATA_TYPE_PREVIEW  , i);

        ipServerQueryAddChild(ctrl->icPipelineDescription.icQuerySource[i], ctrl->icPipelineDescription.icQueryIsp[i]);
        ipServerQueryAddChild(ctrl->icPipelineDescription.icQueryIsp[i], ctrl->icPipelineDescription.icQueryOutput[i]);
    };
}

//
void app3CamIspCbIcTearDown(void) {
    turnOfapp3Cam();
    tearDownEnable = 1;
}

int sippOpipeResurcesFree(void) {
    uint32_t ret = 0;
    uint32_t x;
    for (x = 0; x < MAX_NR_OF_CAMS; x++) {
        ret = ret | plgVdo1[x].plg.status;
    }
    return(0 == ret);
}

int cnt0=0;
int cnt1=0;
int cnt2=0;
int cnt3=0;
//
void app3CamIspMain(void) {
    uint32_t x, i;
    if(tearDownEnable) {
        if(checkTurnOfFinalStop(&tearDownEnable)) {
            ipServerWasTornDown();
            exit(0);
        }
    }

    for (i = 0; i < MAX_NR_OF_CAMS; i++) 
    {
        if(plgWarp[i].meshGenEnd == 1)
        {
            cnt0++;
            plgWarp[i].meshGenEnd = 0;
            plgWarp[i].triggerStartWarp(&plgWarp[i]);
        }

        if(plgWarp[i].chrEOF == 1)
        {
            cnt1++;
            plgWarp[i].chrEOF = 0;
            plgWarp[i].triggerChroma(&plgWarp[i]);
        }

        if(plgWarp[i].lumaEOF == 1)
        {
            cnt2++;
            plgWarp[i].lumaEOF = 0;
            plgWarp[i].triggerLuma(&plgWarp[i]);
        }

        if((PLG_STATS_IDLE == plgWarp[i].plg.status) && (plgWarp[i].runEnFlag > 0)) {
            cnt3++;
            plgWarp[i].runEnFlag--;
            plgWarp[i].triger(&plgWarp[i]);
        }
    }

    // trigger just if opipe is idle
    if (sippOpipeResurcesFree()) {
        for (x = 0; x < MAX_NR_OF_CAMS; x++) {
            // Start Source Command in order to avoid big interrupt time
            if (1 == startSrcState[x]) {
                startSourcesLocal(startSrcLocConfig[x], x);
                startSrcState[x] = 0;
            }
            // Stop Source Command in order to avoid big interrupt time
            if (1 == stopSrcState[x]) {
                if(plgSource[x].plg.fini)
                    plgSource[x].plg.fini(&plgSource[x]);
                if(plgVdo1[x].plg.fini)
                    plgVdo1[x].plg.fini(&plgVdo1[x]);
                if(plgWarp[x].plg.fini) plgWarp[x].plg.fini(&plgWarp[x]);
                while ( (PLG_STATS_RUNNING == plgSource[x].plg.status) ||
                        (PLG_STATS_RUNNING == plgVdo1[x].plg.status)) {
                    NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;
                    NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;
                }
                stopSrcState[x] = 0;
                ipServerSourceStopped(x);
            }
        }

        plgFifo.triger((void*)&plgFifo);

        NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;
    }
}

//
icStatusCode app3CamIspSrcComit(icCtrl *ctrl) {
    int32_t x;

    /*ALLOC*/ AllocOpipeReset(); //clear prev alloc
    /*ALLOC*/ AllocOpipeRxCmxBuffs (ctrl);
    /*ALLOC*/
    /*ALLOC*/ PlgIspBase *ispBase[MAX_NR_OF_CAMS]; //Isp buffs
    /*ALLOC*/ for(x=0; x<MAX_NR_OF_CAMS; x++)
        /*ALLOC*/   ispBase[x] = &plgVdo1[x].base;
    /*ALLOC*/ AllocOpipeIspCmxBuffs(ctrl, MAX_NR_OF_CAMS, ispBase);

    // Allocate frames buffers memory
    for (x = 0; x < MAX_NR_OF_CAMS; x++) {
        if(IPIPE_SRC_SETUP == ctrl->source[x].sourceStatus) {
            //size.w = (inSz.w * hN - 1)/hD + 1;
            //size.h = (inSz.h * vN - 1)/vD + 1;
            uint32_t maxIspW = ((ctrl->source[x].sourceSetup.maxWidth *
                    ctrl->source[x].sourceSetup.maxHorizN - 1) /
                    ctrl->source[x].sourceSetup.maxHorizD + 1);
            uint32_t maxIspH = ((ctrl->source[x].sourceSetup.maxHeight *
                    ctrl->source[x].sourceSetup.maxVertN - 1) /
                    ctrl->source[x].sourceSetup.maxVertD + 1);
            // Allocate smaller ddr as video output for cam0 is quarter from the input resolution
            uint32_t videoFrameSize = (maxIspW * maxIspH);
            if(QUARTER_MODE_FOR_VIDEO_PIPE == ctrl->source[x].sourceSetup.appSpecificInfo) {
                videoFrameSize = videoFrameSize>>2;
                previewAbleMode = 1;
            }
            ipServerFrameMgrAddBuffs(frameMgrFrameVideo[x],videoFrameSize,videoFrameSize>>1, 0);
            // this functionality is available just now !!! Important to do that
            gServerInfo.sourceServerCtrl[x].cbStartSource = app3CamIspStartSrc;
            gServerInfo.sourceServerCtrl[x].cbStopSource = app3CamIspStopSrc;
            gServerInfo.pluginServerCtrl[x].cbConfigPlugin = cbConfigIsp;

            ipServerFrameMgrAddBuffs(frameMgrFrameWarp[x],
                    IMG_W_OUT_WARP *  (IMG_H_OUT_WARP), // div by 8 size in bytes
                    (IMG_W_OUT_WARP *  IMG_H_OUT_WARP)>>1, 0);

        }
    }

    gServerInfo.cbDataWasSent   = FrameMgrReleaseFrame; // !!! different approach here


    return IC_STATS_SUCCESS;
}




/**************************************************************************************************
 ~~~ Local Functions Implementation
 **************************************************************************************************/

// Output callback linked to plug-ins
static void cbOutput(FrameT *frame, void *pluginObj) {
    //uint32_t ispInstance = getIspPluginId(pluginObj);
    uint32_t ispInstance = 0;
    ipServerSendData(frame, ispInstance);
}

static void cbSofSourceEvent(void *plg, FrameT *frame) {
    uint32_t idx = getSourcePluginId(plg);
    if(frame) {
        if(nextCfg[idx]) {
            frame->appSpecificData = nextCfg[idx];
            nextCfg[idx] = NULL;
            ipServerReadoutStart((icSourceInstance)idx,
                    ((icIspConfig*)(frame->appSpecificData))->userData,
                    frame->seqNo, frame->timestamp[0]);
        }
        else {
            frame->appSpecificData = NULL;
            //ipServerReadoutStart((icSourceInstance)idx,  NULL, frame->seqNo, frame->timestamp[0]);
        }
    }
    else {
        //ipServerReadoutStart((icSourceInstance)idx, NULL, 0, 0);
    }
}
static void cbEofSourceEvent(void *plg, FrameT *frame) {
    uint32_t idx = getSourcePluginId(plg);
    if(frame) {
        if(frame->appSpecificData) {
            if (IC_PIPECTL_ZSL_LOCK & (((icIspConfig *)(frame->appSpecificData))->pipeControl)) {
                //            if (IC_PIPECTL_ZSL_LOCK &
                //                    (swcLeonReadNoCacheU32((uint32_t)&(((icIspConfig *)nextCfg[idx])->pipeControl)))) {
                FrameMgrIncreaseNrOfConsumer(frame, 1);
                ipServerFrameLocked((icSourceInstance)idx, frame);
            }
            ipServerReadoutEnd((icSourceInstance)idx,
                    ((icIspConfig*)(frame->appSpecificData))->userData,
                    frame->seqNo, frame->timestamp[0]);
        }
        else {
            //ipServerReadoutEnd((icSourceInstance)idx, 0, frame->seqNo, frame->timestamp[0]);
        }
    }
    else {
        //ipServerReadoutEnd((icSourceInstance)idx, NULL, 0, 0);
    }
}

static void cbStartIspEvent(void *plg, uint32_t seqNr, void *userData) {
    uint32_t ispInstance = getIspPluginId(plg);
    ipServerIspStart(ispInstance, seqNr, userData);
}
static void cbEndIspEvent(void *plg, uint32_t seqNr, void *userData) {
    uint32_t ispInstance = getIspPluginId(plg);
    ipServerIspEnd(ispInstance, seqNr, userData);
}

// Error reporting callbacks
void cbErrorIspEvent(void *plg, icSeverity severity, icError errorNo, void *userData) {
    uint32_t ispInstance = getIspPluginId(plg);
    ipServerIspReportError(ispInstance, severity, errorNo, userData);
}

static void cbConfigIsp(uint32_t ispInstance, void *iconf) {
    nextCfg[ispInstance] = iconf;
}

static void startSourcesLocal(icSourceConfig  *sourceConfig, uint32_t sourceId) {
    icSize       iSize;
    icSize       oVdoSize;
    icSize frameSizeOutWarp = {IMG_W_OUT_WARP, IMG_H_OUT_WARP};
    iSize.w  = sourceConfig->cropWindow.x2 - sourceConfig->cropWindow.x1;
    iSize.h  = sourceConfig->cropWindow.y2 - sourceConfig->cropWindow.y1;
    oVdoSize = iSize;
    if(QUARTER_MODE_FOR_VIDEO_PIPE == pSrvIcCtrl->source[sourceId].sourceSetup.appSpecificInfo) {
        oVdoSize.w = iSize.w>>1;
        oVdoSize.h = iSize.h>>1;
    }
    FrmMgrUtilsInitList(frameMgrFrameC[sourceId], iSize,
            FrmMgrUtilsGetRawFrm(sourceConfig->bitsPerPixel, (sourceConfig->mipiRxData.recNrl <= IC_SIPP_DEVICE3 ? 1 : 0)));

    FrmMgrUtilsInitList(frameMgrFrameVideo[sourceId], oVdoSize, FRAME_T_FORMAT_YUV420);
    FrmMgrUtilsInitList(frameMgrFrameWarp[sourceId],  frameSizeOutWarp, FRAME_T_FORMAT_YUV420);

    uint32_t sourceFmt = SIPP_FMT_16BIT; //CIF default
    if(sourceConfig->mipiRxData.recNrl <= IC_SIPP_DEVICE3)
        sourceFmt = GetFrameBppPackFormat(sourceConfig->bitsPerPixel);
    PlgIspFullConfig(&plgVdo1 [sourceId], iSize, sourceFmt, previewAbleMode);

    //Pipes to clear used resources at EOF
    plgVdo1 [sourceId].op.p.flags |= CLEAN_EXIT;

    if(QUARTER_MODE_FOR_VIDEO_PIPE == pSrvIcCtrl->source[sourceId].sourceSetup.appSpecificInfo)
        plgVdo1[sourceId].op.p.flags |=  GEN_PREVIEW;
    plgSource[sourceId].eofEvent        = cbEofSourceEvent;
    plgSource[sourceId].sofEvent        = cbSofSourceEvent;
    plgVdo1[sourceId].procesStart       = cbStartIspEvent;
    plgVdo1[sourceId].procesEnd         = cbEndIspEvent;
    plgVdo1[sourceId].procesIspError    = cbErrorIspEvent;
    PlgSourceStart(&plgSource[sourceId], sourceConfig, sourceFmt);
    ipServerSourceReady(sourceId);
}

static void app3CamIspStartSrc(uint32_t sourceInstance, icSourceConfig  *sourceConfig) {
    startSrcState[sourceInstance] = 1;
    startSrcLocConfig[sourceInstance] = sourceConfig;
}
static void app3CamIspStopSrc(uint32_t sourceInstance) {
    stopSrcState[sourceInstance] = 1;
}

static void turnOfapp3Cam(void) {
    uint32_t i;
    for(i = 0; i < MAX_NR_OF_CAMS; i++) {
        if(plgVdo1[i].plg.fini)   plgVdo1[i].plg.fini(&plgVdo1[i]);
        if(plgSource[i].plg.fini) plgSource[i].plg.fini(&plgSource[i]);
        if(plgWarp[i].plg.fini) plgWarp[i].plg.fini(&plgWarp[i]);
        gServerInfo.sourceServerCtrl[i].cbStartSource  = NULL;
        gServerInfo.sourceServerCtrl[i].cbStopSource   = NULL;
        gServerInfo.pluginServerCtrl[i].cbConfigPlugin = NULL;
    }
    if(plgFifo.plg.fini) plgFifo.plg.fini(&plgFifo);
    gServerInfo.cbDataWasSent   = NULL;
}
static uint32_t checkTurnOfFinalStop(uint32_t *updateVal) {
    if(     (0 == plgSource[0].plg.status) &&
            (0 == plgSource[1].plg.status) &&
            (0 == plgSource[2].plg.status) &&
            (0 == plgVdo1[0].plg.status)   &&
            (0 == plgVdo1[1].plg.status)   &&
            (0 == plgVdo1[2].plg.status)   &&
            (0 == plgFifo.plg.status)         ) {
        MemMgrReset();
        *updateVal = 0;
        return 1;
    }
    return 0;
}

static uint32_t getSourcePluginId(void *plg) {
    uint32_t i;
    for(i = 0; i < MAX_NR_OF_CAMS; i++)
        if(plg == &plgSource[i]) return i;
    assert(0); return 0;
}

static uint32_t getIspPluginId(void *plg) {
    uint32_t i;
    for(i = 0; i < MAX_NR_OF_CAMS; i++)
        if(plg == &plgVdo1[i]) return i;
    assert(0); return 0;
}


