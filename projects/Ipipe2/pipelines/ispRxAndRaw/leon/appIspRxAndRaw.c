#include <string.h>
#include <assert.h>
#include "ipipe.h"
#include "IpipeServerApi.h"
#include "ipipeOpipeUtils.h"
#include "FrameMgrUtils.h"
#include "FrameMgrApi.h"
#include "PlgSrcIspApi.h"
#include "PlgSourceApi.h"
#include "appIspRxAndRaw.h"

#define MAX_NR_OF_CAMS             2
#define NR_OF_BUFFERS_PER_SOURCE   4

#define ISP_RX_OUT_ID  0
#define RAW_OUT_ID     1


//in cdmaDescriptor section that bypasses caches !!!
static PlgSrcIsp   plgSrcIsp SECTION(".cmx.cdmaDescriptors") ALIGNED(8);
static PlgSource   plgSource SECTION(".cmx.cdmaDescriptors") ALIGNED(8);


static FramePool   frameMgrPoolIsp;
static FramePool   frameMgrPoolRaw;

static FrameT     *frameMgrFrameIsp;
static FrameT     *frameMgrFrameRaw;

static uint32_t        startSrcState[MAX_NR_OF_CAMS];
static uint32_t        stopSrcState[MAX_NR_OF_CAMS];
static icSourceConfig  *startSrcLocConfig[MAX_NR_OF_CAMS];
static uint32_t        tearDownEnable;
static void            *nextCfg;

static FrameProducedCB cbOutputIsp;
static FrameProducedCB cbOutputRaw;

static uint32_t getSourcePluginId(void *plg) {if(plg == &plgSrcIsp) return ISP_RX_OUT_ID; else return RAW_OUT_ID;}
static void     startSourcesLocal(icSourceConfig  *sourceConfig, uint32_t sourceId);
static void     appRxIspStartSrc(uint32_t sourceInstance, icSourceConfig  *sourceConfig);
static void     appRxIspStopSrc (uint32_t sourceInstance) ;
static void     cbEofSourceEvent(void *plg, FrameT *frame);
static void     cbSofSourceEvent(void *plg, FrameT *frame);
static void     cbConfigIsp    (uint32_t ispInstance, void *iconf);

// Output callback linked to plug-ins
static inline void outputIsp(FrameT *frame, void *pluginObj) { UNUSED(pluginObj); ipServerSendData(frame, ISP_RX_OUT_ID);}
static inline void outputRaw(FrameT *frame, void *pluginObj) { UNUSED(pluginObj); ipServerSendData(frame, RAW_OUT_ID);}




//#################################################################################################
void appRxIspCbIcSetup(icCtrl *ctrl)
{
    OpipeReset(); //general inits

    memset((void*)startSrcState,     0, sizeof(startSrcState));
    memset((void*)stopSrcState,      0, sizeof(stopSrcState));
    memset((void*)startSrcLocConfig, 0, sizeof(startSrcLocConfig));
    nextCfg                   = NULL;
    tearDownEnable            = 0;
    gServerInfo.cbDataWasSent = NULL;

    {// create plug-ins IspRx
        uint32_t i = ISP_RX_OUT_ID;
        gServerInfo.sourceServerCtrl[i].cbStartSource  = NULL;
        gServerInfo.sourceServerCtrl[i].cbStopSource   = NULL;
        gServerInfo.pluginServerCtrl[i].cbConfigPlugin = NULL;

        PlgSrcIspCreate ((void*)&plgSrcIsp, i);
        plgSrcIsp.plg.init(&frameMgrPoolIsp, 1, (void*)&plgSrcIsp);

        gServerInfo.sourceServerCtrl[i].pool = plgSrcIsp.outputPools;
        frameMgrFrameIsp = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_SOURCE);

        // init callback output, this pluginObj not refer this time to output plugin, refere to source
        // plugin, in order to identify the source
        cbOutputIsp.callback  = outputIsp;
        cbOutputIsp.pluginObj = &plgSrcIsp;

        // SOURCE->OUTPUT link
        FrameMgrCreatePool(&frameMgrPoolIsp, frameMgrFrameIsp, &cbOutputIsp, 1);

        // create descriptions for available functionality regarding isp. Los isp side, base on this
        // informations, will properly update parameters and config sensors.
        ipServerRegSourceQuery(i,
                "Source",
                IC_SOURCE_ATTR_HAS_VIDEO_ISP |
                IC_SOURCE_ATTR_HAS_VIDEO_OUT,
                NR_OF_BUFFERS_PER_SOURCE,
                i, 0, 0, 0);

        ipServerRegIspQuery   (i , "IspVdo", IC_ISP_ATTR_VIDEO_LINK, i);
        ipServerRegOutputQuery(i , "Out",    IC_OUTPUT_FRAME_DATA_TYPE_PREVIEW  , i); // preview cam output

        ipServerQueryAddChild(ctrl->icPipelineDescription.icQuerySource[i], ctrl->icPipelineDescription.icQueryIsp[i]);
        ipServerQueryAddChild(ctrl->icPipelineDescription.icQueryIsp   [i], ctrl->icPipelineDescription.icQueryOutput[i]);
    };

    {// create plug-ins IspRx
        uint32_t i = RAW_OUT_ID;
        gServerInfo.sourceServerCtrl[i].cbStartSource  = NULL;
        gServerInfo.sourceServerCtrl[i].cbStopSource   = NULL;
        gServerInfo.pluginServerCtrl[i].cbConfigPlugin = NULL;

        PlgSourceCreate ((void*)&plgSource, i);
        plgSource.plg.init(&frameMgrPoolRaw, 1, (void*)&plgSource);

        gServerInfo.sourceServerCtrl[i].pool = plgSource.outputPools;
        frameMgrFrameRaw = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_SOURCE);

        // init callback output, this pluginObj not refer this time to output plugin, refere to source
        // plugin, in order to identify the source
        cbOutputRaw.callback  = outputRaw;
        cbOutputRaw.pluginObj = &plgSource;

        // SOURCE->OUTPUT link
        FrameMgrCreatePool(&frameMgrPoolRaw, frameMgrFrameRaw, &cbOutputRaw, 1);

        // create descriptions for available functionality regarding isp. Los isp side, base on this
        // informations, will properly update parameters and config sensors.
        ipServerRegSourceQuery(i,
                "Source",
                IC_SOURCE_ATTR_HAS_VIDEO_OUT,
                NR_OF_BUFFERS_PER_SOURCE,
                i, 0, 0, 0);
        ipServerRegOutputQuery(i , "Out",    IC_OUTPUT_FRAME_DATA_TYPE_STILL_RAW  , i); // preview cam output
        ipServerQueryAddChild(ctrl->icPipelineDescription.icQuerySource[i], ctrl->icPipelineDescription.icQueryOutput[i]);
    };
}

void appRxIspCbIcTearDown(void) {
    //turnOfappRx();
    tearDownEnable = 1;
}

int sippOpipeResurcesFree(void) {
    return 1;
}

//#################################################################################################
void appRxIspMain()
{
    if(tearDownEnable) {
        ipServerWasTornDown();
        exit(0);
    }
    //trigger just if opipe is idle,
    if (sippOpipeResurcesFree()) {
        // IspRx Start Source Command in order to avoid big interrupt time
        if (1 == startSrcState[ISP_RX_OUT_ID]) {
            startSourcesLocal(startSrcLocConfig[ISP_RX_OUT_ID], ISP_RX_OUT_ID);
            startSrcState[ISP_RX_OUT_ID] = 0;
        }
        // Raw Start Source Command in order to avoid big interrupt time
        if (1 == startSrcState[RAW_OUT_ID]) {
            startSourcesLocal(startSrcLocConfig[RAW_OUT_ID], RAW_OUT_ID);
            startSrcState[RAW_OUT_ID] = 0;
        }
    }
}

//#################################################################################################
icStatusCode appRxIspSrcComit(icCtrl *ctrl) {
    int32_t x;

    //Before this CB is called, full-DDR SOURCE buffers are allocated !!!

    //Ajust UV plane pointers within these buffs
    FrameT *cur = &frameMgrFrameIsp[0];
    do{
        /*DBG*/ //printf("fbPtr = 0x%lx\n", (uint32_t)cur->fbPtr[0]);
        cur->fbPtr[1] = cur->fbPtr[0] + 4192*3120; //TBD: do this properly
        cur = cur->next;
    }while(cur);

    /*ALLOC*/ AllocOpipeReset(); //clear prev alloc
    /*ALLOC*/ PlgSrcIspCmxAlloc(&plgSrcIsp, &ctrl->source[0].sourceSetup, CheckAllocOmemPool());
    /*DBG*/ //printf("Free = %ld\n", MemMgrGetFreeMem(CheckAllocOmemPool()));

    if(IPIPE_SRC_SETUP == ctrl->source[ISP_RX_OUT_ID].sourceStatus) {
        gServerInfo.sourceServerCtrl[ISP_RX_OUT_ID].cbStartSource  = appRxIspStartSrc;
        gServerInfo.sourceServerCtrl[ISP_RX_OUT_ID].cbStopSource   = appRxIspStopSrc;
        gServerInfo.pluginServerCtrl[ISP_RX_OUT_ID].cbConfigPlugin = cbConfigIsp;
    }
    if(IPIPE_SRC_SETUP == ctrl->source[RAW_OUT_ID].sourceStatus) {
        gServerInfo.sourceServerCtrl[RAW_OUT_ID].cbStartSource  = appRxIspStartSrc;
        gServerInfo.sourceServerCtrl[RAW_OUT_ID].cbStopSource   = appRxIspStopSrc;
        gServerInfo.pluginServerCtrl[RAW_OUT_ID].cbConfigPlugin = cbConfigIsp;
    }
    gServerInfo.cbDataWasSent = FrameMgrReleaseFrame;
    return IC_STATS_SUCCESS;
}

//#################################################################################################
// Local Functions Implementation



static void cbSofSourceEvent(void *plg, FrameT *frame) {
    uint32_t idx = getSourcePluginId(plg);
    if(frame) {
        if(nextCfg) {
            frame->appSpecificData = nextCfg;
            nextCfg = NULL;
            ipServerReadoutStart((icSourceInstance)idx,
                    ((icIspConfig*)(frame->appSpecificData))->userData,
                    frame->seqNo, frame->timestamp[0]);
        }
        else {
            frame->appSpecificData = NULL;
            //ipServerReadoutStart((icSourceInstance)idx,  NULL, frame->seqNo, frame->timestamp[0]);
        }
    }
}

static void cbEofSourceEvent(void *plg, FrameT *frame) {
    uint32_t idx = getSourcePluginId(plg);
    if(frame) {
        if(frame->appSpecificData) {
            ipServerReadoutEnd((icSourceInstance)idx,
                    ((icIspConfig*)(frame->appSpecificData))->userData,
                    frame->seqNo, frame->timestamp[0]);
        }
    }
    else{ //send READOUT_END to LOS even when frames get skipped,
        //as that triggers test code to send new configs
        ipServerReadoutEnd((icSourceInstance)idx, 0, 0, 0);
    }
}

static void cbConfigIsp(uint32_t ispInstance, void *iconf) {
    UNUSED(ispInstance);
    nextCfg             = iconf;
    plgSrcIsp.nxtIspCfg = nextCfg;

    //===============================================================
    //Patches that are NOT required for metal-fixed ma2150 silicon:
    extern uint16_t cmxGammaLut[512*4];
    plgSrcIsp.nxtIspCfg->gamma.table  = cmxGammaLut; //for bug 22777
    plgSrcIsp.nxtIspCfg->dog.strength = 0; //LTM-Only for better performance on ma2150
    //===============================================================
}

static void startSourcesLocal(icSourceConfig  *sourceConfig, uint32_t sourceId) {
    icSize   iSize;
    iSize.w  = sourceConfig->cropWindow.x2 - sourceConfig->cropWindow.x1;
    iSize.h  = sourceConfig->cropWindow.y2 - sourceConfig->cropWindow.y1;

    if(ISP_RX_OUT_ID == sourceId) {
        //new SrcIsp outputs YUV420 !
        FrmMgrUtilsInitList(frameMgrFrameIsp, iSize, FRAME_T_FORMAT_YUV420);

        plgSrcIsp.eofEvent = cbEofSourceEvent;
        plgSrcIsp.sofEvent = cbSofSourceEvent;
        PlgSrcIspStart(&plgSrcIsp, sourceConfig);
    }

    if(RAW_OUT_ID == sourceId) {
        //new SrcIsp outputs YUV420 !
        FrmMgrUtilsInitList(frameMgrFrameRaw, iSize,
                FrmMgrUtilsGetRawFrm(sourceConfig->bitsPerPixel,
                        (sourceConfig->mipiRxData.recNrl <= IC_SIPP_DEVICE3 ? 1 : 0)));
        plgSource.eofEvent = cbEofSourceEvent;
        plgSource.sofEvent = cbSofSourceEvent;
        PlgSourceStart(&plgSource, sourceConfig, SIPP_FMT_16BIT);
    }
    ipServerSourceReady(sourceId);
}

static void appRxIspStartSrc(uint32_t sourceInstance, icSourceConfig  *sourceConfig) {
    startSrcState[sourceInstance] = 1;
    startSrcLocConfig[sourceInstance] = sourceConfig;
}
static void appRxIspStopSrc(uint32_t sourceInstance) {
    stopSrcState[sourceInstance] = 1;
}
