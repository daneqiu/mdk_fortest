
/**************************************************************************************************

 @File         : main_rt.c
 @Author       : Florin Cotoranu
 @Brief        : Contains LRT code starting point
 Date          : March 29th 2016
 E-mail        : florin.cotoranu@movidius.com
 Copyright     : � Movidius Srl 2016, � Movidius Ltd 2016
 Description   : LRT code starting point for isp4xVdoMonoLuma pipeline

 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include <stdio.h>

#include "app4CamIspMonoLuma.h"
#include "DrvShaveL2Cache.h"
#include "DrvLeonL2C.h"
#include "swcLeonUtils.h"
#include "ipipe.h"
#include "IpipeServerApi.h"
#include "ipipeMsgQueue.h"


/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Local File function declarations
 **************************************************************************************************/


/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
int main(void)
{
    DrvLL2CDisable(LL2C_OPERATION_INVALIDATE);
    DrvLL2CInitWriteThrough();

    gServerInfo.cbIcSetup       = isp4xMonoLumaCbIcSetup;
    gServerInfo.cbIcTearDown    = isp4xMonoLumaCbIcTearDown;
    gServerInfo.cbSourcesCommit = isp4xMonoLumaSrcComit;

    setupIpipeServer();

    while (1) {
       isp4xMonoLumaMain();
    }
    return 0;
}

/**************************************************************************************************
 ~~~ Local Functions Implementation
 **************************************************************************************************/
