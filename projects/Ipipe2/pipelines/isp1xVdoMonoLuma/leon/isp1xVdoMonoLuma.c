/**************************************************************************************************

 @File         : isp1xVdoMonoLuma.c
 @Author       : Florin Cotoranu
 @Brief        : Contains 1 Mono Luma ISP plugin
 Date          : March 29th 2016
 E-mail        : florin.cotoranu@movidius.com
 Copyright     : � Movidius Srl 2016, � Movidius Ltd 2016
 Description   : Receive and process image from one single camera; type of sensor is
                 selected at application level, on LOS

 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include "isp1xVdoMonoLuma.h"

#include <string.h>
#include <stdio.h>
#include <assert.h>
#include "DrvLeonL2C.h"
#include "ipipe.h"
#include "ipipeDbg.h"
#include "FrameMgrUtils.h"
#include "ipipeUtils.h"
#include "ipipeOpipeUtils.h"
#include "FrameMgrApi.h"
#include "IpipeServerApi.h"
#include "ipipeMsgQueue.h"
#include "PlgSourceApi.h"
#include "PlgFifoApi.h"
#include "PlgIspMonoLumaApi.h"
#include <VcsHooksApi.h>

/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/
#define NR_OF_BUFFERS_PER_SOURCE  3
#define NR_OF_BUFFERS_PER_ISP_OUT 4

/**************************************************************************************************
 ~~~  Local variables
 **************************************************************************************************/

static PlgSource       plgSource  SECTION(".cmx.cdmaDescriptors") ALIGNED(8);
static PlgIspMonoLuma      plgVdo1    SECTION(".cmx.cdmaDescriptors") ALIGNED(8);
static PlgFifo         plgFifo;

static FramePool       frameMgrPoolC;
static FramePool       frameMgrPoolFifo;
static FramePool       frameMgrPoolP;

static FrameT          *frameMgrFrameC;
static FrameT          *frameMgrFrameP;

static uint32_t        startSrcState;
static uint32_t        stopSrcState;
static icSourceConfig  *startSrcLocConfig;
static uint32_t        tearDownEnable = 0;
static void            *nextCfg;
static FrameProducedCB cbOutputList;

uint32_t width;
/**************************************************************************************************
 ~~~ Local File function declarations
 **************************************************************************************************/
static void     isp1xMonoLumaTurnOff(void);
static uint32_t checkTurnOfFinalStop(uint32_t *updateVal);
static void     startSourcesLocal(icSourceConfig  *sourceConfig, uint32_t sourceId);
static uint32_t getIspPluginId(void *plg);
static void     isp1xMonoLumaStartSrc(uint32_t sourceInstance, icSourceConfig  *sourceConfig);
static void     isp1xMonoLumaStopSrc (uint32_t sourceInstance) ;
static void     cbEofSourceEvent(void *plg, FrameT *frame);
static void     cbSofSourceEvent(void *plg, FrameT *frame);
static void     cbConfigIsp    (uint32_t ispInstance, void *iconf);
static void     cbStartIspEvent(void *plg, uint32_t seqNr, void *userData);
static void     cbEndIspEvent  (void *plg, uint32_t seqNr, void *userData);
static void     cbOutput(FrameT *frame, void *pluginObj);


/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
void isp1xMonoLumaCbIcSetup(icCtrl *ctrl) {
   UNUSED(ctrl);
   OpipeReset(); //general inits
   memset((void*)startSrcState, 0, sizeof(startSrcState));
   memset((void*)stopSrcState, 0, sizeof(stopSrcState));
   memset((void*)startSrcLocConfig, 0, sizeof(startSrcLocConfig));
   memset((void*)nextCfg, 0, sizeof(nextCfg));
   tearDownEnable = 0;
   gServerInfo.cbDataWasSent   = NULL;
   PlgFifoCreate       ((void*)&plgFifo);
   PlgFifoConfig       ((void*)&plgFifo, 1);
   plgFifo.plg.init(&frameMgrPoolFifo, 1, (void*)&plgFifo);
   gServerInfo.sourceServerCtrl[0].cbStartSource  = NULL;
   gServerInfo.sourceServerCtrl[0].cbStopSource   = NULL;
   gServerInfo.pluginServerCtrl[0].cbConfigPlugin = NULL;
   PlgSourceCreate ((void*)&plgSource, 0);
   PlgIspMonoLumaCreate((void*)&plgVdo1);

   plgSource.plg.init(&frameMgrPoolC, 1, (void*)&plgSource);
   plgVdo1.plg.init(&frameMgrPoolP, 1, (void*)&plgVdo1);

   gServerInfo.sourceServerCtrl[0].pool = plgSource.outputPools;
   frameMgrFrameC = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_SOURCE);
   frameMgrFrameP = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_ISP_OUT);
   // Source output pool
   FrameMgrCreatePool(&frameMgrPoolC, frameMgrFrameC, plgFifo.plg.callbacks, 1);
   // Mux  output pool, special case for serialization plug-in, no frame inside, as it pass the input data out
   FrameMgrCreatePool(&frameMgrPoolFifo, NULL, plgVdo1.plg.callbacks, 1);

   cbOutputList.callback  = cbOutput;
   cbOutputList.pluginObj = &plgVdo1;
   FrameMgrCreatePool(&frameMgrPoolP, frameMgrFrameP,  &cbOutputList, 1);

   ipServerRegSourceQuery(0,
         "Source",
         IC_SOURCE_ATTR_HAS_VIDEO_ISP |
         IC_SOURCE_ATTR_HAS_VIDEO_OUT ,
         NR_OF_BUFFERS_PER_SOURCE,
         0, 0, 0, 0);

   ipServerRegIspQuery(0, "IspVdo", IC_ISP_ATTR_VIDEO_LINK, 0);
   ipServerRegOutputQuery(0, "Out", IC_OUTPUT_FRAME_DATA_TYPE_PREVIEW  , 0);

   ipServerQueryAddChild(ctrl->icPipelineDescription.icQuerySource[0], ctrl->icPipelineDescription.icQueryIsp[0]);
   ipServerQueryAddChild(ctrl->icPipelineDescription.icQueryIsp[0], ctrl->icPipelineDescription.icQueryOutput[0]);
}

//
void isp1xMonoLumaCbIcTearDown(void) {
   isp1xMonoLumaTurnOff();
   tearDownEnable = 1;
}

//
void isp1xMonoLumaMain(void) {
   if(tearDownEnable) {
      if(checkTurnOfFinalStop(&tearDownEnable)) {
         ipServerWasTornDown();
         exit(0);
      }
   }

   // triger just if opipe is idle
   if (
         (0 == plgVdo1.plg.status)
   ) {
      // Start Source Command in order to avoid big interrupt time
      if (1 == startSrcState) {
         startSourcesLocal(startSrcLocConfig, 0);
         startSrcState = 0;
      }
      // Stop Source Command in order to avoid big interrupt time
      if (1 == stopSrcState)
      {
         if(plgSource.plg.fini)
            plgSource.plg.fini(&plgSource);
         if(plgVdo1.plg.fini)
            plgVdo1.plg.fini(&plgVdo1);
         while ( (PLG_STATS_RUNNING == plgSource.plg.status) ||
               (PLG_STATS_RUNNING == plgVdo1.plg.status)) {
            NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;
            NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;
         }
         stopSrcState = 0;
         ipServerSourceStopped(0);
      }
      plgFifo.triger((void*)&plgFifo);
      NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;
   }
}

//
icStatusCode isp1xMonoLumaSrcComit(icCtrl *ctrl) {
   /*ALLOC*/ AllocOpipeReset(); //clear prev alloc
   /*ALLOC*/ AllocOpipeRxCmxBuffs (ctrl);
   /*ALLOC*/
   /*ALLOC*/ PlgIspBase *ispBase; //Isp buffs
   /*ALLOC*/ ispBase = &plgVdo1.base;
   /*ALLOC*/ AllocOpipeIspCmxBuffs(ctrl, 1, &ispBase);

   // Allocate frames buffers memory
   if(IPIPE_SRC_SETUP == ctrl->source[0].sourceStatus)
   {
      uint32_t maxIspW = ((ctrl->source[0].sourceSetup.maxWidth *
            ctrl->source[0].sourceSetup.maxHorizN - 1) /
            ctrl->source[0].sourceSetup.maxHorizD + 1);
      uint32_t maxIspH = ((ctrl->source[0].sourceSetup.maxHeight *
            ctrl->source[0].sourceSetup.maxVertN - 1) /
            ctrl->source[0].sourceSetup.maxVertD + 1);
      uint32_t videoFrameSize = (maxIspW * maxIspH);
      ipServerFrameMgrAddBuffs(frameMgrFrameP,videoFrameSize,0, 0);
      // this functionality is available just now !!! Important to do that
      gServerInfo.sourceServerCtrl[0].cbStartSource = isp1xMonoLumaStartSrc;
      gServerInfo.sourceServerCtrl[0].cbStopSource = isp1xMonoLumaStopSrc;
      gServerInfo.pluginServerCtrl[0].cbConfigPlugin = cbConfigIsp;
   }

   gServerInfo.cbDataWasSent   = FrameMgrReleaseFrame; // !!! different approach here

   return IC_STATS_SUCCESS;
}




/**************************************************************************************************
 ~~~ Local Functions Implementation
 **************************************************************************************************/

// Output callback linked to plug-ins
static void cbOutput(FrameT *frame, void *pluginObj) {
   uint32_t ispInstance = getIspPluginId(pluginObj);
   ipServerSendData(frame, ispInstance);
}

static void cbSofSourceEvent(void *plg, FrameT *frame) {
   UNUSED(plg);
   uint32_t idx = 0;
   if(frame) {
      if(nextCfg) {
         frame->appSpecificData = nextCfg;
         nextCfg = NULL;
         ipServerReadoutStart((icSourceInstance)idx,
               ((icIspConfig*)(frame->appSpecificData))->userData,
               frame->seqNo, frame->timestamp[0]);
      }
      else {
         frame->appSpecificData = NULL;
         ipServerReadoutStart((icSourceInstance)idx,  NULL, frame->seqNo, frame->timestamp[0]);
      }
   }
   else {
      ipServerReadoutStart((icSourceInstance)idx, NULL, 0, 0);
   }
}

static void cbEofSourceEvent(void *plg, FrameT *frame) {
   UNUSED(plg);
   uint32_t idx = 0;
   if(frame) {
      if(frame->appSpecificData) {
         ipServerReadoutEnd((icSourceInstance)idx,
               ((icIspConfig*)(frame->appSpecificData))->userData,
               frame->seqNo, frame->timestamp[0]);
      }
      else {
         ipServerReadoutEnd((icSourceInstance)idx, 0, frame->seqNo, frame->timestamp[0]);
      }
   }
   else {
      ipServerReadoutEnd((icSourceInstance)idx, NULL, 0, 0);
   }
}

static void cbStartIspEvent(void *plg, uint32_t seqNr, void *userData) {
   uint32_t ispInstance = getIspPluginId(plg);
   ipServerIspStart(ispInstance, seqNr, userData);
}
static void cbEndIspEvent(void *plg, uint32_t seqNr, void *userData) {
   uint32_t ispInstance = getIspPluginId(plg);
   ipServerIspEnd(ispInstance, seqNr, userData);
}

static void cbConfigIsp(uint32_t ispInstance, void *iconf) {
   UNUSED(ispInstance);
   nextCfg = iconf;
}

void startSourcesLocal(icSourceConfig  *sourceConfig, uint32_t sourceId) {
   icSize       iSize;
   iSize.w  = sourceConfig->cropWindow.x2-sourceConfig->cropWindow.x1;
   iSize.h  = sourceConfig->cropWindow.y2-sourceConfig->cropWindow.y1;
   FrmMgrUtilsInitList(frameMgrFrameC, iSize,
         FrmMgrUtilsGetRawFrm(sourceConfig->bitsPerPixel, 1));
   FrmMgrUtilsInitList(frameMgrFrameP, iSize, FRAME_T_FORMAT_RAW_8);
   // set input frames params
   PlgIspMonoLumaConfig(&plgVdo1, iSize,
         GetFrameBppPackFormat(sourceConfig->bitsPerPixel), 0);
   plgSource.eofEvent    = cbEofSourceEvent;
   plgSource.sofEvent    = cbSofSourceEvent;
   plgVdo1.procesStart   = cbStartIspEvent;
   plgVdo1.procesEnd     = cbEndIspEvent;
   PlgSourceStart(&plgSource, sourceConfig,
         GetFrameBppPackFormat(sourceConfig->bitsPerPixel));
   ipServerSourceReady(sourceId);
}

static void isp1xMonoLumaStartSrc(uint32_t sourceInstance, icSourceConfig  *sourceConfig) {
   UNUSED(sourceInstance);
   startSrcState = 1;
   startSrcLocConfig = sourceConfig;
}
static void isp1xMonoLumaStopSrc(uint32_t sourceInstance) {
   UNUSED(sourceInstance);
   stopSrcState = 1;
}

// Turn off capability ############################################################################
static void isp1xMonoLumaTurnOff(void)
{
   if(plgVdo1.plg.fini)   plgVdo1.plg.fini(&plgVdo1);
   if(plgSource.plg.fini) plgSource.plg.fini(&plgSource);
   gServerInfo.sourceServerCtrl[0].cbStartSource  = NULL;
   gServerInfo.sourceServerCtrl[0].cbStopSource   = NULL;
   gServerInfo.pluginServerCtrl[0].cbConfigPlugin = NULL;
   if(plgFifo.plg.fini) plgFifo.plg.fini(&plgFifo);
   gServerInfo.cbDataWasSent   = NULL;
}

static uint32_t checkTurnOfFinalStop(uint32_t *updateVal) {
   if(     (0 == plgSource.plg.status) &&
         (0 == plgVdo1.plg.status)   &&
         (0 == plgFifo.plg.status)         ) {
      MemMgrReset();
      *updateVal = 0;
      return 1;
   }
   return 0;
}

static uint32_t getIspPluginId(void *plg) {
   if(plg == &plgVdo1) return 0;
   assert(0); return 0;
}
