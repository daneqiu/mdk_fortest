/**************************************************************************************************

 @File         : app4CamIspMonoBayer.h
 @Author       : Florin Cotoranu
 @Brief        : Contains application interface For 4 mono Bayer isp in parallel
 Date          : 08 - April - 2016
 E-mail        : florin.cotoranu@movidius.com
 Copyright     : � Movidius Srl 2016, � Movidius Ltd 2016

 Description : Pipeline supports up to 4 mono Bayer ISP


 **************************************************************************************************/

#ifndef _ISP4MONOBAYER_H_
#define _ISP4MONOBAYER_H_

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include "IcTypes.h"
#include "ipipe.h"

/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
// as lrt start, this function will be called. (Doc. 4.2)
void isp4xMonoBayerCbIcSetup(icCtrl *ctrl);

// application specific Tear Down
void isp4xMonoBayerCbIcTearDown(void);

//
icStatusCode isp4xMonoBayerSrcComit(icCtrl *ctrl);

//
void isp4xMonoBayerMain(void);

#endif // _ISP4MONOBAYER_H_


