/**************************************************************************************************

 @File         : stitch6cam.c
 @Author       : AG
 @Brief        : Contains 3 Isp project main control functionality
 Date          : 01 - March - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : � Movidius Srl 2014, � Movidius Ltd 2015

 Description :


 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include "DrvLeonL2C.h"
#include "ipipe.h"
#include "ipipeDbg.h"
#include "FrameMgrUtils.h"
#include "ipipeUtils.h"
#include "ipipeOpipeUtils.h"
#include "FrameMgrApi.h"
#include "IpipeServerApi.h"
#include "ipipeMsgQueue.h"
#include "PlgSourceApi.h"
#include "PlgFifoApi.h"
#include "PlgIspFullApi.h"
#include "PlgStitch6camsApi.h"
#include "stitch6cam.h"
#include <VcsHooksApi.h>


/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/
#define REAL_NR_OF_CAMS             3 //TODO: remove this and replace everywhere with  when 6 cams are available MAX_NR_OF_CAMS
#define MAX_NR_OF_CAMS              6
#define NR_OF_BUFFERS_PER_SOURCE    3
#define NR_OF_BUFFERS_PER_ISP_OUT   3
#define NR_OF_BUFFERS_PER_STITCH6   2



/**************************************************************************************************
 ~~~  Local variables
 **************************************************************************************************/
static PlgSource      plgSource[REAL_NR_OF_CAMS] SECTION(".cmx.cdmaDescriptors") ALIGNED(8);
static PlgIspFull     plgVdo1  [MAX_NR_OF_CAMS] SECTION(".cmx.cdmaDescriptors") ALIGNED(8);
static PlgFifo        plgFifo;
static PlgStitch6cams plgStitch6 SECTION(".cmx.cdmaDescriptors") ALIGNED(8);

static FramePool frameMgrPoolC    [REAL_NR_OF_CAMS];
static FramePool frameMgrPoolFifo [MAX_NR_OF_CAMS];
static FramePool frameMgrPoolVideo[MAX_NR_OF_CAMS];
static FramePool frameMgrPoolStitch6;

static FrameT *frameMgrFrameC    [REAL_NR_OF_CAMS];
static FrameT *frameMgrFrameVideo[MAX_NR_OF_CAMS];
static FrameT *frameMgrFrameStitch6;

static uint32_t        startSrcState[REAL_NR_OF_CAMS];
static uint32_t        stopSrcState [REAL_NR_OF_CAMS];
static icSourceConfig  *startSrcLocConfig[REAL_NR_OF_CAMS];
static uint32_t        tearDownEnable;
static void            *nextCfg [MAX_NR_OF_CAMS];
static uint32_t        previewAbleMode;
static FrameProducedCB cbOutputList;
static FrameProducedCB cbOutputListIsp[MAX_NR_OF_CAMS][2];
static FrameProducedCB cbOutputListFIFO[MAX_NR_OF_CAMS];
static FrameProducedCB cbOutputListSrc[REAL_NR_OF_CAMS][2];

/**************************************************************************************************
 ~~~ Local File function declarations
 **************************************************************************************************/
static void     turnOfapp6Cam(void);
static uint32_t checkTurnOfFinalStop(uint32_t *updateVal);
static void     startSourcesLocal(icSourceConfig  *sourceConfig, uint32_t sourceId);
static uint32_t getSourcePluginId(void *plg);
static uint32_t getIspPluginId(void *plg);
static void     app6CamIspStartSrc(uint32_t sourceInstance, icSourceConfig  *sourceConfig);
static void     app6CamIspStopSrc (uint32_t sourceInstance) ;
static void     cbEofSourceEvent(void *plg, FrameT *frame);
static void     cbSofSourceEvent(void *plg, FrameT *frame);
static void     cbConfigIsp    (uint32_t ispInstance, void *iconf);
static void     cbStartIspEvent(void *plg, uint32_t seqNr, void *userData);
static void     cbEndIspEvent  (void *plg, uint32_t seqNr, void *userData);
static void     cbOutput(FrameT *frame, void *pluginObj);

static void     cbErrorIspEvent(void* plg, icSeverity severity, icError errorNo, void *userData);
/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/

void app6CamIspCbIcSetup(icCtrl *ctrl) {
    uint32_t i;
    OpipeReset(); //general inits
    memset((void*)startSrcState, 0, sizeof(startSrcState));
    memset((void*)stopSrcState, 0, sizeof(stopSrcState));
    memset((void*)startSrcLocConfig, 0, sizeof(startSrcLocConfig));
    memset((void*)nextCfg, 0, sizeof(nextCfg));
    tearDownEnable = 0;
    previewAbleMode = 0;
    gServerInfo.cbDataWasSent   = NULL;
    // create plug-ins
    PlgFifoCreate   ((void*)&plgFifo);
    PlgFifoConfig   ((void*)&plgFifo, (uint32_t)MAX_NR_OF_CAMS);
    plgFifo.plg.init(frameMgrPoolFifo, MAX_NR_OF_CAMS, (void*)&plgFifo);

    icSize outputFrameSize;
    outputFrameSize.w = 1920;
    outputFrameSize.h = 1080;
    PlgStitch6camsCreate((void *)&plgStitch6);
    PlgStitch6camsSetParams((void *)&plgStitch6, 6, outputFrameSize);
    frameMgrFrameStitch6 = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_STITCH6);

    for(i = 0; i < MAX_NR_OF_CAMS; i++) {
        PlgIspFullCreate((void*)&plgVdo1[i]);
        plgVdo1[i].plg.init  (&frameMgrPoolVideo[i], 1, (void*)&plgVdo1[i]);
    }
    for(i = 0; i < MAX_NR_OF_CAMS; i++) {
        if(i < REAL_NR_OF_CAMS)
        {
            gServerInfo.sourceServerCtrl[i].cbStartSource  = NULL;
            gServerInfo.sourceServerCtrl[i].cbStopSource   = NULL;
            gServerInfo.pluginServerCtrl[i].cbConfigPlugin = NULL;
            PlgSourceCreate ((void*)&plgSource[i], i);
            plgSource[i].plg.init(&frameMgrPoolC[i],     1, (void*)&plgSource[i]);
            // source that have associated trigger capture capability
            gServerInfo.sourceServerCtrl[i].pool = plgSource[i].outputPools;
            frameMgrFrameC[i]       = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_SOURCE);

            cbOutputListSrc[i][0].callback = plgFifo.plg.callbacks[i].callback;
            cbOutputListSrc[i][0].pluginObj = plgFifo.plg.callbacks[i].pluginObj;
            cbOutputListSrc[i][1].callback = plgFifo.plg.callbacks[i + 3].callback;
            cbOutputListSrc[i][1].pluginObj = plgFifo.plg.callbacks[i + 3].pluginObj;
            // Source output pool
            FrameMgrCreatePool(&frameMgrPoolC[i], frameMgrFrameC[i], cbOutputListSrc[i], 2);

        }

        frameMgrFrameVideo[i]   = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_ISP_OUT);
        cbOutputListFIFO[i].callback        = plgVdo1[i].plg.callbacks[0].callback;
        cbOutputListFIFO[i].pluginObj       = plgVdo1[i].plg.callbacks[0].pluginObj;
        // Mux  output pool, special case for serialization plug-in, no frame inside, as it pass the input data out
        FrameMgrCreatePool(&frameMgrPoolFifo[i],  NULL,                  &cbOutputListFIFO[i], 1);

        cbOutputListIsp[i][0].callback  = cbOutput;
        cbOutputListIsp[i][0].pluginObj = &plgVdo1[i];
        cbOutputListIsp[i][1].callback  = plgStitch6.plg.callbacks[i].callback;
        cbOutputListIsp[i][1].pluginObj = plgStitch6.plg.callbacks[i].pluginObj;
        FrameMgrCreatePool(&frameMgrPoolVideo[i], frameMgrFrameVideo[i], cbOutputListIsp[i],         2);
        // create descriptions for available functionality regarding isp. Los isp side, base on this
        // informations, will properly update parameters and config sensors.
        ipServerRegSourceQuery(i,
                "Source",
                IC_SOURCE_ATTR_HAS_VIDEO_ISP |
                IC_SOURCE_ATTR_HAS_VIDEO_OUT |
                IC_SOURCE_ATTR_HAS_RAW_OUT   ,
                NR_OF_BUFFERS_PER_SOURCE,
                i, i + MAX_NR_OF_CAMS*1, i +  + MAX_NR_OF_CAMS*2, 0);
        ipServerRegIspQuery(i                       , "IspVdo",     IC_ISP_ATTR_VIDEO_LINK, i);
        ipServerRegOutputQuery(i                    , "Out",        IC_OUTPUT_FRAME_DATA_TYPE_PREVIEW  , i); // preview cam output

        ipServerQueryAddChild(ctrl->icPipelineDescription.icQuerySource[i], ctrl->icPipelineDescription.icQueryIsp[i]);
        ipServerQueryAddChild(ctrl->icPipelineDescription.icQueryIsp[i], ctrl->icPipelineDescription.icQueryOutput[i]);
    };
    cbOutputList.callback  = cbOutput;
    cbOutputList.pluginObj = &plgStitch6;
    FrameMgrCreatePool(&frameMgrPoolStitch6, frameMgrFrameStitch6, &cbOutputList,       1);
    plgStitch6.plg.init(&frameMgrPoolStitch6, 1, (void *)&plgStitch6);
}

//
void app6CamIspCbIcTearDown(void) {
    turnOfapp6Cam();
    tearDownEnable = 1;
}

int sippOpipeResurcesFree(void) {
    uint32_t ret = 0;
    uint32_t x;
    for (x = 0; x < MAX_NR_OF_CAMS; x++) {
        ret = ret | plgVdo1[x].plg.status;
    }
    return(0 == ret);
}

//
void app6CamIspMain(void) {
    uint32_t x;
    if(tearDownEnable) {
        if(checkTurnOfFinalStop(&tearDownEnable)) {
            ipServerWasTornDown();
            exit(0);
        }
    }

    // trigger just if opipe is idle
    if (sippOpipeResurcesFree()) {
        for (x = 0; x < REAL_NR_OF_CAMS; x++) {
            // Start Source Command in order to avoid big interrupt time
            if (1 == startSrcState[x]) {
                startSourcesLocal(startSrcLocConfig[x], x);
                startSrcState[x] = 0;
            }
            // Stop Source Command in order to avoid big interrupt time
            if (1 == stopSrcState[x]) {
                if(plgSource[x].plg.fini)
                    plgSource[x].plg.fini(&plgSource[x]);

                if(plgVdo1[x].plg.fini)
                    plgVdo1[x].plg.fini(&plgVdo1[x]);
                if(plgVdo1[x + 3].plg.fini)
                    plgVdo1[x + 3].plg.fini(&plgVdo1[x + 3]);
                while ( (PLG_STATS_RUNNING == plgSource[x].plg.status) ||
                        (PLG_STATS_RUNNING == plgVdo1[x].plg.status) ||
                        (PLG_STATS_RUNNING == plgVdo1[x + 3].plg.status) ) {
                    NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;
                    NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;
                }

                stopSrcState[x] = 0;
                ipServerSourceStopped(x);
            }
        }
        plgFifo.triger((void*)&plgFifo);
        NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;
    }
}

//
icStatusCode app6CamIspSrcComit(icCtrl *ctrl) {
    int32_t x;

    /*ALLOC*/ AllocOpipeReset(); //clear prev alloc
    /*ALLOC*/ AllocOpipeRxCmxBuffs (ctrl);
    /*ALLOC*/
    /*ALLOC*/ PlgIspBase *ispBase[MAX_NR_OF_CAMS]; //Isp buffs
    /*ALLOC*/ for(x=0; x<MAX_NR_OF_CAMS; x++)
        /*ALLOC*/   ispBase[x] = &plgVdo1[x].base;
    /*ALLOC*/ AllocOpipeIspCmxBuffs(ctrl, MAX_NR_OF_CAMS, ispBase);

    // Allocate frames buffers memory
    for (x = 0; x < REAL_NR_OF_CAMS; x++) {
        if(IPIPE_SRC_SETUP == ctrl->source[x].sourceStatus) {
            //size.w = (inSz.w * hN - 1)/hD + 1;
            //size.h = (inSz.h * vN - 1)/vD + 1;
            uint32_t maxIspW = ((ctrl->source[x].sourceSetup.maxWidth *
                    ctrl->source[x].sourceSetup.maxHorizN - 1) /
                    ctrl->source[x].sourceSetup.maxHorizD + 1);
            uint32_t maxIspH = ((ctrl->source[x].sourceSetup.maxHeight *
                    ctrl->source[x].sourceSetup.maxVertN - 1) /
                    ctrl->source[x].sourceSetup.maxVertD + 1);
            // Allocate smaller ddr as video output for cam0 is quarter from the input resolution
            uint32_t videoFrameSize = (maxIspW * maxIspH);
            if(QUARTER_MODE_FOR_VIDEO_PIPE == ctrl->source[x].sourceSetup.appSpecificInfo) {
                videoFrameSize = videoFrameSize>>2;
                previewAbleMode = 1;
            }
            ipServerFrameMgrAddBuffs(frameMgrFrameVideo[x],videoFrameSize,videoFrameSize>>1, 0);
            ipServerFrameMgrAddBuffs(frameMgrFrameVideo[x + 3],videoFrameSize,videoFrameSize>>1, 0);
            if(x==0)
                ipServerFrameMgrAddBuffs(frameMgrFrameStitch6,videoFrameSize,videoFrameSize>>1, 0);
            // this functionality is available just now !!! Important to do that
            gServerInfo.sourceServerCtrl[x].cbStartSource = app6CamIspStartSrc;
            gServerInfo.sourceServerCtrl[x].cbStopSource = app6CamIspStopSrc;
            gServerInfo.pluginServerCtrl[x].cbConfigPlugin = cbConfigIsp;
        }
    }

    gServerInfo.cbDataWasSent   = FrameMgrReleaseFrame; // !!! different approach here


    return IC_STATS_SUCCESS;
}


/**************************************************************************************************
 ~~~ Local Functions Implementation
 **************************************************************************************************/
// Output callback linked to plug-ins
static void cbOutput(FrameT *frame, void *pluginObj) {
    uint32_t ispInstance = getIspPluginId(pluginObj);
    ipServerSendData(frame, ispInstance);
}

static void cbSofSourceEvent(void *plg, FrameT *frame) {
    uint32_t idx = getSourcePluginId(plg);
    if(frame) {
        if(nextCfg[idx]) {
            frame->appSpecificData = nextCfg[idx];
            nextCfg[idx] = NULL;
            ipServerReadoutStart((icSourceInstance)idx,
                    ((icIspConfig*)(frame->appSpecificData))->userData,
                    frame->seqNo, frame->timestamp[0]);
        }
        else {
            frame->appSpecificData = NULL;
            //ipServerReadoutStart((icSourceInstance)idx,  NULL, frame->seqNo, frame->timestamp[0]);
        }
    }
    else {
        //ipServerReadoutStart((icSourceInstance)idx, NULL, 0, 0);
    }
}
static void cbEofSourceEvent(void *plg, FrameT *frame) {
    uint32_t idx = getSourcePluginId(plg);
    if(frame) {
        if(frame->appSpecificData) {
            if (IC_PIPECTL_ZSL_LOCK & (((icIspConfig *)(frame->appSpecificData))->pipeControl)) {
                //            if (IC_PIPECTL_ZSL_LOCK &
                //                    (swcLeonReadNoCacheU32((uint32_t)&(((icIspConfig *)nextCfg[idx])->pipeControl)))) {
                FrameMgrIncreaseNrOfConsumer(frame, 1);
                ipServerFrameLocked((icSourceInstance)idx, frame);
            }
            ipServerReadoutEnd((icSourceInstance)idx,
                    ((icIspConfig*)(frame->appSpecificData))->userData,
                    frame->seqNo, frame->timestamp[0]);
        }
        else {
            //ipServerReadoutEnd((icSourceInstance)idx, 0, frame->seqNo, frame->timestamp[0]);
        }
    }
    else {
        //ipServerReadoutEnd((icSourceInstance)idx, NULL, 0, 0);
    }
}

static void cbStartIspEvent(void *plg, uint32_t seqNr, void *userData) {
    uint32_t ispInstance = getIspPluginId(plg);
    ipServerIspStart(ispInstance, seqNr, userData);
}
static void cbEndIspEvent(void *plg, uint32_t seqNr, void *userData) {
    uint32_t ispInstance = getIspPluginId(plg);
    ipServerIspEnd(ispInstance, seqNr, userData);
}

// Error reporting callbacks
void cbErrorIspEvent(void *plg, icSeverity severity, icError errorNo, void *userData) {
    uint32_t ispInstance = getIspPluginId(plg);
    ipServerIspReportError(ispInstance, severity, errorNo, userData);
}

static void cbConfigIsp(uint32_t ispInstance, void *iconf) {
    nextCfg[ispInstance] = iconf;
}

static void startSourcesLocal(icSourceConfig  *sourceConfig, uint32_t sourceId) {
    icSize       iSize;
    icSize       oVdoSize;
    iSize.w  = sourceConfig->cropWindow.x2 - sourceConfig->cropWindow.x1;
    iSize.h  = sourceConfig->cropWindow.y2 - sourceConfig->cropWindow.y1;
    oVdoSize = iSize;
    if(QUARTER_MODE_FOR_VIDEO_PIPE == pSrvIcCtrl->source[sourceId].sourceSetup.appSpecificInfo) {
        oVdoSize.w = iSize.w>>1;
        oVdoSize.h = iSize.h>>1;
    }
    FrmMgrUtilsInitList(frameMgrFrameC[sourceId], iSize,
            FrmMgrUtilsGetRawFrm(sourceConfig->bitsPerPixel, (sourceConfig->mipiRxData.recNrl <= IC_SIPP_DEVICE3 ? 1 : 0)));
    FrmMgrUtilsInitList(frameMgrFrameVideo[sourceId], oVdoSize, FRAME_T_FORMAT_YUV420);
    FrmMgrUtilsInitList(frameMgrFrameVideo[sourceId + 3], oVdoSize, FRAME_T_FORMAT_YUV420);
    if(sourceId == 0)
        FrmMgrUtilsInitList(frameMgrFrameStitch6, oVdoSize, FRAME_T_FORMAT_YUV420);

    PlgIspFullConfig(&plgVdo1 [sourceId], iSize, GetFrameBppPackFormat(sourceConfig->bitsPerPixel), previewAbleMode);
    PlgIspFullConfig(&plgVdo1 [sourceId + 3], iSize, GetFrameBppPackFormat(sourceConfig->bitsPerPixel), previewAbleMode);
    //Pipes to clear used resources at EOF
    plgVdo1 [sourceId].op.p.flags |= CLEAN_EXIT;
    plgVdo1 [sourceId + 3].op.p.flags |= CLEAN_EXIT;

    if(QUARTER_MODE_FOR_VIDEO_PIPE == pSrvIcCtrl->source[sourceId].sourceSetup.appSpecificInfo)
        plgVdo1[sourceId].op.p.flags |=  GEN_PREVIEW;
    plgSource[sourceId].eofEvent        = cbEofSourceEvent;
    plgSource[sourceId].sofEvent        = cbSofSourceEvent;
    plgVdo1[sourceId].procesStart       = cbStartIspEvent;
    plgVdo1[sourceId].procesEnd         = cbEndIspEvent;
    plgVdo1[sourceId].procesIspError    = cbErrorIspEvent;
    plgVdo1[sourceId + 3].procesStart       = cbStartIspEvent;
    plgVdo1[sourceId + 3].procesEnd         = cbEndIspEvent;
    plgVdo1[sourceId + 3].procesIspError    = cbErrorIspEvent;
    PlgSourceStart(&plgSource[sourceId], sourceConfig,
            GetFrameBppPackFormat(sourceConfig->bitsPerPixel));
    ipServerSourceReady(sourceId);
}

static void app6CamIspStartSrc(uint32_t sourceInstance, icSourceConfig  *sourceConfig) {
    startSrcState[sourceInstance] = 1;
    startSrcLocConfig[sourceInstance] = sourceConfig;
}
static void app6CamIspStopSrc(uint32_t sourceInstance) {
    stopSrcState[sourceInstance] = 1;
}

static void turnOfapp6Cam(void) {
    uint32_t i;
    for(i = 0; i < REAL_NR_OF_CAMS; i++) {
        if(plgVdo1[i].plg.fini)   plgVdo1[i].plg.fini(&plgVdo1[i]);
        if(plgVdo1[i + 3].plg.fini)   plgVdo1[i + 3].plg.fini(&plgVdo1[i + 3]);
        if(plgSource[i].plg.fini) plgSource[i].plg.fini(&plgSource[i]);
        gServerInfo.sourceServerCtrl[i].cbStartSource  = NULL;
        gServerInfo.sourceServerCtrl[i].cbStopSource   = NULL;
        gServerInfo.pluginServerCtrl[i].cbConfigPlugin = NULL;
    }
    if(plgFifo.plg.fini) plgFifo.plg.fini(&plgFifo);
    if(plgStitch6.plg.fini) plgStitch6.plg.fini(&plgStitch6);
    gServerInfo.cbDataWasSent   = NULL;
}
static uint32_t checkTurnOfFinalStop(uint32_t *updateVal) {
    uint32_t idle = 1; //"all plugins idle" flag; assume true initially
    uint32_t i;
    if(PLG_STATS_IDLE != plgFifo.plg.status)
        idle = 0;

    if(idle) {
        for(i=0; i<REAL_NR_OF_CAMS; i++)
            if(PLG_STATS_IDLE != plgSource[i].plg.status){
                idle = 0;
                break;
            }
    }

    if(idle) {
        for(i=0; i<MAX_NR_OF_CAMS; i++)
            if(PLG_STATS_IDLE != plgVdo1[i].plg.status){
                idle = 0;
                break;
            }
    }

    if(idle){
        MemMgrReset();
        *updateVal = 0;
        return 1;
    }else return 0;
}

static uint32_t getSourcePluginId(void *plg) {
    uint32_t i;
    for(i = 0; i < REAL_NR_OF_CAMS; i++)
        if(plg == &plgSource[i]) return i;
    assert(0); return 0;
}

static uint32_t getIspPluginId(void *plg) {
    uint32_t i;
    for(i = 0; i < MAX_NR_OF_CAMS; i++)
        if(plg == &plgVdo1[i]) return i;
    if(plg == &plgStitch6) return (MAX_NR_OF_CAMS);
    assert(0); return 0;
}
