///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// -----------------------------------------------------------------------------
///
/// Revision History
/// ===================================
/// 05-Nov-2014 : Author ( MM Solutions AD )
/// Created
/// =============================================================================

#ifndef _MIPI_TX_MODULE_H
#define _MIPI_TX_MODULE_H

#ifdef __cplusplus
extern "C" {
#endif

#define MIPI_TX_ROUND_UP(X,N) \
    ((((X) + (N) - 1) / (N)) * (N))

typedef struct {
    unsigned int chunk;
    char client_data[0];
} mipi_tx_header_t;

typedef struct {
    int num_lanes;
    int mipi_clock;
    int use_irq;
} mipi_tx_create_t;

int mipi_tx_create(mipi_tx_create_t *params);
void mipi_tx_destroy(void);

typedef struct mipi_tx_client mipi_tx_client_t;

typedef struct {
    int priority;
    int data_type;
    int width;
    int height_1;
    int height_2;
    int height_3;
    int line_size;
    int header_height;
    int header_data_type;
    int num_chunks;
} mipi_tx_characteristics_t;

typedef void mipi_tx_clinet_sent_callback_t(
        mipi_tx_client_t *client,
        void *clinet_prv,
        mipi_tx_header_t *header,
        void *p1,
        void *p2,
        void *p3
    );

void mipi_tx_poll(void);
mipi_tx_client_t * mipi_tx_client_register(
        mipi_tx_characteristics_t *characteristics,
        void *clinet_prv,
        mipi_tx_clinet_sent_callback_t *sent_callback
    );
void mipi_tx_client_unregister(
        mipi_tx_client_t *client
    );

int mipi_tx_send(
        mipi_tx_client_t *client,
        mipi_tx_header_t *header,
        void *p1,
        void *p2,
        void *p3
    );

#ifdef __cplusplus
}
#endif

#endif /* _MIPI_TX_MODULE_H */

