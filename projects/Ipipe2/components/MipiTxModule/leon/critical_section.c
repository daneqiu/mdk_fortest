///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// -----------------------------------------------------------------------------
///
/// Revision History
/// ===================================
/// 05-Nov-2014 : Author ( MM Solutions AD )
/// Created
/// =============================================================================

#include "config.h"

#if CRITICAL_SECTION_ENABLE

#if defined(__RTEMS__)
#include <rtems.h>
#else
#include <swcLeonUtils.h>
#endif

int critical_section_enter(void)
{
    int key;
#if  defined(__RTEMS__)
    rtems_interrupt_disable(key);
#else //defined(__RTEMS__)
    key = swcLeonSetPIL(15);
    if (key <= 6) {
        swcLeonSetPIL(6);
    }
#endif //defined(__RTEMS__)
    return key;
}

void critical_section_exit(int key)
{
#if  defined(__RTEMS__)
    rtems_interrupt_enable(key);
#else //defined(__RTEMS__)
    swcLeonSetPIL(key);
#endif //defined(__RTEMS__)
}

#else /* CRITICAL_SECTION_ENABLE */

int critical_section_enter(void)
{
    return 0;
}

void critical_section_exit(int key)
{
}

#endif /* CRITICAL_SECTION_ENABLE */
