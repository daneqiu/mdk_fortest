///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// -----------------------------------------------------------------------------
///
/// Revision History
/// ===================================
/// 05-Nov-2014 : Author ( MM Solutions AD )
/// Created
/// =============================================================================

#include "list.h"
#include "critical_section.h"
#include "pool_bm.h"

#include "config.h"

#define ARRAY_SIZE(arr) \
    ((int)(sizeof(arr) / sizeof((arr)[0])))

struct pool_bm {
    struct list_head link;
    char *name;
    unsigned int elem_size;
    unsigned int elems_count;
    void *mem;
    struct list_head free_list;
};

static pool_bm_t pool_bm_storage[POOL_BM_NUM];

static struct {
    struct list_head free_list;
    struct list_head in_use_list;
} pool_bm;

void pool_bm_free(pool_bm_t *pool, void *p)
{
    struct list_head *link;
    int key;

    link = p;

    key = critical_section_enter();
    list_add(link, &pool->free_list);
    critical_section_exit(key);
}

void * pool_bm_try_alloc(pool_bm_t *pool)
{
    struct list_head *link;
    int key;

    key = critical_section_enter();
    if (list_empty(&pool->free_list)) {
        critical_section_exit(key);
        return NULL;
    }
    link = pool->free_list.next;
    list_del(link);
    critical_section_exit(key);

    return link;
}

void pool_bm_destroy(pool_bm_t *pool)
{
    int key;

    key = critical_section_enter();
    list_move(&pool->link, &pool_bm.free_list);
    critical_section_exit(key);
}

pool_bm_t * pool_bm_create(
        char *pool_name,
        unsigned int elem_size,
        unsigned int elems_count,
        void *mem
    )
{
    pool_bm_t *pool;
    struct list_head *link;
    int i;
    int key;

    if (elem_size < sizeof (struct list_head)) {
        return NULL;
    }

    key = critical_section_enter();
    if (list_empty(&pool_bm.free_list)) {
        critical_section_exit(key);
        return NULL;
    }
    pool = list_first_entry(&pool_bm.free_list, pool_bm_t, link);
    list_move(&pool->link, &pool_bm.in_use_list);
    critical_section_exit(key);

    pool->name = pool_name;
    pool->elem_size = elem_size;
    pool->elems_count = elems_count;
    pool->mem = mem;

    INIT_LIST_HEAD(&pool->free_list);
    for (i = 0; (unsigned int)i < pool->elems_count; i++) {
        link = (struct list_head *)((char *)pool->mem + i*pool->elem_size);
        list_add(link, &pool->free_list);
    }

    return pool;
}

void pool_bm_init(void)
{
    int i;

    INIT_LIST_HEAD(&pool_bm.free_list);
    INIT_LIST_HEAD(&pool_bm.in_use_list);

    for (i = 0; i < ARRAY_SIZE(pool_bm_storage); i++) {
        list_add(&pool_bm_storage[i].link, &pool_bm.free_list);
    }
}

