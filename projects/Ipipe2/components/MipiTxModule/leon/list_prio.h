///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// -----------------------------------------------------------------------------
///
/// Revision History
/// ===================================
/// 05-Nov-2014 : Author ( MM Solutions AD )
/// Created
/// =============================================================================

#ifndef _LINUX_LIST_PRIO_H
#define _LINUX_LIST_PRIO_H

#ifdef __cplusplus
extern "C" {
#endif

#include "list.h"

static inline void list_prio_add(
        struct list_head *list,
        struct list_head *new,
        int prio,
        int (*cmp)(struct list_head *next, int prio)
    )
{
    struct list_head *next;
    list_for_each(next, list)
        if (cmp(next, prio))
            break;
    list_add_tail(new, next);
}

#ifdef __cplusplus
}
#endif

#endif /* _LINUX_LIST_PRIO_H */

