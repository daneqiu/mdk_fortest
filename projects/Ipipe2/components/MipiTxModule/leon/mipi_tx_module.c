///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// -----------------------------------------------------------------------------
///
/// Revision History
/// ===================================
/// 05-Nov-2014 : Author ( MM Solutions AD )
/// Created
/// =============================================================================

#include "list.h"
#include "list_prio.h"
#include "critical_section.h"
#include "pool_bm.h"
#include "mipi_tx_hw.h"
#include "mipi_tx_module.h"

#include "config.h"

#ifndef UNUSED
#define UNUSED(x) (void)x
#endif


#define ARRAY_SIZE(arr) \
    ((int)(sizeof(arr) / sizeof((arr)[0])))

typedef struct mipi_tx_module mipi_tx_module_t;

typedef struct {
    struct list_head link;
    mipi_tx_client_t *client;
    mipi_tx_header_t *header;
    void *p1;
    void *p2;
    void *p3;
    int chunk;
} request_t;

struct mipi_tx_client {
    int priority;
    int data_type;
    int width;
    int height_1;
    int height_2;
    int height_3;
    int line_size;
    int header_height;
    int header_data_type;
    int num_chunks;
    void *prv;
    mipi_tx_clinet_sent_callback_t *sent_callback;
    mipi_tx_module_t *mipi_tx_module;
    int lines_per_chunk_1;
    int lines_per_chunk_2;
    int lines_per_chunk_3;
};

typedef enum {
    MIPI_TX_STATE__IDLE,
    MIPI_TX_STATE__IDLE_TO_RUN,
    MIPI_TX_STATE__RUN,
} mipi_tx_state_t;

struct mipi_tx_module {
    int num_lanes;
    int mipi_clock;

    int created;
    int use_irq;

    pool_bm_t *clients_pool;
    pool_bm_t *requests_pool;
    struct list_head requests;
    mipi_tx_state_t state;
    mipi_tx_hw_t *hw;
};

static mipi_tx_client_t client_storage[MIPI_TX_MODULE_CLENTS_NUM];
static request_t request_storage[MIPI_TX_MODULE_REQUEST_QUEUE_SIZE];
static mipi_tx_module_t mipi_tx_module_storage = {
    .created = 0
};

static int cmp_client_prio(struct list_head *next, int prio)
{
    return prio < list_entry(next, request_t, link)->client->priority;
}

static int calc_lines_per_chunk(int data_type, int lines, int chunks)
{
    UNUSED(data_type);
    /* TODO: Depend on type and plane */
    return lines/chunks;
}

#define CHUNK_ADDR(R,N) \
( \
    (void *)( \
              (char *)((R)->p##N) \
            + (R)->chunk * (R)->client->lines_per_chunk_##N * (R)->client->line_size \
        ) \
)

static void start_new_transaction(mipi_tx_module_t *mipi_tx_module)
{
    request_t *request;
    mipi_tx_hw_frame_t frame;
    int key;

    key = critical_section_enter();
    if (list_empty(&mipi_tx_module->requests)) {
        mipi_tx_module->state = MIPI_TX_STATE__IDLE;
        critical_section_exit(key);
        mipi_tx_hw_stop(mipi_tx_module->hw);
        return;
    }
    request = list_entry(mipi_tx_module->requests.next, request_t, link);
    critical_section_exit(key);

    request->header->chunk = request->chunk;

    frame.width = request->client->width;
    frame.line_size = request->client->line_size;
    frame.height[0] = request->client->header_height;
    frame.height[1] = request->client->lines_per_chunk_1;
    frame.height[2] = request->client->lines_per_chunk_2;
    frame.height[3] = request->client->lines_per_chunk_3;
    frame.data_type[0] = request->client->header_data_type;
    frame.data_type[1] = request->client->data_type;
    frame.data_type[2] = request->client->data_type;
    frame.data_type[3] = request->client->data_type;
    frame.p[0] = request->header;
    frame.p[1] = CHUNK_ADDR(request, 1);
    frame.p[2] = CHUNK_ADDR(request, 2);
    frame.p[3] = CHUNK_ADDR(request, 3);
    mipi_tx_hw_send(mipi_tx_module->hw, request, &frame);
}

static void hw_sent_callback(mipi_tx_hw_t *mipi_tx_hw, void *client_prv)
{
    mipi_tx_module_t *mipi_tx_module;
    mipi_tx_client_t *client;
    request_t *request;
    mipi_tx_header_t *header;
    void *p1;
    void *p2;
    void *p3;

    UNUSED(mipi_tx_hw);

    request = client_prv;
    client = request->client;
    mipi_tx_module = client->mipi_tx_module;
    int key;

    request->chunk++;
    if (client->num_chunks <= request->chunk) {
        key = critical_section_enter();
        list_del(&request->link);
        critical_section_exit(key);

        header = request->header;
        p1 = request->p1;
        p2 = request->p2;
        p3 = request->p3;

        pool_bm_free(mipi_tx_module->requests_pool, request);

        client->sent_callback(
                client,
                client->prv,
                header,
                p1,
                p2,
                p3
            );
    }

    start_new_transaction(mipi_tx_module);
}

void mipi_tx_poll(void)
{
    mipi_tx_module_t *mipi_tx_module;
    mipi_tx_module = &mipi_tx_module_storage;
    if (mipi_tx_module->created && !mipi_tx_module->use_irq) {
        mipi_tx_hw_poll(mipi_tx_module->hw);
    }
}

int mipi_tx_send(
        mipi_tx_client_t *client,
        mipi_tx_header_t *header,
        void *p1,
        void *p2,
        void *p3
    )
{
    mipi_tx_module_t *mipi_tx_module;
    request_t *request;
    int key;

    mipi_tx_module = client->mipi_tx_module;

    request = pool_bm_try_alloc(mipi_tx_module->requests_pool);
    if (!request) {
        printf("Failed to allocate new Request struct!\n");
        goto exit1;
    }
    request->client = client;
    request->header = header;
    request->p1 = p1;
    request->p2 = p2;
    request->p3 = p3;
    request->chunk = 0;

    key = critical_section_enter();
    list_prio_add(
            &mipi_tx_module->requests,
            &request->link,
            client->priority,
            cmp_client_prio
        );
    if (MIPI_TX_STATE__IDLE == mipi_tx_module->state) {
        mipi_tx_module->state = MIPI_TX_STATE__IDLE_TO_RUN;
    }
    critical_section_exit(key);

    if (MIPI_TX_STATE__IDLE_TO_RUN == mipi_tx_module->state) {
        mipi_tx_module->state = MIPI_TX_STATE__RUN;
        mipi_tx_hw_start(mipi_tx_module->hw);
        start_new_transaction(mipi_tx_module);
    }

    return 0;
exit1:
    return -1;
}

mipi_tx_client_t * mipi_tx_client_register(
        mipi_tx_characteristics_t *characteristics,
        void * clinet_prv,
        mipi_tx_clinet_sent_callback_t *sent_callback
    )
{
    mipi_tx_module_t *mipi_tx_module;
    mipi_tx_client_t *client;

    mipi_tx_module = &mipi_tx_module_storage;

    /* TODO: check input params */
    if (characteristics->height_1 % characteristics->num_chunks) {
        printf(
                "Wrong parameter: height_1=%d not aligned to num_chunks=%d!\n",
                characteristics->height_1,
                characteristics->num_chunks
            );
        goto exit1;
    }
    if (characteristics->height_2 % characteristics->num_chunks) {
        printf(
                "Wrong parameter: height_2=%d not aligned to num_chunks=%d!\n",
                characteristics->height_2,
                characteristics->num_chunks
            );
        goto exit1;
    }
    if (characteristics->height_3 % characteristics->num_chunks) {
        printf(
                "Wrong parameter: height_3=%d not aligned to num_chunks=%d!\n",
                characteristics->height_3,
                characteristics->num_chunks
            );
        goto exit1;
    }

    client = pool_bm_try_alloc(mipi_tx_module->clients_pool);
    if (!client) {
        printf("Failed to allocate new Clinet struct!\n");
        goto exit1;
    }
    client->priority = characteristics->priority;
    client->data_type = characteristics->data_type;
    client->width = characteristics->width;
    client->height_1 = characteristics->height_1;
    client->height_2 = characteristics->height_2;
    client->height_3 = characteristics->height_3;
    client->line_size = characteristics->line_size;
    client->header_height = characteristics->header_height;
    client->header_data_type = characteristics->header_data_type;
    client->num_chunks = characteristics->num_chunks;
    client->prv = clinet_prv;
    client->sent_callback = sent_callback;
    client->mipi_tx_module = mipi_tx_module;

    client->lines_per_chunk_1 = calc_lines_per_chunk(
            client->data_type,
            client->height_1,
            client->num_chunks
        );
    client->lines_per_chunk_2 = calc_lines_per_chunk(
            client->data_type,
            client->height_2,
            client->num_chunks
        );
    client->lines_per_chunk_3 = calc_lines_per_chunk(
            client->data_type,
            client->height_3,
            client->num_chunks
        );

    return client;
exit1:
    return NULL;
}

void mipi_tx_client_unregister(
        mipi_tx_client_t *client
    )
{
    mipi_tx_module_t *mipi_tx_module;
    mipi_tx_module = client->mipi_tx_module;
    /* TODO: Dequeue all reduest for this client? */
    pool_bm_free(mipi_tx_module->clients_pool, client);
}

void mipi_tx_destroy(void)
{
    mipi_tx_module_t *mipi_tx_module;

    mipi_tx_module = &mipi_tx_module_storage;

    mipi_tx_hw_destroy(mipi_tx_module->hw);
    pool_bm_destroy(mipi_tx_module->requests_pool);
    pool_bm_destroy(mipi_tx_module->clients_pool);
    mipi_tx_module->created = 0;
}

int mipi_tx_create(mipi_tx_create_t *params)
{
    mipi_tx_module_t *mipi_tx_module;

    mipi_tx_module = &mipi_tx_module_storage;
    mipi_tx_module->num_lanes = params->num_lanes;
    mipi_tx_module->mipi_clock = params->mipi_clock;
    mipi_tx_module->use_irq = params->use_irq;
    mipi_tx_module->state = MIPI_TX_STATE__IDLE;
    INIT_LIST_HEAD(&mipi_tx_module->requests);

    mipi_tx_module->clients_pool = pool_bm_create(
            "MIPI Tx clients pool",
            sizeof (client_storage[0]),
            ARRAY_SIZE(client_storage),
            client_storage
        );
    if (!mipi_tx_module->clients_pool) {
        printf(
                "Failed to create Clients Pool: elem_size=%zu, count=%d!\n",
                sizeof (client_storage[0]),
                ARRAY_SIZE(client_storage)
            );
        goto exit1;
    }

    mipi_tx_module->requests_pool = pool_bm_create(
            "MIPI Tx clients pool",
            sizeof (request_storage[0]),
            ARRAY_SIZE(request_storage),
            request_storage
        );
    if (!mipi_tx_module->requests_pool) {
        printf(
                "Failed to create Request Pool: elem_size=%zu, count=%d!\n",
                sizeof (request_storage[0]),
                ARRAY_SIZE(request_storage)
            );
        goto exit2;
    }

    mipi_tx_module->hw = mipi_tx_hw_create(
            mipi_tx_module->num_lanes,
            mipi_tx_module->mipi_clock,
            mipi_tx_module->use_irq,
            hw_sent_callback
        );
    if (!mipi_tx_module->hw) {
        printf("Failed to create MIPI Tx HW!\n");
        goto exit3;
    }

    mipi_tx_module->created = 1;

    return 0;
exit3:
    pool_bm_destroy(mipi_tx_module->requests_pool);
exit2:
    pool_bm_destroy(mipi_tx_module->clients_pool);
exit1:
    return -1;
}

