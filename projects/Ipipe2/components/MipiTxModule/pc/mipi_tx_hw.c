///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// -----------------------------------------------------------------------------
///
/// Revision History
/// ===================================
/// 05-Nov-2014 : Author ( MM Solutions AD )
/// Created
/// =============================================================================

#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <pthread.h>
#include <semaphore.h>
#include <profile.h>
#include <profile_ids.h>
#include <mipi_tx_module.h>
#include "mipi_tx_hw.h"

struct mipi_tx_hw {
    int num_lanes;
    int mipi_clock;
    mipi_tx_hw_sent_callback_t *sent_callback;
    void *client_prv;
    pthread_t thread_isr;
    sem_t sem_isr;
    int thread_isr_exit;
    int in_send;
};

static mipi_tx_hw_t mipi_tx_hw_storage;

static void * thread_isr(void *arg)
{
    mipi_tx_hw_t *mipi_tx_hw;

    mipi_tx_hw = arg;

    for (;;) {
        sem_wait(&mipi_tx_hw->sem_isr);
        if (mipi_tx_hw->thread_isr_exit) {
            break;
        }
        usleep(1000);
        mipi_tx_hw->in_send = 0;
        PROFILE_ADD(PROFILE_ID_MIPI_TX_HW_ISR, (uintptr_t)mipi_tx_hw->client_prv & 0xFFFFFFFF, 0);
        mipi_tx_hw->sent_callback(mipi_tx_hw, mipi_tx_hw->client_prv);
    }

    return NULL;
}

void mipi_tx_hw_poll(mipi_tx_hw_t *mipi_tx_hw)
{
}

int mipi_tx_hw_send(
        mipi_tx_hw_t *mipi_tx_hw,
        void *client_prv,
        mipi_tx_hw_frame_t *frame
    )
{
    assert(!mipi_tx_hw->in_send);
    mipi_tx_hw->in_send = 1;
    mipi_tx_hw->client_prv = client_prv;
    PROFILE_ADD(PROFILE_ID_MIPI_TX_HW_SEND_W_H, frame->width, frame->height[1]);
    PROFILE_ADD(PROFILE_ID_MIPI_TX_HW_SEND_T_C, frame->data_type[1], ((mipi_tx_header_t *)frame->p[0])->chunk);
    sem_post(&mipi_tx_hw->sem_isr);
    return 0;
}

void mipi_tx_hw_stop(mipi_tx_hw_t *mipi_tx_hw)
{
    PROFILE_ADD(PROFILE_ID_MIPI_TX_HW_STOP, 0, 0);
}

int mipi_tx_hw_start(mipi_tx_hw_t *mipi_tx_hw)
{
    PROFILE_ADD(PROFILE_ID_MIPI_TX_HW_START, 0, 0);
    return 0;
}

void mipi_tx_hw_destroy(mipi_tx_hw_t *mipi_tx_hw)
{
    mipi_tx_hw->thread_isr_exit = 1;
    sem_post(&mipi_tx_hw->sem_isr);
    pthread_join(mipi_tx_hw->thread_isr, NULL);
    sem_destroy(&mipi_tx_hw->sem_isr);
    PROFILE_ADD(PROFILE_ID_MIPI_TX_HW_DESTROY, 0, 0);
}

mipi_tx_hw_t *mipi_tx_hw_create(
        int num_lanes,
        int mipi_clock,
        int use_irq,
        mipi_tx_hw_sent_callback_t *sent_callback
    )
{
    mipi_tx_hw_t *mipi_tx_hw;
    int err;

    PROFILE_ADD(PROFILE_ID_MIPI_TX_HW_CREATE, 0, 0);

    mipi_tx_hw = &mipi_tx_hw_storage;

    mipi_tx_hw->num_lanes = num_lanes;
    mipi_tx_hw->mipi_clock = mipi_clock;
    mipi_tx_hw->sent_callback = sent_callback;

    mipi_tx_hw->client_prv = NULL;
    mipi_tx_hw->thread_isr_exit = 0;
    mipi_tx_hw->in_send = 0;

    sem_init(&mipi_tx_hw->sem_isr, 0, 0);

    err = pthread_create(
            &mipi_tx_hw->thread_isr,
            NULL,
            thread_isr,
            mipi_tx_hw
        );
    if (err) {
        printf("Failed to create thread!\n");
        goto exit1;
    }

    return mipi_tx_hw;
exit1:
    return NULL;
}

