///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// -----------------------------------------------------------------------------
///
/// Revision History
/// ===================================
/// 05-Nov-2014 : Author ( MM Solutions AD )
/// Created
/// =============================================================================

#ifndef _CRITICAL_SECTION_PC_H
#define _CRITICAL_SECTION_PC_H

#ifdef __cplusplus
extern "C" {
#endif

void critical_section_pc_init(void);
void critical_section_pc_deinit(void);

#ifdef __cplusplus
}
#endif

#endif /* _CRITICAL_SECTION_PC_H */

