/**************************************************************************************************

 @File         : dumpISPConfigToBinary.h
 @Author       : MT
 Date          : 01 - March - 2016
 E-mail        : xxx.xxx@movidius.com
 Copyright     : � Movidius Ltd 2016

 Description :
    Dumps Opipe configuration into binary format.
    Loads Opipe configuration from binary format.
 **************************************************************************************************/

#ifndef DUMPISPCONFIG_H_
#define DUMPISPCONFIG_H_

#include "Opipe.h"
#include "OpipeApps.h"
#include <string.h>

#define FILE_DUMP_BUFFER_MAX_SIZE    600 * 1024
#define FILE_NAME_MAX_SIZE                  256

typedef struct
{
    uint8_t fileName[FILE_NAME_MAX_SIZE];
    uint32_t  u32BayerWidth;
    uint32_t  u32BayerHeight;
    uint32_t  u32SkipLines;
    uint32_t  u32Bits;
    uint32_t  bayerOrder;
} generalData_s;

typedef void (*ispCopyDoneCallback) ();

void loadConfiguration(Opipe *pOpipe, char * pFileName, char * pBuff);
void dumpConfiguration(Opipe *pOpipe, char * pFileName, char * pBuff);

#endif //DUMPISPCONFIG_H_
