#include <math.h>

#include "common/leon/ipipe.h"
#include "plugins/arch/ma2100/common/IspCommon/leon/IspCommon.h"

static float
f16_to_f32(uint16_t h)
{
    int         sign = ((h>>15) & 1) ? -1 : 1;
    int         exp  = ((h >> 10) & 0x1f);
    unsigned    mant = h & 0x3ff;
    float       f;

    switch (exp) {
      case 31:
        if (mant == 0) {
            return (1.0f/0.0f) * sign;    /* +/- Infinity */
        } else {
            return (0.0f/0.0f) * -sign;   /* +/- nan */
        }
        break;
      case 0:
        return 0;
    }

    exp -= 15;

    f = sign * (1.0 + (float)mant / (1<<10));
    if (exp < 0) {
        return f / (1<<-exp);
    } else {
        return f * (1<<exp);
    }
}

static uint16_t
f32_to_f16(float f)
{
    uint32_t    u32 = *(uint32_t *)&f;
    int         sign = ((u32>>31) & 1) ? -1 : 1;
    int         exp  = ((u32 >> 23) & 0xff);
    unsigned    mant = u32 & 0x7fffff;

    switch (exp) {
      case 0xff:
        if (mant == 0) {
            return 0x7c00 | ((u32>>16) & 0x8000);   /* +/- Infinity */
        } else {
            return 0x7fff | ((u32>>16) & 0x8000);   /* +/- nan */
        }
        break;
      case 0:
        return 0;
    }

    exp -= 127-15;
    if (exp > 30) {
        exp = 30;
    }
    if (exp < 0) {
        exp = 0;
    }

    return exp << 10 | mant >> (23-10) | ((u32>>16) & 0x8000);
}

static float
lerp(float x0, float x1, float alpha, float min, float max)
{
    float x = x0 + alpha * (x1 - x0);
    
    if (x < min) {
        x = min;
    }
    if (x > max) {
        x = max;
    }

    return x;
}

static int
lerpi(float x0, float x1, float alpha, int min, int max)
{
    float   x = x0 + alpha * (x1 - x0);
    int     i;

    /* Round to nearest (default C rounding is round toward 0) */
    if (x < 0) {
        i = x - .5;
    } else {
        i = x + .5;
    }

    if (i < min) {
        i = min;
    }
    if (i > max) {
        i = max;
    }

    return i;
}

static inline unsigned
clampu(unsigned x, unsigned a, unsigned b)
{
    return x < a ? a : (x > b ? b : x);
}

static uint32_t
luma_denoise_interp_f2(uint32_t c0, uint32_t c1, float alpha)
{
    int c0i, c1i, c, c_out = 0;
    int i;

    /*
     * Convert kernel co-efficients from log2 values to integers, interpolate,
     * then convert back to log2 bit-packed form.
     */
    for (i = 0; i < 16; i++) {
        c0i = 1 << ((c0 >> (i*2)) & 3);
        c1i = 1 << ((c1 >> (i*2)) & 3);
        c = clampu(c0i * (1.0-alpha) + c1i * alpha, 0, 8);
        if (c <= 1) {
            c = 0;
        } else if (c <= 2) {
            c = 1;
        } else if (c <= 4) {
            c = 2;
        } else if (c <= 8) {
            c = 3;
        }
        c_out |= c << (i*2);
    }

    return c_out;
}

static void
luma_denoise_gen_lut_bitpos(icIspConfig *conf, float strength)
{
    uint8_t     *lut = conf->lumaDenoise.lut;
    uint32_t    *bitpos = &conf->lumaDenoise.bitpos;
    float       alpha, divisor, sigma, npot, l;
    int         i;

    if (strength < .001f) {
        /* Avoid division by 0 */
        strength = .001f;
    }
    if (strength > 2047.0f) {
        /* Limit to prevent 'bitpos' > 11 */
        strength = 2047.0f;
    }
    *bitpos = (int)floorf(log2f(strength));   /* MSB position */
    if (*bitpos < 0) {
        *bitpos = 0;
    }
    npot = (float)(1<<*bitpos);             /* nearest power of two (rounding down) */
    alpha = (strength - npot) / npot;
    divisor = 4*(1-alpha) + 8*(alpha);
    sigma = .05f;
    if (strength < 1) {
        /* Reduce sigma when 0 < strength < 1 */
        sigma = sigma * strength;
    }
    for (i = 0; i < 32; i++) {
        l = (float)i / 31 / divisor;
        l = expf(-(powf(l, 2)) / (2*(powf(sigma,2))));    /* Gaussian */

        /* LUT entries will be quantized to 16 possible values */
        lut[i] = (uint8_t)(l*16-1 + .5f);
    }
}

static void
luma_denoise_ref_gen_lut_dist(icIspConfig *conf,
  float angle_of_view, int image_width, int image_height)
{
    float   angle;
    int     w2 = image_width/2;
    int     h2 = image_height/2;
    int     maxval, shift, dx2_plus_dy2;
    int     i;
    float   cornerval;
    uint8_t *lut = conf->lumaDenoiseRef.lutDist;

	/* Get half the angle, in radians */
    angle = (angle_of_view/2) / 360 * 2*M_PI;

	/* Maximum value that can be put into LUT */
    maxval = w2*w2 + h2*h2;

    /*
     * Find how many bits to shift values right by, so that we can use
	 * an 8-bit LUT.
     */
	shift = floorf(log2f(maxval)) + 1 - 8;
	cornerval = sqrtf(w2*w2+h2*h2);

    /*
     * LUT is indexed at runtime by (dx^2+dy^2) >> shift, where dx, dy
     * is the x/y displacement of the current pixel from the image centre.
     */
    for (i = 0; i < 255; i++) {
        dx2_plus_dy2 = i << shift;
        lut[i] = powf(cosf(sqrtf(dx2_plus_dy2) / cornerval * angle), 4) * 255 + .5;
    }

    conf->lumaDenoiseRef.shift = shift;
}

static void
luma_denoise_ref_gen_lut_gamma(icIspConfig *conf, float gamma)
{
    uint8_t *lut = conf->lumaDenoiseRef.lutGamma;
    int     i;

    for (i = 0; i < 256; i++) {
        lut[i] = powf((float)i / 255, gamma) * 255 + .5;
    }
}

static void
sharpen_gen_coeffs(float sigma, uint16_t *coeffs)
{
    int     i, j;
    float   denom = 2 * sigma*sigma;
    float   k[4], sum = 0;

    for (i = 0; i < 4; i++) {
        j = 3 - i;
        k[i] = expf(-(float)(j*j) / denom);
        sum += k[i];
    }

    // Coefficients should sum to 1
    sum = sum + (sum - k[3]);
    for (i = 0; i < 4; i++) {
        coeffs[i] = f32_to_f16(k[i] / sum);
    }
}

/* Interpolate "Horizontal Enables" in Chroma Denoise filter */
static unsigned
interp_hEnab(unsigned e0, unsigned e1, float alpha)
{
    int kw0, kw1, kw;

    /* Compute kernel widths */
    kw0 = (e0 & 1) * 23 + ((e0>>1) & 1) * 17 + ((e0>>2) & 1) * 13;
    kw1 = (e1 & 1) * 23 + ((e1>>1) & 1) * 17 + ((e1>>2) & 1) * 13;

    kw = clampu(kw0 * (1.0-alpha) + kw1 * alpha, 0, 53);

    if (kw <= 13) {
        return 4;
    } else if (kw <= 17) {
        return 2;
    } else if (kw <= 23) {
        return 1;
    } else if (kw <= 30) {
        return 6;
    } else if (kw <= 36) {
        return 5;
    } else if (kw <= 40) {
        return 3;
    } else {
        return 7;
    }
}

static unsigned
interp_median_ksize(unsigned k0, unsigned k1, float alpha)
{
    int    k;

    k = clampu(k0 * (1.0-alpha) + k1 * alpha + 0.5, 0, 7);
    k = ((k-1) / 2) * 2 + 1;
    return  k < 3 ? 0 : k;
}

/*
 * "gain_lo" is the gain (sensor analog gain times digital gain) at which
 * the first set of parameters (cfg0) were tuned.  "gain_hi" is the gain at
 * which the second set of parameters (cfg1) were tuned.  "gain_current"
 * is the current gain being applied, and must be provided by the AE
 * algorithm.
 * 
 * The calling function *must* set cfgOut->lsc.pLscTable to point to a memory
 * area of sufficient size to store the interpolated Lens Shading table,
 * before calling this function.  Similarly for cfgOut->gamma.table.
 */
void
interp_configs(icIspConfig *cfg0, icIspConfig *cfg1, icIspConfig *cfgOut, 
  float gain_lo, float gain_hi, float gain_current, int width, int height)
{
    float   alpha = (gain_lo - gain_current) / (gain_lo - gain_hi);
    int     bpp = cfg0->pipelineBits;
    int     maxval = (1<<cfg0->pipelineBits)-1;
    int     x, y, i;
    int     fp16_max = 65504;
    int     offset;

    /*
     * Copy some parameters which are assumed to be identical in both
     * input configs.
     */
    cfgOut->bayerOrder = cfg0->bayerOrder;
    cfgOut->pipelineBits = bpp;
    cfgOut->raw.outputBits = cfg0->raw.outputBits;
    cfgOut->lsc.lscWidth = cfg0->lsc.lscWidth;
    cfgOut->lsc.lscHeight = cfg0->lsc.lscHeight;
    cfgOut->lsc.lscStride = cfg0->lsc.lscStride;
    cfgOut->aeAwbConfig = cfg0->aeAwbConfig;
    cfgOut->afConfig = cfg0->afConfig;
    cfgOut->gamma.rangetop = cfg0->gamma.rangetop;
    cfgOut->gamma.size = cfg0->gamma.size;

    cfgOut->blc.gr = lerpi(cfg0->blc.gr, cfg1->blc.gr, alpha, 0, maxval);
    cfgOut->blc.r  = lerpi(cfg0->blc.r,  cfg1->blc.r,  alpha, 0, maxval);
    cfgOut->blc.b  = lerpi(cfg0->blc.b,  cfg1->blc.b,  alpha, 0, maxval);
    cfgOut->blc.gb = lerpi(cfg0->blc.gb, cfg1->blc.gb, alpha, 0, maxval);

    cfgOut->raw.gainGr = lerpi(cfg0->raw.gainGr, cfg1->raw.gainGr, alpha, 0, 0xffff);
    cfgOut->raw.gainR  = lerpi(cfg0->raw.gainR,  cfg1->raw.gainR,  alpha, 0, 0xffff);
    cfgOut->raw.gainB  = lerpi(cfg0->raw.gainB,  cfg1->raw.gainB,  alpha, 0, 0xffff);
    cfgOut->raw.gainGb = lerpi(cfg0->raw.gainGb, cfg1->raw.gainGb, alpha, 0, 0xffff);

    cfgOut->raw.clampGr = lerpi(cfg0->raw.clampGr, cfg1->raw.clampGr, alpha, 0, 0xffff);
    cfgOut->raw.clampR  = lerpi(cfg0->raw.clampR,  cfg1->raw.clampR,  alpha, 0, 0xffff);
    cfgOut->raw.clampB  = lerpi(cfg0->raw.clampB,  cfg1->raw.clampB,  alpha, 0, 0xffff);
    cfgOut->raw.clampGb = lerpi(cfg0->raw.clampGb, cfg1->raw.clampGb, alpha, 0, 0xffff);

    cfgOut->raw.grgbImbalPlatDark = lerpi(cfg0->raw.grgbImbalPlatDark, cfg1->raw.grgbImbalPlatDark, alpha, 0, 0x3fff);
    cfgOut->raw.grgbImbalDecayDark  = lerpi(cfg0->raw.grgbImbalDecayDark,  cfg1->raw.grgbImbalDecayDark,  alpha, 0, 0x3fff);
    cfgOut->raw.grgbImbalPlatBright  = lerpi(cfg0->raw.grgbImbalPlatBright,  cfg1->raw.grgbImbalPlatBright,  alpha, 0, 0x3fff);
    cfgOut->raw.grgbImbalDecayBright = lerpi(cfg0->raw.grgbImbalDecayBright, cfg1->raw.grgbImbalDecayBright, alpha, 0, 0x3fff);
    cfgOut->raw.grgbImbalThr = lerpi(cfg0->raw.grgbImbalThr, cfg1->raw.grgbImbalThr, alpha, 0, 255);

    cfgOut->raw.dpcAlphaHotG   = lerpi(cfg0->raw.dpcAlphaHotG,   cfg1->raw.dpcAlphaHotG,   alpha, 0, 15);
    cfgOut->raw.dpcAlphaHotRb  = lerpi(cfg0->raw.dpcAlphaHotRb,  cfg1->raw.dpcAlphaHotRb,  alpha, 0, 15);
    cfgOut->raw.dpcAlphaColdG  = lerpi(cfg0->raw.dpcAlphaColdG,  cfg1->raw.dpcAlphaColdG,  alpha, 0, 15);
    cfgOut->raw.dpcAlphaColdRb = lerpi(cfg0->raw.dpcAlphaColdRb, cfg1->raw.dpcAlphaColdRb, alpha, 0, 15);
    cfgOut->raw.dpcNoiseLevel  = lerpi(cfg0->raw.dpcNoiseLevel,  cfg1->raw.dpcNoiseLevel,  alpha, 0, 0xffff);

    for (y = 0; y < cfgOut->lsc.lscHeight; y++) {
        for (x = 0; x < cfgOut->lsc.lscWidth; x++) {
            offset = y*cfgOut->lsc.lscStride + x;
            cfgOut->lsc.pLscTable[offset] =
              lerpi(cfg0->lsc.pLscTable[offset],
              cfg1->lsc.pLscTable[offset], alpha, 0, 0xffff);
        }
    }

    cfgOut->wbGains.gainR = lerpi(cfg0->wbGains.gainR, cfg1->wbGains.gainR, alpha, 1, 0xffff);
    cfgOut->wbGains.gainG = lerpi(cfg0->wbGains.gainG, cfg1->wbGains.gainG, alpha, 0, 0xffff);
    cfgOut->wbGains.gainB = lerpi(cfg0->wbGains.gainB, cfg1->wbGains.gainB, alpha, 0, 0xffff);

    cfgOut->demosaic.dewormGradientMul = lerpi(cfg0->demosaic.dewormGradientMul, cfg1->demosaic.dewormGradientMul, alpha, 0, 255);
    cfgOut->demosaic.dewormSlope = lerpi(cfg0->demosaic.dewormSlope, cfg1->demosaic.dewormSlope, alpha, 0, 0xffff);
    cfgOut->demosaic.dewormOffset = lerpi(cfg0->demosaic.dewormOffset, cfg1->demosaic.dewormOffset, alpha, -32768, 32767);

    cfgOut->chromaGen.epsilon = lerp(cfg0->chromaGen.epsilon, cfg1->chromaGen.epsilon, alpha, 1.0/maxval, 5);
    cfgOut->chromaGen.scaleR = lerp(cfg0->chromaGen.scaleR, cfg1->chromaGen.scaleR, alpha, 1, 2.5);
    cfgOut->chromaGen.scaleG = lerp(cfg0->chromaGen.scaleG, cfg1->chromaGen.scaleG, alpha, 1, 2.5);
    cfgOut->chromaGen.scaleB = lerp(cfg0->chromaGen.scaleB, cfg1->chromaGen.scaleB, alpha, 1, 2.5);

    cfgOut->purpleFlare.strength = lerpi(cfg0->purpleFlare.strength, cfg1->purpleFlare.strength, alpha, 0, 20);

    cfgOut->lumaDenoise.alpha = lerpi(cfg0->lumaDenoise.alpha, cfg1->lumaDenoise.alpha, alpha, 0, 255);
    cfgOut->lumaDenoise.strength = lerp(cfg0->lumaDenoise.strength, cfg1->lumaDenoise.strength, alpha, 0, 2047);
    cfgOut->lumaDenoise.f2 = luma_denoise_interp_f2(cfg0->lumaDenoise.f2, cfg1->lumaDenoise.f2, alpha);
    luma_denoise_gen_lut_bitpos(cfgOut, cfgOut->lumaDenoise.strength);

    cfgOut->lumaDenoiseRef.angle_of_view = lerp(cfg0->lumaDenoiseRef.angle_of_view, cfg1->lumaDenoiseRef.angle_of_view, alpha, 0, 359);
    cfgOut->lumaDenoiseRef.gamma = lerp(cfg0->lumaDenoiseRef.gamma, cfg1->lumaDenoiseRef.gamma, alpha, 0, 1);
    luma_denoise_ref_gen_lut_dist(cfgOut, cfgOut->lumaDenoiseRef.angle_of_view, width, height);
    luma_denoise_ref_gen_lut_gamma(cfgOut, cfgOut->lumaDenoiseRef.gamma);

    cfgOut->randomNoise.strength = lerp(cfg0->randomNoise.strength, cfg1->randomNoise.strength, alpha, 0, fp16_max);

    for (y = 0; y < 16; y++) {
        for (x = 0; x < 10; x++) {
            offset = y*10 + x;
            cfgOut->ltm.curves[offset] =
              f32_to_f16(lerp(f16_to_f32(cfg0->ltm.curves[offset]),
              f16_to_f32(cfg1->ltm.curves[offset]), alpha, 0, 0xffff));
        }
    }

    cfgOut->ltmLBFilter.th1 = lerpi(cfg0->ltmLBFilter.th1, cfg1->ltmLBFilter.th1, alpha, 0, 255);
    cfgOut->ltmLBFilter.th2 = lerpi(cfg0->ltmLBFilter.th2, cfg1->ltmLBFilter.th2, alpha, 0, 255);
    cfgOut->ltmLBFilter.limit = lerpi(cfg0->ltmLBFilter.limit, cfg1->ltmLBFilter.limit, alpha, 0, 255);

    cfgOut->chromaDenoise.th_r = lerpi(cfg0->chromaDenoise.th_r, cfg1->chromaDenoise.th_r, alpha, 0, 255);
    cfgOut->chromaDenoise.th_g = lerpi(cfg0->chromaDenoise.th_g, cfg1->chromaDenoise.th_g, alpha, 0, 255);
    cfgOut->chromaDenoise.th_b = lerpi(cfg0->chromaDenoise.th_b, cfg1->chromaDenoise.th_b, alpha, 0, 255);
    cfgOut->chromaDenoise.limit = lerpi(cfg0->chromaDenoise.limit, cfg1->chromaDenoise.limit, alpha, 0, 255);
    cfgOut->chromaDenoise.hEnab = interp_hEnab(cfg0->chromaDenoise.hEnab, cfg1->chromaDenoise.hEnab, alpha);

    cfgOut->median.kernelSize = interp_median_ksize(cfg0->median.kernelSize, cfg1->median.kernelSize, alpha);

    cfgOut->medianMix.slope = lerp(cfg0->medianMix.slope, cfg1->medianMix.slope, alpha, 0, 255);
    cfgOut->medianMix.offset = lerp(cfg0->medianMix.offset, cfg1->medianMix.offset, alpha, -1, 1);

    cfgOut->lowpass.coefs[0] = lerp(cfg0->lowpass.coefs[0], cfg1->lowpass.coefs[0], alpha, 0, 1);
    cfgOut->lowpass.coefs[1] = lerp(cfg0->lowpass.coefs[1], cfg1->lowpass.coefs[1], alpha, 0, 1);

    cfgOut->greyDesat.slope = lerpi(cfg0->greyDesat.slope, cfg1->greyDesat.slope, alpha, 0, 255);
    cfgOut->greyDesat.offset = lerpi(cfg0->greyDesat.offset, cfg1->greyDesat.offset, alpha, -128, 127);
    cfgOut->greyDesat.grey_cr = lerpi(cfg0->greyDesat.grey_cr, cfg1->greyDesat.grey_cr, alpha, 0, 255);
    cfgOut->greyDesat.grey_cg = lerpi(cfg0->greyDesat.grey_cg, cfg1->greyDesat.grey_cg, alpha, 0, 255);
    cfgOut->greyDesat.grey_cb = lerpi(cfg0->greyDesat.grey_cb, cfg1->greyDesat.grey_cb, alpha, 0, 255);

    cfgOut->sharpen.sigma = lerp(cfg0->sharpen.sigma, cfg1->sharpen.sigma, alpha, 0, 1000);
    sharpen_gen_coeffs(cfgOut->sharpen.sigma, cfgOut->sharpen.sharpen_coeffs);
    cfgOut->sharpen.strength_darken = f32_to_f16(lerp(f16_to_f32(cfg0->sharpen.strength_darken), f16_to_f32(cfg1->sharpen.strength_darken), alpha, 0, fp16_max));
    cfgOut->sharpen.strength_lighten = f32_to_f16(lerp(f16_to_f32(cfg0->sharpen.strength_lighten), f16_to_f32(cfg1->sharpen.strength_lighten), alpha, 0, fp16_max));
    cfgOut->sharpen.alpha = f32_to_f16(lerp(f16_to_f32(cfg0->sharpen.alpha), f16_to_f32(cfg1->sharpen.alpha), alpha, 0, 1));
    cfgOut->sharpen.undershoot = f32_to_f16(lerp(f16_to_f32(cfg0->sharpen.undershoot), f16_to_f32(cfg1->sharpen.undershoot), alpha, 0, 1));
    cfgOut->sharpen.overshoot = f32_to_f16(lerp(f16_to_f32(cfg0->sharpen.overshoot), f16_to_f32(cfg1->sharpen.overshoot), alpha, 1, 2));
    cfgOut->sharpen.rangeStop0 = f32_to_f16(lerp(f16_to_f32(cfg0->sharpen.rangeStop0), f16_to_f32(cfg1->sharpen.rangeStop0), alpha, 0, 1));
    cfgOut->sharpen.rangeStop1 = f32_to_f16(lerp(f16_to_f32(cfg0->sharpen.rangeStop0), f16_to_f32(cfg1->sharpen.rangeStop0), alpha, 0, 1));
    cfgOut->sharpen.rangeStop2 = f32_to_f16(lerp(f16_to_f32(cfg0->sharpen.rangeStop2), f16_to_f32(cfg1->sharpen.rangeStop2), alpha, 0, 1));
    cfgOut->sharpen.rangeStop3 = f32_to_f16(lerp(f16_to_f32(cfg0->sharpen.rangeStop3), f16_to_f32(cfg1->sharpen.rangeStop3), alpha, 0, 1));
    cfgOut->sharpen.minThr = f32_to_f16(lerp(f16_to_f32(cfg0->sharpen.minThr), f16_to_f32(cfg1->sharpen.minThr), alpha, 0, fp16_max));

    cfgOut->colorCombine.desat_mul = lerp(cfg0->colorCombine.desat_mul, cfg1->colorCombine.desat_mul, alpha, 0, 255);
    cfgOut->colorCombine.desat_t1 = lerp(cfg0->colorCombine.desat_t1, cfg1->colorCombine.desat_t1, alpha, 0, 1);
    for (i = 0; i < 9; i++) {
        cfgOut->colorCombine.ccm[i] = lerp(cfg0->colorCombine.ccm[i], cfg1->colorCombine.ccm[i], alpha, -32, 31.999);
    }

    for (i = 0; i < cfgOut->gamma.size*4; i++) {
        cfgOut->gamma.table[i] = f32_to_f16(lerp(f16_to_f32(cfg0->gamma.table[i]), f16_to_f32(cfg1->gamma.table[i]), alpha, 0, 1));
    }

    for (i = 0; i < 9; i++) {
        cfgOut->colorConvert.mat[i] = lerp(cfg0->colorConvert.mat[i], cfg1->colorConvert.mat[i], alpha, -fp16_max, fp16_max);
    }
    for (i = 0; i < 3; i++) {
        cfgOut->colorConvert.offset[i] = lerp(cfg0->colorConvert.offset[i], cfg1->colorConvert.offset[i], alpha, -fp16_max, fp16_max);
    }
}
