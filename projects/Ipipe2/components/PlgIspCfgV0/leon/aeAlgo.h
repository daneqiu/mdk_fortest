/// =====================================================================================
///
///        @file:      aeAlgo.h
///        @brief:     simpleMonoAE Algorithm API
///        @author:    csoka, attila.csok@movidius.com
///        @copyright: All code copyright Movidius Ltd 2015, all rights reserved.
///                  For License Warranty see: common/license.txt
/// =====================================================================================
///

#ifndef AE_ALGO_H
#define AE_ALGO_H

/// System Includes
/// -------------------------------------------------------------------------------------
#include <inttypes.h>

/// Source Specific #defines and types (typedef,enum,struct)
/// -------------------------------------------------------------------------------------


typedef struct aeExposure {
    unsigned integrationTime;
    unsigned gainCode;
} aeExposure_t;

typedef struct {
    uint32_t AE_STATS_START_X;
    uint32_t AE_STATS_START_Y;
    uint32_t AE_STATS_SKIP_X;
    uint32_t AE_STATS_SKIP_Y;
    uint32_t AE_STATS_PATCH_H;
    uint32_t AE_STATS_PATCH_W;
    uint32_t AE_STATS_PATCHES_H;
    uint32_t AE_STATS_PATCHES_V;
} AeAwbPatchConfigs;

/// Function Prototypes
/// -------------------------------------------------------------------------------------

/// ===  FUNCTION  ======================================================================
///  Name:  aeGetNewExposure
///  Description: main function of exposure calculation
/// =====================================================================================
void aeGetNewExposure(uint32_t* statPatchesBuffer, AeAwbPatchConfigs *aeAwbPatchConfigs, float *gain, aeExposure_t *exposureOut);

#endif /// AE_ALGO_H
