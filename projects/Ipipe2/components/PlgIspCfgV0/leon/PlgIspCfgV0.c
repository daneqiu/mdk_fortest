
#include "common/shared/IcTypes.h"
#include "components/FrameMgr/leon/FrameMgrApi.h"
#include "plugins/arch/ma2100/common/IspCommon/leon/IspCommon.h"
#include "components/PlgIspCfgV0/leon/PlgIspCfgV0Api.h"
#include "components/PlgIspCfgV0/leon/aeAlgo.h"
// stl
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <semaphore.h>

/*
 * Internal debug code, disabled by default.
 */
//#define ENABLE_DBG_LOCAL_ae
#ifdef ENABLE_DBG_LOCAL_ae
#define DBG_MAX_MESSAGE_ae 8096
static volatile uint32_t dbgMsgBuf_ae[DBG_MAX_MESSAGE_ae];
static volatile uint32_t dbgMsgIdx_ae = 0;
static inline void dbgMAddMes_ae(uint32_t mesage) {
    dbgMsgBuf_ae[dbgMsgIdx_ae] = mesage;
    dbgMsgIdx_ae++;
    assert(dbgMsgIdx_ae < DBG_MAX_MESSAGE_ae);
    //if(dbgMsgIdx == DBG_MAX_MESSAGE) {
    //    dbgMsgIdx = 0;
    //}
}
#else
#define dbgMAddMes_ae(X)
#endif


static void *statsProc(void *objIsp);
static void *IspParamInit(IspDataBase *dataBaseStruct, void *objIsp);
static void *IspParamFini(void *objIsp);
static icIspConfig* ispParamsGet(void *objIsp);
static void ispParamSendStats(uint32_t userData, void *objIsp);
static void convertAndPopulateFinalIsp(void *objIsp);
void paramsUpdateInit(void *objIsp);
void paramsUpdateRun(void *objIsp);

void PlgIspCfgV0Create(void *objIsp) {
    PlgIspCfgV0 *ispInstance = (PlgIspCfgV0*)objIsp;
    int i = 0;
    // clear the buffers
    memset((void*)ispInstance, 0, sizeof(PlgIspCfgV0));
    ispInstance->procIspCfg.ispCfg.lsc.pLscTable = ispInstance->procIspCfg.lsc;
    ispInstance->procIspCfg.ispCfg.gamma.table   = ispInstance->procIspCfg.gamma;
    //printf(" procIspCfg %p, %p, %p \n", &ispInstance->procIspCfg.ispCfg, ispInstance->procIspCfg.ispCfg.lsc.pLscTable, ispInstance->procIspCfg.ispCfg.gamma.table);

    ispInstance->lastIspCfg.ispCfg.lsc.pLscTable = ispInstance->lastIspCfg.lsc;
    ispInstance->lastIspCfg.ispCfg.gamma.table   = ispInstance->lastIspCfg.gamma;
    //printf(" lastIspCfg %p, %p, %p \n", &ispInstance->lastIspCfg.ispCfg, ispInstance->lastIspCfg.ispCfg.lsc.pLscTable, ispInstance->lastIspCfg.ispCfg.gamma.table);
    for(i = 0; i < MAX_ISP_CFG_BUF; i++ ) {
        ispInstance->locIspCfg[i].ispCfg.lsc.pLscTable = ispInstance->locIspCfg[i].lsc;
        ispInstance->locIspCfg[i].ispCfg.gamma.table   = ispInstance->locIspCfg[i].gamma;
        ispInstance->locIspCfg[i].ispCfg.aeAwbStats    = &ispInstance->locIspCfg[i].locAeAwbBufs;
        //printf(" locIspCfg[%d] %p, %p, %p \n",i,  &ispInstance->locIspCfg[i].ispCfg, ispInstance->locIspCfg[i].ispCfg.lsc.pLscTable, ispInstance->locIspCfg[i].ispCfg.gamma.table);
    }
    ispInstance->statsProc      = statsProc;
    ispInstance->init           = IspParamInit;
    ispInstance->fini           = IspParamFini;
    ispInstance->getParams      = ispParamsGet;
    ispInstance->sendStats      = ispParamSendStats;
}

static void *statsProc(void *objIsp) {
    PlgIspCfgV0 *ispInstance = (PlgIspCfgV0*)objIsp;
    while(1) {
        assert(sem_wait(&ispInstance->semNewStatsCame) != -1);
        paramsUpdateRun(objIsp);
        convertAndPopulateFinalIsp(objIsp);
        ispInstance->last2AStats = NULL;
        if(ispInstance->turnOf) {
            return NULL;
        }
    }
    return NULL;
}
static void *IspParamInit(IspDataBase *dataBaseStruct, void *objIsp) {
    int        rc;
    PlgIspCfgV0 *ispInstance = (PlgIspCfgV0*)objIsp;
    if((rc = sem_init(&ispInstance->semNewStatsCame, 0, 0)) == -1)
        printf("pthread_create: %s\n", strerror(rc));
    assert(0 == pthread_mutex_init(&ispInstance->lock, NULL));
    ispInstance->dataBasePointer = dataBaseStruct;
    paramsUpdateInit(objIsp);
    convertAndPopulateFinalIsp(objIsp);
    assert(0 == pthread_create(&ispInstance->statsProcThread, NULL, ispInstance->statsProc, (void*)objIsp));
    return (NULL);
}
static void *IspParamFini(void *objIsp) {
    PlgIspCfgV0 *ispInstance = (PlgIspCfgV0*)objIsp;
    ispInstance->turnOf = 1;
    pthread_join(ispInstance->statsProcThread, NULL);
    pthread_mutex_destroy(&ispInstance->lock);
    sem_destroy(&ispInstance->semNewStatsCame);
    return (NULL);
}
static icIspConfig* ispParamsGet(void *objIsp) {
    PlgIspCfgV0 *ispInstance = (PlgIspCfgV0*)objIsp;
    icIspConfig* returnP = &ispInstance->locIspCfg[ispInstance->locIspCfgIdx].ispCfg;
    icAeAwbStats  *statsPointer = returnP->aeAwbStats;
    ispInstance->locIspCfgIdx++;
    if(MAX_ISP_CFG_BUF==ispInstance->locIspCfgIdx) ispInstance->locIspCfgIdx = 0;
    pthread_mutex_lock(&ispInstance->lock);
    memcpy((void*)returnP,                  (void*)&ispInstance->lastIspCfg.ispCfg,      sizeof(icIspConfig));
    memcpy((void*)returnP->lsc.pLscTable,   (void*)ispInstance->lastIspCfg.ispCfg.lsc.pLscTable,    MAX_LSC_TBL_SIZE * sizeof(uint16_t));
    memcpy((void*)returnP->gamma.table,     (void*)ispInstance->lastIspCfg.ispCfg.gamma.table,      MAX_GAMMA_TBL_SIZE * sizeof(uint16_t));
    returnP->aeAwbStats = statsPointer;
    pthread_mutex_unlock(&ispInstance->lock);
    returnP->userData = (void*)returnP;

    dbgMAddMes_ae(0x01);
    dbgMAddMes_ae(returnP->userData);
    dbgMAddMes_ae(returnP->aeAwbStats);
    dbgMAddMes_ae(0x01);
    return (returnP);
}

static void ispParamSendStats(uint32_t userData, void *objIsp) {
    PlgIspCfgV0 *ispInstance = (PlgIspCfgV0*)objIsp;
    if((NULL == ispInstance->last2AStats)&&(0 != userData)) {
        ispInstance->last2AStats = ((icIspConfig*)userData)->aeAwbStats;
        // kick thread if is not already running
        if(sem_post(&ispInstance->semNewStatsCame) == -1)  {/// Inform that lrt is ready
            printf("sem_post error\n");
        }
    }
}

static void convertAndPopulateFinalIsp(void *objIsp) {
    PlgIspCfgV0 *ispInstance = (PlgIspCfgV0*)objIsp;
    pthread_mutex_lock(&ispInstance->lock);
    memcpy((void*)&ispInstance->lastIspCfg.ispCfg,               (void*)&ispInstance->procIspCfg.ispCfg,                  sizeof(icIspConfig));
    memcpy((void*)ispInstance->lastIspCfg.ispCfg.lsc.pLscTable, (void*)ispInstance->procIspCfg.ispCfg.lsc.pLscTable,    MAX_LSC_TBL_SIZE * sizeof(uint16_t));
    memcpy((void*)ispInstance->lastIspCfg.ispCfg.gamma.table,   (void*)ispInstance->procIspCfg.ispCfg.gamma.table,      MAX_GAMMA_TBL_SIZE * sizeof(uint16_t));
    pthread_mutex_unlock(&ispInstance->lock);
    // extra setting lsc line need to be 8b aligned
    //TODO: write code here
}


extern void interp_configs(icIspConfig *cfg0, icIspConfig *cfg1, icIspConfig *cfgOut,
  float gain_lo, float gain_hi, float gain_current, int width, int height);

void paramsUpdateInit(void *objIsp) {
    PlgIspCfgV0 *ispInstance = (PlgIspCfgV0*)objIsp;
    //&ispInstance->dataBasePointer->ispCfgs[0];
    ispInstance->imgSize.w      = ispInstance->dataBasePointer->size.w;
    ispInstance->imgSize.h      = ispInstance->dataBasePointer->size.h;
    ispInstance->statsSize.w    = ispInstance->dataBasePointer->ispCfgs[0]->aeAwbConfig.nPatchesX;
    ispInstance->statsSize.h    = ispInstance->dataBasePointer->ispCfgs[0]->aeAwbConfig.nPatchesY;
    ispInstance->ispIntermGain  = (ispInstance->dataBasePointer->gain[0] +
            ispInstance->dataBasePointer->gain[1])/2.0f;

    ispInstance->aeAwbPatchConfigs.AE_STATS_START_X = ispInstance->dataBasePointer->ispCfgs[0]->aeAwbConfig.firstPatchX;
    ispInstance->aeAwbPatchConfigs.AE_STATS_START_Y = ispInstance->dataBasePointer->ispCfgs[0]->aeAwbConfig.firstPatchY;
    ispInstance->aeAwbPatchConfigs.AE_STATS_SKIP_X = ispInstance->dataBasePointer->ispCfgs[0]->aeAwbConfig.patchGapX;
    ispInstance->aeAwbPatchConfigs.AE_STATS_SKIP_Y = ispInstance->dataBasePointer->ispCfgs[0]->aeAwbConfig.patchGapY;
    ispInstance->aeAwbPatchConfigs.AE_STATS_PATCH_W = ispInstance->dataBasePointer->ispCfgs[0]->aeAwbConfig.patchWidth;
    ispInstance->aeAwbPatchConfigs.AE_STATS_PATCH_H = ispInstance->dataBasePointer->ispCfgs[0]->aeAwbConfig.patchHeight;
    ispInstance->aeAwbPatchConfigs.AE_STATS_PATCHES_H = ispInstance->dataBasePointer->ispCfgs[0]->aeAwbConfig.nPatchesX;
    ispInstance->aeAwbPatchConfigs.AE_STATS_PATCHES_V = ispInstance->dataBasePointer->ispCfgs[0]->aeAwbConfig.nPatchesY;

    interp_configs(ispInstance->dataBasePointer->ispCfgs[0], ispInstance->dataBasePointer->ispCfgs[1],
            &ispInstance->procIspCfg.ispCfg,
            ispInstance->dataBasePointer->gain[0], ispInstance->dataBasePointer->gain[1],
            ispInstance->ispIntermGain,
            ispInstance->imgSize.w, ispInstance->imgSize.h);

    icIspConfig *crtIspVal = &ispInstance->procIspCfg.ispCfg;
//    printf("save %p %d ispCfg.bin \n", crtIspVal, sizeof(icIspConfig));
//    printf("save %p %d lsc.bin \n", crtIspVal->lsc.pLscTable, 80*50*2);
//    printf("save %p %d gama.bin \n", crtIspVal->gamma.table, 512*4*2);
//    asm("ta 1\n");
}


void paramsUpdateRun(void *objIsp) {
    PlgIspCfgV0 *ispInstance = (PlgIspCfgV0*)objIsp;


    dbgMAddMes_ae(0x02);
    dbgMAddMes_ae(ispInstance->last2AStats);
    dbgMAddMes_ae(*((uint32_t*)(ispInstance->last2AStats)));
    dbgMAddMes_ae(0x02);
    //float demoGainRandom = 0.0f;
    //demoGainRandom = ispInstance->ispIntermGain;
    aeGetNewExposure((uint32_t*)ispInstance->last2AStats, &ispInstance->aeAwbPatchConfigs, &ispInstance->ispIntermGain, &ispInstance->sensorSetting);

    //printf("g: %f\n", ispInstance->ispIntermGain);
    if(ispInstance->ispIntermGain > ispInstance->dataBasePointer->gain[1]) {
        ispInstance->ispIntermGain = ispInstance->dataBasePointer->gain[1];
    }
    else {
        if(ispInstance->ispIntermGain < ispInstance->dataBasePointer->gain[0]) {
            ispInstance->ispIntermGain = ispInstance->dataBasePointer->gain[0];
        }
    }
    if(ispInstance->aeAlgoFinishCallbch) ispInstance->aeAlgoFinishCallbch(&ispInstance->sensorSetting);
    interp_configs(ispInstance->dataBasePointer->ispCfgs[0], ispInstance->dataBasePointer->ispCfgs[1],
            &ispInstance->procIspCfg.ispCfg,
            ispInstance->dataBasePointer->gain[0], ispInstance->dataBasePointer->gain[1],
            ispInstance->ispIntermGain,
            ispInstance->imgSize.w, ispInstance->imgSize.h);
}

// ae side
void aeInit(void *dataBaseStruct) {

}

// ae side
void awbInit(void *dataBaseStruct) {

}

void aeCalc(uint32_t* aeVal, void *dataBaseStruct) {

}

void awbCalc(uint32_t* colorTempVal, void *dataBaseStruct) {

}
