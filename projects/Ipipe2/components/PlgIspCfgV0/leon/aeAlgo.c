/// =====================================================================================
///
///        @file:      aeAlgo.c
///        @brief:     simpleMonoAE Algorithm
///        @author:    csoka, attila.csok@movidius.com
///        @copyright: All code copyright Movidius Ltd 2015, all rights reserved.
///                  For License Warranty see: common/license.txt
/// =====================================================================================
///

/// System Includes
/// -------------------------------------------------------------------------------------
#include <stdio.h>
#include <math.h>
#include <DrvI2cMaster.h>

/// Application Includes
/// -------------------------------------------------------------------------------------
#include "components/PlgIspCfgV0/leon/aeAlgo.h"

/*
 * Internal debug code, disabled by default.
 */
//#define ENABLE_DBG_LOCAL_aei
#ifdef ENABLE_DBG_LOCAL_aei
#include "assert.h"
#define DBG_MAX_MESSAGE_aei 8096
static volatile uint32_t dbgMsgBuf_aei[DBG_MAX_MESSAGE_aei];
static volatile uint32_t dbgMsgIdx_aei = 0;
static inline void dbgMAddMes_aei(uint32_t mesage) {
    dbgMsgBuf_aei[dbgMsgIdx_aei] = mesage;
    dbgMsgIdx_aei++;
    assert(dbgMsgIdx_aei < DBG_MAX_MESSAGE_aei);
    //if(dbgMsgIdx == DBG_MAX_MESSAGE) {
    //    dbgMsgIdx = 0;
    //}
}
#else
#define dbgMAddMes_aei(X)
#endif

/*
 * Internal debug code, disabled by default.
 */
//#define ENABLE_DBG_LOCAL_aeii
#ifdef ENABLE_DBG_LOCAL_aeii
#include "assert.h"
#define DBG_MAX_MESSAGE_aeii 8096
static volatile uint32_t dbgMsgBuf_aeii[DBG_MAX_MESSAGE_aeii];
static volatile uint32_t dbgMsgIdx_aeii = 0;
static inline void dbgMAddMes_aeii(uint32_t mesage) {
    dbgMsgBuf_aeii[dbgMsgIdx_aeii] = mesage;
    dbgMsgIdx_aeii++;
    assert(dbgMsgIdx_aeii < DBG_MAX_MESSAGE_aeii);
    //if(dbgMsgIdx == DBG_MAX_MESSAGE) {
    //    dbgMsgIdx = 0;
    //}
}
#else
#define dbgMAddMes_aeii(X)
#endif

#if 0
#define CMX_NOCACHE __attribute__((section(".cmx_direct.data")))
#else
#define CMX_NOCACHE static const
#endif

/// Source Specific #defines and types (typedef,enum,struct)
/// -------------------------------------------------------------------------------------
/// Global control affecting image brightness after AE - tune to taste
CMX_NOCACHE u32 AE_BRIGHTNESS_CONTROL_HC = 100; // (255/3)
CMX_NOCACHE u32 AE_BRIGHTNESS_CONTROL_LC =  75; // (255/2)

/// Control affecting AE convergence speed.  Range is [0, 1.0]
CMX_NOCACHE float AE_CONVERGENCE_SPEED = 0.1f;

/// AE controls controlling fast convergence feature.  Fast convergence
/// increases the convergence speed when the image is greatly overexposed
/// (high number of saturated patches detected) in proportion to the number
/// of saturated patches.
/// If the number of saturated patches is below AE_SAT_FAST_CONVERGE_FLOOR,
/// then fast convergence is disabled.
/// AE_SAT_FAST_CONVERGE_SCALE controls the convergence acceleration.  A value
/// of zero effectively disables fast convergence.
#define AE_SAT_FAST_CONVERGE_FLOOR  0
#define AE_SAT_FAST_CONVERGE_SCALE  0.04f
#define AE_SAT_FAST_CONVERGE_LIMIT  0.8f

#define MAXIMUM_INTEGRATION_TIME    0x006400

CMX_NOCACHE u32 STDDEV_HIGH_CONTRAST_THRESH   = 20;
CMX_NOCACHE u32 STDDEV_CONTRAST_SWITCH_THRESH = 15;

/// Global Data (Only if absolutely necessary)
/// -------------------------------------------------------------------------------------

/// Static Local Data
/// -------------------------------------------------------------------------------------

/// Static Function Prototypes
/// -------------------------------------------------------------------------------------
static float aeCalcExpGain(float totalExposure, int maxIntTime, int *intTime, int *gainCode);
static unsigned aeGetTargetBrightness(unsigned stddev);
static float aeTemporalFilter(float currentExp, float targetExp, int saturatedPatches, int darkPatches);

/// Functions Implementation
/// -------------------------------------------------------------------------------------

/// converts fp32 gain to reg value and fills back the actually possible gain
s32 (imgOv7750CalcGainReg)(fp32* gain)
{
    s32 gainReg = 0;
    s32 gainRegLin = 0x10;
    fp32 gainTmp = *gain;
    fp32 gain2 = 1.0;

    if (gainTmp > (15 + 16))
    {
        gainTmp = 15 + 16;
    }

    if (gainTmp >= 2.0)
    {
        gainReg |= 0x10;
        gainRegLin = 0x20;
        gainTmp /= 2;
        gain2 *= 2;
    }

    if (gainTmp >= 2.0)
    {
        gainReg |= 0x20;
        gainRegLin = 0x40;
        gainTmp /= 2;
        gain2 *= 2;
    }

    if (gainTmp >= 2.0)
    {
        gainReg |= 0x40;
        gainRegLin = 0x80;
        gainTmp /= 2;
        gain2 *= 2;
    }

    if (gainTmp >= 2.0)
    {
        gainReg |= 0x80;
        gainRegLin = 0x100;
        gainTmp /= 2;
        gain2 *= 2;
    }

//    gainReg += (s32)((gainTmp - 1.0) * 16);
    *gain = gain2 * (1 + (fp32)(gainReg & 0xF) / 16);

    ///for sensor revision Ver1C the gain format is changed
    ///bit[4] in register 0x3509 is set to 1 i.e. Use real gain {0x350A, 0x350B} as linear gain
    /// and we return linear gain (gainRegLin)
    /// if bit[4] in register 0x3509 is set to 0  i.e. Use sensor gain {0x350A, 0x350B} as sensor gain
    /// we return sensor gain (gainReg)
    return gainRegLin;
}

/// ===  FUNCTION  ======================================================================
///  Name:  aeCalcExpGain
///  Description:  Find a gain and integration that yield the desired total exposure.
///                Use the smallest gain possible, to minimise noise amplification.
///                'totalExposure' is the integration time in terms of lines, times the
///                analog gain.
/// =====================================================================================
static float aeCalcExpGain(float totalExposure, int maxIntTime, int *intTime, int *gainCode)
{
    int     mx = 240, mn = 0, code;
    float   gain;

    /*
     * Find the minimum gain that's high enough, within the range of gain
     * codes [0, 24] (i.e. range of gains within [1.0, 16.0]), by divide and
     * conquer.
     */
    while (mx != mn) {
        code = (mn + mx) / 2;
        gain = 256.0f / (256.0f - code);
        if (totalExposure > gain * maxIntTime) {
            /* Desired exposure needs a higher gain */
            mn = code + 1;
        } else {
            /* This gain could work */
            mx = code;
        }
    }

    code = mn;
    gain = 256.0f / (256.0f - code);

    *gainCode = code;
    *intTime = totalExposure / gain + .5;

    if (*intTime > maxIntTime) {
        *intTime = maxIntTime;
    }

    return gain * *intTime;
}

/// ===  FUNCTION  ======================================================================
///  Name:  aeGetTargetBrightness
///  Description:  Work out the desired average brightness of the scene (target average
///                brightness).  When there's more contrast in the scene, use a lower
///                target average brightness.
/// =====================================================================================
static unsigned aeGetTargetBrightness(unsigned stddev)
{
    float   alpha;
    static float    alphaLast = 0;
    static int      stddevWhenChanged = 0;
    int     highContrast;
    static int     highContrastLast = 0;


    highContrast = stddev > STDDEV_HIGH_CONTRAST_THRESH;

    if (highContrast != highContrastLast) {
        if (fabs(stddev-stddevWhenChanged) > STDDEV_CONTRAST_SWITCH_THRESH) {
            highContrastLast = highContrast;
            stddevWhenChanged = stddev;
        }
    }

    return highContrastLast ?
      AE_BRIGHTNESS_CONTROL_HC : AE_BRIGHTNESS_CONTROL_LC;
}

/// ===  FUNCTION  ======================================================================
///  Name:  aeTemporalFilter
///  Description:
/// =====================================================================================
static float aeTemporalFilter(float currentExp, float targetExp, int saturatedPatches, int darkPatches)
{
    float   alpha = AE_CONVERGENCE_SPEED;
    float   exposure;
    int     badPatches = saturatedPatches;

    if (darkPatches > saturatedPatches) {
        badPatches = darkPatches;
    }

    badPatches -= AE_SAT_FAST_CONVERGE_FLOOR;
    if (badPatches < 0) {
        badPatches = 0;
    }

    alpha = alpha * (1.0f + (float)badPatches*AE_SAT_FAST_CONVERGE_SCALE);
    if (alpha > AE_SAT_FAST_CONVERGE_LIMIT) {
        alpha = AE_SAT_FAST_CONVERGE_LIMIT;
    }

    exposure = currentExp * (1.0f-alpha) + targetExp * alpha;

    return exposure;
}

/// ===  FUNCTION  ======================================================================
///  Name:  aeGetNewExposure
///  Description: main function of exposure calculation
/// =====================================================================================
void aeGetNewExposure(uint32_t* statPatchesBuffer, AeAwbPatchConfigs *aeAwbPatchConfigs, float *gain, aeExposure_t *exposureOut)
{
    int             x, n;
    uint32_t        y, h;
    int             saturatedPatches = 0, darkPatches = 0, total = 0;
    unsigned        avgBrightness, targetBrightness;
    unsigned        variance, stddev;
    float           ratio;
    float           totalExposure;
    unsigned        intTime;
    unsigned        exposure, gainCode;
    static float    TE_N, TE_N_Minus_1, TE_N_Minus_2;
    uint32_t        AE_STATS_PATCH_NPIXELS  = ((aeAwbPatchConfigs->AE_STATS_PATCH_W)*(aeAwbPatchConfigs->AE_STATS_PATCH_H));
    uint32_t        AE_STATS_SAT_THRESHOLD  = ((AE_STATS_PATCH_NPIXELS)*255);
    uint32_t        AE_STATS_DARK_THRESHOLD = ((AE_STATS_PATCH_NPIXELS)*18);

    statPatchesBuffer = (uint32_t*)((uint32_t)statPatchesBuffer|0xC0000000);

    dbgMAddMes_aei(statPatchesBuffer);
    dbgMAddMes_aei(statPatchesBuffer[y+0]);
    dbgMAddMes_aei(statPatchesBuffer[y+1]);

    for (y = 0; y < (aeAwbPatchConfigs->AE_STATS_PATCHES_V); y+=1)
    {
        for (h = 0; h < (aeAwbPatchConfigs->AE_STATS_PATCHES_H); h+=1)
        {
            uint32_t patchBrightness =
                    statPatchesBuffer[(y * 2 + 0) * aeAwbPatchConfigs->AE_STATS_PATCHES_H + h * 2    ]+
                    statPatchesBuffer[(y * 2 + 0) * aeAwbPatchConfigs->AE_STATS_PATCHES_H + h * 2 + 1]+
                    statPatchesBuffer[(y * 2 + 1) * aeAwbPatchConfigs->AE_STATS_PATCHES_H + h * 2    ]+
                    statPatchesBuffer[(y * 2 + 1) * aeAwbPatchConfigs->AE_STATS_PATCHES_H + h * 2 + 1];

            if (patchBrightness >= AE_STATS_SAT_THRESHOLD) {
                saturatedPatches++;
            }
            if (patchBrightness <= AE_STATS_DARK_THRESHOLD) {
                darkPatches++;
            }
            total += patchBrightness;
        }
    }
    dbgMAddMes_aei(total);
    dbgMAddMes_aeii(total);

    avgBrightness = total /
      (AE_STATS_PATCH_NPIXELS*(aeAwbPatchConfigs->AE_STATS_PATCHES_H * aeAwbPatchConfigs->AE_STATS_PATCHES_V));
    /* Work out the variance of the stats (scene contrast) */
    variance = 0;
    for (y = 0; y < (aeAwbPatchConfigs->AE_STATS_PATCHES_V); y+=1)
    {
        for (h = 0; h < (aeAwbPatchConfigs->AE_STATS_PATCHES_H); h+=1)
        {
            uint32_t patchBrightness =
                    statPatchesBuffer[(y * 2 + 0) * aeAwbPatchConfigs->AE_STATS_PATCHES_H + h * 2    ]+
                    statPatchesBuffer[(y * 2 + 0) * aeAwbPatchConfigs->AE_STATS_PATCHES_H + h * 2 + 1]+
                    statPatchesBuffer[(y * 2 + 1) * aeAwbPatchConfigs->AE_STATS_PATCHES_H + h * 2    ]+
                    statPatchesBuffer[(y * 2 + 1) * aeAwbPatchConfigs->AE_STATS_PATCHES_H + h * 2 + 1];

            n = patchBrightness / AE_STATS_PATCH_NPIXELS - avgBrightness;
            variance += n*n;
        }
    }

    stddev = sqrt(variance / (aeAwbPatchConfigs->AE_STATS_PATCHES_H*aeAwbPatchConfigs->AE_STATS_PATCHES_V));

    targetBrightness = aeGetTargetBrightness(stddev);

    /*
     * Calculate ratio of current brightness to new target brightness.
     * This is the same as the ratio of current total exposure to target
     * total exposure.
     */
    ratio = (float)targetBrightness / (avgBrightness+1);

    if (TE_N_Minus_2 <= 10) {
        TE_N_Minus_2 = 10;
    }
    if (TE_N <= 10) {
        TE_N = 10;
    }

    totalExposure = aeTemporalFilter(TE_N,
      TE_N_Minus_2 * ratio, saturatedPatches, darkPatches);

    /* Keep track of exposure that was programmed for previous frames */
    TE_N_Minus_2 = TE_N_Minus_1;
    TE_N_Minus_1 = TE_N;

    /* Calculate exposure to programm for next frame */
    TE_N = aeCalcExpGain(totalExposure, MAXIMUM_INTEGRATION_TIME, &exposure, &gainCode);

    exposureOut->integrationTime = exposure;
    exposureOut->gainCode = gainCode;

    *gain = exposureOut->gainCode * 16.0 / 240;

    exposureOut->gainCode = imgOv7750CalcGainReg(gain);
}
