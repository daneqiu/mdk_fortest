///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved
///            For License Warranty see: common/license.txt
///
/// @defgroup CameraModules Camera Modules
/// @defgroup CamDefines Camera Defines
/// @ingroup CameraModules
/// @{
/// @brief Definitions and types needed by the Camera Module component
///
/// This is the file that contains all the Sensor definitions of constants, typedefs,
/// structures or enums or exported variables from this module

#ifndef _CAM_API_DEFINES_H_
#define _CAM_API_DEFINES_H_
#include <mv_types.h>
#include "DrvMipiDefines.h"
#include "swcFrameTypes.h"

#define COUNT_OF(x) ((sizeof(x)/sizeof(0[x])) / ((!(sizeof(x) % sizeof(0[x])))))

typedef enum i2cConfigType_t
  {
    CAM_FULL_CONFIG,
    CAM_COLORBAR,
    CAM_START_STREAMING,
    CAM_STOP_STREAMING,
    CAM_STANDBY,
    CAM_WAKEUP,
    CAM_INTEGRATION_TIME,
    CAM_GAIN,
    // to be continued
} i2cConfigType_t;

typedef enum gpioConfigType_t
{
    END = 0,
    POWER_PIN,
    SHUTDOWN_PIN,
    RESET_PIN,
    MCLK_PIN, //TODO: not yet supported
    // to be continued
} gpioConfigType_t;

typedef struct GpioConfigDescriptorS
{
    gpioConfigType_t configType;
    u8 gpioNumber;
    u8 activeLevel;
    u32 delayMs;
} GpioConfigDescriptor;

/// @brief I2C configuration descriptor
typedef struct I2CConfigDescriptorS
{
  const u16       (*regValues)[2];
  i2cConfigType_t   configType;
  u32               numberOfRegs;
  u32               delayMs;
} I2CConfigDescriptor;

///  @brief MIPI configuration
typedef struct
{
	/// @brief Pixel format
   eDrvMipiDataType pixelFormat;
   /// @brief Data bitrate
   u32              dataRateMbps;
   /// @brief Number of lanes
   u32              nbOflanes;
   /// @brief MIPI data transfer mode
   eDrvMipiDataMode dataMode;
} mipiSpec;                      //set all fields to 0 for parallel interface

typedef struct //GASTEMP to (be moved in)/(replace the camSpec of) mipiCamDefines.h ??
{
    u32              frameWidth;
    u32              frameHeight;
    u32              hBackPorch;
    u32              hFrontPorch;
    u32              vBackPorch;
    u32              vFrontPorch;
    const mipiSpec   *mipiCfg;
    frameType        internalPixelFormat;
    u32              bytesPerPixel;
    u32              idealRefFreq;

    u32              sensorI2CAddress1;
    u32              sensorI2CAddress2;
    u32              regSize;

    const I2CConfigDescriptor *i2cConfigSteps;
    const u32              nbOfI2CConfigSteps;
} GenericCamSpec;

/// @}
#endif // _CAM_API_DEFINES_H_
