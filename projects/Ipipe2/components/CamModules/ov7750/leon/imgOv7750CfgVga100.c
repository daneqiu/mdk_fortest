///
/// @file
/// @copyright All code copyright Movidius Ltd 2013, all rights reserved
///            For License Warranty see: common/license.txt
///
/// @brief
///
///Configuration file for OmniVision 7750
//# 640x480p @ 100 fps RAW10

#include "imgOv7750CfgVga100.h"

#include "CamDefines.h"
#include "mv_types.h"
#include "swcFrameTypes.h"


static const u16 imgOv7750CfgVga100Raw10SensorCfg[][2] =
{
    {0x0103, 0x01},
    {0x0100, 0x00},
    {0x3005, 0x00},
    {0x3013, 0x12},
    {0x3014, 0x04},
    {0x3016, 0xf0},
    {0x3017, 0xf0},
    {0x3018, 0xf0},
    {0x301a, 0xf0},
    {0x301b, 0xf0},
    {0x301c, 0xf0},
    {0x3023, 0x07},
    {0x3037, 0xf0},
    {0x3098, 0x04},
    {0x3099, 0x28},
    {0x309a, 0x05},
    {0x309b, 0x04},
    {0x30b0, 0x0a},
    {0x30b1, 0x01},
    {0x30b3, 0x64},
    {0x30b4, 0x03},
    {0x30b5, 0x05},
    {0x3106, 0x12},
    {0x3500, 0x00},
    {0x3501, 0x1f},
    {0x3502, 0x80},
    {0x3503, 0x07},
    {0x3509, 0x08},
    {0x350b, 0x10},
    {0x3600, 0x1c},
    {0x3602, 0x62},
    {0x3620, 0xb7},
    {0x3622, 0x04},
    {0x3626, 0x21},
    {0x3627, 0x30},
    {0x3634, 0x41},
    {0x3636, 0x00},
    {0x3662, 0x01},
    {0x3664, 0xf0},
    {0x366a, 0x00},
    {0x366b, 0x50},
    {0x3705, 0xc1},
    {0x3709, 0x40},
    {0x373c, 0x08},
    {0x3742, 0x00},
    {0x3788, 0x00},
    {0x37a8, 0x01},
    {0x37a9, 0xc0},
    {0x3800, 0x00}, // horizontal start HS[9:8]
    {0x3801, 0x04}, // horizontal start HS]7:0]
    {0x3802, 0x00}, // vertical   start VS[9:8]
    {0x3803, 0x04}, // vertical   start VS[7:0]
    {0x3804, 0x02}, // horizontal end   HE[9:8]
    {0x3805, 0x8b}, // horizontal end   HE]7:0]
    {0x3806, 0x01}, // vertical   end   VE[9:8]
    {0x3807, 0xeb}, // vertical   end   VE[7:0]
    {0x3808, 0x02},
    {0x3809, 0x80},
    {0x380a, 0x01},
    {0x380b, 0xe0},
    {0x380c, 0x03},
    {0x380d, 0xa0},
    {0x380e, 0x06},  // VBLANKING HIGH
    {0x380f, 0xBC},  // VBLANKING LOW
    {0x3810, 0x00},
    {0x3811, 0x04},
    {0x3812, 0x00},
    {0x3813, 0x05},
    {0x3814, 0x11},
    {0x3815, 0x11},
    {0x3820, 0x00},
    {0x3821, 0x00},
    {0x382f, 0xc4},
    {0x3832, 0xff},
    {0x3833, 0xff},
    {0x3834, 0x00},
    {0x3835, 0x05},
    {0x3837, 0x00},
    {0x3b80, 0x00},
    {0x3b81, 0xa5},
    {0x3b82, 0x10},
    {0x3b83, 0x00},
    {0x3b84, 0x08},
    {0x3b85, 0x00},
    {0x3b86, 0x01},
    {0x3b87, 0x00},
    {0x3b88, 0x00},
    {0x3b89, 0x00},
    {0x3b8a, 0x00},
    {0x3b8b, 0x05},
    {0x3b8c, 0x00},
    {0x3b8d, 0x00},
    {0x3b8e, 0x00},
    {0x3b8f, 0x1a},
    {0x3b94, 0x05},
    {0x3b95, 0xf2},
    {0x3b96, 0x40},
    {0x3c00, 0x89},
    {0x3c01, 0xab},
    {0x3c02, 0x01},
    {0x3c03, 0x00},
    {0x3c04, 0x00},
    {0x3c05, 0x03},
    {0x3c06, 0x00},
    {0x3c07, 0x05},
    {0x3c0c, 0x00},
    {0x3c0d, 0x00},
    {0x3c0e, 0x00},
    {0x3c0f, 0x00},
    {0x4001, 0x42},
    {0x4004, 0x04},
    {0x404e, 0x01},
    {0x4300, 0xff},
    {0x4301, 0x00},
    {0x4600, 0x00},
    {0x4601, 0x4e},
    {0x4801, 0x0f},
    {0x4806, 0x0f},
    {0x4837, 0x19},
    {0x4a0d, 0x00},
    {0x5000, 0x85},
};

static const u16 imgOv7750CfgVga100Raw10SensorColorbarCfg[][2] =
{
    {0x5e00, 0x8c},
};

static const u16 imgOv7750CfgVga100Raw10SensorStartStreaming[][2] =
{
    {0x0100, 0x01},
};

static const u16 imgOv7750CfgVga100Raw10SensorStopStreaming[][2] =
{
    {0x0100, 0x00},
};

static const u16 imgOv7750CfgVga100Raw10SensorIntegrationRegistere[][2] =
{
    {0x3502, 0x00},
    {0x3501, 0x00},
    {0x3500, 0x00},
};

static const u16 imgOv7750CfgVga100Raw10SensorGainRegisters[][2] =
{
    {0x350B, 0x00},
    {0x350A, 0x00},
};

static const I2CConfigDescriptor imgOv7750CfgVga100Raw10_I2CConfigSteps[] =
{
    {
        .regValues = imgOv7750CfgVga100Raw10SensorCfg,
        .numberOfRegs = COUNT_OF(imgOv7750CfgVga100Raw10SensorCfg),
        .configType = CAM_FULL_CONFIG,
        .delayMs = 0
    },
    {
        .regValues = imgOv7750CfgVga100Raw10SensorColorbarCfg,
        .numberOfRegs = COUNT_OF(imgOv7750CfgVga100Raw10SensorColorbarCfg),
        .configType = CAM_COLORBAR,
        .delayMs = 0
    },
    {
        .regValues = imgOv7750CfgVga100Raw10SensorStartStreaming,
        .numberOfRegs = COUNT_OF(imgOv7750CfgVga100Raw10SensorStartStreaming),
        .configType = CAM_START_STREAMING,
        .delayMs = 0
    },
    {
        .regValues = imgOv7750CfgVga100Raw10SensorStopStreaming,
        .numberOfRegs = COUNT_OF(imgOv7750CfgVga100Raw10SensorStopStreaming),
        .configType = CAM_STOP_STREAMING,
        .delayMs = 0
    },
    {
        .regValues = imgOv7750CfgVga100Raw10SensorIntegrationRegistere,
        .numberOfRegs = COUNT_OF(imgOv7750CfgVga100Raw10SensorIntegrationRegistere),
        .configType = CAM_INTEGRATION_TIME,
        .delayMs = 0
    },
    {
        .regValues = imgOv7750CfgVga100Raw10SensorGainRegisters,
        .numberOfRegs = COUNT_OF(imgOv7750CfgVga100Raw10SensorGainRegisters),
        .configType = CAM_GAIN,
        .delayMs = 0
    },
};

static const mipiSpec imgOv7750CfgVga100Raw10_mipiCfg =
{
    .dataMode     = MIPI_D_MODE_0,
    .dataRateMbps = 600,
    .nbOflanes    = 1,
    .pixelFormat  = CSI_RAW_10
};


const GenericCamSpec imgOv7750CfgVga100Raw8_camCfg =
{
    .frameWidth            = OV7750_WIDTH,
    .frameHeight           = OV7750_HEIGHT,
    .hBackPorch            = 0,
    .hFrontPorch           = 0,
    .vBackPorch            = 0,
    .vFrontPorch           = 0,
    .bytesPerPixel         = 1,     //2 bytes on MIPI interface, stored on 1 byte after SIPP (ONLY!) conversion 10bits -> 8 bits
    .internalPixelFormat   = RAW8,
    .mipiCfg               = &imgOv7750CfgVga100Raw10_mipiCfg,
    .idealRefFreq          = 24,
    .sensorI2CAddress1     = (0xC0 >> 1),
    .sensorI2CAddress2     = 0,            //only used for stereo cameras
    .nbOfI2CConfigSteps    = COUNT_OF(imgOv7750CfgVga100Raw10_I2CConfigSteps),
    .i2cConfigSteps        = imgOv7750CfgVga100Raw10_I2CConfigSteps,
    .regSize               = 1,
};

const GenericCamSpec imgOv7750CfgVga100Raw10_camCfg =
{
    .frameWidth            = OV7750_WIDTH,
    .frameHeight           = OV7750_HEIGHT,
    .hBackPorch            = 0,
    .hFrontPorch           = 0,
    .vBackPorch            = 0,
    .vFrontPorch           = 0,
    .bytesPerPixel         = 2,     //2 bytes on MIPI interface, stored on 1 byte after SIPP (ONLY!) conversion 10bits -> 8 bits
    .internalPixelFormat   = RAW16,
    .mipiCfg               = &imgOv7750CfgVga100Raw10_mipiCfg,
    .idealRefFreq          = 24,
    .sensorI2CAddress1     = (0xC0 >> 1),
    .sensorI2CAddress2     = 0,            //only used for stereo cameras
    .nbOfI2CConfigSteps    = COUNT_OF(imgOv7750CfgVga100Raw10_I2CConfigSteps),
    .i2cConfigSteps        = imgOv7750CfgVga100Raw10_I2CConfigSteps,
    .regSize               = 1,
};
