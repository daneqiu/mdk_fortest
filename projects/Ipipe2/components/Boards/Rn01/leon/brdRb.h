///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved
///            For License Warranty see: common/license.txt
///
/// @brief     API for the Rb Board Driver
///
///
///
///
///
#ifndef BRD_Rb_H
#define BRD_Rb_H

// 1: Includes
// ----------------------------------------------------------------------------
#include "DrvI2cMasterDefines.h"
#include <brdRbDefines.h>

#define NUM_I2C_DEVICES                 (3)

typedef struct
{
    I2CM_Device * i2c0Handle;
    I2CM_Device * i2c1Handle;
    I2CM_Device * i2c2Handle;
} tyAppDeviceHandles;

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------
extern tyAppDeviceHandles gAppDevHndls;

extern I2CM_Device i2cDevHandle[NUM_I2C_DEVICES];


// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------

/// Initialise the default configuration for I2C0,I2C1, I2C2 on the Rb Board
/// @param[in] pointer to an I2C configuration structure for I2C0 (OR NULL to use board defaults)
/// @param[in] pointer to an I2C configuration structure for I2C1 (OR NULL to use board defaults)
/// @param[in] pointer to an I2C configuration structure for I2C2 (OR NULL to use board defaults)
/// @param[in] pointer to storage for an *I2CM_Device Handle for I2C Device 0
/// @param[in] pointer to storage for an *I2CM_Device Handle for I2C Device 1
/// @param[in] pointer to storage for an *I2CM_Device Handle for I2C Device 2
/// @return  0 on Success
s32 brdRbInitialiseI2C(tyI2cConfig * i2c0Cfg,
                            tyI2cConfig * i2c1Cfg,
                            tyI2cConfig * i2c2Cfg,
                            I2CM_Device ** i2c0Dev,
                            I2CM_Device ** i2c1Dev,
                            I2CM_Device ** i2c2Dev);

s32 brdRbInitializeBoard(void);

#endif // BRD_Rb_H
