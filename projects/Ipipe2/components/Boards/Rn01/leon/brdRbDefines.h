///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved
///            For License Warranty see: common/license.txt
///
/// @brief     Definitions and types needed by the Rb Board Driver API
///
/// This header contains all necessary hardware defined constants for this board
/// e.g. GPIO assignments, I2C addresses
///

#ifndef BRD_Rb_DEF_H
#define BRD_Rb_DEF_H

#include <DrvCpr.h>
#include <CamDefines.h>

// 1: Defines
// ----------------------------------------------------------------------------

#define Rb_OSC_IN_FREQ_KHZ         12000

// ----------------------------------------------------------------------------
// I2C
// ----------------------------------------------------------------------------

// I2C0 -- Master
#define Rb_I2C0_SCL_PIN            (60)
#define Rb_I2C0_SDA_PIN            (61)
#define Rb_I2C0_SPEED_KHZ_DEFAULT  (100)
#define Rb_I2C0_ADDR_SIZE_DEFAULT  (ADDR_7BIT)

// I2C1 -- Master
#define Rb_I2C1_SCL_PIN            (12)
#define Rb_I2C1_SDA_PIN            (13)
#define Rb_I2C1_SPEED_KHZ_DEFAULT  (100)
#define Rb_I2C1_ADDR_SIZE_DEFAULT  (ADDR_7BIT)

// I2C2 -- Master
#define Rb_I2C2_SCL_PIN            (79)
#define Rb_I2C2_SDA_PIN            (80)
#define Rb_I2C2_SPEED_KHZ_DEFAULT  (100)
#define Rb_I2C2_ADDR_SIZE_DEFAULT  (ADDR_7BIT)

// ----------------------------------------------------------------------------
// General GPIO definition
// ----------------------------------------------------------------------------

// GPIO:  Inputs

// GPIO:  Outputs
//#define Rb_V3V3_PIN            (21)
//#define Rb_V2V8_PIN            (22)
//#define Rb_V1V5_PIN            (23)
//#define Rb_V1V8_SENS_PIN       (25)

#define Rb_PMD_RESET_PIN       (27)

#define RB_GPIO_PIN_IR_IMAGER_EN    49
#define RB_GPIO_PIN_IR_CAM_RESET_N  27
#define RB_GPIO_PIN_IR_MCLK         40
#define RB_GPIO_PIN_IR_CAM_FSYNC    33

#define RB_GPIO_PIN_OVT_RESET       28
#define RB_GPIO_PIN_OVT_MCLK        41

extern const GpioConfigDescriptor pmdCamGpios[];
extern const GpioConfigDescriptor ov7750CamGpios[];

// ----------------------------------------------------------------------------
// General CLOCKS definition
// ----------------------------------------------------------------------------

//#define DESPERATE_CLOCKS

#ifndef DESPERATE_CLOCKS
#define CSS_LOS_CLOCKS     (                                    \
                            DEFAULT_CORE_CSS_DSS_CLOCKS  |      \
                            DEV_CSS_LAHB_CTRL            |      \
                            DEV_CSS_APB4_CTRL            |      \
                            DEV_CSS_CPR                  |      \
                            DEV_CSS_ROM                  |      \
                            DEV_CSS_LOS_L2C              |      \
                            DEV_CSS_MAHB_CTRL            |      \
                            DEV_CSS_LOS_ICB              |      \
                            DEV_CSS_LOS_DSU              |      \
                            DEV_CSS_LOS_TIM              |      \
                            DEV_CSS_APB1_CTRL            |      \
                            DEV_CSS_APB3_CTRL            |      \
                            DEV_CSS_SAHB_CTRL            |      \
                            DEV_CSS_MSS_MAS              |      \
                            DEV_CSS_UPA_MAS              |      \
                            DEV_CSS_LAHB2SHB             |      \
                            DEV_CSS_SAHB2MAHB            |      \
                            DEV_CSS_SPI0                 |      \
                            DEV_CSS_I2C0                 |      \
                            DEV_CSS_I2C1                 |      \
                            DEV_CSS_I2C2                 )

#define MSS_CLOCKS         (                                    \
                            DEV_MSS_APB_SLV              |      \
                            DEV_MSS_APB2_CTRL            |      \
                            DEV_MSS_RTBRIDGE             |      \
                            DEV_MSS_RTAHB_CTRL           |      \
                            DEV_MSS_LRT                  |      \
                            DEV_MSS_LRT_DSU              |      \
                            DEV_MSS_LRT_L2C              |      \
                            DEV_MSS_LRT_ICB              |      \
                            DEV_MSS_AXI_BRIDGE           |      \
                            DEV_MSS_MXI_CTRL             |      \
                            DEV_MSS_MXI_DEFSLV           |      \
                            DEV_MSS_AXI_MON              |      \
                            DEV_MSS_NAL                  |      \
                            DEV_MSS_MIPI                 |      \
                            DEV_MSS_CIF0                 |      \
                            DEV_MSS_LCD                  |      \
                            DEV_MSS_AMC                  |      \
                            DEV_MSS_SIPP                 |      \
                            DEV_MSS_TIM                  |      \
                            DEV_MSS_SIPP_ABPSLV          )

#define SIPP_CLOCKS        (                                    \
                            DEV_SIPP_RAW                 |      \
                            DEV_SIPP_HARRIS              |      \
                            0xFFFFFFFF                   |      \
                            DEV_SIPP_MIPI_RX0            )

#define UPA_CLOCKS         (                                    \
                            DEV_UPA_SH0                  |      \
                            DEV_UPA_SH1                  |      \
                            DEV_UPA_SH2                  |      \
                            DEV_UPA_SH3                  |      \
                            DEV_UPA_SH4                  |      \
                            DEV_UPA_SH5                  |      \
                            DEV_UPA_SH6                  |      \
                            DEV_UPA_SHAVE_L2             |      \
                            DEV_UPA_CDMA                 |      \
                            DEV_UPA_CTRL                 )
#else
#    define CSS_LOS_CLOCKS 0xFFFFFFFFFFFFFFFFULL
#    define MSS_CLOCKS     0xFFFFFFFFUL
#    define SIPP_CLOCKS    0xFFFFFFFFUL
#    define UPA_CLOCKS     0xFFFFFFFFUL
#endif

// 2: Typedefs (types, enums, structs)
// ----------------------------------------------------------------------------


// 3: Local const declarations     NB: ONLY const declarations go here
// ----------------------------------------------------------------------------
static const tyAuxClkDividerCfg auxClkRb[] =
{
    {
        .auxClockEnableMask     = AUX_CLK_MASK_MIPI_TX1,
        .auxClockSource         = CLK_SRC_REFCLK0,
        .auxClockDivNumerator   = 1,
        .auxClockDivDenominator = 1
    },
    {
        .auxClockEnableMask     = AUX_CLK_MASK_MEDIA | AUX_CLK_MASK_LCD | AUX_CLK_MASK_CIF0,
        .auxClockSource         = CLK_SRC_SYS_CLK_DIV2,
        .auxClockDivNumerator   = 1,
        .auxClockDivDenominator = 1,
    },
    {
        .auxClockEnableMask     = AUX_CLK_MASK_GPIO3, // OV7750 Clock
        .auxClockSource         = CLK_SRC_PLL0,
        .auxClockDivNumerator   = 1,
        .auxClockDivDenominator = 20,                 // gives 24MHz clock
    },
    {
        .auxClockEnableMask     = AUX_CLK_MASK_GPIO2, // PMD_IR_MCLK Clock
        .auxClockSource         = CLK_SRC_PLL0,
        .auxClockDivNumerator   = 1,
        .auxClockDivDenominator = 20,                 // gives 24MHz clock
    },
    {
        .auxClockEnableMask     = AUX_CLK_MASK_GPIO1, // MAIN_13MP_MCLK Clock
        .auxClockSource         = CLK_SRC_PLL0,
        .auxClockDivNumerator   = 1,
        .auxClockDivDenominator = 20,                 // gives 24MHz clock
    },
    {
        .auxClockEnableMask     = AUX_CLK_MASK_MIPI_ECFG | AUX_CLK_MASK_MIPI_CFG,
        .auxClockSource         = CLK_SRC_SYS_CLK,
        .auxClockDivNumerator   = 1,
        .auxClockDivDenominator = 24,                 // gives 20MHz clock
    },

    {0,0,0,0}, // Null Terminated List
};


static const tySocClockConfig appClockConfigRb =
{
    .refClk0InputKhz         = 12000,           // Default 12Mhz input clock
    .refClk1InputKhz         = 0,               // Assume no secondary oscillator for now
    .targetPll0FreqKhz       = 480000,
    .targetPll1FreqKhz       = 0,               // set in DDR driver
    .clkSrcPll1              = CLK_SRC_REFCLK0, // Supply both PLLS from REFCLK0
    .masterClkDivNumerator   = 1,
    .masterClkDivDenominator = 1,
    .cssDssClockEnableMask   = CSS_LOS_CLOCKS,
    .mssClockEnableMask      = MSS_CLOCKS,                            // Not enabling any MSS clocks for now
    .sippClockEnableMask     = SIPP_CLOCKS,
    .upaClockEnableMask      = UPA_CLOCKS,                            // Not enabling any UPA clocks for now
    .pAuxClkCfg              = auxClkRb,
};

#endif // BRD_Rb_DEF_H
