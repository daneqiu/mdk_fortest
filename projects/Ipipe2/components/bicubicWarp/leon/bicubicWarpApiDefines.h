///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @defgroup bicubicWarpApiDefines Bicubic Warp API Defines
/// @ingroup  bicubicWarp
/// @{
/// @brief Definitions and types needed by the bicubic warp component
///
/// This file contains all the definitions of constants, typedefs,
/// structures, enums and exported variables for the bicubic warp component
///

#ifndef __BICUBIC_WARP_API_DEFINES_H__
#define __BICUBIC_WARP_API_DEFINES_H__

// CTRL register bitfield defines
#ifndef BICUBIC_RUN_SHIFT
#define BICUBIC_RUN_SHIFT            0
#endif

#ifndef BICUBIC_RUN_MASK
#define BICUBIC_RUN_MASK             0x01
#endif

#ifndef BICUBIC_BYPASS_SHIFT
#define BICUBIC_BYPASS_SHIFT         1
#endif

#ifndef BICUBIC_BYPASS_MASK
#define BICUBIC_BYPASS_MASK          0x02
#endif

#ifndef BICUBIC_IRQ_SHIFT
#define BICUBIC_IRQ_SHIFT            2
#endif

#ifndef BICUBIC_IRQ_MASK
#define BICUBIC_IRQ_MASK             0x04
#endif

#ifndef BICUBIC_SMOKETEST_SHIFT
#define BICUBIC_SMOKETEST_SHIFT      3
#endif

#ifndef BICUBIC_SMOKETEST_MASK
#define BICUBIC_SMOKETEST_MASK       0x08
#endif

#ifndef BICUBIC_RUNSTATUS_SHIFT
#define BICUBIC_RUNSTATUS_SHIFT      4
#endif

#ifndef BICUBIC_RUNSTATUS_MASK
#define BICUBIC_RUNSTATUS_MASK       0x10
#endif

#ifndef BICUBIC_CACHE_REQ_SHIFT
#define BICUBIC_CACHE_REQ_SHIFT      5
#endif

#ifndef BICUBIC_CACHE_REQ_MASK
#define BICUBIC_CACHE_REQ_MASK       0x20
#endif

#ifndef BICUBIC_CACHE_WEN_SHIFT
#define BICUBIC_CACHE_WEN_SHIFT      6
#endif

#ifndef BICUBIC_CACHE_WEN_MASK
#define BICUBIC_CACHE_WEN_MASK       0x40
#endif

#ifndef BICUBIC_BILINEAR_SHIFT
#define BICUBIC_BILINEAR_SHIFT       7
#endif

#ifndef BICUBIC_BILINEAR_MASK
#define BICUBIC_BILINEAR_MASK        0x80
#endif

#ifndef BICUBIC_PIX_FORMAT_SHIFT
#define BICUBIC_PIX_FORMAT_SHIFT     8
#endif

#ifndef BICUBIC_PIX_FORMAT_MASK
#define BICUBIC_PIX_FORMAT_MASK      0x300
#endif

#ifndef BICUBIC_CLAMP_SHIFT
#define BICUBIC_CLAMP_SHIFT          10
#endif

#ifndef BICUBIC_CLAMP_MASK
#define BICUBIC_CLAMP_MASK           0x400
#endif

#ifndef BICUBIC_BORDER_MODE_SHIFT
#define BICUBIC_BORDER_MODE_SHIFT    11
#endif

#ifndef BICUBIC_BORDER_MODE_MASK
#define BICUBIC_BORDER_MODE_MASK     0x800
#endif

#ifndef BICUBIC_BAYER_ENABLE_SHIFT
#define BICUBIC_BAYER_ENABLE_SHIFT   12
#endif

#ifndef BICUBIC_BAYER_ENABLE_MASK
#define BICUBIC_BAYER_ENABLE_MASK    0x1000
#endif

#ifndef BICUBIC_BAYER_Y_ODD_SHIFT
#define BICUBIC_BAYER_Y_ODD_SHIFT    13
#endif

#ifndef BICUBIC_BAYER_Y_ODD_MASK
#define BICUBIC_BAYER_Y_ODD_MASK     0x2000
#endif

#ifndef BICUBIC_BAYER_X_MODE_SHIFT
#define BICUBIC_BAYER_X_MODE_SHIFT   14
#endif

#ifndef BICUBIC_BAYER_X_MODE_MASK
#define BICUBIC_BAYER_X_MODE_MASK    0xC000
#endif

#ifndef BICUBIC_PIX_FORMAT_RGBX8888
#define BICUBIC_PIX_FORMAT_RGBX8888  0
#endif

#ifndef BICUBIC_PIX_FORMAT_U8F
#define BICUBIC_PIX_FORMAT_U8F       1
#endif

#ifndef BICUBIC_PIX_FORMAT_FP16
#define BICUBIC_PIX_FORMAT_FP16      2
#endif

#ifndef BICUBIC_PIX_FORMAT_U16F
#define BICUBIC_PIX_FORMAT_U16F      3
#endif

#ifndef BICUBIC_BAYER_MODE_NORMAL
#define BICUBIC_BAYER_MODE_NORMAL    0
#endif

#ifndef BICUBIC_BAYER_MODE_INVERT
#define BICUBIC_BAYER_MODE_INVERT    1
#endif

#ifndef BICUBIC_BAYER_MODE_ODD
#define BICUBIC_BAYER_MODE_ODD       2
#endif

typedef struct
{
    int width;
    int height;
    u64 bic_id;
    u64 bic_xy_pixel_list_base;
    u64 pixel_format;
    u64 bpp;
    u64 bic_line_length;
    u64 bic_input_base_addr;
    u64 bic_input_top_addr;
    u64 bic_input_chunk_stride;
    u64 bic_input_chunk_width;
    u64 bic_input_line_stride;
    u64 bic_output_base_addr;
    u64 bic_output_chunk_stride;
    u64 bic_output_chunk_width;
    u64 bic_border_pixel;
    u64 bic_clamp_max;
    u64 bic_clamp_min;
    u64 irq_en;
} tBicubicSetup;

/// @}
#endif // __BICUBIC_WARP_API_DEFINES_H__
