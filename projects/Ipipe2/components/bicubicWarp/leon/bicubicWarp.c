///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     bicubicWarp API
///

#include <stdio.h>
#include <mv_types.h>
#include <DrvCpr.h>
#include <registersMyriad.h>
#include <VcsHooksApi.h>
#include <DrvTimer.h>
#include <math.h>
#include <DrvDdr.h>

#include "bicubicWarpApi.h"
#include "bicubicWarpApiDefines.h"

//static tyTimeStamp timer_data;

// Bicubic interrupt handler
/*static*/ volatile int done = 0;
void bicubicIrqHandler() 
{
    //u32 cycles_elapsed = DrvTimerElapsedTicks(&timer_data);
    //char buf[40];
    //sprintf(buf, "bicubicIrqHandler: total cycles_elapsed: %d", cycles_elapsed);
    //vcsFastPuts(buf);
    done = 1;
}

// Initialize bicubicWarp component
void bicubicWarpInit(void * bicubicIrqHandler, u32 irqNumber)
{
    DrvIcbSetupIrq(irqNumber, 7, POS_EDGE_INT, bicubicIrqHandler);
    DrvIcbEnableIrq(irqNumber);
}

// Generate mesh for rotation and translation relative to the center of the image
void bicubicWarpGenerateMeshRT(fp32* xyRectifiedBuffer, const fp32 *matrixRT, u32 width, u32 height) 
{
    u32 y, x;
    fp32 coord;
    fp32 x0 = 0.5f * width;
    fp32 y0 = 0.5f * height;

    fp32* xyRectified = &xyRectifiedBuffer[0];

    for (y = 0; y < height; y++)
    {
        for (x = 0; x < width; x++)
        {
            coord = x0 + (x-x0) * matrixRT[0] + (y-y0) * matrixRT[1] + matrixRT[2];
            *xyRectified = coord;
            xyRectified++;
            coord = y0 + (x-x0) * matrixRT[3] + (y-y0) * matrixRT[4] + matrixRT[5];
            *xyRectified = coord;
            xyRectified++;
        }
    }
}

// Generate mesh for rotation, translation and projection relative to the top-left corner
void bicubicWarpGenerateMeshHomographyRTP(fp32* xyRectifiedBuffer, const fp32 *matrixRTP, u32 width, u32 height) 
{
    u32 y, x;
    fp32 coord;
    fp32 w;

    fp32* xyRectified = &xyRectifiedBuffer[0];

    for (y = 0; y < height; y++)
    {
        for (x = 0; x < width; x++)
        {
            w = x * matrixRTP[6] + y * matrixRTP[7] + matrixRTP[8];
            coord = x * matrixRTP[0] + y * matrixRTP[1] + matrixRTP[2];
            *xyRectified = coord/w;
            xyRectified++;
            w = x * matrixRTP[6] + y * matrixRTP[7] + matrixRTP[8];
            coord = x * matrixRTP[3] + y * matrixRTP[4] + matrixRTP[5];
            *xyRectified = coord/w;
            xyRectified++;
        }
    }
    //dumpMemoryToFile((u32)&xyRectifiedBuffer[0], 640*480*sizeof(fp32)*2);
}

void bicubicWarpStart(u8* input, u8* output, fp32* xyRectifiedBuffer, u32 width, u32 height, u32 bpp, u32 pixelFormat)
{
    u64 bicubicCtrl;
    tBicubicSetup BicSetup;

    done=0;
    BicSetup.bpp = bpp;
    BicSetup.width = width;
    BicSetup.height = height;
    BicSetup.bic_id = 0xa0;
    BicSetup.bic_line_length = width * height;
    BicSetup.bic_input_chunk_width = width;
    BicSetup.bic_input_chunk_stride = width * bpp;
    BicSetup.bic_input_line_stride = width * bpp;
    BicSetup.bic_output_chunk_width = width;
    BicSetup.bic_output_chunk_stride = width * bpp;
    BicSetup.bic_input_base_addr =  (u32)(&input[0]);
    BicSetup.bic_input_top_addr = (u32)(&input[0] + width * height * bpp);
    BicSetup.bic_xy_pixel_list_base = (u32)(&xyRectifiedBuffer[0]);
    BicSetup.bic_output_base_addr = (u32)&output[0];
    BicSetup.bic_border_pixel = 0.0f;
    BicSetup.bic_clamp_min = CLAMP_MIN;
    BicSetup.bic_clamp_max = CLAMP_MAX;

    // Configure Bicubic block
    SET_REG_WORD(BIC_ID_ADR, BicSetup.bic_id);
    SET_REG_WORD(BIC_LINE_LENGTH_ADR, BicSetup.bic_line_length);
    SET_REG_WORD(BIC_INPUT_HEIGHT_ADR, BicSetup.height);
    SET_REG_WORD(BIC_XY_PIXEL_LIST_BASE_ADR, BicSetup.bic_xy_pixel_list_base);

    SET_REG_WORD(BIC_INPUT_BASE_ADR, BicSetup.bic_input_base_addr);
    SET_REG_WORD(BIC_INPUT_TOP_ADR, BicSetup.bic_input_top_addr);
    SET_REG_WORD(BIC_INPUT_CHUNK_STRIDE_ADR, BicSetup.bic_input_chunk_stride);
    SET_REG_WORD(BIC_INPUT_CHUNK_WIDTH_ADR, BicSetup.bic_input_chunk_width);
    SET_REG_WORD(BIC_INPUT_LINE_STRIDE_ADR, BicSetup.bic_input_line_stride);
    SET_REG_WORD(BIC_INPUT_LINE_WIDTH_ADR, BicSetup.width);

    SET_REG_WORD(BIC_OUTPUT_BASE_ADR, BicSetup.bic_output_base_addr);
    SET_REG_WORD(BIC_OUTPUT_CHUNK_STRIDE_ADR, BicSetup.bic_output_chunk_stride);
    SET_REG_WORD(BIC_OUTPUT_CHUNK_WIDTH_ADR, BicSetup.bic_output_chunk_width);

    SET_REG_WORD(BIC_CLAMP_MAX_ADR, BicSetup.bic_clamp_max);
    SET_REG_WORD(BIC_CLAMP_MIN_ADR, BicSetup.bic_clamp_min);

    bicubicCtrl = 0;
    bicubicCtrl |= (pixelFormat << BICUBIC_PIX_FORMAT_SHIFT) & BICUBIC_PIX_FORMAT_MASK;
    bicubicCtrl |= (1 << BICUBIC_IRQ_SHIFT) & BICUBIC_IRQ_MASK;
    bicubicCtrl |= (1 << BICUBIC_RUN_SHIFT) & BICUBIC_RUN_MASK;
    bicubicCtrl |= (1 << BICUBIC_BYPASS_SHIFT) & BICUBIC_BYPASS_MASK;
    bicubicCtrl |= (1 << BICUBIC_BORDER_MODE_SHIFT) & BICUBIC_BORDER_MODE_MASK;
    bicubicCtrl |= (1 << BICUBIC_CLAMP_SHIFT) & BICUBIC_CLAMP_MASK;
    bicubicCtrl |= (1 << BICUBIC_BILINEAR_SHIFT) & BICUBIC_BILINEAR_MASK;

    // Start bicubic filter
    SET_REG_WORD(BIC_CTRL_ADR, bicubicCtrl);
}
void bicubicWarpWait()
{
#if 0
    while (1)
    {
        if( done )
            break;
    }
#else
    uint32_t status;
    do{
        status = GET_REG_WORD_VAL(BIC_CTRL_ADR);
        NOP;NOP;NOP;NOP;NOP; NOP;NOP;NOP;NOP;NOP;
        NOP;NOP;NOP;NOP;NOP; NOP;NOP;NOP;NOP;NOP;
    }while(status & (0x1<<4)); //while running
#endif
}

// Apply bicubic filter on frame
void bicubicWarpProcessFrame(u8* input, u8* output, fp32* xyRectifiedBuffer, u32 width, u32 height, u32 bpp, u32 pixelFormat)
{
    bicubicWarpStart(input, output, xyRectifiedBuffer, width, height, bpp, pixelFormat);
    bicubicWarpWait();
}
