ifeq ($(LEON_RT_BUILD),yes)
	PlgWarp_OBJECTS=RAWDATAOBJECTFILES
else
	PlgWarp_OBJECTS=RAWDATAOBJECTFILES
endif

MVCV_KERNELS_USED = warpMeshExpand warpMeshSample8bit

MVCV_KERNELS_PATH_UP =  $(MV_COMMON_BASE)/components/kernelLib/MvCV/kernels
MVCV_KERNELS_PATH_01 += $(patsubst %, $(MVCV_KERNELS_PATH_UP)/%/shave/src, $(MVCV_KERNELS_USED))
MVCV_KERNELS_PATH_02 += $(patsubst %, $(MVCV_KERNELS_PATH_UP)/%/arch/ma2x5x/shave/src, $(MVCV_KERNELS_USED))


SHAVE_ASM_SOURCES_MVCV_PlgWarpShv += $(foreach var, $(MVCV_KERNELS_PATH_01),$(wildcard $(var)/*.asm))
SHAVE_ASM_SOURCES_MVCV_PlgWarpShv += $(foreach var, $(MVCV_KERNELS_PATH_02),$(wildcard $(var)/*.asm))
#SHAVE_ASM_SOURCES_MVCV_PlgWarpShv += $(wildcard $(MVCV_KERNELS_PATH_01)/*.asm)
#SHAVE_ASM_SOURCES_MVCV_PlgWarpShv += $(wildcard $(MVCV_KERNELS_PATH_02)/*.asm)


MVCV_KERNELS_PATH_INCLUDE = $(patsubst %, $(MVCV_KERNELS_PATH_UP)/%/shave/include, $(MVCV_KERNELS_USED))
MVCV_KERNELS_PATH_INCLUDE +=  $(MVCV_KERNELS_PATH_UP)/../include
#MVCV_KERNELS_PATH_INCLUDE += $(IPIPE_BASE)/plugins/PlgWarp/shared

MVCV_KERNELS_INCLUDE += $(patsubst %,-I %, $(MVCV_KERNELS_PATH_INCLUDE))

MVCCOPT += $(MVCV_KERNELS_INCLUDE)

PLGWARP_DIR = $(IPIPE_BASE)/plugins/PlgWarp

SHAVE_C_SOURCES_PlgWarpShv = $(wildcard $(MVCV_KERNELS_PATH_UP)/%/shave/src/cpp/*.cpp)
SHAVE_CPP_SOURCES_PlgWarpShv = $(wildcard $(PLGWARP_DIR)/shave/*.cpp)
SHAVE_ASM_SOURCES_PlgWarpShv = $(wildcard $(MVCV_KERNELS_PATH_UP)/%/shave/src/*.asm)
SHAVE_GENASMS_PlgWarpShv = $(patsubst %.cpp,$(DirAppObjBase)%.asmgen,$(SHAVE_C_SOURCES_PlgWarpShv)) \
						   $(patsubst %.cpp,$(DirAppObjBase)%.asmgen,$(SHAVE_CPP_SOURCES_PlgWarpShv))
SHAVE_PlgWarpShv_OBJS = $(patsubst %.asm,$(DirAppObjBase)%_shave.o,$(SHAVE_ASM_SOURCES_PlgWarpShv)) \
						$(patsubst %.asm,$(DirAppObjBase)%_shave.o,$(SHAVE_ASM_SOURCES_MVCV_PlgWarpShv)) \
						$(patsubst $(DirAppObjBase)%.asmgen,$(DirAppObjBase)%_shave.o,$(SHAVE_GENASMS_PlgWarpShv))

ComponentList   += imageWarp

APPNAME_WARP = warpYuv

SHV_ID_WARP_Y0 ?= 4
SHV_ID_WARP_Y1 ?= 5
SHV_ID_WARP_C0 ?= 6

CCOPT_LRT += -D'PLGWARP_SHV_L1=$(SHV_ID_WARP_Y0)'
CCOPT_LRT += -D'PLGWARP_SHV_L2=$(SHV_ID_WARP_Y1)'
CCOPT_LRT += -D'PLGWARP_SHV_UV1=$(SHV_ID_WARP_C0)'

SHAVE_APP_LIBS = $(APPNAME_WARP).mvlib

SHAVE$(SHV_ID_WARP_Y0)_APPS = $(APPNAME_WARP).shv$(SHV_ID_WARP_Y0)lib
SHAVE$(SHV_ID_WARP_Y1)_APPS = $(APPNAME_WARP).shv$(SHV_ID_WARP_Y1)lib
SHAVE$(SHV_ID_WARP_C0)_APPS = $(APPNAME_WARP).shv$(SHV_ID_WARP_C0)lib

PROJECTCLEAN += $(SHAVE_PlgWarpShv_OBJS) $(SHAVE_GENASMS_PlgWarpShv) $(SHAVE_APP_LIBS) \
				$(APPNAME_WARP).shv$(SHV_ID_WARP_Y0)lib $(APPNAME_WARP).shv$(SHV_ID_WARP_Y1)lib \
				$(APPNAME_WARP).shv$(SHV_ID_WARP_C0)lib \
				$(APPNAME_WARP).shv$(SHV_ID_WARP_Y0)tail $(APPNAME_WARP).shv$(SHV_ID_WARP_Y1)tail \
				$(APPNAME_WARP).shv$(SHV_ID_WARP_C0)tail \
				$(APPNAME_WARP).shv$(SHV_ID_WARP_Y0)templib $(APPNAME_WARP).shv$(SHV_ID_WARP_Y1)templib \
				$(APPNAME_WARP).shv$(SHV_ID_WARP_C0)templib

#PROJECTINTERM += $(SHAVE_GENASMS_PlgWarpShv)
