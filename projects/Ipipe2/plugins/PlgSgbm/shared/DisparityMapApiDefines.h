///
/// @file DisparityMapConfig.h
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief DisparityMapConfig structures
///

#ifndef DISPARITY_MAP_CONFIG_H
#define DISPARITY_MAP_CONFIG_H

#include <mv_types.h>
#include <swcFrameTypes.h>

// Algorithm parameters
#define CENSUS_KERNEL_SIZE      5
#define MEDIAN_KERNEL_SIZE      5
#define LINE_PADDING            2   // Line padding for Census 5x5 kernel
#define MEDIAN_PADDING          2   // Line padding for Median 5x5 filter

#ifdef __MOVICOMPILE__
const u32 paddingForDmaTransfer = MEDIAN_PADDING; // needed in case of using HW median filter
#endif

#ifndef SGBM_DMA_AGENT
#define SGBM_DMA_AGENT (1)
#endif

typedef struct
{
    u8*     data;
    u32     w;
    u32     h;
    u32     step;
} MatImage;

typedef struct
{
    u32 no;             /* tile number */
    u32 x;              /* x coordinate for beginning of the patch */
    u32 y;              /* y coordinate for beginning of the patch */
    u32 width;          /* patch width */
    u32 height;         /* patch height */
    u32 overlapW;       /* width overlap for census */
    u32 overlapH;       /* height overlap for census */
    u32 overlapPatch;   /* patch overlap taking into account disparity range */
}Tile_t;

// DisparityConfig structure holds the needed buffers for input and output images
typedef struct
{
    Tile_t*         tile;                   /* tile configuration */

    frameBuffer     leftImage;              /* tile from left image for specific level */
    frameBuffer     rightImage;             /* tile from right image for specific level */
    u8*             disparityMap;           /* tile disparity map for specific level */
    u8*             disparityMapFinal;      /* total disparity map final level */

    u32             pyramidLevels;          /* number of pyramid levels */

} DisparityConfig;

// AlgorithmConfig structure holds the configuration
typedef struct
{
    s32 cfgCensusKernelSize;    /* census kernel size; default value = CENSUS_KERNEL_SIZE */
    s32 cfgMedianKernelSize;    /* median kernel size; default value = MEDIAN_KERNEL_SIZE */
    s32 cfgLinePadding;         /* size of line padding  (half of census kernel); default value = LINE_PADDING */
    s32 cfgMedianPadding;       /* size of line padding for median filtering (half of median kernel); default value = MEDIAN_PADDING */
    s32 cfgMaxDisparities;      /* maximum disparity; default value = MAX_DISPARITIES */

} AlgorithmConfig;

// DisparityConfig structure holds the needed buffers for input and output images
typedef struct
{
    u8*             censusCost;         /* input left image */
    u8*             aggregatedCost;     /* input right image */
    u32*            penaltyTableP1;     /* output disparity map */
    u32*            penaltyTableP2;     /* output disparity map */
    u32             currentBuffer;      /* patch number */
    u32             currentLine;
    u32             width;

} SGBMConfig;

#endif // DISPARITY_MAP_CONFIG_H
