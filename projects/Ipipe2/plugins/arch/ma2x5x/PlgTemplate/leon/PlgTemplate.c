/**************************************************************************************************

 @File         : PlgTemplate.c
 @Author       : MT
 @Brief        : Contain Dummy Example plug-in interface
 Date          : 29 - June - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : © Movidius Srl 2014, © Movidius Ltd 2015

 Description :
     This plug-in just copy a frame from input place to output place. His scope is to allow easy
     understand plug-in framework interface.

     Resources used:
         Leon

     No interrupt base, used Leon Rt for a full frame time processing time

 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 ***************************************************************************************************/
#include "TimeSyncMgr.h"
#include "PlgTemplateApi.h"
#include "FrameMgrApi.h"
#include <DrvTimer.h>
#include "swcLeonUtils.h"
#include <string.h>
#include "assert.h"


/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/
enum {
    PLGSAMPLE_STATS_OFF = 0,
    PLGSAMPLE_STATS_ON  = 1
};


/**************************************************************************************************
 ~~~  Local variables
 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Local File function declarations
 **************************************************************************************************/

static void producedInputFrame(FrameT *frame, void *pluginObject);
static void specificPlgInit (icSize iframeSize, void *pluginObject);
static void specificPlgInit (icSize iframeSize, void *pluginObject);
static int32_t init(FramePool *outputPools, int nOutputPools, void *pluginObject);
static int32_t  fini(void *pluginObject);
/**************************************************************************************************
 ~~~ Exported Functions Implementation
 **************************************************************************************************/
void PlgTemplateCreate(void *pluginObject) {
    PlgTemplate *object = (PlgTemplate*)pluginObject;
// init hw things, or all the init side that not need params,
    object->plg.init       = init;
    object->plg.fini       = fini;
    object->init        = specificPlgInit;
    object->cbList[0].callback     = producedInputFrame;
    object->cbList[0].pluginObj    = pluginObject;
    object->plg.callbacks  = object->cbList;
    object->plg.status     = PLG_STATS_IDLE;
    object->crtStatus      = PLGSAMPLE_STATS_OFF;
}

//
static int32_t  fini(void *pluginObject) {
    PlgTemplate *object = (PlgTemplate*)pluginObject;
    object->crtStatus      = PLGSAMPLE_STATS_OFF;
    return 0;
}

//
static int32_t init(FramePool *outputPools, int nOutputPools, void *pluginObject) {
    PlgTemplate* object = (PlgTemplate*)pluginObject;
    // this plugin produce just 1 output frame, so not take in consideration nOutputPools params,
    // as this have to be 1
    object->outputPools = outputPools;
    return 0;
}

//
static void specificPlgInit (icSize iframeSize, void *pluginObject) {
    PlgTemplate *object = (PlgTemplate *) pluginObject;
    object->privateCfg.w = iframeSize.w;
    object->privateCfg.h = iframeSize.h;
    // do things that is about init side.
    object->crtStatus      = PLGSAMPLE_STATS_ON;
}

//
static void producedInputFrame(FrameT *frame, void *pluginObject) {
    PlgTemplate* object        = (PlgTemplate*)pluginObject;

    // in case of isp video, configuration params are taken from input frame strut
    // this examples don't have any params
    //ispObject->privateIspConfigPointer.ispParams = frame->appSpecificData;

    assert(PLG_STATS_IDLE == object->plg.status);

    // fini suport, assuming that finish is not possible to stop immediately
    if(PLGSAMPLE_STATS_ON == object->crtStatus) {
        object->plg.status = PLG_STATS_RUNNING;
        // skip frame if isp config doesn't exist can be implemented here in case of isp plugin
        //if (NULL == ispObject->privateIspConfigPointer.ispParams) {
        //    FrameMgrReleaseFrame(frame);
        //    ispObject->plg.status = PLG_STATS_IDLE;
        //    return;
        //}
        FrameT *oFrame = FrameMgrAcquireFrame(object->outputPools);
        // skip frame if no more output buffer available
        if (NULL == oFrame) { // release input frame and exit, frame will be dropped
            FrameMgrReleaseFrame(frame);
            //assert(oFrame != NULL); // TODO: delete debug code
            object->plg.status = PLG_STATS_IDLE;
            return;
        }

        oFrame->appSpecificData = frame->appSpecificData;

        if (object->procesStart) {
            object->procesStart(oFrame->seqNo);
        }

        // the real processing part
        //copy frames in this demo with leon
        memcpy(oFrame->fbPtr[0], frame->fbPtr[0], object->privateCfg.w*object->privateCfg.h*2);

        if (object->procesEnd) {
            object->procesEnd(oFrame->seqNo);
        }
        // new frame produced, updating times informations
        FrameMgrAddTimeStampHist(oFrame, frame);
        FrameMgrAndAddTimeStamp(oFrame, TimeSyncMsgGetTimeUs());

        FrameMgrReleaseFrame(frame);
        FrameMgrProduceFrame(oFrame);
        object->plg.status = PLG_STATS_IDLE;
    }
}



