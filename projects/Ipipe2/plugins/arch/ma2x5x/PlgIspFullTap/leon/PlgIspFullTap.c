/**************************************************************************************************

 @File         : PlgIspFullApi.c
 @Author       : AL
 @Brief        : Contain Opipe full Isp plug-in implementation
 Date          : 01 - March - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : © Movidius Srl 2014, © Movidius Ltd 2015

 Description :

 **************************************************************************************************/

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include "DrvTimer.h"
#include "swcLeonUtils.h"
#include "Opipe.h"
#include "TimeSyncMgr.h"
#include "PlgIspFullTapApi.h"
#include "FrameMgrApi.h"
#include "ipipeDbg.h"
#include "ipipeUtils.h"


/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/
//Plugin status
enum {
    PLG_OFF = 0,
    PLG_ON  = 1
};

/**************************************************************************************************
 ~~~ Local Functions Implementation
 **************************************************************************************************/

//###################################################################################
static void plgIspFullSetParams(PlgIspFullTap *obj) {

    Opipe       *p  = &obj->op.p;
    icIspConfig *ic =  obj->ispCfg;

    fetchIcIspConfig(p, ic); //icIspConfig -> Opipe translation
    computePolyfirParams(&obj->scale, ic);

    //Default CFG words:
    OpipeDefaultCfg(p, SIPP_SIGMA_ID  );
    OpipeDefaultCfg(p, SIPP_RAW_ID    );
    OpipeDefaultCfg(p, SIPP_DBYR_ID   );
    OpipeDefaultCfg(p, SIPP_DOGL_ID   );
    OpipeDefaultCfg(p, SIPP_SHARPEN_ID);
    OpipeDefaultCfg(p, SIPP_CGEN_ID   );
    OpipeDefaultCfg(p, SIPP_MED_ID    );
    OpipeDefaultCfg(p, SIPP_CHROMA_ID );
    OpipeDefaultCfg(p, SIPP_CC_ID     );
    OpipeDefaultCfg(p, SIPP_LUT_ID    );

   //Default chroma bits = 12, if 8bit RGB tap-out is needed, must patch a few regs
    if(obj->tapParam.mode == TAP_RGB)
     if(obj->tapParam.bpp == 1/*byte*/)
     {
      //override DBYR.CHROMA_BITS
        p->cfg[SIPP_DBYR_ID] &= ~(0xF <<8); //clear old value
        p->cfg[SIPP_DBYR_ID] |= ((8-1)<<8); //set   new value
      //override CGEN.CHROMA_BITS
        p->cfg[SIPP_CGEN_ID] &= ~(0xF <<24);//clear old value
        p->cfg[SIPP_CGEN_ID] |= ((8-1)<<24);//set   new value
     }
}

//###################################################################################
int32_t cbTrigerCapture(FrameT *frame, void *params, int (*callback)(int status), void *pluginObj)
{
    icSize size;
    PlgIspFullTap* obj = (PlgIspFullTap*)pluginObj;
    assert(frame);
    assert(params);

    if(PLG_ON == obj->crtStatus)
    {
        obj->plg.status = PLG_STATS_RUNNING;
        obj->ispCfg = (icIspConfig  *)params;

       //Get a frame from each output mempool
        FrameT *oFrameYuv = FrameMgrAcquireFrame(&obj->outputPools[0]);
        FrameT *oFrameTap = FrameMgrAcquireFrame(&obj->outputPools[1]);

        //Skip input frame if no more output buffer available
        if((NULL == oFrameYuv)||(NULL == oFrameTap)){
            FrameMgrReleaseFrame(frame);
            obj->plg.status = PLG_STATS_IDLE;
            return -1;
        }
        oFrameYuv->appSpecificData = frame->appSpecificData;
        oFrameTap->appSpecificData = frame->appSpecificData;

        //Else, we can process
        if (obj->procesStart) obj->procesStart((void*)obj, oFrameYuv->seqNo, obj->ispCfg->userData);

        //From currently set "icIspConfig" to Opipe regs
        plgIspFullSetParams(obj);
        OpipeSetSizeTap (&obj->op, obj->frmSz.w, obj->frmSz.h);

        //Frame pointers:
        obj->op.pIn->ddr.base         = (uint32_t)frame    ->fbPtr[0];
        obj->op.pOutY->ddr.base       = (uint32_t)oFrameYuv->fbPtr[0]; //outYuv420.Y
        obj->op.pOutUV->ddr.base      = (uint32_t)oFrameYuv->fbPtr[1]; //outYuv420.UV
        obj->op.pOutTap.any->ddr.base = (uint32_t)oFrameTap->fbPtr[0]; //tapOut.RGB/Y

        //References needed in Opipe EOF handler to update plugin info
        obj->op.p.params[0] = (void*)obj;
        obj->op.p.params[1] = (void*)frame;
        obj->op.p.params[2] = (void*)oFrameYuv;
        obj->op.p.params[3] = (void*)oFrameTap;
        //The kick
        OpipeStart(&obj->op.p);
    }
    return 0;
}

//###################################################################################
static void cbNewInputFrame(FrameT *iFrame, void *pluginObject) {
    PlgIspFullTap* obj = (PlgIspFullTap*)pluginObject;
    assert(iFrame);
    if(PLG_ON == obj->crtStatus)
    {
        obj->plg.status = PLG_STATS_RUNNING;

        if(iFrame->appSpecificData) {
            obj->ispCfg = (icIspConfig  *)iFrame->appSpecificData;
        }
        else {
            FrameMgrReleaseFrame(iFrame);
            obj->plg.status = PLG_STATS_IDLE;
            return;
        }

       //Else, can try capture (will succeed if oFrame can be allocated)
        cbTrigerCapture(iFrame, obj->ispCfg, NULL, pluginObject);
    }
}

//###################################################################################
//Opipe EOF callback: adjust associated Plugin Frame buffers
void opipeIspEof(Opipe *p)
{
    PlgIspFullTap *obj = (PlgIspFullTap *)p->params[0];
    FrameT     *iFrame = (FrameT        *)p->params[1];
    FrameT  *oFrameYuv = (FrameT        *)p->params[2]; //yuv420
    FrameT  *oFrameTap = (FrameT        *)p->params[3]; //tap frame

    assert(iFrame);
    assert(oFrameYuv);
    assert(oFrameTap);

  //Set correct resolution to out frame (now that frame's produced out res is also known)
    assert(FRAME_T_FORMAT_YUV420 == oFrameYuv->type);
    oFrameYuv->stride[0] = obj->op.p.pUpfirdn0Cfg->oW;
    oFrameYuv->height[0] = obj->op.p.pUpfirdn0Cfg->oH;
    oFrameYuv->stride[1] = obj->op.p.pUpfirdn12Cfg->oW<<1;//*2 (got 2 UV planes)
    oFrameYuv->height[1] = obj->op.p.pUpfirdn12Cfg->oH;

  //Set resolution of tap frame
    oFrameTap->stride[0] = obj->op.p.width;
    oFrameTap->height[0] = obj->op.p.height;

  //dbg :if(obj->op.p.flags & MISSING_DATA)
    if (obj->procesEnd)
        obj->procesEnd((void*)obj, oFrameYuv->seqNo, obj->ispCfg->userData);

  //New frame produced, updating times informations
    FrameMgrAddTimeStampHist(oFrameYuv, iFrame);
    FrameMgrAddTimeStampHist(oFrameTap, iFrame);

  //Both frames should have same time stamp
    icTimestamp ts = TimeSyncMsgGetTimeUs();
    FrameMgrAndAddTimeStamp(oFrameYuv, ts);
    FrameMgrAndAddTimeStamp(oFrameTap, ts);

  //Release input frame; was processed
    FrameMgrReleaseFrame(iFrame);

  //Mark output frames being produced
    FrameMgrProduceFrame(oFrameYuv);
    FrameMgrProduceFrame(oFrameTap);

    obj->plg.status = PLG_STATS_IDLE;
}

//###################################################################################
static int32_t fini(void *pluginObject) {
    PlgIspFullTap *object = (PlgIspFullTap*)pluginObject;
    object->crtStatus  = PLG_OFF;
    return 0;
}

//###################################################################################
static int32_t init(FramePool *outputPools, int nOutputPools, void *pluginObject) {
    PlgIspFullTap* obj = (PlgIspFullTap*)pluginObject;
    // this plugin produce just 1 output frame, so not take in consideration nOutputPools params,
    // as this have to be 1
    obj->outputPools = outputPools;
    return 0;
}

//###################################################################################
//Opipe Full Isp yuv420 output creation
static void CreateOpipe(PlgIspFullTap *o)
{
    OpipeTap *op = &o->op;

    if(o->cSigma   == NULL) o->cSigma   = ogcBuff[SIPP_MIPI_RX0_ID ];
    if(o->cDbyrY   == NULL) o->cDbyrY   = ogcBuff[SIPP_DBYR_LUMA_ID]; //used when tap.Y
    if(o->cDbyrRgb == NULL) o->cDbyrRgb = ogcBuff[SIPP_DBYR_ID     ]; //used when tap.RGB
    if(o->cSharpY  == NULL) o->cSharpY  = ogcBuff[SIPP_SHARPEN_ID  ];
    if(o->cLut     == NULL) o->cLut     = ogcBuff[SIPP_LUT_ID      ];
    if(o->cUpfirDn == NULL) o->cUpfirDn = ogcBuff[SIPP_UPFIRDN0_ID ];

    assert(o->cSigma  != NULL);
    assert(o->cDbyrY  != NULL);
    assert(o->cSharpY != NULL);
    assert(o->cLut    != NULL);
    assert(o->cUpfirDn!= NULL);

  //Must specify buffers first
    op->in.cBufSigma.base = (uint32_t)o->cSigma;   op->in.cBufSigma.h = I_CBUFF_H;
    op->cBufDbyrY.base    = (uint32_t)o->cDbyrY;   op->cBufDbyrY.h    = DBYR_Y_H;
    op->cBufSharp.base    = (uint32_t)o->cSharpY;  op->cBufSharp.h    = SHARP_Y_H;
    op->cBufLut.base      = (uint32_t)o->cLut;     op->cBufLut.h      = LUT_H;
    op->cBufPoly.base     = (uint32_t)o->cUpfirDn; op->cBufPoly.h     = O_CBUFF_H;

    if(o->tapParam.mode == TAP_RGB)
    {
       assert(o->cDbyrRgb != NULL);
       op->cBufDbyrRgb.base = (uint32_t)o->cDbyrRgb; op->cBufDbyrRgb.h = O_CBUFF_H;
    }

    OpipeCreateDbyrTap(op, o->tapParam);
    initLutCopyTask();
}

//###################################################################################
//Opipe related: returns Opipe circular buffer requirements in bytes.
//WARNING: using SIPP_MIPI_RX0_ID for Sigma Input !
// wSig  : width for RAW filters (Sigma, Lsc, Raw, Dbyr input)
// wMain : width for filters below debayer (can be wSig/2 if preview is enabled)
// wPoly : width for Polyfir output (based on associated source max N,D params)
// sz    : required circular-output-buffer size in bytes
static void QueryCircBuffSizes(PlgIspBase *me, uint32_t wSig, uint32_t wMain, uint32_t wPoly, uint32_t* req)
{
   TapParam *prm = (TapParam*)me->params;

  //Enlarge line widths a bit to accommodate internal padding:
   wSig  +=  8;
   wMain +=  8;
   wPoly += 16;

  //                         height    * pl * width * bpp
   req[SIPP_MIPI_RX0_ID ] =  I_CBUFF_H      * wSig     ; // bpp:N/A
   req[SIPP_DBYR_LUMA_ID] =  DBYR_Y_H       * wMain * 2; // bpp:2(fp16)
   req[SIPP_SHARPEN_ID  ] =  SHARP_Y_H      * wMain * 2; // bpp:2(fp16)
   req[SIPP_LUT_ID      ] =  LUT_H     * 3  * wMain * 1; // bpp:1(  u8)
   req[SIPP_UPFIRDN0_ID ] =  O_CBUFF_H * 2  * wPoly * 1; // bpp:1(  u8)

   if(prm->mode == TAP_RGB){
     req[SIPP_DBYR_ID]    =  O_CBUFF_H * 3  * wMain * prm->bpp;
   }
}

/**************************************************************************************************
 ~~~ Exported Functions Implementation
 **************************************************************************************************/

//Note: before Config, plg::tapParam needs to be set !
void PlgIspFullTapConfig(void *plgObject, icSize frameSz)
{
    PlgIspFullTap *obj   = (PlgIspFullTap *)plgObject;
    obj->frmSz  = frameSz;
    //Must know resolution in order do create an Opipe object
    if (PLG_ISPFULL_CREATED == obj->status) {
        CreateOpipe(obj);
        obj->status = PLG_ISPFULL_INUSE;
    }
    else {
        if(PLG_ISPFULL_INUSE == obj->status) {
            //Assume that change resolution, so update it
            //Resolution updates (if needed)
            //OpipeSetRes(&obj->op.p, frameSz.w, frameSz.h);
        }
        else {
            // try to start a source but not create is first,
            // or a memory corruption overwrite plug-in structure
            assert(0);
        }
    }

    obj->crtStatus = PLG_ON;

    //In Opipe EOF callback, will update Ipipe Frame buffers status
    obj->op.p.cbEndOfFrame = opipeIspEof;
}

//###################################################################################
void PlgIspFullTapCreate(void *pluginObject)
{
  #if !defined(TAPOUT_EN)
    assert(0); //Opipe flag must be defined !!!
  #endif

  #if defined(OPIPE_RUNTIME_CHECKS)
    assert(0); //this kills the tapout-dmaSink
               //test would hang with this ON, as DBYR will never update OBFL,
               //an so will prematurely terminate
  #endif

    memset(pluginObject, 0, sizeof(PlgIspFullTap));
    PlgIspFullTap *object       = (PlgIspFullTap*)pluginObject;
    // init hw things, or all the init side that not need params,
    object->plg.init            = init; //to associate output pool
    object->plg.fini            = fini; //to mark STOP
    object->plg.trigger         = cbTrigerCapture;
    object->cbList[0].callback  = cbNewInputFrame;
    object->cbList[0].pluginObj = pluginObject;
    object->plg.callbacks       = object->cbList;
    object->plg.status          = PLG_STATS_IDLE;
    object->crtStatus           = PLG_OFF;
    object->status              = PLG_ISPFULL_CREATED;
    object->base.fnQueryMemReq  = QueryCircBuffSizes; /*query*/
    object->base.params         = &object->tapParam;  /*query*/
    computePolyFirInit(&object->scale);
}
