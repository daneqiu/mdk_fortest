/**************************************************************************************************

 @File         : PlgIspBayerMonoApi.h
 @Author       : Florin Cotoranu
 @Brief        : Contain Opipe bayer mono Isp plug-in interface
 Date          : 09 - March - 2016
 E-mail        : florin.cotoranu@movidius.com
 Copyright     : © Movidius Srl 2016, © Movidius Ltd 2016

 Description :

     Resources used:
         Leon RT.
         All sipp isp filters

    Interrupt base.

 **************************************************************************************************/
#ifndef __PLG_ISP_MONO_BAYER_API__
#define __PLG_ISP_MONO_BAYER_API__

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include "PlgTypes.h"
#include "IspCommon.h"
#include "MemMgrApi.h"
#include "Opipe.h"
#include "OpipeApps.h"
#include "IspCommonUtils.h"
#include "ipipe.h"


/**************************************************************************************************
 ~~~  Basic typedefs
 **************************************************************************************************/
// After a stop, at the new start not allocate again the buffers, just change the size
typedef enum {
   PLG_ISPMONOBAYER_NOTMADE = 0,
   PLG_ISPMONOBAYER_CREATED = 1,
   PLG_ISPMONOBAYER_INUSE = 2
}PlgIspMonoBayerStatus;

typedef struct PlgIspMonoBayerStruct {
   PlgType    plg;
   PlgIspBase base;


   //Main opipe objects
   OpipeBayerMono op;
   //Required circular buffers
   uint8_t   *cSigma;
   uint8_t   *cDbyr;
   uint8_t   *cUsm;
   uint8_t   *cLut;

   //ISP params
   icIspConfig  *ispCfg;
   icSize        frmSz; //input frame size
   //Specific component interface
   void    (*procesStart)    (void* plg, uint32_t seqNr, void *userData);
   void    (*procesEnd  )    (void* plg, uint32_t seqNr, void *userData);
   void    (*procesIspError) (void* plg, icSeverity severity, icError errorNo, void *userData);

   //Private members. All data structures have to be internal
   FramePool               *outputPools;
   volatile int32_t        crtStatus; //internal usage
   FrameProducedCB         cbList[1];
   PlgIspMonoBayerStatus   status;    //used for avid double plug-in initialization start/stop.
} PlgIspMonoBayer;


/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
void PlgIspMonoBayerCreate(void *pluginObject);
void PlgIspMonoBayerConfig(void *pluginObject, icSize frameSz, uint32_t inFmt, uint32_t prevAble);

#endif
