/**************************************************************************************************

 @File         : PlgIspMonoLumaApi.c
 @Author       : Florin Cotoranu
 @Brief        : Contains Opipe mono luma Isp plugin implementation
 Date          : 09 - March - 2016
 E-mail        : florin.cotoranu@movidius.com
 Copyright     : © Movidius Srl 2016, © Movidius Ltd 2016

 Description :

     Plugin does single plane luma Isp

 **************************************************************************************************/

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include <string.h>
#include <assert.h>
#include "DrvTimer.h"
#include "swcLeonUtils.h"
#include "Opipe.h"
#include "TimeSyncMgr.h"
#include "PlgIspMonoLumaApi.h"
#include "FrameMgrApi.h"
#include "ipipeDbg.h"
#include "ipipeUtils.h"


/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/
#ifndef PLG_ISP_MAX_GAMMA_SIZE
#define PLG_ISP_MAX_GAMMA_SIZE (512)
#endif

#ifndef PLG_ISP_GAMMA_SECT
#define PLG_ISP_GAMMA_SECT ".cmx.bss"
#endif

#ifndef PLG_ISP_MAX_LSC_SIZE
#define PLG_ISP_MAX_LSC_SIZE (4096)
#endif

#ifndef PLG_ISP_LSC_SECT
#define PLG_ISP_LSC_SECT ".cmx.bss"
#endif



//Plugin status
enum {
    PLG_OFF = 0,
    PLG_ON  = 1
};

/**************************************************************************************************
 ~~~  Local variables
 **************************************************************************************************/

uint16_t locGammaMono[PLG_ISP_MAX_GAMMA_SIZE] SECTION(PLG_ISP_GAMMA_SECT) ALIGNED(8);
LutCfg   locLutMonoCfg = {0, 0, {0, 0}, (uint16_t*)locGammaMono};

uint16_t locLscMono[PLG_ISP_MAX_LSC_SIZE] SECTION(PLG_ISP_LSC_SECT) ALIGNED(8);
LscCfg   locLscMonoCfg = {0, 0, 0, (uint16_t*)locLscMono};

RawCfg   locRawMonoCfg;


/**************************************************************************************************

  ~~~ Local Functions Implementation
 **************************************************************************************************/

static void fetchIcIspMonoConfig(Opipe *p, icIspConfig *ic)
{
    // Function transfers information from ic -> p.
    // RAW filter gains and clamp hardoded to force 10bit->8bit
    // The LSC uses only green channel.
    // The gamma lut requires only one channel.
    #define RAW_GAIN         (0x100 >> 2) // 10 -> 8 bit
    #define RAW_CLAMP        ((1<<8) - 1) // 10 -> 8 bit
    #define MODULO_4_MASK    0x00000003   // Mask for %4

    uint32_t i, x, y;
    uint32_t lscWidth, lscHeight, lscStride;

    p->format       = PLANAR;
    p->rawBits      = ic->pipelineBits;
    p->bayerPattern = ic->bayerOrder;

    //Filter specific
    p->pBlcCfg        = &ic->blc;
    p->pSigmaCfg      = &ic->sigma;
    p->pLscCfg        = &locLscMonoCfg; //Opipe will use local LscCfg instead of &ic->lsc;
    p->pRawCfg        = &locRawMonoCfg;
    p->pDbyrCfg       = &ic->demosaic;
    p->pLtmCfg        = &ic->ltm;
    p->pDogCfg        = &ic->dog;
    p->pLumaDnsCfg    = &ic->lumaDenoise;
    p->pLumaDnsRefCfg = &ic->lumaDenoiseRef;
    p->pSharpCfg      = &ic->sharpen;
    p->pChrGenCfg     = &ic->chromaGen;
    p->pMedCfg        = &ic->median;
    p->pChromaDnsCfg  = &ic->chromaDenoise;
    p->pColCombCfg    = &ic->colorCombine;
    p->pLutCfg        = &locLutMonoCfg; //Tell Opipe to use local LutCfg instead of &ic->gamma;
    p->pColConvCfg    = &ic->colorConvert;
    p->aeCfg          = &ic->aeAwbConfig;
    p->aeStats        = ic->aeAwbStats;
    p->afCfg          = &ic->afConfig;
    p->afStats        =  ic->afStats;
    p->pUpfirdn0Cfg   = &ic->updnCfg0;
    p->pUpfirdn12Cfg  = &ic->updnCfg12;

    // RAW. Adjust for 8bit output
    locRawMonoCfg.gainGr = RAW_GAIN;
    locRawMonoCfg.gainR  = RAW_GAIN;
    locRawMonoCfg.gainB  = RAW_GAIN;
    locRawMonoCfg.gainGb = RAW_GAIN;

    locRawMonoCfg.clampGr = RAW_CLAMP;
    locRawMonoCfg.clampR  = RAW_CLAMP;
    locRawMonoCfg.clampB  = RAW_CLAMP;
    locRawMonoCfg.clampGb = RAW_CLAMP;

    locRawMonoCfg.grgbImbalPlatDark    = 0;
    locRawMonoCfg.grgbImbalDecayDark   = 0;
    locRawMonoCfg.grgbImbalPlatBright  = 0;
    locRawMonoCfg.grgbImbalDecayBright = 0;
    locRawMonoCfg.grgbImbalThr         = 0;

    // Equalized hot/cold pixels
    locRawMonoCfg.dpcAlphaHotG   = ic->raw.dpcAlphaHotG;
    locRawMonoCfg.dpcAlphaHotRb  = ic->raw.dpcAlphaHotG;
    locRawMonoCfg.dpcAlphaColdG  = ic->raw.dpcAlphaColdG;
    locRawMonoCfg.dpcAlphaColdRb = ic->raw.dpcAlphaColdG;
    locRawMonoCfg.dpcNoiseLevel  = ic->raw.dpcNoiseLevel;
    locRawMonoCfg.outputBits     = 8;

    // LUT. Only one channel is used
    locLutMonoCfg.size       = ic->gamma.size;
    locLutMonoCfg.rgnSize[0] = ic->gamma.rgnSize[0];
    locLutMonoCfg.rgnSize[1] = ic->gamma.rgnSize[1];
    for(i = 0; i < locLutMonoCfg.size; i++)
    {
        locGammaMono[i] = ic->gamma.table[i*4];
    }

    // LSC. Only green channel is used
    lscWidth = ic->lsc.lscWidth / 2;
    lscHeight = ic->lsc.lscHeight / 2;
    lscStride = lscWidth + (lscWidth & MODULO_4_MASK); // Stride must be aligned to 64bit

    // Store parameters
    locLscMonoCfg.lscWidth = lscWidth;
    locLscMonoCfg.lscStride = lscStride;
    locLscMonoCfg.lscHeight = lscHeight;
    if((p->bayerPattern == RGGB) || (p->bayerPattern == BGGR))
    {
        for (y = 0; y < lscHeight; y++)
        {
            for (x = 0; x < lscStride; x++)
            {
                if(x < lscWidth)
                {
                    // Set green channel
                    locLscMono[(y*lscStride + x)] = ic->lsc.pLscTable[(2*y*lscWidth) + (2*x) + 1];
                }
                else
                {
                    // Clear alignment buffer
                    locLscMono[(y*lscStride + x)] = 0; // Clear alignment buffer
                }
            }
        }
    }
    else // (p->bayerPattern == GRBG) || (p->bayerPattern == GBRG)
    {
        for (y = 0; y < lscHeight; y++)
        {
            for (x = 0; x < lscStride; x++)
            {
                if(x < lscWidth)
                {
                    // Set green channel
                    locLscMono[(y*lscStride + x)] = ic->lsc.pLscTable[(2*y*lscWidth) + (2*x)];
                }
                else
                {
                    // Clear alignment buffer
                    locLscMono[(y*lscStride + x)] = 0;
                }
            }
        }
    }
}

static void plgIspMonoLumaSetParams(PlgIspMonoLuma *obj)
{
    Opipe       *p  = &obj->op.p;
    icIspConfig *ic =  obj->ispCfg;

    // Filter config defines
    #define PASSTROUGH_MODE    1 // Sigma passhtrough
    #define SIGMA_CFG         (p->format              << 0) |\
                              (PASSTROUGH_MODE        << 1) |\
                              ((p->rawBits-1)         << 4)

    #define GRGB_IMB_EN        0 //Gr/Gb imbalance enable
    #define BAD_PIXEL_FIX_EN   1 //Hot/Cold pixel suppression enable
    #define LUMA_HIST_EN       0 //Luma histogram enable
    #define GAIN_MODE          1 //Bayer 2x2 mode
    #define AF_STATS_EN        0 //AF stats
    #define RGB_HIST_EN        0 //RGB histogram enable
    #define SDC_EN             0 //Static pixel correction
    #define RAW_CFG           (p->format                <<  0) |\
                              (p->bayerPattern          <<  1) |\
                              (GRGB_IMB_EN              <<  3) |\
                              (BAD_PIXEL_FIX_EN         <<  4) |\
                              (LUMA_HIST_EN             <<  7) |\
                              ((p->rawBits - 1)         <<  8) |\
                              (GAIN_MODE                << 12) |\
                              (AF_STATS_EN              << 13) |\
                              (p->pRawCfg->grgbImbalThr << 16) |\
                              (RGB_HIST_EN              << 24) |\
                              (SDC_EN                   << 27)

    #define FP16_MODE          1 // FP16 mode
    #define CHANNEL_MODE       0 // Channel mode
    #define LUT_CFG           (FP16_MODE                   << 0) |\
                              (CHANNEL_MODE                << 1) |\
                              ((p->oPlanes[SIPP_LUT_ID]-1) << 12)

    // icIspConfig -> Opipe translation
    fetchIcIspMonoConfig(p, ic);

    // Configure SIPP filters
    OpipeDefaultCfg(p, SIPP_SHARPEN_ID);
    p->cfg[SIPP_SIGMA_ID] = SIGMA_CFG;
    p->cfg[SIPP_RAW_ID]   = RAW_CFG;
    //Opipe internally will force progPlanes = 1;
    p->iPlanes[SIPP_LUT_ID] = 1; //just 1 plane
    p->oPlanes[SIPP_LUT_ID] = 1; //just 1 plane
    p->cfg[SIPP_LUT_ID] = LUT_CFG;
}

static int32_t cbTrigerCapture(FrameT *frame, void *params, int (*callback)(int status), void *pluginObj)
{
    UNUSED(callback);
    PlgIspMonoLuma* obj = (PlgIspMonoLuma*)pluginObj;
    assert(frame);
    assert(params);
    if(PLG_ON == obj->crtStatus)
    {
        obj->plg.status = PLG_STATS_RUNNING;
        obj->ispCfg = (icIspConfig  *)params;
        //Get a frame from output mempool
        FrameT *oFrame = FrameMgrAcquireFrame(obj->outputPools);

        //Skip input frame if no more output buffer available
        if (NULL == oFrame) {
            if (obj->procesIspError) {
            obj->procesIspError(pluginObj, IC_SEVERITY_NORMAL, IC_ERROR_RT_OUT_BUFFERS_NOT_AVAILABLE, ((icIspConfig  *)frame->appSpecificData)->userData);
            } else {
                assert (0);
            }
            FrameMgrReleaseFrame(frame);
            obj->plg.status = PLG_STATS_IDLE;
            return -1;
        }

        //Else, we can process
        if (obj->procesStart) obj->procesStart((void*)obj, oFrame->seqNo, obj->ispCfg->userData);

        //From currently set "icIspConfig" to Opipe regs
        plgIspMonoLumaSetParams(obj);

        // Set image size
        obj->op.p.width = obj->frmSz.w;
        obj->op.p.height = obj->frmSz.h;

        //Frame pointers:
        obj->op.pIn->ddr.base    = (uint32_t)frame->fbPtr[0];
        obj->op.pOut->ddr.base  = (uint32_t)oFrame->fbPtr[0];

        //References needed in Opipe EOF handler to update plugin info
        obj->op.p.params[0] = (void*)obj;
        obj->op.p.params[1] = (void*)frame;
        obj->op.p.params[2] = (void*)oFrame;
        //The kick
        OpipeStart(&obj->op.p);
    }
    return 0;
}
//
static void cbNewInputFrame(FrameT *iFrame, void *pluginObject)
{
    PlgIspMonoLuma* obj = (PlgIspMonoLuma*)pluginObject;
    assert(iFrame);
    if(PLG_ON == obj->crtStatus)
    {
        obj->plg.status = PLG_STATS_RUNNING;

        if(iFrame->appSpecificData) {
            obj->ispCfg = (icIspConfig  *)iFrame->appSpecificData;
        }
        else {
            FrameMgrReleaseFrame(iFrame);
            obj->plg.status = PLG_STATS_IDLE;
            return;
        }

        //Else, can try capture (will succeed if oFrame can be allocated)
        cbTrigerCapture(iFrame, obj->ispCfg, NULL, pluginObject);
    }
}
//Opipe EOF callback: adjust associated Plugin Frame buffers
static void opipeIspEof(Opipe *p)
{
    PlgIspMonoLuma *obj    = (PlgIspMonoLuma *)p->params[0];
    FrameT     *iFrame = (FrameT     *)p->params[1];
    FrameT     *oFrame = (FrameT     *)p->params[2];

    assert(iFrame);
    assert(oFrame);

    //Set correct resolution to out frame (now that frame's produced out res is also known)
    switch(iFrame->type)
    {
    case FRAME_T_FORMAT_RAW_8:
        oFrame->stride[0] = iFrame->stride[0];
        oFrame->height[0] = iFrame->height[0];
        break;
    case FRAME_T_FORMAT_RAW_10:
    case FRAME_T_FORMAT_RAW_10_PACK:
        oFrame->stride[0] = ((iFrame->stride[0] * 4) / 5);
        oFrame->height[0] = iFrame->height[0];
        break;
    case FRAME_T_FORMAT_RAW_16:
        oFrame->stride[0] = (iFrame->stride[0] / 2);
        oFrame->height[0] = iFrame->height[0];
        break;
    default:
        oFrame->stride[0] = iFrame->stride[0];
        oFrame->height[0] = iFrame->height[0];
    }

    if (obj->procesEnd)   obj->procesEnd((void*)obj, oFrame->seqNo, obj->ispCfg->userData);

    //new frame produced, updating times informations
    FrameMgrAddTimeStampHist(oFrame, iFrame);
    FrameMgrAndAddTimeStamp (oFrame, TimeSyncMsgGetTimeUs());

    FrameMgrReleaseFrame(iFrame);
    FrameMgrProduceFrame(oFrame);
    obj->plg.status = PLG_STATS_IDLE;
}

//
static int32_t fini(void *pluginObject)
{
    PlgIspMonoLuma *object = (PlgIspMonoLuma*)pluginObject;
    object->crtStatus  = PLG_OFF;
    return 0;
}

//
static int32_t init(FramePool *outputPools, int nOutputPools, void *pluginObject)
{
    UNUSED(nOutputPools);
    PlgIspMonoLuma* obj = (PlgIspMonoLuma*)pluginObject;
    // this plugin produce just 1 output frame, so not take in consideration nOutputPools params,
    // as this have to be 1
    obj->outputPools = outputPools;
    return 0;
}

//Opipe Mono Luma Isp yuv420 output creation
static void CreateOpipe(PlgIspMonoLuma *o, uint32_t inFmt, uint32_t prevAble)
{
    UNUSED(prevAble);
    OpipeLumaMono *opL = &o->op;

    if(o->cSigma == NULL) o->cSigma   = ogcBuff[SIPP_MIPI_RX0_ID ];
    if(o->cRaw  == NULL)  o->cRaw     = ogcBuff[SIPP_RAW_ID      ];
    if(o->cUsm   == NULL) o->cUsm     = ogcBuff[SIPP_SHARPEN_ID  ];
    if(o->cLut   == NULL) o->cLut     = ogcBuff[SIPP_LUT_ID      ];

    assert(o->cSigma != NULL);
    assert(o->cRaw  != NULL);
    assert(o->cUsm   != NULL);
    assert(o->cLut   != NULL);

    //Must specify buffers first
    opL->cBufInSig .base = (uint32_t)o->cSigma;  opL->cBufInSig .h = I_CBUFF_H;
    opL->cBufOutRaw.base = (uint32_t)o->cRaw;    opL->cBufOutRaw.h = O_CBUFF_H;
    opL->cBufOutUsm.base = (uint32_t)o->cUsm;    opL->cBufOutUsm.h = O_CBUFF_H;
    opL->cBufOutLut.base = (uint32_t)o->cLut;    opL->cBufOutLut.h = O_CBUFF_H;

    OpipeCreateLumaMono(opL, inFmt);
}

//Opipe related: returns Opipe circular buffer requirements in bytes.
//WARNING: using SIPP_MIPI_RX0_ID for Sigma Input !
// wSig  : width for RAW filters (Sigma, Lsc, Raw, Dbyr input)
// wMain : width for filters below debayer (can be wSig/2 if preview is enabled)
// req   : required circular-output-buffer size in bytes
static void QueryCircBuffSizes(PlgIspBase *me, uint32_t wSig, uint32_t wMain, uint32_t wOut, uint32_t* req)
{
    UNUSED(me);
    //Enlarge line widths a bit to accommodate internal padding:
    wSig += 8; wMain += 8;

    //                         height    * pl * width * bpp
    req[SIPP_MIPI_RX0_ID ] =  I_CBUFF_H      * wSig     ; // bpp:N/A
    req[SIPP_RAW_ID]       =  O_CBUFF_H      * wMain * 2; // bpp:2(fp16)
    req[SIPP_SHARPEN_ID  ] =  O_CBUFF_H      * wMain * 2; // bpp:2(fp16)
    req[SIPP_LUT_ID      ] =  O_CBUFF_H      * wOut * 1;  // bpp:1(  u8)
}


/**************************************************************************************************
 ~~~ Exported Functions Implementation
 **************************************************************************************************/

//
void PlgIspMonoLumaConfig(void *plgObject, icSize frameSz, uint32_t inFmt, uint32_t prevAble)
{
    PlgIspMonoLuma *obj   = (PlgIspMonoLuma *)plgObject;
    obj->frmSz  = frameSz;
    //Must know resolution in order do create an Opipe object
    if (PLG_ISPMONOLUMA_CREATED == obj->status) {
        CreateOpipe(obj, inFmt, prevAble);
        obj->status = PLG_ISPMONOLUMA_INUSE;
    }
    else {
        if(PLG_ISPMONOLUMA_INUSE == obj->status) {
            //Assume that change resolution, so update it
            //Resolution updates (if needed)
            //OpipeSetRes(&obj->op.p, frameSz.w, frameSz.h);
        }
        else {
            // try to start a source but not create is first,
            // or a memory corruption overwrite plug-in structure
            assert(0);
        }
    }

    obj->crtStatus = PLG_ON;
    //In Opipe EOF callback, will update Ipipe Frame buffers status
    obj->op.p.cbEndOfFrame = opipeIspEof;
}


//
void PlgIspMonoLumaCreate(void *pluginObject) {
    memset(pluginObject, 0, sizeof(PlgIspMonoLuma));
    PlgIspMonoLuma *object          = (PlgIspMonoLuma*)pluginObject;
    // init hw things, or all the init side that not need params,
    object->plg.init            = init; //to associate output pool
    object->plg.fini            = fini; //to mark STOP
    object->plg.trigger         = cbTrigerCapture;
    object->cbList[0].callback  = cbNewInputFrame;
    object->cbList[0].pluginObj = pluginObject;
    object->plg.callbacks       = object->cbList;
    object->plg.status          = PLG_STATS_IDLE;
    object->crtStatus           = PLG_OFF;
    object->status              = PLG_ISPMONOLUMA_CREATED;

    object->base.fnQueryMemReq  = QueryCircBuffSizes;
}
