/**************************************************************************************************

 @File         : PlgBicubicApi.h
 @Author       : AG
 @Brief        : Contain Bicubic plug-in interface
 Date          : 03 - Dec - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : © Movidius Srl 2014, © Movidius Ltd 2015

 Description :

     Resources used:
         Leon RT.
         Bicubic filter

    Interrupt base.

 **************************************************************************************************/
#ifndef __PLG_BICUBIC_API__
#define __PLG_BICUBIC_API__

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include "PlgTypes.h"
#include "mv_types.h"
#include <bicubicWarpApi.h>     /// bicubic block API

/**************************************************************************************************
 ~~~  Basic typedefs
 **************************************************************************************************/

 // After a stop, at the new start not allocate again the buffers, just change the size
typedef enum {
    PLG_BICUBIC_NOTMADE = 0,
    PLG_BICUBIC_CREATED = 1,
    PLG_BICUBIC_INUSE   = 2
}PlgBicubicStatus;

typedef struct PlgBicubicStruct {
    PlgType plg;

    //Bicubic params
    icSize        frmSz; //input frame size

    //Specific component interface; called when Bicubic processing is done
    void    (*procesEnd  )   (void);

    //use in IRQ the frame processed by bicubic filter
    FrameT           *oFrame;
    FrameT           *iFrame;

    //Private members. All data structures have to be internal
    FramePool               *outputPools;
    volatile int32_t        crtStatus; // internal usage
    FrameProducedCB         cbList[1];
    PlgBicubicStatus        status; // used for avid double plug-in initialization start/stop.

} PlgBicubic;


/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
void PlgBicubicCreate(void *pluginObject);
void PlgBicubicConfig(void *pluginObject, icSize frameSz, float*  homographyValues);
#endif
