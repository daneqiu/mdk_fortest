PlgBicubic

Supported Platform
===================
Myriad2 Ma2150 - This component works on Myriad2 silicon V2

Overview
==========
This plugin uses the Bicubic SIPP filter. 
(Read Sipp documentation)

User configuration
=====================

Precalibration - calibration file is available containing rotation-translation-projection homographies for several sensor modules.
The homographies are computed from pairs of images taken "at infinity".

*** User must configure the sensor module used, by sending to the plugin a list of HOMOGRAPHY_SIZE elements (default 9):
static float* homography = &calibrationH[0]; // USER_CONFIG = {SENSOR_IMX208_200124, SENSOR_OV7251_21012,
							SENSOR_OV7251_210013, SENSOR_OV7251_210014, SENSOR_OV7251_210015};

Naming convention (SENSOR_SensorModuleType_SensorDaughterCardSerialNumber):
SENSOR_IMX208_200124 // IMX208 sensor module type with MV0200 daughter card SN 200124
SENSOR_OV7251_21012  // OV7251 sensor module type with MV0210 daughter card SN 21012
SENSOR_OV7251_210013 // OV7251 sensor module type with MV0210 daughter card SN 210013
SENSOR_OV7251_210014 // OV7251 sensor module type with MV0210 daughter card SN 210014
SENSOR_OV7251_210015 // OV7251 sensor module type with MV0210 daughter card SN 210015

If no homography values are transmitted to the plugin (a NULL pointer), the plugin will used the default values for IMX208 sensor:
 
const float calibrationH[HOMOGRAPHY_SIZE] ={ // SENSOR_IMX208_200315
                                0.9744101060179016, -0.01842923359075381, 36.02105011698299,
                                0.01303609916072842, 0.9824515102716611, -3.28977967967297,
                                -1.787124750237852e-005, -3.439739752742806e-006, 1};

When running the application it is possible to select:

PLG_BICUBIC_MAX_WIDTH - default is 640
PLG_BICUBIC_MAX_HEIGHT - default is 480

HOMOGRAPHY_SIZE = the desired homography matrix size   
by default it is 9
