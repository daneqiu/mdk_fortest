PlgStitch6cams

Supported Platform
===================
Myriad2 Ma2150 - This component works on Myriad2 silicon V2

Overview
==========
    This plug-in takes a maximum of 6 inputs and stitches them together. 
    It also provides streaming from one camera if requested.
     Resources used:
         Leon RT.
         CmxDma controled by LeonRt.

    Interrupt base.
 

Software description
=======================
                         
 
Hardware needed
==================

Build
==================

Setup
==================

Expected output
==================

User interaction
==================
