/**************************************************************************************************

 @File         : PlgStitch6camsApi.h
 @Author       : AG
 @Brief        : APIs for creating, configuring and use PlgStitch6cams plugin
 Date          : 22 - Jan - 2016
 E-mail        : xxx.xxx@movidius.com
 Copyright     : © Movidius Srl 2014, © Movidius Ltd 2015

 Description :

    Resources used:
         Leon RT.
         CmxDma controlled by LeonRt.

    Interrupt base.

 **************************************************************************************************/
#ifndef __PlgStitch6cams_API__
#define __PlgStitch6cams_API__

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include "PlgTypes.h"
#include "DrvCmxDma.h"
#include "stitch6camsCommon.h"

/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/

/**************************************************************************************************
 ~~~  Basic typedefs
 **************************************************************************************************/

typedef struct PlgStitch6camsStruct {
    PlgType plg;
    volatile int32_t        crtStatus; // internal usage
    FrameProducedCB         cbList[MAX_NR_OF_FRAME_STICTCHED];
    FramePool               *outputPools;
    FrameT                  *oFrame;
    dmaRequesterId          dmaId;
    //
    uint32_t                noInputs;   //number of active cameras
    uint32_t                camId;      //number of current camera
    uint32_t                noOfAvailableInputFrames;
    icSize                  size;
    FrameT                  *inputFrameList[MAX_NR_OF_FRAME_STICTCHED];
    FrameT                  *nextInputFrameList[MAX_NR_OF_FRAME_STICTCHED];
}PlgStitch6cams;

/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
void PlgStitch6camsCreate   (void *pluginObject);
void PlgStitch6camsSetParams(void *pluginObject, uint32_t noInputs, icSize frameSize);

#endif //__PlgStitch6cams_API__

