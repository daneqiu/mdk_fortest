/**************************************************************************************************

 @File         : stitch6camsAlgo.c
 @Author       : AL
 @Brief        : Create a 1080p frame by stitching 6 smaller input frames
 Date          : 22 - Jan - 2016
 E-mail        : xxx.xxx@movidius.com
 Copyright     : © Movidius Srl 2014, © Movidius Ltd 2015

 Description : An input frame list containing maximum 6 frames is copied in the right order
              to an output frame with a fixed resolution (1080p). Input frame resolution
              can vary, maximum width is 960.
              CmxDma is used to perform the copying. Each plane (luma and chroma) is copied independently.

    Resources used:
         Leon RT.
         CmxDma controlled by LeonRt.

    Interrupt base.

 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 ***************************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdint.h>
#include <DrvCmxDma.h>
#include "DrvSvu.h"
#include "PlgStitch6camsApi.h"
#include "VcsHooksApi.h"

/**************************************************************************************************
 ~~~  Specific #defines

/**************************************************************************************************
 ~~~  Imported Variable symbol from shave variables
 **************************************************************************************************/
static dmaTransactionList_t  task[2*MAX_NR_OF_FRAME_STICTCHED];
static uint32_t              nTasks, curTask;

/**************************************************************************************************
 ~~~ Exported Functions Implementation
 **************************************************************************************************/

//##########################################################
// 6 cam preview format  | 0 | 1 | 2 |
//                       -------------
//                       | 3 | 4 | 5 |
//##########################################################
static void dmaDone(dmaTransactionList* ListPtr, void* userContext)
{
    curTask++;
    if(curTask == nTasks)
      eofFrameHandler();
    else
      DrvCmxDmaStartTaskAsync((dmaTransactionList_t*)&task[curTask], dmaDone, NULL);
}

//##########################################################
void StitchStart(FrameT   *in[MAX_NR_OF_FRAME_STICTCHED],
                 FrameT   *out,
                 uint32_t  sel, void *pluginObject)
{
    PlgStitch6cams *plug = (PlgStitch6cams*)pluginObject;

    uint32_t c, cLo, cHi;
    uint32_t cur; //current camera slot in raster order
    uint32_t copyW;
    nTasks  = 0;
    curTask = 0;

   //Decide the range (all or just one)
    if(sel < MAX_NR_OF_FRAME_STICTCHED)
    {//individual cam
        cLo   = sel;
        cHi   = sel;
        copyW = out->stride[0];
    }else
    {//all cams
        cLo   = 0;
        cHi   = MAX_NR_OF_FRAME_STICTCHED-1;
        copyW = out->stride[0]/3;
    }

  //==============================
  //Limit number of stitches
  //  cLo = 2;
  //  cHi = 2; //copy just 1st cam
  //==============================
    
    for(cur=0, c=cLo; c<=cHi; c++)
    {
      if(NULL == in[c])
         continue;

      uint32_t iW = in[c]->stride[0];
      uint32_t iH = in[c]->height[0];
      uint32_t oW = out->stride[0];
      uint32_t oH = out->height[0];
      uint32_t vO = (oH/2 - iH)/2;
      uint32_t hO = (iW - copyW)/2;

      //no vertical offset if single cam !
      if(cLo == cHi)
      {
         vO = 0;
         hO = 0;
      }

    //figure out TLC position
      uint32_t x = cur % 3;
      uint32_t y = cur / 3;

    //DMA descriptors setup (NO LINKS BETWEEN THEM, we run them one at a time !!!)

    //Luma task
      DrvCmxDmaTransactionBriefInit(plug->dmaId, DMA_2D_TRANSACTION, &task[nTasks]);
      task[nTasks].src              = in[c]->fbPtr[0] + hO;
      task[nTasks].dst              =   out->fbPtr[0] + ((y*(oH/2)+vO)*out->stride[0] + x*copyW);
      task[nTasks].no_planes        = 1 - 1;    //1 plane
      task[nTasks].src_width        = copyW;  //line width
      task[nTasks].src_stride       = in[c]->stride[0];
      task[nTasks].dst_width        = copyW;
      task[nTasks].dst_stride       = out->stride[0];
      task[nTasks].length           = copyW*iH; //total transfer size
      task[nTasks].src_plane_stride = 0; //unused
      task[nTasks].dst_plane_stride = 0; //unused
      nTasks++;

    //Chroma task
      iW = iW>>1;  iH = iH>>1;
      oW = oW>>1;  oH = oH>>1;
      vO = vO>>1;  copyW = copyW>>1;
      DrvCmxDmaTransactionBriefInit(plug->dmaId, DMA_2D_TRANSACTION, &task[nTasks]);
      task[nTasks].src              = in[c]->fbPtr[1] + (hO>>1);
      task[nTasks].dst              =   out->fbPtr[1] + ((y*(oH/2)+vO)*(out->stride[0]>>1) + x*copyW);
      task[nTasks].no_planes        = 2 - 1;   //2 planes
      task[nTasks].src_width        = copyW; //line width
      task[nTasks].src_stride       = iW;
      task[nTasks].dst_width        = copyW;
      task[nTasks].dst_stride       = out->stride[0]>>1;
      task[nTasks].length           = copyW*iH; //total transfer size
      task[nTasks].src_plane_stride = iW*iH;
      task[nTasks].dst_plane_stride = oW*oH;
      nTasks++;

      copyW = copyW<<1; //must restore...

      cur++;
  }

  //Start first task
  DrvCmxDmaStartTaskAsync((dmaTransactionList_t*)&task[curTask], dmaDone, NULL);
}
