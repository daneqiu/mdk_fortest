/**************************************************************************************************

 @File         : PlgSadDm.c
 @Author       : MT
 @Brief        : Contain Demo SAD base dense depth map plugin interface
 Date          : 01 - March - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : © Movidius Srl 2014, © Movidius Ltd 2015

 Description :
    This plug-in calculate a dense depth map, base just on sum of absolute difference filter.
    Resources used:
         Leon RT.
         Shaves, configurable number of shaves used, dynamic code loaded.
         CmxDma controled by LeonRt.

    Interrupt base.

 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 ***************************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include <stdint.h>
#include "DrvCmxDma.h"
#include "TimeSyncMgr.h"
#include "FrameMgrApi.h"
#include "PlgSadDmApi.h"
#include "DrvSvu.h"
#include "VcsHooksApi.h"

/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/
enum {
    PLGDM_STATS_OFF = 0,
    PLGDM_STATS_ON  = 1
};
//#define PLGDM_DBG_PRINTF printf
#define PLGDM_DBG_PRINTF

//Internal debug code, disabled by default.
//#define PLGDMENABLE_DBG_LOCAL
#ifdef PLGDMENABLE_DBG_LOCAL
#define PLGDMDBG_MAX_MESSAGE 8096
volatile uint32_t PLGDMdbgMsgBuf[PLGDMDBG_MAX_MESSAGE];
volatile uint32_t PLGDMdbgMsgIdx = 0;
static inline void PLGDMdbgMAddMes(uint32_t mesage) {
    PLGDMdbgMsgBuf[PLGDMdbgMsgIdx] = mesage;
    PLGDMdbgMsgIdx++;
    assert(PLGDMdbgMsgIdx < PLGDMDBG_MAX_MESSAGE);
    //if(dbgMsgIdx == DBG_MAX_MESSAGE) {
    //    dbgMsgIdx = 0;
    //}
}
#else
#define PLGDMdbgMAddMes(X)
#endif


/**************************************************************************************************
 ~~~  Local variables
 **************************************************************************************************/
extern u32 plgShvImageAddr[];
extern u32 PlgDmShv_runTask;
extern u32 PlgDmShv_shvBufL;
extern u32 PlgDmShv_shvBufR;
extern u32 PlgDmShv_shvBufO;


/**************************************************************************************************
 ~~~ Local File function declarations
 **************************************************************************************************/
static int32_t init(FramePool *outputPools, int nOutputPools, void *pluginObject);
static int32_t  stop(void *pluginObject);
static void runDmaInTask(PlgDm *plug);
static void runDmaInOutTask(PlgDm *plug);
static void runDmaOutTask(PlgDm *plug);
static inline void eolCallback(PlgDm *plug);
static void dmaHndlIrq(dmaTransactionList* ListPtr, void* userContext);
static void shvIrqHandler(uint32_t source);
static inline void eofFrameHandler(PlgDm *plug);
static void runShvLines(PlgDm *plug);
static void runFrame(PlgDm *plug);
static void produceFrame(void *pluginObject);
static void producedFrameX(FrameT *frame, void *pluginObject, int inFrameId);
static void producedFrame0(FrameT *frame, void *pluginObject);
static void producedFrame1(FrameT *frame, void *pluginObject);



/**************************************************************************************************
 ~~~ Local Functions Implementation
 **************************************************************************************************/

//
static int32_t init(FramePool *outputPools, int nOutputPools, void *pluginObject) {
    PLGDMdbgMAddMes(0x15);
    PlgDm *plug = (PlgDm*)pluginObject;
    plug->outputPools = outputPools;
    plug->crtStatus      = PLGDM_STATS_ON;
    return 0;
}

//
static int32_t  stop(void *pluginObject) {
    PLGDMdbgMAddMes(0x14);
    PlgDm *plug = (PlgDm*)pluginObject;
    plug->crtStatus      = PLGDM_STATS_OFF;
    return 0;
}

// dma side
static void runDmaInTask(PlgDm *plug) {
    PLGDMdbgMAddMes(0x13);
    uint32_t x;
    PLGDM_DBG_PRINTF("runDmaInTask \n");
    for (x = plug->shvFirst; x <= plug->shvLast; x++) {
        plug->dmaRef[x*3+0] = DrvCmxDmaCreateTransactionFullOptions(
                plug->dmaId,
                &plug->dmaTask[x*3+0],
                plug->shaveTaskDesc[x].inL,
                plug->shaveTaskDesc[x].left[plug->dmaRunNr&1][0],
                plug->size.w*4,
                plug->size.w,
                plug->size.w,
                plug->size.w,
                PLGDM_CMX_LINE_STRIDE);
        plug->shaveTaskDesc[x].inL += plug->size.w*4;
        plug->dmaRef[x*3+1] = DrvCmxDmaCreateTransactionFullOptions(
                plug->dmaId,
                &plug->dmaTask[x*3+1],
                plug->shaveTaskDesc[x].inR,
                plug->shaveTaskDesc[x].right[plug->dmaRunNr&1][0],
                plug->size.w*4,
                plug->size.w,
                plug->size.w,
                plug->size.w,
                PLGDM_CMX_LINE_STRIDE);
        plug->shaveTaskDesc[x].inR += plug->size.w*4;
        DrvCmxDmaLinkTasks(plug->dmaRef[x*3+0], 1, plug->dmaRef[x*3+1]);
        if(x > plug->shvFirst) {
            DrvCmxDmaLinkTasks(plug->dmaRef[(x-1)*3+1], 1, plug->dmaRef[x*3+0]);
        }
    }
    //PLGDM_DBG_PRINTF("DrvCmxDmaStartTaskAsync 0x%x\n", plug->dmaRef[plug->shvFirst*3]);
    DrvCmxDmaStartTaskAsync(plug->dmaRef[plug->shvFirst*3], dmaHndlIrq, (void*)plug);
}

// dma side
static void runDmaInOutTask(PlgDm *plug) {
    PLGDMdbgMAddMes(0x12);
    uint32_t x;
    for (x = plug->shvFirst; x <= plug->shvLast; x++) {
        plug->dmaRef[x*3+0] = DrvCmxDmaCreateTransactionFullOptions(
                plug->dmaId,
                &plug->dmaTask[x*3+0],
                plug->shaveTaskDesc[x].inL,
                plug->shaveTaskDesc[x].left[plug->dmaRunNr&1][0],
                plug->size.w*4,
                plug->size.w,
                plug->size.w,
                plug->size.w,
                PLGDM_CMX_LINE_STRIDE);
        plug->shaveTaskDesc[x].inL += plug->size.w*4;
        plug->dmaRef[x*3+1] = DrvCmxDmaCreateTransactionFullOptions(
                plug->dmaId,
                &plug->dmaTask[x*3+1],
                plug->shaveTaskDesc[x].inR,
                plug->shaveTaskDesc[x].right[plug->dmaRunNr&1][0],
                plug->size.w*4,
                plug->size.w,
                plug->size.w,
                plug->size.w,
                PLGDM_CMX_LINE_STRIDE);
        plug->shaveTaskDesc[x].inR += plug->size.w*4;
        plug->dmaRef[x*3+2] = DrvCmxDmaCreateTransactionFullOptions(
                plug->dmaId,
                &plug->dmaTask[x*3+2],
                plug->shaveTaskDesc[x].out[plug->dmaRunNr&1],
                plug->shaveTaskDesc[x].dm,
                plug->size.w*4,
                plug->size.w,
                plug->size.w,
                0,
                plug->size.w);
        plug->shaveTaskDesc[x].dm+=plug->size.w*4;
        DrvCmxDmaLinkTasks(plug->dmaRef[x*3+0], 2, plug->dmaRef[x*3+1], plug->dmaRef[x*3+2]);
        if(x > plug->shvFirst) {
            DrvCmxDmaLinkTasks(plug->dmaRef[(x-1)*3+2], 1, plug->dmaRef[x*3+0]);
        }
    }
    DrvCmxDmaStartTaskAsync(plug->dmaRef[plug->shvFirst*3], dmaHndlIrq, (void*)plug);
}

// dma side
static void runDmaOutTask(PlgDm *plug) {
    PLGDMdbgMAddMes(0x11);
    uint32_t x;
    for (x = plug->shvFirst; x <= plug->shvLast; x++) {
        plug->dmaRef[x*3+2] = DrvCmxDmaCreateTransactionFullOptions(
                plug->dmaId,
                &plug->dmaTask[x*3+2],
                plug->shaveTaskDesc[x].out[plug->dmaRunNr&1],
                plug->shaveTaskDesc[x].dm,
                plug->size.w*4,
                plug->size.w,
                plug->size.w,
                0,
                plug->size.w);
        plug->shaveTaskDesc[x].dm+=plug->size.w*4;
        if(x > plug->shvFirst) {
            DrvCmxDmaLinkTasks(plug->dmaRef[(x-1)*3+2], 1, plug->dmaRef[x*3+2]);
        }
    }
    DrvCmxDmaStartTaskAsync(plug->dmaRef[plug->shvFirst*3+2], dmaHndlIrq, (void*)plug);
}

static inline void eolCallback(PlgDm *plug) {
    PLGDMdbgMAddMes(0x10);
    if((plug->shvRunMask == plug->shvTotal)&&(plug->dmaRunMask)) {
        runFrame(plug);
    }
}

static void dmaHndlIrq(dmaTransactionList* ListPtr, void* userContext) {
    PLGDMdbgMAddMes(0x09);
    PlgDm *plug = (PlgDm*)userContext;
    plug->dmaRunNr++;
    plug->dmaRunMask = 1;
    PLGDM_DBG_PRINTF("dmaHndlIrq \n");
    eolCallback(plug);
}

//
static void shvIrqHandler(uint32_t source) {
    PLGDMdbgMAddMes(0x08);

    PlgDm *plug = (PlgDm*)GET_REG_WORD_VAL(SVU_CTRL_ADDR[source - IRQ_SVE_0]+SLC_OFFSET_SVU+IRF_BASE+(18<<2));
    PLGDM_DBG_PRINTF("shvIrqHandler %x \n", plug);
    PLGDM_DBG_PRINTF("shvIrqHandlerSource %x \n", source - IRQ_SVE_0);
    plug->shvRunMask++;
    DrvIcbIrqClear(source);
    if(plug->shvRunMask == plug->shvTotal) {
        plug->shvRunNr++;
        eolCallback(plug);
    }

}

//
static inline void eofFrameHandler(PlgDm *plug) {
    PLGDMdbgMAddMes(0x07);
    FrameMgrReleaseFrame(plug->frameInProcessing[0]);
    FrameMgrReleaseFrame(plug->frameInProcessing[1]);
    plug->frameInProcessing[0] = NULL;
    plug->frameInProcessing[1] = NULL;
    FrameMgrProduceFrame(plug->oFrame);
    plug->frameInProcMask = 0;
    plug->plg.status = PLG_STATS_IDLE;
}

//
static void runShvLines(PlgDm *plug) {
    PLGDMdbgMAddMes(0x06);
    //printf("runTask: %x \n",(u32)&PlgDmShv_runTask);
    //PLGDMdbgMAddMes((u32)&PlgDmShv_runTask);
    //PLGDMdbgMAddMes(PlgDmShv_runTask);
    uint32_t shv;
    for (shv = plug->shvFirst; shv <= plug->shvLast; shv++) {
        // Set STOP bit in control register
        SET_REG_WORD(SVU_CTRL_ADDR[shv] + SLC_OFFSET_SVU + SVU_OCR, OCR_STOP_GO);
        swcSetAbsoluteDefaultStack(shv);
        //
        SET_REG_WORD(SVU_CTRL_ADDR[shv]+SLC_OFFSET_SVU+IRF_BASE+(18<<2), ((u32)plug));
        SET_REG_WORD(SVU_CTRL_ADDR[shv]+SLC_OFFSET_SVU+IRF_BASE+(17<<2), ((u32)plug->shaveTaskDesc[shv].out[plug->shvRunNr&1]));
        SET_REG_WORD(SVU_CTRL_ADDR[shv]+SLC_OFFSET_SVU+IRF_BASE+(16<<2), ((u32)plug->shaveTaskDesc[shv].left[plug->shvRunNr&1]));
        SET_REG_WORD(SVU_CTRL_ADDR[shv]+SLC_OFFSET_SVU+IRF_BASE+(15<<2), ((u32)plug->shaveTaskDesc[shv].right[plug->shvRunNr&1]));
        SET_REG_WORD(SVU_CTRL_ADDR[shv]+SLC_OFFSET_SVU+IRF_BASE+(14<<2), ((u32)plug->size.w));
        // Enable SWI interrupt
        SET_REG_WORD(SVU_CTRL_ADDR[shv] + SLC_OFFSET_SVU + SVU_IRR, 0xFF);
        SET_REG_WORD(SVU_CTRL_ADDR[shv] + SLC_OFFSET_SVU + SVU_ICR, 0x20);
        // set IP
        PLGDM_DBG_PRINTF("PlgDmShv_runTask : %x \n", &PlgDmShv_runTask);
        SET_REG_WORD(SVU_CTRL_ADDR[shv]+SLC_OFFSET_SVU+SVU_PTR, (u32)&PlgDmShv_runTask);
        //Start shave
        SET_REG_WORD(SVU_CTRL_ADDR[shv]+SLC_OFFSET_SVU+SVU_OCR, 0);
    }
}

//
static void runFrame(PlgDm *plug) {
    PLGDMdbgMAddMes(0x05);
    PLGDM_DBG_PRINTF("runFrame %d\n", plug->dmaRunNr);
    PLGDM_DBG_PRINTF("nrRunsPerFrame %d\n", plug->nrRunsPerFrame);
    if(0 == plug->dmaRunNr) {
        plug->dmaRunMask = 0;
        plug->shvRunMask = plug->shvTotal;
        runDmaInTask(plug);
        // shave is skiped
        return;
    }
    if(1 == plug->dmaRunNr) {
        plug->dmaRunMask = 0;
        plug->shvRunMask = 0;
        runDmaInTask(plug);
        runShvLines(plug);
        return;
    }
    if(plug->nrRunsPerFrame > plug->dmaRunNr) {
        plug->dmaRunMask = 0;
        plug->shvRunMask = 0;
        runDmaInOutTask(plug);
        //runDmaInTask(plug);
        runShvLines(plug);
        return;
    }
    if(plug->nrRunsPerFrame == plug->dmaRunNr) {
        plug->dmaRunMask = 0;
        plug->shvRunMask = 0;
        runDmaOutTask(plug);
        runShvLines(plug);
        return;
    }
    if((plug->nrRunsPerFrame+1) == plug->dmaRunNr) {
        plug->dmaRunMask = 0;
        plug->shvRunMask = plug->shvTotal;
        runDmaOutTask(plug);
        return;
    }
    if((plug->nrRunsPerFrame+2) == plug->dmaRunNr) {
        eofFrameHandler(plug);
        return;
    }
}

//
static void produceFrame(void *pluginObject) {
    PLGDMdbgMAddMes(0x04);
    PlgDm *plug = (PlgDm*)pluginObject;
    uint32_t shv;
    assert(PLG_STATS_IDLE == plug->plg.status);

    if(PLGDM_STATS_ON == plug->crtStatus) {
        plug->plg.status = PLG_STATS_RUNNING;

        FrameT *oFrame = FrameMgrAcquireFrame(plug->outputPools);
        // skip frame if no more output buffer available
        if (NULL == oFrame) { // release input frame and exit, frame will be dropped
            FrameMgrReleaseFrame(plug->frameInProcessing[0]);
            FrameMgrReleaseFrame(plug->frameInProcessing[1]);
            plug->plg.status = PLG_STATS_IDLE;
            plug->frameInProcMask = 0;
            return;
        }
        plug->oFrame = oFrame;

        uint8_t *inL = (uint8_t*)plug->frameInProcessing[0]->fbPtr[0];
        uint8_t *inR = (uint8_t*)plug->frameInProcessing[1]->fbPtr[0];
        uint8_t *out = (uint8_t*)plug->oFrame->fbPtr[0];

        for (shv = plug->shvFirst; shv <= plug->shvLast; shv++) {

            plug->shaveTaskDesc[shv].inL = inL; inL+=(plug->sliceH * plug->size.w);
            plug->shaveTaskDesc[shv].inR = inR; inR+=(plug->sliceH * plug->size.w);
            plug->shaveTaskDesc[shv].dm = out;  out+=(plug->sliceH * plug->size.w);

        }
        // run the shave code
        plug->shvRunMask = 0;
        plug->shvRunNr   = 0;
        plug->dmaRunNr   = 0;
        PLGDM_DBG_PRINTF("produceFrame \n");
        runFrame(plug);
    }
}

void PlgSadDmStopSrc(void *pluginObject) {
    PlgDm *plug = (PlgDm*)pluginObject;
    plug->frameInExpectation[0] = 0;
    plug->frameInExpectation[1] = 0;
    plug->runEnMask = 0;
}

//
static void producedFrameX(FrameT *frame, void *pluginObject, int inFrameId) {
    PlgDm *plug = (PlgDm*)pluginObject;
    PLGDMdbgMAddMes(0x03);
    if(frame) {
        if(plug->frameInExpectation[inFrameId]) {
            FrameMgrReleaseFrame(plug->frameInExpectation[inFrameId]);
            plug->frameInExpectation[inFrameId] = frame;
            plug->runEnMask = plug->runEnMask | (1 << inFrameId);
        }
        else {
            plug->frameInExpectation[inFrameId] = frame;
            plug->runEnMask = plug->runEnMask | (1 << inFrameId);
        }
        if(PLGDM_FRAME_MASK == plug->runEnMask) {
            if(plug->frameInProcMask) {
                // skip all pending frame
                FrameMgrReleaseFrame(plug->frameInExpectation[0]);
                FrameMgrReleaseFrame(plug->frameInExpectation[1]);
                plug->frameInExpectation[0] = 0;
                plug->frameInExpectation[1] = 0;
                plug->runEnMask = 0;
            }
            else {
                plug->frameInProcMask = PLGDM_FRAME_MASK;
                plug->frameInProcessing[0] = plug->frameInExpectation[0];
                plug->frameInProcessing[1] = plug->frameInExpectation[1];
                plug->frameInExpectation[0] = 0;
                plug->frameInExpectation[1] = 0;
                produceFrame(pluginObject);
                plug->runEnMask = 0;
            }
        }
    }
}

//
static void producedFrame0(FrameT *frame, void *pluginObject) {
    PLGDMdbgMAddMes(0x02);
    producedFrameX(frame, pluginObject, 0);
}

//
static void producedFrame1(FrameT *frame, void *pluginObject) {
    PLGDMdbgMAddMes(0x01);
    producedFrameX(frame, pluginObject, 1);
}

/**************************************************************************************************
 ~~~ Exported Functions Implementation
 **************************************************************************************************/
void PlgDmCreate(void *pluginObject) {
    PlgDm *plug = (PlgDm*)pluginObject;
    plug->plg.init = init;
    plug->plg.fini = stop;
    plug->cbList[0].callback   = producedFrame0;
    plug->cbList[0].pluginObj  = pluginObject;
    plug->cbList[1].callback   = producedFrame1;
    plug->cbList[1].pluginObj  = pluginObject;
    plug->plg.callbacks   = plug->cbList;
    plug->plg.status  = PLG_STATS_IDLE;
    plug->crtStatus = PLGDM_STATS_OFF;
    DrvCmxDmaInitDefault();
}

void PlgDmSetParams(void *pluginObject, icSize frameSz, uint32_t firstShave, uint32_t lastShave) {
    PlgDm *plug = (PlgDm*)pluginObject;
    uint32_t shv;
    uint32_t x;
    uint32_t y;
    plug->shvFirst = firstShave;
    plug->shvLast  = lastShave;
    plug->size.w   = frameSz.w;
    plug->size.h   = frameSz.h;
    plug->irqLevel = PLGDM_IRQ_LEVEL;
    plug->shvTotal = plug->shvLast - plug->shvFirst + 1;
    plug->sliceH   = (plug->size.h/plug->shvTotal)&(~0x03);
    plug->nrRunsPerFrame = plug->sliceH>>2;
    PLGDMdbgMAddMes(0x00);
    for (shv = plug->shvFirst; shv <= plug->shvLast; shv++) {
        swcResetShave(shv);
        swcSetAbsoluteDefaultStack(shv);
        swcSetShaveWindowsToDefault(shv);
        //printf("plgShvImageAddr : %x \n", plgShvImageAddr);
        PLGDM_DBG_PRINTF("shv : %x \n", shv);
        swcLoadshvdlib((u8*)plgShvImageAddr, shv);
        SET_REG_WORD(DCU_ICR(shv), ICR_SWI_ENABLE);
        // assert
        //assert(plgShvImageAddr == 0);
        DrvIcbSetupIrq(IRQ_SVE_0 + shv, plug->irqLevel, POS_EDGE_INT, shvIrqHandler);
        uint8_t *bufAdrL = (uint8_t*)swcSolveShaveRelAddrAHB((uint32_t)&PlgDmShv_shvBufL, shv);
        uint8_t *bufAdrR = (uint8_t*)swcSolveShaveRelAddrAHB((uint32_t)&PlgDmShv_shvBufR, shv);
        uint8_t *bufAdrO = (uint8_t*)swcSolveShaveRelAddrAHB((uint32_t)&PlgDmShv_shvBufO, shv);
        PLGDM_DBG_PRINTF("bufAdrL : %x \n", bufAdrL);
        PLGDM_DBG_PRINTF("bufAdrR : %x \n", bufAdrR);
        PLGDM_DBG_PRINTF("bufAdrO : %x \n", bufAdrO);
        for (y = 0; y < 2; y++) {
            for (x = 0; x < PROC_PATCH; x++) {
                plug->shaveTaskDesc[shv].left[y][x] = &bufAdrL[(y*PLGDM_PROC_PATCH+x)* PLGDM_CMX_LINE_STRIDE + PLGDM_MAX_PADSIZE];
                plug->shaveTaskDesc[shv].right[y][x] = &bufAdrR[(y*PLGDM_PROC_PATCH+x)* PLGDM_CMX_LINE_STRIDE + PLGDM_MAX_PADSIZE];
            }
            plug->shaveTaskDesc[shv].out[y] = &bufAdrO[y * PLGDM_CMX_LINE_STRIDE + PLGDM_MAX_PADSIZE];
        }
    }

    plug->dmaId  = DrvCmxDmaInitRequester(3);
}

