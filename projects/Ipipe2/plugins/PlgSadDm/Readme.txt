PlgSadDm

Supported Platform
===================
Myriad2 Ma2150 - This component works on Myriad2 silicon V2

Overview
==========
    This plug-in calculate a dense depth map, base just on sum of absolute difference filter.
     Resources used:
         Leon RT.
         Shaves, configurable number of shaves used, dynamic code loaded.
         CmxDma controled by LeonRt.

    Interrupt base.


Software description
=======================


Hardware needed
==================

Build
==================

Setup
==================

Expected output
==================

User interaction
==================
