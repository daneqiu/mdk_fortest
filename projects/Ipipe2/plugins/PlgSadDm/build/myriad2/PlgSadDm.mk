ifeq ($(LEON_RT_BUILD),yes)
	PLGDM_OBJECTS=RAWDATAOBJECTFILES
else
	PLGDM_OBJECTS=RAWDATAOBJECTFILES
endif

PLGDM_LEON_C_SOURCES += $(IPIPE_BASE)/plugins/PlgSadDm/leon/PlgSadDm.c

PlgDmShv = PlgDmShv
SHAVE_C_SOURCES_PlgDmShv = $(wildcard $(IPIPE_BASE)/plugins/PlgSadDm/shave/*.c)
SHAVE_ASM_SOURCES_PlgDmShv = $(wildcard $(IPIPE_BASE)/plugins/PlgSadDm/shave/*.asm)
SHAVE_GENASMS_PlgDmShv = $(patsubst %.c,$(DirAppObjBase)%.asmgen,$(SHAVE_C_SOURCES_PlgDmShv))
SHAVE_PlgDmShv_OBJS = $(patsubst %.asm,$(DirAppObjBase)%_shave.o,$(SHAVE_ASM_SOURCES_PlgDmShv)) \
                   $(patsubst $(DirAppObjBase)%.asmgen,$(DirAppObjBase)%_shave.o,$(SHAVE_GENASMS_PlgDmShv))
$(PLGDM_OBJECTS)  += $(PlgDmShv)_sym.o $(PlgDmShv)_bin.o     
PROJECTCLEAN += $(SHAVE_GENASMS_PlgDmShv) $(SHAVE_PlgDmShv_OBJS) $(PlgDmShv).mvlib        
PROJECTINTERM += $(SHAVE_GENASMS_PlgDmShv)    
PROJECTCLEAN += $(PlgDmShv).shvdlib $(PlgDmShv).map
PROJECTCLEAN += $(PlgDmShv).shvdcomplete $(PlgDmShv)_sym.o   
