/**************************************************************************************************

 @File         : interleaveShv.h
 @Author       : csoka
 @Brief        : interleave planar YUV422
 Copyright     : © Movidius Srl 2014, © Movidius Ltd 2015

 Description :

 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include <stdint.h>
#include "PlgInterleaveSharedDefines.h"

/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/


/**************************************************************************************************
 ~~~  Local variables
 **************************************************************************************************/
// not touched by LEON
uint8_t shvBufY[SHV_LINEBUFFS_IN_Y_SZ * SHV_NR_BUFFS] ALIGNED(32);
uint8_t shvBufU[SHV_LINEBUFFS_IN_UV_SZ * SHV_NR_BUFFS] ALIGNED(32);
uint8_t shvBufV[SHV_LINEBUFFS_IN_UV_SZ * SHV_NR_BUFFS] ALIGNED(32);
uint8_t shvBufO[SHV_LINEBUFFS_OUT_SZ * SHV_NR_BUFFS] ALIGNED(32);

/**************************************************************************************************
 ~~~ Function declarations
 **************************************************************************************************/
extern void yuv422Pto422Iconversion_asm(uint8_t *out, uint8_t *inY, uint8_t *inU, uint8_t *inV, uint32_t width);
void yuv422Pto422Iconversion(uint8_t *out, uint8_t *inY, uint8_t *inU, uint8_t *inV, uint32_t width);
/**************************************************************************************************
 ~~~ Exported Functions Implementation
 **************************************************************************************************/
void* runTask(void *plugCall, uint32_t buffid, uint32_t w)
{
    //yuv422Pto422Iconversion((uint8_t*)shvBufO, (uint8_t*)shvBufY, (uint8_t*)shvBufU, (uint8_t*)shvBufV,  ((w >> 1) * SHV_NR_OF_LINES_IN_BUF));
    yuv422Pto422Iconversion_asm(
            &shvBufO[SHV_LINEBUFFS_OUT_SZ   * buffid],
            &shvBufY[SHV_LINEBUFFS_IN_Y_SZ  * buffid],
            &shvBufU[SHV_LINEBUFFS_IN_UV_SZ * buffid],
            &shvBufV[SHV_LINEBUFFS_IN_UV_SZ * buffid],
            ((w >> 1) * SHV_NR_OF_LINES_IN_BUF));
    __asm("cmu.cpii i18, %0 \n bru.swih 0x1f \n nop 6 \n" : : "r"(plugCall) : "I18");
    return plugCall;
}


void yuv422Pto422Iconversion(uint8_t *out, uint8_t *inY, uint8_t *inU, uint8_t *inV, uint32_t width) {
    uint32_t i;
    uint32_t i_o   = 0;
    uint32_t i_y   = 0;
    uint32_t i_uv  = 0;
    for(i=0; i < width; i++)
    {
        out[i_o++] = inY[i_y++];
        out[i_o++] = inU[i_uv];
        out[i_o++] = inY[i_y++];
        out[i_o++] = inV[i_uv++];
    }
}
