/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved
///            For License Warranty see: common/license.txt
///
#include "mvtMulAB.h"


#ifndef _RVA_
#include "stdio.h"
#endif // _RVA_

//#define ADD_BIAS_AT_THE_END


extern "C"
{

/*
    *  C = A * B
    *
    *      A is m * k
    *      B is k * n
    *      C is m * n
    *
    *      Matrixes are row-major order
    *
    */
void mvtMulAB_hhhh_c(const half *A, const half *B, half *C, int m, int k, int n,
    int wA, int wB, int wC)
{
    int row_out;
    int col_out;
    int common_dim;
    half acc;

    for (row_out = 0; row_out < m; row_out++)
    {
        for (col_out = 0; col_out < n; col_out++)
        {
            acc = 0;
            for (common_dim = 0; common_dim < k; common_dim++)
            {
                acc += A[row_out * wA + common_dim] * B[common_dim * wB + col_out];
            }
            C[row_out * wC + col_out] = acc;
        }
    }
}



void mvtMulAB_hhhh_c_opt(const half *A, const half *B, half *C, int m, int k, int n,
    int wA, int wB, int wC)
{
    int row_out;
    int col_out;
    int common_dim;
    half8 a_half8;
    half8 b_half8;
    half8 *pv_C;
    half8 acc_half8;
    const half  *pA, *pB;

    for (row_out = 0; row_out < m; row_out++)
    {
        pv_C = reinterpret_cast<half8*>(C + row_out * wC);
        for (col_out = 0; col_out < n; col_out += 8)
        {
#ifdef __PC__
            acc_half8[0] = 0;
            acc_half8[1] = 0;
            acc_half8[2] = 0;
            acc_half8[3] = 0;
            acc_half8[4] = 0;
            acc_half8[5] = 0;
            acc_half8[6] = 0;
            acc_half8[7] = 0;
#else
            acc_half8 = half8(0);
#endif // __PC__

            pA = &A[row_out * wA];
            pB = &B[col_out];
            for (common_dim = 0; common_dim < k; common_dim++)
            {
                half a_tmp = *pA;
                pA++;
#ifdef __PC__
                half8 a_tmp_half8;
                a_tmp_half8[0] = a_tmp;
                a_tmp_half8[1] = a_tmp;
                a_tmp_half8[2] = a_tmp;
                a_tmp_half8[3] = a_tmp;
                a_tmp_half8[4] = a_tmp;
                a_tmp_half8[5] = a_tmp;
                a_tmp_half8[6] = a_tmp;
                a_tmp_half8[7] = a_tmp;
                a_half8 = a_tmp_half8;
#else
                a_half8 = half8(a_tmp);
#endif
                b_half8 = *reinterpret_cast<half8*>((u32)pB);
                pB += wB;
                acc_half8 = acc_half8 + a_half8 * b_half8;
            }

            *pv_C = acc_half8;
            pv_C++;
        }
    }
}


void mvtMulAB_hhhh_c_ref(const float *A, const float *B, float *C, int m, int k, int n,
    int wA, int wB, int wC)
{
    int row_out;
    int col_out;
    int common_dim;
    float acc;

    for (row_out = 0; row_out < m; row_out++)
    {
        for (col_out = 0; col_out < n; col_out++)
        {
            acc = 0;
            for (common_dim = 0; common_dim < k; common_dim++)
            {
                acc += A[row_out * wA + common_dim] * B[common_dim * wB + col_out];
            }
            C[row_out * wC + col_out] = acc;
        }
    }
}








///////////////////////////////////////////////////////////////////

/*
*  C = A * B + bias
*
*      A is m * k
*      B is k * n
*      C is m * n
*
*      Matrixes are row-major order
*
*/
void mvtMulABBias_hhhh_c(const half *A, const half *B, const half *bias, half *C, int m, int k, int n,
    int wA, int wB, int wBias, int wC)
{
    int row_out;
    int col_out;
    int common_dim;
    half acc;

    for (row_out = 0; row_out < m; row_out++)
    {
        for (col_out = 0; col_out < n; col_out++)
        {
#ifdef ADD_BIAS_AT_THE_END
            acc = 0;
#else
            acc = bias[row_out * wBias + col_out];
#endif
            for (common_dim = 0; common_dim < k; common_dim++)
            {
                acc += A[row_out * wA + common_dim] * B[common_dim * wB + col_out];
            }
#ifdef ADD_BIAS_AT_THE_END
            C[row_out * wC + col_out] = acc + bias[row_out * wC + col_out];
#else
            C[row_out * wC + col_out] = acc;
#endif
        }
    }
}





void mvtMulABBias_hhhh_c_opt(const half *A, const half *B, const half *bias, half *C, int m, int k, int n,
    int wA, int wB, int wBias, int wC)
{
    int row_out;
    int col_out;
    int common_dim;
    half8 a_half8;
    half8 b_half8;
    half8 *pv_C;
    const half8 *pv_bias;
    half8 acc_half8;
    const half  *pA, *pB;

    for (row_out = 0; row_out < m; row_out++)
    {
        pv_C = reinterpret_cast<half8*>(C + row_out * wC);
        pv_bias = reinterpret_cast<const half8*>(bias + row_out * wBias);
        for (col_out = 0; col_out < n; col_out += 8)
        {
#ifdef ADD_BIAS_AT_THE_END
#ifdef __PC__
            acc_half8[0] = 0;
            acc_half8[1] = 0;
            acc_half8[2] = 0;
            acc_half8[3] = 0;
            acc_half8[4] = 0;
            acc_half8[5] = 0;
            acc_half8[6] = 0;
            acc_half8[7] = 0;
#else
            acc_half8 = half8(0);
#endif // __PC__
#else
            acc_half8 = *pv_bias;
#endif

            pA = &A[row_out * wA];
            pB = &B[col_out];
            for (common_dim = 0; common_dim < k; common_dim++)
            {
                half a_tmp = *pA;
                pA++;
#ifdef __PC__
                half8 a_tmp_half8;
                a_tmp_half8[0] = a_tmp;
                a_tmp_half8[1] = a_tmp;
                a_tmp_half8[2] = a_tmp;
                a_tmp_half8[3] = a_tmp;
                a_tmp_half8[4] = a_tmp;
                a_tmp_half8[5] = a_tmp;
                a_tmp_half8[6] = a_tmp;
                a_tmp_half8[7] = a_tmp;
                a_half8 = a_tmp_half8;
#else
                a_half8 = half8(a_tmp);
#endif
                b_half8 = *reinterpret_cast<half8*>((u32)pB);
                pB += wB;
                acc_half8 = acc_half8 + a_half8 * b_half8;
            }

#ifdef ADD_BIAS_AT_THE_END
            *pv_C = *pv_bias + acc_half8;
#else
            *pv_C = acc_half8;
#endif
            pv_C++;
            pv_bias++;
        }
    }
}

void mvtMulABBias_hhhh_c_ref(const float *A, const float *B, const float *bias, float *C, int m, int k, int n,
    int wA, int wB, int wBias, int wC)
{
    int row_out;
    int col_out;
    int common_dim;
    float acc;

    for (row_out = 0; row_out < m; row_out++)
    {
        for (col_out = 0; col_out < n; col_out++)
        {
            acc = bias[row_out * wBias + col_out];
            for (common_dim = 0; common_dim < k; common_dim++)
            {
                acc += A[row_out * wA + common_dim] * B[common_dim * wB + col_out];
            }
            C[row_out * wC + col_out] = acc;
        }
    }
}


///////////////////////////////////////////////////////////////////

/*
*  C = CLAMP(A * B + bias, 0, X)
*
*      A is m * k
*      B is k * n
*      C is m * n
*
*      Matrixes are row-major order
*
*/
void mvtMulABBias_hhhh_ReluX_c(const half *A, const half *B, const half *bias, half *C, int m, int k, int n,
    int wA, int wB, int wBias, int wC, half X)
{
    int row_out;
    int col_out;
    int common_dim;
    half acc;

    for (row_out = 0; row_out < m; row_out++)
    {
        for (col_out = 0; col_out < n; col_out++)
        {
#ifdef ADD_BIAS_AT_THE_END
            acc = 0;
#else
            acc = bias[row_out * wBias + col_out];
#endif
            for (common_dim = 0; common_dim < k; common_dim++)
            {
                acc += A[row_out * wA + common_dim] * B[common_dim * wB + col_out];
            }
#ifdef ADD_BIAS_AT_THE_END
            C[row_out * wC + col_out] = acc + bias[row_out * wC + col_out];
#else
            C[row_out * wC + col_out] = acc;
#endif
            C[row_out * wC + col_out] = CLAMPAB(C[row_out * wC + col_out], (half)0.0, X);
        }
    }
}





void mvtMulABBias_hhhh_ReluX_c_opt(const half *A, const half *B, const half *bias, half *C, int m, int k, int n,
    int wA, int wB, int wBias, int wC, half X)
{
    int row_out;
    int col_out;
    int common_dim;
    half8 a_half8;
    half8 b_half8;
    half8 *pv_C;
    const half8 *pv_bias;
    half8 acc_half8;
    const half  *pA, *pB;
    half8 X_half8;
    half8 zero_half8;

#ifdef __PC__
    for (int i = 0; i < 8; i++)
    {
        X_half8[i] = X;
        zero_half8[i] = (half)0.0;
    }
#else
    X_half8 = half8(X);
    zero_half8 = half8((half)0.0);
#endif
    for (row_out = 0; row_out < m; row_out++)
    {
        pv_C = reinterpret_cast<half8*>(C + row_out * wC);
        pv_bias = reinterpret_cast<const half8*>(bias + row_out * wBias);
        for (col_out = 0; col_out < n; col_out += 8)
        {
#ifdef ADD_BIAS_AT_THE_END
#ifdef __PC__
            acc_half8[0] = 0;
            acc_half8[1] = 0;
            acc_half8[2] = 0;
            acc_half8[3] = 0;
            acc_half8[4] = 0;
            acc_half8[5] = 0;
            acc_half8[6] = 0;
            acc_half8[7] = 0;
#else
            acc_half8 = half8(0);
#endif // __PC__
#else
            acc_half8 = *pv_bias;
#endif

            pA = &A[row_out * wA];
            pB = &B[col_out];
            for (common_dim = 0; common_dim < k; common_dim++)
            {
                half a_tmp = *pA;
                pA++;
#ifdef __PC__
                half8 a_tmp_half8;
                a_tmp_half8[0] = a_tmp;
                a_tmp_half8[1] = a_tmp;
                a_tmp_half8[2] = a_tmp;
                a_tmp_half8[3] = a_tmp;
                a_tmp_half8[4] = a_tmp;
                a_tmp_half8[5] = a_tmp;
                a_tmp_half8[6] = a_tmp;
                a_tmp_half8[7] = a_tmp;
                a_half8 = a_tmp_half8;
#else
                a_half8 = half8(a_tmp);
#endif
                b_half8 = *reinterpret_cast<half8*>((u32)pB);
                pB += wB;
                acc_half8 = acc_half8 + a_half8 * b_half8;
            }

#ifdef ADD_BIAS_AT_THE_END
            *pv_C = *pv_bias + acc_half8;
#else
            *pv_C = acc_half8;
#endif
            CLAMPAB_HALF8(*pv_C, zero_half8, X_half8);
            pv_C++;
            pv_bias++;
        }
    }
}

void mvtMulABBias_ReluX_hhhh_c_ref(const float *A, const float *B, const float *bias, float *C, int m, int k, int n,
    int wA, int wB, int wBias, int wC, float X)
{
    int row_out;
    int col_out;
    int common_dim;
    float acc;

    for (row_out = 0; row_out < m; row_out++)
    {
        for (col_out = 0; col_out < n; col_out++)
        {
            acc = bias[row_out * wBias + col_out];
            for (common_dim = 0; common_dim < k; common_dim++)
            {
                acc += A[row_out * wA + common_dim] * B[common_dim * wB + col_out];
            }
            C[row_out * wC + col_out] = CLAMPAB(C[row_out * wC + col_out], (float)0.0, X);
        }
    }
}





















} // extern "C"
