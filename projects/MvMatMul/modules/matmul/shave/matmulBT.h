#ifndef _MATMULBT_H_
#define _MATMULBT_H_
#include "mv_types.h"

#ifdef __PC__
#include "half.h"
#include "VectorTypes.h"
#include "builtinFunctions.h"
#else
#include <moviVectorUtils.h>
#endif // __PC__

#include "matmul_common.h"

extern "C"
{
/*
 *  C += A * B'
 *
 *      A is m * k
 *      B is n * k
 *      C is m * n
 *
 *      Matrixes are row-major order
 *
 */
void matmulBT_hhhh_c(const half *A, const half *B, half *C, int m, int k, int n,
    int wA, int wB, int wC);
void matmulBT_hhhh_c_opt(const half *A, const half *B, half *C, int m, int k, int n,
    int wA, int wB, int wC);
void matmulBT_hhhh_asm(const half *A, const half *B, half *C, int m, int k, int n,
    int wA, int wB, int wC);
void matmulBT_hhhh_c_ref(const float *A, const float *B, float *C, int m, int k, int n,
    int wA, int wB, int wC);


/*
 *  C += CLAMPAB(A * B', 0, X)
 *
 *      A is m * k
 *      B is n * k
 *      C is m * n
 *
 *      Matrixes are row-major order
 *
 *
 */
void matmulBT_hhhh_ReluX_c_opt(const half *A, const half *B, half *C, int m, int k, int n,
    int wA, int wB, int wC, half X);
void matmulBT_hhhh_ReluX_asm(const half *A, const half *B, half *C, int m, int k, int n,
    int wA, int wB, int wC, half X);
void matmulBT_hhhh_ReluX_c_ref(const float *A, const float *B, float *C, int m, int k, int n,
    int wA, int wB, int wC, half X);

/*
 *  C += A * B'
 *
 *      A is m * k
 *      B is n * k
 *      C is m * n
 *
 *      Matrixes are row-major order
 *
 */
void matmulBT_xxii_c_ref(const uint8_t *A, const uint8_t *B, int32_t *C, int m, int k, int n,
    int wA, int wB, int wC);
void matmulBT_xxii_c_opt(const uint8_t *A, const uint8_t *B, int32_t *C, int m, int k, int n,
    int wA, int wB, int wC);
void matmulBT_xxii_asm(const uint8_t *A, const uint8_t *B, int32_t *C, int m, int k, int n,
    int wA, int wB, int wC);

/*
 *  C += CLAMPAB(A * B', 0, X)
 *
 *      A is m * k
 *      B is n * k
 *      C is m * n
 *
 *      Matrixes are row-major order
 *
 *
 */
void matmulBT_xxii_ReluX_c_ref(const uint8_t *A, const uint8_t *B, int32_t *C, int m, int k, int n,
    int wA, int wB, int wC, int32_t X);
void matmulBT_xxii_ReluX_c_opt(const uint8_t *A, const uint8_t *B, int32_t *C, int m, int k, int n,
    int wA, int wB, int wC, int32_t X);
void matmulBT_xxii_ReluX_asm(const uint8_t *A, const uint8_t *B, int32_t *C, int m, int k, int n,
    int wA, int wB, int wC, int32_t X);




} // extern "C"


#endif // _MATMULBT_H_
