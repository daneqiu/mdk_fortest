///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "swcCdma.h"
#include <swcWhoAmI.h>
#include "matmul_dma.h"
#include "dma_profile.h"
#include <mvTensorInternal.h>

namespace matmul
{

DmaChannel::DmaChannel(int priority, unsigned int dma_link_agent)
    : m_transfer_active(false)
    , m_transaction(-1)
    , m_reqId(0)
    , m_transaction_cnt_(0)
{
    reset();
    m_reqId = dmaInitRequesterWithAgent(priority, dma_link_agent);
}

DmaChannel::~DmaChannel()
{
}

template <typename T>
void DmaChannel::transfer_submat(Tensor& dst, Tensor& src, DmaTransferTypes tt)
{
    int rows = 0;
    src.dim0() <= dst.dim0() ? rows = src.dim0() : rows = dst.dim0();
    int cols = 0;
    src.dim1() <= dst.dim1() ? cols = src.dim1() : cols = dst.dim1();
    int line_sz = cols * sizeof(T);

    switch (tt)
    {
        case DMA_1D:
            create((unsigned char*) dst.buffer().data(), (unsigned char*) src.buffer().data(), rows * line_sz);
        break;
        case DMA_2D:
            create((unsigned char*) dst.buffer().data(), (unsigned char*) src.buffer().data(), rows * line_sz,
                    line_sz, line_sz, dst.st1(), src.st1());
        break;
        default:
            assert(0);
    }
}

void DmaChannel::create(unsigned char* dst, unsigned char* src, int bytes,
        int dst_line, int src_line, int dst_stride, int src_stride)
{
    if ((dst) && (src))
    {
        DMA_PROFILER_REC_TRANSACTION(src, src_line, src_stride, dst, dst_line, dst_stride, bytes);
        m_transfer_active = true;
        m_transaction++;
        if (src_line || dst_line || src_stride || dst_stride)
        {
            pTransactions[m_transaction] = dmaCreateTransactionExt(
                    DMA_2D_TRANSACTION, m_reqId, &lTransactions[m_transaction],
                    src, dst, bytes, src_line, dst_line, src_stride, dst_stride,
                    1);
        }
        else
        {
            pTransactions[m_transaction] = dmaCreateTransactionExt(
                    DMA_1D_TRANSACTION, m_reqId, &lTransactions[m_transaction],
                    src, dst, bytes, 1, 1, 1, 1, 1);
        }
    }
}

void DmaChannel::link()
{
    if (m_transaction > 0)
    {
        dmaLinkTasks(pTransactions[TRANSACTION_LIST_HEAD], m_transaction,
            pTransactions[1], pTransactions[2], pTransactions[3]);
    }
}

void DmaChannel::start()
{
    if (m_transfer_active)
    {
        m_transaction_cnt_++;
        dmaStartListTask(pTransactions[TRANSACTION_LIST_HEAD]);
    }
}

void DmaChannel::wait()
{
    if (pTransactions[TRANSACTION_LIST_HEAD])
    {
        if (!dmaIsTaskFinished(pTransactions[TRANSACTION_LIST_HEAD]))
        {
            DMA_PROFILER_REC_WAIT;
            dmaWaitTask(pTransactions[TRANSACTION_LIST_HEAD]);
        }
    }
    reset();
    DMA_PROFILER_REC_WAIT;
}

void DmaChannel::reset()
{
    for (int i = 0; i < MAX_TRANSACTIONS; i++)
    {
        pTransactions[i] = 0;
        lTransactions[i].linkAddress = NULL;
    }
    m_transfer_active = false;
    m_transaction = -1;
}

template void DmaChannel::transfer_submat<float>(Tensor& dst, Tensor& src, DmaChannel::DmaTransferTypes tt);
template void DmaChannel::transfer_submat<half>(Tensor& dst, Tensor& src, DmaChannel::DmaTransferTypes tt);

} /* namespace matmul */

