///
/// @file
/// @copyright All code copyright Movidius Ltd 2015, all rights reserved.
///            For License Warranty see: common/license.txt
///

#include <svuCommonShave.h>
#include "matmul_ipc.h"

namespace matmul
{

IpcAgent::IpcAgent(int id)
    : m_id(id)
    , n_slaves(0)
{
}

IpcAgent::~IpcAgent()
{
}

void IpcAgent::write_msg(int dst, int msg) const
{
    scFifoWrite(dst, msg);
}

int IpcAgent::read_msg() const
{
    return scFifoReadShave(m_id);
}

int IpcAgent::get_queue_fill_level() const
{
    return scFifoGetFillLevel(m_id);
}

void IpcAgent::wait_queue_fill_level(int level) const
{
    while (get_queue_fill_level() < level)
    {
        asm volatile("nop");
    }
}

void IpcAgent::empty_queue() const
{
    while(get_queue_fill_level())
    {
        read_msg();
    }
}

void IpcAgent::register_slave(int first_shave, int shaves)
{
    for (int idx = first_shave; idx < first_shave + shaves; idx++)
    {
        if (idx != m_id)
        {
            m_slaves[n_slaves] = idx;
            n_slaves++;
        }
    }
}

void IpcAgent::broadcast_msg(int msg) const
{
    for (int idx = 0; idx < n_slaves; idx++)
        write_msg(m_slaves[idx], msg);
}

} /* namespace matmul */

