#ifndef _MATMUL_COMMON_H_
#define _MATMUL_COMMON_H_
#include "mv_types.h"
#include "matmul_common.h"

#define CLAMPAB(x, A, B)    ((x)>(B)?(B):((x)<(A))?(A):(x))

#ifdef __PC__
#define CLAMPAB_HALF8(x, A, B)    \
       (x)[0] = CLAMPAB((x)[0], (A)[0], (B)[0]);  \
       (x)[1] = CLAMPAB((x)[1], (A)[1], (B)[1]);  \
       (x)[2] = CLAMPAB((x)[2], (A)[2], (B)[2]);  \
       (x)[3] = CLAMPAB((x)[3], (A)[3], (B)[3]);  \
       (x)[4] = CLAMPAB((x)[4], (A)[4], (B)[4]);  \
       (x)[5] = CLAMPAB((x)[5], (A)[5], (B)[5]);  \
       (x)[6] = CLAMPAB((x)[6], (A)[6], (B)[6]);  \
       (x)[7] = CLAMPAB((x)[7], (A)[7], (B)[7]);
#else
#define CLAMPAB_HALF8(x, A, B) x = __builtin_shave_cmu_clampab_f16_rrr_half8((x), (A), (B));
#endif

#endif // _MATMUL_COMMON_H_
