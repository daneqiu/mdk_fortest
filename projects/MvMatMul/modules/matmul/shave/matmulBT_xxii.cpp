/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved
///            For License Warranty see: common/license.txt
///
#include "matmul_kernel.h"
#include "matmulBT.h"
#include "math.h"


#ifndef _RVA_
#include "stdio.h"
#endif // _RVA_
//#define ADD_C_AT_THE_END

extern "C"
{

int8_t mult_u8f(const uint8_t a, const uint8_t b)
{
    u64 u_res = (u64)floor(((double)(a * b) / 255) + 0.5);
    int8_t mult = (int8_t)((uint32_t)u_res);
    return mult;
}


#ifdef __MOVICOMPILE__
//#define TEST_U8F
#endif

#ifdef TEST_U8F
#include "stdlib.h"

void TestMulU8F()
{
    uint8_t a, b;
    int num_errors = 0;

    printf("Testing U8F multiplication (C vs asm) ...\n");
    for (a=0; a<255; a++)
    {
        for (b=0; b<255; b++)
        {
            int32_t res_asm;
            int32_t res_c;
            asm("SAU.MUL.U8F %0, %1, %2 \n\t"
                "NOP 2\n\t"
                : "=r"(res_asm)
                : "r"(a), "r"(b)
                : );

#if 0
            res_c = ((uint32_t)a*b) >> 8;
#else
            u64 u_res = (u64)floor(((double)(a * b) / 255) + 0.5);
            res_c = (uint32_t)u_res;
#endif

            if (abs(res_c - res_asm) > 0)
            {
                printf("Error: a=0x%x, b=0x%x, res_c=0x%x, res_asm=0x%x, diff=%d \n", a, b, res_c, res_asm, abs(res_c-res_asm));
                num_errors++;
            }
        }
    }

#if 0
    a = -3;
    b = 6;
    printf("uint8: a=%x, b=%x, a+b=%x\n", a, b, a+b);
    printf("int8: a=%x, b=%x, a+b=%x\n", (int8_t)a, (int8_t)b, (int8_t)a+(int8_t)b);
#endif
    printf("Testing U8F multiplication (C vs asm) DONE, num_errors=%d\n", num_errors);
}
#endif


void matmulBT_xxii_c_opt(const uint8_t *A, const uint8_t *B, int32_t *C, int m, int k, int n,
    int wA, int wB, int wC)
{
    int row_out;
    int col_out;
    int common_dim;
    int32_t acc;

#ifdef TEST_U8F
    TestMulU8F();
#endif

    for (row_out = 0; row_out < m; row_out++)
    {
        for (col_out = 0; col_out < n; col_out++)
        {
#ifdef ADD_C_AT_THE_END
            acc = 0;
#else
            acc = C[row_out * wC + col_out];
#endif
            for (common_dim = 0; common_dim < k; common_dim++)
            {
                uint8_t a = A[row_out * wA + common_dim];
                uint8_t b = B[col_out * wB + common_dim];

                uint8_t mult = (uint8_t)mult_u8f(a, b);
                acc += mult;
            }
#ifdef ADD_C_AT_THE_END
            C[row_out * wC + col_out] += acc;
#else
            C[row_out * wC + col_out] = acc;
#endif
        }
    }
}

void matmulBT_xxii_ReluX_c_opt(const uint8_t *A, const uint8_t *B, int32_t *C, int m, int k, int n,
    int wA, int wB, int wC, int32_t X)
{
    int row_out;
    int col_out;
    int common_dim;
    int32_t acc;

#ifdef TEST_U8F
    TestMulU8F();
#endif

    for (row_out = 0; row_out < m; row_out++)
    {
        for (col_out = 0; col_out < n; col_out++)
        {
#ifdef ADD_C_AT_THE_END
            acc = 0;
#else
            acc = C[row_out * wC + col_out];
#endif
            for (common_dim = 0; common_dim < k; common_dim++)
            {
                uint8_t a = A[row_out * wA + common_dim];
                uint8_t b = B[col_out * wB + common_dim];

                uint8_t mult = (uint8_t)mult_u8f(a, b);
                acc += mult;
            }
#ifdef ADD_C_AT_THE_END
            C[row_out * wC + col_out] += acc;
#else
            C[row_out * wC + col_out] = CLAMPAB(acc, 0, X);
#endif
        }
    }
}


void matmulBT_xxii_c_ref(const uint8_t *A, const uint8_t *B, int32_t *C, int m, int k, int n,
    int wA, int wB, int wC)
{
    int row_out;
    int col_out;
    int common_dim;
    int32_t acc;

    for (row_out = 0; row_out < m; row_out++)
    {
        for (col_out = 0; col_out < n; col_out++)
        {
#ifdef ADD_C_AT_THE_END
            acc = 0;
#else
            acc = C[row_out * wC + col_out];
#endif
            for (common_dim = 0; common_dim < k; common_dim++)
            {
                uint8_t a = A[row_out * wA + common_dim];
                uint8_t b = B[col_out * wB + common_dim];

                uint8_t mult = (uint8_t)mult_u8f(a, b);
                acc += mult;
            }
#ifdef ADD_C_AT_THE_END
            C[row_out * wC + col_out] += acc;
#else
            C[row_out * wC + col_out] = acc;
#endif
        }
    }
}


void matmulBT_xxii_ReluX_c_ref(const uint8_t *A, const uint8_t *B, int32_t *C, int m, int k, int n,
                         int wA, int wB, int wC, int32_t X)
{
    int row_out;
    int col_out;
    int common_dim;
    int32_t acc;

    for (row_out = 0; row_out < m; row_out++)
    {
        for (col_out = 0; col_out < n; col_out++)
        {
#ifdef ADD_C_AT_THE_END
            acc = 0;
#else
            acc = C[row_out * wC + col_out];
#endif
            for (common_dim = 0; common_dim < k; common_dim++)
            {
                uint8_t a = A[row_out * wA + common_dim];
                uint8_t b = B[col_out * wB + common_dim];

                uint8_t mult = (uint8_t)mult_u8f(a, b);
                acc += mult;
            }
#ifdef ADD_C_AT_THE_END
            C[row_out * wC + col_out] += acc;
#else
            C[row_out * wC + col_out] = CLAMPAB(acc, 0, X);
#endif
        }
    }
}



} // extern "C"

