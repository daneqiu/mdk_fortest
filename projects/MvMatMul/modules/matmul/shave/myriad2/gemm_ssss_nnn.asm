///
/// @file
/// @copyright All code copyright Movidius Ltd 2015, all rights reserved.
///            For License Warranty see: common/license.txt
///

  .version 00.50.50

  .code .text
; C[m][n] += A[m][k] * B[k][n]
; void gemm_ssss_nnn( const float *A, const float *B, float *C,
;                  int n, int m, int p,
;                  int wA, int wB, int wC );
gemm_ssss_nnn:
  .set A i18
  .set B i17
  .set C i16
  .set n i15
  .set m i14
  .set p i13
  .set wA i12
  .set wB i11
  .set wC i10 ; actually, wC @ [sp+0x04] initially
  .set _a i9
  .set _b i8
  .set _c i7
  .set _b1 i6
  .set _p i5
  lsu1.ld.32   wC, i19 || iau.incs     i19, -24 || sau.or      i2,  n m
  lsu1.sti.64 i20, i19 || iau.shr.u32    n, n 2 || sau.or      i2, i2 p
  lsu1.sti.64 i22, i19 || iau.shr.u32    m, m 2 || sau.shl.x32 i2, i2 30
  lsu1.st.64  i24, i19 || iau.shr.u32    p, p 2
                          iau.incs m, -1        || cmu.cmz.i32   i2
  peu.oracc            || iau.cmpi n, 1
  peu.oracc            || iau.cmpi p, 1
  peu.pcc0i.or neq lt  || bru.jmp i30 ; if( CMU{((n|m|p)<<30)!=0}|| IAU{(n/4)-1<0||(m/4)<1||(p/4)<1}) return;
    lsu0.cp  _a, A     || iau.mul   n, n p
    lsu0.cp  _b, B     || lsu1.cp _b1, B
    lsu0.cp  _c, C     || lsu1.cp  _p, p
    lsu0.cp  i4, A
    nop 2
  iau.incs  i19, -16

  .set C0 v9
  .set C1 v10
  .set C2 v11
  .set B0 v12
  .set B1 v13
  .set B2 v14
  .set B3 v15
  .set tB0 v16 ; transposed B
  .set tB1 v17
  .set tB2 v18
  .set tB3 v19
  .set A0 v20
  .set A1 v21
  .set A2 v22
  .set a3 v23
  ; v0:v2   : temps for collecting C0/C1/C2 elements
  ; v3:v6   : temps for doing A*tB SIMD mul
  ; i0:i3   : temps for hsumming v2:v5
  ; i20:23  : A3 elements
  ; i24:i25 : general purpose temps
  .lalign
.Lmul_loop_outer:
  lsu0.ldxvi   A0, i4 wA || lsu1.ldil i24, .Lmul_loop_end
  lsu1.ldxvi   B0, _b wB || cmu.cpzv C0 2     ; clear C[0:2][]
  lsu0.ldxvi   A1, i4 wA || cmu.cpzt V_ACC0 3 ; clear VAU-ACC / C[3][]
  lsu1.ldxvi   B1, _b wB || iau.incs  _a, 16  ; _a+=4;
  lsu0.ldxvi   A2, i4 wA || iau.incs _b1, 16  ; _b1+=4
  lsu1.ldxvi   B2, _b wB
  lsu0.ldi.64 i20, i4    || cmu.cmz.i32 m
  lsu1.ldxvi   B3, _b wB
  lsu0.ld.64  i22, i4
  bru.bra .Lmul_loop_last || peu.pc1c eq
    iau.xor   i0, i0 i0   || lsu0.ldih i24, .Lmul_loop_end
    iau.xor   i1, i1 i1   || vau.xor    v6, v6 v6
    iau.xor   i2, i2 i2
    iau.xor   i3, i3 i3
    iau.mul  i25, wA 3
    cmu.tp4r tB0, B0

  .lalign
.Lmul_loop:
  vau.mul.f32   v3, A0 tB0 || bru.rpl     i24  m  || lsu0.cp     v2.0, i0  || lsu1.ldxvi   B0, _b wB
  vau.mul.f32   v4, A0 tB1                        || cmu.cpivr.x32 a3, i20 || iau.add      i4, _a i25
  vau.mul.f32   v5, A0 tB2 || sau.sumx.f32 i3, v6 || lsu0.cp     v2.1, i1  || lsu1.ldxvi   B1, _b wB
  vau.mul.f32   v6, A0 tB3 || sau.sumx.f32 i0, v3 || lsu0.cp     v2.2, i2
  vau.macp.f32      a3 B0  || sau.sumx.f32 i1, v4 || cmu.cpivr.x32 a3, i21 || lsu1.ldi.64 i20, i4
  vau.mul.f32   v3, A1 tB0 || sau.sumx.f32 i2, v5                          || lsu1.ldxvi   B2, _b wB
  vau.mul.f32   v4, A1 tB1 || sau.sumx.f32 i3, v6                          || iau.incs     _a, 16
  vau.macp.f32      a3 B1                         || cmu.cpivr.x32 a3, i22 || lsu1.ld.64  i22, i4
  vau.mul.f32   v5, A1 tB2 || sau.sumx.f32 i0, v3 || lsu0.cp     v2.3, i3  || lsu1.ldxvi   B3, _b wB
  vau.mul.f32   v6, A1 tB3 || sau.sumx.f32 i1, v4 || lsu0.cp     v0.0, i0  || iau.sub      i4, _a 16
  vau.macp.f32      a3 B2                         || lsu0.cp     v0.1, i1  || lsu1.ldxvi   A0, i4 wA
  vau.mul.f32   v3, A2 tB0 || sau.sumx.f32 i2, v5 || lsu0.cp     v0.2, i2
  .lalign
.Lmul_loop_end:
    vau.mul.f32 v4, A2 tB1 || sau.sumx.f32 i3, v6 || cmu.cpivr.x32 a3, i23
    vau.macp.f32    a3 B3                         || lsu0.cp     v0.3, i3  || lsu1.ldxvi   A1, i4 wA
    vau.mul.f32 v5, A2 tB2 || sau.sumx.f32 i0, v3 || lsu0.cp     v1.0, i0
    vau.mul.f32 v6, A2 tB3                        || lsu0.cp     v1.1, i1  || lsu1.ldxv    A2, i4
    vau.add.f32 C2, C2 v2  || sau.sumx.f32 i1, v4 || lsu0.cp     v1.2, i2
    vau.add.f32 C0, C0 v0  || sau.sumx.f32 i2, v5 || lsu0.cp     v1.3, i3
    vau.add.f32 C1, C1 v1                         || cmu.tp4r     tB0, B0

.Lmul_loop_last:
  ; last inner iteration
  vau.mul.f32    v3, A0 tB0 || sau.sumx.f32 i3, v6     || lsu0.cp     v2.0, i0
  vau.mul.f32    v4, A0 tB1                            || cmu.cpivr.x32 a3, i20 || iau.incs    _c, 16
  vau.mul.f32    v5, A0 tB2                            || lsu0.cp     v2.1, i1  || iau.sub     i4, _c 16
  vau.mul.f32    v6, A0 tB3 || sau.sumx.f32 i0, v3     || lsu0.cp     v2.2, i2
  vau.macp.f32       a3 B0  || sau.sumx.f32 i1, v4     || cmu.cpivr.x32 a3, i21
  vau.mul.f32    v3, A1 tB0 || sau.sumx.f32 i2, v5     || lsu0.cp     v2.3, i3
  vau.mul.f32    v4, A1 tB1 || sau.sumx.f32 i3, v6                              || lsu1.ldxvi  B0, i4 wC
  vau.macp.f32       a3 B1                             || cmu.cpivr.x32 a3, i22 || iau.incs    _p, -1
  vau.mul.f32    v5, A1 tB2 || sau.sumx.f32 i0, v3     || lsu0.cp     v0.0, i0
  vau.mul.f32    v6, A1 tB3 || sau.sumx.f32 i1, v4     || lsu0.cp     v0.1, i1
  vau.macp.f32       a3 B2                             || lsu0.cp     v0.2, i2  || lsu1.ldxvi  B1, i4 wC
  vau.mul.f32    v3, A2 tB0 || sau.sumx.f32 i2, v5     || lsu0.cp     v0.3, i3
  vau.mul.f32    v4, A2 tB1 || sau.sumx.f32 i3, v6     || cmu.cpivr.x32 a3, i23
  vau.macpw.f32  a3, a3 B3                             || lsu0.cp     v1.0, i0  || lsu1.ldxvi  B2, i4 wC
  vau.mul.f32    v5, A2 tB2 || sau.sumx.f32 i0, v3     || lsu0.cp     v1.1, i1
                                                          iau.shl    i24, wA 2  || lsu1.ldxv   B3, i4
  vau.mul.f32    v6, A2 tB3 || sau.sumx.f32 i1, v4     || lsu0.cp     v1.2, i2
  vau.add.f32    C2, C2 v2                             || lsu0.cp     v1.3, i3  || iau.shl    i25, wC 2
  vau.add.f32    A0, C0 v0  || sau.sumx.f32 i2, v5     || lsu0.cp       _a,  A
  vau.add.f32    A1, C1 v1  || sau.sumx.f32 i3, v6     || lsu0.cp       _b, _b1
                                                          lsu0.cp     v2.0, i0  || iau.sub     i4, _c 16
  vau.add.f32    B0, B0 A0                             || cmu.cmz.i32   _p
  vau.add.f32    B1, B1 A1                             || lsu0.cp     v2.1, i1
  vau.add.f32    B3, B3 a3                             || lsu0.cp     v2.2, i2
  lsu1.stxvi     B0, i4 wC                             || lsu0.cp     v2.3, i3
  vau.add.f32    A2, C2 v2  || peu.pccx.eq  16         || lsu0.cp      _b1, B
  lsu1.cp        _p, p      || peu.pc1c     eq         || lsu0.cp       _b, B
  lsu1.stxvi     B1, i4 wC  || iau.incs      n, -1
  vau.add.f32    B2, B2 A2  || peu.pcix.neq  0         || bru.bra .Lmul_loop_outer
    iau.add      _a, A i24  || peu.pc1c     eq
    iau.add      _c, C i25  || peu.pc1c     eq         || lsu0.cp        A, _a
    lsu1.stxvi   B2, i4 wC
    lsu0.cp       C, _c     || peu.pc1c     eq
    lsu1.stxv    B3, i4
    lsu0.cp      i4, _a
.Lmul_end:
  bru.jmp i30
    lsu1.ldi.64 i20, i19
    lsu1.ldi.64 i22, i19
    lsu1.ldi.64 i24, i19
    nop 3
.Lmul_done:
