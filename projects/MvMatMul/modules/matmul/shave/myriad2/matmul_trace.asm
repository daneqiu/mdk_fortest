///
/// @file
/// @copyright All code copyright Movidius Ltd 2015, all rights reserved.
///            For License Warranty see: common/license.txt
///

.version 00.65.6

.set SVU_DCR 0x1800		// Debug Control Register
.set SVU_PC0 0x1830		// Performance Counter 0
.set SVU_PCC0 0x1834		// Performance Counter 0 Control Register

.code .text.trace_cpu_cycles_asm
.lalign

    trace_cpu_cycles_asm:
		lsu0.lda.32 i2, SHAVE_LOCAL, SVU_DCR || lsu1.lda.32 i3, SHAVE_LOCAL, SVU_PCC0
		lsu0.ldil i1 0x0001 || lsu1.ldil i4 0x0004
		lsu0.ldil i0 0x0000 || lsu1.ldil i5 0x0005
		nop 4
		iau.and i2 i2 i1 || sau.and i3 i3 i4
		iau.or i6 i2 i3
		cmu.cmii.i8 i6 i5
		peu.pc1c neq || lsu0.sta.32 i1, SHAVE_LOCAL, SVU_DCR || lsu1.sta.32 i4, SHAVE_LOCAL, SVU_PCC0
		peu.pc1c neq || lsu0.sta.32 i0, SHAVE_LOCAL, SVU_PC0
		lsu0.lda.32 i18, SHAVE_LOCAL, SVU_PC0 || bru.jmp i30
		nop 6
