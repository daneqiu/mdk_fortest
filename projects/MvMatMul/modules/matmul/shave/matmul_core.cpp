///
/// @file
/// @copyright All code copyright Movidius Ltd 2015, all rights reserved.
///            For License Warranty see: common/license.txt
///

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <svuCommonShave.h>
#include <swcWhoAmI.h>
#include <swcCdma.h>
#include "matmul.h"
#include "matmul_graph.h"
#include "matmul_pool.h"
#include "matmul_core.h"
#include "matmul_kernel.h"
#include "matmul_dma.h"
#include "matmul_ipc.h"
#include "matmul_trace.h"
#include "matmul_util.h"

template <typename T>
void SHVMatGEMM_(matmul::MatMulSharePOD& mms)
{
    int first_shave = mms.first_shave;
    int total_shaves = mms.total_shaves;
    unsigned int dma_link_agent =  mms.dma_link_agent;
    unsigned int calls = mms.calls;
    unsigned int tensor_cnt = mms.tensor_cnt;
    unsigned int command_cnt = mms.command_cnt;
    unsigned int major_slices = mms.major_slices;
    unsigned int minor_slices = mms.minor_slices;

    matmul::DmaChannel dma(1, dma_link_agent);
    int shvNo = swcWhoAmI() - PROCESS_SHAVE0 - first_shave;
    int ipcId =  swcWhoAmI() - PROCESS_SHAVE0;
    matmul::IpcAgent ipc(ipcId);
    if (ipcId == first_shave) ipc.register_slave(first_shave, total_shaves);

    matmul::MatMulBuffer<T> b0, b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11;
    matmul::Tensor Addr(b0), Bddr(b1), Cddr(b2), Acmx(b3), Acmx_next(b4), Bcmx(b5),
        Bcmx_next(b6), Ccmx(b7), Ccmx_old(b8), Ccmx_next(b9), Cddr_old(b10), Cddr_next(b11);

    unsigned int* tensor_ids = (unsigned int*) mms.tensor_ids;
    matmul::TensorPOD* tensor_arr = (matmul::TensorPOD*) mms.tensor_arr;
    matmul::MatMulPool tensor_pool(tensor_ids, tensor_arr, tensor_cnt);

    unsigned int* command_ids = (unsigned int*) mms.command_ids;
    matmul::MatMulCmdPOD* command_arr = (matmul::MatMulCmdPOD*) mms.command_arr;
    matmul::MatMulPool command_pool(command_ids, command_arr, command_cnt * total_shaves);

    void* graph_arr = mms.graph_arr;
    matmul::MatMulGraph graph;
    graph.remap(graph_arr, major_slices, minor_slices, total_shaves);

    int A_last_loaded = -1;
    int B_last_loaded = -1;
    for (unsigned int call = 0; call < calls; call++)
    {
        matmul::GraphNode node;
        int offsetA = mms.offsetA[call];
        int offsetB = mms.offsetB[call];
        int offsetC = mms.offsetC[call];
        matmul::kernel_t kernel_type = mms.kernel_type[call];

        if (B_last_loaded >= 0)
            graph.Bddr_in_cmx[B_last_loaded] = 0;
        if (A_last_loaded >= 0)
            graph.Addr_in_cmx[A_last_loaded] = 0;

        tensor_pool.look_up_by_index<matmul::Tensor>(graph.Ccmx[shvNo], Ccmx);
        tensor_pool.look_up_by_index<matmul::Tensor>(graph.Ccmx_old[shvNo], Ccmx_old);
        tensor_pool.look_up_by_index<matmul::Tensor>(graph.Ccmx_next[shvNo], Ccmx_next);

        //load initial C values for first command
        matmul::MatMulCmd cmd_next;
        command_pool.look_up_by_index<matmul::MatMulCmd>(graph.command[shvNo], cmd_next);
        node = graph.Cddr[cmd_next.A_to_process * major_slices + cmd_next.B_to_process];
        tensor_pool.look_up_by_index<matmul::Tensor>(node.index_, Cddr_next);
        Cddr_next.set_data((void*) ((uint32_t) Cddr_next.buffer().data() + (int)(node.offset_ + offsetC)));
        if (kernel_type != matmul::GEMM_HHHH_NNN_NAC)
        {
            dma.transfer_submat<T>(Ccmx_next, Cddr_next, matmul::DmaChannel::DMA_2D);
        }
        if (cmd_next.A_to_load >= 0)
        {
            tensor_pool.look_up_by_index<matmul::Tensor>(graph.Acmx_next[shvNo], Acmx_next);
            node = graph.Addr[(int)cmd_next.A_to_load];
            tensor_pool.look_up_by_index<matmul::Tensor>(node.index_, Addr);
            Addr.set_data((void*) ((uint32_t) Addr.buffer().data() + (int)(node.offset_ + offsetA)));
            if ((mms.padK) || (Addr.dim1() * sizeof(T) != (unsigned) Addr.st1()))
                dma.transfer_submat<T>(Acmx_next, Addr, matmul::DmaChannel::DMA_2D);
            else
                dma.transfer_submat<T>(Acmx_next, Addr, matmul::DmaChannel::DMA_1D);
            A_last_loaded = cmd_next.A_to_load;
        }
        if (cmd_next.B_to_load >= 0)
        {
            tensor_pool.look_up_by_index<matmul::Tensor>(graph.Bcmx_next[shvNo], Bcmx_next);
            node = graph.Bddr[(int)cmd_next.B_to_load];
            tensor_pool.look_up_by_index<matmul::Tensor>(node.index_, Bddr);
            Bddr.set_data((void*) ((uint32_t) Bddr.buffer().data() + (int)(node.offset_ + offsetB)));
            dma.transfer_submat<T>(Bcmx_next, Bddr, matmul::DmaChannel::DMA_2D);
            B_last_loaded = cmd_next.B_to_load;
        }
        dma.link();
        dma.start();
        dma.wait();

        bool write_back_needed = false;
        for (unsigned int idxc = 0; idxc < command_cnt; idxc++)
        {
            matmul::MatMulCmd cmd;
            command_pool.look_up_by_index<matmul::MatMulCmd>(graph.command[idxc * total_shaves + shvNo], cmd);

            if ((cmd.A_to_process >= 0) && (cmd.B_to_process >= 0))
            {
                // Load B
                if (cmd.B_to_load >= 0)
                {
                    if (B_last_loaded >= 0)
                        graph.Bddr_in_cmx[B_last_loaded] = 0;

                    std::swap(graph.Bcmx[shvNo], graph.Bcmx_next[shvNo]);
                    graph.Bddr_in_cmx[(u8) cmd.B_to_load] = 1;
                    B_last_loaded = cmd.B_to_load;
                }

                // Load A
                if (cmd.A_to_load >= 0)
                {
                    if (A_last_loaded >= 0)
                        graph.Addr_in_cmx[A_last_loaded] = 0;

                    std::swap(graph.Acmx[shvNo], graph.Acmx_next[shvNo]);
                    graph.Addr_in_cmx[(u8) cmd.A_to_load] = 1;
                    A_last_loaded = cmd.A_to_load;
                }

                while (!graph.Addr_in_cmx[(u8) cmd.A_to_process])
                    asm volatile("nop");
                while (!graph.Bddr_in_cmx[(u8) cmd.B_to_process])
                    asm volatile("nop");

                tensor_pool.look_up_by_index<matmul::Tensor>(graph.Acmx[(int)cmd.A_loader], Acmx);
                tensor_pool.look_up_by_index<matmul::Tensor>(graph.Bcmx[(int)cmd.B_loader], Bcmx);

                matmul::swap_buffers(Ccmx, Ccmx_next);
                Cddr = Cddr_next;
                if (write_back_needed)
                {
                    dma.transfer_submat<T>(Cddr_old, Ccmx_old, matmul::DmaChannel::DMA_2D);
                    write_back_needed = false;
                }
                if (idxc < command_cnt - 1)
                {
                    //load initial C values and write back last C to DDR
                    command_pool.look_up_by_index<matmul::MatMulCmd>(graph.command[(idxc + 1) * total_shaves + shvNo], cmd_next);
                    node = graph.Cddr[cmd_next.A_to_process * major_slices + cmd_next.B_to_process];
                    tensor_pool.look_up_by_index<matmul::Tensor>(node.index_, Cddr_next);
                    Cddr_next.set_data((void*) ((uint32_t) Cddr_next.buffer().data() + (int)(node.offset_ + offsetC)));
                    if (kernel_type != matmul::GEMM_HHHH_NNN_NAC)
                    {
                        dma.transfer_submat<T>(Ccmx_next, Cddr_next, matmul::DmaChannel::DMA_2D);
                    }
                    if (cmd_next.A_to_load >= 0)
                    {
                        tensor_pool.look_up_by_index<matmul::Tensor>(graph.Acmx_next[shvNo], Acmx_next);
                        node = graph.Addr[(int)cmd_next.A_to_load];
                        tensor_pool.look_up_by_index<matmul::Tensor>(node.index_, Addr);
                        Addr.set_data((void*) ((uint32_t) Addr.buffer().data() + (int)(node.offset_ + offsetA)));
                        if ((mms.padK) || (Addr.dim1() * sizeof(T) != (unsigned) Addr.st1()))
                            dma.transfer_submat<T>(Acmx_next, Addr, matmul::DmaChannel::DMA_2D);
                        else
                            dma.transfer_submat<T>(Acmx_next, Addr, matmul::DmaChannel::DMA_1D);
                    }
                    if (cmd_next.B_to_load >= 0)
                    {
                        tensor_pool.look_up_by_index<matmul::Tensor>(graph.Bcmx_next[shvNo], Bcmx_next);
                        node = graph.Bddr[(int)cmd_next.B_to_load];
                        tensor_pool.look_up_by_index<matmul::Tensor>(node.index_, Bddr);
                        Bddr.set_data((void*) ((uint32_t) Bddr.buffer().data() + (int)(node.offset_ + offsetB)));
                        dma.transfer_submat<T>(Bcmx_next, Bddr, matmul::DmaChannel::DMA_2D);
                    }
                }
                dma.link();
                dma.start();

                if (Cddr.buffer().data())
                {
                    //Do Matrix Multiplication
                    matmul::kernel(Acmx.buffer().data(), Bcmx.buffer().data(), Ccmx.buffer().data(),
                        Acmx.dim0(), Bcmx.dim0(), Ccmx.dim1(),
                        Acmx.st1(), Bcmx.st1(), Ccmx.st1(),
                        kernel_type);

                    write_back_needed = true;
                    Cddr_old = Cddr;
                    matmul::swap_buffers(Ccmx_old, Ccmx);
                }
                dma.wait();
            } //if ((cmd.A_to_process >= 0) && (cmd.B_to_process >= 0))

            //Sync shaves
            if (((cmd_next.sync) && (idxc < command_cnt - 1)) || ((calls > 1) && (idxc == command_cnt - 1)))
            {
                if (ipcId == first_shave)
                {
                    ipc.wait_queue_fill_level(total_shaves - 1);
                    ipc.empty_queue();
                    ipc.broadcast_msg(matmul::IpcAgent::SLAVE_START);
                }
                else
                {
                    ipc.write_msg(first_shave, matmul::IpcAgent::SLAVE_DONE);
                    ipc.wait_queue_fill_level(1);
                    ipc.empty_queue();
                }
            }//if (total_shaves > 1)

        }//for (int idxc = 0; idxc < command_cnt; idxc++)

        if (write_back_needed)
        {
            //Write back last C chunck to DDR
            dma.transfer_submat<T>(Cddr_old, Ccmx_old, matmul::DmaChannel::DMA_2D);
            dma.start();
            dma.wait();
        }

    }//for (unsigned int call = 0; call < calls; call++)

    if (mms.trace)
    {
        matmul::MatMulTracePOD* tracer_stats = (matmul::MatMulTracePOD*) mms.tracer_stats;
        tracer_stats->dma_transactions[shvNo] = dma.transaction_cnt();
    }
}

void SHVMatGEMM(void* pmms, int type)
{
    matmul::MatMulSharePOD* mms = (matmul::MatMulSharePOD*) pmms;

    matmul::MatMulTracePOD* tracer_stats = (matmul::MatMulTracePOD*) mms->tracer_stats;
    if (mms->trace)
    {
        int shvNo = swcWhoAmI() - PROCESS_SHAVE0;
        tracer_stats->lsu_stalls[shvNo] = matmul::trace_lsu_stalls();
        tracer_stats->idc_stalls[shvNo] = matmul::trace_idc_stalls();
    }

    switch (type)
    {
        case matmul::MMT_SINGLE:
            SHVMatGEMM_<float>(*mms);
            break;
        case matmul::MMT_HALF:
            SHVMatGEMM_<half>(*mms);
            break;
        default:
            assert(0);
    }

    if (mms->trace)
    {
        int shvNo = swcWhoAmI() - PROCESS_SHAVE0;
        tracer_stats->lsu_stalls[shvNo] =  matmul::trace_lsu_stalls() - tracer_stats->lsu_stalls[shvNo];
        tracer_stats->idc_stalls[shvNo] =  matmul::trace_idc_stalls() - tracer_stats->idc_stalls[shvNo];
    }
    asm("NOP 13");
    SHAVE_HALT;
    exit(0);
}

template <typename T>
void SHVMatGEMV_(matmul::MatMulSharePOD& mms)
{
    //TODO: rewrite this code with new structures
    assert(0);
    
    // unused argument
    (void)mms;

}

void SHVMatGEMV(void* pmms, int type)
{
    matmul::MatMulSharePOD* mms = (matmul::MatMulSharePOD*) pmms;
    switch (type)
    {
        case matmul::MMT_SINGLE:
            SHVMatGEMV_<float>(*mms);
            break;
        case matmul::MMT_HALF:
            assert(0);
            break;
        default:
            assert(0);
    }

}

