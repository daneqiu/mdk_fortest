///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef _MATMUL_CMD_H_
#define _MATMUL_CMD_H_

#ifdef __cplusplus

namespace matmul
{

struct MatMulCmdPOD
{
    short A_to_process;
    char B_to_process;
    char A_loader;
    char B_loader;
    char B_to_load;
    short A_to_load;
    unsigned int id;
    short sync;
};

class MatMulCmd
{
    static unsigned int refid;

public:
    MatMulCmd(short r = -1, char c = -1, char la = -1, char lb = -1);
    MatMulCmd& operator= (const MatMulCmd& rhs);
    void serialize(void* ptr);
    void unserialize(void* ptr);
    unsigned int id() const { return id_; }
    unsigned int pod_size() const { return pod_size_; };
    unsigned int pod_aligment() const { return pod_aligment_; };

    short A_to_process;
    char B_to_process;
    char A_loader;
    char B_loader;
    char B_to_load;
    short A_to_load;
    unsigned int id_;
    char sync;
    unsigned int pod_size_;
    unsigned int pod_aligment_;
};

} /* namespace matmul */

#endif /* __cplusplus */

#endif // _MATMUL_CMD_H_

