///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef _MATMUL_GRAPH_H_
#define _MATMUL_GRAPH_H_

#include "matmul_alloc.h"

#ifdef __cplusplus

namespace matmul
{

class GraphNode
{
public:
    GraphNode();
    GraphNode(unsigned int idx, int off);
    GraphNode(const GraphNode& other);
    ~GraphNode();

    GraphNode& operator=(GraphNode tmp);

    unsigned int index_;
    int offset_;
};

class MatMulGraph
{
public:

    MatMulGraph();
    MatMulGraph(MatMulAllocator* a);
    MatMulGraph(int maj, int min, int shv, MatMulAllocator* a);
    ~MatMulGraph();
    void* array() const { return array_; }
    void remap(void* arr, int major, int minor, int shv);
    size_t graph_size() const { return graph_size_; }

    void reset();
    void reserve(unsigned major, unsigned minor, int cmd_cnt, unsigned shv);

    GraphNode* Addr;
    GraphNode* Bddr;
    GraphNode* Cddr;
    unsigned short* Acmx;
    unsigned short* Acmx_next;
    unsigned short* Bcmx;
    unsigned short* Bcmx_next;
    unsigned short* Ccmx;
    unsigned short* Ccmx_old;
    unsigned short* Ccmx_next;
    unsigned short* Addr_in_cmx;
    unsigned short* Bddr_in_cmx;
    unsigned short* command;

private:

    MatMulAllocator* alloc_;
    void* array_;
    unsigned int graph_size_;
};

} /* namespace matmul */

#endif /* __cplusplus */

#endif // _MATMUL_GRAPH_H_

