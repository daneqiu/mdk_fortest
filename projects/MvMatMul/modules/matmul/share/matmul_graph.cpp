///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#include "matmul.h"
#include "matmul_graph.h"

namespace matmul
{


GraphNode::GraphNode()
    : index_(0)
    , offset_(0)
{
}

GraphNode::GraphNode(unsigned int idx, int off)
    : index_(idx)
    , offset_(off)
{
}

GraphNode::GraphNode(const GraphNode& other)
{
    index_ = other.index_;
    offset_ = other.offset_;
}

GraphNode::~GraphNode()
{
}

GraphNode& GraphNode::operator=(GraphNode tmp)
{
    index_ = tmp.index_;
    offset_ = tmp.offset_;
    return *this;
}

MatMulGraph::MatMulGraph()
    : Addr(0)
    , Bddr(0)
    , Cddr(0)
    , Acmx(0)
    , Acmx_next(0)
    , Bcmx(0)
    , Bcmx_next(0)
    , Ccmx(0)
    , Ccmx_old(0)
    , Ccmx_next(0)
    , Addr_in_cmx(0)
    , Bddr_in_cmx(0)
    , command(0)
    , alloc_(0)
    , array_(0)
    , graph_size_(0)
{
}

MatMulGraph::MatMulGraph(MatMulAllocator* a)
    : Addr(0)
    , Bddr(0)
    , Cddr(0)
    , Acmx(0)
    , Acmx_next(0)
    , Bcmx(0)
    , Bcmx_next(0)
    , Ccmx(0)
    , Ccmx_old(0)
    , Ccmx_next(0)
    , Addr_in_cmx(0)
    , Bddr_in_cmx(0)
    , command(0)
    , alloc_(a)
    , array_(0)
    , graph_size_(0)
{
    array_ = alloc_->base();
}

MatMulGraph::~MatMulGraph()
{
}

void MatMulGraph::remap(void* arr, int major, int minor, int shv)
{
    array_ = arr;
    Addr = reinterpret_cast<GraphNode*>(array_);
    Bddr = Addr + minor;
    Cddr = Bddr + major;
    Acmx = (unsigned short*) (Cddr + minor * major);
    Acmx_next = Acmx + shv;
    Bcmx = Acmx_next + shv;
    Bcmx_next = Bcmx + shv;
    Ccmx = Bcmx_next + shv;
    Ccmx_old = Ccmx + shv;
    Ccmx_next = Ccmx_old + shv;
    Addr_in_cmx = Ccmx_next + shv;
    Bddr_in_cmx = Addr_in_cmx + minor;
    command = Bddr_in_cmx + major;
}

void MatMulGraph::reset()
{
    alloc_->reset();
    array_ = alloc_->base();
    Addr = 0;
    Bddr = 0;
    Cddr = 0;
    Acmx = 0;
    Acmx_next = 0;
    Bcmx = 0;
    Bcmx_next = 0;
    Ccmx = 0;
    Ccmx_old = 0;
    Ccmx_next = 0;
    Addr_in_cmx = 0;
    Bddr_in_cmx = 0;
    command = 0;
}

void MatMulGraph::reserve(unsigned major, unsigned minor, int cmd_cnt, unsigned shv)
{
    unsigned memory_used = alloc_->memory_used();
    Addr = reinterpret_cast<GraphNode*>(alloc_->allocate(minor * sizeof(GraphNode)));
    Bddr = reinterpret_cast<GraphNode*>(alloc_->allocate(major * sizeof(GraphNode)));
    Cddr = reinterpret_cast<GraphNode*>(alloc_->allocate(minor * major * sizeof(GraphNode)));
    Acmx = reinterpret_cast<unsigned short*>(alloc_->allocate(shv * sizeof(unsigned short)));
    Acmx_next = reinterpret_cast<unsigned short*>(alloc_->allocate(shv * sizeof(unsigned short)));
    Bcmx = reinterpret_cast<unsigned short*>(alloc_->allocate(shv * sizeof(unsigned short)));
    Bcmx_next = reinterpret_cast<unsigned short*>(alloc_->allocate(shv * sizeof(unsigned short)));
    Ccmx = reinterpret_cast<unsigned short*>(alloc_->allocate(shv * sizeof(unsigned short)));
    Ccmx_old = reinterpret_cast<unsigned short*>(alloc_->allocate(shv * sizeof(unsigned short)));
    Ccmx_next = reinterpret_cast<unsigned short*>(alloc_->allocate(shv * sizeof(unsigned short)));
    Addr_in_cmx = reinterpret_cast<unsigned short*>(alloc_->allocate(minor * sizeof(unsigned short)));
    Bddr_in_cmx = reinterpret_cast<unsigned short*>(alloc_->allocate(major * sizeof(unsigned short)));
    command = reinterpret_cast<unsigned short*>(alloc_->allocate(cmd_cnt * sizeof(unsigned short)));
    array_ = Addr;

    graph_size_ = alloc_->memory_used() - memory_used;
}

} /* namespace matmul */

