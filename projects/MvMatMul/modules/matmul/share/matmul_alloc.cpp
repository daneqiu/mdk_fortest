///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#include <cstdio>
#include <cstring>
#include "matmul_alloc.h"

namespace matmul
{

MatMulAllocator::MatMulAllocator(const char* msg)
{
    base_ = 0;
    base_size_ = 0;
    reset();
    strncpy(msg_, msg, MSG_LENGTH);
}

MatMulAllocator::MatMulAllocator(char* block, size_t size, const char* msg)
{
    base_ = block;
    base_size_ = size;
    reset();
    strncpy(msg_, msg, MSG_LENGTH);
}

MatMulAllocator::~MatMulAllocator()
{
}

void MatMulAllocator::reset()
{
    allocation_ptr_ = base_;
    memory_available_ = base_size_;
    memory_used_ = 0;
}

void MatMulAllocator::reset(char* block, size_t size)
{
    base_ = block;
    base_size_ = size;
    reset();
}

} /* namespace matmul */

