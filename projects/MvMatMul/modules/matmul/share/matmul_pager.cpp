///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#include <cassert>
#include <cstddef>
#include <cstring>
#include "matmul_pager.h"

namespace matmul
{

MatMulPager::MatMulPager(unsigned int pages, size_t page_sz, void* internal, void* external)
    : pages_(pages)
    , page_sz_(page_sz)
    , internal_(internal)
    , external_(external)
{
}

MatMulPager::~MatMulPager()
{
}

void MatMulPager::save_page(unsigned int idx)
{
    assert(idx < pages_);
    char* dst = reinterpret_cast<char*>(external_) + (page_sz_ * idx);
    memcpy(dst, internal_, page_sz_);
}

void MatMulPager::load_page(unsigned int idx)
{
    assert(idx < pages_);
    char* src = reinterpret_cast<char*>(external_) + (page_sz_ * idx);
    memcpy(internal_, src, page_sz_);
}

} /* namespace matmul */

