#ifndef _MATMUL_VECTOR_H_
#define _MATMUL_VECTOR_H_

#include <string>
#include <new>
#include "matmul_alloc.h"

#ifdef __cplusplus

namespace matmul
{

template <class T>
class Vector
{
    // needs some other constructors and a bunch of other methods!
public:
    typedef T* iterator;
    typedef T value_type;

    Vector(MatMulAllocator& alloc)
        : size_(0)
        , capacity_(0)
        , data_(0)
        , alloc_(alloc)
    {
    }

    ~Vector()
    {
        delete_range(data_, data_ + size_);
        alloc_.free(data_);
    }

    size_t size() { return size_; }

    size_t capacity() { return capacity_; }

    bool empty() { return !static_cast<bool>(size_); }

    T* data() { return data_; }

    void reserve (size_t n)
    {
        if (n > size_) reallocate(n);
    }

    void clear() { size_ = 0; }

    void push_back(const T& value)
    {
        if(size_ != capacity_)
        {
            new((void*)(data_ + size_)) T(value);
            ++size_;
            return;
        }
        size_t newCapacity = capacity_ ? capacity_ * 2 : 1;
        reallocate(newCapacity);
        new((void*)(data_ + size_)) T(value);
        ++size_;
    }

    T& operator[](size_t index)
    {
        return data_[index];
    }

    const T& operator[](size_t index) const
    {
        return data_[index];
    }

    T* begin() const
    {
        return data_;
    }

    T* end() const
    {
        return data_ + size_;
    }

    void resize(size_t newSize)
    {
        if(newSize <= size_)
        {
            delete_range(data_ + newSize, data_ + size_);
            size_ = newSize;
            return;
        }
        if(newSize <= capacity_)
        {
            construct_range(data_ + size_, data_ + newSize);
            size_ = newSize;
            return;
        }
        size_t newCapacity = newSize;
        if(newCapacity < size_ * 2)
        {
            newCapacity = size_ * 2;
        }
        reallocate(newCapacity);
        construct_range(data_ + size_, data_ + newSize);
        size_ = newSize;
    }

    void resize(size_t newSize, const T& val)
    {
        if(newSize <= size_)
        {
            delete_range(data_ + newSize, data_ + size_);
            size_ = newSize;
            return;
        }
        if(newSize <= capacity_)
        {
            construct_range(data_ + size_, data_ + newSize, val);
            size_ = newSize;
            return;
        }
        size_t newCapacity = newSize;
        if(newCapacity < size_ * 2)
        {
            newCapacity = size_ * 2;
        }
        reallocate(newCapacity);
        construct_range(data_ + size_, data_ + newSize, val);
        for(size_t i = size_; i < newSize; i++)
        {
            ::new((void*)(data_ + i)) T(val);
        }
        size_ = newSize;
    }

private:
    size_t size_;
    size_t capacity_;
    T* data_;
    MatMulAllocator& alloc_;

    T* allocate(size_t size)
    {
        return reinterpret_cast<T*>(alloc_.allocate(sizeof(T) * size, sizeof(T)));
    }

    void reallocate(size_t newCapacity)
    {
        T* newData = allocate(newCapacity);
        copy_range(data_, data_ + size_, newData);
        delete_range(data_, data_ + size_);
        alloc_.free(data_);
        data_ = newData;
        capacity_ = newCapacity;
    }

    static void copy_range(T* begin, T* end, T* dest)
    {
        while(begin != end)
        {
            new((void*)dest) T(*begin);
            ++begin;
            ++dest;
        }
    }

    static void delete_range(T* begin, T* end)
    {
        while(begin != end)
        {
            begin->~T();
            ++begin;
        }
    }

    static void construct_range(T* begin, T* end)
    {
        while(begin != end)
        {
            new((void*)begin) T();
            ++begin;
        }
    }

    static void construct_range(T* begin, T* end, const T& val)
    {
        while(begin != end)
        {
            new((void*)begin) T(val);
            ++begin;
        }
    }
};

} /* namespace matmul */

#endif /* __cplusplus */

#endif // _MATMUL_VECTOR_H_

