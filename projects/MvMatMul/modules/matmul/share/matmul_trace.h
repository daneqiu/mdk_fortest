///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef _MATMUL_TRACE_H_
#define _MATMUL_TRACE_H_

#include <cstdint>
#include "matmul_iface.h"

#ifdef __cplusplus

namespace matmul
{

uint32_t trace_cpu_cycles();
extern "C" uint32_t trace_cpu_cycles_asm();
uint32_t trace_lsu_stalls();
uint32_t trace_idc_stalls();

struct MatMulTracePOD
{
    uint32_t M;
    uint32_t K;
    uint32_t N;
    uint32_t S;
    uint32_t C;
    uint32_t S0;
    uint32_t major_slices;
    uint32_t minor_slices;
    uint32_t major_slice_len;
    uint32_t minor_slice_len;
    uint32_t A_slice_size;
    uint32_t B_slice_size;
    uint32_t C_slice_size;
    uint32_t lsu_stalls[matmul::SHAVES_TOUSE_MAX];
    uint32_t idc_stalls[matmul::SHAVES_TOUSE_MAX];
    uint64_t cycles;
    uint32_t dma_transactions[matmul::SHAVES_TOUSE_MAX];
};

class MatMulTrace
{
public:

    MatMulTracePOD& stats_;

    MatMulTrace(MatMulTracePOD& tpod)
        : stats_(tpod)
    {}

    ~MatMulTrace() {}

    void init();
    MatMulTracePOD& get_stats() const { return stats_; }
    uint64_t profile_start();
    uint64_t profile_stop();
    uint64_t stalls_start();
    uint64_t stalls_stop();

    void collect(void* mm);
    void log_short();
    void log_extended();
};

} /* namespace matmul */

#endif /* __cplusplus */

#endif // _MATMUL_TRACE_H_


