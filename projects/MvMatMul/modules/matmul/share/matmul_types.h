///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef _MATMUL_TYPES_H_
#define _MATMUL_TYPES_H_

#ifdef __cplusplus

namespace matmul
{

enum MatMulType
{
    MMT_INVALID = 0,
    MMT_HALF,
    MMT_SINGLE
};

template <typename T>
struct IsValidDataType;

template <typename T>
struct DataTypeToEnum
{
    static_assert(IsValidDataType<T>::value, "Data Type not supported");
};

#define MATCH_TYPE_AND_ENUM(TYPE, ENUM)                 \
template <>                                             \
struct DataTypeToEnum<TYPE>                             \
{                                                       \
    static MatMulType v() { return ENUM; }              \
    static const MatMulType value = ENUM;               \
};                                                      \
template <>                                             \
struct IsValidDataType<TYPE>                            \
{                                                       \
    static const bool value = true;                     \
}

MATCH_TYPE_AND_ENUM(half, MMT_HALF);
MATCH_TYPE_AND_ENUM(float, MMT_SINGLE);

#undef MATCH_TYPE_AND_ENUM

} /* namespace matmul */

#endif /* __cplusplus */

#endif // _MATMUL_TYPES_H_

