///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef _MATMUL_LOCK_H_
#define _MATMUL_LOCK_H_

#if defined(__RTEMS__)
    #include <pthread.h>
#endif

#ifdef __cplusplus

namespace matmul
{

//TODO: reimplement in C++11 using standard <mutex> module
#if defined(__RTEMS__)

struct Mutex
{
public:
    Mutex(pthread_mutex_t& mtx);

    void lock();
    void unlock();

private:
    pthread_mutex_t& mutex_;
};

#else

struct Mutex
{
    void lock() {};
    void unlock() {};
};

#endif //if defined(__RTEMS__)

class LockGuard
{
public:
    LockGuard(Mutex& mtx);
    ~LockGuard();
private:
    Mutex& mtx_;
};

} /* namespace matmul */

#endif /* __cplusplus */

#endif // _MATMUL_LOCK_H_


