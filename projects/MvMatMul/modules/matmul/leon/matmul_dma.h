///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef __MATMULDMA_HPP__
#define __MATMULDMA_HPP__

#if defined(__RTEMS__)
#include <OsDrvCmxDma.h>
#else
#include <DrvCmxDma.h>
#endif
#include "matmul.h"

#ifdef __cplusplus

namespace matmul
{

struct DmaChannel
{
    static const int MAX_INSTANCES = 4;

public:
    DmaChannel(int channel, unsigned int dma_link_agent = 0);
    virtual ~DmaChannel();

    void copy(unsigned char* dst, unsigned char* src, int bytes);
    void wait();

private:
    dmaRequesterId m_reqId;
    dmaTransactionList_t* tasks;
    dmaTransactionList_t* refs;
    static int instances_;
};

} /* namespace matmul */

#endif /* __cplusplus */

#endif //ifndef __MATMUL_HPP__

