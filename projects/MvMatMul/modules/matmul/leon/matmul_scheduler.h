///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef _MATMUL_SCHEDULER_H_
#define _MATMUL_SCHEDULER_H_

#include "matmul_pool.h"
#include "matmul_graph.h"

#ifdef __cplusplus

namespace matmul
{

struct SchParams
{
    int strideA;
    int strideB;
    int strideC;
    int major_slices;
    int minor_slices;
    int minor_slice_len;
    int major_slice_len;
    int M;
    int K;
    int N;
    int S;
    void* inA;
    void* inB;
    void* inoutC;
    int first_shave;
    int padM;
    int padK;
    int padN;
};

class MatMulScheduler
{
public:
    MatMulScheduler();
    virtual ~MatMulScheduler();

    void schedule(SchParams& p,
        MatMulPool* tensor_pool,
        MatMulPool* command_pool,
        MatMulGraph* graph);

    template <typename T>
    void schedule_inner();

    void generate_cmd_list();
    void set_cmd_list();
    void set_load_cmd(int shave);
    int inc_sat(int n, int s);
    void clean_last_command();

    half* inA;
    half* inB;
    half* inoutC;
    int M;
    int K;
    int N;
    int S;
    int padM;
    int padK;
    int padN;
    int strideA;
    int strideB;
    int strideC;
    int major_slices;
    int minor_slices;
    int minor_slice_len;
    int major_slice_len;
    int asini;
    int asend;
    int bsini;
    int bsend;
    int m_first_shave;
    int command_cnt;
    MatMulPool* tensor_pool_;
    MatMulPool* command_pool_;
    MatMulGraph* graph_;
    matmul::MatMulCmd command_last[matmul::SHAVES_TOUSE_MAX];
};

} /* namespace matmul */

#endif /* __cplusplus */

#endif // _MATMUL_SCHEDULER_H_

