///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#include "matmul_lock.h"

namespace matmul
{

#if defined(__RTEMS__)
Mutex::Mutex(pthread_mutex_t& mtx)
    : mutex_(mtx)
{
}

void Mutex::lock()
{
    pthread_mutex_lock(&mutex_);
}

void Mutex::unlock()
{
    pthread_mutex_unlock(&mutex_);
}

#endif //if defined(__RTEMS__)

LockGuard::LockGuard(Mutex& mtx)
    :mtx_(mtx)
{
    mtx_.lock();
}

LockGuard::~LockGuard()
{
    mtx_.unlock();
}

} /* namespace matmul */


