'''
    @file
    @copyright All code copyright Movidius Ltd 2012, all rights reserved.
               For License Warranty see: common/license.txt
'''

import os
import sys
import xmlrunner
import unittest
import yaml
import numpy as np
sys.path.append('../test')
import mvmatmul

cfg = {}


class TestMvMatMul(unittest.TestCase):
    test_base_dir = os.path.abspath(os.getcwd())
    test_out_dir = str()
    tester = mvmatmul.Mvmatmul('../myriad', 'output/mvMatMul.elf')
    stats_arr = [mvmatmul.Matmul_stats()] * 300

    @classmethod
    def setUpClass(cls):
        cls.test_out_dir = os.path.join(cls.test_base_dir, cfg['test_out_dir'])
        if not os.path.exists(cls.test_out_dir):
            os.makedirs(cls.test_out_dir)
        cls.tester.init()
        cls.tester.debugger.run_command('mdbg::breset')

    @classmethod
    def tearDownClass(cls):
        fname = cls.test_out_dir + '/cycles.txt'
        print('fname: ', fname)
        f = open(fname, 'w+')
        for stat in cls.stats_arr:
            load_eff = 100
            if stat.kernel_calls + stat.kernel_skips != 0:
                load_eff = 100 * stat.kernel_calls / (stat.kernel_calls + stat.kernel_skips)
            f.write('{} \t {} \t {:3.1f} \t {}\n'.format(stat.major_slices, stat.minor_slices, load_eff, stat.cycles))
        f.close()
        cls.tester.close()


def test_generator(M, K, N, S, id_=0, minf=0.0, maxf=1.0):
    def test(self):
        TestMvMatMul.tester.debugger.run_command('mdbg::breset')
        TestMvMatMul.tester.debugger.load_application(TestMvMatMul.tester.appname)
        TestMvMatMul.tester.set_float_range(minf, maxf)
        TestMvMatMul.tester.debugger.write_var('MATMUL_TRACE', np.uint64(0))
        TestMvMatMul.tester.debugger.write_var('MATMUL_CACHE', np.uint32(1))
        (Cpy, C, stats) = TestMvMatMul.tester.calculate(M, K, N, S, mvmatmul.KernelType.GEMM_HHHH_NNN, mvmatmul.MatMulType.MMT_HALF)

        atol = cfg['atol_h']
        if K >= 512:
            atol = cfg['atol_h'] * 2.5

        result = np.allclose(Cpy, C, atol=atol)
        if result:
            mvmatmul.log_stats(M, K, N, S, stats)
            TestMvMatMul.stats_arr[id_] = stats
        else:
            stats = mvmatmul.Matmul_stats()
            TestMvMatMul.stats_arr[id_] = stats
        self.assertTrue(result)
    return test


if __name__ == '__main__':

    # Load yaml configuration
    f = open('../unittest/config.yml')
    cfg = yaml.load(f)
    f.close()

    TestMvMatMul.test_out_dir = os.path.join(TestMvMatMul.test_base_dir, cfg['test_out_dir'])

    small_min = cfg['float_range']['small']['min']
    small_max = cfg['float_range']['small']['max']
    large_min = cfg['float_range']['large']['min']
    large_max = cfg['float_range']['large']['max']
    kernel_width = cfg['kernel_width']

    if cfg['run_test_googlenet']:
        sz = len(cfg['test_googlenet'])
        for idx in range(sz):
            M = cfg['test_googlenet'][idx]['M']
            K = cfg['test_googlenet'][idx]['K']
            N = cfg['test_googlenet'][idx]['N']
            S = cfg['test_googlenet'][idx]['S']
            id_ = cfg['test_googlenet'][idx]['id']

            if K % kernel_width:
                print('Dimension (K={}, id={}) must be multiple of {}'.format(M, K, N, id_, kernel_width))
                sys.exit()

            test_name = 'test_%s_%s_%s_%s_%s_small' % (id_, M, K, N, S)
            test = test_generator(M, K, N, S, id_)
            setattr(TestMvMatMul, test_name, test)

            if cfg['test_float_range']:
                test_name = 'test_%s_%s_%s_%s_%s_small' % (id_, M, K, N, S)
                test = test_generator(M, K, N, S, small_min, small_max)
                setattr(TestMvMatMul, test_name, test)

                test_name = 'test_%s_%s_%s_%s_%s_large' % (id_, M, K, N, S)
                test = test_generator(M, K, N, S, large_min, large_max)
                setattr(TestMvMatMul, test_name, test)

    if cfg['run_test_manual']:
        sz = len(cfg['test_manual'])
        for idx in range(sz):
            M = cfg['test_manual'][idx]['M']
            K = cfg['test_manual'][idx]['K']
            N = cfg['test_manual'][idx]['N']
            S = cfg['test_manual'][idx]['S']
            id_ = cfg['test_manual'][idx]['id']

            test_name = 'test_%s_%s_%s_%s_%s_small' % (id_, M, K, N, S)
            test = test_generator(M, K, N, S, id_)
            setattr(TestMvMatMul, test_name, test)

    if cfg['test_auto']:
        range_M = range(cfg['range_M']['min'], cfg['range_M']['max'], cfg['range_M']['step'])
        range_K = range(cfg['range_K']['min'], cfg['range_K']['max'], cfg['range_K']['step'])
        range_N = range(cfg['range_N']['min'], cfg['range_N']['max'], cfg['range_N']['step'])
        range_S = range(cfg['range_S']['min'], cfg['range_S']['max'], cfg['range_S']['step'])
        for M in range_M:
            for K in range_K:
                for N in range_N:
                    for S in range_S:
                        test_name = 'test_%s_%s_%s_%s' % (M, K, N, S)
                        test = test_generator(M, K, N, S)
                        setattr(TestMvMatMul, test_name, test)

    unittest.main(testRunner=xmlrunner.XMLTestRunner(output=TestMvMatMul.test_out_dir))

