'''
    @file
    @copyright All code copyright Movidius Ltd 2012, all rights reserved.
               For License Warranty see: common/license.txt
'''

import sys
import numpy as np
sys.path.append('../../shared/mvdbg')
import mvmatmul

# USAGE:
#   python3 test_single_call.py 32 8 32 1 4
#   python3 test_single_call.py 256 64 256 4 4

# K must be a multiple of 8
M = 784
K = 96
N = 128
S = 12
ATOL = 1.0
MATMUL_CALLS = 3
C_STRIDE = 512


def test_single_call():

    if int(C_STRIDE) % np.float16(0).itemsize:
        print('ERROR, stride must be multiple of', np.float16(0).itemsize)
        sys.exit()

    if int(C_STRIDE) / np.float16(0).itemsize < N:
        print('ERROR, stride must be greater or equal than N times the element size')
        sys.exit()

    print('Testing single call ---------------------------------------')

    def float_formatter(x): return "%.2f" % x
    np.set_printoptions(formatter={'float_kind': float_formatter}, linewidth=120)

    mvmm = mvmatmul.Mvmatmul('../myriad', 'output/mvMatMul.elf')
    mvmm.init()
    mvmm.debugger.write_var('MATMUL_TRACE', np.uint64(1))
    mvmm.debugger.write_var('MATMUL_CACHE', np.uint32(1))

    C_STRIDE_ELEMS = int(C_STRIDE/2 - N)
    stridedN = N + C_STRIDE_ELEMS
    newM = M * MATMUL_CALLS
    newK = K * MATMUL_CALLS

    A = np.random.uniform(mvmm.minf, mvmm.maxf, (newM, K)).astype(dtype=np.float16)
    B = np.random.uniform(mvmm.minf, mvmm.maxf, (newK, N)).astype(dtype=np.float16)
    C0 = np.zeros((1, stridedN)).astype(dtype=np.float16)
    auxC0 = np.random.uniform(mvmm.minf, mvmm.maxf, (N)).astype(dtype=np.float16)
    C0[:, 0:N] = auxC0
    C = np.tile(C0, (M, 1))
    Cnumpy = np.zeros((M, stridedN)).astype(dtype=np.float16)

    for i in range(MATMUL_CALLS):
        offsetM = i * M
        offsetK = i * K
        newA = A[offsetM:offsetM + M, :]
        newB = B[offsetK:offsetK + K, :]
        Cnumpy += np.dot(newA, newB)

    # Set leon inputs
    mvmm.A = A
    mvmm.B = B
    mvmm.C0 = C0
    mvmm.C = C

    mvmm.debugger.write_var('C_STRIDE', np.uint32(C_STRIDE))
    mvmm.debugger.write_var('MATMUL_CALLS', np.uint32(MATMUL_CALLS))
    mvmm.debugger.write_var('S', np.uint32(S))
    mvmm.debugger.write_var('M', np.uint32(M))
    mvmm.debugger.write_var('K', np.uint32(K))
    mvmm.debugger.write_var('N', np.uint32(N))
    mvmm.debugger.write_var('KernelType', np.uint32(mvmatmul.KernelType.GEMM_HHHH_NNN))
    mvmm.debugger.write_var('MatrixType', np.uint32(mvmatmul.MatMulType.MMT_HALF))
    mvmm.debugger.write_array('A', mvmm.A)
    mvmm.debugger.write_array('B', mvmm.B)
    mvmm.debugger.write_array('C', mvmm.C)

    # Run myriad and get output
    mvmm.debugger.run_command('mdbg::runw')
    mvmm.debugger.run_command('mdbg::uart flush')

    # Get output
    cycles = np.uint64(0)
    cycles = mvmm.debugger.read_var('MATMUL_CYCLES', cycles)
    Cmyriad = np.zeros((M, stridedN), dtype=np.float16)
    Cmyriad = mvmm.debugger.read_array('C', Cmyriad)

    # Save arrays
    np.save('Cnumpy.npy', Cnumpy)
    np.save('Cmyriad.npy', Cmyriad)
    np.save('A.npy', mvmm.A)
    np.save('B.npy', mvmm.B)
    np.save('C0.npy', mvmm.C0)

    mvmm.close()

    # Compare results numpy vs myriad C kernel
    print('numpy OUT[', M, 'x', N, '] = \n', Cnumpy)
    print('myriad ASM kernel OUT[', M, 'x', N, '] = \n', Cmyriad)

    # Compare results numpy vs myriad ASM kernel
    comp = np.allclose(Cnumpy, Cmyriad, atol=ATOL)
    if comp:
        print('Numpy result IS same as ASM kernel')
    else:
        print('Numpy result IS NOT same as ASM kernel')

    print('  MAX diff = ', np.amax(np.fabs(Cnumpy - Cmyriad)))
    print('  OUT of BOUNDS = ', (np.count_nonzero((Cnumpy - Cmyriad) > ATOL)))

    stats = mvmatmul.Matmul_stats()
    stats.cycles = cycles
    mvmatmul.log_stats(M, K, N, S, stats)

    print('Single call test done -------------------------------------')


if __name__ == "__main__":

    if len(sys.argv) == 7:
        M = int(sys.argv[1])
        K = int(sys.argv[2])
        N = int(sys.argv[3])
        S = int(sys.argv[4])
        MATMUL_CALLS = int(sys.argv[5])
        C_STRIDE = int(sys.argv[6])

    test_single_call()

