'''
    @file
    @copyright All code copyright Movidius Ltd 2012, all rights reserved.
               For License Warranty see: common/license.txt
'''

import sys
import numpy as np
sys.path.append('../../shared/mvdbg')
import mvmatmul


# K must be a multiple of 8
M = 784
K = 192
N = 64
S = 1
ATOL = 1.0
C_PADDING = 8


def test_strides():

    print('Testing strides ---------------------------------------')

    def float_formatter(x): return "%.2f" % x
    np.set_printoptions(formatter={'float_kind': float_formatter}, linewidth=120)

    mvmm = mvmatmul.Mvmatmul('../myriad', 'output/mvMatMul.elf')
    mvmm.init()

    A = np.random.uniform(mvmm.minf, mvmm.maxf, (M, K)).astype(dtype=np.float16)
    B = np.random.uniform(mvmm.minf, mvmm.maxf, (K, N)).astype(dtype=np.float16)
    C0 = np.random.uniform(mvmm.minf, mvmm.maxf, (N)).astype(dtype=np.float16)
    C = np.tile(C0, (M, 1))
    Cnumpy = np.dot(A, B) + C

    M_padded = M + C_PADDING * 2
    N_padded = N + C_PADDING * 2
    C_PADDED = np.zeros((M_padded, N_padded)).astype(dtype=np.float16)
    C_PADDED[C_PADDING: M + C_PADDING, C_PADDING: N + C_PADDING] = C

    # Set leon inputs
    mvmm.A = A
    mvmm.B = B
    mvmm.C0 = C0
    mvmm.C = C_PADDED

    C_STRIDE = N_padded * 2
    mvmm.debugger.write_var('C_STRIDE', np.uint32(C_STRIDE))
    mvmm.debugger.write_var('C_PADDING', np.uint32(C_PADDING))
    mvmm.debugger.write_var('MATMUL_CALLS', np.uint32(1))
    mvmm.debugger.write_var('MATMUL_TRACE', np.uint32(1))
    mvmm.debugger.write_var('MATMUL_CACHE', np.uint32(1))

    mvmm.debugger.write_var('S', np.uint32(S))
    mvmm.debugger.write_var('M', np.uint32(M))
    mvmm.debugger.write_var('K', np.uint32(K))
    mvmm.debugger.write_var('N', np.uint32(N))
    mvmm.debugger.write_var('KernelType', np.uint32(mvmatmul.KernelType.GEMM_HHHH_NNN))
    mvmm.debugger.write_var('MatrixType', np.uint32(mvmatmul.MatMulType.MMT_HALF))
    mvmm.debugger.write_array('A', mvmm.A)
    mvmm.debugger.write_array('B', mvmm.B)
    mvmm.debugger.write_array('C', mvmm.C)

    # Run myriad and get output
    mvmm.debugger.run_command('mdbg::runw')
    mvmm.debugger.run_command('mdbg::uart flush')

    # Get output
    cycles = np.uint64(0)
    cycles = mvmm.debugger.read_var('MATMUL_CYCLES', cycles)
    Caux = np.zeros((M_padded, N_padded), dtype=np.float16)
    Caux = mvmm.debugger.read_array('C', Caux)
    Cmyriad = Caux[C_PADDING: M + C_PADDING, C_PADDING: N + C_PADDING]

    print(Caux)

    # Save arrays
    np.save('Cnumpy.npy', Cnumpy)
    np.save('Cmyriad.npy', Cmyriad)
    np.save('A.npy', mvmm.A)
    np.save('B.npy', mvmm.B)
    np.save('C0.npy', mvmm.C0)

    mvmm.close()

    # Compare results numpy vs myriad C kernel
    print('numpy OUT[', M, 'x', N, '] = \n', Cnumpy)
    print('myriad ASM kernel OUT[', M, 'x', N, '] = \n', Cmyriad)

    # Compare results numpy vs myriad ASM kernel
    comp = np.allclose(Cnumpy, Cmyriad, atol=ATOL)
    if comp:
        print('Numpy result IS same as ASM kernel')
    else:
        print('Numpy result IS NOT same as ASM kernel')

    print('  MAX diff = ', np.amax(np.fabs(Cnumpy - Cmyriad)))
    print('  OUT of BOUNDS = ', (np.count_nonzero((Cnumpy - Cmyriad) > ATOL)))

    stats = mvmatmul.Matmul_stats()
    stats.cycles = cycles
    mvmatmul.log_stats(M, K, N, S, stats)

    print('Strides test done -------------------------------------')


if __name__ == "__main__":

    if len(sys.argv) == 6:
        M = int(sys.argv[1])
        K = int(sys.argv[2])
        N = int(sys.argv[3])
        S = int(sys.argv[4])
        C_PADDING = int(sys.argv[5])

    test_strides()

