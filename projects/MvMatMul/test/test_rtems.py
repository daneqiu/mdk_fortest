'''
    @file
    @copyright All code copyright Movidius Ltd 2012, all rights reserved.
               For License Warranty see: common/license.txt

    NOTE: build with "make RTOS=1 -j"

'''
import sys
import numpy as np
import mvmatmul


# USAGE:
#   python3 test_rtems.py

M = 256                     # Rows of matrix A
K = 256                     # Cols of matrix A, Rows of matrix B
N = 256                     # Cols of matrix B
S = 2                       # Number of shaves
THREADS = 2                 # Threads
RUNS = 10                   # Runs per thread
ATOL = 1.0                  # Tolerance acceptable to compare myriad <-> numpy


def test_rtems():

    print('Testing multithreading ---------------------------------------')

    def float_formatter(x): return "%.2f" % x
    np.set_printoptions(formatter={'float_kind': float_formatter}, linewidth=120)

    mvmm = mvmatmul.Mvmatmul('../myriad', 'output/mvMatMul.elf')
    mvmm.init()

    mvmm.A = np.empty(shape=[M * THREADS, K]).astype(dtype=np.float16)
    mvmm.B = np.empty(shape=[K * THREADS, N]).astype(dtype=np.float16)
    Cnumpy = np.empty(shape=[M * THREADS, N]).astype(dtype=np.float16)

    for idx in range(THREADS):
        A = np.random.uniform(mvmm.minf, mvmm.maxf, (M, K)).astype(dtype=np.float16)
        B = np.random.uniform(mvmm.minf, mvmm.maxf, (K, N)).astype(dtype=np.float16)
        C = np.dot(A, B)
        mvmm.A[M * idx: M * (idx + 1), :] = A
        mvmm.B[K * idx: K * (idx + 1), :] = B
        Cnumpy[M * idx: M * (idx + 1), :] = np.dot(A, B)

    mvmm.debugger.write_var('S', np.uint32(S))
    mvmm.debugger.write_var('M', np.uint32(M))
    mvmm.debugger.write_var('K', np.uint32(K))
    mvmm.debugger.write_var('N', np.uint32(N))
    mvmm.debugger.write_var('KernelType', np.uint32(mvmatmul.KernelType.GEMM_HHHH_NNN))
    mvmm.debugger.write_var('MatrixType', np.uint32(mvmatmul.MatMulType.MMT_HALF))
    mvmm.debugger.write_var('THREADS', np.uint32(THREADS))
    mvmm.debugger.write_var('RUNS', np.uint32(RUNS))
    mvmm.debugger.write_array('A', mvmm.A)
    mvmm.debugger.write_array('B', mvmm.B)

    # Run myriad and get output
    mvmm.debugger.run_command('mdbg::runw')
    mvmm.debugger.run_command('mdbg::uart flush')

    # Get output
    Cmyriad = np.zeros(shape=[M * THREADS, N]).astype(dtype=np.float16)
    Cmyriad = mvmm.debugger.read_array('C', Cmyriad)

    mvmm.close()

    # Compare results numpy vs myriad C kernel
    print('numpy OUT[', M * THREADS, 'x', N, '] = \n', Cnumpy)
    print('myriad ASM kernel OUT[', M * THREADS, 'x', N, '] = \n', Cmyriad)

    # Compare results numpy vs myriad ASM kernel
    comp = np.allclose(Cnumpy, Cmyriad, atol=ATOL)
    if comp:
        print('Numpy result IS same as ASM kernel')
    else:
        print('Numpy result IS NOT same as ASM kernel')

    print('  MAX diff = ', np.amax(np.fabs(Cnumpy - Cmyriad)))
    print('  OUT of BOUNDS = ', (np.count_nonzero((Cnumpy - Cmyriad) > ATOL)))

    print('Multi threading test done -------------------------------------')


if __name__ == "__main__":

    test_rtems()

