'''
    @file
    @copyright All code copyright Movidius Ltd 2012, all rights reserved.
               For License Warranty see: common/license.txt
'''

import sys
import numpy as np
sys.path.append('../../shared/mvdbg')
import mvmatmul


# K must be a multiple of 8
M = 784
K = 16
N = 32
S = 1
S0 = 0
ATOL = 1.0
TESTS = [[1, 3],
         [2, 3],
         [4, 3],
         [8, 3],
         [1, 7],
         [2, 7],
         [4, 7]]


def test_multiple_calls(mvmm, S, S0):

    print('Testing shave order ---------------------------------------')

    def float_formatter(x): return "%.2f" % x
    np.set_printoptions(formatter={'float_kind': float_formatter}, linewidth=120)

    mvmm.debugger.run_command('mdbg::breset')
    mvmm.debugger.load_application(mvmm.appname)

    A = np.random.uniform(mvmm.minf, mvmm.maxf, (M, K)).astype(dtype=np.float16)
    B = np.random.uniform(mvmm.minf, mvmm.maxf, (K, N)).astype(dtype=np.float16)
    C0 = np.random.uniform(mvmm.minf, mvmm.maxf, (N)).astype(dtype=np.float16)
    C = np.tile(C0, (M, 1))

    Cnumpy = np.dot(A, B) + C

    # Set leon inputs
    mvmm.A = A
    mvmm.B = B
    mvmm.C0 = C0
    mvmm.C = C

    mvmm.debugger.write_var('CHECK_UNUSED_SLICES', np.uint32(1))
    mvmm.debugger.write_var('M', np.uint32(M))
    mvmm.debugger.write_var('K', np.uint32(K))
    mvmm.debugger.write_var('N', np.uint32(N))
    mvmm.debugger.write_var('S', np.uint32(S))
    mvmm.debugger.write_var('S0', np.uint32(S0))
    mvmm.debugger.write_var('KernelType', np.uint32(mvmatmul.KernelType.ASM))
    mvmm.debugger.write_var('MatrixType', np.uint32(mvmatmul.MatMulType.MMT_HALF))
    mvmm.debugger.write_array('A', mvmm.A)
    mvmm.debugger.write_array('B', mvmm.B)
    mvmm.debugger.write_array('C', mvmm.C)

    # Run myriad and get output
    mvmm.debugger.run_command('mdbg::runw')
    mvmm.debugger.run_command('mdbg::uart flush')

    # Get output
    cycles = np.uint64(0)
    cycles = mvmm.debugger.read_var('MATMUL_CYCLES', cycles)
    Cmyriad = np.zeros((M, N), dtype=np.float16)
    Cmyriad = mvmm.debugger.read_array('C', Cmyriad)

    # Save arrays
    np.save('Cnumpy.npy', Cnumpy)
    np.save('Cmyriad.npy', Cmyriad)
    np.save('A.npy', mvmm.A)
    np.save('B.npy', mvmm.B)
    np.save('C0.npy', mvmm.C0)

    # Compare results numpy vs myriad C kernel
    print('numpy OUT[', M, 'x', N, '] = \n', Cnumpy)
    print('myriad ASM kernel OUT[', M, 'x', N, '] = \n', Cmyriad)

    # Compare results numpy vs myriad ASM kernel
    comp = np.allclose(Cnumpy, Cmyriad, atol=ATOL)
    if comp:
        print('Numpy result IS same as ASM kernel')
    else:
        print('Numpy result IS NOT same as ASM kernel')

    print('  MAX diff = ', np.amax(np.fabs(Cnumpy - Cmyriad)))
    print('  OUT of BOUNDS = ', (np.count_nonzero((Cnumpy - Cmyriad) > ATOL)))

    stats = mvmatmul.Matmul_stats()
    stats.cycles = cycles
    mvmatmul.log_stats(M, K, N, S, stats)

    print('Shave order test done -------------------------------------')


if __name__ == "__main__":

    mvmm = mvmatmul.Mvmatmul('../myriad', 'output/mvMatMul.elf')
    mvmm.init()

    if len(sys.argv) == 6:
        M = int(sys.argv[1])
        K = int(sys.argv[2])
        N = int(sys.argv[3])
        S = int(sys.argv[4])
        S0 = int(sys.argv[5])
        test_multiple_calls(mvmm, S, S0)
    else:
        for idx in range(len(TESTS)):
            S = TESTS[idx][0]
            S0 = TESTS[idx][1]
            test_multiple_calls(mvmm, S, S0)

    mvmm.close()

