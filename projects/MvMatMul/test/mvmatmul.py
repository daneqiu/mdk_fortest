'''
    @file
    @copyright All code copyright Movidius Ltd 2012, all rights reserved.
               For License Warranty see: common/license.txt
'''

import os
import sys
import numpy as np
from enum import IntEnum
sys.path.append('../../MvTensor/mvdbg/build/')
import myriad2Dbg as mdbg


class KernelType(IntEnum):
    GEMM_HHHH_NNN = 0
    GEMM_HHHH_NNN_C = 1
    GEMM_HHHH_NNN_K8 = 2
    GEMM_HHHH_NNN_K16 = 3
    GEMM_HHHH_NNN_NAC = 4


class MatMulType(IntEnum):
    MMT_INVALID = 0
    MMT_HALF = 1
    MMT_SINGLE = 2


class Matmul_stats:
    cycles = 0
    major_slices = 0
    minor_slices = 0
    kernel_calls = 0
    kernel_skips = 0


def log_stats(M, K, N, S, stats):
    cycles = stats.cycles
    flops = M * K * (2 * N - 1)
    ideal_cycles = 19 + M * (12 + N / 8 * (11 + 9 * K / 8))
    ideal_flops_cycle = flops / ideal_cycles
    flops_cycle = 0
    if cycles:
        flops_cycle = flops / cycles
    shave_ef = flops_cycle / (S * ideal_flops_cycle) * 100

    print('  M={}, K={}, N={}, S={}, flops/cycle={:.2f}, efficiency={:.2f}, cycles={}'.format(M, K, N, S, flops_cycle, shave_ef, cycles))

    load_eff = 0
    if stats.kernel_calls + stats.kernel_skips != 0:
        load_eff = 100 * stats.kernel_calls / (stats.kernel_calls + stats.kernel_skips)

    print('  kernel_calls={}, kernel_skips={}, load={:.2f}'.format(stats.kernel_calls, stats.kernel_skips, load_eff))


class Mvmatmul:

    project_dir = str()
    app_name = str()
    debugger = None
    minf = 0
    maxf = 1

    def __init__(self, pdir, appname):
        self.project_dir = pdir
        self.appname = appname
        os.chdir(self.project_dir)

    def init(self):
        self.debugger = mdbg.Myriad2Dbg(self.project_dir)
        self.debugger.init()
        self.debugger.load_application(self.appname)

    def close(self):
        self.debugger.close()

    def calculate(self, M, K, N, NSHVs, kt=KernelType.GEMM_HHHH_NNN, mt=MatMulType.MMT_SINGLE, newMats=True, gemv=False):

        with open("test_cases.txt", "a") as myfile:
            myfile.write(str(M)+","+str(K)+","+str(N)+"\n")

        dtype = None
        if mt == MatMulType.MMT_SINGLE:
            dtype = np.float32
        elif mt == MatMulType.MMT_HALF:
            dtype = np.float16

        # Variables to run matrix multiplication
        if newMats:
            self.A = np.random.uniform(self.minf, self.maxf, (M, K)).astype(dtype=dtype)
            self.B = np.random.uniform(self.minf, self.maxf, (K, N)).astype(dtype=dtype)
            self.C0 = np.random.uniform(self.minf, self.maxf, (N)).astype(dtype=dtype)
            self.C = np.tile(self.C0, (M, 1))

        # self.A = np.ones((M, K)).astype(dtype=dtype)
        # self.B = np.ones((K, N)).astype(dtype=dtype)

        #Numpy
        Cpy = np.dot(self.A, self.B) + self.C

        # Set leon inputs
        self.debugger.write_var('S', np.uint32(NSHVs))
        self.debugger.write_var('M', np.uint32(M))
        self.debugger.write_var('K', np.uint32(K))
        self.debugger.write_var('N', np.uint32(N))
        self.debugger.write_var('KernelType', np.uint32(kt))
        self.debugger.write_var('MatrixType', np.uint32(mt))

        self.debugger.write_array('A', self.A.flatten().astype(dtype=dtype))
        if gemv:
            self.debugger.write_array('B', np.transpose(self.B).flatten().astype(dtype=dtype))
        else:
            self.debugger.write_array('B', self.B)
        self.debugger.write_array('C', self.C.astype(dtype=dtype))

        # Run myriad and get output
        self.debugger.run_command('mdbg::runw')
        C = np.zeros((M, N), dtype=dtype)
        C = self.debugger.read_array('C', C)

        MATMUL_CYCLES = np.uint64(0)
        MATMUL_CYCLES = self.debugger.read_var('MATMUL_CYCLES', MATMUL_CYCLES)
        MATMUL_MAJOR_SLICES = np.uint64(0)
        MATMUL_MAJOR_SLICES = self.debugger.read_var('MATMUL_MAJOR_SLICES ', MATMUL_MAJOR_SLICES)
        MATMUL_MINOR_SLICES = np.uint64(0)
        MATMUL_MINOR_SLICES = self.debugger.read_var('MATMUL_MINOR_SLICES ', MATMUL_MINOR_SLICES)

        stats = Matmul_stats()
        stats.cycles = MATMUL_CYCLES
        stats.major_slices = MATMUL_MAJOR_SLICES
        stats.minor_slices = MATMUL_MINOR_SLICES
        stats.kernel_calls = 1
        stats.kernel_skips = 0

        return (Cpy, C, stats)

    def set_float_range(self, minf, maxf):
        self.minf = minf
        self.maxf = maxf



