'''
    @file
    @copyright All code copyright Movidius Ltd 2012, all rights reserved.
               For License Warranty see: common/license.txt
'''

import sys
import numpy as np
sys.path.append('../../shared/mvdbg')
import mvmatmul


# USAGE:
#   python3 test_strides.py M K N S A_STRIDE C_STRIDE
#   python3 test_strides.py

M = 49                      # Rows of matrix A
K = 416                     # Cols of matrix A, Rows of matrix B
N = 384                     # Cols of matrix B
S = 1                       # Number of shaves
A_STRIDE = 1664             # Stride in matrix A, in bytes
C_STRIDE = 2048             # Stride in matrix C, in bytes
ATOL = 1.0                  # Tolerance acceptable to compare myriad <-> numpy


def test_strides():

    print('Testing strides ---------------------------------------')

    assert not A_STRIDE % 2, 'A_STRIDE must be multiple of 2'
    assert not C_STRIDE % 2, 'C_STRIDE must be multiple of 2'
    assert A_STRIDE >= K * 2 , 'A_STRIDE must be equal or greater than K * 2'
    assert C_STRIDE >= N * 2 , 'C_STRIDE must be equal or greater than N * 2'

    A_STRIDE_ELEMS = int(A_STRIDE/2 - K)
    C_STRIDE_ELEMS = int(C_STRIDE/2 - N)

    def float_formatter(x): return "%.2f" % x
    np.set_printoptions(formatter={'float_kind': float_formatter}, linewidth=120)

    mvmm = mvmatmul.Mvmatmul('../myriad', 'output/mvMatMul.elf')
    mvmm.init()

    A = np.random.uniform(mvmm.minf, mvmm.maxf, (M, K)).astype(dtype=np.float16)
    B = np.random.uniform(mvmm.minf, mvmm.maxf, (K, N)).astype(dtype=np.float16)
    C0 = np.random.uniform(mvmm.minf, mvmm.maxf, (N)).astype(dtype=np.float16)
    C = np.tile(C0, (M, 1))
    Cnumpy = np.dot(A, B) + C

    A_STRIDED = np.zeros((M, K + A_STRIDE_ELEMS)).astype(dtype=np.float16)
    A_STRIDED[0:M, 0:K] = A

    C_STRIDED = np.zeros((M, N + C_STRIDE_ELEMS)).astype(dtype=np.float16)
    C_STRIDED[0:M, 0:N] = C

    # Set leon inputs
    mvmm.A = A_STRIDED
    mvmm.B = B
    mvmm.C0 = C0
    mvmm.C = C_STRIDED

    mvmm.debugger.write_var('A_STRIDE', np.uint32(A_STRIDE))
    mvmm.debugger.write_var('C_STRIDE', np.uint32(C_STRIDE))
    mvmm.debugger.write_var('MATMUL_CALLS', np.uint32(1))
    mvmm.debugger.write_var('MATMUL_TRACE', np.uint32(1))
    mvmm.debugger.write_var('MATMUL_CACHE', np.uint32(1))

    mvmm.debugger.write_var('S', np.uint32(S))
    mvmm.debugger.write_var('M', np.uint32(M))
    mvmm.debugger.write_var('K', np.uint32(K))
    mvmm.debugger.write_var('N', np.uint32(N))
    mvmm.debugger.write_var('KernelType', np.uint32(mvmatmul.KernelType.GEMM_HHHH_NNN))
    mvmm.debugger.write_var('MatrixType', np.uint32(mvmatmul.MatMulType.MMT_HALF))
    mvmm.debugger.write_array('A', mvmm.A)
    mvmm.debugger.write_array('B', mvmm.B)
    mvmm.debugger.write_array('C', mvmm.C)

    # Run myriad and get output
    mvmm.debugger.run_command('mdbg::runw')
    mvmm.debugger.run_command('mdbg::uart flush')

    # Get output
    cycles = np.uint64(0)
    cycles = mvmm.debugger.read_var('MATMUL_CYCLES', cycles)
    Caux = np.zeros((M, N + C_STRIDE_ELEMS), dtype=np.float16)
    Caux = mvmm.debugger.read_array('C', Caux)
    Cmyriad = Caux[0:M, 0:N]

    # Save arrays
    np.save('Cnumpy.npy', Cnumpy)
    np.save('Cmyriad.npy', Cmyriad)
    np.save('A.npy', mvmm.A)
    np.save('B.npy', mvmm.B)
    np.save('C0.npy', mvmm.C0)

    mvmm.close()

    # Compare results numpy vs myriad C kernel
    print('C stride = {}'.format(C_STRIDE_ELEMS * 2))
    print('numpy OUT[', M, 'x', N, '] = \n', Cnumpy)
    print('myriad ASM kernel OUT[', M, 'x', N, '] = \n', Cmyriad)

    # Compare results numpy vs myriad ASM kernel
    comp = np.allclose(Cnumpy, Cmyriad, atol=ATOL)
    if comp:
        print('Numpy result IS same as ASM kernel')
    else:
        print('Numpy result IS NOT same as ASM kernel')

    print('  MAX diff = ', np.amax(np.fabs(Cnumpy - Cmyriad)))
    print('  OUT of BOUNDS = ', (np.count_nonzero((Cnumpy - Cmyriad) > ATOL)))

    stats = mvmatmul.Matmul_stats()
    stats.cycles = cycles
    mvmatmul.log_stats(M, K, N, S, stats)

    print('Strides test done -------------------------------------')


if __name__ == "__main__":

    if len(sys.argv) == 7:
        M = int(sys.argv[1])
        K = int(sys.argv[2])
        N = int(sys.argv[3])
        S = int(sys.argv[4])
        A_STRIDE = int(sys.argv[5])
        C_STRIDE = int(sys.argv[6])

    test_strides()

