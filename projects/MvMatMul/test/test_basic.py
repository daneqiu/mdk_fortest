'''
    @file
    @copyright All code copyright Movidius Ltd 2012, all rights reserved.
               For License Warranty see: common/license.txt
'''

import sys
import numpy as np
sys.path.append('../../MvTensor/mvdbg')
import mvmatmul

test_single = False
test_half = True
test_C = False

# K size must be a multiple of 8
M = 1031
K = 192
N = 73
NSHVs = 12


def test_iteration(mvmm, dtype, atol, string):

        print('Testing ', string, ' precision ---------------------------------------')
        # Test ASM kernel
        mvmm.init()
        mvmm.debugger.write_var('MATMUL_CALLS', np.uint32(1))
        mvmm.debugger.write_var('MATMUL_TRACE', np.uint32(1))
        mvmm.debugger.write_var('MATMUL_CACHE', np.uint32(1))

        (Cnumpy_asm, Casm, stats) = mvmm.calculate(M, K, N, NSHVs, mvmatmul.KernelType.GEMM_HHHH_NNN, dtype, True)

        aux = np.zeros(4864, dtype=np.float32)
        aux = mvmm.debugger.read_array('s0Data', aux)
        np.save('s0Data.npy', aux)
        aux = mvmm.debugger.read_array('s1Data', aux)
        np.save('s1Data.npy', aux)
        np.save('Cnumpy.npy', Cnumpy_asm)
        np.save('Casm.npy', Casm)
        np.save('A.npy', mvmm.A)
        np.save('B.npy', mvmm.B)
        np.save('C0.npy', mvmm.C0)

        mvmm.close()

        if test_C:
            # Test C kernel
            mvmm.init()
            (Cnumpy_c, Cpurec, stats) = mvmm.calculate(M, K, N, NSHVs, mvmatmul.KernelType.PURE_C, dtype, False)
            mvmm.close()

        # Compare results numpy vs myriad C kernel
        print('numpy OUT[', M, 'x', N, '] = \n', Cnumpy_asm)
        if test_C:
            print('myriad C kernel OUT[', M, 'x', N, '] = \n', Cpurec)
        print('myriad ASM kernel OUT[', M, 'x', N, '] = \n', Casm)

        # Compare results numpy vs myriad ASM kernel
        if test_C:
            comp = np.allclose(Cnumpy_c, Cpurec, atol=atol)
            if comp:
                print('Numpy result IS same as C kernel')
            else:
                print('Numpy result IS NOT same as C kernel')

        comp = np.allclose(Cnumpy_asm, Casm, atol=atol)
        if comp:
            print('Numpy result IS same as ASM kernel')
        else:
            print('Numpy result IS NOT same as ASM kernel')

        print(' ASM kernel MAX diff = ', np.amax(np.fabs(Cnumpy_asm - Casm)))
        print(' ASM kernel OUT of BOUNDS = ', (np.count_nonzero((Cnumpy_asm - Casm) > atol)))
        if test_C:
            print(' C kernel MAX diff = ', np.amax(np.fabs(Cnumpy_c - Cpurec)))
            print(' C kernel OUT of BOUNDS = ', (np.count_nonzero((Cnumpy_c - Cpurec) > atol)))

        mvmatmul.log_stats(M, K, N, NSHVs, stats)

        print(string, ' precision test done -------------------------------------')


def test_basic():

    def float_formatter(x): return "%.2f" % x
    np.set_printoptions(formatter={'float_kind': float_formatter}, linewidth=120)

    mvmm = mvmatmul.Mvmatmul('../myriad', 'output/mvMatMul.elf')

    if test_single:
        test_iteration(mvmm, mvmatmul.MatMulType.MMT_SINGLE, 0.02, 'SINGLE')

    if test_half:
        test_iteration(mvmm, mvmatmul.MatMulType.MMT_HALF, 1.0, 'HALF')


if __name__ == "__main__":

    if len(sys.argv) == 5:
        M = int(sys.argv[1])
        K = int(sys.argv[2])
        N = int(sys.argv[3])
        NSHVs = int(sys.argv[4])

    test_basic()
