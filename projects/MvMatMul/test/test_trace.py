'''
    @file
    @copyright All code copyright Movidius Ltd 2012, all rights reserved.
               For License Warranty see: common/license.txt

    NOTE: need to build matmul enabling macro DMA_PROFILE in modules/matmul/share/dma_profile.h
'''

import sys
import numpy as np
import struct
import csv
sys.path.append('../../shared/mvdbg')
import mvmatmul


# K must be a multiple of 8
M = 3136
K = 64
N = 192
S = 8


class DmaProfilePOD:
    cnt = 0
    timestamp = 0
    src = 0
    src_line = 0
    src_stride = 0
    dst = 0
    dst_line = 0
    dst_stride = 0
    sz_bytes = 0

    def __init__(self, args):
        self.cnt = args[0]
        self.timestamp = args[1]
        self.src = args[2]
        self.src_line = args[3]
        self.src_stride = args[4]
        self.dst = args[5]
        self.dst_line = args[6]
        self.dst_stride = args[7]
        self.sz_bytes = args[8]

    def print_data(self):
        print('{:03d} {:07d} 0x{:08X} {:03d} {:03d} 0x{:08X} {:03d} {:03d} {:05d}'.format(
            self.cnt, self.timestamp, self.src, self.src_line, self.src_stride, self.dst,
            self.dst_line, self.dst_stride, self.sz_bytes))

    def csv(self):
        csv = '{:03d} {:07d} 0x{:08X} {:03d} {:03d} 0x{:08X} {:03d} {:03d} {:05d}'.format(
            self.cnt, self.timestamp, self.src, self.src_line, self.src_stride, self.dst,
            self.dst_line, self.dst_stride, self.sz_bytes)
        return csv


def test_strides():

    print('Tracing ---------------------------------------')

    def float_formatter(x): return "%.2f" % x
    np.set_printoptions(formatter={'float_kind': float_formatter}, linewidth=120)

    mvmm = mvmatmul.Mvmatmul('../myriad', 'output/mvMatMul.elf')
    mvmm.init()

    A = np.random.uniform(mvmm.minf, mvmm.maxf, (M, K)).astype(dtype=np.float16)
    B = np.random.uniform(mvmm.minf, mvmm.maxf, (K, N)).astype(dtype=np.float16)
    C0 = np.random.uniform(mvmm.minf, mvmm.maxf, (N)).astype(dtype=np.float16)
    C = np.tile(C0, (M, 1))

    mvmm.debugger.write_var('S', np.uint32(S))
    mvmm.debugger.write_var('M', np.uint32(M))
    mvmm.debugger.write_var('K', np.uint32(K))
    mvmm.debugger.write_var('N', np.uint32(N))
    mvmm.debugger.write_var('KernelType', np.uint32(mvmatmul.KernelType.ASM))
    mvmm.debugger.write_var('MatrixType', np.uint32(mvmatmul.MatMulType.MMT_HALF))
    mvmm.debugger.write_array('A', A)
    mvmm.debugger.write_array('B', B)
    mvmm.debugger.write_array('C', C)

    # Run myriad and get output
    mvmm.debugger.run_command('mdbg::runw')
    mvmm.debugger.run_command('mdbg::uart flush')

    # Get output
    cycles = np.uint64(0)
    cycles = mvmm.debugger.read_var('MATMUL_CYCLES', cycles)

    dma_log_cnt = np.zeros(S, dtype=np.int32)
    for idxs in range(S):
        var_name = 'MatMul{}_dma_log_cnt'.format(str(idxs))
        dma_log_cnt[idxs] = mvmm.debugger.read_var(var_name, dma_log_cnt[idxs])

    sz = 32 * 1024
    dma_log = np.zeros((S, sz), dtype=np.uint8)
    for idxs in range(S):
        var_name = 'MatMul{}_dma_log'.format(str(idxs))
        dma_log[idxs, :] = mvmm.debugger.read_array(var_name, dma_log[idxs, :])

    mvmm.close()

    trace_data = [[] for i in range(S)]
    for idxs in range(S):
        for idxl in range(dma_log_cnt[idxs]):
            offset = 32 * idxl
            trace_data[idxs].append(DmaProfilePOD((idxl,) + struct.unpack(
                'IIIIIIII', dma_log[idxs, offset:offset + 32].tobytes())))

    for idxs in range(S):
        filename = 'dma_log_shave_{}.csv'.format(idxs)
        with open(filename, 'w+') as csvfile:
            writer = csv.writer(csvfile, quoting=csv.QUOTE_NONE, delimiter=' ')
            for item in trace_data[idxs]:
                writer.writerow(item.csv().split())

    print('cycles =', cycles)
    print('dma_log_cnt =', dma_log_cnt)
    print('Trace done -------------------------------------')


if __name__ == "__main__":

    if len(sys.argv) == 5:
        M = int(sys.argv[1])
        K = int(sys.argv[2])
        N = int(sys.argv[3])
        S = int(sys.argv[4])

    test_strides()

