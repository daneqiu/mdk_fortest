#MvMatMul


This release contains a Myriad implementation of a matrix multipication library with accompanying Python test suites. For usage and installation see the README.md file.

####Current Restrictions
Dimension K must be multiple of 8.
No fp32 support
No GEMV support

####v0.0.5 (01/04/2016):
Bug fix: for small A matrices, solved inter-shave synchronization issue


####v0.0.4 (22/03/2016):
Support padding in M and N
Leon optimization through hash table, lowering memory requirements and improving timings


####v0.0.3 (14/03/2016):
Support striding
Added profiling capabilities


####v0.0.2 (19/02/2016):
Scheduling is now following M. Horea et al "An analysis of the suitability of the Movidius Myriad architecture for scientific computing"
Performance and parallelization efficiency is improved for large matrixes


####v0.0.1 (12/01/2016):
Large speedups included in this release, including a new assembly kernel.
Some bugfixes for edge cases.


####v0.0.0 (17/12/2015):
General Multiplication functions GEMV & GEMM for Matrix-Vector and Matrix-Matrix respectively.


**GEMM:**

* Optimized kernels for fp32 and fp16.
* Runs on any number of shaves provided.
* Scheduling system to split up matricies effeciently.
* Requires dimensions to be padded to be multiples of 4.
* Limited by the size of K (512) in a M\*K x K\*N. Unlimited by the size of M or N. Adjust limits in the main.cpp file.


**GEMV**

* Functional C kernels for fp32.
* An optomized kernel will be included in a future release.
* Limited by the size of K(1200) in a 1\*K x K\*N. Unlimited by the size of N. Adjust limits in the main.cpp file.
* Calculates A\*B', rather than A\*B for faster reading.

**Performance Figures:**
![2048 Matrix Graph](doc/m2048-k256-n2048.png)

 
