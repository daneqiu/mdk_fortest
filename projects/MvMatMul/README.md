# MvMatMul
Movidius Matrix Multiplication Library

Originally cloned from the MvCNN project

Supporting:

* GEMM

Myriad2
=======

Build
-----

    $ cd <repo_root>/myriad
    $ make -j

or

    $ make all MV_SOC_REV=ma2450

for RTEMS based test

    $ make RTOS=1 -j

Test
====
Test is done comparing bit exactness between myriad and numpy. Python3 testing
script will access the myriad. The debug server must be up and running.
To access the debug server, a swig wrapper is generated in order to use the
debugger's dynamic library from within python

To run the unit tests, navigate to the unittest folder and run python3 mvmatmul_unittest.py. 


SWIG
----
Using swig > 3.0.7. If available in default package repositories, install with
(NOTE: commands with # as root, and with $ as any non-root user)

    # aptitude install swig

If building swig from source, please check if MV_TOOLS_VERSION is set to the correct version 
of Tools that you will be using. This is different to MV_TOOLS_VER that you may have set currently. 
This will be corrected in a later release.

If needed, remove existing installation of swig

    # aptitude purge swig

Install tools and components

    # aptitude install automake autoconf libtool byacc yodl tidy htmldoc

To clone, build and install swig, do

    # aptitude build-dep swig
    $ cd swig
    $ git clone /home/gartola/dev/repos/git/swig.git
    $ git checkout tags/rel-3.0.7
    $ ./autogen.sh
    $ ./configure --prefix=/opt/swig
    $ make
    $ make docs
    # make install
    $ cd $HOME/bin; ln -s /opt/swig/bin/swig

assuming *$HOME/bin* is in your path

Python requirements
-------------------

Requirements are

    # aptitude install python3-dev python3-yaml python3-numpy python3-pip python3-matplotlib
    # pip3 install unittest-xml-reporting

Generating SWIG wraper
----------------------
To generate swig wrapers and make the debug server available in python run

    $ cd <MvTensor>/mvdbg
    $ mkdir build
    $ cd build
    $ cmake ..
    $ make


Running python unittest
-----------------------

Build the myriad project and start the moviDebugServer, then

    $ cd <repo_root>/unittest
    $ python3 mvmatmul_unittest.py

results are collected in *<repo_root>/unittest/report*

It is possible to add test cases in the yaml configuration file.

Benchmarking
-------------

MvMatMul can be benchmarked running

    $ cd <repo_root>/test
    $ python3 test_benchmark.py

Profiling
----------

MvMatMul can be profiled running

    $ cd <repo_root>/myriad
    $ make matmul_profile

Debugging
----------

MvMatMul can be debugged running

    $ cd <repo_root>/test
    $ python3 test_basic.py M K N S

Multi-threading can be tested, building for RTEMS and running

    $ cd <repo_root>/test
    $ python3 test_rtems

