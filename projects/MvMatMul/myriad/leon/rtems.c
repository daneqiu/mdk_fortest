
#if defined(__RTEMS__)
#include <string.h>
#include <stdio.h>
#include "mv_types.h"
#include "app_config.h"
#include "swcCrc.h"
#include <swcShaveLoaderLocal.h>
#include <DrvLeonL2C.h>
#include <OsDrvShaveL2Cache.h>
#include <stdlib.h>
#include <OsDrvSvu.h>
#include <DrvCpr.h>
#include <OsDrvCpr.h>
#include "rtems.h"
#include "app_config.h"
#include "mvHelpersApi.h"
#include <theSubModulePrivate.h>

#define S_MAX 12
extern DynamicSubModule_t MODULE_DATA(MatMul);
DynamicSubModule_t localModule[S_MAX];
struct DynamicSubModulePrivate_elm localModulePrivD[S_MAX];

void rtems_test(void);

void POSIX_Init (void* args)
{
    UNUSED(args);
    int shvNo;
    int i;

    initClocksAndMemory();

    u64 shavesEnableMask = 0;
    MODULE_DATA(MatMul).heapSize = 1024;
    for(shvNo = 0; shvNo < S_MAX; shvNo++)
    {
        memcpy(&localModule[shvNo], &MODULE_DATA(MatMul),sizeof(DynamicSubModule_t));
        memcpy(&localModulePrivD[shvNo], MODULE_DATA(MatMul).privateData, sizeof(localModulePrivD[shvNo]));
        localModule[shvNo].privateData = &localModulePrivD[shvNo];
    }

    shavesEnableMask |= 1 << (0);
    shavesEnableMask |= 1 << (1);
    shavesEnableMask |= 1 << (2);
    shavesEnableMask |= 1 << (3);
    swcShaveUnit_t svuList1[2] = {0 ,1};
    swcShaveUnit_t svuList2[2] = {2, 3};

    int status;
    status = OsDrvSvuInit();
    assert(status == 0);

    status = OsDrvSvuOpenShaves(svuList1, 2, OS_MYR_PROTECTION_SEM);
    assert(status == 0);

    status = OsDrvSvuOpenShaves(svuList2, 2, OS_MYR_PROTECTION_SEM);
    assert(status == 0);

    status = OsDrvCprTurnOnShaveMask(shavesEnableMask);
    assert(status == 0);

    for (int i = 0; i < 4; i++)
    {
        swcDynSetShaveWindows(&localModule[i], i);
        OsDrvShaveL2CSetWindowPartition((shaveId_t)i, SHAVEL2CACHEWIN_C, 0);
        OsDrvShaveL2CSetNonWindowedPartition((shaveId_t)i, 0, NON_WINDOWED_DATA_PARTITION);
        OsDrvShaveL2CSetNonWindowedPartition((shaveId_t)i, 1, NON_WINDOWED_INSTRUCTIONS_PARTITION);
    }
    swcSetupDynShaveAppsComplete(&localModule[0], &svuList1[0], 1);
    swcSetupDynShaveAppsComplete(&localModule[1], &svuList1[1], 1);
    swcSetupDynShaveAppsComplete(&localModule[2], &svuList2[0], 1);
    swcSetupDynShaveAppsComplete(&localModule[3], &svuList2[1], 1);

    rtems_test();

    status = OsDrvSvuCloseShaves(svuList1, 2);
    assert(status == 0);
    status = OsDrvSvuCloseShaves(svuList2, 2);
    assert(status == 0);

    exit(0);
}

void Fatal_extension(Internal_errors_Source the_source, bool is_internal, uint32_t the_error)
{
    switch (the_source)
    {
        case RTEMS_FATAL_SOURCE_EXIT:
            if (the_error)
            printk("Exited with error code %d\n", the_error);
        break; // normal exit
        case RTEMS_FATAL_SOURCE_ASSERT:
            printk("%s : %d in %s \n", ((rtems_assert_context * )the_error)->file,
                    ((rtems_assert_context * )the_error)->line,
                    ((rtems_assert_context * )the_error)->function);
        break;
        case RTEMS_FATAL_SOURCE_EXCEPTION:
            rtems_exception_frame_print((const rtems_exception_frame *) the_error);
        break;
        default:
            printk("\nSource %d Internal %d Error %d  0x%X:\n", the_source,
                    is_internal, the_error, the_error);
        break;
    }
}
#endif
