///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef _APP_CONFIG_H_
#define _APP_CONFIG_H_

#include <mv_types.h>

#define MAX_SHAVES 12

#ifdef __cplusplus
extern "C" {
#endif

//notify other tools of the cmx configuration
extern CmxRamLayoutCfgType __cmx_config;
extern u32 __l2_config ;

/// Setup all the clock configurations needed by this application and also the ddr
///
/// @return    0 on success, non-zero otherwise
int initClocksAndMemory(void);

#ifdef __cplusplus
}
#endif

#endif

