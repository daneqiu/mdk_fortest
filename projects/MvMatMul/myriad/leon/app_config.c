///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Application configuration Leon file
///
#include "app_config.h"
#include <registersMyriad.h>
#include <swcShaveLoader.h>
#if defined(__RTEMS__)
#include <myriad2_version.h>
#include <OsDrvTimer.h>
#include <OsDrvCpr.h>
#include <OsDrvSvu.h>
#include <OsDrvShaveL2Cache.h>
#include "rtems_config.h"
#else
#include "DrvCpr.h"
#include <DrvDdr.h>
#include <DrvRegUtils.h>
#include <DrvShaveL2Cache.h>
#include "DrvGpioDefines.h"
#include "DrvGpio.h"
#include "DrvTimer.h"
#include "brdGpioCfgs/brdMv0182GpioDefaults.h"
#endif
#include <DrvLeonL2C.h>

#define CMX_CONFIG_SLICE_7_0       (0x11111111)
#define CMX_CONFIG_SLICE_15_8      (0x11111111)
#define L2CACHE_CFG                (SHAVE_L2CACHE_BYPASS_MODE)

#define APP_UPA_CLOCKS (DEV_UPA_SH0      | \
                        DEV_UPA_SH1      | \
                        DEV_UPA_SH2      | \
                        DEV_UPA_SH3      | \
                        DEV_UPA_SH4      | \
                        DEV_UPA_SH5      | \
                        DEV_UPA_SH6      | \
                        DEV_UPA_SH7      | \
                        DEV_UPA_SH8      | \
                        DEV_UPA_SH9      | \
                        DEV_UPA_SH10     | \
                        DEV_UPA_SH11     | \
                        DEV_UPA_SHAVE_L2 | \
                        DEV_UPA_CDMA     | \
                        DEV_UPA_CTRL         )

// Sections decoration is required here for downstream tools
u32 __l2_config   __attribute__((section(".l2.mode")))  = L2CACHE_CFG;
CmxRamLayoutCfgType __attribute__((section(".cmx.ctrl"))) __cmx_config = {CMX_CONFIG_SLICE_7_0, CMX_CONFIG_SLICE_15_8};

#if defined(__RTEMS__)
/*
 * RTEMS --------------------------------------------------------------------------------
 */

#define EXTRACLOCKS   (DEV_MSS_APB_SLV     | \
                       DEV_MSS_APB2_CTRL   | \
                       DEV_MSS_RTBRIDGE    | \
                       DEV_MSS_RTAHB_CTRL  | \
                       DEV_MSS_LRT         | \
                       DEV_MSS_LRT_DSU     | \
                       DEV_MSS_LRT_L2C     | \
                       DEV_MSS_LRT_ICB     | \
                       DEV_MSS_AXI_BRIDGE  | \
                       DEV_MSS_MXI_CTRL  )

#define PLL_DESIRED_FREQ_KHZ        600000
#define DEFAULT_OSC0_KHZ            12000
BSP_SET_CLOCK(DEFAULT_OSC0_KHZ, PLL_DESIRED_FREQ_KHZ, 1, 1,
              (DEFAULT_RTEMS_CSS_LOS_CLOCKS),
              EXTRACLOCKS,
              APP_UPA_CLOCKS, 0, 0);

// Program the  L2C at startup
#if (MV_RTEMS_VERSION > 0x02000000)
BSP_SET_L2C_CONFIG(0, L2C_REPL_LRU, 0, L2C_MODE_WRITE_THROUGH, 0, NULL);
#else
BSP_SET_L2C_CONFIG(0, L2C_REPL_LRU, 0, L2C_MODE_WRITE_TROUGH, 0, NULL);
#endif

int initClocksAndMemory(void)
{
    swcLeonDataCacheFlush();

    OsDrvCprInit();
    OsDrvCprOpen();
    OsDrvCprSysDeviceAction(UPA_DOMAIN, DEASSERT_RESET, APP_UPA_CLOCKS);
    OsDrvCprSysDeviceAction(MSS_DOMAIN, DEASSERT_RESET, EXTRACLOCKS);
    OsDrvTimerInit();
    OsDrvShaveL2CacheInit(L2CACHE_CFG);

    int part1Id, part2Id;
    OsDrvShaveL2CGetPartition(SHAVEPART16KB, &part1Id);
    OsDrvShaveL2CGetPartition(SHAVEPART128KB, &part2Id);
    OsDrvShaveL2CacheAllocateSetPartitions();
    OsDrvShaveL2CachePartitionInvalidate(part1Id);
    OsDrvShaveL2CachePartitionInvalidate(part2Id);

    return 0;
}
#else
/*
 * BARE METAL ---------------------------------------------------------------------------
 */

#ifndef BUP_PLL0_FREQ
    // no PLL by default
    #define BUP_PLL0_FREQ 300000
#endif
#ifndef BUP_PLL1_FREQ
    // no PLL by default
    #define BUP_PLL1_FREQ 300000
#endif

static tyAuxClkDividerCfg auxClk[] =
{
        {
            .auxClockEnableMask = AUX_CLK_MASK_LCD | CSS_AUX_MEDIA,
            .auxClockSource = CLK_SRC_SYS_CLK,
            .auxClockDivNumerator = 1,
            .auxClockDivDenominator = 1
        },    // media clock and LCD
        {0, 0, 0, 0}, // Null Terminated List
    };

int initClocksAndMemory(void)
{
    int i;

    tySocClockConfig clockConfig =
    {
        .refClk0InputKhz         = 12000,           // Default 12Mhz input clock
        .refClk1InputKhz         = 0,               // Assume no secondary oscillator for now
        .targetPll0FreqKhz       = 604000,          // should be multiple of 12000
        .targetPll1FreqKhz       = 0,               // will be set by DDR driver
        .clkSrcPll1              = CLK_SRC_REFCLK1, // Supply both PLLS from REFCLK0
        .masterClkDivNumerator   = 1,
        .masterClkDivDenominator = 1,
        .cssDssClockEnableMask   = DEFAULT_CORE_CSS_DSS_CLOCKS,
        .mssClockEnableMask      = APP_UPA_CLOCKS,
        .upaClockEnableMask      = 0xFFFFFFFF,
        .sippClockEnableMask     = 0,
        .pAuxClkCfg              = auxClk
    };

    LL2CConfig_t ll2cfg =
    {
        .LL2CEnable = 1,
        .LL2CLockedWaysNo = 0,
        .LL2CWayToReplace = 0,
        .busUsage = BUS_WRAPPING_MODE,
        .hitRate = HIT_WRAPPING_MODE,
        .replacePolicy = LRU,
        .writePolicy = WRITE_THROUGH
    };

    swcLeonDataCacheFlush();
    DrvLL2CInitialize(&ll2cfg);
    DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_INVALIDATE, 0);

    // Configure the system
    DrvCprInit();
    DrvCprSetupClocks(&clockConfig);
    SET_REG_WORD(MSS_CLK_CTRL_ADR,      0xffffffff);
    DrvCprSysDeviceAction(MSS_DOMAIN, DEASSERT_RESET, 0xFFFFFFFF);
    DrvCprSysDeviceAction(UPA_DOMAIN, DEASSERT_RESET, APP_UPA_CLOCKS);

    DrvTimerInit();
    DrvDdrInitialise(NULL);

    DrvShaveL2CacheSetupPartition(SHAVEPART256KB);
    DrvShaveL2CacheAllocateSetPartitions();
    for (i = 0; i < 12; i++)
    {
        DrvShaveL2CacheSetLSUPartId(i, 0);
    }

    DrvGpioInitialiseRange(brdMV0182GpioCfgDefault);

    return 0;
}
#endif
