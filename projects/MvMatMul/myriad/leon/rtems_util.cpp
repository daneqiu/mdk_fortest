#include "rtems_util.h"

#if defined(__RTEMS__)

namespace matmul
{

Thread::Thread()
{
}

Thread::~Thread()
{
}

int Thread::start()
{
    int sc = pthread_attr_init(&attr_);
    if (sc)
    {
        printf("ERROR: Thread::create() pthread_attr_init");
        return sc;
    }
    sc = pthread_attr_setinheritsched(&attr_, PTHREAD_EXPLICIT_SCHED);
    if (sc)
    {
        printf("ERROR: Thread::create() pthread_attr_setinheritsched");
        return sc;
    }
    sc = pthread_attr_setschedpolicy(&attr_, SCHED_RR);
    if (sc)
    {
        printf("ERROR: Thread::create() pthread_attr_setschedpolicy");
        return sc;
    }

    int rc = pthread_create(&handle_, NULL, run_internal, this);
    if (rc != 0)
    {
        printf("ERROR: Thread::start() pthread_create\n");
    }

    return 0;
}

void Thread::join()
{
    int rc = pthread_join(handle_, NULL);
    if (rc != 0)
    {
        printf("ERROR: Thread::join() pthread_join\n");
    }
}

long unsigned Thread::id()
{
    return pthread_self();
}

void* Thread::run_internal(void* ptr)
{
    Thread* th = reinterpret_cast<Thread*>(ptr);
    th->run();
    pthread_exit(0);
    return 0;
}

} /* namespace matmul */

#endif //if defined(__RTEMS__)

