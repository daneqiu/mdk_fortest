///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

// 1: Includes
// ----------------------------------------------------------------------------
#if !defined(__RTEMS__)
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <string.h>
#include <DrvLeonL2C.h>
#include <DrvTimer.h>
#include "mv_types.h"
#include <swcCrc.h>
#include "mvHelpersApi.h"

#include "app_config.h"
#include "matmul_iface.h"
#include "matmul_profile.h"
#include "matmul_leon.h"
#include "matmul_cache.h"
#include "dma_profile.h"

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
static const int LEON_HEAP_SIZE = 64 * 1024;
static const int SCRATCH_MEMORY = 110 * 1024;
u8 DDR_BSS leonHeap[LEON_HEAP_SIZE];
char __attribute__((section(".ddr.bss"))) cache_memory[matmul::CACHE_MEMORY_SIZE];
char __attribute__((section(".cmx.bss"))) matmul_scratch_memory[SCRATCH_MEMORY];

static const int SINGLE_CALLS_MAX = 3;
static const int S_MAX = 12;
static const int A_MAX_SIZE = 2 * 1024 * 1024;
static const int B_MAX_SIZE = 3 * 1024 * 1024;
static const int C_MAX_SIZE = 6 * 1024 * 1024;

float DDR_BSS_DIRECT A[A_MAX_SIZE] __attribute__((aligned(64)));
float DDR_BSS_DIRECT B[B_MAX_SIZE] __attribute__((aligned(64)));
float DDR_BSS_DIRECT C[C_MAX_SIZE] __attribute__((aligned(64)));
float DDR_BSS_DIRECT AUX[C_MAX_SIZE] __attribute__((aligned(64)));
int DDR_DATA M = 200;
int DDR_DATA K = 528;
int DDR_DATA N = 256;
int DDR_DATA S = 12;
int DDR_DATA KernelType = matmul::GEMM_HHHH_NNN;
int DDR_DATA MatrixType = matmul::MMT_HALF;
uint64_t DDR_DATA MATMUL_CYCLES;
uint64_t DDR_DATA MATMUL_MAJOR_SLICES = 0;
uint64_t DDR_DATA MATMUL_MINOR_SLICES = 0;
int DDR_DATA A_STRIDE = 0;
int DDR_DATA C_STRIDE = 0;
int DDR_DATA C_PADDING = 0;
int DDR_DATA C_PTR_OFFSET = 0;
int DDR_DATA NUMBER_OF_CALLS = 1;
int DDR_DATA S0 = 0;
int DDR_DATA MATMUL_CALLS = 1;
int DDR_DATA CHECK_UNUSED_SLICES = 0;
int DDR_DATA MATMUL_TRACE = 0;
int DDR_DATA MATMUL_CACHE = 0;

uint64_t DDR_DATA MATMUL_KERNEL_CALLS = 1;
uint64_t DDR_DATA MATMUL_KERNEL_SKIPS = 0;
extern int MatMul0_MATMUL_KERNEL_CALLS;
extern int MatMul0_MATMUL_KERNEL_SKIPS;

extern DynamicContext_t MODULE_DATA(MatMul);
DynamicContext_t localModule[S_MAX];
DynamicContextInstances_elm localModulePrivD[S_MAX];

// 6: Functions Implementation
// ----------------------------------------------------------------------------
static void clearUnusedSlice(unsigned int sliceNo, unsigned int crcs[], bool check)
{
    const unsigned int cmxStartAddr = 0x70000000;
    const unsigned int cmxSliceSize = 0x20000;
    u8* sliceStart = (u8*)(cmxStartAddr + sliceNo * cmxSliceSize);

    printf("Slice %d: %p - %p CRC...", sliceNo, sliceStart, sliceStart + cmxSliceSize);

    if (!check)
    {
        memset(sliceStart, 0xAB, cmxSliceSize);
        crcs[sliceNo] = swcCalcCrc32(sliceStart, cmxSliceSize, le_pointer);
        printf("%x\n", crcs[sliceNo]);
    }
    else
    {
        unsigned int crc = swcCalcCrc32(sliceStart, cmxSliceSize, le_pointer);
        printf("%x\n", crc);
        assert(crcs[sliceNo] == crc);
    }
}

static void clearUnusedSlices(unsigned int crcs[], bool check)
{
    if (!check)
        printf("\nClearing unused Shave slices...\n");
    else
        printf("\nChecking unused Shave slices...\n");

    for (int i = 0; i < S0; i++)
        clearUnusedSlice(i, crcs, check);

    for (int i = S0 + S; i < S_MAX; i++)
        clearUnusedSlice(i, crcs, check);
}

int main(void)
{
    // Heap for the profiler
    mvSetHeap((unsigned int)leonHeap, LEON_HEAP_SIZE);

    initClocksAndMemory();

    MODULE_DATA(MatMul).heap_size = 1024;
    saveShaveDynContextData(0, S_MAX, &MODULE_DATA(MatMul));

    assert(S <= S_MAX);

    int shvNo;
    for(shvNo = S0; shvNo < S; shvNo++)
        setupShaveDynApp(shvNo);

    unsigned int crcs[S_MAX];
    if (CHECK_UNUSED_SLICES) clearUnusedSlices(crcs, 0);

    printf("C[m][n] = A[m][k] * B[k][n] + C[m][n]\n");
    if (!MATMUL_TRACE)
    {
        printf("M               = %d\n", M);
        printf("K               = %d\n", K);
        printf("N               = %d\n", N);
        printf("S               = %d\n", S);
        printf("C               = %d\n", MATMUL_CALLS);
        printf("S0              = %d\n", S0);
    }

    assert(M * K * MATMUL_CALLS * sizeof(half) < A_MAX_SIZE);
    assert(K * N * MATMUL_CALLS * sizeof(half) < B_MAX_SIZE);
    assert(M * N * MATMUL_CALLS * sizeof(half) < C_MAX_SIZE);
    MATMUL_CYCLES = 0;

    matmul::MatMulConfig cfg;
    cfg.matrix_type = static_cast<matmul::MatMulType> (MatrixType);
    cfg.kernel_type = static_cast<matmul::kernel_t> (KernelType);
    cfg.kernel_width = 8;
    cfg.dma_link_agent = 3;
    cfg.first_shave = S0;
    cfg.scratch_memory_size = sizeof(matmul_scratch_memory);
    cfg.scratch_memory = matmul_scratch_memory;
    cfg.cache_memory_ptr = cache_memory;
    cfg.cache_memory_size = sizeof(cache_memory);
    cfg.error_buffer = 0;
    matmul::MatMulCache& cache = matmul::MatMulCache::instance();
    cache.config(cfg);
    matmul::MvMatMul<half> mmm(cfg, cache);
    mmm.disable_trace();
    if (MATMUL_TRACE) mmm.enable_trace();

    int strideA;
    A_STRIDE ? strideA = A_STRIDE : strideA = K * sizeof(half);
    int strideB = N * sizeof(half);
    int strideC;
    C_STRIDE ? strideC = C_STRIDE : strideC = N * sizeof(half);
    half* ptrC;
    C_PADDING ? ptrC = (half*) C + (C_PADDING * 2 + N) * C_PADDING + C_PADDING : ptrC = (half*) C;

    matmul::MatMulBuffer<half> bufA((half*)A, M * strideA);
    matmul::MatMulBuffer<half> bufB((half*)B, K * strideB);
    matmul::MatMulBuffer<half> bufC((half*)ptrC, M * strideC);
    matmul::Tensor tA(&bufA, matmul::DataTypeToEnum<half>::v(), M, K, strideA);
    matmul::Tensor tB(&bufB, matmul::DataTypeToEnum<half>::v(), K, N, strideB);
    matmul::Tensor tC(&bufC, matmul::DataTypeToEnum<half>::v(), M, N, strideC);

    matmul::TensorList lOptions;
    for (int i = 0; i < MATMUL_CALLS; i++)
    {
        //Options: offsets are meassured in elements
        if (i == 0)
        {
            matmul::MatMulOptions opt(M * strideA * i, K * strideB * i, 0, matmul::GEMM_HHHH_NNN_NAC);
            lOptions.add(opt);
        }
        else
        {
            matmul::MatMulOptions opt(M * strideA * i, K * strideB * i, 0);
            lOptions.add(opt);
        }
    }

    if (MATMUL_CALLS == 1)
    {
        // One call test
        if (MATMUL_CACHE)
        {
            memcpy(AUX, C, (M + C_PADDING * 2) * strideC);
            mmm.multiply(tA, tB, tC, S, static_cast<matmul::kernel_t> (KernelType));
            memcpy(C, AUX, (M + C_PADDING * 2) * strideC);
        }
        for (int idx = 0; idx < NUMBER_OF_CALLS; idx++)
            MATMUL_CYCLES = mmm.multiply(tA, tB, tC, S, static_cast<matmul::kernel_t> (KernelType));
    }
    else
    {
        // Single call test
        if (MATMUL_CACHE)
        {
            memcpy(AUX, C, M * MATMUL_CALLS * strideC);
            mmm.multiply(tA, tB, tC, S, lOptions);
            memcpy(C, AUX, M * MATMUL_CALLS * strideC);
        }
        MATMUL_CYCLES = mmm.multiply(tA, tB, tC, S, lOptions);
    }

    if (MATMUL_TRACE)
    {
        MATMUL_MAJOR_SLICES = mmm.tracer().stats_.major_slices;
        MATMUL_MINOR_SLICES = mmm.tracer().stats_.minor_slices;
        mmm.tracer().log_short();
        mmm.tracer().log_extended();
    }

    for(shvNo = S0; shvNo < S; shvNo++)
        cleanShaveDynApp(shvNo);

    if (CHECK_UNUSED_SLICES) clearUnusedSlices(crcs, 1);

    if (!MATMUL_TRACE) printf("cycles          = %llu \n", MATMUL_CYCLES);

    MATMUL_PROFILER_LOG;
    DMA_PROFILER_LOG;

    return 0;
}
#endif

