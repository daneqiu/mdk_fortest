#if defined(__RTEMS__)

#include <stdlib.h>
#include <stdio.h>
#include <stdio.h>
#include <new>
#include <assert.h>
#include <string.h>
#include "rtems_util.h"
#include "matmul_leon.h"
#include "matmul_cache.h"

static const int THREADS_MAX = 2;
static const int SHAVES_MAX = 2;
static const int A_MAX_SIZE = 2 * 1024 * 1024;
static const int B_MAX_SIZE = 3 * 1024 * 1024;
static const int C_MAX_SIZE = 6 * 1024 * 1024;
static const int unsigned CACHE_MEMORY_SIZE = 2 * 1024 * 1024;
static const int unsigned SCRATCH_MEMORY_SIZE = 32 * 1024;
int DDR_DATA RUNS = 5;
int DDR_DATA THREADS = THREADS_MAX;
int DDR_DATA S = SHAVES_MAX;
int DDR_DATA M[] = {224*224, 64*128, 64*64, 224*64, 16*16, 28*28, 32*32, 64*64, 7*7, 56*56, 16*16};
int DDR_DATA K[] = {1, 1, 3, 16, 32, 16, 128, 48, 64, 8, 256};
int DDR_DATA N[] = {16, 32, 256, 16, 128, 8, 256, 56, 128, 196, 8};
int DDR_DATA KernelType = 0;
int DDR_DATA MatrixType = matmul::MMT_HALF;
half DDR_BSS_DIRECT A[A_MAX_SIZE];
half DDR_BSS_DIRECT B[B_MAX_SIZE];
half DDR_BSS_DIRECT C[C_MAX_SIZE];

char __attribute__((section(".ddr.bss"))) cache_memory[THREADS_MAX * CACHE_MEMORY_SIZE];
char __attribute__((section(".cmx.bss"))) scratch_memory[THREADS_MAX * SCRATCH_MEMORY_SIZE];

class TestMultiThreading : public matmul::Thread
{
    static int scounter;

public:
    TestMultiThreading();
private:
    void run();
    int instance_;
};

int TestMultiThreading::scounter = 0;

TestMultiThreading::TestMultiThreading()
    : Thread()
    , instance_(scounter)
{
    scounter++;
}

void TestMultiThreading::run()
{

    //printf("instance_ = %d\n", instance_);

    matmul::MatMulCache cache(&cache_memory[instance_ * CACHE_MEMORY_SIZE], CACHE_MEMORY_SIZE);
    matmul::MatMulConfig cfg;
    cfg.matrix_type = static_cast<matmul::MatMulType> (MatrixType);
    cfg.kernel_type = static_cast<matmul::kernel_t> (KernelType);
    cfg.kernel_width = 8;
    cfg.dma_link_agent = instance_ * SHAVES_MAX;
    cfg.first_shave = instance_ * SHAVES_MAX;
    cfg.scratch_memory_size = SCRATCH_MEMORY_SIZE;
    cfg.scratch_memory = &scratch_memory[instance_ * SCRATCH_MEMORY_SIZE];
    cfg.error_buffer = 0;
    matmul::MvMatMul<half> mmm(cfg, cache);
    mmm.disable_trace();

    int runs = 1;
    while (1)
    {
        unsigned m = M[(runs%10)+instance_];
        unsigned k = K[(runs%10)+instance_];
        unsigned n = N[(runs%10)+instance_];
        //printf("m=%d, k=%d, n%n\n", m, k, n);

        half* ptrA = &A[instance_ * m * k];
        half* ptrB = &B[instance_ * k * n];
        half* ptrC = &C[instance_ * m * n];
        int strideA = k * sizeof(half);
        int strideB = n * sizeof(half);
        int strideC = n * sizeof(half);
        matmul::MatMulBuffer<half> bufA((half*)ptrA, m * strideA);
        matmul::MatMulBuffer<half> bufB((half*)ptrB, k * strideB);
        matmul::MatMulBuffer<half> bufC((half*)ptrC, m * strideC);
        matmul::Tensor tA(&bufA, matmul::DataTypeToEnum<half>::v(), m, k, strideA);
        matmul::Tensor tB(&bufB, matmul::DataTypeToEnum<half>::v(), k, n, strideB);
        matmul::Tensor tC(&bufC, matmul::DataTypeToEnum<half>::v(), m, n, strideC);

        memset(bufC.data(), 0, bufC.size());
        mmm.multiply(tA, tB, tC, S);

        if((runs % 100) == 0)
            printf("Run %d in thread %d with id %lu\n", runs, instance_, id());
        runs++;
    }
}

extern "C" void rtems_test(void)
{
    printf("RTEMS TEST RUNNING ...\n");

    assert(THREADS <= THREADS_MAX);
    assert(S == SHAVES_MAX);

    for(int i = 0; i <= 10; i++)
    {
        assert(THREADS * M[i] * K[i] * sizeof(half) <= A_MAX_SIZE);
        assert(THREADS * K[i] * N[i] * sizeof(half) <= B_MAX_SIZE);
        assert(THREADS * M[i] * N[i] * sizeof(half) <= C_MAX_SIZE);
    }

    TestMultiThreading* ths = new TestMultiThreading[THREADS];

    for (int i = 0; i < THREADS; i++)
        ths[i].start();

    for (int i = 0; i < THREADS; i++)
        ths[i].join();

    delete [] ths;

    printf("RTEMS TEST DONE\n");
}

#endif
