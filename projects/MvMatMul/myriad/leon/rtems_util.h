#ifndef _RTEMS_UTIL_H_
#define _RTEMS_UTIL_H_

#if defined(__RTEMS__)

#include <stdio.h>
#include <pthread.h>
#include "rtems_util.h"

#ifdef __cplusplus

namespace matmul
{

class Thread
{
public:
    Thread();
    virtual ~Thread();

    int start();
    void join();
    long unsigned id();
    virtual void run() = 0;

private:
    static void* run_internal(void* ptr);

    pthread_t handle_;
    pthread_attr_t attr_;
};

} /* namespace matmul */

#endif /* __cplusplus */

#endif //if defined(__RTEMS__)

#endif // _RTEMS_UTIL_H_

