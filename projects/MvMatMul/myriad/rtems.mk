# ------------------------------[ App ]--------------------------------------------#
# Default application name is the same as the folder it is in.
# This can be overridden here if something different is required
APPNAME ?= mvMatMul
VERS_STR = 0.1.0-34

DirAppRoot = .
MATMUL_DIR= $(DirAppRoot)/../modules/matmul
MV_COMMON_BASE   ?= ../../../common

MV_SOC_OS = rtems
RTEMS_BUILD_NAME =b-prebuilt

MV_SOC_PLATFORM = myriad2
MV_SOC_REV ?= ma2450
LinkerScript ?= config/custom.ldscript

MV_MVDBG_GEN = 2
DISABLE_ERROR_ON_WARNINGS =yes

# Include the generic Makefile
include $(DirAppRoot)/sources.mk
include $(MV_COMMON_BASE)/generic.mk
include $(MV_COMMON_BASE)/components/sampleProfiler/sampleProfiler.mk
include $(DirAppRoot)/rules.mk

MVLIBOPT := -T ./config/local_shave_phase.ldscript $(filter-out -T,$(filter-out $(MOVI_COMPILE_LDSCR),$(MVLIBOPT)))

TEST_TAGS := "MA2150, MA2450"

