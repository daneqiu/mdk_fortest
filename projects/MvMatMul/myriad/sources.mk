
KERNEL_LIB_DIR = $(MV_COMMON_BASE)/components/kernelLib
SHAVE_COMPONENTS = no

#-------------------------------[ Local shave applications sources ]--------------------#
MatMulApp = MatMul
DirTestOutput = ./output/$(MatMulApp)
SHAVE_CPP_SOURCES_MatMul += $(wildcard $(MATMUL_DIR)/shave/*.cpp)
SHAVE_CPP_SOURCES_MatMul += $(wildcard $(MATMUL_DIR)/share/*.cpp)
SHAVE_CPP_SOURCES_MatMul += $(DirAppRoot)/../../MvTensor/shave/entry.cpp
SHAVE_ASM_SOURCES_MatMul = $(wildcard $(MATMUL_DIR)/shave/$(MV_SOC_PLATFORM)/*.asm)

SHAVE_GENASMS_MatMul = $(patsubst %.cpp,$(DirAppObjBase)%.asmgen,$(SHAVE_CPP_SOURCES_MatMul))
SHAVE_OBJS_MatMul = $(patsubst %.asm,$(DirAppObjBase)%_shave.o,$(SHAVE_ASM_SOURCES_MatMul)) \
                   $(patsubst %.asmgen,%_shave.o,$(SHAVE_GENASMS_MatMul))

PROJECTCLEAN  += $(SHAVE_GENASMS_MatMul) $(SHAVE_OBJS_MatMul)
PROJECTINTERM += $(SHAVE_GENASMS_MatMul)

#--------------------------[ Shave applications section ]--------------------------#
MV_DEFAULT_SHAVE_GROUP_APPS := $(DirTestOutput)
MvMatMul_EntryPoint := Entry

# ------------------------------[ Build overrides ]--------------------------------#
# overrides that need to be set before including generic.mk
LEON_HEADERS += $(MATMUL_DIR)/../../../MvTensor/shared/mvTensorInternal.h
LEON_APP_C_SOURCES +=$(wildcard $(MATMUL_DIR)/../../../MvTensor/leon/*.c)
LEON_APP_CPP_SOURCES +=$(wildcard $(MATMUL_DIR)/leon/*.cpp)
LEON_APP_CPP_SOURCES +=$(wildcard $(MATMUL_DIR)/share/*.cpp)
LEON_APP_CPP_SOURCES += $(DirAppRoot)/../../MvTensor/shared/mvTensorInternal.cpp

# -------------------------------- [ Build Options ] ------------------------------ #
# App related build options

# Extra app related options
LEONOPTLEVEL= -O2
CCOPT 	+= -D'MVTENSOR=$(MatMulApp)'
CCOPT   += -DMYRIAD2 -DMATMUL
CPPOPT  += -Wno-reorder -Wno-unused-variable -Wno-error -DMATMUL
CCOPT   += -I$(MATMUL_DIR)/leon  -Wno-unused-variable -Wno-error
CCOPT   += -I$(MATMUL_DIR)/share

MVCCOPT += -DMYRIAD2  -Wno-unused-variable -Wno-error -ffast-math -fslp-vectorize
MVCCOPT += -I$(MATMUL_DIR)/share
MVCCOPT += -I$(MATMUL_DIR)/../../../MvTensor/shared

