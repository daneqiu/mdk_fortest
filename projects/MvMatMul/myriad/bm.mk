# ------------------------------[ App ]--------------------------------------------#
# Default application name is the same as the folder it is in.
# This can be overridden here if something different is required
APPNAME ?= mvMatMul
VERS_STR = 0.1.0-34

DirAppRoot = .
MATMUL_DIR= $(DirAppRoot)/../modules/matmul
MV_COMMON_BASE   ?= ../../../common

MV_SOC_REV = ma2450
MV_SOC_PLATFORM = myriad2
DEFAULT_HEAP_SIZE = 20480
LinkerScript ?= config/custom.ldscript

# ------------------------------[ Sample profiler ]--------------------------------#
SAMPLE_PERIOD_MICRO = 10
SAMPLE_DURATION_MS = 5000
SAMPLE_PROFILER_GRAPHS_ENABLED = 1
SAMPLE_PROFILER_OUTPUT?=$(DirAppOutput)/sampleProfiler/profiler.png

MV_MVDBG_GEN = 2

# Include the generic Makefile
include $(DirAppRoot)/sources.mk
include $(MV_COMMON_BASE)/generic.mk
include $(MV_COMMON_BASE)/components/profiler/profiler.mk
include $(DirAppRoot)/rules.mk

TEST_TAGS := "MA2150, MA2450"

