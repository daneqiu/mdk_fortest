
#-------------------------------[ Local shave applications build rules ]------------------#
#Describe the rule for building theApp application. Simple rule specifying
#which objects build up the said application. The application will be built into a library
ENTRYPOINTS = -e Entry -u SHVMatGEMM -u SHVMatGEMV -u sData --gc-sections
$(DirTestOutput).mvlib : $(SHAVE_OBJS_MatMul) $(PROJECT_SHAVE_LIBS)
	$(ECHO) $(LD) $(ENTRYPOINTS) $(MVLIBOPT) $(SHAVE_OBJS_MatMul) -o $@

# ------------------------------[ Extra Rules]-------------------------------------------#
matmul_profile: CPPOPT += -D MATMUL_PROFILE
matmul_profile: profile

