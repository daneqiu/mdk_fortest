#!/bin/bash

clear 
echo "running doxgen..."
doxygen ./doxygen/Doxyfile
cd ./Doc/latex/
echo "generating the pdf..."
make
cd ../../
echo "copying the pdf file to the base directory..."
cp ./Doc/latex/refman.pdf ./MvTensorDoc.pdf
echo "Done."
