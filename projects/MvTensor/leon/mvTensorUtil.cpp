#include "mvTensorUtil.h"

namespace mv
{
    namespace tensor
    {
        namespace util
        {
            u32 getMidStride(t_mvTensorGenData *data)
            {
                u32 stride = 0;

                if (data != nullptr)
                {
                    switch (data->storageOrder)
                    {
                        case orderZYX:
                            stride = data->dimYStride;
                            break;

                        case orderYZX:
                            stride = data->dimZStride;
                            break;

                        case orderYXZ:
                            stride = data->dimXStride;
                            break;

                        default:
                            stride = data->dimXStride;
                            break;
                    }
                }

                return stride;
            }

            u32 getBpp(t_MvTensorDataType type)
            {
                u32 bpp = 0;

                switch (type)
                {
                    case t_fp16:
                        bpp = 2;
                        break;

                    case t_u8f:
                        bpp = 1;
                        break;

                    case t_int:
                        bpp = 4;
                        break;

                    default:
                        bpp= 2;
                        break;
                }

                return bpp;
            }
        }
    }
}
