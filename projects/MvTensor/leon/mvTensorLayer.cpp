#include "mvTensorLayer.h"

namespace mv
{
    namespace tensor
    {
        Layer::Layer() :
            errorCode_(NoError)
        {}

        Layer::~Layer()
        {}

        ErrorCode Layer::run(const t_MvTensorParam &p, const t_MvTensorOp &op, const Optimization &opt, const Resources &res)
        {
            errorCode_ = NoError;
            run_(&p, op, opt, res);
            return errorCode_;
        }

        void Layer::setError(ErrorCode ec)
        {
            errorCode_ = ec;
        }
    }
}
