#ifndef MV_TENSOR_CPP_H_
#define MV_TENSOR_CPP_H_

#include "mvTensor.h"
#include "mvTensorResources.h"
#include "cache/mvTensorLeonCacheController.h"
#include "cache/mvTensorShaveCacheController.h"

namespace mv
{
    namespace tensor
    {
        class Processor
        {
        public:
            Processor(
                const t_MvTensorMyriadResources &myriadResources,
                const t_MvMatMulMyriadResources *matmulResources,
                const t_MvTensorDebugInfo *debugInfo);

            ~Processor();

            void run(const t_MvTensorParam &mvTensorParam);

        private:
            t_MvTensorMyriadResources myriadResources_;
            OutputStream debugStream_;
            dma::Config dmaConfig_;
            dma::User dmaUser_;
            cache::LeonController leonCache_;
            cache::ShaveController shaveCache_;
            Resources resources_;
            int prevShaves_;

            Processor(const Processor &);
            Processor &operator =(const Processor &);
        };
    }
}

#endif // MV_TENSOR_CPP_H_
