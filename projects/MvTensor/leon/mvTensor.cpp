///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     MvTensor API
///

#include "mvTensor.h"
#include "mvTensor_cpp.h"

#include <stdio.h>
#include <mvTensorDebug.h>


#include "mvModuleRegistry.h"
#include "mvTensorUtil.h"
#include "mvTensorOptimization.h"
#include "mvTensorDma.h"
#include "mvTensorLayer.h"
#include "mvTensorOutputStream.h"
#include "mvTensorTimer.h"
#include "mvTensorInternal.h"

#include "cache/mvTensorLeonCacheController.h"
#include "cache/mvTensorShaveCacheController.h"

#define DEBUG 0

#if DEBUG
#define MVT_DPRINTF(...) printf(__VA_ARGS__)
#else
#define MVT_DPRINTF(...)
#endif

extern DynamicContext_t MODULE_DATA(mvTensor);

namespace
{
    using namespace mv::tensor;

    const char * const OP_NAMES[] =
    {
        "kConv",                 ///< Convolution
        "kMaxPool",              ///< Max-Pooling
        "kAvgPool",              ///< Average-Pooling
        "kSoftMax",              ///< SoftMax
        "kFC",                   ///< Fully connected layer
        "kNone0",                ///< No post operation
        "kRelu",                 ///< rectified linear unit (Relu) rectifier
        "kReluX",                ///< rectified linear unit (Relu) rectifier - clamp(0,X)
        "kDepthConv",            ///< Depthwise Convolution
        "kBias",                 ///< Bias
        "kPRelu",                ///< PReLU
        "kLRN",                  ///< LRN
        "kSum",                  ///< Sum of input and weight
        "kProd",                 ///< Prod of input and weight
        "kMax",                  ///< Max between input and weight
        "kScale",                ///< Multiply each plane with a multiplier and add a bias
        "kRelayout",
        "kSquare",               ///< Square the input
        "kInnerLRN",             ///< Output = (1 + alpha * input) ^ -beta
        "kCopy",                 ///< Copy considering input and output strides
        "kSigmoid",              ///< Sigmoid
        "kTanh",                 ///< Tanh
        "kDeconvolution",        ///< Deconvolution a.k.a. Transposed convolution
        "kElu",                  ///< Exponential linear unit (ELU) rectifier
        "kReshape",              ///< Reshape
        "kToPlaneMajor",         ///< Convert from plane major to plane minor
        "kPower",                ///< Power layer
        "kCrop",                 ///< Crop layer
        "kTile",                 ///< Tile layer
        "kRegionYolo",           ///< Region layer
        "kReorgYolo",            ///< Reorg layer
        "kConvert_u8f16",        ///< Convert_u8f16 layer
        "kConvert_f32f16",       ///< Convert_f32f16 layer
        "kMyriadXHwConvolution", ///< Reserved for Hardware Ops
        "kMyriadXHwPooling",     ///< Reserved for Hardware Ops
        "kMyriadXHwFCL",         ///< Reserved for Hardware Ops
        "kMyriadXHwPostOps",     ///< Reserved for Hardware Ops
        "kConvertHwSw",          ///< Convert HW/SW layer
        "kPermute",              ///< Permute
        "kNormalize",            ///< Normalize
        "kPriorBox",             ///< PriorBox
        "kDetectionOutput",      ///< kDetectionOutput
        "kLeakyRelu",            ///< LeakyRelu
		"kSumReduce",			 ///< Sum of channels
		"kMaxWithConstant"		 ///< Maximum of input and constant
		"kRsqrt"				 ///< Inverse square root
		"kScaleWithScalar"		 ///< Multiply tensor by scalar
		"kDepthDeconvolution"    ///< depth-wise deconvolution
    };

    const char *getOpName(t_MvTensorOpType op) __attribute__((unused));
    const char *getOpName(t_MvTensorOpType op)
    {
        static char zero = 0;
        return (op < sizeof(OP_NAMES) / sizeof(OP_NAMES[0])) ? OP_NAMES[op] : &zero;
    }

    class Dummy : public Layer
    {
        virtual void run_(const t_MvTensorParam *, const t_MvTensorOp &, const Optimization &, const Resources &)
        {
        }
    };

    const ModuleHandle handles[] =
    {
        ModuleHandle().add<Dummy>(kNone0),
    };

    u32 checkForErrors(const t_MvTensorParam *mvTensorParam, OutputStream &debugStream)
    {
        u32 errors = 0;

        // Check if input pointer is null
        if(mvTensorParam->input->data == NULL)
        {
            debugStream << "Err: in pointer NULL\n";
            ++errors;
        }

        // Check if output pointer is null
        if(mvTensorParam->output->data == NULL)
        {
            debugStream << "Err: out pointer NULL\n";
            ++errors;
        }

        // Check weights pointer is needed to
        if(mvTensorParam->op->type == kConv ||
           mvTensorParam->op->type == kFC ||
           mvTensorParam->op->type == kDepthConv)
            if(mvTensorParam->weights->data == NULL)
            {
                debugStream << "Err: Weights pointer NULL\n";
                ++errors;
            }

        // Check shave first and last shave order
        if(mvTensorParam->myriadResources->firstShave > mvTensorParam->myriadResources->lastShave)
        {
            debugStream
                << "Err: firstShave (" << mvTensorParam->myriadResources->firstShave <<
                ") > lastShave (" << mvTensorParam->myriadResources->lastShave << ")\n";
            ++errors;
        }

        if(mvTensorParam->postOp != NULL)
            if((mvTensorParam->postOp->type != kRelu) &&
               (mvTensorParam->postOp->type != kReluX) &&
               (mvTensorParam->postOp->type != kLeakyRelu) &&
               (mvTensorParam->postOp->type != kBias) &&
               (mvTensorParam->postOp->type != kNone0) &&
               (mvTensorParam->postOp->type != kElu)
               )
            {
                debugStream << "Err: PostOp " << static_cast<int>(mvTensorParam->postOp->type) << " not allowed!\n";
                ++errors;
            }

        return errors;
    }
}

namespace mv
{
    namespace tensor
    {
        void layerCall(Layer &layer, const t_MvTensorParam *p, const t_MvTensorOp &op, const Optimization &opt, const Resources &res)
        {
            if (p != 0)
                layer.run(*p, op, opt, res);
        }

        Processor::Processor(
            const t_MvTensorMyriadResources &myriadResources,
            const t_MvMatMulMyriadResources * /* not yet used */,
            const t_MvTensorDebugInfo *debugInfo) :
            myriadResources_(myriadResources),
            debugStream_(debugInfo ? debugInfo->debugMsg : nullptr, MV_TENSOR_DBG_MSG_SIZE, OutputStream::Overwrite),
            dmaConfig_{ MV_TENSOR_DMA_PRIORITY, myriadResources.dmaLinkAgent },
            dmaUser_(dmaConfig_, myriadResources.dmaTransactions),
            leonCache_(),
            shaveCache_(myriadResources.dataPartitionNo),
            resources_{ dmaConfig_, dmaUser_, debugStream_, leonCache_, shaveCache_ },
            prevShaves_(0)
        {
            const u32 noShaves = myriadResources_.lastShave - myriadResources_.firstShave + 1;
            swcShaveUnit_t svuList[noShaves];
            for (u32 i = 0; i < noShaves; ++i)
                svuList[i] = myriadResources_.firstShave + i;

            saveShaveDynContextData(myriadResources_.firstShave, noShaves, &MODULE_DATA(mvTensor));

            openShaves(svuList, noShaves);
            powerShaves(prevShaves_, myriadResources_.firstShave, myriadResources_.lastShave, myriadResources_.dataPartitionNo, myriadResources_.instrPartitionNo);
            prevShaves_ = noShaves;

            for (u32 i = 0; i < noShaves; ++i)
                setupShaveDynApp(myriadResources_.firstShave + i);
        }

        Processor::~Processor()
        {
            powerShaves(prevShaves_, myriadResources_.firstShave, myriadResources_.firstShave - 1, myriadResources_.dataPartitionNo, myriadResources_.instrPartitionNo);

            const u32 noShaves = myriadResources_.lastShave - myriadResources_.firstShave + 1;
            swcShaveUnit_t svuList[noShaves];
            for (u32 i = 0; i < noShaves; ++i)
                svuList[i] = myriadResources_.firstShave + i;

            for (u32 i = 0; i < noShaves; ++i)
                cleanShaveDynApp(svuList[i]);

            closeShaves(svuList, noShaves);
        }

        void Processor::run(const t_MvTensorParam &mvTensorParam)
        {
            powerShaves(prevShaves_, mvTensorParam.myriadResources->firstShave, mvTensorParam.myriadResources->lastShave, myriadResources_.dataPartitionNo, myriadResources_.instrPartitionNo);
            prevShaves_ = mvTensorParam.myriadResources->lastShave - mvTensorParam.myriadResources->firstShave + 1;

            Timer timer(&mvTensorParam.debugInfo->ms);

            if (checkForErrors(&mvTensorParam, debugStream_))
                return;

            const Optimization optimization(mvTensorParam.op->optMask);

            ModuleRegistry &registry = ModuleRegistry::getInstance();

            const t_MvTensorOp * const ops[] =
            {
                mvTensorParam.preOp,
                mvTensorParam.op,
                mvTensorParam.postOp,
            };

            for (int i = 0, n = sizeof(ops) / sizeof(ops[0]); i < n; ++i)
            {
                const t_MvTensorOp * const op = ops[i];
                if (op != 0)
                {
                    if (ModuleEntryPoint mep = registry.lookup(op->type))
                    {
                        MVT_DPRINTF("%s\n", getOpName(op->type));

                        mep(&mvTensorParam, *op, optimization, resources_);
                    }
                }
            }
        }
    }
}

void* mvTensor(void *params)
{
    t_MvTensorParam *mvTensorParam = (t_MvTensorParam*) params;
    ErrorManager::Init(mvTensorParam->debugInfo->debugMsg);

    using namespace mv::tensor;
    Processor proc(*mvTensorParam->myriadResources, mvTensorParam->matmulResources, mvTensorParam->debugInfo);
    proc.run(*mvTensorParam);
    return nullptr;
}

char ** OptimizationNames()
{
    char ** names = (char **)calloc(sizeof(char*), opt_MAXIMUM_OPTIMIZATIONS);
    for (int i = 0; i != opt_MAXIMUM_OPTIMIZATIONS; i++){
        names[i] = (char *)calloc(sizeof(char), opt_MAXIMUM_NAME_SIZE);
    }
    //ensure these are inserted in the same order as above.

    strcpy(names[opt_conv_3_3_1_1_specific], "opt_conv_3_3_1_1_specific\x7E");
    strcpy(names[opt_conv_im2col], "opt_conv_im2col\x7E");
    strcpy(names[opt_conv_im2col_v2], "opt_conv_im2col_v2\x7E");
    strcpy(names[opt_conv_3_3_2_2_specific], "opt_conv_3_3_2_2_specific\x7E");
    strcpy(names[opt_conv_5_5_1_1_specific], "opt_conv_5_5_1_1_specific\x7E");
    strcpy(names[opt_conv_5_5_2_2_specific], "opt_conv_5_5_2_2_specific\x7E");
    strcpy(names[opt_conv_7_7_2_2_specific], "opt_conv_7_7_2_2_specific\x7E");
    strcpy(names[opt_maxpool_2_2_2_2_specific], "opt_maxpool_2_2_2_2_specific\x7E");
    strcpy(names[opt_maxpool_3_3_1_1_specific], "opt_maxpool_3_3_1_1_specific\x7E");
    strcpy(names[opt_conv_7_7_2_2_spatial], "opt_conv_7_7_2_2_spatial\x7E");
    strcpy(names[opt_conv_generic_spatial], "opt_conv_generic_spatial\x7E");

    strcpy(names[opt_deconv_3_3_1_1_same_specific], "opt_deconv_3_3_1_1_same_specific\x7E");
    strcpy(names[opt_deconv_5_5_1_1_same_specific], "opt_deconv_5_5_1_1_same_specific\x7E");
    strcpy(names[opt_deconv_M_N_1_1_same_spatial], "opt_deconv_M_N_1_1_same_spatial\x7E");
    strcpy(names[opt_deconv_M_N_1_1_same_generic], "opt_deconv_M_N_1_1_same_generic\x7E");
    strcpy(names[opt_deconv_general], "opt_deconv_general\x7E");

    strcpy(names[opt_power_accurate], "opt_power_accurate\x7E");

    return names;
}
