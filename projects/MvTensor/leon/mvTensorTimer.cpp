#include "mvTensorTimer.h"
#include <algorithm>

// Use the BM interface on MyriadX.
// OS interface is not available at the moment of writing.

#if defined(__RTEMS__) && !defined(MA2480)
#define MV_TIMER_OS
#endif

#ifdef MV_TIMER_OS
#include <OsDrvTimer.h>
#include <OsDrvCpr.h>
#else
#include <DrvTimer.h>
#include <DrvCpr.h>
#endif

namespace
{
    void start_timer(tyTimeStamp &timestamp)
    {
#ifdef MV_TIMER_OS
        OsDrvTimerStartTicksCount(&timestamp);
#else
        DrvTimerStartTicksCount(&timestamp);
#endif
    }

    u64 elapsed_ticks(const tyTimeStamp &start_timestamp)
    {
        u64 ticks = 0;

#ifdef MV_TIMER_OS
        OsDrvTimerGetElapsedTicks(const_cast<tyTimeStamp *>(&start_timestamp), &ticks);
#else
        DrvTimerGetElapsedTicks(const_cast<tyTimeStamp *>(&start_timestamp), &ticks);
#endif

        return ticks;
    }

    u32 ticks_per_us()
    {
        u32 ticksPerUs = 1;

#ifdef MV_TIMER_OS
        OsDrvCprGetSysClockPerUs(&ticksPerUs);
#else
        ticksPerUs = DrvCprGetSysClocksPerUs();
#endif

        return ticksPerUs;
    }
}

namespace mv
{
    namespace tensor
    {
        Timer::Timer(double *store) :
            store_(store),
            timestamp_(),
            ticksPerUs_(std::max<u32>(1, ::ticks_per_us()))
        {
            reset();
        }

        void Timer::reset()
        {
            ::start_timer(timestamp_);
        }

        double Timer::elapsed() const
        {
            return ::elapsed_ticks(timestamp_) / 1000.0 / ticksPerUs_;
        }

        Timer::~Timer()
        {
            if (store_ != nullptr)
                *store_ = elapsed();
        }
    }
}
