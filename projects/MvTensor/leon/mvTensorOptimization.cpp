#include "mvTensorOptimization.h"

namespace
{
    u32 getDefaultOptimization()
    {
        using mv::tensor::Optimization;

        return
#ifdef FATHOMRUN
            Optimization::OPT_IM2COL_V2 |
            Optimization::OPT_SPATIALCONV7X7_S2 |
            Optimization::OPT_DECONV_GENERAL |
            Optimization::OPT_CONV3X3_S1 |
            Optimization::OPT_CONV5X5_S1 |
            Optimization::OPT_SPATIALCONV |
            Optimization::OPT_MAXPOOL3X3 |
            Optimization::OPT_MAXPOOL2X2;
#else
           Optimization::OPT_IM2COL_V2 |
            Optimization::OPT_SPATIALCONV7X7_S2 |
            Optimization::OPT_DECONV_GENERAL |
            Optimization::OPT_CONV3X3_S1 |
            Optimization::OPT_CONV5X5_S1 |
            Optimization::OPT_SPATIALCONV |
            Optimization::OPT_MAXPOOL3X3 |
            Optimization::OPT_MAXPOOL2X2;
#endif
    }
}

namespace mv
{
    namespace tensor
    {
        Optimization::Optimization(u32 word) :
            word_(((word & MV_TENSOR_DEFAULT_OPT) == MV_TENSOR_DEFAULT_OPT) ? getDefaultOptimization() : word)
        {
        }
    }
}
