#include "mvModuleRegistry.h"
#include "mvTensorDebug.h"

namespace mv
{
	namespace tensor
	{
		ModuleRegistry &ModuleRegistry::getInstance()
		{
			static ModuleRegistry registry;
			return registry;
		}

		ModuleRegistry::ModuleRegistry() :
			entry_points_()
		{
		}

		bool ModuleRegistry::add(t_MvTensorOpType op, ModuleEntryPoint entry)
		{
		    mvTensorAssert(entry_points_[op] == 0, "ModuleRegistry: Multiple registration of the same Op type is not allowed.");
			return entry_points_[op] == 0 ? (entry_points_[op] = entry, true) : false;
		}

		ModuleEntryPoint ModuleRegistry::lookup(t_MvTensorOpType op) const
		{
			return entry_points_[op];
		}
	}
}
