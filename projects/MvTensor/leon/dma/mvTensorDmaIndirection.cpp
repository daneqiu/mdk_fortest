#include <mvTensorDma.h>

// Keep this in sync with the conditions from <mvTensorDmaIndirection.h> file

#if defined(MV_TENSOR_DMA_OS)
#include "mvTensorDmaOs.inc"
#elif defined(MV_TENSOR_DMA_BM)
#include "mvTensorDmaBm.inc"
#else
#include "dma/mvTensorDmaStub.inc"
#endif
