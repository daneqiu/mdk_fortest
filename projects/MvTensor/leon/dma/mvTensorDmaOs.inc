#include <mvTensorDma.h>
#include <mvTensorDebug.h>

#if defined(MV_TENSOR_DMA_2)
#pragma message "Using DMA implementation OS_2."
#include <OsDrvCmxDma.h>

namespace mv
{
    namespace tensor
    {
        namespace dma
        {
            namespace helper
            {
                User::User(const Config &config, Helper *helper) :
                    requester_id_(),
                    transaction_(helper)
                {
                    // TODO: Proper error handling
                    const int result = OsDrvCmxDmaInitRequester(config.priority, &requester_id_);
                    mvTensorAssert(result == OS_MYR_DRV_SUCCESS, "OsDrvCmxDmaInitRequester failed");
                }

                Task::Task(const User &user) :
                    user_(user)
                {
                }

                void Task::create(const void *src, void *dst, u32 byteLength, u32 srcWidth, u32 dstWidth, u32 srcStride, u32 dstStride)
                {
                    if (user_.transaction() != nullptr)
                    {
                        // TODO: Proper error handling
                        const int result  = OsDrvCmxDmaCreateTransactionFullOptions(
                            user_.requesterId(),
                            user_.transaction(),
                            reinterpret_cast<u8 *>(const_cast<void *>(src)),
                            reinterpret_cast<u8 *>(dst),
                            byteLength,
                            srcWidth,
                            dstWidth,
                            srcStride,
                            dstStride);
                        mvTensorAssert(result == OS_MYR_DRV_SUCCESS, "OsDrvCmxDmaCreateTransactionFullOptions failed");
                    }
                    else
                    {
                        // fallback scenario?
                        mvTensorAssert(false, "DMA Helper not available");
                    }
                }

                void Task::start()
                {
                    // TODO: Proper error handling
                    int innerResult = 0;
                    const int result = OsDrvCmxDmaStartListTask(user_.transaction(), &innerResult);
                    mvTensorAssert(result == OS_MYR_DRV_SUCCESS, "OsDrvCmxDmaStartListTask failed in Os");
                    mvTensorAssert(innerResult != CDMA_TASK_QUEUE_FULL, "OsDrvCmxDmaStartListTask failed in Drv");
                }

                void Task::wait()
                {
                    // TODO: Proper error handling
                    const int result = OsDrvCmxDmaWaitTask(user_.transaction());
                    mvTensorAssert(result == OS_MYR_DRV_SUCCESS, "OsDrvCmxDmaWaitTask failed");
                }
            }
        }
    }
}
#elif defined(MV_TENSOR_DMA_X)
#pragma message "Using DMA implementation OS_X."
#include <OsDrvCmxDma.h>

namespace mv
{
    namespace tensor
    {
        namespace dma
        {
            namespace helper
            {
                User::User(const Config &config, Helper *helper) :
                    transaction_(helper)
                {
                    OsDrvCmxDmaSetupStruct setup =
                    {
                        .irq_priority = static_cast<uint8_t>(config.priority),
                        .irq_enable = 1
                    };

                    const int result = OsDrvCmxDmaInitialize(&setup);
                    mvTensorAssert(result != OS_MYR_DRV_ERROR, "OsDrvCmxDmaInitialize failed");
                }

                Task::Task(const User &user) :
                    user_(user)
                {
                }

                void Task::create(const void *src, void *dst, u32 byteLength, u32 srcWidth, u32 dstWidth, u32 srcStride, u32 dstStride)
                {
                    if (user_.transaction() != nullptr)
                    {
                        const int result  = OsDrvCmxDmaCreateStrideTransaction(
                            &handle_,
                            user_.transaction(),
                            reinterpret_cast<u8 *>(const_cast<void *>(src)),
                            reinterpret_cast<u8 *>(dst),
                            srcWidth,
                            dstWidth,
                            srcStride,
                            dstStride,
                            byteLength);
                        mvTensorAssert(result == OS_MYR_DRV_SUCCESS, "OsDrvCmxDmaCreateStrideTransaction failed");
                    }
                    else
                    {
                        mvTensorAssert(false, "DMA Helper not available");
                    }
                }

                void Task::start()
                {
                    const int result = OsDrvCmxDmaStartTransfer(&handle_, 0);
                    mvTensorAssert(result == OS_MYR_DRV_SUCCESS, "OsDrvCmxDmaStartTransfer failed.");
                }

                void Task::wait()
                {
                    // This new driver really doesn't offer other async option than busy-wait???
                    OsDrvCmxDmaTransactionStatus status;
                    do
                    {
                        const int result = OsDrvCmxDmaGetTransactionStatus(&handle_, &status);
                        mvTensorAssert(result == OS_MYR_DRV_SUCCESS, "OsDrvCmxDmaGetTransactionStatus failed");
                    } while (status != OS_DRV_CMX_DMA_TRANSFER_FINISHED);
                }
            }
        }
    }
}
#else
#include <dma/mvTensorDmaStub.inc>
#endif

