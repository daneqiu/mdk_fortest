#ifndef MV_TENSOR_DMA_TYPES_H_
#define MV_TENSOR_DMA_TYPES_H_

#if defined(MV_TENSOR_DMA_OS)
    #if defined(MV_TENSOR_DMA_X)
        #include <OsDrvCmxDmaDefines.h>
        typedef OsDrvCmxDmaTransaction MvTensorDmaDescriptor;
    #elif defined(MV_TENSOR_DMA_2)
        #include <OsDrvCmxDma.h>
        typedef dmaTransactionList MvTensorDmaDescriptor;
    #else
        #warning "What kind of build is this?"
        struct MvTensorDmaDescriptor {};
    #endif
#elif defined(MV_TENSOR_DMA_BM)
    #if defined(MV_TENSOR_DMA_X)
        #error "Bare-metal builds not supported on MyriadX!"
    #elif defined(MV_TENSOR_DMA_2)
        #include <DrvCmxDma.h>
        typedef dmaTransactionList MvTensorDmaDescriptor;
    #else
        #warning "What kind of build is this?"
        struct MvTensorDmaDescriptor {};
    #endif
#else
    #warning "What kind of build is this?"
    struct MvTensorDmaDescriptor {};
#endif

#endif // MV_TENSOR_DMA_TYPES_H_
