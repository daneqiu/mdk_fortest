#include <mvTensorDma.h>
#include <mvTensorDebug.h>

#ifdef MV_TENSOR_DMA_2
#pragma message "Using DMA implementation BM_2."

namespace mv
{
    namespace tensor
    {
        namespace dma
        {
            namespace helper
            {
                User::User(const Config &config, Helper *helper) :
                    requester_id_(DrvCmxDmaInitRequesterOnAgent(config.priority, config.agent)),
                    transaction_(helper)
                {
                }

                Task::Task(const User &user) :
                    user_(user),
                    ref_(0)
                {
                }

                void Task::create(const void *src, void *dst, u32 byteLength, u32 srcWidth, u32 dstWidth, u32 srcStride, u32 dstStride)
                {
                    if (user_.transaction() != nullptr)
                    {
                        ref_  = DrvCmxDmaCreateTransactionFullOptions(
                            user_.requesterId(),
                            user_.transaction(),
                            reinterpret_cast<u8 *>(const_cast<void *>(src)),
                            reinterpret_cast<u8 *>(dst),
                            byteLength,
                            srcWidth,
                            dstWidth,
                            srcStride,
                            dstStride);
                    }
                    else
                    {
                        // fallback scenario?
                        mvTensorAssert(false, "DMA Helper not available");
                    }
                }

                void Task::start()
                {
                    // TODO: Proper error handling
                    const int result = DrvCmxDmaStartListTask(ref_);
                    mvTensorAssert(result != CDMA_TASK_QUEUE_FULL, "DrvCmxDmaStartListTask failed");
                }

                void Task::wait()
                {
                    const int result = DrvCmxDmaWaitTask(ref_);
                    mvTensorAssert(result == CMX_DMA_DRV_SUCCESS, "DrvCmxDmaWaitTask failed");
                }
            }
        }
    }
}
#else
#include <dma/mvTensorDmaStub.inc>
#endif

