#ifndef MV_TENSOR_DMA_OS_H_
#define MV_TENSOR_DMA_OS_H_

// Do not include this file directly.
// Use mvTensorDma.h

#include <mvTensorDma.h>

#if defined(MV_TENSOR_DMA_2)
#include <OsDrvCmxDma.h>

namespace mv
{
    namespace tensor
    {
        namespace dma
        {
            namespace platform
            {
                typedef dmaRequesterId RequesterId;
                typedef dmaTransactionList TransactionList;
            }

            typedef platform::TransactionList Helper;

            namespace helper
            {
                class User
                {
                public:
                    User(const Config &config, Helper *helper);

                private:
                    platform::RequesterId requester_id_;
                    platform::TransactionList *transaction_;

                    platform::RequesterId requesterId() const
                    {
                        return requester_id_;
                    }

                    platform::TransactionList *transaction() const
                    {
                        return transaction_;
                    }

                    friend class Task;

                    User(const User &);
                    User &operator =(const User &);
                };

                class Task
                {
                public:
                    explicit Task(const User &user);

                    void create(const void *src, void *dst, u32 byteLength, u32 srcWidth, u32 dstWidth, u32 srcStride, u32 dstStride);

                    void start();
                    void wait();
                    void append(Task &next);

                private:
                    const User &user_;

                    Task(const Task &);
                    Task &operator =(const Task &);
                };
            }
        }
    }
}
#elif defined(MV_TENSOR_DMA_X)
#include <OsDrvCmxDmaDefines.h>

namespace mv
{
    namespace tensor
    {
        namespace dma
        {
            namespace platform
            {
                typedef OsDrvCmxDmaTransaction TransactionList;
            }

            typedef platform::TransactionList Helper;

            namespace helper
            {
                class User
                {
                public:
                    User(const Config &config, Helper *helper);

                private:
                    platform::TransactionList *transaction_;

                    platform::TransactionList *transaction() const
                    {
                        return transaction_;
                    }

                    friend class Task;

                    User(const User &);
                    User &operator =(const User &);
                };

                class Task
                {
                public:
                    explicit Task(const User &user);

                    void create(const void *src, void *dst, u32 byteLength, u32 srcWidth, u32 dstWidth, u32 srcStride, u32 dstStride);

                    void start();
                    void wait();
                    void append(Task &next);

                private:
                    const User &user_;
                    OsDrvCmxDmaTransactionHnd handle_;

                    Task(const Task &);
                    Task &operator =(const Task &);
                };
            }
        }
    }
}
#else
#include <dma/mvTensorDmaStub.h>
#endif

#endif // MV_TENSOR_DMA_OS_H_
