#ifndef MV_TENSOR_DMA_INDIRECTION_H_
#define MV_TENSOR_DMA_INDIRECTION_H_

// Do not include this file directly.
// Use mvTensorDma.h

// Keep this in sync with the conditions from <mvTensorDmaIndirection.cpp> file

#if defined(MV_TENSOR_DMA_OS)
#include "mvTensorDmaOs.h"
#elif defined(MV_TENSOR_DMA_BM)
#include "mvTensorDmaBm.h"
#else
#include "dma/mvTensorDmaStub.h"
#endif

#endif // MV_TENSOR_DMA_INDIRECTION_H_
