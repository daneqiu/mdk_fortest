///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief     MvTensor Internal API
///
/// \details   Module registry
///

#ifndef MV_MODULE_REGISTRY_H_
#define MV_MODULE_REGISTRY_H_

#include "mvModuleHandle.h"
#include <array>

namespace mv
{
	namespace tensor
	{
		class ModuleRegistry
		{
		public:
			static ModuleRegistry &getInstance();

			bool add(t_MvTensorOpType op, ModuleEntryPoint entry);
			ModuleEntryPoint lookup(t_MvTensorOpType op) const;

		private:
			std::array<ModuleEntryPoint, OP_TYPE_COUNT_> entry_points_;

			ModuleRegistry();
		};
	}
}

#endif // MV_MODULE_REGISTRY_H_
