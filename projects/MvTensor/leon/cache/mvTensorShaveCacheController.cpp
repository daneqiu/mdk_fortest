#include "mvTensorShaveCacheController.h"

#ifdef __RTEMS__
    #ifdef MA2480
#include <OsDrvShaveL2c.h>

namespace
{
    void invalidate(int partition)
    {
        OsDrvShaveL2cFlushInvPart(partition, OS_DRV_SHAVE_L2C_INV);
    }

    void writeback(int partition)
    {
        OsDrvShaveL2cFlushInvPart(partition, OS_DRV_SHAVE_L2C_FLUSH);
    }
}
    #else
#include <OsDrvShaveL2Cache.h>

namespace
{
    void invalidate(int partition)
    {
        OsDrvShaveL2CachePartitionInvalidate(partition);
    }

    void writeback(int partition)
    {
        OsDrvShaveL2CachePartitionFlush(partition, DO_NOT_PERFORM_INVALIDATION);
    }
}
    #endif
#else
#include <DrvShaveL2Cache.h>

namespace
{
    void invalidate(int partition)
    {
        DrvShaveL2CachePartitionInvalidate(partition);
    }

    void writeback(int partition)
    {
        DrvShaveL2CachePartitionFlush(partition);
    }
}
#endif

// We assume Shave L1 Data cache is disabled (by default, according to the manual).

namespace mv
{
    namespace tensor
    {
        namespace cache
        {
            ShaveController::ShaveController(int partition) :
                Controller(),
                partition_(partition)
            {
            }

            void ShaveController::invalidate()
            {
                ::invalidate(partition_);
            }

            void ShaveController::invalidate(void *, void *)
            {
                invalidate();
            }

            void ShaveController::writeback()
            {
                ::writeback(partition_);
            }

            void ShaveController::writeback(void *, void *)
            {
                writeback();
            }

            int ShaveController::getPartitionId()
            {
                return partition_;
            }
        }
    }
}
