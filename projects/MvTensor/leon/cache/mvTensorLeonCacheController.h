#ifndef MV_TENSOR_LEON_CACHE_H_
#define MV_TENSOR_LEON_CACHE_H_

#include <mvTensorCacheController.h>

namespace mv
{
    namespace tensor
    {
        namespace cache
        {
            class LeonController : public Controller
            {
            public:
                virtual void invalidate();
                virtual void invalidate(void *from, void *to);
                
                virtual void writeback();
                virtual void writeback(void *from, void *to);
                
                virtual int getPartitionId();
            };
        }
    }
}

#endif // MV_TENSOR_LEON_CACHE_H_
