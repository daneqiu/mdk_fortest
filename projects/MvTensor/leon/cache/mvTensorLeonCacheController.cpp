#include "mvTensorLeonCacheController.h"

#ifdef __RTEMS__
#include <libcpu/cache_.h>

// This RTEMS interface is valid for both ma2x5x and ma2x8x

namespace
{
    void invalidate()
    {
        rtems_cache_invalidate_l2();
        rtems_cache_invalidate_entire_l1_data();
    }

    void invalidate(void *from, void *to)
    {
        const unsigned int bytes = reinterpret_cast<unsigned char *>(to) - reinterpret_cast<unsigned char *>(from);
        rtems_cache_invalidate_range_l2(from, bytes);
        rtems_cache_invalidate_l1_data_range(from, bytes);
    }

    void writeback()
    {
        // Leon L1 data cache is always write through
        rtems_cache_writeback_l2();
    }

    void writeback(void *from, void *to)
    {
        // Leon L1 data cache is always write through
        rtems_cache_writeback_range_l2(from, reinterpret_cast<unsigned char *>(to) - reinterpret_cast<unsigned char *>(from));
    }
}
#else
#include <DrvLeonL2C.h>
#include <swcLeonUtils.h>

namespace
{
    void invalidate()
    {
        DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_INVALIDATE, 0);
        swcLeonFlushDcache(); // No! This is not a flush! It's invalidate, even if it doesn't look like one.
    }

    void invalidate(void *from, void *to)
    {
        DrvLL2CFlushOpOnAddrRange(LL2C_OPERATION_INVALIDATE, 0, reinterpret_cast<u32>(from), reinterpret_cast<u32>(to));
        swcLeonFlushDcache(); // No! This is not a flush! It's invalidate, even if it doesn't look like one.
    }

    void writeback()
    {
        // Leon L1 data cache is always write through
        DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_WRITE_BACK, 0);
    }

    void writeback(void *from, void *to)
    {
        // Leon L1 data cache is always write through
        DrvLL2CFlushOpOnAddrRange(LL2C_OPERATION_WRITE_BACK, 0, reinterpret_cast<u32>(from), reinterpret_cast<u32>(to));
    }
}
#endif

namespace mv
{
    namespace tensor
    {
        namespace cache
        {
            void LeonController::invalidate()
            {
                ::invalidate();
            }

            void LeonController::invalidate(void *from, void *to)
            {
                ::invalidate(from, to);
            }

            void LeonController::writeback()
            {
                ::writeback();
            }

            void LeonController::writeback(void *from, void *to)
            {
                ::writeback(from, to);
            }

            int LeonController::getPartitionId()
            {
                return 0;
            }
        }
    }
}
