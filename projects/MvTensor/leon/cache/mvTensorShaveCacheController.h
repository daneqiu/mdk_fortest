#ifndef MV_TENSOR_SHAVE_CACHE_H_
#define MV_TENSOR_SHAVE_CACHE_H_

#include <mvTensorCacheController.h>

namespace mv
{
    namespace tensor
    {
        namespace cache
        {
            class ShaveController : public Controller
            {
            public:
                explicit ShaveController(int partition);

                virtual void invalidate();
                virtual void invalidate(void *from, void *to);

                virtual void writeback();
                virtual void writeback(void *from, void *to);

                virtual int getPartitionId();
            private:
                int partition_;
            };
        }
    }
}

#endif // MV_TENSOR_SHAVE_CACHE_H_
