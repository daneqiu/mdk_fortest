#ifndef MV_TENSOR_TIMER_H_
#define MV_TENSOR_TIMER_H_

#include <mv_types.h>

namespace mv
{
    namespace tensor
    {
        class Timer
        {
        public:
            explicit Timer(double *store = nullptr);
            ~Timer();

            void reset();
            double elapsed() const;

        private:
            double *store_;
            u64 timestamp_;
            u32 ticksPerUs_;
        };
    }
}

#endif // MV_TENSOR_TIMER_H_
