#ifndef MV_TENSOR_LAYER_H_
#define MV_TENSOR_LAYER_H_

#include "mvTensor.h"
#include "mvTensorOptimization.h"
#include "mvTensorResources.h"

namespace mv
{
    namespace tensor
    {
        enum ErrorCode
        {
            NoError,
        };

        class Layer
        {
        public:
            Layer();
            virtual ~Layer();

            ErrorCode run(const t_MvTensorParam &p, const t_MvTensorOp &op, const Optimization &opt, const Resources &res);

        protected:
            void setError(ErrorCode ec);

        private:
            ErrorCode errorCode_;

            virtual void run_(const t_MvTensorParam *p, const t_MvTensorOp &op, const Optimization &opt, const Resources &res) = 0;
        };
    }
}

#endif // MV_TENSOR_LAYER_H_
