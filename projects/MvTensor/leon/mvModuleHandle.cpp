#include "mvModuleHandle.h"
#include "mvModuleRegistry.h"

namespace mv
{
	namespace tensor
	{
        ModuleHandle::ModuleHandle()
        {
        }

        ModuleHandle::ModuleHandle(t_MvTensorOpType op, ModuleEntryPoint entry)
        {
            add(op, entry);
        }

        void ModuleHandle::add(t_MvTensorOpType op, ModuleEntryPoint entry)
        {
            ModuleRegistry &registry = ModuleRegistry::getInstance();
            registry.add(op, entry);
        }
	}
}
