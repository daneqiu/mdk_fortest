'''
    @file
    @copyright All code copyright Movidius Ltd 2012, all rights reserved.
               For License Warranty see: common/license.txt
'''

try:
    import sys
    import os
    import os.path
    import errno
    import logging

    sharedFldPath = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../mvdbg/build'))
    sys.path.append(sharedFldPath)

    import mvdbg

except ImportError:
    print(" In ImportError : {} ".format(os.path.basename(__file__)))
    quit()


logging.basicConfig(level=logging.INFO)


class Myriad2Dbg:

    project_path = str()
    dbg_srv_ip = '127.0.0.1'
    dbg_srv_port = '30001'
    jtag_freq = 3000

    def __init__(self, project_path, logger=None):
        self.project_path = project_path
        self.logger = logger or logging.getLogger('Myriad2Dbg')

    def init(self):
        env = os.environ
        MV_COMMON_BASE = str(env['MV_COMMON_BASE'])
        self.logger.info('init')
        mvdbg.init()
        cmd = 'mdbg::breset'
        self.run_command(cmd)
        cmd = 'mdbg::ddrinit'
        self.run_command(cmd)
        cmd = 'source ' + MV_COMMON_BASE + '/scripts/debug/mdkTcl/mdkTclStart.tcl'
        self.run_command(cmd)

    def close(self):
        self.logger.info('close')
        mvdbg.close()

    def load_application(self, elf_file_path):
        self.logger.debug(elf_file_path)
        if not os.path.isfile(elf_file_path):
            self.logger.error('cannot find ELF file')
            sys.exit(errno.ENOENT)

        cmd = 'mdbg::startupcore LOS'
        self.run_command(cmd)

        cmd = 'mdbg::loadfile ' + elf_file_path
        self.run_command(cmd)

    def run_command(self, cmd):
        self.logger.info('cmd = %s', cmd)
        mvdbg.run_command(cmd)

    def write_array(self, var_name, arr):
        mvdbg.write_array(var_name, arr)

    def read_array(self, var_name, arr):
        out = mvdbg.read_array(var_name, arr)
        return out

    def read_var(self, var_name, var):
        out = mvdbg.read_var(var_name, var)
        return out

    def write_var(self, symbol_name, npy_var):
        mvdbg.write_var(symbol_name, npy_var)
