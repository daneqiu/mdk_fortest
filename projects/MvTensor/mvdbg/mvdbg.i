///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

%module mvdbg

%include "typemaps.i"

#include "moviDebug2Dll.h"

%{
    typedef struct MoviDebugDllOption MoviDebugDllOption;

    #define SWIG_FILE_WITH_INIT
    #include "moviDebug2Dll.h"
%}

%include "numpy.i"

%init
%{
    import_array();
%}


%apply (int DIM1, unsigned char* IN_ARRAY1) {(int sz, unsigned char* arr)};
%apply (int DIM1, unsigned char* INPLACE_ARRAY1)  {(int sz, unsigned char* arr_out)};
%inline
%{
    const MoviDebugDllFunctions* mddllfun;
    MoviDebugDllCallbacks callbacks;
    MoviDebugDllInitOptions mddllopt;
    MoviDebugObjectPtr mdobjptr;

    void init(void)
    {
        #define STRINGIZE(x) #x
        #define STRINGIZE_VALUE_OF(x) STRINGIZE(x)

        const MoviDebugInitOption options[] = {
            {"cv", "ma2450"},
            {"tcp:host", "127.0.0.1"},
            {"tcp:port", "30001"},
            {"ddrinit:elf", STRINGIZE_VALUE_OF(DDR_INIT)}
        };

        memset((void*) &callbacks, 0, sizeof(callbacks));;
        memset((void*) &mddllopt, 0, sizeof(mddllopt));;
        memset((void*) &mdobjptr, 0, sizeof(mdobjptr));;

        callbacks.exit = 0;
        callbacks.print = 0;
        callbacks.error = 0;
        callbacks.log = 0;
        callbacks.alloc_mem = 0;
        callbacks.free_mem = 0;

        mddllopt.optionCount = 4;
        mddllopt.initOptions = options;
        mddllopt.callbacks = &callbacks;

        mdobjptr = moviDebug2_DllInit(&mddllopt, &mddllfun);
        printf("mvdbg::init() done\n");
    }

    void close(void)
    {
        moviDebug2_DllDone(mdobjptr);
    }

    void run_command(const char* cmd)
    {
        moviDebug2_DllCommand(mdobjptr, cmd, NULL);
    }

    unsigned int get_symbol_address(const char * symbol_name)
    {
        unsigned int ret;
        mddllfun->symAddr(mdobjptr, "LOS", symbol_name, &ret);
        return ret;
    }

    void write_array(int sz, unsigned char* arr, unsigned addr)
    {
        mddllfun->writeBlock(mdobjptr, addr, (const void *) arr, sz);
    }

    void read_array(int sz, unsigned char* arr_out, unsigned addr)
    {
        mddllfun->readBlock(mdobjptr, addr, (void *) arr_out, sz);
    }

    unsigned long long read_var(unsigned addr, int sz)
    {
        unsigned long long ret;
        mddllfun->readBlock(mdobjptr, addr, (void *) &ret, sz);
        return ret;
    }

    void write_var(unsigned addr, unsigned long var, int sz)
    {
        mddllfun->writeBlock(mdobjptr, addr, (const void *) &var, sz);
    }
%}

%pythoncode
%{
import numpy as np
def read_array(symbol_name, npy_arr):
    org_shape = npy_arr.shape
    org_dtype = npy_arr.dtype
    symbolAddr = _mvdbg.get_symbol_address(symbol_name)
    npy_arr = npy_arr.flatten()
    npy_arr.dtype = np.uint8
    _mvdbg.read_array(npy_arr, symbolAddr)
    npy_arr.dtype = org_dtype
    npy_arr = np.reshape(npy_arr, org_shape)
    return npy_arr

def write_array(symbol_name, npy_arr):
    symbolAddr = _mvdbg.get_symbol_address(symbol_name)
    npy_arr = npy_arr.flatten()
    npy_arr.dtype = np.uint8
    _mvdbg.write_array(npy_arr, symbolAddr)

def read_var(symbol_name, npy_var):
    symbolAddr = _mvdbg.get_symbol_address(symbol_name)
    npy_var = _mvdbg.read_var(symbolAddr, npy_var.itemsize)
    return npy_var

def write_var(symbol_name, npy_var):
    symbolAddr = _mvdbg.get_symbol_address(symbol_name)
    _mvdbg.write_var(symbolAddr, int(npy_var), npy_var.itemsize)
%}

