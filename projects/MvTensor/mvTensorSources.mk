ComponentList += kernelLib/MvCV

# Flags
HAVE_MVCV_LIB ?= no
MVTENSOR_FEATURES ?= full
MVTENSOR_CMX_BUFFER ?= 84

# CMX Buffer size
ifeq ($(MV_SOC_REV), ma2480)
ARCH_SUB_FOLDER = ma2x8x
else
ARCH_SUB_FOLDER = ma2x5x
endif

MVTENSOR_DEFINES += -D"MV_TENSOR_STAGE_SIZE=$(MVTENSOR_CMX_BUFFER)" -D"MV_TENSOR_FAST__OS_DRV_SVU"

#-------------------------------[ Paths ]---------------------------------------#
MVCV_KERNEL_LIB_DIR = $(MV_COMMON_BASE)/components/kernelLib/MvCV

#-----------------------------[ Sub-modules sources ]---------------------------#
ifeq ($(HAVE_MVCV_LIB), yes)
# Link in MvCV as a binary library. The target to build the binary is provided.
MVCV_PATH ?= $(MV_COMMON_BASE)/components/kernelLib/MvCV
MVCV_LIB ?= $(MVCV_PATH)/shared/unittest/output/mvcv.a
MVCV_LIB_TARGET = mvcv_lib

PROJECT_DEP_TARGETS += $(MVCV_LIB_TARGET)
PROJECT_DEP_LIBS += $(MVCV_LIB)

$(MVCV_LIB_TARGET):
	@cd $(MVCV_PATH)/kernels/`ls $(MVCV_PATH)/kernels | head -1`/unittest/dummy && \
	$(MAKE) ../../../../$(MVCV_LIB) -j

else
ifneq ($(SHAVE_COMPONENTS), yes)
# Include MvCV kernels into the local project.
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/include
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution3x3Fp16ToFp16/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution3x3s2hhhh/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution3x3s3hhhh/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution3x3s4hhhh/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution3x3s8hhhh/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution5x5Fp16ToFp16/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution5x5s2hhhh/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution5x5s3hhhh/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution5x5s4hhhh/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution5x5s8hhhh/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution7x7Fp16ToFp16/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution7x7s2hhhh/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution7x7s3hhhh/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution7x7s4hhhh/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution7x7s8hhhh/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution9x9Fp16ToFp16/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution9x9s2hhhh/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution9x9s3hhhh/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution9x9s4hhhh/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution9x9s8hhhh/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution11x11s1hhhh/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution11x11s2hhhh/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution11x11s3hhhh/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution11x11s4hhhh/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/maximumV3/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/maximumV2/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/maximumV9/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/averageV3/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/matrixVectorMultfp16x4/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/accumulateFp16/shave/include
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/scaleFp16/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/addV2Fp16/shave/include/
SH_COMPONENTS_HEADERS_PATHS += $(MVCV_KERNEL_LIB_DIR)/kernels/arithmeticSubFp16ToFp16/shave/include/

SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/maximumV3/shave/src/cpp/maximumV3.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/maximumV2/shave/src/cpp/maximumV2.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/maximumV9/shave/src/cpp/maximumV9.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/averageV3/shave/src/cpp/averageV3.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/matrixVectorMultfp16x4/shave/src/cpp/matrixVectorMultfp16x4.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/accumulateFp16/shave/src/cpp/accumulateFp16.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/addV2Fp16/shave/src/cpp/addV2Fp16.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/scaleFp16/shave/src/cpp/scaleFp16.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution3x3Fp16ToFp16/shave/src/cpp/convolution3x3Fp16ToFp16.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution3x3s2hhhh/shave/src/cpp/convolution3x3s2hhhh.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution3x3s3hhhh/shave/src/cpp/convolution3x3s3hhhh.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution3x3s4hhhh/shave/src/cpp/convolution3x3s4hhhh.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution3x3s8hhhh/shave/src/cpp/convolution3x3s8hhhh.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution5x5Fp16ToFp16/shave/src/cpp/convolution5x5Fp16ToFp16.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution5x5s2hhhh/shave/src/cpp/convolution5x5s2hhhh.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution5x5s3hhhh/shave/src/cpp/convolution5x5s3hhhh.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution5x5s4hhhh/shave/src/cpp/convolution5x5s4hhhh.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution5x5s8hhhh/shave/src/cpp/convolution5x5s8hhhh.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution7x7Fp16ToFp16/shave/src/cpp/convolution7x7Fp16ToFp16.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution7x7s2hhhh/shave/src/cpp/convolution7x7s2hhhh.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution7x7s3hhhh/shave/src/cpp/convolution7x7s3hhhh.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution7x7s4hhhh/shave/src/cpp/convolution7x7s4hhhh.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution7x7s8hhhh/shave/src/cpp/convolution7x7s8hhhh.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution9x9Fp16ToFp16/shave/src/cpp/convolution9x9Fp16ToFp16.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution9x9s2hhhh/shave/src/cpp/convolution9x9s2hhhh.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution9x9s3hhhh/shave/src/cpp/convolution9x9s3hhhh.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution9x9s4hhhh/shave/src/cpp/convolution9x9s4hhhh.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution9x9s8hhhh/shave/src/cpp/convolution9x9s8hhhh.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/arithmeticSubFp16ToFp16/shave/src/cpp/arithmeticSubFp16ToFp16.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution11x11s1hhhh/shave/src/cpp/convolution11x11s1hhhh.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution11x11s2hhhh/shave/src/cpp/convolution11x11s2hhhh.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution11x11s3hhhh/shave/src/cpp/convolution11x11s3hhhh.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution11x11s4hhhh/shave/src/cpp/convolution11x11s4hhhh.cpp
SHAVE_CPP_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution11x11s8hhhh/shave/src/cpp/convolution11x11s8hhhh.cpp

SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/addV2Fp16/arch/$(ARCH_SUB_FOLDER)/shave/src/addV2Fp16.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/maximumV3/arch/$(ARCH_SUB_FOLDER)/shave/src/maximumV3.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/maximumV2/arch/$(ARCH_SUB_FOLDER)/shave/src/maximumV2.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/maximumV9/arch/$(ARCH_SUB_FOLDER)/shave/src/maximumV9.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/averageV3/arch/$(ARCH_SUB_FOLDER)/shave/src/averageV3.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/scaleFp16/arch/$(ARCH_SUB_FOLDER)/shave/src/scaleFp16.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/arithmeticSubFp16ToFp16/arch/$(ARCH_SUB_FOLDER)/shave/src/arithmeticSubFp16ToFp16.asm

SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/matrixVectorMultfp16x4/arch/$(ARCH_SUB_FOLDER)/shave/src/matrixVectorMultfp16x4.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/accumulateFp16/arch/$(ARCH_SUB_FOLDER)/shave/src/accumulateFp16.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution3x3Fp16ToFp16/arch/$(ARCH_SUB_FOLDER)/shave/src/convolution3x3Fp16ToFp16.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution3x3s2hhhh/arch/$(ARCH_SUB_FOLDER)/shave/src/convolution3x3s2hhhh.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution3x3s3hhhh/arch/$(ARCH_SUB_FOLDER)/shave/src/convolution3x3s3hhhh.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution3x3s4hhhh/arch/$(ARCH_SUB_FOLDER)/shave/src/convolution3x3s4hhhh.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution3x3s8hhhh/arch/$(ARCH_SUB_FOLDER)/shave/src/convolution3x3s8hhhh.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution5x5Fp16ToFp16/arch/$(ARCH_SUB_FOLDER)/shave/src/convolution5x5Fp16ToFp16.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution5x5s2hhhh/arch/$(ARCH_SUB_FOLDER)/shave/src/convolution5x5s2hhhh.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution5x5s3hhhh/arch/$(ARCH_SUB_FOLDER)/shave/src/convolution5x5s3hhhh.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution5x5s4hhhh/arch/$(ARCH_SUB_FOLDER)/shave/src/convolution5x5s4hhhh.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution5x5s8hhhh/arch/$(ARCH_SUB_FOLDER)/shave/src/convolution5x5s8hhhh.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution7x7Fp16ToFp16/arch/$(ARCH_SUB_FOLDER)/shave/src/convolution7x7Fp16ToFp16.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution7x7s2hhhh/arch/$(ARCH_SUB_FOLDER)/shave/src/convolution7x7s2hhhh.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution7x7s3hhhh/arch/$(ARCH_SUB_FOLDER)/shave/src/convolution7x7s3hhhh.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution7x7s4hhhh/arch/$(ARCH_SUB_FOLDER)/shave/src/convolution7x7s4hhhh.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution7x7s8hhhh/arch/$(ARCH_SUB_FOLDER)/shave/src/convolution7x7s8hhhh.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution9x9Fp16ToFp16/arch/$(ARCH_SUB_FOLDER)/shave/src/convolution9x9Fp16ToFp16.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution9x9s2hhhh/arch/$(ARCH_SUB_FOLDER)/shave/src/convolution9x9s2hhhh.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution9x9s3hhhh/arch/$(ARCH_SUB_FOLDER)/shave/src/convolution9x9s3hhhh.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution9x9s4hhhh/arch/$(ARCH_SUB_FOLDER)/shave/src/convolution9x9s4hhhh.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution9x9s8hhhh/arch/$(ARCH_SUB_FOLDER)/shave/src/convolution9x9s8hhhh.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution11x11s1hhhh/arch/$(ARCH_SUB_FOLDER)/shave/src/convolution11x11s1hhhh.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution11x11s2hhhh/arch/$(ARCH_SUB_FOLDER)/shave/src/convolution11x11s2hhhh.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution11x11s3hhhh/arch/$(ARCH_SUB_FOLDER)/shave/src/convolution11x11s3hhhh.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution11x11s4hhhh/arch/$(ARCH_SUB_FOLDER)/shave/src/convolution11x11s4hhhh.asm
SHAVE_ASM_SOURCES_MvTensor += $(MVCV_KERNEL_LIB_DIR)/kernels/convolution11x11s8hhhh/arch/$(ARCH_SUB_FOLDER)/shave/src/convolution11x11s8hhhh.asm
SHAVE_ASM_SOURCES_MvTensor += $(MV_TENSOR_BASE)/shave/arch/$(ARCH_SUB_FOLDER)/memcpy_asm.asm
endif
endif

# DMA
ifeq ($(MV_SOC_OS), rtems)
ifeq ($(MV_SOC_REV), ma2480)
MVTENSOR_DMA ?= OS_X
else
MVTENSOR_DMA ?= OS_2
endif
else
ifeq ($(MV_SOC_REV), ma2480)
MVTENSOR_DMA ?= BM_X
else
MVTENSOR_DMA ?= BM_2
endif
endif

ifeq ($(MVTENSOR_DMA), OS_2)
MVTENSOR_DEFINES += -DMV_TENSOR_DMA_OS -DMV_TENSOR_DMA_2
endif
ifeq ($(MVTENSOR_DMA), OS_X)
MVTENSOR_DEFINES += -DMV_TENSOR_DMA_OS -DMV_TENSOR_DMA_X
endif
ifeq ($(MVTENSOR_DMA), BM_2)
MVTENSOR_DEFINES += -DMV_TENSOR_DMA_BM -DMV_TENSOR_DMA_2
endif
ifeq ($(MVTENSOR_DMA), BM_X)
MVTENSOR_DEFINES += -DMV_TENSOR_DMA_BM -DMV_TENSOR_DMA_X
endif
ifeq ($(MVTENSOR_DMA), 2)
MVTENSOR_DEFINES += -DMV_TENSOR_DMA_2
endif
ifeq ($(MVTENSOR_DMA), X)
MVTENSOR_DEFINES += -DMV_TENSOR_DMA_X
endif

# MvTensor Modules
MVTENSOR_MODULES_PATH := $(MV_TENSOR_BASE)/modules

ifeq ($(MVTENSOR_FEATURES), light)
MVTENSOR_MODULES += $(MVTENSOR_MODULES_PATH)/eltwise/module.mk
MVTENSOR_MODULES += $(MVTENSOR_MODULES_PATH)/imgproc/module.mk
MVTENSOR_MODULES += $(MVTENSOR_MODULES_PATH)/morphing/module.mk
MVTENSOR_MODULES += $(MVTENSOR_MODULES_PATH)/pooling/module.mk
MVTENSOR_MODULES += $(MVTENSOR_MODULES_PATH)/postops/module.mk
MVTENSOR_MODULES += $(MVTENSOR_MODULES_PATH)/relational/module.mk
MVTENSOR_MODULES += $(MVTENSOR_MODULES_PATH)/yolo/module.mk
else
MVTENSOR_MODULES += $(wildcard $(MVTENSOR_MODULES_PATH)/*/module.mk)
endif

include $(MVTENSOR_MODULES)

LEON_COMPONENT_HEADERS_PATHS += $(MVTENSOR_LEON_HEADER_PATHS)
LEON_COMPONENT_CPP_SOURCES_LOS += $(wildcard $(foreach DIR, $(MVTENSOR_LEON_COMPONENT_PATHS), $(DIR)/*.cpp))

SH_COMPONENTS_HEADERS_PATHS += $(MVTENSOR_SHAVE_HEADER_PATHS)
SHAVE_CPP_SOURCES_MvTensor += $(wildcard $(foreach DIR, $(MVTENSOR_SHAVE_COMPONENT_PATHS), $(DIR)/*.cpp))

# leon MvTensor components
LEON_COMPONENT_PATHS += $(MV_TENSOR_BASE)/leon/
LEON_COMPONENT_PATHS += $(MV_TENSOR_BASE)/leon/cache/
LEON_COMPONENT_PATHS += $(MV_TENSOR_BASE)/leon/dma/
LEON_COMPONENT_HEADERS_PATHS += $(MV_TENSOR_BASE)/shared/
LEON_HEADERS += $(wildcard $(MV_TENSOR_BASE)/leon/*.h)
LEON_HEADERS += $(wildcard $(MV_TENSOR_BASE)/shared/*.h)
LEON_COMPONENT_CPP_SOURCES_LOS += $(wildcard $(MV_TENSOR_BASE)/shared/*.cpp)

ifneq ($(MVTENSOR_NEED_MATMUL),)
MV_MAT_MUL_DIR = $(MV_TENSOR_BASE)/../MvMatMul

# leon MvMatMul components
LEON_HEADERS += $(wildcard $(MV_MAT_MUL_DIR)/modules/matmul/leon/*.h)
LEON_HEADERS += $(wildcard $(MV_MAT_MUL_DIR)/modules/matmul/share/*.h)
LEON_COMPONENT_HEADERS_PATHS += $(MV_MAT_MUL_DIR)/modules/matmul/leon
LEON_COMPONENT_HEADERS_PATHS += $(MV_MAT_MUL_DIR)/modules/matmul/share
LEON_COMPONENT_CPP_SOURCES_LOS += $(wildcard $(MV_MAT_MUL_DIR)/modules/matmul/leon/*.cpp)
LEON_COMPONENT_CPP_SOURCES_LOS += $(wildcard $(MV_MAT_MUL_DIR)/modules/matmul/share/*.cpp)

# shave headers for MvMatMul
SH_COMPONENTS_HEADERS_PATHS += $(MV_MAT_MUL_DIR)/modules/matmul/shave/
SH_COMPONENTS_HEADERS_PATHS += $(MV_MAT_MUL_DIR)/modules/matmul/share/

MVTENSOR_DEFINES += -DMVTENSOR_USE_MATMUL
endif

# shave headers for MvTensor
SH_COMPONENTS_HEADERS_PATHS += $(MV_TENSOR_BASE)/shared
SH_COMPONENTS_HEADERS_PATHS += $(MV_TENSOR_BASE)/shave

#-------------------------------[ Local shave applications sources ]--------------------#
#Choosing C sources the MvTensor application on shave
MvTensorAppName = mvTensor
#MvTensor = $(MV_TENSOR_BASE)/shave/$(MvTensorAppName)
DirTestOutput = ./output/$(MvTensorAppName)

SHAVE_CPP_SOURCES_MvTensor+= $(wildcard $(MV_TENSOR_BASE)/shave/*.cpp)
SHAVE_CPP_SOURCES_MvTensor+= $(wildcard $(MV_TENSOR_BASE)/shave/dma/*.cpp)
SHAVE_CPP_SOURCES_MvTensor+= $(wildcard $(MV_TENSOR_BASE)/shared/*.cpp)

#-----------------------------[ Other Shave applications sources ]-----------------------#
ifneq ($(MVTENSOR_NEED_MATMUL),)
#MvMatMul = $(MV_MAT_MUL_DIR)/modules/matmul/shave/mvMatMul
SHAVE_CPP_SOURCES_MvTensor += $(wildcard $(MV_MAT_MUL_DIR)/modules/matmul/shave/*.cpp)
SHAVE_CPP_SOURCES_MvTensor += $(wildcard $(MV_MAT_MUL_DIR)/modules/matmul/share/*.cpp)

SHAVE_ASM_SOURCES_MvTensor += $(wildcard $(MV_MAT_MUL_DIR)/modules/matmul/shave/myriad2/*.asm)

MATMUL_ENTRY_POINTS += -u SHVMatGEMM -u SHVMatGEMV
endif

SHAVE_GENASMS_MvTensor = $(patsubst %.cpp,$(DirAppObjBase)%.asmgen,$(SHAVE_CPP_SOURCES_MvTensor))
SHAVE_MvTensor_OBJS = $(patsubst %.asm,$(DirAppObjBase)%_shave.o,$(SHAVE_ASM_SOURCES_MvTensor)) \
                        $(patsubst $(DirAppObjBase)%.asmgen,$(DirAppObjBase)%_shave.o,$(SHAVE_GENASMS_MvTensor))

PROJECTCLEAN += $(SHAVE_GENASMS_MvTensor) $(SHAVE_MvTensor_OBJS)
PROJECTINTERM += $(SHAVE_GENASMS_MvTensor)

#--------------------------[ Shave applications section ]--------------------------#
#SHAVE_APP_LIBS += $(DirTestOutput).mvlib

MV_DEFAULT_SHAVE_GROUP_APPS := $(DirTestOutput)
#$(MvMatMul)
#$(MvTensor)
MvTensor_EntryPoint := Entry
#MvMatMul_EntryPoint := SHVMatGEMV

#CCOPT += -DMATMUL_PROFILE

CCOPT += -D'MVTENSOR=$(MvTensorAppName)'

LEON_ALIGN_OPTS += -falign-functions=64 -falign-loops=64
#-falign-labels=64 -falign-jumps=64
SHAVE_ALIGN_OPTS += -falign-functions=64

CCOPT += $(LEON_ALIGN_OPTS)
CCOPT += $(MVTENSOR_DEFINES)

CPPOPT += $(LEON_ALIGN_OPTS)
CPPOPT += $(MVTENSOR_DEFINES)
#MVCCOPT += $(SHAVE_ALIGN_OPTS)
#MVCCPPOPT += $(SHAVE_ALIGN_OPTS)

MVCCOPT += -fno-align-labels
MVCCOPT += -fno-align-loops
MVCCOPT += -fno-align-jumps
MVCCOPT += $(MVTENSOR_DEFINES)

MVCCPPOPT += -fno-align-labels
MVCCPPOPT += -fno-align-loops
MVCCPPOPT += -fno-align-jumps
MVCCPPOPT += $(MVTENSOR_DEFINES)
