# MvTensor

Is a library and framework for running tensor math operations efficiently on myriad family silicon.

Myriad2
=======

Build
-----

    $ cd <repo_root>/myriad
    $ make all
or

    $ make all MV_SOC_REV=ma2150

Test
====
Test is done comparing bit exactness between myriad and numpy. Python3 testing
script will access the myriad. The debug server must be up and running.
To access the debug server, a swig wrapper is generated in order to use the
debugger's dinamyc library from within python

SWIG
----
Using swig 3.0.7 and not the default swig available in Ubuntu package
repositories.
(note: commands with # as root, and with $ as any non-root user)

If needed, remove existing installation of swig

    # apt-get purge swig

Install tools and components

    # apt-get install automake autoconf libtool byacc yodl tidy htmldoc

To clone, build and install swig, do

    # apt-get build-dep swig
    $ cd swig
    $ git clone https://github.com/swig/swig.git
    $ git checkout tags/rel-3.0.7
    $ ./autogen.sh
    $ ./configure --prefix=/opt/swig
    $ make
    $ make docs
    # make install
    $ cd /usr/bin; ln -s /opt/swig/bin/swig

Python requirements
-------------------

Requirements are

    # apt-get install python3-dev python3-yaml python3-numpy python3-scipy python3-pip
    # pip3 install unittest-xml-reporting

Generating SWIG wraper
----------------------

Make sure to set the MV_TOOLS_DIR and MV_TOOLS_VERSION environment variables

To generate swig wrapers and make the debug server available in python run

    $ cd <repo_root>/mvdbg
    $ mkdir build
    $ cd build
    $ cmake ..
    $ make


Running python unittest
-----------------------

Build the myriad project and start the moviDebugServer, then
    
    $ cd <repo_root>/unittest
    $ python3 mvtensor_unittest.py


Documentation
===============
Requirements:
  # sudo apt-get install texlive-full doxygen
  
Running:
  ./createDoc.sh



