#include <stdio.h>

#ifndef __MOVICOMPILE__
#include <swcShaveLoader.h>
#include <OsDrvCpr.h>

#ifdef MA2480
#include <OsDrvShaveL2c.h>
#else
#include <OsDrvShaveL2Cache.h>
#endif

#endif // __MOVICOMPILE__

#include <mvTensorInternal.h>
#include "mvTensorDebug.h"

// --------- Debug -----------
char* ErrorManager::msg = NULL;

#ifndef __MOVICOMPILE__
static DynamicContext_t localModule[MVTENSOR_MAX_SHAVES];
static DynamicContextInstances_elm localModulePrivD[MVTENSOR_MAX_SHAVES];

void setupShaveDynApp(swcShaveUnit_t shave)
{
    DynamicContext_t *mData = &localModule[shave];

#ifdef MV_TENSOR_FAST__OS_DRV_SVU
    swcSetupDynShaveAppsComplete(mData, &shave, 1);
#else
#if defined(__RTEMS__)
    OsDrvSvuSetupDynShaveApps(mData, &shave, 1);
#else
    swcDynSetShaveWindows(mData, shave);
    swcSetupDynShaveApps(mData, &shave, 1);
#endif
#endif
}

void saveShaveDynContextData(u32 firstShave, u32 noShaves, DynamicContext_t *source)
{
    for (u32 i = 0; i < noShaves; i++)
    {
        memcpy(&localModule[i + firstShave], source, sizeof(DynamicContext_t));
        memcpy(&localModulePrivD[i + firstShave], source->instancesData, sizeof(localModulePrivD[i + firstShave]));
        localModule[i + firstShave].instancesData = &localModulePrivD[i + firstShave];
    }
}

void openShaves(swcShaveUnit_t* shaves, u32 shaveNo)
{
#if defined(__RTEMS__)
    int status = OsDrvSvuOpenShaves(shaves, shaveNo, OS_MYR_PROTECTION_SEM);
    ErrorManager::Assert(status == OS_MYR_DRV_SUCCESS, "openShaves function failed");
#endif
}

void closeShaves(swcShaveUnit_t* shaves, u32 shaveNo)
{
#if defined(__RTEMS__)
    int status = OsDrvSvuCloseShaves(shaves, shaveNo);
    ErrorManager::Assert(status == OS_MYR_DRV_SUCCESS, "closeShaves function failed");
#endif
}

void powerShaves(int prevShaves, int firstShave, int lastShave, int dataPartitionNo, int instrPartitionNo)
{
    int noShaves = lastShave - firstShave + 1;

#if defined(__RTEMS__)
    if(noShaves > prevShaves)
    {
        u64 shavesEnableMask = (1<<(firstShave + noShaves)) - (1<<(firstShave + prevShaves));
//        printf("powerShaves up using mask %x, first %d, last %d, prev %d\n", (unsigned int)shavesEnableMask, firstShave, lastShave, prevShaves);
        int status = RTEMS_SUCCESSFUL;
#ifdef MA2480
        for(int i = firstShave + prevShaves; i < firstShave + noShaves; i++)
        {
            status += OsDrvCprTurnOnIsland((HglCprPwrIsland)(HGL_CPR_PWR_ISLAND_SHAVE_0 + i));
        }
#else
        status = OsDrvCprTurnOnShaveMask(shavesEnableMask);
#endif
        ErrorManager::Assert(status == RTEMS_SUCCESSFUL, "enableShaves function failed");

        // Set back stuff that was lost when the power islands went off
        for (int i = firstShave + prevShaves; i < firstShave + noShaves; i++)
        {
            swcDynSetShaveWindows(&localModule[i], i);
#ifdef MA2480
            status = OsDrvShaveL2cAssignPart(i, dataPartitionNo, OS_DRV_SHAVE_L2C_NON_WIN_DATA_PART, (OsDrvShaveL2cWinId)0);
            ErrorManager::Assert(status == RTEMS_SUCCESSFUL, "assigne shave L2c data partition failed");
            status = OsDrvShaveL2cAssignPart(i, instrPartitionNo, OS_DRV_SHAVE_L2C_NON_WIN_INST_PART, (OsDrvShaveL2cWinId)0);
            ErrorManager::Assert(status == RTEMS_SUCCESSFUL, "assigne shave L2c instr partition failed");
#else
            OsDrvShaveL2CSetNonWindowedPartition((shaveId_t)i, dataPartitionNo, NON_WINDOWED_DATA_PARTITION);
            OsDrvShaveL2CSetNonWindowedPartition((shaveId_t)i, instrPartitionNo, NON_WINDOWED_INSTRUCTIONS_PARTITION);
            OsDrvShaveL2CSetWindowPartition((shaveId_t)i,SHAVEL2CACHEWIN_C,dataPartitionNo);
#endif
        }
    }
    else if(noShaves < prevShaves)
    {
        u64 shavesEnableMask = (1<<(firstShave + prevShaves)) - (1<<(firstShave + noShaves));
//        printf("powerShaves down using mask %x, first %d, last %d, prev %d\n", (unsigned int)shavesEnableMask, firstShave, lastShave, prevShaves);
        int status = RTEMS_SUCCESSFUL;

#ifdef MA2480
        for(int i = firstShave + prevShaves - 1; i >= firstShave + noShaves; i--)
        {
            status += OsDrvCprTurnOffIsland((HglCprPwrIsland)(HGL_CPR_PWR_ISLAND_SHAVE_0 + i), 1);
        }
#else
        status = OsDrvCprTurnOffShaveMask(shavesEnableMask);
#endif
        ErrorManager::Assert(status == RTEMS_SUCCESSFUL, "disableShaves function failed");
    }
#endif // (__RTEMS__)
}

void busyWaitShaveL1(int shvno)
{
    const static u32 shv_iL1_status_addr[MVTENSOR_MAX_SHAVES] =
    {
        SHAVE_0_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
        SHAVE_1_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
        SHAVE_2_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
        SHAVE_3_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
        SHAVE_4_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
        SHAVE_5_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
        SHAVE_6_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
        SHAVE_7_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
        SHAVE_8_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
        SHAVE_9_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
        SHAVE_10_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
        SHAVE_11_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
#ifdef MA2480
        SHAVE_12_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
        SHAVE_13_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
        SHAVE_14_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
        SHAVE_15_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
#endif
    };

    mvTensorAssert(shvno >= 0 && shvno < MVTENSOR_MAX_SHAVES);

    while (GET_REG_WORD_VAL(shv_iL1_status_addr[shvno]))
        ;
}

void startShave( u32 shvno, u32 entry, u32 param, u32 param2)
{
    mvTensorAssert(shvno < MVTENSOR_MAX_SHAVES);
    mvTensorAssert(isValidDdrAddress(entry));
    mvTensorAssert(isValidDdrAddress(param) || isValidCmxAddress(param));

    busyWaitShaveL1(shvno);

    s32 status;
#if defined(__RTEMS__)
#ifdef MV_TENSOR_FAST__OS_DRV_SVU
    status = OsDrvSvuRunShaveAlgoOnAssignedShaveCCNoSetup(&localModule[shvno], shvno, "iii", entry, param, param2);
#else
    status = OsDrvSvuRunShaveAlgoOnAssignedShaveCC(&localModule[shvno], shvno, "iii", entry, param, param2);
#endif
    assert((status == OS_MYR_DRV_SUCCESS) && "Attempting to start an unallocated Shave. Make sure the Shave \
        belongs to your application, is not busy and it has been initialized.");

#else // BM
#ifdef MV_TENSOR_FAST__OS_DRV_SVU
    status = swcRunShaveAlgoOnAssignedShaveCCNoSetup(&localModule[shvno], shvno, "iii", entry, param, param2);
    assert((status == (s32)shvno) && "Attempting to start an unallocated Shave. Make sure the Shave \
        belongs to your application, is not busy and it has been initialized.");
#else
    status = swcRunShaveAlgoOnAssignedShaveCC(&localModule[shvno], shvno, "iii", entry, param, param2);
    assert((status == MYR_DYN_INFR_SUCCESS) && "Attempting to start an unallocated Shave. Make sure the Shave \
        belongs to your application, is not busy and it has been initialized.");
#endif

#endif
}

void waitShaves(u32 firstShave, u32 lastShave)
{
    mvTensorAssert(firstShave <= lastShave);
    mvTensorAssert(lastShave < MVTENSOR_MAX_SHAVES);
#if defined(__RTEMS__)
    swcShaveUnit_t svuList[lastShave  - firstShave + 1];
    u32 runningShaves;
    u32 shv_start_count = 0;
    for(u32 i = firstShave; i <= lastShave; i++)
    {
        svuList[shv_start_count] = i;
        shv_start_count++;
    }
    OsDrvSvuDynWaitShaves(svuList, shv_start_count, OS_DRV_SVU_WAIT_FOREVER, &runningShaves);

    for(u32 i = firstShave; i <= lastShave; i++)
        SET_REG_WORD(DCU_OCR(i), OCR_STOP_GO | OCR_TRACE_ENABLE);
#else
    while (swcShavesRunning(firstShave, lastShave));
#endif
}

void cleanShaveDynApp(int shave)
{
#if defined(__RTEMS__)
    OsDrvSvuCleanupDynShaveApps(&localModule[shave]);
#else
    swcCleanupDynShaveApps(&localModule[shave]);
#endif
}

#endif // __MOVICOMPILE__
