#ifndef __MVTENSOR_INTERNAL_H__
#define __MVTENSOR_INTERNAL_H__

#ifdef NOTHREADS
#undef __RTEMS__
#endif

#include <stdio.h>
#include <stdlib.h>
#include <mv_types.h>
#include <swcWhoAmI.h>

#ifndef __MOVICOMPILE__
#include <assert.h>
#include <DrvSvu.h>
#include <swcShaveLoaderLocal.h>
#endif // ifndef __MOVICOMPILE__

#if defined(__RTEMS__)
#include <OsDrvSvu.h>
#endif

#include "mprintf.h"
#include "mvTensorConfig.h"
#include "mvTensorDebug.h"

// Dynamic loading macros -----------------------------------------------
#define PRIMITIVE_CAT(a, ...) a ## __VA_ARGS__
#define CAT(a, ...) PRIMITIVE_CAT(a, __VA_ARGS__)

#define MODULE_ENTRY_DECL(entry) extern u32 CAT(MVTENSOR, X ## _ ## entry[])
#define MODULE_ENTRY(entry) CAT(MVTENSOR, X ## _ ## entry)

#ifndef __MOVICOMPILE__

template <typename T>
T *useShaveParam(int shvno)
{
    static_assert(sizeof(T) <= MVTENSOR_PARAM_SIZE); // Parameter structure too large
    mvTensorAssert(0 <= shvno && shvno < MVTENSOR_MAX_SHAVES);

    MODULE_ENTRY_DECL(sParam);

    return reinterpret_cast<T *>(swcSolveShaveRelAddr(reinterpret_cast<u32>(&MODULE_ENTRY(sParam)), shvno));
}

static inline u8* getCMXSliceDataSection(int shvno)
{
	mvTensorAssert(shvno >= 0 && shvno < MVTENSOR_MAX_SHAVES);

	// This is actually a data buffer
	MODULE_ENTRY_DECL(sData);

	// Some Mv* modules rely on absolute addresses for initializations or precalculations.
	return (u8*)swcSolveShaveRelAddr((u32)&MODULE_ENTRY(sData), shvno);
}

#ifndef __MOVICOMPILE__
void saveShaveDynContextData(u32 firstShave, u32 noShaves, DynamicContext_t *source);
void openShaves(swcShaveUnit_t* shaves, u32 shaveNo);
void powerShaves(int prevShaves, int firstShave, int lastShave, int dataPartitionNo, int instrPartitionNo);
void setupShaveDynApp(swcShaveUnit_t shave);
void busyWaitShaveL1(int shvno);
void startShave( u32 shvno, u32 entry, u32 param, u32 param2 = 0);
void waitShaves(u32 firstShave, u32 lastShave);
void cleanShaveDynApp(int shave);
void closeShaves(swcShaveUnit_t* shaves, u32 shaveNo);
#endif // __MOVICOMPILE__

#endif

#endif
