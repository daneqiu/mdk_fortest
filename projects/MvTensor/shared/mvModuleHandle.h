///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief     MvTensor Public API
///
/// \details   Layer handle
///

#ifndef MV_MODULE_HANDLE_H_
#define MV_MODULE_HANDLE_H_

#include "mvTensor.h"
#include "mvTensorOptimization.h"
#include "mvTensorResources.h"
#include "mvTensorLayer.h"

namespace mv
{
	namespace tensor
	{
		typedef void (*ModuleEntryPoint)(const t_MvTensorParam *, const t_MvTensorOp &, const Optimization &, const Resources &);

		void layerCall(Layer &layer, const t_MvTensorParam *, const t_MvTensorOp &, const Optimization &, const Resources &);

        class ModuleHandle
        {
            template <typename T>
            static void caller(const t_MvTensorParam *p, const t_MvTensorOp &op, const Optimization &opt, const Resources &res)
            {
                T layer;
                layerCall(layer, p, op, opt, res);
            }

        public:
		    ModuleHandle();
            ModuleHandle(t_MvTensorOpType op, ModuleEntryPoint entry);

            template <typename T>
            ModuleHandle &add(t_MvTensorOpType op)
            {
                add(op, &caller<T>);
                return *this;
            }

        private:
            void add(t_MvTensorOpType op, ModuleEntryPoint entry);
        };
	}
}

#endif // MV_MODULE_HANDLE_H_
