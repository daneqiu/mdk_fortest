#ifndef MV_TENSOR_UTIL_H_
#define MV_TENSOR_UTIL_H_

#include <mvTensor.h>
#include <type_traits>

namespace mv
{
    namespace tensor
    {
        namespace util
        {
            template <unsigned int N>
            struct is_power_of_2
            {
                enum { value = ((N & 1) == 0) && is_power_of_2<N / 2>::value };
            };

            template <>
            struct is_power_of_2<1>
            {
                enum { value = 1 };
            };

            template <>
            struct is_power_of_2<0>
            {
                enum { value = 0 };
            };

            template <unsigned int N, typename T>
            typename std::enable_if<is_power_of_2<N>::value, T>::type align_up(T t)
            {
                return (t + N - 1) & ~(N - 1);
            }

            u32 getMidStride(t_mvTensorGenData *data);
            u32 getBpp(t_MvTensorDataType type);
        }
    }
}

#endif // MV_TENSOR_UTIL_H_
