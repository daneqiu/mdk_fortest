#ifndef MV_TENSOR_DMA_H_
#define MV_TENSOR_DMA_H_

#include "mvTensorDmaTypes.h"

namespace mv
{
    namespace tensor
    {
        namespace dma
        {
            struct Config
            {
                int priority;
                int agent;
            };
        }
    }
}

// based on the include path this goes either to leon/ or to shave/
#include <dma/mvTensorDmaIndirection.h>

namespace mv
{
    namespace tensor
    {
        namespace dma
        {
            using helper::User;

            class Task
            {
            public:
                explicit Task(const User &user) :
                    task_(user),
                    phase_(Virgin)
                {}

                ~Task()
                {
                    wait();
                }

                inline void create(const void *src, void *dst, u32 byteLength)
                {
                    create(src, dst, byteLength, byteLength, byteLength, byteLength, byteLength);
                }

                void create(const void *src, void *dst, u32 byteLength, u32 srcWidth, u32 dstWidth, u32 srcStride, u32 dstStride)
                {
                    wait();
                    task_.create(src, dst, byteLength, srcWidth, dstWidth, srcStride, dstStride);
                    phase_ = Created;
                }

                inline void start(const void *src, void *dst, u32 byteLength)
                {
                    create(src, dst, byteLength);
                    start();
                }

                void start(const void *src, void *dst, u32 byteLength, u32 srcWidth, u32 dstWidth, u32 srcStride, u32 dstStride)
                {
                    create(src, dst, byteLength, srcWidth, dstWidth, srcStride, dstStride);
                    start();
                }

                void start()
                {
                    if (phase_ == Created)
                    {
                        task_.start();
                        phase_ = Started;
                    }
                }

                void wait()
                {
                    if (phase_ == Started)
                    {
                        task_.wait();
                        phase_ = Created;
                    }
                }

                void append(Task &next)
                {
                    if ((phase_ == Created) && (next.phase_ == Created))
                    {
                        task_.append(next.task_);
                    }
                }

            private:
                enum Phase
                {
                    Virgin,
                    Created,
                    Started,
                };

                helper::Task task_;
                Phase phase_;

                Task(const Task &);
                Task &operator =(const Task &);
            };
        }
    }
}

#endif // MV_TENSOR_DMA_H_
