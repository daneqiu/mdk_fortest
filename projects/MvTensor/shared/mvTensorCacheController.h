#ifndef MV_TENSOR_CACHE_CONTROLLER_H_
#define MV_TENSOR_CACHE_CONTROLLER_H_

namespace mv
{
    namespace tensor
    {
        namespace cache
        {
            class Controller
            {
            public:
                virtual ~Controller() {}

                virtual void invalidate() = 0;
                virtual void invalidate(void *from, void *to) = 0;

                virtual void writeback() = 0;
                virtual void writeback(void *from, void *to) = 0;

                virtual int getPartitionId() = 0;
            };
        }
    }
}

#endif // MV_TENSOR_CACHE_CONTROLLER_H_
