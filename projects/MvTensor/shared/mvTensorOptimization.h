///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief     MvTensor API -- interface to MvTensor compute library
///
/// \details   Library API definition
///

#ifndef MV_TENSOR_OPTIMIZATION_H_
#define MV_TENSOR_OPTIMIZATION_H_

#include "mvTensor.h"

namespace mv
{
    namespace tensor
    {
        class Optimization
        {
        public:
            enum Mask
            {
                // NOTE: when enabled, the output buffer has to be
                // overallocated by 2 * pad bytes, where pad is
                // (mvTensorParam->input->dimX + 1) * outputStride
                // see the convoltuion 3x3s1 SAME implementation
                OPT_CONV3X3_S1 = 1 << opt_conv_3_3_1_1_specific,

                OPT_IM2COL = 1 << opt_conv_im2col,
                OPT_IM2COL_V2 = 1 << opt_conv_im2col_v2,
                OPT_CONV3X3_S2 = 1 << opt_conv_3_3_2_2_specific,

                // NOTE: when enabled, the output buffer has to be
                // overallocated by 2 * pad bytes, where pad is
                // (2 * mvTensorParam->input->dimX + 2) * outputStride
                OPT_CONV5X5_S1 = 1 << opt_conv_5_5_1_1_specific,

                OPT_CONV5X5_S2 = 1 << opt_conv_5_5_2_2_specific,
                OPT_CONV7X7_S2 = 1 << opt_conv_7_7_2_2_specific,
                OPT_MAXPOOL2X2 = 1 << opt_maxpool_2_2_2_2_specific,
                OPT_MAXPOOL3X3 = 1 << opt_maxpool_3_3_1_1_specific,
                OPT_SPATIALCONV7X7_S2 = 1 << opt_conv_7_7_2_2_spatial,
                OPT_SPATIALCONV = 1 << opt_conv_generic_spatial,

                // NOTE: when enabled, the output buffer has to be
                // overallocated by 2 * pad bytes, where pad is
                // (mvTensorParam->input->dimX + 1) * outputStride
                // see the convoltuion 3x3s1 SAME implementation
                OPT_DECONV_SAME_3X3_S1 = 1 << opt_deconv_3_3_1_1_same_specific,

                // NOTE: when enabled, the output buffer has to be
                // overallocated by 2 * pad bytes, where pad is
                // (2 * mvTensorParam->input->dimX + 2) * outputStride
                OPT_DECONV_SAME_5X5_S1 = 1 << opt_deconv_5_5_1_1_same_specific,

                OPT_SPATIAL_DECONV_SAME_S1 = 1 << opt_deconv_M_N_1_1_same_spatial,
                OPT_DECONV_GENERIC_SAME_S1 = 1 << opt_deconv_M_N_1_1_same_generic,
                OPT_POWER = 1 << opt_power_accurate,
                OPT_DECONV_GENERAL = 1 << opt_deconv_general,
            };

            Optimization(u32 word);

            inline bool isSet(Mask mask) const
            {
                return (word_ & mask) == mask;
            }

        private:
            u32 word_;
        };
    }
}

#endif // MV_TENSOR_OPTIMIZATION_H_
