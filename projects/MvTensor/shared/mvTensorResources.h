#ifndef MV_TENSOR_RESOURCES_H_
#define MV_TENSOR_RESOURCES_H_

#include "mvTensorCacheController.h"
#include "mvTensorDma.h"
#include "mvTensorOutputStream.h"

namespace mv
{
    namespace tensor
    {
        struct Resources
        {
            const dma::Config &dmaConfig;
            const dma::User &dmaUser;
            OutputStream &debug;
            cache::Controller &leonCache;
            cache::Controller &shaveCache;
        };
    }
}

#endif // MV_TENSOR_RESOURCES_H_
