#include <mvTensorDma.h>
#include <algorithm>

#pragma message "Using DMA implementation stub."

namespace mv
{
    namespace tensor
    {
        namespace dma
        {
            namespace helper
            {
                Task::Task(const User &) :
                    src_(nullptr),
                    dst_(nullptr),
                    byteLength_(0),
                    srcWidth_(0),
                    dstWidth_(0),
                    srcStride_(0),
                    dstStride_(0),
                    next_(nullptr)
                {
                }

                void Task::create(const void *src, void *dst, u32 byteLength, u32 srcWidth, u32 dstWidth, u32 srcStride, u32 dstStride)
                {
                    src_ = src;
                    dst_ = dst;
                    byteLength_ = byteLength;
                    srcWidth_ = srcWidth;
                    dstWidth_ = dstWidth;
                    srcStride_ = srcStride;
                    dstStride_ = dstStride;
                    next_ = nullptr;
                }

                void Task::start_() const
                {
                    // Do not alter the state of the member variables.
                    // This enables replaying the same transfer without
                    // recreating it each time, just like the real DMA.

                    const u8 *src = reinterpret_cast<const u8 *>(src_);
                    u8 *dst = reinterpret_cast<u8 *>(dst_);

                    for (u32 si = 0, di = 0, length = byteLength_; length > 0;)
                    {
                        const u32 chunk = std::min(std::min(srcWidth_ - si, dstWidth_ - di), length);
                        std::copy(src, src + chunk, dst);

                        si += chunk;
                        di += chunk;
                        src += chunk;
                        dst += chunk;
                        length -= chunk;

                        if (si == srcWidth_)
                        {
                            si = 0;
                            src += srcStride_ - srcWidth_;
                        }

                        if (di == dstWidth_)
                        {
                            di = 0;
                            dst += dstStride_ - dstWidth_;
                        }
                    }

                    if(next_ != nullptr)
                        next_->start();
                }

                void Task::append(Task &next)
                {
                    next_ = &next;
                }
            }
        }
    }
}
