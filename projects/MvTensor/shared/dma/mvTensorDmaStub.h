#ifndef MV_TENSOR_DMA_STUB_H_
#define MV_TENSOR_DMA_STUB_H_

// Do not include this file directly.
// Use mvTensorDma.h

#include <mvTensorDma.h>
#include <mv_types.h>

namespace mv
{
    namespace tensor
    {
        namespace dma
        {
            typedef void Helper;

            namespace helper
            {
                class User
                {
                public:
                    explicit User(const Config &, Helper * = 0) {}

                private:
                    friend class Task;

                    User(const User &);
                    User &operator =(const User &);
                };

                class Task
                {
                public:
                    explicit Task(const User &user);

                    void create(const void *src, void *dst, u32 byteLength, u32 srcWidth, u32 dstWidth, u32 srcStride, u32 dstStride);

                    void start()
                    {
                        // Just to constrain the implementation to be read-only
                        start_();
                    }

                    void wait() {}
                    void append(Task &task);

                private:
                    const void *src_;
                    void *dst_;
                    u32 byteLength_;
                    u32 srcWidth_;
                    u32 dstWidth_;
                    u32 srcStride_;
                    u32 dstStride_;
                    Task *next_;

                    Task(const Task &);
                    Task &operator =(const Task &);

                    void start_() const;
                };
            }
        }
    }
}

#endif // MV_TENSOR_DMA_STUB_H_
