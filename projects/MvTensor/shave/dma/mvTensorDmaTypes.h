#ifndef MV_TENSOR_DMA_TYPES_H_
#define MV_TENSOR_DMA_TYPES_H_

#if defined(MV_TENSOR_DMA_X)
    #include <ShDrvCmxDmaDefines.h>
    typedef ShDrvCmxDmaTransaction MvTensorDmaDescriptor;
#elif defined(MV_TENSOR_DMA_2)
    #include <swcCdmaCommonDefines.h>
    typedef dmaTransactionList MvTensorDmaDescriptor;
#else
    #warning "What kind of build is this?"
    struct MvTensorDmaDescriptor {};
#endif

#endif // MV_TENSOR_DMA_TYPES_H_
