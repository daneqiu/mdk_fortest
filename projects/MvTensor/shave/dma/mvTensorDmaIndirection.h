#ifndef MV_TENSOR_DMA_INDIRECTION_H_
#define MV_TENSOR_DMA_INDIRECTION_H_

// Do not include this file directly.
// Use mvTensorDma.h

#include <mvTensorDma.h>

#if defined(MV_TENSOR_DMA_2)
#include <swcCdmaCommonDefines.h>

namespace mv
{
    namespace tensor
    {
        namespace dma
        {
            namespace platform
            {
                typedef dmaRequesterId RequesterId;
                typedef dmaTransactionList TransactionList;
            }

            namespace helper
            {
                class User
                {
                public:
                    explicit User(Config &config);

                private:
                    platform::RequesterId requester_id_;

                    platform::RequesterId requesterId() const
                    {
                        return requester_id_;
                    }

                    friend class Task;

                    User(const User &);
                    User &operator =(const User &);
                };

                class Task
                {
                public:
                    explicit Task(const User &user);

                    void create(const void *src, void *dst, u32 byteLength, u32 srcWidth, u32 dstWidth, u32 srcStride, u32 dstStride);

                    void start();
                    void append(Task &next);
                    void wait();

                private:
                    const User &user_;
                    platform::TransactionList transaction_;
                    platform::TransactionList *ref_;

                    Task(const Task &);
                    Task &operator =(const Task &);

                };
            }
        }
    }
}
#elif defined(MV_TENSOR_DMA_X)
#include <ShDrvCmxDmaDefines.h>

namespace mv
{
    namespace tensor
    {
        namespace dma
        {
            namespace helper
            {
                class User
                {
                public:
                    explicit User(Config &config);

                private:
                    friend class Task;

                    User(const User &);
                    User &operator =(const User &);
                };

                class Task
                {
                public:
                    explicit Task(const User &user);

                    void create(const void *src, void *dst, u32 byteLength, u32 srcWidth, u32 dstWidth, u32 srcStride, u32 dstStride);

                    void start();
                    void append(Task &next);
                    void wait();

                private:
                    ShDrvCmxDmaTransactionHnd handle_;
                    ShDrvCmxDmaTransaction transaction_;

                    Task(const Task &);
                    Task &operator =(const Task &);
                };
            }
        }
    }
}
#else
#include <dma/mvTensorDmaStub.h>
#endif

#endif // MV_TENSOR_DMA_INDIRECTION_H_
