#include <mvTensorDma.h>
//#include <mvTensorDebug.h>

#if defined(MV_TENSOR_DMA_2)
#pragma message "Using DMA implementation 2."

#include <swcCdma.h>

namespace mv
{
    namespace tensor
    {
        namespace dma
        {
            namespace helper
            {
                User::User(Config &config) :
                    requester_id_(dmaInitRequesterWithAgent(config.priority, config.agent))
                {
                }

                Task::Task(const User &user) :
                    user_(user),
                    transaction_(),
                    ref_(0)
                {
                }

                void Task::create(const void *src, void *dst, u32 byteLength, u32 srcWidth, u32 dstWidth, u32 srcStride, u32 dstStride)
                {
                    ref_ = dmaCreateTransactionFullOptions(
                        user_.requesterId(),
                        &transaction_,
                        reinterpret_cast<u8 *>(const_cast<void *>(src)),
                        reinterpret_cast<u8 *>(dst),
                        byteLength,
                        srcWidth,
                        dstWidth,
                        srcStride,
                        dstStride);
                }

                void Task::start()
                {
                    const int status = dmaStartListTask(ref_);
//                    mvTensorAssert(status != 0, "dmaStartListTask failed.");
                }

                void Task::wait()
                {
                    dmaWaitTask(ref_);
                }

                void Task::append(Task &next)
                {
                    dmaLinkTasks(ref_, 1, next.ref_);
                }

            }
        }
    }
}
#elif defined(MV_TENSOR_DMA_X)
#pragma message "Using DMA implementation X."

#include <ShDrvCmxDma.h>
#include <svuCommonShave.h>

namespace mv
{
    namespace tensor
    {
        namespace dma
        {
            namespace helper
            {
                User::User(Config &)
                {
                    const int status = ShDrvCmxDmaInitialize(0);
//                    mvTensorAssert(status != MYR_DRV_ERROR, "ShDrvCmxDmaInitialize failed.");
                }

                Task::Task(const User &) :
                    handle_(),
                    transaction_()
                {
                }

                void Task::create(const void *src, void *dst, u32 byteLength, u32 srcWidth, u32 dstWidth, u32 srcStride, u32 dstStride)
                {
                    const int status = ShDrvCmxDmaCreateStrideTransaction(
                        &handle_,
                        &transaction_,
                        reinterpret_cast<u8 *>(const_cast<void *>(src)),
                        reinterpret_cast<u8 *>(dst),
                        srcWidth,
                        dstWidth,
                        srcStride,
                        dstStride,
                        byteLength);
//                    mvTensorAssert(status == MYR_DRV_SUCCESS, "ShDrvCmxDmaCreateStrideTransaction failed.");
                }

                void Task::start()
                {
                    scMutexRequest(MVTENSOR_DMA_MUTEX);
                    const int status = ShDrvCmxDmaStartTransfer(&handle_);
//                    mvTensorAssert(status == MYR_DRV_SUCCESS, "ShDrvCmxDmaStartTransfer failed.");
                }

                void Task::wait()
                {
                    const int status = ShDrvCmxDmaWaitTransaction(&handle_);
//                    mvTensorAssert(status == MYR_DRV_SUCCESS, "ShDrvCmxDmaWaitTransaction failed.");
                    scMutexRelease(MVTENSOR_DMA_MUTEX);
                }

                // needs to be tested on myriadX branch
                void Task::append(Task &next)
                {
                    ShDrvCmxDmaLinkTransactions(&handle_, 1, &(next.handle_));
                }

            }
        }
    }
}
#else
#include <dma/mvTensorDmaStub.inc>
#endif
