///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///


#include <mvTensorInternal.h>

#include "mvReorg.h"
#include "../priv/mvReorgParam.h"

#define BUFFER_CMX_SIZE (MVTENSOR_HEAP_DATA_SIZE)
#define MIN(a, b) (((a) < (b))?(a):(b))

using namespace mv::tensor;

void reorg(fp16                      *input,
           fp16                      *output,
           s32                       in_width,
           s32                       in_height,
           s32                       in_channels,
           s32                       out_width,
           s32                       out_height,
           s32                       out_channels,
           u32                       *reorg_params,
           t_MvTensorMyriadResources *myriadResources,
           const mv::tensor::Resources &res)

{
    mvTensorAssert(in_channels * in_height * in_width == out_channels * out_height * out_width, "the sizes of input and output layers have to be equal!");

    if (in_channels * in_height * in_width == 0)
        return;

    s32 c = in_channels;
    s32 h = in_height;
    s32 w = in_width;

    int tensor_size = c * w * h * sizeof(fp16);
    int tensor_size0 = MIN(tensor_size, BUFFER_CMX_SIZE);
    int tensor_size1 = tensor_size - tensor_size0;

    /* SHAVEs initialization */
    s32 first_shave = myriadResources->firstShave;
    s32 last_shave = myriadResources->lastShave;
    int nshaves = last_shave - first_shave + 1;

    // the available amount of memory for one buffer
    int available_cmx_memory = BUFFER_CMX_SIZE + (BUFFER_CMX_SIZE / 2);

    // if there is enough memory space for 2 double buffers
    bool use_cmx = (tensor_size <= available_cmx_memory) && (nshaves >= 3);

    if (!use_cmx)
    {
        // for bigger SHAVEs number, there are a lot of cache misses and performance gets worse
        nshaves = MIN(nshaves, 2);
        last_shave = first_shave + (nshaves-1);
    }

    fp16 *buf_out0 = 0, *buf_out1 = 0;
    fp16 *buf_in0 = 0, *buf_in1 = 0;
    if (use_cmx)
    {
        // Buffers for input/output data
        buf_out0 = (fp16*)getCMXSliceDataSection(first_shave);
        buf_out1 = (fp16*)getCMXSliceDataSection(first_shave + 1);
        buf_in0  = (fp16*)getCMXSliceDataSection(first_shave + 2);
        buf_in1  = (fp16*)((u8*)buf_out1 + tensor_size1);

        // Copy from DDR to CMX
        dma::Task(res.dmaUser).start((uint8_t*)(input), (uint8_t*)buf_in0, tensor_size0);
        if (tensor_size1)
            dma::Task(res.dmaUser).start((uint8_t*)(input) + tensor_size0, (uint8_t*)buf_in1, tensor_size1);
    }

    int inter_step = (in_channels + (nshaves-1)) / nshaves;
    int n_shaves_to_run = (in_channels + (inter_step-1)) / inter_step;
    last_shave = first_shave + n_shaves_to_run-1;

    int curr_channel = 0;
    for(s32 shave_idx = first_shave; shave_idx <= last_shave; shave_idx++)
    {
        t_MvReorgParams *reorgParam = useShaveParam<t_MvReorgParams>(shave_idx);

        reorgParam->input  = input;
        reorgParam->output = output;

        reorgParam->in_width    = in_width;
        reorgParam->in_height   = in_height;
        reorgParam->in_channels = in_channels;

        reorgParam->reorg_params = reorg_params;

        reorgParam->start_c = curr_channel;
        reorgParam->end_c = MIN(curr_channel + (inter_step-1), in_channels-1);
        curr_channel += inter_step;

        // set CMX buffers
        reorgParam->use_cmx = use_cmx;
        reorgParam->buffer_size0 = tensor_size0 / 2;
        reorgParam->out_buffer0 = (u8*)buf_out0;
        reorgParam->out_buffer1 = (u8*)buf_out1;
        reorgParam->inp_buffer0 = (u8*)buf_in0;
        reorgParam->inp_buffer1 = (u8*)buf_in1;

        MODULE_ENTRY_DECL(mvReorg);

        startShave(shave_idx, (u32)&MODULE_ENTRY(mvReorg), (u32)reorgParam);
    }
    waitShaves(first_shave, last_shave);

    if (use_cmx)
    {
        // Copy from CMX to DDR
        dma::Task(res.dmaUser).start(buf_out0, (uint8_t*)(output), tensor_size0);
        if (tensor_size1)
            dma::Task(res.dmaUser).start(buf_out1, (uint8_t*)(output) + tensor_size0, tensor_size1);
    }

    res.shaveCache.writeback();
    res.shaveCache.invalidate();
}

