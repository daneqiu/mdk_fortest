///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief Leon wrappers for CNN post operations.
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <mv_types.h>

#include "mvRegion.h"
#include <mvTensorInternal.h>
#include "../priv/mvRegionParam.h"

/* TODO
 * Add support to run in multiple shaves
 */

void region(fp16                      *input,
             fp16                      *output,
             s32                       in_width,
             s32                       in_height,
             s32                       in_channels,
             s32                       out_width,
             s32                       out_height,
             s32                       out_channels,
             u32                       *region_params,
             t_MvTensorMyriadResources *myriadResources,
             const mv::tensor::Resources &res)
{
    s32 first_shave = myriadResources->firstShave;
    s32 last_shave = first_shave;
    u32 dmaLinkAgent = myriadResources->dmaLinkAgent;

    mvTensorAssert(BUFFER_SIZE_REGION >= in_channels * in_height * in_width, "Buffer size is less than the size of the layer ");

    for (s32 shave_idx = first_shave; shave_idx <= last_shave; ++shave_idx)
    {
        t_MvRegionParams *regionParam = useShaveParam<t_MvRegionParams>(shave_idx);
        regionParam->input         = input;
        regionParam->output        = output;

        regionParam->in_width      = in_width;
        regionParam->in_height     = in_height;
        regionParam->in_channels   = in_channels;

        regionParam->out_width     = out_width;
        regionParam->out_height    = out_height;
        regionParam->out_channels  = out_channels;

        regionParam->start_height  = 0;
        regionParam->end_height    = in_height;
        regionParam->region_params = region_params;
        regionParam->dmaLinkAgent  = dmaLinkAgent;

        regionParam->cmxslice = getCMXSliceDataSection(shave_idx);

        MODULE_ENTRY_DECL(mvRegion);
        startShave(shave_idx, ((u32)&MODULE_ENTRY(mvRegion)), (u32)regionParam);
    }
    waitShaves(first_shave, last_shave);

    res.shaveCache.writeback();
}
