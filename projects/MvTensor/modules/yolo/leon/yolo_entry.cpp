#include "mvRegion.h"
#include "mvReorg.h"

#include <mvModuleHandle.h>

namespace
{
    using namespace mv::tensor;

    class Region : public Layer
    {
        virtual void run_(const t_MvTensorParam *mvTensorParam, const t_MvTensorOp &op, const Optimization &, const Resources &res)
        {
            region((fp16*)mvTensorParam->input->data,
                    (fp16*)mvTensorParam->output->data,
                     mvTensorParam->input->dimX,
                     mvTensorParam->input->dimY,
                     mvTensorParam->input->dimZ,
                     mvTensorParam->output->dimX,
                     mvTensorParam->output->dimY,
                     mvTensorParam->output->dimZ,
                     (u32*)op.params,
                     mvTensorParam->myriadResources,
                     res);
        }
    };

    class Reorg : public Layer
    {
        virtual void run_(const t_MvTensorParam *mvTensorParam, const t_MvTensorOp &op, const Optimization &, const Resources &res)
        {
            reorg((fp16*)mvTensorParam->input->data,
                    (fp16*)mvTensorParam->output->data,
                     mvTensorParam->input->dimX,
                     mvTensorParam->input->dimY,
                     mvTensorParam->input->dimZ,
                     mvTensorParam->output->dimX,
                     mvTensorParam->output->dimY,
                     mvTensorParam->output->dimZ,
                     (u32*)op.params,
                     mvTensorParam->myriadResources,
                     res);
        }
    };

    const ModuleHandle handles[] =
    {
        ModuleHandle().add<Region>(kRegionYolo),
        ModuleHandle().add<Reorg>(kReorgYolo),
    };
}
