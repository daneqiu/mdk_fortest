///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef REGION_LEON_H_
#define REGION_LEON_H_

#include <mv_types.h>
#include <mvTensor.h>

#include <mvTensorResources.h>

void region(fp16                      *input,
             fp16                      *output,
             s32                       in_width,
             s32                       in_height,
             s32                       in_channels,
             s32                       out_width,
             s32                       out_height,
             s32                       out_channels,
             u32                       *tile_params,
             t_MvTensorMyriadResources *myriadResources,
             const mv::tensor::Resources &res);

#endif // REGIONLEON_H_
