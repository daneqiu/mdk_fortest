///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef REORG_LEON_H_
#define REORG_LEON_H_

#include <mv_types.h>
#include <mvTensor.h>

#include <mvTensorResources.h>

void reorg(fp16                      *input,
             fp16                      *output,
             s32                       in_width,
             s32                       in_height,
             s32                       in_channels,
             s32                       out_width,
             s32                       out_height,
             s32                       out_channels,
             u32                       *reorg_params,
             t_MvTensorMyriadResources *myriadResources,
             const mv::tensor::Resources &res);

#endif // REORGLEON_H_
