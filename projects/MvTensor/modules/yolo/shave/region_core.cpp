/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///


// 1: Includes
// ----------------------------------------------------------------------------
#include <mv_types.h>
#include <cstring>
#include "math.h"
#include "stdio.h"
#include "stdlib.h"
#include <mvTensorDma.h>

#include "region_core.h"

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
#define INPUT_BPP 2
#define log_2_e ((fp16)1.442695040888963) // log2(exp(1.0))
#define nlog_2_e ((fp16)(-1.442695040888963))

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------

static int entry_index(int w, int h, int outputs, int coords, int classes, int batch, int location, int entry)
{
    int n = location / (w * h);
    int loc = location % (w * h);
    return batch * outputs + n * w * h * (coords + classes + 1) + entry * w * h + loc;
}

static inline fp16 logistic_activate(fp16 x)
{
    fp16 res = (fp16)(1)/((fp16)(1) + __builtin_shave_sau_exp2_f16_l_r(x*nlog_2_e));
    return res;
}

static void activate_2array(fp16 *x, fp16 *x1, const int n)
{
    fp16 rx    = x[0];
    fp16 rx1   = x1[0];
    fp16 rx1_n = x1[n];
    int i = 0;
    for(; i < n-1; ++i){
        fp16 rx_    = x[i+1];
        fp16 rx1_   = x1[i+1];
        fp16 rx1_n_ = x1[n+i+1];
        x[i]    = logistic_activate(rx);
        x1[i]   = logistic_activate(rx1);
        x1[i+n] = logistic_activate(rx1_n);
        rx    = rx_;
        rx1   = rx1_;
        rx1_n = rx1_n_;
    }
    {
        x[i]    = logistic_activate(rx);
        x1[i]   = logistic_activate(rx1);
        x1[i+n] = logistic_activate(rx1_n);
    }
}

static void softmax(fp16 *in, int n, fp16 temp, int stride, fp16 *out)
{
    int i;
    fp16 * __restrict__ rin  = in;
    fp16 * __restrict__ rout = out;
    fp16 sum = 0;
    fp16 largest = -10000.0;
    fp16 rx = rin[0*stride];
#pragma clang loop unroll_count(2)
    for(i = 0; i < n - 1; ++i){
        fp16 rx_ = rin[(i+1)*stride];
        largest = (fp16)__builtin_shave_cmu_max_f16_rr_half ( (half)largest,  (half)rx );
        rx = rx_;
    }
    {
        largest = (fp16)__builtin_shave_cmu_max_f16_rr_half ( (half)largest,  (half)rx );
    }
    temp = log_2_e/temp;
    rx = rin[0*stride];
#pragma clang loop unroll_count(2)
    for(i = 0; i < n-1; ++i){
        fp16 rx_ = rin[(i+1)*stride];
        fp16 e = __builtin_shave_sau_exp2_f16_l_r(temp * (rx - largest));

        sum += e;
        rout[i*stride] = e;
        rx = rx_;
    }
    {
        fp16 e = __builtin_shave_sau_exp2_f16_l_r(temp * (rx - largest));
        sum += e;
        rout[i*stride] = e;
    }
    rx = rout[0*stride];
    for(i = 0; i < n-1; ++i){
        fp16 rx_ = rout[(i+1)*stride];
        rout[i*stride] = rx / sum;
        rx = rx_;
    }
    {
        rout[i*stride] = rx / sum;
    }
}

static void softmax_cpu(fp16 *input, int n, int batch, int batch_offset, int groups, int group_offset, int stride, fp16 temp, fp16 *output)
{
    int g, b;
    for(b = 0; b < batch; ++b){
        for(g = 0; g < groups; ++g){
            softmax(input + b*batch_offset + g*group_offset, n, temp, stride, output + b*batch_offset + g*group_offset);
        }
    }
}

void mvRegion(t_MvRegionParams *params)
{
    using namespace mv::tensor;
    dma::Config dmaConfig = { 1, params->dmaLinkAgent };
    dma::User dmaUser(dmaConfig);

    fp16                 *p_input, *p_output;
    s32                  in_height, in_width, in_channels;
    s32                  out_height, out_width, out_channels;
    u32                  *region_params;
    u32 classes ;
    s32 coords;
    s32 num;

    half4 *  p_in;
    fp16  *  p_out;
    fp16 * clear_input;

    /* Input dimensions */
    in_height   = params->in_height;
    in_width    = params->in_width;
    in_channels = params->in_channels;
    if(in_height * in_width * in_channels > 0)
    {
        s32 k,j;

        region_params = params->region_params;
        classes= region_params[0];
        coords = region_params[1];
        num = region_params[2];

        out_height   = params->out_height;
        out_width    = params->out_width;
        out_channels = params->out_channels;

        int in_bytes  = in_height * in_width * in_channels * INPUT_BPP;
        int out_bytes = out_height * out_width * out_channels * INPUT_BPP;
        int m_bytes   = (out_bytes > in_bytes) ? out_bytes : in_bytes;

        int batch = 1;
        int outputs = in_height * in_width * num * (classes + coords + 1);

        p_output = params->output ;

        p_input = (fp16*)params->cmxslice;
        p_output = p_input;
        clear_input = (fp16*)((u8*)p_input + m_bytes);

        dma::Task(dmaUser).start((u8*)(params->input), p_input, in_bytes);

        // repack
        p_in = (half4*)(p_input);
        for(j = 0; j < in_height; ++j){
            for(int i = 0; i < in_width; ++i){

                int k = 0;
                p_out = clear_input + i + in_width*j + in_width*in_height*0;

                for(; k < in_channels - 3; k += 4){
                    half4 rr = *p_in;
                    p_in++;
                    *p_out = rr[0];
                    p_out += in_width*in_height;
                    *p_out = rr[1];
                    p_out += in_width*in_height;
                    *p_out = rr[2];
                    p_out += in_width*in_height;
                    *p_out = rr[3];
                    p_out += in_width*in_height;
                }

                fp16 * sp_in = (fp16*)(p_in);
                for(; k < in_channels; ++k){
                    *p_out = *sp_in;
                    sp_in++;
                    p_out += in_width*in_height;
                }
                p_in = (half4*)sp_in;
            }
        }

        memcpy((u8*)p_output, clear_input, m_bytes);

        for (int b = 0; b < batch; ++b){
            for(int n = 0; n < num; ++n){
                int index = entry_index(in_width, in_height, outputs, coords, classes, b, n * in_width * in_height, 0);
                int index1 = entry_index(in_width, in_height, outputs, coords, classes, b, n * in_height * in_width, coords);
                activate_2array(p_output + index1, p_output + index, in_width * in_height);
            }
        }

        int index = entry_index(in_width, in_height, outputs, coords, classes, 0, 0, coords + 1);
        softmax_cpu(clear_input + index, classes + 0, batch * num, outputs/num, in_height * in_width, 1, in_height * in_width, 1, p_output + index);

        dma::Task(dmaUser).start((u8*)(p_output), (u8*)(params->output), out_bytes);
    }
}
