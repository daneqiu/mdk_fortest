/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///

#include "reorg_core.h"

#define INPUT_BPP sizeof(fp16)

#define BUFFER_ADDRESS_TRANSLATE(index, buffer_size, buffer_delta) (((index) < (buffer_size))?(index):(index + buffer_delta))

static void reorg_calculate(fp16 * __restrict inp, int w, int h, int c, int batch, int stride, fp16 * __restrict out,
                            fp16* __restrict out_buffer_cmx0,
                            fp16* __restrict out_buffer_cmx1,
                            fp16* __restrict inp_buffer_cmx0,
                            fp16* __restrict inp_buffer_cmx1,
                            int buffer_size0,
                            int start_c, int end_c, bool use_cmx)
{
    if (use_cmx) // 2 double CMX buffers
    {
        int delta_out_buf = (out_buffer_cmx1 - out_buffer_cmx0) - (buffer_size0);
        int delta_inp_buf = (inp_buffer_cmx1 - inp_buffer_cmx0) - (buffer_size0);

        int out_c = c / (stride * stride);
        int oc = c * (stride * stride);
        int oh = h / stride;
        int ow = w / stride;

        for(int b = 0; b < batch; ++b)
        {
            for(int k = start_c; k <= end_c; k++)
            {
                for(int j = 0; j < h; ++j)
                {
                    int in_index = w * (j + h*(k + c*b)) + (0);
                    int new_z = in_index / (oh*ow);
                    int new_y = (in_index %(oh*ow)) / ow;
                    int new_x = (in_index %(oh*ow)) % ow;
                    int new_index = new_z + new_x * oc + new_y * oc * ow;

                    in_index++;

                    int c2 = k % out_c;
                    int offset = k / out_c;
                    int w2 = 0 * stride + offset % stride;
                    int h2 = j * stride + offset / stride;
                    int out_index = w2 + w * stride * (h2 + h * stride * (c2 + out_c * b));

                    #pragma clang loop unroll_count(2)
                    for(int i = 0; i < w; ++i, out_index+=stride, in_index++)
                    {
                        // repacking coordinates
                        int k0 = out_index / (h*w);
                        int j0 = (out_index % (h*w)) / w;
                        int i0 = (out_index % (h*w)) % w;
                        int out_index_repack = k0 + c * i0 + c * w * j0;

                        /* split buffer address on 2 parts - (slice1, slice2) */
                        new_index = BUFFER_ADDRESS_TRANSLATE(new_index, buffer_size0, delta_out_buf);
                        out_index_repack = BUFFER_ADDRESS_TRANSLATE(out_index_repack, buffer_size0, delta_inp_buf);

                        out_buffer_cmx0[new_index] = inp_buffer_cmx0[out_index_repack];

                        int new_z = in_index / (oh*ow);
                        int new_y = (in_index %(oh*ow)) / ow;
                        int new_x = (in_index %(oh*ow)) % ow;
                        new_index = new_z + new_x * oc + new_y * oc * ow;
                    }
                }
            }
        }
    }
    else // Read/Write DDR directly
    {
        int out_c = c / (stride * stride);
        int oc = c * (stride * stride);
        int oh = h / stride;
        int ow = w / stride;

        for(int b = 0; b < batch; ++b)
        {
            for(int j = 0; j < h; ++j)
            {
                for(int k = start_c; k <= end_c; k++)
                {
                    int in_index = w * (j + h*(k + c*b)) + (0);
                    int new_z = in_index / (oh*ow);
                    int new_y = (in_index %(oh*ow)) / ow;
                    int new_x = (in_index %(oh*ow)) % ow;
                    int new_index = new_z + new_x * oc + new_y * oc * ow;

                    in_index++;

                    int c2 = k % out_c;
                    int offset = k / out_c;
                    int w2 = 0 * stride + offset % stride;
                    int h2 = j * stride + offset / stride;
                    int out_index = w2 + w * stride * (h2 + h * stride * (c2 + out_c * b));

                    #pragma clang loop unroll_count(2)
                    for(int i = 0; i < w; ++i, out_index+=stride, in_index++)
                    {
                        // repacking coordinates
                        int k0 = out_index / (h*w);
                        int j0 = (out_index % (h*w)) / w;
                        int i0 = (out_index % (h*w)) % w;
                        int out_index_repack = k0 + c * i0 + c * w * j0;

                        out[new_index] = inp[out_index_repack];

                        int new_z = in_index / (oh*ow);
                        int new_y = (in_index %(oh*ow)) / ow;
                        int new_x = (in_index %(oh*ow)) % ow;
                        new_index = new_z + new_x * oc + new_y * oc * ow;
                    }
                }
            }
        }
    }
}

void mvReorg(t_MvReorgParams *params)
{
    /* Input dimensions */
    s32 in_height   = params->in_height;
    s32 in_width    = params->in_width;
    s32 in_channels = params->in_channels;

    u32 *reorg_params = params->reorg_params;
    s32 stride = reorg_params[0];

    fp16 *p_input = params->input;
    fp16 *p_output = params->output;

    u32 start_c = params->start_c;
    u32 end_c = params->end_c;
    bool use_cmx = params->use_cmx;

    u8* out_cmxslice0 = params->out_buffer0;
    u8* out_cmxslice1 = params->out_buffer1;
    u8* inp_cmxslice0 = params->inp_buffer0;
    u8* inp_cmxslice1 = params->inp_buffer1;
    u32 buffer_size0 = params->buffer_size0;

    int batch = 1;
    reorg_calculate(p_input, (int)in_width, (int)in_height, (int)in_channels, batch, (int)stride, p_output,
                    (fp16*)out_cmxslice0, (fp16*)out_cmxslice1,
                    (fp16*)inp_cmxslice0, (fp16*)inp_cmxslice1,
                    buffer_size0,
                    start_c, end_c, use_cmx);
}
