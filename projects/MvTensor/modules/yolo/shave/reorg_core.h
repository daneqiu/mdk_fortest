///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef REORG_SHAVE_H_
#define REORG_SHAVE_H_

#include "../priv/mvReorgParam.h"

#ifdef __cplusplus
extern "C"
{
#endif

void mvReorg(t_MvReorgParams *params);

#ifdef __cplusplus
}
#endif

#endif // REORG_SHAVE_H_
