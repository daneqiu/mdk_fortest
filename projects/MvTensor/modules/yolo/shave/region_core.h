///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef REGION_SHAVE_H_
#define REGION_SHAVE_H_

#include "../priv/mvRegionParam.h"

#ifdef __cplusplus
extern "C"
{
#endif

void mvRegion(t_MvRegionParams *params);

#ifdef __cplusplus
}
#endif

#endif // REGION_SHAVE_H_
