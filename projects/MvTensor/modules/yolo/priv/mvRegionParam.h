///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
///
/// \details
///

#ifndef _MV_REGIONPARAM_H_
#define _MV_REGIONPARAM_H_

#include <mv_types.h>

#ifdef __MOVICOMPILE__
    #include <moviVectorTypes.h>
#else
    typedef fp16 half;
#endif

#define BUFFER_SIZE_REGION   (13 * 13 * 125 + 1)
/// Region parameters
typedef struct
{
    fp16 *input;
    fp16 *output;
    s32  in_width;
    s32  in_height;
    s32  in_channels;
    s32  out_width;
    s32  out_height;
    s32  out_channels;
    s32  start_height;
    s32  end_height;
    u32 *region_params;
    u32  dmaLinkAgent;
    u8  *cmxslice;
} t_MvRegionParams;

#endif /* _MV_REGIONPARAM_H_ */
