///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
///
/// \details
///

#ifndef _MV_REORGPARAM_H_
#define _MV_REORGPARAM_H_

#include <mv_types.h>

#ifdef __MOVICOMPILE__
    #include <moviVectorTypes.h>
#else
    typedef fp16 half;
#endif

/// Reorg parameters
typedef struct
{
    fp16* input;
    fp16* output;

    s32 in_width;
    s32 in_height;
    s32 in_channels;

    u32* reorg_params;
    u32 dmaLinkAgent;

    u32 start_c, end_c;
    bool use_cmx;
    // cmx buffers
    u32 buffer_size0;// size of CMX buffer in elements
    u8* out_buffer0;
    u8* out_buffer1;
    u8* inp_buffer0;
    u8* inp_buffer1;
} t_MvReorgParams;

#endif /* _MV_REORGPARAM_H_ */
