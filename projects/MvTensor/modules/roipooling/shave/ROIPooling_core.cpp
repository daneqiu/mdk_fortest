///
/// @file
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief C version of ROI Poooling adapted from
/// https://github.com/Austriker/caffe/blob/3c27a70ae7304feb48c60b268c70adf585879d50/src/caffe/layers/roi_pooling_layer.cpp
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <moviVectorUtils.h>
#include <swcCdma.h>
#include <svuCommonShave.h>
#include <mv_types.h>
#include <math.h>
#include <mvMacros.h>

#include <accumulateFp16.h>
#include "../priv/mvROIPoolingParam.h"
#include "ROIPooling_core.h"

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
#define INPUT_BPP   2

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------
#define align8(x) (((x) + 0x7) & (u32) ~0x7)
#define align16(x) (((x) + 0xF) & (u32) ~0xF)
#define align32(x) (((x) + 0x1F) & (u32) ~0x1F)

#define CMX_BUDGET (21000 * 4)

static inline void convert_from_channel_minor_B(t_MvROIPoolingParam *p, fp16* inputA, u32 c){

    // convert inputA from channel minor to column minor
    half *pinput_A, *ptemp_B;  // from A to B
    pinput_A = inputA;
    ptemp_B = (fp16*)p->temp_data;
    u32 W, H;
    W = p->in_width;
    H = p->in_height;
    for (int i = 0; i < c; i++) {
		fp16 *pinput_C = (fp16*)inputA + i;		
         for (int j = 0; j < H; j++) {
             for (int k = 0; k < W; k++) {
                     *ptemp_B = *pinput_C;
                     pinput_C += c;
                     ptemp_B ++;
                 }
        }
    }
    memcpy((u8*)inputA, (u8*)p->temp_data, c * W * H * sizeof(fp16));
}


static inline void convert_from_channel_minor(t_MvROIPoolingParam *p, fp16* inputA, u32 K){

    // convert inputA from channel minor to column minor
    half *pinput_A, *ptemp_B;  // from A to B
    pinput_A = inputA;
    ptemp_B = (fp16*)p->temp_data;
    u32 W, H;
    W = p->in_width;
    H = p->in_height;
    for (int i = 0; i < W; i++) {
         for (int j = 0; j < H; j++) {
             for (int k = 0; k < K; k++) {
                     *ptemp_B = *pinput_A;
                     pinput_A++;
                     ptemp_B += W * H;
                 }
         ptemp_B -= W * H * K;
         ptemp_B++;
        }
    }
    memcpy((u8*)inputA, (u8*)p->temp_data, (pinput_A - inputA) * sizeof(fp16));
}

static inline void convert_from_column_minor(t_MvROIPoolingParam *p, fp16* inputA, u32 K,u32 W,u32 H){

    // convert inputA from column minor to channel minor
    half *pinput_A, *ptemp_B;  // from A to B
    pinput_A = inputA;
    ptemp_B = (fp16*)p->temp_data;
    //u32 W, H;
    //W = p->in_width;
    //H = p->in_height;

    for (int i = 0; i < H; i++) {
         for (int j = 0; j < W; j++) {
             for (int k = 0; k < K; k++) {
                     *ptemp_B = *pinput_A;
                     pinput_A += W * H;
                     ptemp_B++;
                 }
         pinput_A -= W * H * K;
         pinput_A++;
        }
    }
    memcpy((u8*)inputA, (u8*)p->temp_data, K * W * H * sizeof(fp16));
}


void mvROIPooling(t_MvROIPoolingParam *p) {
    half MIN_HALF = -65504;
	dmaTransactionList_t *ref;
    dmaRequesterId dmaId;
    dmaTransactionList_t task1;	
	u8* output;	
	u8* input;	
    dmaId = dmaInitRequesterWithAgent(1, p->dmaLinkAgent);	

	fp16* bottom_rois = p->rois;
    fp16* top_data = (fp16*)p->cmxslice;
	for(int i = 0; i < p->radix_x * p->radix_y * p ->in_channels * p->num_rois; i++)
           top_data[i] = MIN_HALF;
    // For each ROI R = [batch_index x1 y1 x2 y2]: max pool over R
      for (int n = 0; n < p->num_rois; ++n) {
        int roi_batch_ind = 0;
        int roi_start_w = round(*(bottom_rois + 1) * p->spatial_scale);
        int roi_start_h = round(*(bottom_rois + 2) * p->spatial_scale);
        int roi_end_w = round(*(bottom_rois + 3) * p->spatial_scale);
        int roi_end_h = round(*(bottom_rois + 4) * p->spatial_scale);

        //for debug
		//roi_start_w = 0;
		//roi_start_h = 0;
		//roi_end_w = 15;
		//roi_end_h = 11;
		
        int roi_height = MAX(roi_end_h - roi_start_h + 1, 1);
        int roi_width = MAX(roi_end_w - roi_start_w + 1, 1);

        const fp32 bin_size_h = static_cast<fp32>(roi_height)
                                 / static_cast<fp32>(p->radix_y);

        const fp32 bin_size_w = static_cast<fp32>(roi_width)
                                 / static_cast<fp32>(p->radix_x);
        
        fp16* batch_data = p->input;

        for (int c = 0; c < p->in_channels; ++c) {
          for (int ph = 0; ph < p->radix_y; ++ph) {
            for (int pw = 0; pw < p->radix_x; ++pw) {
              // Compute pooling region for this output unit:
              //  start (included) = floor(ph * roi_height / pooled_height_)
              //  end (excluded) = ceil((ph + 1) * roi_height / pooled_height_)
              int hstart = static_cast<int>(floor(static_cast<fp32>(ph)
                                                  * bin_size_h));
              int wstart = static_cast<int>(floor(static_cast<fp32>(pw)
                                                  * bin_size_w));
              int hend = static_cast<int>(ceil(static_cast<fp32>(ph + 1)
                                               * bin_size_h));
              int wend = static_cast<int>(ceil(static_cast<fp32>(pw + 1)
                                               * bin_size_w));

              hstart = MIN(MAX(hstart + roi_start_h, 0), p->in_height);
              hend = MIN(MAX(hend + roi_start_h, 0), p->in_height);
              wstart = MIN(MAX(wstart + roi_start_w, 0), p->in_width);
              wend = MIN(MAX(wend + roi_start_w, 0), p->in_width);

              bool is_empty = (hend <= hstart) || (wend <= wstart);

              const int pool_index = (ph * p->radix_x + pw);
              if (is_empty) {
                  *(top_data + pool_index) = 0;
			  }
              for (int h = hstart; h < hend; ++h) {
                for (int w = wstart; w < wend; ++w) {
                  int index = (h * p->in_width + w);
                  if (*(batch_data + index) > *(top_data + pool_index)) {
                    *(top_data + pool_index) = *(batch_data + index);
                    }
                  }
                }
              }
            }
          // Increment all data pointers by one channel
          batch_data += p->in_height * p->in_width;
          top_data += p->radix_y * p->radix_x;
        }
        // Increment ROI data pointer
        bottom_rois += 5;
      }
	  
	for(int j =0; j<p->num_rois; j++) {
		output = (u8*)(p->output + j*p ->in_channels*p->radix_x*p->radix_y);
		input = (u8*)(p->cmxslice + j*p ->in_channels*p->radix_x*p->radix_y*sizeof(fp16));
		for (int i=0; i<p->radix_x*p->radix_y; i++) {
	    ref = dmaCreateTransactionFullOptions(dmaId, &task1, input,
	            output,
	            p ->in_channels*INPUT_BPP,   			// byte length
	            1 * INPUT_BPP,       					// src width
	            p ->in_channels * INPUT_BPP,      		// dst width
	            p->radix_x*p->radix_y * INPUT_BPP,      // src stride
	            0);     				 // dst stride
	    dmaStartListTask(ref);
	    dmaWaitTask(ref);
		output +=  p ->in_channels *INPUT_BPP;
		input += INPUT_BPP;
    	}	
	}
	
}
