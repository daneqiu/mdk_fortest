///
/// @file
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief
///

#ifndef _MVROIPOOLING_CORE_H_
#define _MVROIPOOLING_CORE_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include "../priv/mvROIPoolingParam.h"

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------

// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif

void mvROIPooling(t_MvROIPoolingParam *p);

#ifdef __cplusplus
}
#endif

#endif //__MVROIPOOLING_CORE_H__
