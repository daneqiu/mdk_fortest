///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
///
/// \details
///

#ifndef _MV_ROIPOOLINGPARAM_H_
#define _MV_ROIPOOLINGPARAM_H_
#include <mv_types.h>

#ifdef __MOVICOMPILE__
    #include <moviVectorTypes.h>
#else
    typedef fp16 half;
#endif
#define INPUT_BPP       2

/// mvROIPooling global parameters structure
typedef struct
{
    half* input;
    u32 in_height;
    u32 in_width;
    u32 in_channels;
    u32 num_rois;
    half* rois;
    u32 radix_x;
    u32 radix_y;
    fp32 spatial_scale;
    half* output;	
    u8* temp_data;
    u8* cmxslice;
    u32 dmaLinkAgent;
} t_MvROIPoolingParam;

#endif  //_MV_ROIPOOLINGPARAM_H_
