///
/// @file
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>

#include <DrvSvu.h>
#include <DrvTimer.h>
#include <DrvShaveL2Cache.h>
#include <mv_types.h>

//#include <mvROIPoolingParam.h>
#include <mvTensorInternal.h>
#include "mvROIPooling.h"
#include <mvMacros.h>
#include "../priv/mvROIPoolingParam.h"
#include <mvTensorInternal.h>
#include <mvModuleHandle.h>

// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------
// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
t_MvROIPoolingParam roipoolingParam[MVTENSOR_MAX_SHAVES] NOCACHE;
#define DMA_DESCRIPTORS_SECTION __attribute__((section(".cmx.cdmaDescriptors")))
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------

const u32 ALIGN_VALUE3 = 64;
namespace
{
    using namespace mv::tensor;
    class Roipooling : public Layer
    {
	virtual void run_(const t_MvTensorParam *mvTensorParam,
                const t_MvTensorOp &op,
                const mv::tensor::Optimization &,
                const Resources &)
    {
        ROIPooling((fp16*)mvTensorParam->input->data,
                mvTensorParam->input->dimY, mvTensorParam->input->dimX, mvTensorParam->input->dimZ,
                mvTensorParam->weights->dimZ, (fp16*)mvTensorParam->weights->data,
                mvTensorParam->weights->dimX, mvTensorParam->weights->dimY,
                *(fp32*)mvTensorParam->op->params,
                (fp16*)mvTensorParam->output->data, mvTensorParam->myriadResources);

				
	} 
    };

	const ModuleHandle handles[] =
    {
        ModuleHandle().add<Roipooling>(kRoipooling)
    };
}


MODULE_ENTRY_DECL(mvROIPooling);

void ROIPooling (fp16* input, u32 in_height, u32 in_width, u32 in_channels,
                 u32 num_rois, fp16* rois,
                 u32 radix_x, u32 radix_y,
                 fp32 spatial_scale,
                 fp16* output, t_MvTensorMyriadResources *myriadResources) {

	u32 temp_space;
	u8* p_temp_space = NULL;
	temp_space = in_height*in_width*in_channels*sizeof(fp16)*2;
    p_temp_space = (u8*)malloc(temp_space + ALIGN_VALUE3);
    mvTensorAssert(p_temp_space != NULL, "Cannot allocate space for proposal temp buffer.");
    u8* p_temp_space_aligned = ALIGN_UP(p_temp_space, ALIGN_VALUE3);	

	printf("num rois original %d....\n", num_rois);
	u32* pout = (u32*)(rois - 2);
	u32 cal_rois = (u32)(*pout);
	if(cal_rois < num_rois)
		num_rois = cal_rois;
	
	dmaTransactionList_t *ref;
    dmaRequesterId dmaId;    
    static dmaTransactionList_t DMA_DESCRIPTORS_SECTION task1;
	DrvCmxDmaInitDefault();
    dmaId = DrvCmxDmaInitRequesterOnAgent(1, myriadResources->dmaLinkAgent);
    u8* poutput = (u8*)p_temp_space_aligned;	
	u8* pinput = (u8*)input;
	for (int i=0; i<in_width*in_height; i++) {
	    ref = DrvCmxDmaCreateTransactionFullOptions(dmaId, &task1, pinput,
	            poutput,
	            in_channels *INPUT_BPP,   // byte length
	            in_channels * INPUT_BPP,       					// src width
	            1 * INPUT_BPP,      					 // dst width
	            0,            							// src stride
	            in_width*in_height * INPUT_BPP);   // dst stride
	    DrvCmxDmaStartListTask(ref);
	    DrvCmxDmaWaitTask(ref);
		poutput +=  INPUT_BPP;
		pinput += in_channels*INPUT_BPP;
    }
	
	ref = DrvCmxDmaCreateTransaction(dmaId, &task1, (u8*)p_temp_space_aligned,
	            (u8*)input, in_width * in_height * in_channels * sizeof(fp16));
	DrvCmxDmaStartListTask(ref);	
	DrvCmxDmaWaitTask(ref);
	
	for(int j=0; j< num_rois;) {
		int num_shave = 0;
	    for (int i = myriadResources->firstShave; i <= myriadResources->lastShave; i++) {
	        roipoolingParam[i].input = input;
	        roipoolingParam[i].in_height = in_height;
	        roipoolingParam[i].in_width = in_width;
	        roipoolingParam[i].in_channels = in_channels;
	        roipoolingParam[i].num_rois = 1;
	        roipoolingParam[i].rois = rois + j*5;
	        roipoolingParam[i].radix_x = radix_x;
	        roipoolingParam[i].radix_y = radix_y;
	        roipoolingParam[i].spatial_scale = spatial_scale;		
	        roipoolingParam[i].temp_data = p_temp_space_aligned;
	        roipoolingParam[i].output = output + j * in_channels * radix_x *radix_y;
	        roipoolingParam[i].dmaLinkAgent = myriadResources->dmaLinkAgent;
	        roipoolingParam[i].cmxslice = getCMXSliceDataSection(i);

	        startShave(i, (u32)&MODULE_ENTRY(mvROIPooling), (u32)&roipoolingParam[i]);
			j++;
			num_shave ++;
			if(j >= num_rois)
				break;
	    }		
		waitShaves(myriadResources->firstShave, myriadResources->firstShave + num_shave);
	}

    DrvShaveL2CachePartitionFlushAndInvalidate(myriadResources->dataPartitionNo);	
    free(p_temp_space);
}
