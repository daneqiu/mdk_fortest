///
/// @file
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief
///

#ifndef _ROIPOOLING_H_
#define _ROIPOOLING_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include <mv_types.h>
#include <mvTensor.h>

#ifdef __cplusplus
extern "C"
{
#endif

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------
// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------

void ROIPooling (fp16* input, u32 in_height, u32 in_width, u32 in_channels,
                 u32 num_rois, fp16* rois,
                 u32 radix_x, u32 radix_y,
                 fp32 spatial_scale,
                 fp16* output, t_MvTensorMyriadResources *myriadResources);

#ifdef __cplusplus
}
#endif

#endif  //_ROIPOOLING_H_
