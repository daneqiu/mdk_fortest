#ifndef _MV_PROPOSALPARAM_H_
#define _MV_PROPOSALPARAM_H_
#include <mv_types.h>

#ifdef __MOVICOMPILE__
    #include <moviVectorTypes.h>
#else
    typedef fp16 half;
#endif

const fp32 RPN_NMS_THRESH = 0.7;
const u32 RPN_PRE_NMS_TOP_N = 6000;
const u32 RPN_POST_NMS_TOP_N = 300;
const u32 ALIGN_VALUE = 64;
const u32 RPN_MIN_SIZE = 16;
const u32 BASE_SIZE = 16;
const u32 NMS_BLOCK_SIZE = 64;
#define INPUT_BPP       2

/// mvROIPooling global parameters structure
typedef struct
{
    half* input1;
    u32 in_height;
    u32 in_width;
    u32 in1_channels;
    u32 num_rois;
    half* input2;
    u32 in2_channels;
    fp32* params;
    u8* temp_data;
    half* output;
    u8* cmxslice;
    u32 dmaLinkAgent;
} t_MvProposalParam;

#endif  //_MV_PROPOSALPARAM_H_

