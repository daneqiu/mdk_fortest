#include <mvTensorInternal.h>
#include <mvModuleHandle.h>
#include "mvProposal.h"
#include <stdio.h>
#include <stdlib.h>

#include <DrvSvu.h>
#include <DrvTimer.h>
#include <DrvShaveL2Cache.h>
#include <mv_types.h>
#include <mvMacros.h>
#include "../priv/mvProposalParam.h"

t_MvProposalParam proposalParam[MVTENSOR_MAX_SHAVES] NOCACHE;

namespace
{
    using namespace mv::tensor;
    class Proposal : public Layer
    {
	virtual void run_(const t_MvTensorParam *mvTensorParam,
                const t_MvTensorOp &op,
                const mv::tensor::Optimization &,
                const Resources &)
    {
        proposal((fp16*)mvTensorParam->input->data,
                        mvTensorParam->input->dimY, mvTensorParam->input->dimX, mvTensorParam->input->dimZ,
                        mvTensorParam->weights->dimY, (fp16*)mvTensorParam->weights->data,
                        mvTensorParam->weights->dimZ,
                        (fp32*)mvTensorParam->op->params,
                        (fp16*)mvTensorParam->output->data, mvTensorParam->myriadResources);

				
	} 
    };

	const ModuleHandle handles[] =
    {
        ModuleHandle().add<Proposal>(kProposal)
    };
}

MODULE_ENTRY_DECL(mvProposal);
void proposal(fp16* input1, u32 in_height, u32 in_width, u32 in1_channels,
              u32 num_rois,
              fp16* input2, u32 in2_channels,
              fp32* params,
              fp16* output, t_MvTensorMyriadResources *myriadResources) {
	
    u32 max_area = 0;
    const u32 input_area = in_height * in_width;
    max_area = MAX(max_area,  input_area);
    const u32 num_anchors_ratios = (u32)*(params + 1);
    const u32 num_anchors_scales = (u32)*(params + 2 + num_anchors_ratios);
    const u32 num_anchors = num_anchors_ratios * num_anchors_scales;
    const u32 anchors_size = num_anchors * 4;
    const u32 index_roi_size = RPN_POST_NMS_TOP_N;
    const u32 proposals_size = num_anchors * max_area * 5;
    const u32 num_boxes_size = MIN(num_anchors * input_area, RPN_PRE_NMS_TOP_N);
    u32 temp_space = anchors_size * sizeof(fp32)
                     + index_roi_size * sizeof(u32)
                     + proposals_size * sizeof(fp32)
                     + num_boxes_size * sizeof(u8);

    u32 input1_space = input_area * in1_channels * sizeof(fp16);
    u32 input2_space = input_area * in2_channels * sizeof(fp16);

    // allocate a buffer big enough for proposals, input transformation and anchor transformation
    temp_space = MAX(temp_space, input1_space);
    temp_space = MAX(temp_space, input2_space);
    u8* p_temp_space = NULL;
    p_temp_space = (u8*)malloc(temp_space + ALIGN_VALUE);
    mvTensorAssert(p_temp_space != NULL, "Cannot allocate space for proposal temp buffer.");
    u8* p_temp_space_aligned = ALIGN_UP(p_temp_space, ALIGN_VALUE);

	//currently we don't support mult shave
	myriadResources->firstShave = 0;
	myriadResources->lastShave = 0;

    for (int i = myriadResources->firstShave; i <= myriadResources->lastShave; i++) {
        proposalParam[i].input1 = input1;
        proposalParam[i].in_height = in_height;
        proposalParam[i].in_width = in_width;
        proposalParam[i].in1_channels = in1_channels;
        proposalParam[i].num_rois = num_rois;
        proposalParam[i].input2 = input2;
        proposalParam[i].in2_channels = in2_channels;
        proposalParam[i].params = params;
        proposalParam[i].temp_data = p_temp_space_aligned;
        proposalParam[i].output = output;
        proposalParam[i].dmaLinkAgent = myriadResources->dmaLinkAgent;
        proposalParam[i].cmxslice = getCMXSliceDataSection(i);

        startShave(i, (u32)&MODULE_ENTRY(mvProposal), (u32)&proposalParam[i]);
    }

    waitShaves(myriadResources->firstShave, myriadResources->lastShave);

    free(p_temp_space);
    DrvShaveL2CachePartitionFlushAndInvalidate(myriadResources->dataPartitionNo);
}

