#ifndef REGION_LEON_H_
#define REGION_LEON_H_

#include <mv_types.h>
#include <mvTensor.h>

#include <mvTensorResources.h>

void proposal(fp16* input1, u32 in_height, u32 in_width, u32 in1_channels,
              u32 num_rois,
              fp16* input2, u32 in2_channels,
              fp32* params,
              fp16* output, t_MvTensorMyriadResources *myriadResources);

#endif
