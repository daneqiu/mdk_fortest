#ifndef PROPOSAL_SHAVE_H_
#define PROPOSAL_SHAVE_H_

#include "../priv/mvProposalParam.h"

#ifdef __cplusplus
extern "C"
{
#endif

//void mvSoftMaxAxisC(t_MvSoftMaxParam *p);
//void mvSoftMaxAxisH(t_MvSoftMaxParam *p);
void mvProposal(t_MvProposalParam *p);
#ifdef __cplusplus
}
#endif

#endif // PROPOSAL_SHAVE_H_

