#include "proposal_core.h"
#include <mvTensorDma.h>
#include <mvTensorConfig.h>
#include <swcCdmaCommon.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <moviVectorUtils.h>
#include <swcCdma.h>
#include <svuCommonShave.h>
#include <mv_types.h>
#include <mvMacros.h>

#include <accumulateFp16.h>

#define INPUT_BPP   2

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------
#define align8(x) (((x) + 0x7) & (u32) ~0x7)
#define align16(x) (((x) + 0xF) & (u32) ~0xF)
#define align32(x) (((x) + 0x1F) & (u32) ~0x1F)

#define CMX_BUDGET (21000 * 4)

int kBatch = 1;

// given a base box, enumerate transformed boxes of varying sizes and ratios
//   base_size: base box's width & height (i.e., base box is square)
//   scales: "num_scales x 1" array of scale factors for base box transform
//   ratios: "num_ratios x 1" array of height-width ratios
//   anchors: "num_anchors x 4" array,  (x1, y1, x2, y2) for each box
//   num_anchors: total number of transformations
//                = num_scales * num_ratios
static inline
void generate_anchors(const fp32 scales[], const fp32 ratios[],
                      fp32 anchors[],
                      const int num_scales, const int num_ratios,
                      const int base_size)
{
  // base box's width & height & center location
  const fp32 base_area = (fp32)(base_size * base_size);
  const fp32 center = 0.5f * (base_size - 1.0f);

  // enumerate all transformed boxes
  {
    fp32* p_anchors = anchors;
    for (int j0 = 0; j0 < num_scales; j0 += num_ratios) {
    for (int i = 0; i < num_ratios; ++i) {
      // transformed width & height for given ratio factors
      const fp32 ratio_w = (fp32)round(sqrt(base_area / ratios[i]));
      const fp32 ratio_h = (fp32)round(ratio_w * ratios[i]);

      for (int j = 0; j < num_ratios; ++j) {
        // transformed width & height for given scale factors
        const fp32 scale_w = 0.5f * (ratio_w * scales[j0 + j] - 1.0f);
        const fp32 scale_h = 0.5f * (ratio_h * scales[j0 + j] - 1.0f);

        // (x1, y1, x2, y2) for transformed box
        p_anchors[0] = center - scale_w;
        p_anchors[1] = center - scale_h;
        p_anchors[2] = center + scale_w;
        p_anchors[3] = center + scale_h;

        p_anchors += 4;
      } // endfor j
    }} // endfor i, j0
  }
}

// transform a box according to a given gradient
//   box: (x1, y1, x2, y2)
//   gradient: dx, dy, d(log w), d(log h)
// with assuring the transformed box to be within image (img_W, img_H),
// and checking whether the new box's size >= (min_box_W, min_box_H)
static
int transform_box(fp32 box[],
                  const fp32 dx, const fp32 dy,
                  const fp32 d_log_w, const fp32 d_log_h,
                  const fp32 img_W, const fp32 img_H,
                  const fp32 min_box_W, const fp32 min_box_H)
{
  // width & height of box
  const fp32 w = box[2] - box[0] + 1.0f;
  const fp32 h = box[3] - box[1] + 1.0f;
  // center location of box
  const fp32 ctr_x = box[0] + 0.5f * w;
  const fp32 ctr_y = box[1] + 0.5f * h;

  // new center location according to gradient (dx, dy)
  const fp32 pred_ctr_x = dx * w + ctr_x;
  const fp32 pred_ctr_y = dy * h + ctr_y;
  // new width & height according to gradient d(log w), d(log h)
  const fp32 pred_w = exp(d_log_w) * w;
  const fp32 pred_h = exp(d_log_h) * h;

  // update upper-left corner location
  box[0] = pred_ctr_x - 0.5f * pred_w;
  box[1] = pred_ctr_y - 0.5f * pred_h;
  // update lower-right corner location
  box[2] = pred_ctr_x + 0.5f * pred_w;
  box[3] = pred_ctr_y + 0.5f * pred_h;

  // adjust new corner locations to be within the image region,
  box[0] = MAX(0.0f,  MIN(box[0],  img_W - 1.0f));
  box[1] = MAX(0.0f,  MIN(box[1],  img_H - 1.0f));
  box[2] = MAX(0.0f,  MIN(box[2],  img_W - 1.0f));
  box[3] = MAX(0.0f,  MIN(box[3],  img_H - 1.0f));

  // recompute new width & height
  const fp32 box_w = box[2] - box[0] + 1.0f;
  const fp32 box_h = box[3] - box[1] + 1.0f;

  // check if new box's size >= threshold
  return (box_w >= min_box_W) * (box_h >= min_box_H);
}

// generate all candidate boxes with their scores
//   bottom: 1 x num_anchors x H x W tensor
//     bottom[0, k, h, w] = foreground score of anchor k at node (h, w)
//   d_anchor: num_anchors x 4 x H x W tensor
//     d_anchor[k, :, h, w] = gradient (dx, dy, d(log w), d(log h))
//                            of anchor k at center location (h, w)
//   num_anchors: number of anchors  (= # scales * # ratios)
//   anchors: num_anchors * 4 array,  (x1, y1, x2, y2) for each anchor
//   img_H, img_W: scaled image height & width
//   min_box_H, min_box_W: minimum box height & width
//   feat_stride: scaled image height (width) / bottom height (width)
//   proposals: num_proposals * 5 array
//     num_proposals = num_anchors * H * W
//     (x1, y1, x2, y2, score) for each proposal
static inline
void enumerate_proposals(const fp16 bottom4d[],
                             const fp16 d_anchor4d[],
                             const fp32 anchors[],
                             fp32 proposals[],
                             int* num_proposals,
                             const int num_anchors,
                             const int bottom_H, const int bottom_W,
                             const fp32 img_H, const fp32 img_W,
                             const fp32 min_box_H, const fp32 min_box_W,
                             const int feat_stride)
{
    const int bottom_area = bottom_H * bottom_W;
    int index = 0;
    *num_proposals = 0;
    fp32 proposal_candidate[5];
    for (int h = 0; h < bottom_H; ++h)
    {
        for (int w = 0; w < bottom_W; ++w)
        {
            const fp32 x = w * feat_stride;
            const fp32 y = h * feat_stride;
            const fp16* p_box = d_anchor4d + h * bottom_W + w;
            const fp16* p_score = bottom4d + h * bottom_W + w;
            for (int k = 0; k < num_anchors; ++k)
            {
                if ((fp32)p_score[k * bottom_area] < 0.1) continue;

                const fp32 dx = (fp32) p_box[(k * 4 + 0) * bottom_area];
                const fp32 dy = (fp32) p_box[(k * 4 + 1) * bottom_area];
                const fp32 d_log_w = (fp32) p_box[(k * 4 + 2) * bottom_area];
                const fp32 d_log_h = (fp32) p_box[(k * 4 + 3) * bottom_area];

                proposal_candidate[0] = x + anchors[k * 4 + 0];
                proposal_candidate[1] = y + anchors[k * 4 + 1];
                proposal_candidate[2] = x + anchors[k * 4 + 2];
                proposal_candidate[3] = y + anchors[k * 4 + 3];

                proposal_candidate[4]
                    = transform_box(proposal_candidate,
                    dx, dy, d_log_w, d_log_h,
                    img_W, img_H, min_box_W, min_box_H)
                    * (fp32)p_score[k * bottom_area];
                if (proposal_candidate[4] >= 0)
                {
                    memcpy(&proposals[index], proposal_candidate, sizeof(fp32)* 5);
                    index += 5;
                    *(num_proposals) = *(num_proposals) + 1;
                }
            } // endfor k
        } // endfor w
    } // endfor h
}

// quick-sort a list of boxes in descending order of their scores (CPU)
//   list_cpu: num_boxes x 5 array,  (x1, y1, x2, y2, score) for each box
//             located at main memory
//   if num_top <= end,  only top-k results are guaranteed to be sorted
//   (for efficient computation)
static inline
void sort_box(fp32 list_cpu[], const int start, const int end,
              const int num_top)
{
    const fp32 pivot_score = list_cpu[start * 5 + 4];
  int left = start + 1, right = end;
  fp32 temp[5];
  while (left <= right) {
    while (left <= end && list_cpu[left * 5 + 4] >= pivot_score) ++left;
    while (right > start && list_cpu[right * 5 + 4] <= pivot_score) --right;
    if (left <= right) {
      for (int i = 0; i < 5; ++i) {
        temp[i] = list_cpu[left * 5 + i];
      }
      for (int i = 0; i < 5; ++i) {
        list_cpu[left * 5 + i] = list_cpu[right * 5 + i];
      }
      for (int i = 0; i < 5; ++i) {
        list_cpu[right * 5 + i] = temp[i];
      }
      ++left;
      --right;
    }
  }

  if (right > start) {
    for (int i = 0; i < 5; ++i) {
      temp[i] = list_cpu[start * 5 + i];
    }
    for (int i = 0; i < 5; ++i) {
      list_cpu[start * 5 + i] = list_cpu[right * 5 + i];
    }
    for (int i = 0; i < 5; ++i) {
      list_cpu[right * 5 + i] = temp[i];
    }
  }

  if (start < right - 1) {
    sort_box(list_cpu, start, right - 1, num_top);
  }
  if (right + 1 < num_top && right + 1 < end) {
    sort_box(list_cpu, right + 1, end, num_top);
  }
}

/**
* "IoU = intersection area / union area" of two boxes A, B
*   A, B: 4-dim array (x1, y1, x2, y2)
*/
static fp32 iou(const fp32* A, const fp32* B)
{
    // overlapped region (= box)
    const fp32 x1 = MAX(A[0], B[0]);
    const fp32 y1 = MAX(A[1], B[1]);
    const fp32 x2 = MIN(A[2], B[2]);
    const fp32 y2 = MIN(A[3], B[3]);

    // intersection area
    const fp32 width = MAX(0.0f, x2 - x1 + 1.0f);
    const fp32 height = MAX(0.0f, y2 - y1 + 1.0f);
    const fp32 area = width * height;

    // area of A, B
    const fp32 A_area = (A[2] - A[0] + 1.0f) * (A[3] - A[1] + 1.0f);
    const fp32 B_area = (B[2] - B[0] + 1.0f) * (B[3] - B[1] + 1.0f);

    // IoU
    return area / (A_area + B_area - area);
}

static inline
void nms(const int num_boxes, fp32* boxes, u8* is_dead,
        int* const num_out, int* index_out_cpu,
        const int base_index, const fp32 nms_thresh, const int max_num_out,
        const int bbox_vote, const fp32 vote_thresh)
{
    int num_selected = 0;

    memset(is_dead, 0, num_boxes * sizeof(unsigned char));

    for (int i = 0; i < num_boxes; ++i) {
        if (is_dead[i]) {
            continue;
        }

        index_out_cpu[num_selected++] = base_index + i;

        if (bbox_vote) {
            fp32 sum_score = boxes[i * 5 + 4];
            fp32 sum_box[4] = {
                sum_score * boxes[i * 5 + 0], sum_score * boxes[i * 5 + 1],
                sum_score * boxes[i * 5 + 2], sum_score * boxes[i * 5 + 3]
            };

            for (int j = 0; j < i; ++j) {
                if (is_dead[j] && iou(&boxes[i * 5], &boxes[j * 5]) > vote_thresh) {
                    fp32 score = boxes[j * 5 + 4];
                    sum_box[0] += score * boxes[j * 5 + 0];
                    sum_box[1] += score * boxes[j * 5 + 1];
                    sum_box[2] += score * boxes[j * 5 + 2];
                    sum_box[3] += score * boxes[j * 5 + 3];
                    sum_score += score;
                }
            }
            for (int j = i + 1; j < num_boxes; ++j) {
                fp32 iou_val = iou(&boxes[i * 5], &boxes[j * 5]);
                if (!is_dead[j] && iou_val > nms_thresh) {
                    is_dead[j] = 1;
                }
                if (iou_val > vote_thresh) {
                    fp32 score = boxes[j * 5 + 4];
                    sum_box[0] += score * boxes[j * 5 + 0];
                    sum_box[1] += score * boxes[j * 5 + 1];
                    sum_box[2] += score * boxes[j * 5 + 2];
                    sum_box[3] += score * boxes[j * 5 + 3];
                    sum_score += score;
                }
            }

            boxes[i * 5 + 0] = sum_box[0] / sum_score;
            boxes[i * 5 + 1] = sum_box[1] / sum_score;
            boxes[i * 5 + 2] = sum_box[2] / sum_score;
            boxes[i * 5 + 3] = sum_box[3] / sum_score;
        }

        else {
            for (int j = i + 1; j < num_boxes; ++j) {
                if (!is_dead[j] && iou(&boxes[i * 5], &boxes[j * 5]) > nms_thresh) {
                    is_dead[j] = 1;
                }
            }
        }

        if (num_selected == max_num_out) {
            break;
        }
    }

    *num_out = num_selected;
}

// retrieve proposals that are determined to be kept as RoIs by NMS
//   proposals : "num_boxes x 5" array,  (x1, y1, x2, y2, score) for each box
//   num_rois: number of RoIs to be retrieved
//   index_roi: "num_rois x 1" array
//   index_roi[i]: index of i-th RoI in proposals
//   rois: "num_rois x 5" array,  (x1, y1, x2, y2, score) for each RoI
void retrieve_rois(const fp32 proposals[],
                   const int index_roi[],
                   fp16 rois[],
                   const int num_rois)
{
  int i;
  for (i = 0; i < num_rois - 1; ++i) {
    const fp32* const proposals_index = proposals + index_roi[i] * 5;
    rois[i * 5 + 0] = (fp16) proposals_index[0];
    rois[i * 5 + 1] = (fp16) proposals_index[1];
    rois[i * 5 + 2] = (fp16) proposals_index[2];
    rois[i * 5 + 3] = (fp16) proposals_index[3];
    rois[i * 5 + 4] = 0; // 0 no scores, otherwise: (fp16) proposals_index[4];
  }

  // last iteration doesn't write outside the buffer
  const fp32* const proposals_index = proposals + index_roi[i] * 5;
  rois[i * 5 + 0] = (fp16) proposals_index[0];
  rois[i * 5 + 1] = (fp16) proposals_index[1];
  rois[i * 5 + 2] = (fp16) proposals_index[2];
  rois[i * 5 + 3] = (fp16) proposals_index[3];

}

void mvProposal(t_MvProposalParam *p) {

	dmaTransactionList_t *ref;
    dmaRequesterId dmaId;
    dmaTransactionList_t task1;	
	dmaId = dmaInitRequesterWithAgent(1, p->dmaLinkAgent);     

    u8* output = (u8*)p->temp_data;    
    u8* input = (u8*)p->input1;
    for (int i=0; i<p->in1_channels; i++) {
    	ref = dmaCreateTransactionFullOptions(dmaId, &task1, input,
                   output,
                   p->in_width * p->in_height *INPUT_BPP,   // byte length
                   1 * INPUT_BPP,                                              // src width
                   1 * INPUT_BPP,                                               // dst width
                   p->in1_channels * INPUT_BPP,            // src stride
                   1 * INPUT_BPP);                                      // dst stride
     	dmaStartListTask(ref);
        dmaWaitTask(ref);
        output +=  p->in_width * p->in_height *INPUT_BPP;
        input += INPUT_BPP;
    }  
    ref = dmaCreateTransaction(dmaId, &task1, (u8*)p->temp_data,
                   (u8*)p->input1, p->in_width * p->in_height * p->in1_channels * sizeof(fp16));
    dmaStartListTask(ref);  
    dmaWaitTask(ref);

	output = (u8*)p->temp_data;    
    input = (u8*)p->input2;
	
    for (int i=0; i<p->in2_channels; i++) {
    	ref = dmaCreateTransactionFullOptions(dmaId, &task1, input,
                   output,
                   p->in_width * p->in_height *INPUT_BPP,   // byte length
                   1 * INPUT_BPP,                                              // src width
                   1 * INPUT_BPP,                                               // dst width
                   p->in2_channels * INPUT_BPP,            // src stride
                   1 * INPUT_BPP);                                      // dst stride
        dmaStartListTask(ref);
        dmaWaitTask(ref);
        output +=  p->in_width * p->in_height *INPUT_BPP;
        input += INPUT_BPP;
    }  
    ref = dmaCreateTransaction(dmaId, &task1, (u8*)p->temp_data,
                   (u8*)p->input2, p->in_width * p->in_height * p->in2_channels * sizeof(fp16));
    dmaStartListTask(ref);  
    dmaWaitTask(ref);
	
    u32 idx_params = 0;
    const int feat_stride = *(p->params + idx_params++);
    // number of anchors  (= number of scales * ratios)
    const int num_ratios = *(p->params + idx_params++);
    idx_params += num_ratios;
    const int num_scales = *(p->params + idx_params++);
    const int num_anchors = num_ratios * num_scales;
    idx_params += num_scales;

    // do forward-pass for each item in the batch
    fp16* p_bottom_item = p->input1;
    fp16* p_d_anchor_item = p->input2;
    fp16* p_top_item = p->output + 1;
    int total_top_size = 0;

    u32 idx_temp_data = 0;
    fp32* anchors = (fp32*)p->temp_data;
    idx_temp_data += num_anchors * 4 * sizeof(fp32);
    int* index_roi = (int*)(p->temp_data + idx_temp_data);
    idx_temp_data += RPN_POST_NMS_TOP_N * sizeof(int);
    fp32* proposals = (fp32*)(p->temp_data + idx_temp_data);


	// bottom shape: 2 x num_anchors x H x W
	const int bottom_H = p->in_height;
	const int bottom_W = p->in_width;
	// input image height & width
	const fp32 img_H = *(p->params + idx_params++);
	const fp32 img_W = *(p->params + idx_params++);
	// scale factor for height & width
	const fp32 scale = *(p->params + idx_params++);
	// minimum box width & height
	const fp32 min_box_H = RPN_MIN_SIZE * scale;
	const fp32 min_box_W = RPN_MIN_SIZE * scale;
	// number of all proposals = num_anchors * H * W
	int num_proposals = num_anchors * bottom_H * bottom_W;
	idx_temp_data += num_proposals * 5 * sizeof(fp32);
	u8* is_dead = (u8*)(p->temp_data + idx_temp_data);
	
	generate_anchors(p->params + 3 + num_ratios /*scales*/,
	                 p->params + 2 /*ratios*/,
	                 anchors,
	                 num_scales, num_ratios,
	                 BASE_SIZE);
	// enumerate all proposals
	//   num_proposals = num_anchors * H * W
	//   (x1, y1, x2, y2, score) for each proposal
	// NOTE: for bottom, only foreground scores are passed
	// printf("\n\nenumerate_proposals\n\n\n ");
	enumerate_proposals(
		        p_bottom_item + num_proposals,  p_d_anchor_item,
		        anchors,  proposals,
		        &num_proposals,
		        num_anchors,
		        bottom_H,  bottom_W,  img_H * scale,  img_W * scale,  min_box_H,  min_box_W,
		        feat_stride);

	sort_box(proposals, 0, num_proposals - 1, RPN_PRE_NMS_TOP_N);


    // NMS & RoI retrieval
    int num_rois = 0;
    fp32* const p_proposals = proposals;
	nms(MIN(num_proposals, RPN_PRE_NMS_TOP_N),  p_proposals,
	        is_dead,  &num_rois,  index_roi,  0,
	        RPN_NMS_THRESH,  MIN(p->num_rois,RPN_POST_NMS_TOP_N),
	        0/*option->bbox_vote*/,  0/*option->vote_thresh*/);

    // RoI retrieval
	*(p->output) = 0;
	u32* pout = (u32*)(p->output - 2);
	*pout = num_rois;
	retrieve_rois(proposals,  index_roi,  p_top_item,  num_rois);
#if 0

    printf("num_rois = %d\n", num_rois);  
	for(int i = 0; i < num_rois; i++)
		printf("all the region result %f, %f, %f, %f\n", (fp32)*(p->output + i * 5 +1),(fp32)*(p->output + i * 5 + 2),(fp32)*(p->output + i * 5 + 3),(fp32)*(p->output + i * 5 + 4),(fp32)*(p->output + i * 5 + 5));
             
    printf("bottom_H: %d\n", bottom_H);
    printf("bottom_W: %d\n", bottom_W);
    printf("img_H: %f\n", img_H);
    printf("img_W: %f\n", img_W);
    printf("scale: %f\n", scale);
    printf("min_box_H: %f\n", min_box_H);
    printf("min_box_W: %f\n", min_box_W);
    printf("num_proposals: %d\n", num_proposals);

#endif
}

