///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief Leon wrappers for CNN post operations.
///

#include <mvModuleHandle.h>
#include <mvTensorUtil.h>
#include <mvTensorDebug.h>
#include <algorithm>

namespace
{
    using namespace mv::tensor;

    class Reshape : public Layer
    {
    private:
        static void reshape(fp16 *input, fp16 *output,
                    s32 in_width, s32 in_height, s32 in_channels,
                    s32 out_width, s32 out_height, s32 out_channels,
                    u32 input_stride, u32 output_stride,
                    const Resources &res)
        {
            s32 elems_in_ch = in_width * in_height;
            s32 elems_out_ch = out_width * out_height;
            s32 rem_in_ch, rem_out_ch;
            s32 elems_dma = 0;
            s32 c_in, c_out;
            u8  *p_input, *p_output;

            // this function needs to perform the reshaping as if data was in
            // ZYX order (Caffe), therefore, we need to reinterleave data for the new out_channels value
            c_in = 0;
            c_out = 0;
            rem_in_ch = elems_in_ch;
            rem_out_ch = elems_out_ch;
            p_input  = (u8*)input  + c_in*sizeof(fp16);
            p_output = (u8*)output + c_out*sizeof(fp16);

            mvTensorAssert(input_stride == in_channels * sizeof(fp16), "Reshape doesn't support input stride != in_ch * sizeof(fp16)");

            if ((in_channels == out_channels) ||
                (in_width * in_height == 1  && out_channels == 1) ||
                (out_width * out_height == 1 && in_channels == 1)
               )
            {
                // in these cases data is contiguous in memory and no data manipulation (reinterleaving) is needed
                elems_dma = out_width * out_height * out_channels;
                dma::Task(res.dmaUser).start(
                    (u8 *)input,
                    (u8 *)output,
                    sizeof(fp16) * elems_dma,
                    sizeof(fp16) * elems_dma,
                    sizeof(fp16) * elems_dma,
                    input_stride,
                    output_stride);
            }
            else if (in_width == 1 && in_height == 1)
            {
                s32 elems_dma, iter, in_step, src_w, dst_w, src_stride;

                // prefer strided read/write on the smaller size and
                // bulk read/write (stride = 1) on the larger size for better DMA speed
                if (out_width * out_height > out_channels)
                {
                    elems_dma = out_width * out_height;
                    iter      = out_channels;
    
                    src_w     = elems_dma;
                    src_stride  = in_channels;
                    dst_w     = 1;
                }
                else
                {
                    elems_dma = out_channels;
                    iter      = out_width * out_height;
    
                    src_w     = 1;
                    src_stride  = out_width * out_height * sizeof(fp16);
                    dst_w     = elems_dma;
                }

                do
                {
                    dma::Task(res.dmaUser).start(
                             p_input,                                         // src
                             p_output,                                        // dst
                             sizeof(fp16) * elems_dma,                        // byte length
                             sizeof(fp16) * src_w,                            // src width
                             sizeof(fp16) * dst_w,                            // dst width
                             src_stride,                                        // src stride
                             output_stride);                                  // dst stride

                    p_input  += src_w*sizeof(fp16);
                    p_output += dst_w*sizeof(fp16);
                }
                while (--iter > 0);
            }
            else if (out_width == 1 && out_height == 1)
            {
                // this is the flatten case
                s32 elems_dma, iter, in_step, src_w, dst_w, src_stride, dst_stride;
    
                // prefer strided read/write on the smaller size and
                // bulk read/write (stride = 1) on the larger size for better DMA speed
                if (in_width * in_height < in_channels &&
                	output_stride == out_channels * sizeof(fp16))
                {
                    elems_dma = in_channels;
                    iter      = in_width * in_height;
                    src_w     = elems_dma;
                    src_stride  = in_channels * sizeof(fp16);
                    dst_w     = 1;
                    dst_stride  = in_width * in_height * sizeof(fp16);
                }
                else
                {
                    elems_dma = in_width * in_height;
                    iter      = in_channels;
                    src_w     = 1;
                    src_stride  = in_channels * sizeof(fp16);
                    dst_w     = elems_dma;
                    dst_stride  = output_stride;
                }
    
                do
                {
                    dma::Task(res.dmaUser).start(
                             p_input,                                         // src
                             p_output,                                        // dst
                             sizeof(fp16) * elems_dma,                        // byte length
                             sizeof(fp16) * src_w,                            // src width
                             sizeof(fp16) * dst_w,                            // dst width
                             src_stride,                                        // src stride
                             dst_stride);                                       // dst stride
    
                    p_input  += src_w*sizeof(fp16);
                    p_output += dst_w*sizeof(fp16);
                }
                while (--iter > 0);
            }    
            else
            {
                do
                {
                    elems_dma = std::min(rem_in_ch, rem_out_ch);

                    dma::Task(res.dmaUser).start(
                             p_input,                                         // src
                             p_output,                                        // dst
                             sizeof(fp16) * elems_dma,                        // byte length
                             sizeof(fp16) * 1,                                // src width
                             sizeof(fp16) * 1,                                // dst width
                             input_stride,                                    // src stride
                             output_stride);                                  // dst stride

                    p_input    += elems_dma * input_stride;
                    p_output   += elems_dma * output_stride;
                    rem_in_ch  -= elems_dma;
                    rem_out_ch -= elems_dma;

                    if(rem_in_ch == 0)
                    {
                        // wrap input
                        c_in++;
                        rem_in_ch = elems_in_ch;
                        p_input  = (u8*)input  + c_in*sizeof(fp16);
                    }

                    if(rem_out_ch == 0)
                    {
                        // wrap output
                        c_out++;
                        rem_out_ch = elems_out_ch;
                        p_output = (u8*)output + c_out*sizeof(fp16);
                    }
                } while (c_out < out_channels);
            }
        }

        virtual void run_(const t_MvTensorParam *mvTensorParam, const t_MvTensorOp &, const Optimization &, const Resources &res)
        {
            int outDimX = mvTensorParam->output->dimX;
            int outDimY = mvTensorParam->output->dimY;
            int outDimZ = mvTensorParam->output->dimZ;
            u32 totalDim = mvTensorParam->input->dimX * mvTensorParam->input->dimY * mvTensorParam->input->dimZ;

            if (outDimX == 0)
                outDimX = mvTensorParam->input->dimX;
            if (outDimY == 0)
                outDimY = mvTensorParam->input->dimY;
            if (outDimZ == 0)
                outDimZ = mvTensorParam->input->dimZ;

            if (outDimX == -1)
                outDimX = totalDim / (outDimY * outDimZ);
            if (outDimY == -1)
                outDimY = totalDim / (outDimX * outDimZ);
            if (outDimZ == -1)
                outDimZ = totalDim / (outDimX * outDimY);

            mvTensorParam->output->dimX = outDimX;
            mvTensorParam->output->dimY = outDimY;
            mvTensorParam->output->dimZ = outDimZ;

            const u32 inputStride = util::getMidStride(mvTensorParam->input);
            const u32 outputStride = util::getMidStride(mvTensorParam->output);

            reshape((fp16*)mvTensorParam->input->data, (fp16*)mvTensorParam->output->data,
                     mvTensorParam->input->dimX, mvTensorParam->input->dimY, mvTensorParam->input->dimZ,
                     mvTensorParam->output->dimX, mvTensorParam->output->dimY, mvTensorParam->output->dimZ,
                     inputStride, outputStride,
                     res);
        }
    };

    const ModuleHandle handles[] =
    {
        ModuleHandle().add<Reshape>(kToPlaneMajor),
        ModuleHandle().add<Reshape>(kReshape),
    };
}
