///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief Copy layer implementation.
///

#include <mvModuleHandle.h>
#include <mvTensorDebug.h>

namespace
{
    using namespace mv::tensor;

    class Tile : public Layer
    {
    private:
        static void tile(fp16      *input,
                         fp16      *output,
                         s32       in_width,
                         s32       in_height,
                         s32       in_channels,
                         s32       out_width,
                         s32       out_channels,
                         u32       *tile_params,
                         const dma::User &dmaUser)
        {
            fp16 *p_input, *p_output;
            u32 axis;
            s32 tiles;
            axis = tile_params[0];
            tiles = tile_params[1];

            const u32 INPUT_BPP = 2;
            s32 i, j, tile_count, out_steps[3];

            out_steps[0] = out_width * out_channels;
            out_steps[1] = out_channels;
            out_steps[2] = 1;

            switch (axis)
            {
                case 1:
                {
                    for(i = 0; i < in_height; i++)
                    {
                        for(j = 0; j < in_width; j++)
                        {
                            for(tile_count = 0; tile_count < tiles; tile_count ++)
                            {
                                p_input = input + i * in_width * in_channels + j * in_channels;
                                p_output = output + i * out_steps[0] + j * out_steps[1] + tile_count * in_channels;

                                dma::Task(dmaUser).start(
                                    (u8*)p_input,              // src
                                    (u8*)p_output,             // dst
                                    in_channels * INPUT_BPP,   // byte length
                                    in_channels * INPUT_BPP,   // src width
                                    INPUT_BPP,                 // dst width
                                    in_channels * INPUT_BPP,   // src stride
                                    out_steps[2] * INPUT_BPP); // dst stride
                            }
                        }
                    }
                    break;
                }

                case 2:
                {
                    for(tile_count = 0; tile_count < tiles; tile_count ++)
                    {
                        for(i = 0; i < in_height; i++)
                        {
                            for(j = 0; j < in_width; j++)
                            {
                                p_input = input + i * in_width * in_channels + j * in_channels;
                                p_output = output + i*out_steps[0] + j*out_steps[1] + in_height * out_steps[0] * tile_count;

                                dma::Task(dmaUser).start(
                                    (u8*)p_input,              // src
                                    (u8*)p_output,             // dst
                                    in_channels * INPUT_BPP,   // byte length
                                    in_channels * INPUT_BPP,   // src width
                                    INPUT_BPP,                 // dst width
                                    in_channels * INPUT_BPP,   // src stride
                                    out_steps[2] * INPUT_BPP); // dst stride
                            }
                        }
                    }
                    break;
                }

                case 3:
                {
                    for(i = 0; i < in_height; i++)
                    {
                        for(tile_count = 0; tile_count < tiles; tile_count ++)
                        {
                            for(j = 0; j < in_width; j++)
                            {
                                p_input = input + i * in_width * in_channels + j * in_channels;
                                p_output =output + i * out_steps[0] + j * out_steps[1] + tile_count * in_width * in_channels;

                                dma::Task(dmaUser).start(
                                    (u8*)p_input,              // src
                                    (u8*)p_output,             // dst
                                    in_channels * INPUT_BPP,   // byte length
                                    in_channels * INPUT_BPP,   // src width
                                    INPUT_BPP,                 // dst width
                                    in_channels * INPUT_BPP,   // src stride
                                    out_steps[2] * INPUT_BPP); // dst stride
                            }
                        }
                    }
                    break;
                }

                default: break;
            }
        }

        virtual void run_(const t_MvTensorParam *mvTensorParam, const t_MvTensorOp &op, const Optimization &, const Resources &res)
        {
            u32 axis;
            s32 tiles;
            axis = ((u32*)op.params)[0];
            tiles = ((s32*)(op.params))[1];

            /*
             * axis = 0 batch not currently supported
             */
            if((axis < 1) || (axis > 3))
            {
                mvTensorAssert(false, "Incorrect axis ");
            }
            if((axis == 1) && ((mvTensorParam->input->dimX != mvTensorParam->output->dimX) || (mvTensorParam->input->dimY != mvTensorParam->output->dimY) ||
                    ((mvTensorParam->input->dimZ * tiles) != mvTensorParam->output->dimZ)) )
            {
                mvTensorAssert(false, "Incorrect dimension");
            }
            if( (axis == 2) && ( (mvTensorParam->input->dimX != mvTensorParam->output->dimX) || ((mvTensorParam->input->dimY * tiles) != mvTensorParam->output->dimY) ||
                    (mvTensorParam->input->dimZ != mvTensorParam->output->dimZ)))
            {
                mvTensorAssert(false, "Incorrect dimension");
            }
            if((axis == 3) && (((mvTensorParam->input->dimX * tiles) != mvTensorParam->output->dimX) || (mvTensorParam->input->dimY != mvTensorParam->output->dimY) ||
                     (mvTensorParam->input->dimZ != mvTensorParam->output->dimZ)))
            {
                mvTensorAssert(false, "Incorrect dimension");
            }

            tile((fp16*)mvTensorParam->input->data,
                    (fp16*)mvTensorParam->output->data,
                     mvTensorParam->input->dimX,
                     mvTensorParam->input->dimY,
                     mvTensorParam->input->dimZ,
                     mvTensorParam->output->dimX,
                     mvTensorParam->output->dimZ,
                     (u32*)op.params,
                     res.dmaUser);
        }
    };

    const ModuleHandle handles[] =
    {
        ModuleHandle().add<Tile>(kTile),
    };
}
