///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief Crop layer implementation.
///

#include <mvModuleHandle.h>

namespace
{
    using namespace mv::tensor;

    class Crop : public Layer
    {
    private:
        virtual void run_(const t_MvTensorParam *mvTensorParam, const t_MvTensorOp &op, const Optimization &, const Resources &res)
        {
            typedef struct
            {
                s32 offset_dimX;
                s32 offset_dimY;
                s32 offset_dimZ;
            } t_CropLayerParams;

            t_CropLayerParams * crop_params = reinterpret_cast<t_CropLayerParams *>(op.params);

            s32 in_axis0, in_axis1;
            s32 offset_axis0, offset_axis1, offset_axis2;
            s32 out_axis0, out_axis1, out_axis2;

            // Make sure that the crop can be done.
            if((mvTensorParam->output->dimX + crop_params->offset_dimX > mvTensorParam->input->dimX) ||
               (mvTensorParam->output->dimY + crop_params->offset_dimY > mvTensorParam->input->dimY) ||
               (mvTensorParam->output->dimZ + crop_params->offset_dimZ > mvTensorParam->input->dimZ))
            {
                return;
            }

            bool crop_axis0, crop_axis1;

            // input->storageOrder == output->storageOrder is assumed.
            switch(mvTensorParam->input->storageOrder)
            {
            case orderYXZ:
                in_axis0 = mvTensorParam->input->dimZ;
                in_axis1 = mvTensorParam->input->dimX;

                out_axis0 = mvTensorParam->output->dimZ;
                out_axis1 = mvTensorParam->output->dimX;
                out_axis2 = mvTensorParam->output->dimY;

                offset_axis0 = crop_params->offset_dimZ;
                offset_axis1 = crop_params->offset_dimX;
                offset_axis2 = crop_params->offset_dimY;

                break;
            case orderZYX:
                in_axis0 = mvTensorParam->input->dimX;
                in_axis1 = mvTensorParam->input->dimY;

                out_axis0 = mvTensorParam->output->dimX;
                out_axis1 = mvTensorParam->output->dimY;
                out_axis2 = mvTensorParam->output->dimZ;

                offset_axis0 = crop_params->offset_dimX;
                offset_axis1 = crop_params->offset_dimY;
                offset_axis2 = crop_params->offset_dimZ;
                break;
            case orderYZX:
                in_axis0 = mvTensorParam->input->dimX;
                in_axis1 = mvTensorParam->input->dimZ;

                out_axis0 = mvTensorParam->output->dimX;
                out_axis1 = mvTensorParam->output->dimZ;
                out_axis2 = mvTensorParam->output->dimY;

                offset_axis0 = crop_params->offset_dimX;
                offset_axis1 = crop_params->offset_dimZ;
                offset_axis2 = crop_params->offset_dimY;
                break;
            case orderXYZ:
                in_axis0 = mvTensorParam->input->dimZ;
                in_axis1 = mvTensorParam->input->dimY;

                out_axis0 = mvTensorParam->output->dimZ;
                out_axis1 = mvTensorParam->output->dimY;
                out_axis2 = mvTensorParam->output->dimX;

                offset_axis0 = crop_params->offset_dimZ;
                offset_axis1 = crop_params->offset_dimY;
                offset_axis2 = crop_params->offset_dimX;
                break;
            case orderXZY:
                in_axis0 = mvTensorParam->input->dimY;
                in_axis1 = mvTensorParam->input->dimZ;

                out_axis0 = mvTensorParam->output->dimY;
                out_axis1 = mvTensorParam->output->dimZ;
                out_axis2 = mvTensorParam->output->dimX;

                offset_axis0 = crop_params->offset_dimY;
                offset_axis1 = crop_params->offset_dimZ;
                offset_axis2 = crop_params->offset_dimX;
                break;
            default:
                return;
            }

            crop_axis0 = !(in_axis0 == out_axis0);
            crop_axis1 = !(in_axis1 == out_axis1);

            s32 transfer_size;
            s32 transfer_width;
            s32 src_stride;
            s32 no_transfers = 0;

            // Cropping on the slowest growing dimension(axis2) is handled
            // via DMA src offset and transfer size/no transfers.
            if(crop_axis0 && crop_axis1)
            {
                // If we have cropping on the fastest and second fastest growing
                // dimensions we have to make out_axis2 dma transfers.
                transfer_width = out_axis0;
                src_stride = in_axis0;
                transfer_size = out_axis0 * out_axis1;
                no_transfers = out_axis2;
            }
            else
            {
                // Otherwise we need one transfer, only.
                no_transfers = 1;

                if(crop_axis0)
                {
                    // We have crop on the fastest growing dimension(axis0).
                    transfer_width = out_axis0;
                    src_stride = in_axis0;
                    transfer_size = out_axis0 * out_axis1 * out_axis2;
                }
                else if(crop_axis1)
                {
                    // We have crop on the second fastest growing dimension(axis1).
                    transfer_width = out_axis0 * out_axis1;
                    src_stride = in_axis0 * in_axis1;
                    transfer_size = out_axis0 * out_axis1 * out_axis2;
                }
                else
                {
                    // Crop only the slowest growing dimension.
                    transfer_size = out_axis0 * out_axis1 * out_axis2;
                    transfer_width = transfer_size;
                    src_stride = 0;
                }
            }

            for(s32 transfer_i = 0; transfer_i < no_transfers; ++transfer_i)
            {
                s32 offset = (offset_axis2 + transfer_i) * in_axis1 * in_axis0 +
                        offset_axis1 * in_axis0 + offset_axis0;

                u8 *src = (u8 *)mvTensorParam->input->data + offset * sizeof(fp16);
                u8 *dst = (u8 *)mvTensorParam->output->data + transfer_i * out_axis0 * out_axis1 * sizeof(fp16);

                const u32 bytes = sizeof(fp16) * transfer_size;
                dma::Task(res.dmaUser).start(src, dst, bytes, sizeof(fp16) * transfer_width, bytes, sizeof(fp16) * src_stride, bytes);
            }
        }
    };

    const ModuleHandle handles[] =
    {
        ModuleHandle().add<Crop>(kCrop),
    };
}
