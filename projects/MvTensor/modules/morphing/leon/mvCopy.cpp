///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief Copy layer implementation.
///

#include <mvModuleHandle.h>
#include <mvTensorUtil.h>

namespace
{
    using namespace mv::tensor;

    class Copy : public Layer
    {
    private:
        virtual void run_(const t_MvTensorParam *mvTensorParam, const t_MvTensorOp &, const Optimization &, const Resources &res)
        {
            const u32 inputStride = util::getMidStride(mvTensorParam->input);
            const u32 outputStride = util::getMidStride(mvTensorParam->output);

            dma::Task(res.dmaUser).start(
                (u8 *)mvTensorParam->input->data,
                (u8 *)mvTensorParam->output->data,
                sizeof(fp16) * mvTensorParam->output->dimX * mvTensorParam->output->dimY * mvTensorParam->output->dimZ,
                sizeof(fp16) * mvTensorParam->output->dimZ,
                sizeof(fp16) * mvTensorParam->output->dimZ,
                inputStride,
                outputStride);
        }
    };

    const ModuleHandle handles[] =
    {
        ModuleHandle().add<Copy>(kCopy),
    };
}
