///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
///
/// \details
///

#ifndef MV_RELAYOUT_PARAM_H_
#define MV_RELAYOUT_PARAM_H_

#include <mv_types.h>

enum t_MvRelayoutVersion {CONVOLUTION, DECONVOLUTION};

typedef struct
{
    u8  *input;
    u8  *output;
    s32 inOutBpp;
    s32 width;
    s32 height;
    s32 no_channels;
    s32 radixX;
    s32 radixY;
    s32 kernelStrideX;
    s32 kernelStrideY;
    s32 dilationX;
    s32 dilationY;
    s32 padL;
    s32 padR;
    s32 padT;
    s32 padB;
    s32 start_height;
    s32 end_height;
    u8  *cmxslice;
    u32 dmaLinkAgent;
    s32 version;
} t_RelayoutParams;

#endif // MV_RELAYOUT_PARAM_H_
