///
/// @file
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief
///

#ifndef _MVDECONV_CORE_H_
#define _MVDECONV_CORE_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include "../priv/mvDeConvParam.h"

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------

// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif

void mvDeConv(t_MvDeConvParam *p);

#ifdef __cplusplus
}
#endif

#endif //__MVDECONV_CORE_H__
