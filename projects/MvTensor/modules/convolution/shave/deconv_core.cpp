///
/// @file
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief
///

#include <algorithm>

#include <mvTensorDma.h>
#include <matmul_kernel.h>
#include "deconv_core.h"

using namespace mv::tensor;

#define align8(x) (((x) + 0x7) & ~0x7)
#define align16(x) (((x) + 0xF) & ~0xF)
#define align32(x) (((x) + 0x1F) & ~0x1F)
#define align64(x) (((x) + 0x3F) & ~0x3F)


inline fp16* get_input_pointer(u8* cmxslice){
    return (fp16*) align64((u32)cmxslice);
}

inline fp16* get_weights_pointer(u8* cmxslice, u32 paddedK){
    return (fp16*)align64((u32)(get_input_pointer(cmxslice) + paddedK));
}

inline fp16* get_output_pointer(u8* cmxslice, u32 paddedK, u32 paddedN){
    return (fp16*)align64((u32)(get_weights_pointer(cmxslice, paddedK) + paddedK * align64(paddedN)));
}

inline void copy_input(fp16* input, u8* input_buffer, u32 idx, u32 offset, u32 slice_channels, u32 channels, dma::User *dma_usr) {
//    dma::Task(*dma_usr).start(input + idx * channels + offset, input_buffer, slice_channels * sizeof(fp16));
    UNUSED(dma_usr);
    memcpy(input_buffer, input + idx * channels + offset, slice_channels * sizeof(fp16));
}

inline void copy_weights(fp16* weights, u8* weights_buffer, u32 idx, u32 offset, u32 slice_channels, u32 K, u32 N, u32 alignedN, dma::User *dma_usr) {
    dma::Task(*dma_usr).start(  weights + idx * K * N + offset,
                                weights_buffer,
                                slice_channels * N * sizeof(fp16),
                                N * sizeof(fp16), N * sizeof(fp16),
                                N * sizeof(fp16),
                                alignedN * sizeof(fp16));
}

inline void copy_output(fp16* output, u8* output_buffer, u32 idx, u32 N, u32 alignedN, u32 out_stride, u32 out_width, dma::User *dma_usr) {
//    UNUSED(dma_usr);
    dma::Task(*dma_usr).start(  output_buffer,
                                output + idx * out_width * out_stride,
                                out_width * N * sizeof(fp16),
                                N * sizeof(fp16), N * sizeof(fp16),
                                alignedN * sizeof(fp16),
                                out_stride * sizeof(fp16));
}

int find_first_weight(int x, int s)
{
    return x % s;
}

int find_next_weight(int w, int s)
{
    return w + s;
}

bool useful(int w, int x, int i, int s)
{
    if (x > (i - 1) * s)
        return (w >= x % ((i-1) * s));
    return (x - w >= 0);
}

int find_input(int x, int w, int s)
{
    return (x - w) / s;
}


void mvDeConv(t_MvDeConvParam *p) {
    dma::Config cfg = {1, p->dmaLinkAgent};
    dma::User dma_usr(cfg);

    u32 paddedK = align8(p->in_channels), paddedN = align8(p->out_channels);
    u32 alignedN = align64(paddedN);

    fp16 *pinput = get_input_pointer(p->cmxslice);
    fp16 *pweights = get_weights_pointer(p->cmxslice, p->slice_channels);
    fp16 *poutput = get_output_pointer(p->cmxslice, p->slice_channels, paddedN);

    if (p->slice_no == 1)
    {
        // make sure the padding for the weights has valid fp16
        u32 padding_K = paddedK - p->in_channels, padding_N = paddedN - p->out_channels;
        u32 padding_size;
        fp16* paddding_pweights;
        paddding_pweights = pweights + p->out_channels;
        if (padding_N)
        {
            padding_size = padding_N;
            for (u32 i = 0; i < p->in_channels; i++) {
                paddding_pweights += alignedN;
                bzero(paddding_pweights, padding_size * sizeof(fp16));
            }
        }
        paddding_pweights -= p->out_channels;
        padding_size = p->out_channels + padding_N;
        for (u32 i = 0; i < padding_K; i++) {
            bzero(paddding_pweights, padding_size * sizeof(fp16));
            paddding_pweights += alignedN;
        }
    }

    for (u32 y = p->from; y < p->to; ++y)
    {

        bzero(poutput, p->out_width * alignedN * sizeof(fp16));

        for (u32 x1 = 0; x1 < p->stride_x; ++x1)
        {
            for (u32 wy = find_first_weight(y + p->pad_y, p->stride_y);
                    wy < p->radix_y;
                    wy = find_next_weight(wy, p->stride_y))
            {
                if (useful(wy, y + p->pad_y, p->in_height, p->stride_y))
                {
                    for (u32 wx = find_first_weight(x1 + p->pad_x, p->stride_x);
                                                        wx < p->radix_x;
                                                        wx = find_next_weight(wx, p->stride_x))
                    {
                        u32 slice;
                        u32 inOffset = 0;
                        u32 wOffset = 0;
                        u32 channels_remaining = p->in_channels;
                        for (u32 sliceK = 0; sliceK < p->slice_no; sliceK++, inOffset += slice, wOffset += slice * p->out_channels, channels_remaining -= slice)
                        {
                            if (sliceK == p->slice_no - 1)
                            {
                                // pad the input
                                for (u32 i = channels_remaining; i < align8(channels_remaining); i++)
                                    pinput[i] = 0.0;
                            }
                            slice = std::min(p->slice_channels, channels_remaining);

                            // TODO: to change this when the old deconvolution is removed
                            // rotate 180 degrees in H,W plane
                            // it should be wy * p->radix_x + wx
                            u32 idx = (p->radix_y - wy - 1) * p->radix_x + (p->radix_x - wx - 1);
                            copy_weights(p->weights, (u8*)pweights, idx,
                                    wOffset, slice,
                                    p->in_channels,
                                    p->out_channels,
                                    alignedN,
                                    &dma_usr);

                            for (u32 x = x1; x < p->out_width; x += p->stride_x)
                            {
                                if (useful(wx, x + p->pad_x, p->in_width, p->stride_x))
                                {
                                    u32 input_idx = find_input(y + p->pad_y, wy, p->stride_y) * p->in_width +
                                                    find_input(x + p->pad_x, wx, p->stride_x);

                                    copy_input(p->input, (u8*)pinput, input_idx, inOffset, slice, p->in_channels, &dma_usr);

                                    if (slice <= 8)
                                    {
                                        gemm_hhhh_nnn_k8(pinput,
                                                   pweights,
                                                   poutput + x * alignedN, 1/*m*/, 8, paddedN,
                                                   64, alignedN, alignedN);
                                    }
                                    else
                                    {
                                        gemm_hhhh_nnn(pinput,
                                                   pweights,
                                                   poutput + x * alignedN, 1/*m*/, align8(slice), paddedN,
                                                   align64(p->slice_channels), alignedN, alignedN);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        copy_output(p->output, (u8*)poutput, y, p->out_channels, alignedN, p->output_stride, p->out_width, &dma_usr);
    }
}
