/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief    Transforms an input volume V in channel minor to V' such that:
///           a kxl filter operation applied on V can be performed through
///           a 1x1 filter operation on V'.
///

#include <cstring>
#include <mvTensorDma.h>
#include "relayout_core.h"

#include <mvTensor.h>

#define CMX_SIZE MVTENSOR_HEAP_DATA_SIZE
//#define DEBUG_RELAYOUT
#define MIN(a, b)       ((a)<(b)?(a):(b))
#define MAX(a, b)       ((a)>(b)?(a):(b))

using namespace mv::tensor;

void relayout_core(t_RelayoutParams *params)
{
    u8  *p_output, *p_slice_input, *p_slice_output;
    s32 real_width, real_height, slice_width, slice_real_width, rest_width;
    s32 real_radixX, real_radixY;
    s32 dst_strideX, dst_strideY;
    s32 start_j, h_slice_i, j, k;
    s32 x, nr_x, nr_kernelsX, total_nr_kernelsX;
    s32 padX, padY;

    // Using the same notation as in mdk for a better readability
    s32 DstLineWidth, DstStride;
    u8  *input        = params->input;
    u8  *output       = params->output;
    s32 bpp           = params->inOutBpp;
    s32 width         = params->width;
    s32 height        = params->height;
    s32 channels      = params->no_channels;
    s32 radixX        = params->radixX;
    s32 radixY        = params->radixY;
    s32 kernelStrideX = params->kernelStrideX;
    s32 kernelStrideY = params->kernelStrideY;
    s32 dilationX     = params->dilationX;
    s32 dilationY     = params->dilationY;
    s32 padL          = params->padL;
    s32 padR          = params->padR;
    s32 padT          = params->padT;
    s32 padB          = params->padB;
    s32 start_height  = params->start_height;
    s32 end_height    = params->end_height;
    s32 version       = params->version;

    dma::Config dmaConfig = { 1, params->dmaLinkAgent };
    dma::User   dmaUser(dmaConfig);
    dma::Task   dmaTask(dmaUser);

    real_width  = width  + padL + padR;
    real_height = height + padT + padB;
    real_radixX = (radixX - 1) * dilationX + 1;
    real_radixY = (radixY - 1) * dilationY + 1;
    dst_strideX = dst_strideY = 1;
    if(version == DECONVOLUTION)
    {
        dst_strideX   = kernelStrideX;
        dst_strideY   = kernelStrideY;
        real_width   += (kernelStrideX - 1) * (width  - 1);
        real_height  += (kernelStrideY - 1) * (height - 1);
        kernelStrideX = kernelStrideY = 1;
    }
    padX = padL;
    padY = padT;

    total_nr_kernelsX = (real_width - real_radixX + 1 + kernelStrideX - 1)/kernelStrideX;

    // Find the maximum number of kernels that can be computed within CMX_SIZE
    nr_x = (real_width*bpp*channels + radixX*total_nr_kernelsX*bpp*channels + (CMX_SIZE - real_radixX*bpp*channels) - 1) / (CMX_SIZE - real_radixX*bpp*channels);
    nr_kernelsX = total_nr_kernelsX/nr_x;
    nr_kernelsX = MAX(1, nr_kernelsX);
    slice_real_width  = real_width / nr_x;
    slice_real_width += real_radixX;

    if((slice_real_width*bpp*channels + radixX*nr_kernelsX*bpp*channels) > CMX_SIZE)
    {
#ifdef DEBUG_RELAYOUT
        printf("CMX buffer size not enough!!! %d vs %d\n", slice_real_width*bpp*channels + radixX*nr_kernelsX*bpp*channels, CMX_SIZE);
#endif
        return;
    }
    p_slice_input  = (u8*)params->cmxslice;
    p_slice_output = p_slice_input + slice_real_width*bpp*channels;
    rest_width     = width;

    // Loop over the number of kernels per slice width
    for(x = total_nr_kernelsX; x > 0; x -= nr_kernelsX)
    {
        nr_kernelsX = MIN(x, nr_kernelsX);
        p_output = output + (start_height/kernelStrideY)*total_nr_kernelsX * radixX * radixY * bpp*channels;

        // Compute the slice width that will be read from input
        slice_width = ((kernelStrideX*nr_kernelsX + radixX - 1 + dst_strideX - 1)*dilationX)/dst_strideX;
        slice_width = MIN(slice_width, rest_width);

        // Compute the number of start output duplications from one input
        start_j = 0;
        if(start_height/kernelStrideY > dilationY - 1)
        {
            start_j = MIN(start_height/kernelStrideY, (radixY - kernelStrideY));
        }
        // Loop over the number of kernels per slice height
        for(h_slice_i = start_height - padY; h_slice_i < end_height - padY; h_slice_i += kernelStrideY)
        {
            for(j = start_j; j < radixY; j++)
            {
                s32 dst_strideY_div, dst_strideY_rem;

                dst_strideY_rem = (h_slice_i + j*dilationY) % dst_strideY;
                dst_strideY_div = (h_slice_i + j*dilationY) / dst_strideY;

                // I. Prepare the first input slice buffer with 0 needed for deconvolution and convolution with padding
                memset(p_slice_input, 0, slice_real_width*bpp*channels);
                if((dst_strideY_div >= 0) && (dst_strideY_div < height))
                {
                    if(dst_strideY_rem == 0)
                    {
                        dmaTask.start((u8*)(input + dst_strideY_div*width*bpp*channels),
                                      (u8*)(p_slice_input + padX*bpp*channels),
                                      slice_width*bpp*channels, // ByteLength
                                      slice_width*bpp*channels, // SrcLineWidth
                                      bpp*channels,             // DstLineWidth
                                      slice_width*bpp*channels, // SrcStride
                                      dst_strideX*bpp*channels);// DstStride
                        dmaTask.wait();
                    }
                }
                // II. Prepare output in CMX
                DstLineWidth = radixX*nr_kernelsX*bpp*channels;
                DstStride    = radixX*nr_kernelsX*bpp*channels;
                if(dilationX > 1)
                {
                    // The intermediate CMX output is prepared by using a stride smaller than width to simulate the dilation process
                    DstLineWidth = dilationX*bpp*channels;
                    DstStride    = bpp*channels;
                }

                dmaTask.start((u8*)p_slice_input,
                              (u8*)p_slice_output,
                              radixX*dilationX*nr_kernelsX*bpp*channels, // ByteLength
                              radixX*dilationX*bpp*channels,             // SrcLineWidth
                              DstLineWidth,                              // DstLineWidth
                              kernelStrideX*bpp*channels,                // SrcStride
                              DstStride);                                // DstStride
                dmaTask.wait();

                // Write the same output corresponding to the prepared output from CMX in different location in DDR
                for(k = 0; k*kernelStrideY <= j; k ++)
                {
                    // Check if output is valid
                    if(k*kernelStrideY*dilationY < (real_height - real_radixY + 1) - (h_slice_i + padY))
                    {
                        dmaTask.start((u8*)p_slice_output,
                                      (u8*)(p_output + k*dilationY*total_nr_kernelsX*radixX*radixY*bpp*channels +
                                            (j-k*kernelStrideY)*radixX*bpp*channels),
                                      nr_kernelsX*radixX*bpp*channels, // ByteLength
                                      nr_kernelsX*radixX*bpp*channels, // SrcLineWidth
                                      radixX*bpp*channels,             // DstLineWidth
                                      nr_kernelsX*radixX*bpp*channels, // SrcStride
                                      radixX*radixY*bpp*channels);     // DstStride
                        dmaTask.wait();
                    }
                }
            }
            // Compute the next number of start output duplications from one input
            if((h_slice_i - start_height + padY) >= kernelStrideY*(dilationY - 1))
            {
                start_j = (radixY - kernelStrideY);
            }
            p_output += total_nr_kernelsX*radixX*radixY*bpp*channels;
        }
        output += nr_kernelsX * radixX*radixY * bpp*channels;
        if((kernelStrideX*nr_kernelsX - padX) > 0)
        {
            input      += ((kernelStrideX*nr_kernelsX - padX + dst_strideX - 1)/dst_strideX)*bpp*channels;
            rest_width -= ((kernelStrideX*nr_kernelsX - padX + dst_strideX - 1)/dst_strideX);
            padX = ((kernelStrideX*nr_kernelsX - padX)%dst_strideX);
        }
        else
        {
            // In case of CMX_SIZE very small this could be happened
            padX = (padX - kernelStrideX*nr_kernelsX);
        }
    }
}
