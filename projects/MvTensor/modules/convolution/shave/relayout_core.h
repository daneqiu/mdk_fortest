///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Functions declarations for input re-layout.
///

#ifndef _MV_RELAYOUT_CORE_H_
#define _MV_RELAYOUT_CORE_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include <mvRelayoutParam.h>

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------

// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif

void relayout_core(t_RelayoutParams *params);

#ifdef __cplusplus
}
#endif

#endif /* _MV_RELAYOUT_CORE_H_ */
