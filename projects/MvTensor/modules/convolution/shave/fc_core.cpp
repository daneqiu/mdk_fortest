///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     FC core implementation for SHAVE. This function
///

#include "fc_core.h"
#include "matmul_kernel.h"
#include <algorithm>
#include <mvTensorDma.h>

#define INPUT_BPP       2
#define align_up8(x)    (((x) + 0x7) & ~0x7)
#define align_down8(x)  ((x) & ~0x7)
#define align_up128(x)  (((x) + 0x7f) & ~0x7f)

// size of buffer B is 16 rows at MAX_SLICE_OUT_UNITS elements/row
#define BUFF_B_ELEMS         16 * MAX_SLICE_OUT_UNITS
#define MAX_SLICE_IN_UNITS   1024

void mvFC(t_MvFCParam *p)
{
    u32 sliceOutUnits = p->sliceOutUnits;
    u32 sliceInUnits, sliceInUnitsMax, sliceOutUnitsStride;
    u32 sliceInUnits_start = 0;
    u32 inUnits = p->inUnits;
    u32 outUnits = p->outUnits;
    u32 i;
    u8* inAddressA = (u8*) ((u32) p->inputA);
    u8* inAddressB = (u8*) ((u32) p->inputB);
    u8* outAddress = (u8*) ((u32) p->output);
    u8 *inputBufferA[2], *inputBufferB[2], *outBufferC, buf_idx;
    u8 *inputBufferA_proc, *inputBufferB_proc;
    u8 *inputBufferA_load, *inputBufferB_load;

    using namespace mv::tensor;
    dma::Config dmaConfig = { 1, p->dmaLinkAgent };
    dma::User dmaUser(dmaConfig);

    // ! The size of cmxslice must be at least
    // ! 127 + (2*MAX_SLICE_IN_UNITS + 2*BUFF_B_ELEMS  + MAX_SLICE_OUT_UNITS)*INPUT_BPP bytes = 71807 bytes

    // set buffers to point to locations relative to cmxslice
    // align the first buffer to 128 bytes boundary;
    // the following buffers will also be aligned, because the increment is a multiple of 128
    inputBufferA[0] = (u8*)(align_up128((u32)p->cmxslice));
    inputBufferA[1] = inputBufferA[0] + MAX_SLICE_IN_UNITS * INPUT_BPP;
    inputBufferB[0] = inputBufferA[1] + MAX_SLICE_IN_UNITS * INPUT_BPP;
    inputBufferB[1] = inputBufferB[0] + BUFF_B_ELEMS * INPUT_BPP;
    outBufferC         = inputBufferB[1] + BUFF_B_ELEMS * INPUT_BPP;

    dma::Task(dmaUser).start(outAddress, outBufferC, 1 * 1 * sliceOutUnits * INPUT_BPP);

    sliceOutUnits      = std::min(sliceOutUnits, (u32)MAX_SLICE_OUT_UNITS);
    sliceOutUnitsStride = sliceOutUnits;
    if(sliceOutUnits % 8)
        sliceOutUnitsStride = sliceOutUnits + 8 - (sliceOutUnits % 8);

    // sliceInUnits will always be >= BUFF_B_ELEMS * MAX_SLICE_OUT_UNITS (= 16)
    sliceInUnitsMax    = align_down8(BUFF_B_ELEMS / sliceOutUnitsStride);
    // make sliceInUnitsMax a multiple of 8
    sliceInUnitsMax    = std::min(sliceInUnitsMax, align_up8(inUnits));
    sliceInUnitsMax    = std::min(sliceInUnitsMax, (u32)MAX_SLICE_IN_UNITS);

    sliceInUnits = std::min(sliceInUnitsMax, inUnits - sliceInUnits_start);
    sliceInUnits_start = 0;

    buf_idx = 0;
    inputBufferA_proc = inputBufferA[buf_idx];
    inputBufferB_proc = inputBufferB[buf_idx];

    {
        dma::Task dmaTask(dmaUser);
        dmaTask.start(
            inAddressB + sliceInUnits_start * outUnits * INPUT_BPP,
            inputBufferB_proc,
            sliceInUnits * sliceOutUnits * INPUT_BPP,   // byte length
            sliceOutUnits * INPUT_BPP,                  // src width
            sliceOutUnits * INPUT_BPP,                  // dst width
            outUnits * INPUT_BPP,                       // src stride
            sliceOutUnitsStride * INPUT_BPP);           // dst stride

        memcpy(inputBufferA_proc, inAddressA + sliceInUnits_start * INPUT_BPP, sliceInUnits * INPUT_BPP);
        memset(inputBufferA_proc + sliceInUnits*INPUT_BPP, 0, (sliceInUnitsMax-sliceInUnits)*INPUT_BPP);
        memset(inputBufferB_proc + sliceInUnits*sliceOutUnitsStride*INPUT_BPP, 0, 
               (sliceInUnitsMax-sliceInUnits)*sliceOutUnitsStride*INPUT_BPP);
    }

    sliceInUnits_start += sliceInUnits;

    while (sliceInUnits_start < inUnits)
    {
        // compute sliceInUnits for the next iteration
        sliceInUnits = std::min(sliceInUnitsMax, inUnits - sliceInUnits_start);

        // fetch the inputA and inputB for the next iteration (over DMA)
        buf_idx = 1 - buf_idx;
        inputBufferA_load = inputBufferA[buf_idx];
        inputBufferB_load = inputBufferB[buf_idx];

        dma::Task dmaTask(dmaUser);
        dmaTask.start(
            inAddressB + sliceInUnits_start * outUnits * INPUT_BPP,
            inputBufferB_load,
            sliceInUnits * sliceOutUnits * INPUT_BPP,   // byte length
            sliceOutUnits * INPUT_BPP,                  // src width
            sliceOutUnits * INPUT_BPP,                  // dst width
            outUnits * INPUT_BPP,                       // src stride
            sliceOutUnitsStride * INPUT_BPP);           // dst stride

        // memcpy(s) and memset(s) go in parallel with the DMA transfer, 
        // but they write in areas that are not overlapping with the destination of the DMA task
        memset(inputBufferB_load + sliceInUnits*sliceOutUnitsStride*INPUT_BPP,
               0,
               (sliceInUnitsMax-sliceInUnits)*sliceOutUnitsStride*INPUT_BPP);
        memcpy(inputBufferA_load, inAddressA + sliceInUnits_start * INPUT_BPP, sliceInUnits * INPUT_BPP);
        memset(inputBufferA_load + sliceInUnits*INPUT_BPP,  0, (sliceInUnitsMax-sliceInUnits)*INPUT_BPP);

        gemm_hhhh_nnn((half*)inputBufferA_proc, (half*)inputBufferB_proc, (half*)outBufferC, 1, sliceInUnitsMax, sliceOutUnitsStride, sliceInUnitsMax, sliceOutUnitsStride, sliceOutUnitsStride);

        // set the processing buffers for the next iteration
        inputBufferA_proc = inputBufferA_load;
        inputBufferB_proc = inputBufferB_load;
        sliceInUnits_start += sliceInUnits;
    };

    // the previous while breaks at the end of the block, before gemm_hhhh
    // is applied for the last transfered slice, therefore we process the last slice out of the loop
    if (sliceInUnits_start == inUnits)
    {
        gemm_hhhh_nnn((half*)inputBufferA_proc, (half*)inputBufferB_proc, (half*)outBufferC, 1, sliceInUnitsMax, sliceOutUnitsStride, sliceInUnitsMax, sliceOutUnitsStride, sliceOutUnitsStride);
    }

    dma::Task(dmaUser).start(outBufferC, outAddress, sliceOutUnits * INPUT_BPP);
}
