/// @file
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Depthwise Convolution
///

#include <mvTensorDma.h>
#include <mvTensorInternal.h>
#include <mvTensorUtil.h>
#include <algorithm>

#include <moviVectorFunctions.h>

#include "depthConv_core.h"
#include "mprintf.h"

//#define DEBUG

#ifdef DEBUG
#define Printf(...) MPRINTF(__VA_ARGS__)
#else
#define Printf(...)
#endif

using namespace mv::tensor;

namespace
{
    void mult(const fp16* const *input, const fp16 *weights, fp16* output, int, int, int c, int slice)
    {
        int input_offset = 0, weights_offset = 0;
        while (input_offset < (slice >> 3))
        {
            half8 out = 0;

            out = __builtin_shave_vau_add_f16_rr (out, __builtin_shave_vau_mul_f16_rr(*((half8*)input[0] + input_offset), *((half8*)weights + weights_offset)));
            out = __builtin_shave_vau_add_f16_rr (out, __builtin_shave_vau_mul_f16_rr(*((half8*)input[1] + input_offset), *((half8*)(weights + c) + weights_offset)));
            out = __builtin_shave_vau_add_f16_rr (out, __builtin_shave_vau_mul_f16_rr(*((half8*)input[2] + input_offset), *((half8*)(weights + (c << 1)) + weights_offset)));
            out = __builtin_shave_vau_add_f16_rr (out, __builtin_shave_vau_mul_f16_rr(*((half8*)input[3] + input_offset), *((half8*)(weights + c * 3) + weights_offset)));
            out = __builtin_shave_vau_add_f16_rr (out, __builtin_shave_vau_mul_f16_rr(*((half8*)input[4] + input_offset), *((half8*)(weights + (c << 2)) + weights_offset)));
            out = __builtin_shave_vau_add_f16_rr (out, __builtin_shave_vau_mul_f16_rr(*((half8*)input[5] + input_offset), *((half8*)(weights + c * 5) + weights_offset)));
            out = __builtin_shave_vau_add_f16_rr (out, __builtin_shave_vau_mul_f16_rr(*((half8*)input[6] + input_offset), *((half8*)(weights + c * 6) + weights_offset)));
            out = __builtin_shave_vau_add_f16_rr (out, __builtin_shave_vau_mul_f16_rr(*((half8*)input[7] + input_offset), *((half8*)(weights + c * 7) + weights_offset)));
            out = __builtin_shave_vau_add_f16_rr (out, __builtin_shave_vau_mul_f16_rr(*((half8*)input[8] + input_offset), *((half8*)(weights + (c << 3)) + weights_offset)));

            *((half8*)output + input_offset) = out;
            ++input_offset;
            weights_offset = (weights_offset + 1) % (c >> 3);
        }
    }

    class Indices
    {
    public:
        Indices(int *indices, int h, int w, int fh, int fw, int top, int /*bottom*/, int left, int /*right*/) :
            indices_(indices),
            h_(h),
            w_(w),
            fh_(fh),
            fw_(fw),
            top_(top),
            left_(left)
        {
        }

        void move_padded(int y, int x)
        {
            for (int i = 0, index = 0; i < fh_; ++i)
            {
                for (int j = 0; j < fw_; ++j, ++index)
                {
                    indices_[index] =
                        (y + i < top_) ? -1 :
                        (x + j < left_) ? -1 :
                        (y + i >= top_ + h_) ? -1 :
                        (x + j >= left_ + w_) ? -1 :
                        (y + i - top_) * w_ + (x + j - left_);

//                    Printf("%d, ", indices_[index]);
                }
//                Printf("\n");
            }
        }

        int *begin(int line) { return indices_ + line * fw_; }
        int *end(int line) { return indices_ + (line + 1) * fw_; }

        const int *begin(int line) const { return indices_ + line * fw_; }
        const int *end(int line) const { return indices_ + (line + 1) * fw_; }

    private:
        int *indices_;
        int h_, w_;
        int fh_, fw_;
        int top_;
        int left_;
    };

    struct Slice
    {
        Slice(void *start, int size) :
            start_(reinterpret_cast<unsigned char *>(start)),
            size_(size)
        {
        }

        template <typename T, int Alignment>
        T *allocate(int size)
        {
            unsigned char *aligned = reinterpret_cast<unsigned char *>(mv::tensor::util::align_up<Alignment>(reinterpret_cast<unsigned int>(start_)));
            size *= sizeof(T);

            if (aligned - start_ + size <= size_)
            {
                size_ -= aligned - start_ + size;
                start_ = aligned + size;
            }
            else
                aligned = nullptr;

            return reinterpret_cast<T *>(aligned);
        }

    private:
        unsigned char *start_;
        int size_;
    };

    void mvDepthConvFullLine(t_MvDepthConvParam *p, bool local_weights)
    {
        const int kernel_size = p->filter_height * p->filter_width;
        int indices[kernel_size];
        Indices lut(indices, p->in_height, p->in_width, p->filter_height, p->filter_width, p->pad_t, p->pad_b, p->pad_l, p->pad_r);

        using namespace mv::tensor;
        dma::Config dmaConfig = { 1, p->dmaLinkAgent };
        dma::User dmaUser(dmaConfig);
        dma::Task dmaTask(dmaUser);

        Slice fullCmx(p->cmxslice, MVTENSOR_HEAP_DATA_SIZE);
        fp16 *outputLine = fullCmx.allocate<fp16, 16>(p->out_width * p->in_channels);

        fp16 *weights = p->weights;
        if (local_weights)
        {
            weights = fullCmx.allocate<fp16, 16>(kernel_size * p->in_channels) ;
            dmaTask.start(p->weights, weights, kernel_size * p->in_channels * sizeof(fp16));
        }

        // keep an extra entry for a prefetched input
        int inputTracker[p->filter_height + 1];
        fp16 *inputLines[p->filter_height + 1];
        for (u32 i = 0; i < p->filter_height; ++i)
        {
            inputTracker[i] = -1;
            inputLines[i] = fullCmx.allocate<fp16, 16>(p->in_width * p->in_channels);
        }

        fp16 *nullPixel = fullCmx.allocate<fp16, 16>(p->in_channels);
        mvTensorAssert(nullPixel != nullptr, "Could not allocate input buffer.");
        for (u32 i = 0; i < p->in_channels; ++i)
            nullPixel[i] = 0;

        inputTracker[p->filter_height] = -1;
        inputLines[p->filter_height] = fullCmx.allocate<fp16, 16>(p->in_width * p->in_channels);
        // this may fail if there's not enough memory. don't worry about it, go on with nullptr
        const bool prefetch = inputLines[p->filter_width] != nullptr;

        for (u32 current = p->from, width = std::min(p->out_width - p->from % p->out_width, p->to - p->from);
            current < p->to;
            current += width, width = std::min(p->out_width, p->to - current))
        {
            Printf("working between [%d, %d) output pixels\n", current, current + width);

            // these parameters are kernel positions in the padded input matrix
            lut.move_padded(current / p->out_width * p->stride_h, current % p->out_width * p->stride_w);

            // fetch the cache
            for (u32 i = 0; i < p->filter_height; ++i)
            {
                int required = -1;

                for (int *it = lut.begin(i); it != lut.end(i); ++it)
                    if (*it >= 0)
                    {
                        required = *it - *it % p->in_width;
                        break;
                    }

                if (required >= 0)
                {
                    // check the existing ones, including a possibly prefetched one (j <= p->filter_height)
                    for (u32 j = i + 1; j <= p->filter_height; ++j)
                        if (inputTracker[j] == required)
                        {
                            std::swap(inputTracker[i], inputTracker[j]);
                            std::swap(inputLines[i], inputLines[j]);
                            required = -1;
                            break;
                        }

                    if (required >= 0)
                    {
                        inputTracker[i] = required;
                        Printf("Starting synchronous DMA from input index %d\n", inputTracker[i]);
                        dmaTask.start(p->input + inputTracker[i] * p->in_channels, inputLines[i], p->in_width * p->in_channels * sizeof(fp16));
                    }
                }
            }

            // make sure all the input has arrived
            dmaTask.wait();

            if (prefetch && inputTracker[p->filter_height - 1] >= 0)
            {
                // estimate the next line and prefetch it
                inputTracker[p->filter_height] = inputTracker[p->filter_height - 1] + p->in_width;
                dmaTask.start(p->input + inputTracker[p->filter_height] * p->in_channels, inputLines[p->filter_height], p->in_width * p->in_channels * sizeof(fp16));
            }

            for (u32 i = 0, pixel = current; i < width; ++i, ++pixel)
            {
                // these parameters are kernel positions in the padded input matrix
                lut.move_padded(pixel / p->out_width * p->stride_h, pixel % p->out_width * p->stride_w);

                fp16 *pixel_data[kernel_size];
                for (u32 j = 0, b = 0; j < p->filter_height; ++j)
                    for (u32 k = 0; k < p->filter_width; ++k, ++b)
                    {
                        int o = *(lut.begin(j) + k);
                        pixel_data[b] = (o < 0) ? nullPixel : inputLines[j] + (o - inputTracker[j]) * p->in_channels;
                    }

                mult(pixel_data, weights, outputLine + (pixel - current) * p->in_channels, p->filter_height, p->filter_width, p->in_channels, p->in_channels);
            }

            dmaTask.start(outputLine, p->output + current * p->in_channels, width * p->in_channels * sizeof(fp16));
        }
    }

    void mvDepthConvDDRInput(t_MvDepthConvParam *p)
    {
        //mvTensorAssert(false, "DepthConv by pixel not yet implemented.");
        const int kernel_size = p->filter_height * p->filter_width;
        int indices[kernel_size];
        Indices lut(indices, p->in_height, p->in_width, p->filter_height, p->filter_width, p->pad_t, p->pad_b, p->pad_l, p->pad_r);

        using namespace mv::tensor;
        dma::Config dmaConfig = { 1, p->dmaLinkAgent };
        dma::User dmaUser(dmaConfig);
        dma::Task dmaTask(dmaUser);

        Slice fullCmx(p->cmxslice, MVTENSOR_HEAP_DATA_SIZE);
        fp16 *outputLine = fullCmx.allocate<fp16, 16>(p->out_width * p->in_channels);

        fp16 *weights = p->weights;
        weights = fullCmx.allocate<fp16, 16>(kernel_size * p->in_channels) ;
        dmaTask.start(p->weights, weights, kernel_size * p->in_channels * sizeof(fp16));

        // keep an extra entry for a prefetched input
        int inputTracker[p->filter_height + 1];
        fp16 *inputLines[p->filter_height + 1];
        for (u32 i = 0; i < p->filter_height; ++i)
        {
            inputTracker[i] = -1;
            //inputLines[i] = fullCmx.allocate<fp16, 16>(p->in_width * p->in_channels);
            inputLines[i] = p->input + i * p->in_width * p->in_channels;
        }

        fp16 *nullPixel = fullCmx.allocate<fp16, 16>(p->in_channels);
        mvTensorAssert(nullPixel != nullptr, "Could not allocate input buffer.");
        for (u32 i = 0; i < p->in_channels; ++i)
            nullPixel[i] = 0;

        inputTracker[p->filter_height] = -1;
        inputLines[p->filter_height] = fullCmx.allocate<fp16, 16>(p->in_width * p->in_channels);
        // this may fail if there's not enough memory. don't worry about it, go on with nullptr
        const bool prefetch = inputLines[p->filter_height] != nullptr;

        for (u32 current = p->from, width = std::min(p->out_width - p->from % p->out_width, p->to - p->from);
            current < p->to;
            current += width, width = std::min(p->out_width, p->to - current))
        {
            Printf("working between [%d, %d) output pixels\n", current, current + width);

            // these parameters are kernel positions in the padded input matrix
            lut.move_padded(current / p->out_width * p->stride_h, current % p->out_width * p->stride_w);
            // fetch the cache
            for (u32 i = 0; i < p->filter_height; ++i)
            {
                int required = -1;

                for (int *it = lut.begin(i); it != lut.end(i); ++it)
                    if (*it >= 0)
                    {
                        required = *it - *it % p->in_width;
                        break;
                    }

                if (required >= 0)
                {
                    // check the existing ones, including a possibly prefetched one (j <= p->filter_height)
                    for (u32 j = i + 1; j <= p->filter_height; ++j)
                        if (inputTracker[j] == required)
                        {
                            std::swap(inputTracker[i], inputTracker[j]);
                            std::swap(inputLines[i], inputLines[j]);
                            required = -1;
                            break;
                        }

                    if (required >= 0)
                    {
                        inputTracker[i] = required;
                        Printf("Starting synchronous DMA from input index %d to line %d\n", inputTracker[i], i);
                        //dmaTask.start(p->input + inputTracker[i] * p->in_channels, inputLines[i], p->in_width * p->in_channels * sizeof(fp16));
                        inputLines[i] = p->input + inputTracker[i] * p->in_channels;
                    }
                }
            }

            // make sure all the input has arrived
            dmaTask.wait();

            if (prefetch && inputTracker[p->filter_height - 1] >= 0)
            {
                // estimate the next line and prefetch it
                inputTracker[p->filter_height] = inputTracker[p->filter_height - 1] + p->in_width;
                //dmaTask.start(p->input + inputTracker[p->filter_height] * p->in_channels, inputLines[p->filter_height], p->in_width * p->in_channels * sizeof(fp16));
                inputLines[p->filter_height] = p->input + inputTracker[p->filter_height] * p->in_channels;
            }
            for (u32 i = 0, pixel = current; i < width; ++i, ++pixel)
            {
                // these parameters are kernel positions in the padded input matrix
                lut.move_padded(pixel / p->out_width * p->stride_h, pixel % p->out_width * p->stride_w);

                fp16 *pixel_data[kernel_size];
                for (u32 j = 0, b = 0; j < p->filter_height; ++j)
                    for (u32 k = 0; k < p->filter_width; ++k, ++b)
                    {
                        int o = *(lut.begin(j) + k);
                        pixel_data[b] = (o < 0) ? nullPixel : inputLines[j] + (o - inputTracker[j]) * p->in_channels;
                    }
                mult(pixel_data, weights, outputLine + (pixel - current) * p->in_channels, p->filter_height, p->filter_width, p->in_channels, p->in_channels);
            }
            // we make some unit test here
            //inputLines[0][0] = p->input[0];

            dmaTask.start(outputLine, p->output + current * p->in_channels, width * p->in_channels * sizeof(fp16));
        }
    }

    void mvDepthConvByPixel(t_MvDepthConvParam *)
    {
        mvTensorAssert(false, "DepthConv by pixel not yet implemented.");
    }
}

void mvDepthConv(t_MvDepthConvParam *p)
{
    const int nullPixel = 1;
    const int outputPixel = 1;
    const int local_weights = p->filter_height * p->filter_width;
    const int memoryForPixel = p->in_channels * (p->filter_height * p->filter_width + nullPixel + outputPixel) * sizeof(fp16);
    const int memoryForDDRInput = p->in_channels * (nullPixel + local_weights + p->out_width) * sizeof(fp16);
    const int memoryForFullLineWeights = p->in_channels * (p->in_width * p->filter_height + nullPixel + local_weights + p->out_width) * sizeof(fp16);
    const int memoryForFullLine = p->in_channels * (p->in_width * p->filter_height + nullPixel + p->out_width) * sizeof(fp16);
    const int availableMemory = MVTENSOR_HEAP_DATA_SIZE;
    Printf("%d %d %d %d\n", availableMemory, memoryForFullLineWeights, memoryForFullLine, memoryForPixel);

    if (memoryForFullLineWeights <= availableMemory)
        mvDepthConvFullLine(p, true);
    else if (memoryForFullLine <= availableMemory)
        mvDepthConvFullLine(p, false);
    else if (memoryForDDRInput <= availableMemory)
        mvDepthConvDDRInput(p);
    else if (memoryForPixel <= availableMemory)
        mvDepthConvByPixel(p);
    else
        mvTensorAssert(false, "Not enough memory for depthwise convolution.");
}
