///
/// @file
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef _MVDEPTHDECONV_CORE_H_
#define _MVDEPTHDECONV_CORE_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include "../priv/mvDepthConvParam.h"

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------

// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif

void mvDepthDeConv(t_MvDepthConvParam *p);

#ifdef __cplusplus
}
#endif

#endif //__MVDEPTHCONV_CORE_H__
