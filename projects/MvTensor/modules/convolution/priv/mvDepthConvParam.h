///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
///
/// \details
///


#ifndef _MV_DEPTHCONVPARAM_H_
#define _MV_DEPTHCONVPARAM_H_

#include <mv_types.h>

#ifdef __MOVICOMPILE__
    #include <moviVectorTypes.h>
#else
    typedef fp16 half;
#endif

/// mvDepthConv global parameters structure
typedef struct
{
    half* input;
    u32 in_height;
    u32 in_width;
    u32 in_channels;
    half* weights;
    u32 filter_height;
    u32 filter_width;
    u32 stride_h;
    u32 stride_w;
    half* output;
    u32 out_height;
    u32 out_width;
    u32 from;
    u32 to;
    u32 pad_t;
    u32 pad_b;
    u32 pad_l;
    u32 pad_r;
    u32 dmaLinkAgent;
    u8* cmxslice;
} t_MvDepthConvParam;

#endif // _MV_DEPTHCONVPARAM_H_
