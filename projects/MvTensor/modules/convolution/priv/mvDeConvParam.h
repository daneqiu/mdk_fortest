///
/// \file
/// \copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
///
/// \details
///

#ifndef _MV_DECONVPARAM_H_
#define _MV_DECONVPARAM_H_
#include <mv_types.h>

#ifdef __MOVICOMPILE__
    #include <moviVectorTypes.h>
#else
    typedef fp16 half;
#endif

/// mvDeConv global parameters structure
typedef struct
{
    half* input;
    u32 slice_no;
    u32 slice_channels;
    u32 in_height;
    u32 in_width;
    u32 in_channels;
    u32 out_width;
    u32 out_channels;
    u32 output_stride;
    u32 radix_x;
    u32 radix_y;
    u32 stride_x;
    u32 stride_y;
    u32 pad_x;
    u32 pad_y;
    u32 from;
    u32 to;
    half* weights;
    half* output;
    u8* cmxslice;
    u32 dmaLinkAgent;
} t_MvDeConvParam;

#endif  //_MV_DECONVPARAM_H_
