BASE_PATH := $(MVTENSOR_MODULES_PATH)/convolution

# Dependencies

MVMODULE_NEED_MATMUL := 1

# Flags

# Headers

MVMODULE_LEON_HEADER_PATHS := $(BASE_PATH)/pub
MVMODULE_SHAVE_HEADER_PATHS := $(BASE_PATH)/pub

# do not add $(BASE_PATH)/priv here
# it should be used only by internal sources, like this
# include "../priv/header_name.h"

# do not add $(BASE_PATH)/leon here
# it should be used only by sources in the same folder, like this
# include "header_name.h"

# do not add $(BASE_PATH)/shave here
# it should be used only by sources in the same folder, like this
# include "header_name.h"

# Sources

MVMODULE_LEON_COMPONENT_PATHS := $(BASE_PATH)/leon
MVMODULE_SHAVE_COMPONENT_PATHS := $(BASE_PATH)/shave

# Linker options

# Shave entry points

MVMODULE_ENTRY_POINTS := \
	-u mvConv7x7s2 \
	-u mvDepthConv \
	-u mvDepthDeConv \
	-u mvFC \
	-u mvSpatialConv \
	-u relayout_core \
	-u reluNegSlopeFp16 \
	-u mvDeConv

# Enable the module

include $(MVTENSOR_MODULES_PATH)/enable_module.mk
