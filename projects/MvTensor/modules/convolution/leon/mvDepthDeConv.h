///
/// @file
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef _DEPTHDECONV_H_
#define _DEPTHDECONV_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include <mv_types.h>
#include <mvTensor.h>
#include <mvTensorResources.h>

using namespace mv::tensor;

#ifdef __cplusplus
extern "C"
{
#endif

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------
// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------

void depthDeConv(fp16* input, u32 in_height, u32 in_width, u32 in_channels,
               fp16* weights, u32 filter_height, u32 filter_width, u32 stride_h, u32 stride_w,
               fp16* output, u32 out_height, u32 out_width, u32 out_channels,
               t_MvTensorPaddStyle pad_style, u32 padX, u32 padY,
               t_MvTensorMyriadResources *myriadResources, const Resources &res);

#ifdef __cplusplus
}
#endif

#endif//_DEPTHDECONV_H_
