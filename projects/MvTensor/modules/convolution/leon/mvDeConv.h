///
/// @file
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef _DECONV_H_
#define _DECONV_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include <mv_types.h>
#include <mvTensor.h>

#ifdef __cplusplus
extern "C"
{
#endif

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------
// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------

void deconv(fp16* input, u32 in_height, u32 in_width, u32 in_channels,
			fp16* output, u32 out_height, u32 out_width, u32 out_channels, u32 out_stride,
			fp16* weights,
            u32 radix_x, u32 radix_y,
            u32 stride_x, u32 stride_y,
            u32 pad_x, u32 pad_y,
            t_MvTensorMyriadResources *myriadResources);

#ifdef __cplusplus
}
#endif

#endif  //_DECONV_H_
