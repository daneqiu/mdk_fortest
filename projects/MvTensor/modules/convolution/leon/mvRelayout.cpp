///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief Leon wrappers for CNN post operations.
///


#include "mvRelayout.h"
#include <mvTensorInternal.h>

#define MAX(a, b)       ((a)>(b)?(a):(b))
#define MIN(a, b)       ((a)<(b)?(a):(b))

void relayout(fp16 *input,
              fp16 *output,
              s32  width,
              s32  height,
              s32  channels,
              s32  radixX,
              s32  radixY,
              s32  kernelStrideX,
              s32  kernelStrideY,
              s32  dilation,
              s32  padL,
              s32  padR,
              s32  padT,
              s32  padB,
              t_MvRelayoutVersion version,
              t_MvTensorMyriadResources *myriadResources)
{
    s32 real_width, real_height;
    s32 dilationX, dilationY;
    s32 real_radixY, nr_kernelsY;
    s32 bpp;
    s32 first_shave, last_shave;
    s32 shave_idx, shave_nr, shaves_used;
    s32 start_height, end_height, shave_height, extra_height;
    s32 pad_along_height, pad_along_width;
    u32 outDimX, outDimY;

    // NOTE: The layer supports support any size of input/output
    bpp = sizeof(fp16);
    // NOTE: The layer supports different dilation on X and Y
    dilationX = dilationY = dilation;

    real_width  = width  + padL + padR;
    real_height = height + padT + padB;
    real_radixY = (radixY - 1) * dilationY + 1;
    if(version == DECONVOLUTION)
    {
        real_width  += (kernelStrideX - 1) * (width  - 1);
        real_height += (kernelStrideY - 1) * (height - 1);
        nr_kernelsY  = (real_height - real_radixY + 1);
    }
    else
    {
        nr_kernelsY = (real_height - real_radixY + 1 + kernelStrideY - 1)/kernelStrideY;
    }

    first_shave  = myriadResources->firstShave;
    last_shave   = myriadResources->lastShave;
    shaves_used  = last_shave - first_shave + 1;
    shaves_used  = MIN(nr_kernelsY, shaves_used);
    last_shave   = first_shave + shaves_used - 1;
    shave_height = nr_kernelsY / shaves_used;
    extra_height = nr_kernelsY - (shave_height * shaves_used);

    end_height = 0;
    for(shave_idx = 0; shave_idx < shaves_used; shave_idx++)
    {
        start_height = end_height;
        end_height  += shave_height * kernelStrideY;
        if(extra_height > 0)
        {
          end_height += kernelStrideY;
        }
        extra_height--;
        end_height = MIN((real_height - real_radixY + 1), end_height);
        shave_nr   = first_shave + shave_idx;

        t_RelayoutParams *relayoutParams = useShaveParam<t_RelayoutParams>(shave_nr);
        relayoutParams->input         = (u8 *)input;
        relayoutParams->output        = (u8 *)output;
        relayoutParams->inOutBpp      = bpp;
        relayoutParams->width         = width;
        relayoutParams->height        = height;
        relayoutParams->no_channels   = channels;
        relayoutParams->radixX        = radixX;
        relayoutParams->radixY        = radixY;
        relayoutParams->kernelStrideX = kernelStrideX;
        relayoutParams->kernelStrideY = kernelStrideY;
        relayoutParams->dilationX     = dilationX;
        relayoutParams->dilationY     = dilationY;
        relayoutParams->padL          = padL;
        relayoutParams->padR          = padR;
        relayoutParams->padT          = padT;
        relayoutParams->padB          = padB;
        relayoutParams->start_height  = start_height;
        relayoutParams->end_height    = end_height;
        relayoutParams->cmxslice      = getCMXSliceDataSection(shave_nr);
        relayoutParams->dmaLinkAgent  = myriadResources->dmaLinkAgent;
        relayoutParams->version       = version;

        u32 readBackAddr = (u32)(&(relayoutParams->version));
        GET_REG_WORD_VAL(readBackAddr);

        MODULE_ENTRY_DECL(relayout_core);
        startShave(shave_nr, (u32)&MODULE_ENTRY(relayout_core), (u32)relayoutParams);
    }
    waitShaves(first_shave, last_shave);
}
