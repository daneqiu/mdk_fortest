///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

#include "../priv/mvConv7x7s2Param.h"
#include <mvTensorInternal.h>
#include "mvConv7x7s2.h"

void conv7x7s2(fp16* input, u32 in_height, u32 in_width,
               u32 in_channels, u32 out_channels,
               fp16* weights, fp16* output,
               t_MvTensorMyriadResources *myriadResources) {
    u32 shaves_no = (myriadResources->lastShave - myriadResources->firstShave + 1);
    u32 out_height = in_height / 2;
    // u32 out_width = in_width / 2;
    // How to Parallelize? Ans: each SHAVE is assigned
    // a range of output rows (start_out_row + out_rows)
    u32 start_out_row = 0;
    u32 out_rows = out_height / shaves_no;
    u32 remaining_rows = out_height - out_rows * shaves_no;
    s32 i;
    for (i = myriadResources->firstShave; i <= myriadResources->lastShave; i++) {
        out_rows =  out_height / shaves_no;
        if (remaining_rows) {
          remaining_rows--;
          out_rows++;
        }

        t_MvConv7x7s2Param *conv7x7s2Param = useShaveParam<t_MvConv7x7s2Param>(i);
        conv7x7s2Param->input = input;
        conv7x7s2Param->in_height = in_height;
        conv7x7s2Param->in_width = in_width;
        conv7x7s2Param->in_channels = in_channels;
        conv7x7s2Param->out_channels = out_channels;
        conv7x7s2Param->start_out_row = start_out_row;
        conv7x7s2Param->out_rows = out_rows;
        conv7x7s2Param->weights = weights;
        conv7x7s2Param->output = output;
        conv7x7s2Param->dmaLinkAgent = myriadResources->dmaLinkAgent;
        conv7x7s2Param->cmxslice = getCMXSliceDataSection(i);

        u32 readBackAddr = (u32)(&(conv7x7s2Param->cmxslice));
        GET_REG_WORD_VAL(readBackAddr);

        MODULE_ENTRY_DECL(mvConv7x7s2);
        startShave(i, (u32)&MODULE_ENTRY(mvConv7x7s2), (u32)conv7x7s2Param);

        start_out_row += out_rows;
    }

    waitShaves(myriadResources->firstShave, myriadResources->lastShave);
}
