///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef _MV_RELAYOUT_H_
#define _MV_RELAYOUT_H_

#include <mvTensor.h>
#include <mvRelayoutParam.h>
#include "mvTensorCacheController.h"

using namespace mv::tensor;

// Performs better than v1 for for nonstrided operations (stride == 1 in both dimension)
// and large number of input channels.
void relayout(fp16 *input, fp16 *output, s32 width, s32 height,
              s32 channels, s32 radixX, s32 radixY,
              s32 kernelStrideX, s32 kernelStrideY, s32 dilation,
              s32 padL, s32 padR, s32 padT, s32 padB, t_MvRelayoutVersion version,
              t_MvTensorMyriadResources *myriadResources);

#endif /* _MV_RELAYOUT_H_ */
