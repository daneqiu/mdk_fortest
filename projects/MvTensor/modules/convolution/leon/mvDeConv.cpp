///
/// @file
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

#include <stdio.h>

#include "../priv/mvDeConvParam.h"
#include <mvTensorInternal.h>
#include "mvDeConv.h"

#define align8(x) (((x) + 0x7) & ~0x7)
#define align64(x) (((x) + 0x3F) & ~0x3F)

inline u32 req_cmx (u32 address, u32 paddedK, u32 paddedN, u32 out_width) {
    return ( align64(address) - address                           + \
             align64(paddedK)                       /* input   */ + \
             paddedK * align64(paddedN)             /* weights */ + \
             out_width * align64(paddedN)           /* output  */   \
           ) * sizeof (fp16);
}

void deconv(fp16* input, u32 in_height, u32 in_width, u32 in_channels,
            fp16* output, u32 out_height, u32 out_width, u32 out_channels, u32 out_stride,
            fp16* weights,
            u32 radix_x, u32 radix_y,
            u32 stride_x, u32 stride_y,
            u32 pad_x, u32 pad_y,
            t_MvTensorMyriadResources *myriadResources) {

    // How to Parallelize? Ans: each SHAVE is assigned
    // a range of output lines [from, to)

    // splitting over input channels
    u32 paddedK = align8(in_channels);
    u32 slice_no = 1;
    u32 slice_channels = paddedK;
    {
        u32 prev_slice_channels;
        while ( req_cmx((u32)getCMXSliceDataSection(0), slice_channels, align8(out_channels), out_width) > MVTENSOR_HEAP_DATA_SIZE )
        {
            slice_no ++;
            prev_slice_channels = slice_channels;
            mvTensorAssert(prev_slice_channels != 8, "Dimensions too big for Deconvolution. (2 * align64(out_channels) * (8 + out_width) > 83.5 K)");
            slice_channels = align8((paddedK + slice_no - 1) / slice_no);
            if (prev_slice_channels == slice_channels)
            {
                slice_channels -= 8;
                slice_no = (paddedK + slice_channels - 1) / slice_channels;
            }
        }
    }
    u32 shaves_no = (myriadResources->lastShave - myriadResources->firstShave + 1);
    u32 remaining_lines = out_height % shaves_no;
    u32 shv_out_lines = out_height / shaves_no;
    for (	int from = 0, to = shv_out_lines, s = myriadResources->firstShave;
            s <= myriadResources->lastShave;
            s++, from = to, to += shv_out_lines) {
        if (remaining_lines) {
            remaining_lines--;
            to += 1;
        }

        t_MvDeConvParam *deconvParam = useShaveParam<t_MvDeConvParam>(s);
        deconvParam->slice_no = slice_no;
        deconvParam->slice_channels = slice_channels;
        deconvParam->input = input;
        deconvParam->in_height = in_height;
        deconvParam->in_width = in_width;
        deconvParam->in_channels = in_channels;
        deconvParam->out_width = out_width;
        deconvParam->out_channels = out_channels;
        deconvParam->radix_x = radix_x;
        deconvParam->radix_y = radix_y;
        deconvParam->stride_x = stride_x;
        deconvParam->stride_y = stride_y;
        deconvParam->pad_x = pad_x;
        deconvParam->pad_y = pad_y;
        deconvParam->from = from;
        deconvParam->to = to;
        deconvParam->weights = weights;
        deconvParam->output = output;
        deconvParam->output_stride = out_stride / 2;
        deconvParam->dmaLinkAgent = myriadResources->dmaLinkAgent;
        deconvParam->cmxslice = getCMXSliceDataSection(s);

        u32 readBackAddr = (u32)(&(deconvParam->cmxslice));
        GET_REG_WORD_VAL(readBackAddr);

        MODULE_ENTRY_DECL(mvDeConv);
        startShave(s, (u32)&MODULE_ENTRY(mvDeConv), (u32)deconvParam);
    }

    waitShaves(myriadResources->firstShave, myriadResources->lastShave);
}
