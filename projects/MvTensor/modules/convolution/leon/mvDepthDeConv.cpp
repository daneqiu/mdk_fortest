///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

#include <algorithm>

#include "mvDepthDeConv.h"
#include "../priv/mvDepthConvParam.h"
#include <mvTensorInternal.h>
#include <mvTensorDma.h>
#include <OsDrvCmxDma.h>
#include <mvMacros.h>
#include <Fp16Convert.h>

#define ALIGN_VALUE 64

//#define DEBUG 1
#if DEBUG
#include <stdio.h>
#define MVT_DPRINTF(...) printf(__VA_ARGS__)
#else
#define MVT_DPRINTF(...)
#endif

using namespace mv::tensor;

// Sets the padding for TF or Caffe
void set_depth_deconv_padding(t_MvTensorPaddStyle pad_style,
                 u32 padX, u32 padY,
                 u32 &pad_l, u32 &pad_r, u32 &pad_t, u32 &pad_b,
				 u32 &gapX, u32 &gapY,
                 u32 filter_height, u32 filter_width,
                 u32 stride_h, u32 stride_w,
                 u32 in_height, u32 in_width) {
    /* paddStyleTFSame -
     * see https://www.tensorflow.org/api_guides/python/nn#Notes_on_SAME_Convolution_Padding*/
    if (pad_style == paddStyleTFSame)
    {
        u32 pad_along_height, pad_along_width;
        if (in_height % stride_h == 0)
          pad_along_height = std::max(filter_height - stride_h, (u32)0);
        else
          pad_along_height = std::max(filter_height - (in_height % stride_h), (u32)0);
        if (in_width % stride_w == 0)
          pad_along_width = std::max(filter_width - stride_w, (u32)0);
        else
          pad_along_width = std::max(filter_width - (in_width % stride_w), (u32)0);

        pad_t = pad_along_height >> 1;
        pad_b = pad_along_height - pad_t;
        pad_l = pad_along_width >> 1;
        pad_r = pad_along_width - pad_l;
    }
    else if (pad_style == paddStyleCaffe)   /*paddStyleCaffe - padding is set in blob*/
    {
        pad_t = pad_b = padY;
        pad_l = pad_r = padX;
    }
    else if (pad_style == paddStyleTFValid) /*paddStyleTFValid - no padding is used*/
    {
        pad_t = pad_b = pad_l = pad_r = 0;
    }
    gapX = stride_w - 1;
    gapY = stride_h - 1;
}

void getGappedInputInfo(u32 in_height, u32 in_width,
		u32 output_height, u32 output_width,
		u32 kernel_height, u32 kernel_width,
		u32 gapx, u32 gapy, u32 channel,
        u32& in_height_new, u32& in_width_new,
		u32& padt_new, u32& padb_new, u32& padl_new, u32& padr_new) {
	in_height_new = in_height + (in_height - 1) * gapy;
	in_width_new = in_width + (in_width - 1) * gapx;
	padl_new = padr_new = (output_width + kernel_width - 1 - in_width_new) / 2;
	padt_new = padb_new = (output_height + kernel_height - 1 - in_height_new) / 2;
	return;
}

void depthDeConv(fp16* input, u32 in_height, u32 in_width, u32 in_channels,
               fp16* weights, u32 filter_height, u32 filter_width, u32 stride_h, u32 stride_w,
               fp16* output, u32 out_height, u32 out_width, u32 out_channels,
               t_MvTensorPaddStyle pad_style, u32 padX, u32 padY,
               t_MvTensorMyriadResources *myriadResources, const Resources &res)
{
    mvTensorAssert(in_channels == out_channels, "Input and output channels need to match for DepthDeConv.");
    MVT_DPRINTF("ashen_debug we are reaching depth-wise deconv layer\n");

    u32 pad_l = 0, pad_r = 0;
    u32 pad_t = 0, pad_b = 0;
    u32 gapX = 0, gapY = 0;

    set_depth_deconv_padding(pad_style,
                             padX, padY,
                             pad_l, pad_r, pad_t, pad_b,
							 gapX, gapY,
                             filter_height, filter_width,
                             stride_h, stride_w,
                             in_height, in_width);

    // get the gapped input info
    u32 in_height_new, in_width_new, padt_new, padb_new, padl_new, padr_new;
    using namespace mv::tensor;
    dma::Task dmaTask(res.dmaUser);
    getGappedInputInfo(in_height, in_width, out_height, out_width,
    		filter_height, filter_width, gapX, gapY, in_channels,
			in_height_new, in_width_new, padt_new, padb_new, padl_new, padr_new);
    // end get the gapped input info

    // create buffer for transformed input
	u32 transformedSize = in_height_new * in_width_new * in_channels;
	u32 oriInputSize = in_height * in_width * in_channels;
	u32 inputBufferSize = in_channels * in_height_new * in_width_new + 1;
	fp16* raw_input_pt = (fp16*) malloc(sizeof(half) * transformedSize + ALIGN_VALUE);
	fp16* input_new = ALIGN_UP(raw_input_pt, ALIGN_VALUE);
	bzero(input_new, sizeof(half) * transformedSize);

    // decide shave partition
    const u32 shaves_no = (myriadResources->lastShave - myriadResources->firstShave + 1);
    const u32 pixels = out_height * out_width;
    const u32 split = pixels / shaves_no;
    u32 remaining = pixels % shaves_no;

    // transform input according to gapped info
	u32 h_new=0,w_new=0;
	for (u32 h = 0; h < in_height; h++) {
        for (u32 w = 0; w < in_width; w++) {
            u32 ori_idx = (h * in_width + w) * in_channels;
            h_new = (gapY + 1)*h;
            w_new = (gapX + 1)*w;
            u32 dst_idx = (h_new * in_width_new + w_new) * in_channels;
            if (in_channels < 4) {
                for (u32 c = 0; c < in_channels; c++) {
                	input_new[dst_idx + c] = input[ori_idx + c];
                }
            } else {
                dmaTask.start(input + ori_idx, input_new + dst_idx, in_channels * sizeof(fp16));
            }
        }
	}
    // make sure all the input transform has been finished
    dmaTask.wait();
	// end transform input

    for (int from = 0, to = split, s = myriadResources->firstShave;
            s <= myriadResources->lastShave;
            ++s, from = to, to += split)
    {
        if (remaining > 0)
        {
            --remaining;
            ++to;
        }

        t_MvDepthConvParam *depthConvParam = useShaveParam<t_MvDepthConvParam>(s);
        depthConvParam->input = input_new;
        depthConvParam->in_height = in_height_new;
        depthConvParam->in_width = in_width_new;
        depthConvParam->in_channels = in_channels;
        depthConvParam->weights = weights;
        depthConvParam->filter_height = filter_height;
        depthConvParam->filter_width = filter_width;
        depthConvParam->stride_h = 1;
        depthConvParam->stride_w = 1;
        depthConvParam->output = output;
        depthConvParam->out_height = out_height;
        depthConvParam->out_width = out_width;
        depthConvParam->from = from;
        depthConvParam->to = to;
        depthConvParam->pad_t = padt_new;
        depthConvParam->pad_b = padb_new;
        depthConvParam->pad_l = padl_new;
        depthConvParam->pad_r = padr_new;
        depthConvParam->dmaLinkAgent = myriadResources->dmaLinkAgent;
        depthConvParam->cmxslice = getCMXSliceDataSection(s);

        u32 readBackAddr = (u32)(&(depthConvParam->cmxslice));
        GET_REG_WORD_VAL(readBackAddr);

        MODULE_ENTRY_DECL(mvDepthDeConv);
        startShave(s, (u32)&MODULE_ENTRY(mvDepthDeConv), (u32)depthConvParam);
    }

    waitShaves(myriadResources->firstShave, myriadResources->lastShave);
    free(raw_input_pt);
}
