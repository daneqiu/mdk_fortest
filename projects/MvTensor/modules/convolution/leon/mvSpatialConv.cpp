///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

#include "../priv/mvSpatialConvParam.h"
#include <mvTensorInternal.h>
#include "mvSpatialConv.h"

void spatialconv(fp16* input, u32 in_height, u32 in_width,
               u32 in_channels, u32 out_channels,
               u32 radix_x, u32 radix_y,
               u32 stride_x, u32 stride_y,
               u32 padding_type,
               s32 padL, s32 padR, s32 padT, s32 padB,
               fp16* weights, fp16* output,
               t_MvTensorMyriadResources *myriadResources) {
    u32 shaves_no = (myriadResources->lastShave - myriadResources->firstShave + 1);
    u32 pad_l, pad_r;

    // NOTE: The values are already computed in convolution_entry.cpp
    //       Please review this and use correct formula for out_height!
    pad_l = pad_r = padding_type * (radix_y/2);
    u32 out_height = (in_height + pad_l + pad_r - radix_y)/stride_y + 1;
    // How to Parallelize? Ans: each SHAVE is assigned
    // a range of output rows (start_out_row + out_rows)
    u32 start_out_row = 0;
    u32 out_rows = out_height / shaves_no;
    u32 remaining_rows = out_height - out_rows * shaves_no;
    s32 i;
    for (i = myriadResources->firstShave; i <= myriadResources->lastShave; i++) {
        out_rows =  out_height / shaves_no;
        if (remaining_rows) {
          remaining_rows--;
          out_rows++;
        }

        t_MvSpatialConvParam *spatialconvParam = useShaveParam<t_MvSpatialConvParam>(i);
        spatialconvParam->input = input;
        spatialconvParam->in_height = in_height;
        spatialconvParam->in_width = in_width;
        spatialconvParam->in_channels = in_channels;
        spatialconvParam->out_channels = out_channels;
        spatialconvParam->radix_x = radix_x;
        spatialconvParam->radix_y = radix_y;
        spatialconvParam->stride_x = stride_x;
        spatialconvParam->stride_y = stride_y;
        spatialconvParam->padding_type = padding_type;
        spatialconvParam->padL = padL;
        spatialconvParam->padR = padR;
        spatialconvParam->padT = padT;
        spatialconvParam->padB = padB;
        spatialconvParam->start_out_row = start_out_row;
        spatialconvParam->out_rows = out_rows;
        spatialconvParam->weights = weights;
        spatialconvParam->output = output;
        spatialconvParam->dmaLinkAgent = myriadResources->dmaLinkAgent;
        spatialconvParam->cmxslice = getCMXSliceDataSection(i);

        u32 readBackAddr = (u32)(&(spatialconvParam->cmxslice));
        GET_REG_WORD_VAL(readBackAddr);

        MODULE_ENTRY_DECL(mvSpatialConv);
        startShave(i, (u32)&MODULE_ENTRY(mvSpatialConv), (u32)spatialconvParam);

        start_out_row += out_rows;
    }

    waitShaves(myriadResources->firstShave, myriadResources->lastShave);
}
