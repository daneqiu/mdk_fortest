///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

#include <algorithm>

#include "mvDepthConv.h"
#include "../priv/mvDepthConvParam.h"
#include <mvTensorInternal.h>

using namespace mv::tensor;

// Sets the padding for TF or Caffe
void set_padding(t_MvTensorPaddStyle pad_style,
                 u32 padX, u32 padY,
                 u32 &pad_l, u32 &pad_r, u32 &pad_t, u32 &pad_b,
                 u32 filter_height, u32 filter_width,
                 u32 stride_h, u32 stride_w,
                 u32 in_height, u32 in_width) {
    /* paddStyleTFSame -
     * see https://www.tensorflow.org/api_guides/python/nn#Notes_on_SAME_Convolution_Padding*/
    if (pad_style == paddStyleTFSame)
    {
        u32 pad_along_height, pad_along_width;
        if (in_height % stride_h == 0)
          pad_along_height = std::max(filter_height - stride_h, (u32)0);
        else
          pad_along_height = std::max(filter_height - (in_height % stride_h), (u32)0);
        if (in_width % stride_w == 0)
          pad_along_width = std::max(filter_width - stride_w, (u32)0);
        else
          pad_along_width = std::max(filter_width - (in_width % stride_w), (u32)0);

        pad_t = pad_along_height >> 1;
        pad_b = pad_along_height - pad_t;
        pad_l = pad_along_width >> 1;
        pad_r = pad_along_width - pad_l;
    }
    else if (pad_style == paddStyleCaffe)   /*paddStyleCaffe - padding is set in blob*/
    {
        pad_t = pad_b = padY;
        pad_l = pad_r = padX;
    }
    else if (pad_style == paddStyleTFValid) /*paddStyleTFValid - no padding is used*/
    {
        pad_t = pad_b = pad_l = pad_r = 0;
    }
}

void depthConv(fp16* input, u32 in_height, u32 in_width, u32 in_channels,
               fp16* weights, u32 filter_height, u32 filter_width, u32 stride_h, u32 stride_w,
               fp16* output, u32 out_height, u32 out_width, u32 out_channels,
               t_MvTensorPaddStyle pad_style, u32 padX, u32 padY,
               t_MvTensorMyriadResources *myriadResources)
{
    mvTensorAssert(in_channels == out_channels, "Input and output channels need to match for DepthConv.");

    u32 pad_l = 0, pad_r = 0;
    u32 pad_t = 0, pad_b = 0;

    set_padding(pad_style,
                padX, padY,
                pad_l, pad_r, pad_t, pad_b,
                filter_height, filter_width,
                stride_h, stride_w,
                in_height, in_width);

    const u32 shaves_no = (myriadResources->lastShave - myriadResources->firstShave + 1);
    const u32 pixels = out_height * out_width;
    const u32 split = pixels / shaves_no;
    u32 remaining = pixels % shaves_no;

    for (int from = 0, to = split, s = myriadResources->firstShave;
            s <= myriadResources->lastShave;
            ++s, from = to, to += split)
    {
        if (remaining > 0)
        {
            --remaining;
            ++to;
        }

        t_MvDepthConvParam *depthConvParam = useShaveParam<t_MvDepthConvParam>(s);
        depthConvParam->input = input;
        depthConvParam->in_height = in_height;
        depthConvParam->in_width = in_width;
        depthConvParam->in_channels = in_channels;
        depthConvParam->weights = weights;
        depthConvParam->filter_height = filter_height;
        depthConvParam->filter_width = filter_width;
        depthConvParam->stride_h = stride_h;
        depthConvParam->stride_w = stride_w;
        depthConvParam->output = output;
        depthConvParam->out_height = out_height;
        depthConvParam->out_width = out_width;
        depthConvParam->from = from;
        depthConvParam->to = to;
        depthConvParam->pad_t = pad_t;
        depthConvParam->pad_b = pad_b;
        depthConvParam->pad_l = pad_l;
        depthConvParam->pad_r = pad_r;
        depthConvParam->dmaLinkAgent = myriadResources->dmaLinkAgent;
        depthConvParam->cmxslice = getCMXSliceDataSection(s);

        u32 readBackAddr = (u32)(&(depthConvParam->cmxslice));
        GET_REG_WORD_VAL(readBackAddr);

        MODULE_ENTRY_DECL(mvDepthConv);
        startShave(s, (u32)&MODULE_ENTRY(mvDepthConv), (u32)depthConvParam);
    }

    waitShaves(myriadResources->firstShave, myriadResources->lastShave);
}
