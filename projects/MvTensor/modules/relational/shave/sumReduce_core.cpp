///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect code
///
#include <mv_types.h>
#include <svuCommonShave.h>
#include <swcCdma.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <moviVectorTypes.h>
//#include "normalize_core.h"
#include "sumReduce_core.h"
#include "mvtMulAB.h"

//#include "addV2Fp16.h"
//#include <mvTensorConfig.h>
//#include <mvTensorDma.h>

#define INPUT_BPP       2
#define CMX_SLICE      (INPUT_BPP*42000)

#if (__MOVICOMPILE_MINOR__ == 50) && (__MOVICOMPILE_PATCH__ < 79)
#define LOCAL_HALF_POWER __hpow
#else
#define LOCAL_HALF_POWER __pows
#endif

//using namespace mv::tensor;

void mvSumReduce(t_MvSumReduceParam *p)
{
    u32 id1, i;
    dmaTransactionList_t task1;
    dmaTransactionList_t *ref1;
    u32  C = p->channels;
    u32  K = p->k;
    u32  pad, maxLines, Cpadded, Kpadded;
    u32  cmxRem = CMX_SLICE;
    half *pCmx = p->cmxslice;
    half *linesBuffer, *normBuffer, *scaleBuffer, *sumMultiplier, *sumBuffer;

    // Optimization ideas:
    //   1. use asm of either matrixVectMult or matmulBT or mvtMultAB
    //   2. make sure that all cmx buffers are aligned to 8

    // TODO: <Not implemented>: accross_spatial=true
    // TODO: <Not implemented>: channel_shared=true

    // pad C in order to align to the next multiple of 8
    Cpadded = (C + 7) &~0x7;
    pad = Cpadded - C;
    Kpadded = (K + 7) &~0x7;

    // increment buffers with Cpadded, in order to keep buffers aligned
    // (since Cpadded is aligned to 8, the buffers will be aligned to 16,
    //  that p->cmxslice is also aligned to 16)
    scaleBuffer = pCmx;
    pCmx   += Cpadded;
    cmxRem -= Cpadded;

    sumMultiplier = pCmx;
    pCmx   += Cpadded;
    cmxRem -= Cpadded;

    maxLines = (cmxRem/2/INPUT_BPP - 7 - pad) / Cpadded;

    linesBuffer  =  pCmx;
    pCmx   += cmxRem/2/INPUT_BPP;
    cmxRem -= cmxRem/2/INPUT_BPP;
    normBuffer =  pCmx;
    sumBuffer = pCmx;

    for (i=0; i<C; i++)
    {
        sumMultiplier[i] = (half)1.0;
    }
    for (;i<Cpadded; i++)
    {
        sumMultiplier[i] = (half)0.0;
    }

    id1 = dmaInitRequesterWithAgent(1, p->dmaLinkAgent);
#if 0
    ref1 = dmaCreateTransaction(id1, &task1,
            (u8 *)(p->input),               // src
            (u8 *)(scaleBuffer),            // dst
            C * INPUT_BPP);                 // byte length
    dmaStartListTask(ref1);
    dmaWaitTask(ref1);
#endif
    for(i = 0; i < p->numlines; i += maxLines)
    {
        u32 nlines = p->numlines - i;

        if(nlines > maxLines)
            nlines = maxLines;
        u32 nelem_padded = nlines * Cpadded;

        // extend nelem_padded to the next multiple of 8, as this is a
        // restriction of the matmul asm kernel
        nelem_padded = (nelem_padded + 7) & ~7;

        ref1 = dmaCreateTransactionFullOptions(id1, &task1,
                (u8 *)(p->input + i*C),         // src
                (u8 *)(linesBuffer),            // dst
                nlines * C * INPUT_BPP,         // byte length
                C * INPUT_BPP,                  // src width
                C * INPUT_BPP,                  // dst width
                C * INPUT_BPP,                  // src stride
                (Cpadded) * INPUT_BPP);           // dst stride
        dmaStartListTask(ref1);
#if 1
        // padding in parallel with the DMA, as the elements locations don't overlap
        for(u32 j = 0; j < nlines; j++)
        {
            for(u32 k = 0; k < pad; k++)
            {
                *(linesBuffer + j*(C+pad) + C + k) = 0.0;
            }
        }
#endif
        dmaWaitTask(ref1);
#if 0
        for(u32 j = 0; j < nelem_padded; j += 8)
        {
            half8 *t = reinterpret_cast<half8*>(linesBuffer + j);
            *t = *t * *t;
        }
#endif
#if 0
        half sum = 0;
        for (u32 x = 0; x < C; x++)
        {
        	sum += linesBuffer[x];
        }
        sumBuffer[0] = sum;
#else
        mvtMulAB_hhhh_c(linesBuffer, sumMultiplier, sumBuffer, nlines, Cpadded, 1, Cpadded, 1, 1);
        //sumBuffer[0] = linesBuffer[0];
#endif

//#define ACCURATE_POW
#if 0
#ifdef ACCURATE_POW
        for(u32 j = 0; j < nlines; j+=8)
        {
            half8* __restrict__ tn = reinterpret_cast<half8*>(normBuffer + j);
            half8 base = *tn;
            half beta = -0.5;
            (*tn)[0] = LOCAL_HALF_POWER(base[0], beta);
            (*tn)[1] = LOCAL_HALF_POWER(base[1], beta);
            (*tn)[2] = LOCAL_HALF_POWER(base[2], beta);
            (*tn)[3] = LOCAL_HALF_POWER(base[3], beta);
            (*tn)[4] = LOCAL_HALF_POWER(base[4], beta);
            (*tn)[5] = LOCAL_HALF_POWER(base[5], beta);
            (*tn)[6] = LOCAL_HALF_POWER(base[6], beta);
            (*tn)[7] = LOCAL_HALF_POWER(base[7], beta);
        }
#else
        for(u32 j = 0; j < nlines; j += 8)
        {
            half8* __restrict__ tn = reinterpret_cast<half8*>(normBuffer + j);

            (*tn)[0] = __builtin_shave_sau_rqt_f16_l_r((*tn)[0]);
            (*tn)[1] = __builtin_shave_sau_rqt_f16_l_r((*tn)[1]);
            (*tn)[2] = __builtin_shave_sau_rqt_f16_l_r((*tn)[2]);
            (*tn)[3] = __builtin_shave_sau_rqt_f16_l_r((*tn)[3]);
            (*tn)[4] = __builtin_shave_sau_rqt_f16_l_r((*tn)[4]);
            (*tn)[5] = __builtin_shave_sau_rqt_f16_l_r((*tn)[5]);
            (*tn)[6] = __builtin_shave_sau_rqt_f16_l_r((*tn)[6]);
            (*tn)[7] = __builtin_shave_sau_rqt_f16_l_r((*tn)[7]);
        }
#endif // ACCURATE_POW

        // Normalize: compute in = in * norm
        ref1 = dmaCreateTransactionFullOptions(id1, &task1,
                (u8 *)(p->input + i*C),         // src
                (u8 *)(linesBuffer),            // dst
                nlines * C * INPUT_BPP,         // byte length
                C * INPUT_BPP,                  // src width
                C * INPUT_BPP,                  // dst width
                C * INPUT_BPP,                  // src stride
                (Cpadded) * INPUT_BPP);         // dst stride
        dmaStartListTask(ref1);
        dmaWaitTask(ref1);

        for(u32 j = 0; j < nlines; j++)
        {
            for(u32 k = 0; k < Cpadded; k+=8)
            {
                half8 *ti = reinterpret_cast<half8*>(linesBuffer + j*(Cpadded) + k);
                half8 *ts = reinterpret_cast<half8*>(scaleBuffer + k);
                half *tn = normBuffer + j;
                *ti = *ti * *tn;
                *ti = *ti * *ts;
            }
        }
#endif
        // output DMA
        ref1 = dmaCreateTransactionFullOptions(id1, &task1,
                (u8 *)(sumBuffer),            // src
                (u8 *)(p->output + i*K),        // dst
                nlines * K * INPUT_BPP,         // byte length
                K * INPUT_BPP,                  // src width
                K * INPUT_BPP,                  // dst width
                (Kpadded) * INPUT_BPP,          // src stride
                K * INPUT_BPP);                 // dst stride
        dmaStartListTask(ref1);
        dmaWaitTask(ref1);
    }
}
