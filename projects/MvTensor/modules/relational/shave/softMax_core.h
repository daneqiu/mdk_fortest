///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef SOFTMAX_SHAVE_H_
#define SOFTMAX_SHAVE_H_

#include "../priv/mvSoftMaxParam.h"

#ifdef __cplusplus
extern "C"
{
#endif

void mvSoftMaxAxisC(t_MvSoftMaxParam *p);
void mvSoftMaxAxisH(t_MvSoftMaxParam *p);

#ifdef __cplusplus
}
#endif

#endif // SOFTMAX_SHAVE_H_
