///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef LRN_SHAVE_H_
#define LRN_SHAVE_H_

#include "../priv/lrnParam.h"

#ifdef __cplusplus
extern "C"
{
#endif

void mvLRN(t_MvLRNParam *p);

#ifdef __cplusplus
}
#endif

#endif // LRN_SHAVE_H_
