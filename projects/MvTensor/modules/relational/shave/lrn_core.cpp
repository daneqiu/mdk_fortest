///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect code
///

#include "lrn_core.h"
#include "addV2Fp16.h"
#include <mvTensorConfig.h>
#include <mvTensorDma.h>

#define INPUT_BPP       2
#define CMX_SLICE      MVTENSOR_HEAP_DATA_SIZE

#if (__MOVICOMPILE_MINOR__ == 50) && (__MOVICOMPILE_PATCH__ < 79)
#define LOCAL_HALF_POWER __hpow
#else
#define LOCAL_HALF_POWER __pows
#endif

using namespace mv::tensor;

static void addMx1_kernel(dma::User &dmaUser,
                          u8* linesBuffer, u8* outputBuffer, u32 radix, u32 elemDist, u32 numElem)
{
    u32 x, span, hasIntermediateOut;
    half* inputLines[2];
    half* outputLines[1];

    x = 1;
    span = radix;
    hasIntermediateOut = 0;

    do
    {
        if (span%2 == 1)
        {
            inputLines[0] = (half*)linesBuffer + x * (span-1) * elemDist;
            inputLines[1] = (half*)outputBuffer;
            outputLines[0] = (half*)outputBuffer;
            if (hasIntermediateOut == 0)
            {
                hasIntermediateOut = 1;
                // this is the first element written to the output buffer -> a simple copy is sufficient
                dma::Task(dmaUser).start((u8*)inputLines[0], (u8*)outputLines[0], numElem * INPUT_BPP);
            }
            else
            {
                mvcvAddV2Fp16_asm((half**)outputLines, (half**)inputLines, numElem);
            }
        }

        inputLines[0] = (half*)linesBuffer + x * elemDist;
        inputLines[1] = (half*)linesBuffer;
        outputLines[0] = (half*)linesBuffer;

        x = x*2;
        span = span/2;

        // last iteration writes to output buffer
        if (span == 1 && !hasIntermediateOut)
        {
            outputLines[0] = (half*)outputBuffer;
        }

        mvcvAddV2Fp16_asm((half**)outputLines, (half**)inputLines, numElem);
    } while (span > 1);

    if (hasIntermediateOut)
    {
        inputLines[0] = (half*)linesBuffer;
        inputLines[1] = (half*)outputBuffer;
        outputLines[0] = (half*)outputBuffer;

        mvcvAddV2Fp16_asm((half**)outputLines, (half**)inputLines, numElem);
    }
}

void mvLRN(t_MvLRNParam *p)
{
	dma::Config dmaConfig = { 1, p->dmaLinkAgent };
	dma::User dmaUser(dmaConfig);

    u32 C = p->channels;
    u32 pad;
    u32 max_lines;
    half* linesBuffer  =  p->cmxslice;
    half* outputBuffer =  p->cmxslice + CMX_SLICE/2/INPUT_BPP;
    half beta = -p->beta;
    half8 k8 = {p->k, p->k, p->k, p->k, p->k, p->k, p->k, p->k};

    // Square the input and multiply by alpha/size
    half8 premult8 = 1.0 / 16.0;
    half8 alpha8 = 256.0 * p->alpha  / p->size;

    pad = p->size/2;
    max_lines = (CMX_SLICE/2/INPUT_BPP - 7 - pad) / (C + pad);

    for(u32 i = 0; i < p->numlines; i += max_lines)
    {
        u32 nlines = p->numlines - i;

        if(nlines > max_lines)
            nlines = max_lines;
        u32 nelem_padded = nlines * (C + pad);

        // extend nelem_padded to the next multiple of 8, as this is a
        // restriction of the asm kernels used in addMx1_kernel
        nelem_padded = (nelem_padded + 7) & ~7;

        dma::Task dmaTask(dmaUser);
        dmaTask.start(
                (u8 *)(p->input + i*C),         // src
                (u8 *)(linesBuffer + pad),      // dst
                nlines * C * INPUT_BPP,         // byte length
                C * INPUT_BPP,                  // src width
                C * INPUT_BPP,                  // dst width
                C * INPUT_BPP,                  // src stride
                (C+pad) * INPUT_BPP);           // dst stride

        // padding in parallel with the DMA, as the buffer locations don't overlap
        for(u32 j = 0; j < nlines+1; j++)
        {
            for(u32 k = 0; k < pad; k++)
            {
                *(linesBuffer + j*(C+pad) + k) = 0.0;
            }
        }

        dmaTask.wait();

        // The compiler optimizes the order of operations
        // and generates infs, so we have to fake him by doing the loop twice
        for(u32 j = 0; j < nelem_padded; j += 8)
        {
            half8 *t = reinterpret_cast<half8*>(linesBuffer + j);
            *t =premult8 * *t;
        }
        for(u32 j = 0; j < nelem_padded; j += 8)
        {
            half8 *t = reinterpret_cast<half8*>(linesBuffer + j);
            *t = *t * *t * alpha8;
        }

        addMx1_kernel(dmaUser, (u8*)linesBuffer, (u8*)outputBuffer, p->size, 1, nelem_padded);

        // Normalize: compute in = in * out^-beta
        dmaTask.start(
                (u8 *)(p->input + i*C),         // src
                (u8 *)(linesBuffer),            // dst
                nlines * C * INPUT_BPP,         // byte length
                C * INPUT_BPP,                  // src width
                C * INPUT_BPP,                  // dst width
                C * INPUT_BPP,                  // src stride
                (C+pad) * INPUT_BPP);           // dst stride

        dmaTask.wait();

//#define ACCURATE_POW
#ifdef ACCURATE_POW
        for(u32 j = 0; j < nelem_padded; j += 8)
        {
            half8* __restrict__ ti = reinterpret_cast<half8*>(linesBuffer + j);
            half8* __restrict__ to = reinterpret_cast<half8*>(outputBuffer + j);
            half8 base = *to + k8;
            half8 mult;
            mult[0] = LOCAL_HALF_POWER(base[0], beta);
            mult[1] = LOCAL_HALF_POWER(base[1], beta);
            mult[2] = LOCAL_HALF_POWER(base[2], beta);
            mult[3] = LOCAL_HALF_POWER(base[3], beta);
            mult[4] = LOCAL_HALF_POWER(base[4], beta);
            mult[5] = LOCAL_HALF_POWER(base[5], beta);
            mult[6] = LOCAL_HALF_POWER(base[6], beta);
            mult[7] = LOCAL_HALF_POWER(base[7], beta);
            *to = *ti * mult;
        }
#else
        for(u32 j = 0; j < nelem_padded; j += 8)
        {
            half8* __restrict__ ti = reinterpret_cast<half8*>(linesBuffer + j);
            half8* __restrict__ to = reinterpret_cast<half8*>(outputBuffer + j);
            half8 base = *to + k8;
            half8 mult;

            base[0] = __builtin_shave_sau_log2_f16_l_r(base[0]);
            base[1] = __builtin_shave_sau_log2_f16_l_r(base[1]);
            base[2] = __builtin_shave_sau_log2_f16_l_r(base[2]);
            base[3] = __builtin_shave_sau_log2_f16_l_r(base[3]);
            base[4] = __builtin_shave_sau_log2_f16_l_r(base[4]);
            base[5] = __builtin_shave_sau_log2_f16_l_r(base[5]);
            base[6] = __builtin_shave_sau_log2_f16_l_r(base[6]);
            base[7] = __builtin_shave_sau_log2_f16_l_r(base[7]);

            mult[0] = __builtin_shave_sau_exp2_f16_l_r(beta*base[0]);
            mult[1] = __builtin_shave_sau_exp2_f16_l_r(beta*base[1]);
            mult[2] = __builtin_shave_sau_exp2_f16_l_r(beta*base[2]);
            mult[3] = __builtin_shave_sau_exp2_f16_l_r(beta*base[3]);
            mult[4] = __builtin_shave_sau_exp2_f16_l_r(beta*base[4]);
            mult[5] = __builtin_shave_sau_exp2_f16_l_r(beta*base[5]);
            mult[6] = __builtin_shave_sau_exp2_f16_l_r(beta*base[6]);
            mult[7] = __builtin_shave_sau_exp2_f16_l_r(beta*base[7]);
            *to = *ti * mult;
        }
#endif

        // output DMA
        dmaTask.start(
                (u8 *)(outputBuffer),           // src
                (u8 *)(p->output + i*C),        // dst
                nlines * C * INPUT_BPP,         // byte length
                C * INPUT_BPP,                  // src width
                C * INPUT_BPP,                  // dst width
                (C+pad) * INPUT_BPP,            // src stride
                C * INPUT_BPP);                 // dst stride
        dmaTask.wait();
    }
}
