///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect code
///

#include <math.h>
#include <cstring>

#include "softMax_core.h"
#include <mvTensorDma.h>
#include <mvTensorConfig.h>

#define INPUT_BPP 2

static half getMax(half *x, u32 h){
    u32 i;
    half max = x[0];
    for (i = 0; i < h; i++)
    {
        if(max < x[i]) max = x[i];
    }
    return max;
}

static void expAll(half *x,u32 h){
    u32 i;
    for (i = 0 ; i < h; i++)
    {
        x[i] = exp(x[i]);
    }
}

static void plusAll(half *x, u32 h, half value){
    u32 i;
    for (i = 0; i < h; i++)
    {
        x[i] = x[i] + value;
    }
}

static half sumAll(half *x, u32 h){
    u32 i;
    float sum = 0;
    for (i = 0; i < h; i++)
    {
        sum += x[i];
    }
    return (half)sum;
}

static void divideAll(half *x, u32 h, half value){
    u32 i;
    for (i = 0; i < h; i++)
    {
        x[i] = x[i] / value;
    }
}

void mvSoftMaxAxisC(t_MvSoftMaxParam *p)
{
    using namespace mv::tensor;
    dma::Config dmaConfig = { 1, p->dmaLinkAgent };
    dma::User dmaUser(dmaConfig);

    u32 sliceC = p->sliceC;
    u32 i;
    u8* inAddress = (u8*) ((u32) p->input);
    u8* outAddress = (u8*) ((u32) p->output);
    u8* inputBuffer;

    // set buffers to point to locations relative to cmxslice
    // the memory allocation is done in MatMul (88000 bytes/slice)
    inputBuffer = p->cmxslice;

    if ((p->bufferOverflow == 1 && (p->stage == 1 || p->stage == 3 || p->stage == 5))
        || (p->bufferOverflow == 0 && p->stage == 1)) {

        dma::Task(dmaUser).start(inAddress, inputBuffer, 1 * 1 * sliceC * INPUT_BPP);
    }

    if (p->stage == 1) {
        // local max
        if (*(half*) (p->staging + p->offset) < getMax((half*) inputBuffer, sliceC))
            *(half*) (p->staging + p->offset) = getMax((half*) inputBuffer, sliceC);
    } else if (p->stage == 2) {
        // run only on 1 SHAVE
        for (i = 1; i < MVTENSOR_MAX_SHAVES; i++) {
            // global max
            if (*((half*) p->staging + i) > *((half*) p->staging))
                *((half*) p->staging) = *((half*) p->staging + i);
        }
    } else if (p->stage == 3) {
        plusAll((half*) inputBuffer, sliceC, -*((half*) p->staging));
        expAll((half*) inputBuffer, sliceC);

        // local sum
        *(half*) (p->staging + p->offset + MVTENSOR_MAX_SHAVES * 2) += sumAll((half*) inputBuffer, sliceC);
    } else if (p->stage == 4) {
        // run only on 1 SHAVE
        for (i = 1; i < MVTENSOR_MAX_SHAVES; i++) {
            // global sum
            *(half*) (p->staging + MVTENSOR_MAX_SHAVES * 2) += *((half*) (p->staging + MVTENSOR_MAX_SHAVES * 2) + i);
        }
    } else {
        // p->stage == 5
        if (p->bufferOverflow == 1) {
            plusAll((half*) inputBuffer, sliceC, -*((half*) p->staging));
            expAll((half*) inputBuffer, sliceC);
        }
        divideAll((half*) inputBuffer, sliceC, *((half*) (p->staging + MVTENSOR_MAX_SHAVES * 2)));

        dma::Task(dmaUser).start(inputBuffer, outAddress, 1 * 1 * sliceC * INPUT_BPP);
    }
}

void mvSoftMaxAxisH(t_MvSoftMaxParam *params)
{
    // !!! The function uses the output as intermediat buffer.

    // Data bpp is only used for code readability. To extend to higher or lower
    // data bpp other changes are required.
    const s32 data_bpp = 2;

    using namespace mv::tensor;
    dma::Config dmaConfig = { 1, params->dmaLinkAgent };
    dma::User dmaUser(dmaConfig);

    s32 height    = params->height;
    s32 width     = params->width;
    s32 channels  = params->channels;

    s32 shave_no  = params->shave_no;
    s32 no_shaves = params->no_shaves;

    // The input and output are expected to be properly offset in the
    // input respectively output volumes.
    half *ddr_input  = (half *)params->input;
    half *ddr_output = (half *)params->output;

    s32 cmx_size_h8 = MVTENSOR_HEAP_DATA_SIZE / sizeof(half8);

    s32 axis = params->axis;
    if(axis == H_AXIS)
    {
        // Number of predictions per class for which to compute the softmax.
        s32 no_preds = width * channels;
        s32 no_preds_per_shave = no_preds / no_shaves;
        s32 no_preds_remainder = no_preds % no_shaves;
        s32 no_preds_to_proccess = no_preds_per_shave;
        if(shave_no < no_preds_remainder)
            ++no_preds_to_proccess;

        if(no_preds_to_proccess <= 0)
        {
            // Return when the number of predictions is less than the number of
            // shaves
            return;
        }

        ddr_input  += shave_no * no_preds_per_shave;
        ddr_output += shave_no * no_preds_per_shave;
        if(no_preds_remainder != 0)
        {
            if(shave_no < no_preds_remainder)
            {
                ddr_input  += shave_no;
                ddr_output += shave_no;
            }
            else
            {
                ddr_input  += no_preds_remainder;
                ddr_output += no_preds_remainder;
            }
        }

        // Number of predictions in vectors of 8 half.
        s32 no_preds_h8 = no_preds_to_proccess / 8;
        s32 no_preds_h8_remainder = no_preds_to_proccess % 8;
        if(no_preds_h8_remainder != 0)
            ++no_preds_h8;

        s32 preds_chunk_size = no_preds_h8;
        // At least 3 cmx buffers are required.
        // 1 for max, 1 for sum and one for itermediate values.
        while(preds_chunk_size * 3 > cmx_size_h8)
        {
            preds_chunk_size = preds_chunk_size / 2;
        }

        // Number of predictions chunks.
        s32 no_preds_chunks       = no_preds_h8 / preds_chunk_size;
        s32 preds_chunks_remainder = no_preds_h8 % preds_chunk_size;
        if(preds_chunks_remainder != 0)
            ++no_preds_chunks;

        s32 preds_transfer_size = preds_chunk_size * 8;
        s32 last_preds_transfer_size = preds_transfer_size;
        s32 last_preds_chunk_size = preds_chunk_size;
        if(preds_chunks_remainder != 0 || no_preds_chunks == 1)
        {
            // Last chunk of predictions is an incomplete one.
            last_preds_transfer_size = no_preds_to_proccess - preds_transfer_size *
                (no_preds_chunks - 1);
            last_preds_chunk_size = last_preds_transfer_size / 8;
            if(last_preds_transfer_size % 8)
                ++last_preds_chunk_size;
        }

        s32 no_cmx_chunks = cmx_size_h8 / preds_chunk_size;
        s32 no_classes = height;

        // If the number of predictions/shave is small enough, it might be
        // possible to transfer the predictions (scores) for multiple classe at
        // once.
        // max_classes_per_chunk always >= 1
        // 1st chunk is used to hold the max.
        // 2nd chunk is used to hold the sum.
        // 3rd chunk is used to hold the temporary input/output.
        s32 class_chunk_size = (no_cmx_chunks - 2) <= no_classes ?
            (no_cmx_chunks - 2) : no_classes;

        s32 no_classes_chunks     = no_classes / class_chunk_size;
        s32 last_class_chunk_size = class_chunk_size;
        s32 no_classes_remainder  = no_classes % class_chunk_size;
        if(no_classes_remainder != 0)
        {
            ++no_classes_chunks;
            last_class_chunk_size = no_classes_remainder;
        }

        // Set cmx buffers.
        half8 *max_buffer   = (half8 *)params->cmxslice;
        half8 *sum_buffer   = max_buffer + preds_chunk_size;
        half8 *input_buffer = sum_buffer + preds_chunk_size;

        // Itereate over chunks of predictions.
        for(s32 p_chunk_i = 0; p_chunk_i < no_preds_chunks; ++p_chunk_i)
        {
            s32 current_preds_chunk_size;
            s32 current_preds_transfer_size;
            if(p_chunk_i < (no_preds_chunks - 1))
            {
                current_preds_chunk_size = preds_chunk_size;
                current_preds_transfer_size = preds_transfer_size;
            }
            else
            {
                current_preds_chunk_size = last_preds_chunk_size;
                current_preds_transfer_size = last_preds_transfer_size;
            }

            // Comput Max
            for(s32 c_chunk_i = 0; c_chunk_i < no_classes_chunks; ++c_chunk_i)
            {
                s32 transfer_size = current_preds_transfer_size;
                s32 current_class_chunk_size;

                if(c_chunk_i < (no_classes_chunks - 1))
                {
                    transfer_size *= class_chunk_size;
                    current_class_chunk_size = class_chunk_size;
                }
                else
                {
                    transfer_size *= last_class_chunk_size;
                    current_class_chunk_size = last_class_chunk_size;
                }

                s32 ddr_offset = c_chunk_i * class_chunk_size * no_preds +
                    p_chunk_i * preds_transfer_size;

                s32 in_src_width  = current_preds_transfer_size;
                s32 in_dst_width  = current_preds_transfer_size;
                s32 in_src_stride = no_preds;
                s32 in_dst_stride = current_preds_chunk_size * 8;

                dma::Task(dmaUser).start(ddr_input + ddr_offset,
                        input_buffer,
                        transfer_size * data_bpp,
                        in_src_width  * data_bpp,
                        in_dst_width  * data_bpp,
                        in_src_stride * data_bpp,
                        in_dst_stride * data_bpp);

                s32 class_start_idx = 0;
                if(c_chunk_i == 0)
                {
                    // Copy the predicitons scores for first class;
                    memcpy((void *)max_buffer,
                           (void *)input_buffer,
                           //current_preds_chunk_size * 8 * 2);
                           current_preds_transfer_size * data_bpp);
                    class_start_idx = 1;
                }

                for(s32 class_i = class_start_idx; class_i < current_class_chunk_size; ++class_i)
                {
                    for(s32 pred_i = 0; pred_i < current_preds_chunk_size; ++pred_i)
                    {
                        max_buffer[pred_i] = __builtin_shave_cmu_max_f16_rr_half8(
                                max_buffer[pred_i],
                                input_buffer[class_i * current_preds_chunk_size + pred_i]);
                    }
                }
            }

            // Substract max and exponentiate.
            // Using the output as intermediate.
            for(s32 c_chunk_i = 0; c_chunk_i < no_classes_chunks; ++c_chunk_i)
            {
                s32 transfer_size = current_preds_transfer_size;
                s32 current_class_chunk_size;

                if(c_chunk_i < (no_classes_chunks - 1))
                {
                    transfer_size *= class_chunk_size;
                    current_class_chunk_size = class_chunk_size;
                }
                else
                {
                    transfer_size *= last_class_chunk_size;
                    current_class_chunk_size = last_class_chunk_size;
                }

                s32 ddr_offset = c_chunk_i * class_chunk_size * no_preds +
                    p_chunk_i * preds_transfer_size;

                s32 in_src_width  = current_preds_transfer_size;
                s32 in_dst_width  = current_preds_transfer_size;
                s32 in_src_stride = no_preds;
                s32 in_dst_stride = current_preds_chunk_size * 8;

                dma::Task(dmaUser).start(ddr_input + ddr_offset,
                        input_buffer,
                        transfer_size * data_bpp,
                        in_src_width  * data_bpp,
                        in_dst_width  * data_bpp,
                        in_src_stride * data_bpp,
                        in_dst_stride * data_bpp);

                for(s32 class_i = 0; class_i < current_class_chunk_size; ++class_i)
                {
                    for(s32 pred_i = 0; pred_i < current_preds_chunk_size; ++pred_i)
                    {
                        const unsigned short inv_ln2 = 0x3dc6;
                        const half inv_ln2_h = *reinterpret_cast<const half *>(&inv_ln2);
                        const half8 inv_ln2_h_half8 = (half8)inv_ln2_h;
                        s32 pred_idx = class_i * current_preds_chunk_size + pred_i;
                        half8 base = (input_buffer[pred_idx] - max_buffer[pred_i]) * inv_ln2_h_half8;

                        base[0] = __builtin_shave_sau_exp2_f16_l_r(base[0]);
                        base[1] = __builtin_shave_sau_exp2_f16_l_r(base[1]);
                        base[2] = __builtin_shave_sau_exp2_f16_l_r(base[2]);
                        base[3] = __builtin_shave_sau_exp2_f16_l_r(base[3]);
                        base[4] = __builtin_shave_sau_exp2_f16_l_r(base[4]);
                        base[5] = __builtin_shave_sau_exp2_f16_l_r(base[5]);
                        base[6] = __builtin_shave_sau_exp2_f16_l_r(base[6]);
                        base[7] = __builtin_shave_sau_exp2_f16_l_r(base[7]);

                        input_buffer[pred_idx] = base;

                        if(class_i == 0 && c_chunk_i == 0)
                        {
                            sum_buffer[pred_i] = base;
                        }
                        else
                        {
                            sum_buffer[pred_i] += base;
                        }
                    }
                }

                s32 out_src_width  = current_preds_transfer_size;
                s32 out_dst_width  = current_preds_transfer_size;
                s32 out_src_stride = current_preds_chunk_size * 8;
                s32 out_dst_stride = no_preds;

                dma::Task(dmaUser).start(input_buffer,
                        ddr_output + ddr_offset,
                        transfer_size  * data_bpp,
                        out_src_width  * data_bpp,
                        out_dst_width  * data_bpp,
                        out_src_stride * data_bpp,
                        out_dst_stride * data_bpp);

            }

            // Compute 1/sum_of_exp_values
            for(s32 pred_i = 0; pred_i < current_preds_chunk_size; ++pred_i)
            {
                sum_buffer[pred_i][0] = __builtin_shave_sau_rcp_f16_l_r(sum_buffer[pred_i][0]);
                sum_buffer[pred_i][1] = __builtin_shave_sau_rcp_f16_l_r(sum_buffer[pred_i][1]);
                sum_buffer[pred_i][2] = __builtin_shave_sau_rcp_f16_l_r(sum_buffer[pred_i][2]);
                sum_buffer[pred_i][3] = __builtin_shave_sau_rcp_f16_l_r(sum_buffer[pred_i][3]);
                sum_buffer[pred_i][4] = __builtin_shave_sau_rcp_f16_l_r(sum_buffer[pred_i][4]);
                sum_buffer[pred_i][5] = __builtin_shave_sau_rcp_f16_l_r(sum_buffer[pred_i][5]);
                sum_buffer[pred_i][6] = __builtin_shave_sau_rcp_f16_l_r(sum_buffer[pred_i][6]);
                sum_buffer[pred_i][7] = __builtin_shave_sau_rcp_f16_l_r(sum_buffer[pred_i][7]);
            }

            // Devide by sum.
            for(s32 c_chunk_i = 0; c_chunk_i < no_classes_chunks; ++c_chunk_i)
            {
                s32 transfer_size = current_preds_transfer_size;
                s32 current_class_chunk_size;

                if(c_chunk_i < (no_classes_chunks - 1))
                {
                    transfer_size *= class_chunk_size;
                    current_class_chunk_size = class_chunk_size;
                }
                else
                {
                    transfer_size *= last_class_chunk_size;
                    current_class_chunk_size = last_class_chunk_size;
                }

                s32 ddr_offset = c_chunk_i * class_chunk_size * no_preds +
                    p_chunk_i * preds_transfer_size;

                s32 in_src_width  = current_preds_transfer_size;
                s32 in_dst_width  = current_preds_transfer_size;
                s32 in_src_stride = no_preds;
                s32 in_dst_stride = current_preds_chunk_size * 8;

                dma::Task(dmaUser).start(ddr_output + ddr_offset,
                        input_buffer,
                        transfer_size * data_bpp,
                        in_src_width  * data_bpp,
                        in_dst_width  * data_bpp,
                        in_src_stride * data_bpp,
                        in_dst_stride * data_bpp);

                for(s32 class_i = 0; class_i < current_class_chunk_size; ++class_i)
                {
                    for(s32 pred_i = 0; pred_i < current_preds_chunk_size; ++pred_i)
                    {
                        s32 pred_idx = class_i * current_preds_chunk_size + pred_i;
                        input_buffer[pred_idx] *= sum_buffer[pred_i];
                    }
                }

                s32 out_src_width  = current_preds_transfer_size;
                s32 out_dst_width  = current_preds_transfer_size;
                s32 out_src_stride = current_preds_chunk_size * 8;
                s32 out_dst_stride = no_preds;

                dma::Task(dmaUser).start(input_buffer,
                        ddr_output + ddr_offset,
                        transfer_size  * data_bpp,
                        out_src_width  * data_bpp,
                        out_dst_width  * data_bpp,
                        out_src_stride * data_bpp,
                        out_dst_stride * data_bpp);

            }
        }
    }
}

