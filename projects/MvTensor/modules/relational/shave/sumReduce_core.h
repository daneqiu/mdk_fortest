///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef SUMREDUCE_SHAVE_H_
#define SUMREDUCE_SHAVE_H_

#include "../priv/sumReduceParam.h"

#ifdef __cplusplus
extern "C"
{
#endif

void mvSumReduce(t_MvSumReduceParam *p);

#ifdef __cplusplus
}
#endif

#endif // SUMREDUCE_SHAVE_H_
