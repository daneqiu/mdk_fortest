///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

#include "lrn.h"
#include "../priv/lrnParam.h"
#include <mvTensorInternal.h>
#include <mvModuleHandle.h>

namespace
{
    using namespace mv::tensor;

    class LRN : public Layer
    {
        virtual void run_(const t_MvTensorParam *mvTensorParam, const t_MvTensorOp &op, const mv::tensor::Optimization &, const Resources &)
        {
            lrn((half *)mvTensorParam->input->data,
                            mvTensorParam->input->dimZ,
                            mvTensorParam->input->dimY,
                            mvTensorParam->input->dimX,
                            (half *)mvTensorParam->output->data,
                            op.radixX,
                            ((half *)mvTensorParam->biases->data)[0],
                            ((half *)mvTensorParam->biases->data)[1],
                            ((half *)mvTensorParam->biases->data)[2],
                            mvTensorParam->myriadResources->firstShave,
                            mvTensorParam->myriadResources->lastShave,
                            mvTensorParam->myriadResources->dmaLinkAgent);
        }
    };

    const ModuleHandle handles[] =
    {
        ModuleHandle().add<LRN>(kLRN)
    };
}

void lrn(fp16* input, u32 channels, u32 height,
                  u32 width, fp16* output,
                  u32 size, fp16 k, fp16 alpha, fp16 beta,
                  u32 firstShave, u32 lastShave,
				  u32 dmaLinkAgent)
{
    u32 nShaves = lastShave - firstShave + 1;

    // Divide between shaves
    u32 nlines_per_shave = (width * height + nShaves - 1) / nShaves;
    for(u32 i = 0; i < nShaves; i++)
    {
		// consider data as a 2D matrix, with num_cols=channels and num_lines=W*H
        t_MvLRNParam *lrnParam = useShaveParam<t_MvLRNParam>(i + firstShave);
		lrnParam->input = input + i * nlines_per_shave * channels;
		lrnParam->output = output + i * nlines_per_shave * channels;
		lrnParam->numlines = i == nShaves - 1 ?
			width * height - i * nlines_per_shave  : nlines_per_shave;
		lrnParam->channels = channels;
		lrnParam->size = size;
		lrnParam->k = k;
		lrnParam->alpha = alpha;
		lrnParam->beta = beta;
		lrnParam->stage = 0;  // Square input to output
		lrnParam->dmaLinkAgent = dmaLinkAgent;
		lrnParam->cmxslice = (half *)getCMXSliceDataSection(firstShave + i);

		u32 readBackAddr = (u32)(&(lrnParam->cmxslice));
		GET_REG_WORD_VAL(readBackAddr);

		MODULE_ENTRY_DECL(mvLRN);
		startShave(i + firstShave, (u32)&MODULE_ENTRY(mvLRN), (u32)lrnParam);
    }
    waitShaves(firstShave, lastShave);
}
