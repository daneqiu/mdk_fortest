///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef SUMREDUCE_LEON_H_
#define SUMREDUCE_LEON_H_

#include <mv_types.h>

void sumReduce(fp16* input, u32 channels, u32 height,
                  u32 width, fp16* output,
                  u32 size, fp16 k, fp16 axis,
                  u32 firstShave, u32 lastShave,
				  u32 dmaLinkAgent);

#endif //SUMREDUCE_LEON_H_
