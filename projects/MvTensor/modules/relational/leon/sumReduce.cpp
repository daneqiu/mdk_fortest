///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

#include "sumReduce.h"
#include "../priv/sumReduceParam.h"
#include <mvTensorInternal.h>
#include <mvModuleHandle.h>

namespace
{
    using namespace mv::tensor;

    class SumReduce : public Layer
    {
        virtual void run_(const t_MvTensorParam *mvTensorParam, const t_MvTensorOp &op, const mv::tensor::Optimization &, const Resources &)
        {
            UNUSED(op);
        	half axis = ((half *)mvTensorParam->op->params)[0];
        	u32 k = mvTensorParam->output->dimZ;
            sumReduce((half *)mvTensorParam->input->data,
                            mvTensorParam->input->dimZ,
                            mvTensorParam->input->dimY,
                            mvTensorParam->input->dimX,
                            (half *)mvTensorParam->output->data,
                            0,
                            k, //((half *)mvTensorParam->biases->data)[0],
							axis, //((half *)mvTensorParam->biases->data)[1],
                            mvTensorParam->myriadResources->firstShave,
                            mvTensorParam->myriadResources->lastShave,
                            mvTensorParam->myriadResources->dmaLinkAgent);
        }
    };

    const ModuleHandle handles[] =
    {
        ModuleHandle().add<SumReduce>(kSumReduce)
    };
}

void sumReduce(fp16* input, u32 channels, u32 height,
                  u32 width, fp16* output,
                  u32 size, fp16 k, fp16 axis,
                  u32 firstShave, u32 lastShave,
				  u32 dmaLinkAgent)
{
    u32 nShaves = lastShave - firstShave + 1;
    if (nShaves > width*height)
    {
    	nShaves = width*height;
    	lastShave = firstShave + nShaves - 1;
    }
    // Divide between shaves
    u32 nlines_per_shave = (width * height + nShaves - 1) / nShaves;
    for(u32 i = 0; i < nShaves; i++)
    {
		// consider data as a 2D matrix, with num_cols=channels and num_lines=W*H
        t_MvSumReduceParam *sumReduceParam = useShaveParam<t_MvSumReduceParam>(i + firstShave);
        sumReduceParam->input = input + i * nlines_per_shave * channels;
        sumReduceParam->output = output + i * nlines_per_shave * channels;
        sumReduceParam->numlines = i == nShaves - 1 ?
			width * height - i * nlines_per_shave  : nlines_per_shave;
        sumReduceParam->channels = channels;
        sumReduceParam->size = size;
        sumReduceParam->k = k;
        sumReduceParam->axis = axis;
        sumReduceParam->stage = 0;  // Square input to output
        sumReduceParam->dmaLinkAgent = dmaLinkAgent;
        sumReduceParam->cmxslice = (half *)getCMXSliceDataSection(firstShave + i);

		u32 readBackAddr = (u32)(&(sumReduceParam->cmxslice));
		GET_REG_WORD_VAL(readBackAddr);

		MODULE_ENTRY_DECL(mvSumReduce);
		startShave(i + firstShave, (u32)&MODULE_ENTRY(mvSumReduce), (u32)sumReduceParam);
    }
    waitShaves(firstShave,lastShave);
}
