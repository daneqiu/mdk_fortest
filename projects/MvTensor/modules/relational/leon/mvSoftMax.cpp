///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

#include "mvSoftMax.h"
#include "../priv/mvSoftMaxParam.h"
#include <mvTensorInternal.h>
#include <mvModuleHandle.h>

namespace
{
    using namespace mv::tensor;

    class SoftMax : public Layer
    {
        virtual void run_(const t_MvTensorParam *mvTensorParam,
                const t_MvTensorOp &op,
                const mv::tensor::Optimization &,
                const Resources &)
        {
            s32 *axis = reinterpret_cast<s32 *>(op.params);
            mvTensorAssert(axis != NULL, "Softmax axis parameter = NULL");
            if((*axis) == C_AXIS)
            {
                for(int i= 0; i < mvTensorParam->input->dimX; i++)
                {
                    for(int j = 0; j < mvTensorParam->input->dimY; j++)
                    {
                        u32 offset = mvTensorParam->input->dimZ * (i*mvTensorParam->input->dimY + j);

                        softMaxAxisC((fp16*)mvTensorParam->input->data + offset,
                        mvTensorParam->input->dimZ,
                        (fp16*)mvTensorParam->output->data + offset,
                        mvTensorParam->myriadResources->dmaLinkAgent,
                        mvTensorParam->myriadResources->firstShave,
                        mvTensorParam->myriadResources->lastShave);
                    }
                }
            }
            else if((*axis) == H_AXIS)
            {
                softMaxAxisH(mvTensorParam);
            }
            else
            {
                mvTensorAssert(false, "Softmax unsupported axis parameter value");
            }
        }
    };

    const ModuleHandle handles[] =
    {
        ModuleHandle().add<SoftMax>(kSoftMax)
    };
}

void softMaxAxisC(fp16* input, u32 classes, fp16* output,
             u32 dmaLinkAgent,
             u32 firstShave, u32 lastShave)
{
    const int MaximumShaveBufferSize = MVTENSOR_HEAP_DATA_SIZE;
    const int StagingAreaSize = 2 * MVTENSOR_MAX_SHAVES * sizeof(u16);
    const u32 max_sliceC = (MaximumShaveBufferSize - StagingAreaSize) / 2;

    MODULE_ENTRY_DECL(mvSoftMaxAxisC);

    u32 i;
    u32 sliceC; // classes to process per SHAVE
    u32 remainingC;
    u32 offsetIO = 0;
    u32 classesMaxToProcess;
    u32 bufferOverflow = 0;
    u32 shaves_no = (lastShave - firstShave + 1);

    t_MvSoftMaxParam *softMaxParam[MVTENSOR_MAX_SHAVES];

    for (i = firstShave; i <= lastShave; ++i)
    {
        softMaxParam[i] = useShaveParam<t_MvSoftMaxParam>(i);

        // fill common parameters
        softMaxParam[i]->cmxslice = getCMXSliceDataSection(i);
        softMaxParam[i]->staging = softMaxParam[firstShave]->cmxslice + 2 * max_sliceC;
        softMaxParam[i]->dmaLinkAgent = dmaLinkAgent;
    }

    // initialize local max;
    for (i = 0; i < MVTENSOR_MAX_SHAVES; i++) {
        // minimum value for half (-65504)
        *((u16*) softMaxParam[firstShave]->staging + i) = 0xfbff;
    }
    // initialize local sum;
    for (i = 0; i < MVTENSOR_MAX_SHAVES; i++) {
        // minimum value for half (-65504)
        *((u16*) (softMaxParam[firstShave]->staging + MVTENSOR_MAX_SHAVES * 2) + i) = 0;
    }

    classesMaxToProcess = classes;

    do {
        sliceC = classesMaxToProcess / shaves_no;
        if (sliceC > max_sliceC) {
            sliceC = max_sliceC;
            remainingC = 0;
            classesMaxToProcess -= sliceC * shaves_no;
            if (classesMaxToProcess > 0)
                bufferOverflow = 1;
        } else {
            remainingC = classesMaxToProcess % shaves_no;
            classesMaxToProcess = 0;
        }

        // stage 1 ; get local max
        u32 at_leastC = sliceC;
        for (i = firstShave; i <= lastShave; i++) {
            sliceC = at_leastC;
            if (remainingC) {
                sliceC++;
                remainingC--;
            }
            if (sliceC == 0)
                break;
            //initialize SoftMax SHAVE param
            softMaxParam[i]->input = input + offsetIO;
            softMaxParam[i]->sliceC = sliceC;
            softMaxParam[i]->stage = 1;
            softMaxParam[i]->offset = 2 * i;
            softMaxParam[i]->bufferOverflow = bufferOverflow;
            softMaxParam[i]->output = output + offsetIO;

            u32 readBackAddr = (u32)(&(softMaxParam[i]->output));
            GET_REG_WORD_VAL(readBackAddr);

            startShave(i, (u32) & MODULE_ENTRY(mvSoftMaxAxisC), (u32) softMaxParam[i]);

            offsetIO += sliceC;
        }

        waitShaves(firstShave, lastShave);

    } while (classesMaxToProcess > 0);

    // stage 2 ; get global max
    for (i = firstShave; i <= firstShave; i++) {
        softMaxParam[i]->stage = 2;

        u32 readBackAddr = (u32)(&(softMaxParam[i]->stage));
        GET_REG_WORD_VAL(readBackAddr);

        startShave(i, (u32) & MODULE_ENTRY(mvSoftMaxAxisC), (u32) softMaxParam[i]);
    }

    waitShaves(firstShave, firstShave);

    // stage 3 ; get plusall, expall, local sum
    classesMaxToProcess = classes;
    offsetIO = 0;
    do {
        sliceC = classesMaxToProcess / shaves_no;
        if (sliceC > max_sliceC) {
            sliceC = max_sliceC;
            remainingC = 0;
            classesMaxToProcess -= sliceC * shaves_no;
            if (classesMaxToProcess > 0)
                bufferOverflow = 1;
        } else {
            remainingC = classesMaxToProcess % shaves_no;
            classesMaxToProcess = 0;
        }

        u32 at_leastC = sliceC;
        for (i = firstShave; i <= lastShave; i++) {
            sliceC = at_leastC;
            if (remainingC) {
                sliceC++;
                remainingC--;
            }
            if (sliceC == 0)
                break;
            //initialize SoftMax SHAVE param
            softMaxParam[i]->input = input + offsetIO;
            softMaxParam[i]->sliceC = sliceC;
            softMaxParam[i]->stage = 3;
            softMaxParam[i]->offset = 2 * i;
            softMaxParam[i]->bufferOverflow = bufferOverflow;
            softMaxParam[i]->output = output + offsetIO;

            u32 readBackAddr = (u32)(&(softMaxParam[i]->output));
            GET_REG_WORD_VAL(readBackAddr);

            startShave(i, (u32) & MODULE_ENTRY(mvSoftMaxAxisC), (u32) softMaxParam[i]);

            offsetIO += sliceC;
        }

        waitShaves(firstShave, lastShave);

    } while (classesMaxToProcess > 0);

//    printf("Softmax: A: %x\n",((fp16*)softMaxParam[0].input)[0]);
//    printf("Softmax: B: %x\n",((fp16*)softMaxParam[0].output)[0]);

    // stage 4 ; get global sum
    for (i = firstShave; i <= firstShave; i++) {
        softMaxParam[i]->stage = 4;

        u32 readBackAddr = (u32)(&(softMaxParam[i]->stage));
        GET_REG_WORD_VAL(readBackAddr);

        startShave(i, (u32) & MODULE_ENTRY(mvSoftMaxAxisC), (u32) softMaxParam[i]);
    }
    waitShaves(firstShave, firstShave);

    // stage 5 ; get softmax
    classesMaxToProcess = classes;
    offsetIO = 0;
    do {
        sliceC = classesMaxToProcess / shaves_no;
        if (sliceC > max_sliceC) {
            sliceC = max_sliceC;
            remainingC = 0;
            classesMaxToProcess -= sliceC * shaves_no;
            if (classesMaxToProcess > 0)
                bufferOverflow = 1;
        } else {
            remainingC = classesMaxToProcess % shaves_no;
            classesMaxToProcess = 0;
        }

        u32 at_leastC = sliceC;
        for (i = firstShave; i <= lastShave; i++) {
            sliceC = at_leastC;
            if (remainingC) {
                sliceC++;
                remainingC--;
            }
            if (sliceC == 0)
                break;
            //initialize SoftMax SHAVE param
            softMaxParam[i]->input = input + offsetIO;
            softMaxParam[i]->sliceC = sliceC;
            softMaxParam[i]->stage = 5;
            softMaxParam[i]->offset = 2 * i;
            softMaxParam[i]->bufferOverflow = bufferOverflow;
            softMaxParam[i]->output = output + offsetIO;

            u32 readBackAddr = (u32)(&(softMaxParam[i]->output));
            GET_REG_WORD_VAL(readBackAddr);

            startShave(i, (u32) & MODULE_ENTRY(mvSoftMaxAxisC), (u32) softMaxParam[i]);

            offsetIO += sliceC;
        }

        waitShaves(firstShave, lastShave);

    } while (classesMaxToProcess > 0);
}

void softMaxAxisH(const t_MvTensorParam *mvTensorParam)
{
    MODULE_ENTRY_DECL(mvSoftMaxAxisH);

    s32 first_shave = mvTensorParam->myriadResources->firstShave;
    s32 last_shave  = mvTensorParam->myriadResources->lastShave;

    u32 no_shaves = last_shave - first_shave + 1;

    t_MvSoftMaxParam *softMaxParam[MVTENSOR_MAX_SHAVES];

    for(u32 shave_i = 0; shave_i < no_shaves; ++shave_i)
    {
        softMaxParam[shave_i] = useShaveParam<t_MvSoftMaxParam>(first_shave + shave_i);

        softMaxParam[shave_i]->input     = (fp16 *)mvTensorParam->input->data;
        softMaxParam[shave_i]->output    = (fp16 *)mvTensorParam->output->data;
        softMaxParam[shave_i]->cmxslice  = getCMXSliceDataSection(first_shave + shave_i);
        softMaxParam[shave_i]->width     = mvTensorParam->input->dimX;
        softMaxParam[shave_i]->height    = mvTensorParam->input->dimY;
        softMaxParam[shave_i]->channels  = mvTensorParam->input->dimZ;
        softMaxParam[shave_i]->no_shaves = no_shaves;
        softMaxParam[shave_i]->shave_no  = shave_i;
        softMaxParam[shave_i]->axis      = H_AXIS;
        softMaxParam[shave_i]->dmaLinkAgent =
            mvTensorParam->myriadResources->dmaLinkAgent;

        u32 readBackAddr = (u32)(&(softMaxParam[shave_i]->axis));
        GET_REG_WORD_VAL(readBackAddr);

        startShave(first_shave + shave_i,
                (u32) & MODULE_ENTRY(mvSoftMaxAxisH),
                (u32) softMaxParam[shave_i]);
    }

    waitShaves(first_shave, last_shave);
}
