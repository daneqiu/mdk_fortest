///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef SOFTMAX_LEON_H_
#define SOFTMAX_LEON_H_

#include <mv_types.h>
#include <mvTensor.h>

void softMaxAxisC(fp16* input, u32 classes, fp16* output,
             u32 dmaLinkAgent,
             u32 firstShave, u32 lastShave);

void softMaxAxisH(const t_MvTensorParam *mvTensorParam);

#endif // SOFTMAX_LEON_H_
