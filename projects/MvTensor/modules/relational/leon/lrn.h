///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef LRN_LEON_H_
#define LRN_LEON_H_

#include <mv_types.h>

void lrn(fp16* input, u32 channels, u32 height,
                  u32 width, fp16* output,
                  u32 size, fp16 k, fp16 alpha, fp16 beta,
                  u32 firstShave, u32 lastShave,
				  u32 dmaLinkAgent);

#endif //LRN_LEON_H_
