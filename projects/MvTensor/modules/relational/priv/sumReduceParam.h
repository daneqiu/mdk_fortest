///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
/// 
/// \details
///


#ifndef _MV_SUMREDUCEPARAM_H
#define _MV_SUMREDUCEPARAM_H

#include <mv_types.h>

#ifdef __MOVICOMPILE__
    #include <moviVectorTypes.h>
#else
    typedef fp16 half;
#endif

/// MvSumReduce global parameters structure
typedef struct
{
    half* input;
    half* output;
    u32 channels;
    u32 stage;
    u32 numlines;
    u32 size;
    u32 k;
    half axis;
    half *cmxslice;
    u32 dmaLinkAgent;
} t_MvSumReduceParam;

#endif // _MV_SUMREDUCEPARAM_H
