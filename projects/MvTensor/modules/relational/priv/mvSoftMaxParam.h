///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
/// 
/// \details
///


#ifndef _MV_SOFTMAXPARAM_H
#define _MV_SOFTMAXPARAM_H

#include <mv_types.h>

#ifdef __MOVICOMPILE__
    #include <moviVectorTypes.h>
#else
    typedef fp16 half;
#endif

enum SoftMaxAxis { C_AXIS = 1, H_AXIS, W_AXIS };

/// MvSoftMax global parameters structure
typedef struct
{
    half* input;
    u32 sliceC;
    u32 stage;
    u8* staging;
    u8* cmxslice;
    u32 offset;
    u32 bufferOverflow;
    u32 dmaLinkAgent;
    half* output;

    // Parameters specific to H axis implementation.
    s32 height;
    s32 width;
    s32 channels;
    s32 shave_no;
    s32 no_shaves;
    s32 axis;
} t_MvSoftMaxParam;

#endif // _MV_SOFTMAXPARAM_H
