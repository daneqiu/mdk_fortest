///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief Leon wrappers for CNN post operations.
///

#include <mv_types.h>

#include "../priv/mvPriorBoxParam.h"
#include <mvTensorInternal.h>
#include <mvModuleHandle.h>

MODULE_ENTRY_DECL(mvPriorBox);

namespace
{
    using namespace mv::tensor;

    class PriorBox : public Layer
    {
        private:
            virtual void run_(const t_MvTensorParam *mvTensorParam,
                    const t_MvTensorOp &op, const Optimization &,
                    const Resources &res);
    };

    void PriorBox::run_(const t_MvTensorParam *mvTensorParam,
            const t_MvTensorOp &op,
            const mv::tensor::Optimization &,
            const Resources &res)
    {
        fp16 *output     = (fp16*)mvTensorParam->output->data;
        s32  in_width    = mvTensorParam->input->dimX;
        s32  in_height   = mvTensorParam->input->dimY;
        s32  img_w       = mvTensorParam->weights->dimX;
        s32  img_h       = mvTensorParam->weights->dimY;
        u32 lastShave    = mvTensorParam->myriadResources->lastShave;
        u32 firstShave   = mvTensorParam->myriadResources->firstShave;
        s32 dmaLinkAgent = mvTensorParam->myriadResources->dmaLinkAgent;
        fp32 *params     = (fp32 *)op.params;
        mvTensorAssert(params != NULL,
                "PriorBox layer parameters buffer = NULL");

        // TODO: Do a multi-shave implementation. Hardcoded to 1 shave for the
        // forseeable future.
        lastShave = firstShave;
        t_MvPriorBoxParam *priorBoxParam[MVTENSOR_MAX_SHAVES];

        for(u32 i = firstShave; i <= lastShave; i++)
        {
            priorBoxParam[i]               = useShaveParam<t_MvPriorBoxParam>(i);
            priorBoxParam[i]->output       = output;
            priorBoxParam[i]->params       = params;
            priorBoxParam[i]->width        = in_width;
            priorBoxParam[i]->height       = in_height;
            priorBoxParam[i]->imgH         = img_h;
            priorBoxParam[i]->imgW         = img_w;
            priorBoxParam[i]->dmaLinkAgent = dmaLinkAgent;
            priorBoxParam[i]->cmxslice     = (half *)getCMXSliceDataSection(i);

            startShave(i, (u32)&MODULE_ENTRY(mvPriorBox), (u32)priorBoxParam[i]);
        }

        waitShaves(firstShave, lastShave);

        res.shaveCache.writeback();
        res.shaveCache.invalidate();

        return;
    }

    const ModuleHandle handles[] =
    {
        ModuleHandle().add<PriorBox>(kPriorBox)
    };
}
