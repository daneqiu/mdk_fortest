///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief Leon wrappers for CNN post operations.
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <mv_types.h>

#include "../priv/mvPermuteParam.h"
#include <mvTensorInternal.h>
#include <mvModuleHandle.h>

#include <algorithm>

MODULE_ENTRY_DECL(mvPermute);

namespace
{
    using namespace mv::tensor;

    // Use maximum 2 SHAVEs because this layer assumes only DMA transfers.
    static const s32 MAX_SHAVES_USED = 2;

    class Permute : public Layer
    {
        private:
            virtual void run_(const t_MvTensorParam *mvTensorParam,
                    const t_MvTensorOp &op, const Optimization &,
                    const Resources &res);
    };

    void Permute::run_(const t_MvTensorParam *mvTensorParam,
            const t_MvTensorOp &op,
            const mv::tensor::Optimization &,
            const Resources &res)
    {
        fp16 *input         = (fp16*)mvTensorParam->input->data;
        fp16 *output        = (fp16*)mvTensorParam->output->data;
        s32  in_width       = mvTensorParam->input->dimX;
        s32  in_height      = mvTensorParam->input->dimY;
        s32  in_channels    = mvTensorParam->input->dimZ;
        u32  *permute_order = (u32 *)op.params;
        mvTensorAssert(permute_order != NULL,
                "Permute layer parameters buffer = NULL");

        s32 first_shave = mvTensorParam->myriadResources->firstShave;
        s32 last_shave  = std::min((int)(first_shave + MAX_SHAVES_USED - 1),
                (int)mvTensorParam->myriadResources->lastShave);
        s32 shaves_used  = last_shave - first_shave + 1;
        s32 shave_height = (in_height + shaves_used - 1) / shaves_used;

        u32 dmaLinkAgent = mvTensorParam->myriadResources->dmaLinkAgent;

        t_MvPermuteParams *permuteParams[MVTENSOR_MAX_SHAVES];
        for(s32 shave_idx = 0; shave_idx < shaves_used; shave_idx++)
        {
            s32 shave_nr = first_shave + shave_idx;
            permuteParams[shave_nr] = useShaveParam<t_MvPermuteParams>(shave_nr);
            permuteParams[shave_nr]->input         = input;
            permuteParams[shave_nr]->output        = output;
            permuteParams[shave_nr]->in_width      = in_width;
            permuteParams[shave_nr]->in_height     = in_height;
            permuteParams[shave_nr]->in_channels   = in_channels;
            permuteParams[shave_nr]->start_height  = shave_idx * shave_height;
            permuteParams[shave_nr]->end_height    = std::min((int)in_height,
                  (int)((shave_idx + 1) * shave_height));
            permuteParams[shave_nr]->permute_order = permute_order;
            permuteParams[shave_nr]->dmaLinkAgent  = dmaLinkAgent;

            startShave(shave_nr,
                    (u32)&MODULE_ENTRY(mvPermute),
                    (u32)permuteParams[shave_nr]);
        }

        waitShaves(first_shave, last_shave);

        res.shaveCache.writeback();
        res.shaveCache.invalidate();
    }

    const ModuleHandle handles[] =
    {
        ModuleHandle().add<Permute>(kPermute)
    };
}
