///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief Leon wrappers for CNN post operations.
///

#include <mv_types.h>

#include <malloc.h>
#include <mvMacros.h>

#include "mvTensorInternal.h"
#include "mvModuleHandle.h"
#include "../priv/mvDetectionOutputParam.h"

#define ALIGN_VALUE 64

MODULE_ENTRY_DECL(detection_output_core);
MODULE_ENTRY_DECL(det_out_apply_nms);
MODULE_ENTRY_DECL(det_out_decode_bboxes);

namespace
{
    using namespace mv::tensor;

    class DetectionOutput : public Layer
    {
        private:
            virtual void run_(const t_MvTensorParam *mvTensorParam,
                    const t_MvTensorOp &op, const Optimization &,
                    const Resources &res);
    };

    void DetectionOutput::run_(const t_MvTensorParam *mvTensorParam,
            const t_MvTensorOp &op,
            const Optimization &,
            const Resources &res)
    {
        t_MvTensorMyriadResources *myriad_resources = mvTensorParam->myriadResources;

        // input->data points to the mbox_loc_data
        t_mvTensorGenData *input   = mvTensorParam->input;
        // weights->data points to the mbox_conf_data
        t_mvTensorGenData *weights = mvTensorParam->weights;
        // biases->data points to the mbox_priorbox_data
        t_mvTensorGenData *biases  = mvTensorParam->biases;

        t_mvTensorGenData *output  = mvTensorParam->output;

        // Detection layer parameters
        t_DetOutputLayerParams *layer_parameters =
            reinterpret_cast<t_DetOutputLayerParams *>(op.params);
        mvTensorAssert(layer_parameters != NULL,
                "DetectionOutput layer parameters buffer = NULL");

        // Each prior consists of 4 values.
        s32 num_priors      = mvTensorParam->input->dimZ / 4;
        s32 num_classes     = layer_parameters->num_classes;
        s32 num_loc_classes = layer_parameters->share_location ? 1 : num_classes;

        // Allocate temporary buffer for BBoxes.
        SizedBBox *all_decoded_bboxes_temp = (SizedBBox *)malloc(
                sizeof(SizedBBox) * num_priors * num_loc_classes + ALIGN_VALUE);
        SizedBBox *all_decoded_bboxes_temp_aligned =
            ALIGN_UP(all_decoded_bboxes_temp, ALIGN_VALUE);
        mvTensorAssert(all_decoded_bboxes_temp_aligned != NULL,
                "Cannot allocate mem for all_decoded_bboxes_temp\n");

        // Allocate temporary buffer for BBoxes.
        ConfIndex *all_indices_temp = (ConfIndex *)malloc(
                sizeof(ConfIndex) * num_classes * num_priors + ALIGN_VALUE);
        ConfIndex *all_indices_temp_aligned =
            ALIGN_UP(all_indices_temp, ALIGN_VALUE);
        mvTensorAssert(all_indices_temp_aligned != NULL,
                "cannot allocate mem for all_indices_temp\n");

        s32 *num_detections_temp = (s32 *)malloc(sizeof(s32) * num_classes
                + ALIGN_VALUE);
        s32 *num_detections_temp_aligned =
            ALIGN_UP(num_detections_temp, ALIGN_VALUE);
        mvTensorAssert(num_detections_temp_aligned != NULL,
                "cannot allocate mem for num_detections_temp\n");

        s32 nms_top_k = layer_parameters->nms_top_k;
        ConfIndexLabel *top_k_elimination_temp = (ConfIndexLabel *)malloc(
                sizeof(ConfIndexLabel) * num_classes * nms_top_k + ALIGN_VALUE);
        ConfIndexLabel *top_k_elimination_temp_aligned =
            ALIGN_UP(top_k_elimination_temp, ALIGN_VALUE);
        mvTensorAssert(top_k_elimination_temp_aligned!= NULL,
                "cannot allocate mem for top_k_elimination\n");

        s32 first_shave = myriad_resources->firstShave;
        s32 no_shaves = myriad_resources->lastShave - first_shave + 1;

        s32 num_classes_per_shave = num_classes / no_shaves;
        s32 num_remainder_classes = num_classes % no_shaves;
        s32 num_bboxes_per_shave  = num_priors / no_shaves;
        s32 num_remainder_bboxes  = num_priors % no_shaves;

        t_DetectionOutputParam *detOutParam[MVTENSOR_MAX_SHAVES];

        for(s32 shave_i = 0; shave_i < no_shaves; ++shave_i)
        {
            detOutParam[shave_i] = useShaveParam<t_DetectionOutputParam>(
                    first_shave + shave_i);
            detOutParam[shave_i]->mbox_loc_data =
                reinterpret_cast<fp16 *>(input->data);
            detOutParam[shave_i]->mbox_conf_data =
                reinterpret_cast<fp16 *>(weights->data);
            detOutParam[shave_i]->mbox_priorbox_data =
                reinterpret_cast<fp16 *>(biases->data);
            detOutParam[shave_i]->output =
                reinterpret_cast<fp16 *>(output->data);
            detOutParam[shave_i]->num_priors = num_priors;
            detOutParam[shave_i]->cmxslice =
                getCMXSliceDataSection(first_shave + shave_i);
            detOutParam[shave_i]->dmaLinkAgent = myriad_resources->dmaLinkAgent;
            detOutParam[shave_i]->layer_parameters = *layer_parameters;

            detOutParam[shave_i]->num_shaves = no_shaves;
            detOutParam[shave_i]->shave_num  = shave_i;

            detOutParam[shave_i]->all_decoded_bboxes_temp =
                all_decoded_bboxes_temp_aligned;
            detOutParam[shave_i]->all_indices_temp = all_indices_temp_aligned;
            detOutParam[shave_i]->top_k_elimination_temp =
                top_k_elimination_temp_aligned;
            detOutParam[shave_i]->num_detections_temp = num_detections_temp_aligned;

            if(shave_i == (no_shaves - 1))
            {
                u32 readBackAddr = (u32)(
                        &(detOutParam[first_shave + shave_i]->num_detections_temp));
                GET_REG_WORD_VAL(readBackAddr);
            }
        }

        for(s32 shave_i = 0; shave_i < no_shaves; ++shave_i)
        {
            startShave(first_shave + shave_i,
                    (u32)&MODULE_ENTRY(det_out_decode_bboxes),
                    (u32)detOutParam[shave_i]);
        }
        waitShaves(myriad_resources->firstShave, myriad_resources->lastShave);

        for(s32 shave_i = 0; shave_i < no_shaves; ++shave_i)
        {
            startShave(first_shave + shave_i,
                    (u32)&MODULE_ENTRY(det_out_apply_nms),
                    (u32)detOutParam[shave_i]);
        }
        waitShaves(myriad_resources->firstShave, myriad_resources->lastShave);

        startShave(first_shave,
                (u32)&MODULE_ENTRY(detection_output_core),
                (u32)detOutParam[0]);
        waitShaves(myriad_resources->firstShave, myriad_resources->firstShave);

        res.shaveCache.writeback();
        res.shaveCache.invalidate();

        free(all_decoded_bboxes_temp);
        free(all_indices_temp);
        free(top_k_elimination_temp);
        free(num_detections_temp);
    }

    const ModuleHandle handles[] =
    {
        ModuleHandle().add<DetectionOutput>(kDetectionOutput)
    };
}
