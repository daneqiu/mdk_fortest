///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief Leon wrappers for CNN post operations.
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <mv_types.h>

#include "mvTensorInternal.h"
#include "mvModuleHandle.h"
#include "../priv/mvNormalizeParam.h"

MODULE_ENTRY_DECL(mvNormalize);

namespace
{
    using namespace mv::tensor;

    class Normalize : public Layer
    {
        private:
            virtual void run_(const t_MvTensorParam *mvTensorParam,
                    const t_MvTensorOp &op, const Optimization &,
                    const Resources &res);
    };

    void Normalize::run_(const t_MvTensorParam *mvTensorParam,
            const t_MvTensorOp &op,
            const Optimization &,
            const Resources &res)
    {
        fp16 *input   = (fp16*)mvTensorParam->input->data;
        fp16 *output  = (fp16*)mvTensorParam->output->data;
        fp16 *scale   = (fp16*)mvTensorParam->weights->data;
        s32  width    = mvTensorParam->input->dimX;
        s32  height   = mvTensorParam->input->dimY;
        s32  channels = mvTensorParam->input->dimZ;

        mvTensorAssert((s32 *)op.params != NULL,
                "Normalize layer parameters buffer = NULL");
        u32 across_spatial = ((u32*)op.params)[0];
        u32 channel_shared = ((u32*)op.params)[1];

        u32 lastShave    = mvTensorParam->myriadResources->lastShave;
        u32 firstShave   = mvTensorParam->myriadResources->firstShave;
        s32 dmaLinkAgent = mvTensorParam->myriadResources->dmaLinkAgent;
        u32 nShaves = lastShave - firstShave + 1;
        u32 shave_nr;

        t_MvNormalizeParam *normalizeParam[MVTENSOR_MAX_SHAVES];

        // Divide between shaves
        u32 nlines_per_shave = (width * height + nShaves - 1) / nShaves;
        for(u32 i = 0; i < nShaves; i++)
        {
            normalizeParam[i] = useShaveParam<t_MvNormalizeParam>(firstShave + i);
            // consider data as a 2D matrix, with num_cols=channels and num_lines=W*H
            normalizeParam[i]->input          = input +
                i * nlines_per_shave * channels;
            normalizeParam[i]->output         = output +
                i * nlines_per_shave * channels;
            normalizeParam[i]->numlines       = (i == nShaves - 1) ?
              width * height - i * nlines_per_shave  : nlines_per_shave;
            normalizeParam[i]->channels       = channels;
            normalizeParam[i]->scale          = scale;
            normalizeParam[i]->across_spatial = across_spatial;
            normalizeParam[i]->channel_shared = channel_shared;
            normalizeParam[i]->dmaLinkAgent   = dmaLinkAgent;
            normalizeParam[i]->cmxslice       = (half *)getCMXSliceDataSection(
                    firstShave + i);

            startShave(firstShave + i,
                    (u32)&MODULE_ENTRY(mvNormalize),
                    (u32)normalizeParam[i]);
        }

        waitShaves(firstShave, lastShave);

        res.shaveCache.writeback();
        res.shaveCache.invalidate();
    }

    const ModuleHandle handles[] =
    {
        ModuleHandle().add<Normalize>(kNormalize)
    };
}
