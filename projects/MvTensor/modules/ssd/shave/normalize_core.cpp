///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect code
///

#include <mv_types.h>
#include <math.h>
#include <moviVectorTypes.h>
#include "normalize_core.h"
#include "mvtMulAB.h"
#include <mvTensorDma.h>
#include <mvTensorConfig.h>

#define INPUT_BPP       2

#if (__MOVICOMPILE_MINOR__ == 50) && (__MOVICOMPILE_PATCH__ < 79)
#define LOCAL_HALF_POWER __hpow
#else
#define LOCAL_HALF_POWER __pows
#endif

void mvNormalize(t_MvNormalizeParam *p)
{
    using namespace mv::tensor;
    dma::Config dmaConfig = { 1, p->dmaLinkAgent };
    dma::User dmaUser(dmaConfig);

    u32  i;
    u32  C = p->channels;
    u32  pad, maxLines, Cpadded;
    u32  cmxRem = MVTENSOR_HEAP_DATA_SIZE;
    half *pCmx = p->cmxslice;
    half *linesBuffer, *normBuffer, *scaleBuffer, *sumMultiplier;

    // Optimization ideas:
    //   1. use asm of either matrixVectMult or matmulBT or mvtMultAB
    //   2. make sure that all cmx buffers are aligned to 8

    // TODO: <Not implemented>: accross_spatial=true
    // TODO: <Not implemented>: channel_shared=true

    // pad C in order to align to the next multiple of 8
    Cpadded = (C + 7) &~0x7;
    pad = Cpadded - C;

    // increment buffers with Cpadded, in order to keep buffers aligned
    // (since Cpadded is aligned to 8, the buffers will be aligned to 16,
    //  that p->cmxslice is also aligned to 16)
    scaleBuffer = pCmx;
    pCmx   += Cpadded;
    cmxRem -= Cpadded;

    sumMultiplier = pCmx;
    pCmx   += Cpadded;
    cmxRem -= Cpadded;

    maxLines = (cmxRem/2/INPUT_BPP - 7 - pad) / Cpadded;

    linesBuffer  =  pCmx;
    pCmx   += cmxRem/2/INPUT_BPP;
    cmxRem -= cmxRem/2/INPUT_BPP;
    normBuffer =  pCmx;

    for (i=0; i<C; i++)
    {
        sumMultiplier[i] = (half)1.0;
    }
    for (;i<Cpadded; i++)
    {
        sumMultiplier[i] = (half)0.0;
    }

    dma::Task(dmaUser).start(
            (u8 *)(p->scale),
            (u8 *)(scaleBuffer),
            C * INPUT_BPP);

    for(i = 0; i < p->numlines; i += maxLines)
    {
        u32 nlines = p->numlines - i;

        if(nlines > maxLines)
            nlines = maxLines;
        u32 nelem_padded = nlines * Cpadded;

        // extend nelem_padded to the next multiple of 8, as this is a
        // restriction of the matmul asm kernel
        nelem_padded = (nelem_padded + 7) & ~7;

        dma::Task(dmaUser).start(
                (u8 *)(p->input + i*C),         // src
                (u8 *)(linesBuffer),            // dst
                nlines * C * INPUT_BPP,         // byte length
                C * INPUT_BPP,                  // src width
                C * INPUT_BPP,                  // dst width
                C * INPUT_BPP,                  // src stride
                (Cpadded) * INPUT_BPP);         // dst stride

        for(u32 j = 0; j < nlines; j++)
        {
            for(u32 k = 0; k < pad; k++)
            {
                *(linesBuffer + j*(C+pad) + C + k) = 0.0;
            }
        }

        for(u32 j = 0; j < nelem_padded; j += 8)
        {
            half8 *t = reinterpret_cast<half8*>(linesBuffer + j);
            *t = *t * *t;
        }

        mvtMulAB_hhhh_c(linesBuffer, sumMultiplier, normBuffer, nlines, Cpadded, 1, Cpadded, 1, 1);


//#define ACCURATE_POW
#ifdef ACCURATE_POW
        for(u32 j = 0; j < nlines; j+=8)
        {
            half8* __restrict__ tn = reinterpret_cast<half8*>(normBuffer + j);
            half8 base = *tn;
            half beta = -0.5;
            (*tn)[0] = LOCAL_HALF_POWER(base[0], beta);
            (*tn)[1] = LOCAL_HALF_POWER(base[1], beta);
            (*tn)[2] = LOCAL_HALF_POWER(base[2], beta);
            (*tn)[3] = LOCAL_HALF_POWER(base[3], beta);
            (*tn)[4] = LOCAL_HALF_POWER(base[4], beta);
            (*tn)[5] = LOCAL_HALF_POWER(base[5], beta);
            (*tn)[6] = LOCAL_HALF_POWER(base[6], beta);
            (*tn)[7] = LOCAL_HALF_POWER(base[7], beta);
        }
#else
        for(u32 j = 0; j < nlines; j += 8)
        {
            half8* __restrict__ tn = reinterpret_cast<half8*>(normBuffer + j);

            (*tn)[0] = __builtin_shave_sau_rqt_f16_l_r((*tn)[0]);
            (*tn)[1] = __builtin_shave_sau_rqt_f16_l_r((*tn)[1]);
            (*tn)[2] = __builtin_shave_sau_rqt_f16_l_r((*tn)[2]);
            (*tn)[3] = __builtin_shave_sau_rqt_f16_l_r((*tn)[3]);
            (*tn)[4] = __builtin_shave_sau_rqt_f16_l_r((*tn)[4]);
            (*tn)[5] = __builtin_shave_sau_rqt_f16_l_r((*tn)[5]);
            (*tn)[6] = __builtin_shave_sau_rqt_f16_l_r((*tn)[6]);
            (*tn)[7] = __builtin_shave_sau_rqt_f16_l_r((*tn)[7]);
        }
#endif // ACCURATE_POW

        // Normalize: compute in = in * norm
        dma::Task(dmaUser).start(
                (u8 *)(p->input + i*C),         // src
                (u8 *)(linesBuffer),            // dst
                nlines * C * INPUT_BPP,         // byte length
                C * INPUT_BPP,                  // src width
                C * INPUT_BPP,                  // dst width
                C * INPUT_BPP,                  // src stride
                (Cpadded) * INPUT_BPP);         // dst stride

        for(u32 j = 0; j < nlines; j++)
        {
            for(u32 k = 0; k < Cpadded; k+=8)
            {
                half8 *ti = reinterpret_cast<half8*>(linesBuffer + j*(Cpadded) + k);
                half8 *ts = reinterpret_cast<half8*>(scaleBuffer + k);
                half *tn = normBuffer + j;
                *ti = *ti * *tn;
                *ti = *ti * *ts;
            }
        }

        // output DMA
        dma::Task(dmaUser).start(
                (u8 *)(linesBuffer),            // src
                (u8 *)(p->output + i*C),        // dst
                nlines * C * INPUT_BPP,         // byte length
                C * INPUT_BPP,                  // src width
                C * INPUT_BPP,                  // dst width
                (Cpadded) * INPUT_BPP,          // src stride
                C * INPUT_BPP);                 // dst stride
    }
}
