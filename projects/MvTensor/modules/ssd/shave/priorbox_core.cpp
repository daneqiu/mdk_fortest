///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect code
///

#include <mv_types.h>
#include <math.h>
#include <moviVectorTypes.h>
#include "priorbox_core.h"

void mvPriorBox(t_MvPriorBoxParam *p)
{
    u32 i, param_index;
    half *top_data = p->output;
    fp32 *pParams = p->params;
    u32 flip, clip;
    u32 img_height, img_width;
    u32 layer_height, layer_width;
    u32 min_sizes_size, max_sizes_size, aspect_ratios_size, variances_size;
    float step_w, step_h;
    float offset;
    u32 num_priors, idx, dim;

    // TODO: (hardcoded values) assign correct maximum sizes
    u32 min_sizes[4];
    u32 max_sizes[4];
    float variance[4];
    float aspect_ratios[4];
    // currently parsing only params used in SSD
    // TODO: parse all other parameters
    ////////////// end hardcoded values

    img_height = p->imgH;
    img_width = p->imgW;
    layer_height = p->height;
    layer_width = p->width;

    min_sizes_size      = (u32)pParams[0];
    max_sizes_size      = (u32)pParams[1];
    aspect_ratios_size  = (u32)pParams[2];
    variances_size      = (u32)pParams[3];
    flip                = (u32)(pParams[4]);
    clip                = (u32)(pParams[5]);
    param_index         = 6;

    for (i=0; i<min_sizes_size; i++)
        min_sizes[i] = (u32)(pParams[param_index++]);

    for (i=0; i<max_sizes_size; i++)
        max_sizes[i] = (u32)(pParams[param_index++]);

    if (!flip)
    {
        for (i=0; i<aspect_ratios_size; i++)
            aspect_ratios[i] = pParams[param_index++];
    }
    else
    {
        for (i=0; i<aspect_ratios_size; i++)
        {
            aspect_ratios[2*i]   = pParams[param_index];
            aspect_ratios[2*i+1] = 1.0f/pParams[param_index++];
        }
        aspect_ratios_size *= 2;
    }

    for (i=0; i<variances_size; i++)
        variance[i] = pParams[param_index++];

    float param_step_w = (float)pParams[param_index++];
    float param_step_h = (float)pParams[param_index++];
    if(param_step_w == 0.0 || param_step_h == 0.0)
    {
        step_w = (float)(img_width) / layer_width;
        step_h = (float)(img_height) / layer_height;
    }
    else
    {
        step_w = param_step_w;
        step_h = param_step_h;
    }

    offset = (float)pParams[param_index];

    num_priors = min_sizes_size + max_sizes_size + min_sizes_size * aspect_ratios_size ;

    dim = layer_height * layer_width * num_priors * 4;
    idx = 0;

    for (u32 h = 0; h < layer_height; ++h)
    {
        for (u32 w = 0; w < layer_width; ++w)
        {
            float center_x = (w + offset) * step_w;
            float center_y = (h + offset) * step_h;
            float box_width, box_height;

            for (u32 s = 0; s < min_sizes_size; ++s)
            {
                u32 min_size_ = min_sizes[s];
                // first prior: aspect_ratio = 1, size = min_size
                box_width = box_height = min_size_;
                // xmin
                top_data[idx] = (half)((center_x - box_width / 2.) / img_width);
                idx += 2;
                // ymin
                top_data[idx] = (half)((center_y - box_height / 2.) / img_height);
                idx += 2;
                // xmax
                top_data[idx] = (half)((center_x + box_width / 2.) / img_width);
                idx += 2;
                // ymax
                top_data[idx] = (half)((center_y + box_height / 2.) / img_height);
                idx += 2;

                if (max_sizes_size > 0)
                {
                    int max_size_ = max_sizes[s];
                    // second prior: aspect_ratio = 1, size = sqrt(min_size * max_size)
                    box_width = box_height = sqrtf(min_size_ * max_size_);
                    // xmin
                    top_data[idx] = (half)((center_x - box_width / 2.) / img_width);
                    idx += 2;
                    // ymin
                    top_data[idx] = (half)((center_y - box_height / 2.) / img_height);
                    idx += 2;
                    // xmax
                    top_data[idx] = (half)((center_x + box_width / 2.) / img_width);
                    idx += 2;
                    // ymax
                    top_data[idx] = (half)((center_y + box_height / 2.) / img_height);
                    idx += 2;
                }

                // rest of priors
                for (u32 r = 0; r < aspect_ratios_size; ++r)
                {
                    float ar = aspect_ratios[r];
                    if (fabs(ar - 1.) < 1e-6)
                    {
                        continue;
                    }
                    box_width = min_size_ * sqrtf(ar);
                    box_height = min_size_ / sqrtf(ar);
                    // xmin
                    top_data[idx] = (half)((center_x - box_width / 2.) / img_width);
                    idx += 2;
                    // ymin
                    top_data[idx] = (half)((center_y - box_height / 2.) / img_height);
                    idx += 2;
                    // xmax
                    top_data[idx] = (half)((center_x + box_width / 2.) / img_width);
                    idx += 2;
                    // ymax
                    top_data[idx] = (half)((center_y + box_height / 2.) / img_height);
                    idx += 2;
                }
            }
        }
    }

    // clip the prior's coordidate such that it is within [0, 1]
    if (clip)
    {
        for (u32 d = 0; d < dim; ++d) 
        {
            top_data[d * 2] = fminf(fmaxf(top_data[d * 2], 0.), 1.);
        }
    }

    // set the variance.
    top_data += 1;
    if (variances_size == 1)
    {
        for (i = 0; i < dim; ++i)
        {
            top_data[i * 2] = (half)variance[0];
        }
    }
    else
    {
        u32 count = 0;
        for (u32 h = 0; h < layer_height; ++h)
        {
            for (u32 w = 0; w < layer_width; ++w)
            {
                for (u32 i = 0; i < num_priors; ++i)
                {
                    for (u32 j = 0; j < 4; ++j)
                    {
                        top_data[count] = (half)variance[j];
                        count += 2;
                    }
                }
            }
        }
    }
}
