///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief   Detection output layer.
///

#include <mv_types.h>
#include <math.h>
#include <algorithm>

#include <moviVectorTypes.h>
#include <mvTensorConfig.h>
#include "detection_output_core.h"
#include <mvTensorDma.h>

#define CMX_SIZE MVTENSOR_HEAP_DATA_SIZE

namespace
{
    bool sort_conf_index(const ConfIndex& conf_idx_0,
            const ConfIndex& conf_idx_1)
    {
        if(conf_idx_0.conf >= conf_idx_1.conf)
        {
            if(conf_idx_0.conf > conf_idx_1.conf)
            {
                return true;
            }
            else
            {
                return conf_idx_0.index < conf_idx_1.index;
            }
        }

        return false;
    }

    bool sort_conf_index_label(const ConfIndexLabel& conf_idx_0,
            const ConfIndexLabel& conf_idx_1)
    {
        return conf_idx_0.conf_index.conf > conf_idx_1.conf_index.conf;
    }

    inline fp16 jaccard_overlap(const SizedBBox *bbox0,
            const SizedBBox *bbox1,
            const bool normalized = true)
    {
        using namespace std;
        // Check if there is overlap.
        if (bbox1->bbox.xmin > bbox0->bbox.xmax ||
            bbox1->bbox.xmax < bbox0->bbox.xmin ||
            bbox1->bbox.ymin > bbox0->bbox.ymax ||
            bbox1->bbox.ymax < bbox0->bbox.ymin)
        {
            // There is no overlap.
            return 0.0;
        }
        else
        {
            fp16 intersect_width  = std::min(bbox0->bbox.xmax, bbox1->bbox.xmax) -
                                   std::max(bbox0->bbox.xmin, bbox1->bbox.xmin);

            fp16 intersect_height = std::min(bbox0->bbox.ymax, bbox1->bbox.ymax) -
                                   std::max(bbox0->bbox.ymin, bbox1->bbox.ymin);

            if(!normalized)
            {
                intersect_width  += 1.0;
                intersect_height += 1.0;
            }

            if (intersect_width > 0 && intersect_height > 0)
            {
                fp16 intersect_size = intersect_width * intersect_height;
                fp16 bbox0_size = bbox0->size;
                fp16 bbox1_size = bbox1->size;
                return intersect_size / (bbox0_size + bbox1_size - intersect_size);
            }
            else
            {
                return 0.0;
            }
        }
    }

    inline void decode_bbox(
            const BBox *prior_bbox,
            const BBoxVariance *prior_variance,
            SizedBBox *local_pred,
            const s32 code_type,
            const bool variance_encoded_in_target,
            const bool clip_bbox,
            const bool normalized)
    {
        // Local pred buffer is used for decode bboxe.
        SizedBBox *decode_bbox = local_pred;

        if (code_type == CORNER)
        {
            if (variance_encoded_in_target)
            {
                // variance is encoded in target, we simply need to add the offset
                // predictions.
                decode_bbox->bbox.xmin = (prior_bbox->xmin + local_pred->bbox.xmin);
                decode_bbox->bbox.ymin = (prior_bbox->ymin + local_pred->bbox.ymin);
                decode_bbox->bbox.xmax = (prior_bbox->xmax + local_pred->bbox.xmax);
                decode_bbox->bbox.ymax = (prior_bbox->ymax + local_pred->bbox.ymax);
            }
            else
            {
                // variance is encoded in bbox, we need to scale the offset
                // accordingly.
                decode_bbox->bbox.xmin = (prior_bbox->xmin +
                        prior_variance->v0 * local_pred->bbox.xmin);
                decode_bbox->bbox.ymin = (prior_bbox->ymin +
                        prior_variance->v1 * local_pred->bbox.ymin);
                decode_bbox->bbox.xmax = (prior_bbox->xmax +
                        prior_variance->v2 * local_pred->bbox.xmax);
                decode_bbox->bbox.ymax = (prior_bbox->ymax +
                        prior_variance->v3 * local_pred->bbox.ymax);
            }
        }
        else if (code_type == CENTER_SIZE)
        {
            fp16 prior_width = prior_bbox->xmax - prior_bbox->xmin;
            fp16 prior_height = prior_bbox->ymax - prior_bbox->ymin;
            fp16 prior_center_x = (prior_bbox->xmin + prior_bbox->xmax) * 0.5;
            fp16 prior_center_y = (prior_bbox->ymin + prior_bbox->ymax) * 0.5;

            fp16 decode_bbox_center_x, decode_bbox_center_y;
            fp16 decode_bbox_width, decode_bbox_height;
            if (variance_encoded_in_target)
            {
                // variance is encoded in target, we simply need to retore the offset
                // predictions.
                decode_bbox_center_x = local_pred->bbox.xmin * prior_width +
                    prior_center_x;
                decode_bbox_center_y = local_pred->bbox.ymin * prior_height +
                    prior_center_y;
                decode_bbox_width = exp(local_pred->bbox.xmax) * prior_width;
                decode_bbox_height = exp(local_pred->bbox.ymax) * prior_height;
            }
            else
            {
                // variance is encoded in bbox,
                // we need to scale the offset accordingly.
                decode_bbox_center_x = prior_variance->v0 *
                    local_pred->bbox.xmin * prior_width + prior_center_x;
                decode_bbox_center_y = prior_variance->v1 *
                    local_pred->bbox.ymin * prior_height + prior_center_y;
                decode_bbox_width = exp(prior_variance->v2 *
                        local_pred->bbox.xmax) * prior_width;
                decode_bbox_height = exp(prior_variance->v3 *
                        local_pred->bbox.ymax) * prior_height;
            }

            decode_bbox->bbox.xmin = (decode_bbox_center_x -
                    decode_bbox_width  * 0.5);
            decode_bbox->bbox.ymin = (decode_bbox_center_y -
                    decode_bbox_height * 0.5);
            decode_bbox->bbox.xmax = (decode_bbox_center_x +
                    decode_bbox_width  * 0.5);
            decode_bbox->bbox.ymax = (decode_bbox_center_y +
                    decode_bbox_height * 0.5);

        }
        else if (code_type == CORNER_SIZE)
        {
            fp16 prior_width = prior_bbox->xmax - prior_bbox->xmin;
            fp16 prior_height = prior_bbox->ymax - prior_bbox->ymin;
            if (variance_encoded_in_target)
            {
                // variance is encoded in target, we simply need to add the offset
                // predictions.
                decode_bbox->bbox.xmin = (prior_bbox->xmin +
                        local_pred->bbox.xmin * prior_width);
                decode_bbox->bbox.ymin = (prior_bbox->ymin +
                        local_pred->bbox.ymin * prior_height);
                decode_bbox->bbox.xmax = (prior_bbox->xmax +
                        local_pred->bbox.xmax * prior_width);
                decode_bbox->bbox.ymax = (prior_bbox->ymax +
                        local_pred->bbox.ymax * prior_height);
            }
            else
            {
                // variance is encoded in bbox,
                // we need to scale the offset accordingly.
                decode_bbox->bbox.xmin = (prior_bbox->xmin +
                        prior_variance->v0 * local_pred->bbox.xmin * prior_width);
                decode_bbox->bbox.ymin = (prior_bbox->ymin +
                        prior_variance->v1 * local_pred->bbox.ymin * prior_height);
                decode_bbox->bbox.xmax = (prior_bbox->xmax +
                        prior_variance->v2 * local_pred->bbox.xmax * prior_width);
                decode_bbox->bbox.ymax = (prior_bbox->ymax +
                        prior_variance->v3 * local_pred->bbox.ymax * prior_height);
            }
        }

        if (clip_bbox)
        {
            // TODO: Decide if BBox clipping is necessary since is hard coded in
            // the reference code.
            // ClipBBox(*decode_bbox, decode_bbox);
        }

        decode_bbox->size = 0;

        if (decode_bbox->bbox.xmax < decode_bbox->bbox.xmin ||
                decode_bbox->bbox.ymax < decode_bbox->bbox.ymin)
        {
            // If bbox is invalid (e.g. xmax < xmin or ymax < ymin), return 0.
            decode_bbox->size = 0;
        }
        else
        {
            fp16 width  = decode_bbox->bbox.xmax - decode_bbox->bbox.xmin;
            fp16 height = decode_bbox->bbox.ymax - decode_bbox->bbox.ymin;
            if (normalized)
            {
                decode_bbox->size = width * height;
            }
            else
            {
                // If bbox is not within range [0, 1].
                decode_bbox->size = (width + 1) * (height + 1);
            }
        }
    }
}

void det_out_decode_bboxes(t_DetectionOutputParam *params)
{
    using namespace mv::tensor;
    dma::Config dmaConfig = { 1, params->dmaLinkAgent };
    dma::User dmaUser(dmaConfig);

    bool share_location = params->layer_parameters.share_location;
    s32 num_priors      = params->num_priors;
    s32 num_classes     = params->layer_parameters.num_classes;
    s32 num_loc_classes = share_location ? 1 : num_classes;
    s32 num_shaves      = params->num_shaves;
    s32 shave_num       = params->shave_num;

    s32 num_loc_classes_per_shave;
    s32 num_loc_classes_remainder;
    s32 num_bboxes_per_shave;
    s32 num_bboxes_remainder;
    s32 first_class;
    s32 last_class;
    s32 first_bbox;

    if(!share_location)
    {
        // If the classes don't share the bbox location split over the classes.
        // And proccess all the priors boxes(num_priors) for the classes
        // alloted the each shave.
        num_loc_classes_per_shave = num_loc_classes / num_shaves;
        num_loc_classes_remainder = num_loc_classes % num_shaves;
        num_bboxes_per_shave      = num_priors;
        num_bboxes_remainder      = 0;
        first_class               = num_loc_classes_per_shave * shave_num;
        first_bbox                = 0;
    }
    else
    {
        // If the classes share the bbox location split over the bboxes.
        num_loc_classes_per_shave = 1;
        num_loc_classes_remainder = 0;
        num_bboxes_per_shave      = num_priors / num_shaves;
        num_bboxes_remainder      = num_priors % num_shaves;
        first_class               = 0;
        first_bbox                = num_bboxes_per_shave * shave_num;
    }

    s32 num_classes_to_proccess = num_loc_classes_per_shave;
    if(shave_num < num_loc_classes_remainder)
    {
        ++num_classes_to_proccess;
        first_class += shave_num;
    }
    else
    {
        first_class += num_loc_classes_remainder;
    }

    s32 num_bboxes_to_proccess = num_bboxes_per_shave;
    if(shave_num < num_bboxes_remainder)
    {
        ++num_bboxes_to_proccess;
        first_bbox += shave_num;
    }
    else
    {
        first_bbox += num_bboxes_remainder;
    }

    last_class = first_class + num_classes_to_proccess;

    BBox *ddr_loc_preds = (BBox *)params->mbox_loc_data;
    ddr_loc_preds += first_class;
    ddr_loc_preds += first_bbox;

    // The prior box values is in channel minor. i.e. data is ordered:
    // xmin_0 v0_0 ymin_0 v1_0 xmax_0 v2_0 ymax_0 v3_0
    // xmin_1 v0_1 ymin_1 v1_1 xmax_1 v2_1 ymax_1 v3_1 etc.
    fp16 *ddr_prior_boxes = (fp16 *)params->mbox_priorbox_data + (first_bbox * 8);
    fp16 *ddr_prior_var   = ddr_prior_boxes + 1;

    SizedBBox *ddr_decoded_bboxes = params->all_decoded_bboxes_temp;
    ddr_decoded_bboxes += (first_class * num_priors);
    ddr_decoded_bboxes += first_bbox;

    //--------------------Decode Bounding Boxes--------------------
    // Split available CMX in 3 buffers:
    // Buffer1 - (Normalized)PriorBoxes
    // Buffer2 - Location predictions => Decoded(Normalized)BBoxes
    // Buffer3 - Prior variances

    s32 prediction_size = sizeof(SizedBBox) + sizeof(BBox) +
            (params->layer_parameters.variance_encoded_in_target ?
             sizeof(BBoxVariance) : 0);

    s32 no_cmx_preds = CMX_SIZE / prediction_size;
    SizedBBox *cmx_loc_preds = (SizedBBox *)params->cmxslice;
    BBox *cmx_prior_boxes = (BBox *)(cmx_loc_preds + no_cmx_preds);
    BBoxVariance *cmx_prior_var = (BBoxVariance *)(cmx_prior_boxes + no_cmx_preds);

    s32 num_boxes_transfers    = num_bboxes_to_proccess / no_cmx_preds;
    s32 num_boxes_per_transfer = no_cmx_preds;
    s32 num_boxes_remainder    = num_bboxes_to_proccess % no_cmx_preds;
    if(num_boxes_remainder != 0)
    {
        ++num_boxes_transfers;
    }

    s32 last_transfer_num_boxes = num_boxes_remainder;

    const s32 code_type = params->layer_parameters.code_type;
    const s32 background_label = params->layer_parameters.background_label;
    const bool variance_encoded_in_target = params->layer_parameters.variance_encoded_in_target;
    const bool normalized = true;
    const bool clip_bbox  = false;

    for(s32 box_transfer_i = 0; box_transfer_i < num_boxes_transfers; ++box_transfer_i)
    {
        s32 current_transfer_num_boxes = box_transfer_i < (num_boxes_transfers - 1) ?
                num_boxes_per_transfer : last_transfer_num_boxes;

        // Transfer boxes priors. These are the same for all classes and one
        // for each location.
        dma::Task(dmaUser).start(
                (u8 *)(ddr_prior_boxes + num_boxes_per_transfer * box_transfer_i * 8),
                (u8 *)(cmx_prior_boxes),
                current_transfer_num_boxes * sizeof(BBox),
                sizeof(fp16),
                current_transfer_num_boxes * sizeof(BBox),
                sizeof(fp16) * 2,
                current_transfer_num_boxes * sizeof(BBox));


        // Transfer boxes variances.These are the same for all classes and one
        // for each location.
        dma::Task(dmaUser).start(
                (u8 *)(ddr_prior_var + num_boxes_per_transfer * box_transfer_i * 8),
                (u8 *)(cmx_prior_var),
                current_transfer_num_boxes * sizeof(BBoxVariance),
                sizeof(fp16),
                current_transfer_num_boxes * sizeof(BBoxVariance),
                sizeof(fp16) * 2,
                current_transfer_num_boxes * sizeof(BBoxVariance));

        // If shared location than first_class = last_class = 0
        for(s32 class_i = first_class; class_i < last_class; ++class_i)
        {
            if(class_i == background_label && !share_location)
            {
                // Skip background class;
                continue;
            }

            s32 loc_transfer_src_stride = num_loc_classes * sizeof(BBox);
            s32 loc_transfer_dst_stride = sizeof(SizedBBox);
            s32 loc_transfer_src_width  = sizeof(BBox);
            s32 loc_transfer_dst_width  = sizeof(BBox);
            s32 loc_pred_transfer_size  = current_transfer_num_boxes * sizeof(BBox);

            // Transfer location prediction for current class_i.
            dma::Task(dmaUser).start(
                    (u8 *)(ddr_loc_preds +
                        num_boxes_per_transfer * box_transfer_i * num_loc_classes +
                        class_i),
                    (u8 *)(cmx_loc_preds),
                    loc_pred_transfer_size,
                    loc_transfer_src_width,
                    loc_transfer_dst_width,
                    loc_transfer_src_stride,
                    loc_transfer_dst_stride);

            for(s32 box_i = 0; box_i < current_transfer_num_boxes; ++box_i)
            {
                decode_bbox(&cmx_prior_boxes[box_i],
                        &cmx_prior_var[box_i],
                        &cmx_loc_preds[box_i],
                        code_type,
                        variance_encoded_in_target,
                        clip_bbox,
                        normalized);
            }

            // Transfer decoded bboxes.
            // Local preds are decoded in place into bboxes.
            SizedBBox *cmx_decoded_boxes = cmx_loc_preds;
            dma::Task(dmaUser).start(
                    (u8 *)(cmx_decoded_boxes),
                    (u8 *)(ddr_decoded_bboxes +
                        num_boxes_per_transfer * box_transfer_i +
                        class_i * num_priors),
                    current_transfer_num_boxes * sizeof(SizedBBox));
        }
    }
}

void det_out_apply_nms(t_DetectionOutputParam *params)
{
    using namespace mv::tensor;
    using namespace std;
    dma::Config dmaConfig = { 1, params->dmaLinkAgent };
    dma::User dmaUser(dmaConfig);

    t_DetOutputLayerParams det_out_params = params->layer_parameters;

    s32 num_classes = det_out_params.num_classes;
    s32 num_priors  = params->num_priors;
    s32 num_shaves  = params->num_shaves;
    s32 shave_num   = params->shave_num;

    s32 num_classes_per_shave = num_classes / num_shaves;
    s32 num_classes_remainder = num_classes % num_shaves;
    s32 first_class           = num_classes_per_shave * shave_num;

    s32 num_classes_to_proccess = num_classes_per_shave;
    if(shave_num < num_classes_remainder)
    {
        ++num_classes_to_proccess;
        first_class += shave_num;
    }
    else
    {
        first_class += num_classes_remainder;
    }

    s32 last_class = first_class + num_classes_to_proccess;

    //--------------------------------------
    // Sort confidences in decreasing order per class.
    s32 confidence_bpp = sizeof(Confidence);

    // Split available CMX in 2 buffers:
    // Buffer1 - N raw confidences;
    // Buffer2 - N ConfIndex pairs with confidence > confidence_threshold.
    s32 conf_conf_index_size = sizeof(Confidence) + sizeof(ConfIndex);
    s32 cmx_cci_size = CMX_SIZE / conf_conf_index_size;

    // Input confidence data. 1 conf value per prior_box per class.
    Confidence *ddr_conf_data = ((Confidence *)params->mbox_conf_data);
    // Temporary cmx buffers.
    Confidence *cmx_conf_data = (Confidence *)params->cmxslice;
    ConfIndex  *cmx_conf_idx_data = (ConfIndex *)(cmx_conf_data + cmx_cci_size);
    // Temporaray ddr buffer for sorted and thresholded confidences_index pairs / class.
    ConfIndex  *ddr_conf_idx_data = params->all_indices_temp;

    s32 no_conf_transfers = num_priors / cmx_cci_size;
    s32 no_conf_per_transfer = cmx_cci_size;
    s32 last_transfer_no_conf = num_priors % cmx_cci_size;
    if(last_transfer_no_conf)
    {
        ++no_conf_transfers;
    }

    // Fill the temporary Confidence index pairs.
    // Temporary ddr buffer for the number of detections per class after
    // thresholding and after non maxima suppression.
    // (i.e. bbox with confidence > confidence_therehold).
    s32 *nms_detections = params->num_detections_temp;

    for(s32 class_i = first_class; class_i < last_class; ++class_i)
    {
        if(class_i == det_out_params.background_label)
        {
            // Skip background class.
            continue;
        }

        s32 class_i_kept = 0;
        for(s32 conf_transfer_i = 0; conf_transfer_i < no_conf_transfers; ++conf_transfer_i)
        {
            s32 current_transfer_no_conf = conf_transfer_i < (no_conf_transfers - 1) ?
                    no_conf_per_transfer : last_transfer_no_conf;
            s32 transfer_size       = current_transfer_no_conf * confidence_bpp;
            s32 transfer_src_width  = confidence_bpp;
            s32 transfer_src_stride = num_classes * confidence_bpp;
            u32 src_offset = (no_conf_per_transfer * conf_transfer_i * num_classes) + class_i;

            // Transfer confidences for class_i
            dma::Task(dmaUser).start(
                    (u8 *)(ddr_conf_data + src_offset),
                    (u8 *)(cmx_conf_data),
                    transfer_size,
                    transfer_src_width,
                    transfer_size,
                    transfer_src_stride,
                    transfer_size);

            // Keep only "bboxes" with confidence over the threshold.
            s32 current_kept = 0;
            for(s32 conf_i = 0; conf_i < current_transfer_no_conf; ++conf_i)
            {

                if(cmx_conf_data[conf_i] > det_out_params.confidence_threshold)
                {
                    cmx_conf_idx_data[current_kept].conf  = cmx_conf_data[conf_i];
                    cmx_conf_idx_data[current_kept].index = conf_i +
                        no_conf_per_transfer * conf_transfer_i;

                    ++current_kept;
                }
            }

            if(current_kept > 0)
            {
                if(no_conf_transfers == 1)
                {
                    // Sort the ConfIndex pairs in cmx before transferring them
                    // to temporary storage.
                    // If we can't store all confs in cmx it is better
                    // to sort them in ddr directly.

                    sort(cmx_conf_idx_data,
                            cmx_conf_idx_data + current_kept,
                            sort_conf_index);
                }

                // Transfer the ConfIndex pairs to temporary storage.
                dma::Task(dmaUser).start(
                        (u8 *)(cmx_conf_idx_data),
                        (u8 *)(ddr_conf_idx_data + class_i * num_priors +
                            class_i_kept),
                        current_kept * sizeof(ConfIndex));

                class_i_kept += current_kept;
            }
        }

        if(no_conf_transfers > 1)
        {
            // Sort the confidences in DDR since there were multiple
            // transfers required.
            // This is not usually the case.

            sort(ddr_conf_idx_data + class_i * num_priors,
                    ddr_conf_idx_data + class_i * num_priors + class_i_kept,
                    sort_conf_index);
        }

        // Only the first nms_top_k bboxes with confidence > confidence_threshold
        // are used in the non maxima suppression.
        nms_detections[class_i] = class_i_kept <= det_out_params.nms_top_k ?
                class_i_kept : det_out_params.nms_top_k;
    }

    // Do Non maxima suppression
    fp16 adaptive_threshold = params->layer_parameters.nms_threshold;
    fp16 eta = params->layer_parameters.eta;
    bool share_location = params->layer_parameters.share_location;

    for(s32 class_i = first_class; class_i < last_class; ++class_i)
    {
        s32 class_i_num_detections = nms_detections[class_i];

        if(class_i == params->layer_parameters.background_label
                || class_i_num_detections <= 0)
        {
            // Ignore background label class and classes that have 0 detections
            // left after thresholding.
            continue;
        }

        // Transfer the ConfIndex pairs.
        dma::Task(dmaUser).start(
                (u8 *)(ddr_conf_idx_data + class_i * num_priors),
                (u8 *)(cmx_conf_idx_data),
                class_i_num_detections * sizeof(ConfIndex));

        // The bbox with highest score is always kept.
        s32 no_processed_bboxes = 1;
        s32 nms_kept = 1;

        SizedBBox *decoded_bboxes = share_location ? params->all_decoded_bboxes_temp :
               params->all_decoded_bboxes_temp + (num_priors * class_i);

        while(no_processed_bboxes < class_i_num_detections)
        {
            s32 idx = cmx_conf_idx_data[no_processed_bboxes].index;
            bool keep = true;
            for(s32 bbox_i = 0; bbox_i < nms_kept; ++bbox_i)
            {
                const s32 kept_idx = cmx_conf_idx_data[bbox_i].index;

                fp16 overlap = jaccard_overlap(&decoded_bboxes[idx],
                        &decoded_bboxes[kept_idx]);

                keep = overlap <= adaptive_threshold;

                if(!keep)
                {
                    break;
                }

            }

            if(keep)
            {
                cmx_conf_idx_data[nms_kept++] = cmx_conf_idx_data[no_processed_bboxes];

                if(eta < 1 && adaptive_threshold > 0.5)
                {
                    adaptive_threshold *= eta;
                }
            }

            ++no_processed_bboxes;
        }

        nms_detections[class_i] = nms_kept;

        dma::Task(dmaUser).start(
                (u8 *)(cmx_conf_idx_data),
                (u8 *)(ddr_conf_idx_data + class_i * num_priors),
                nms_kept * sizeof(ConfIndex));
    }
}

void detection_output_core(t_DetectionOutputParam *params)
{
    using namespace mv::tensor;
    using namespace std;

    dma::Config dmaConfig = { 1, params->dmaLinkAgent };
    dma::User dmaUser(dmaConfig);

    //-------------------Write the output----------
    t_DetOutputLayerParams layer_params = params->layer_parameters;

    s32 num_classes = layer_params.num_classes;
    s32 num_priors  = params->num_priors;

    // Temporary ddr buffer for the number of detections per class.
    // (i.e. bbox with confidence > confidence_therehold).
    s32 *num_nms_detections = params->num_detections_temp;
    const bool share_location = params->layer_parameters.share_location;

    ConfIndex *ddr_conf_idx_data = params->all_indices_temp;

    s32 keep_top_k = layer_params.keep_top_k;

    s32 num_detections_total = 0;
    for (s32 class_i = 0; class_i < num_classes; ++class_i)
        num_detections_total += class_i == layer_params.background_label ? 0 :
            num_nms_detections[class_i];

    bool top_k_in_cmx = CMX_SIZE > num_detections_total * sizeof(ConfIndexLabel);

    ConfIndexLabel *top_k_elimination = top_k_in_cmx ?
                (ConfIndexLabel *)params->cmxslice :
                (ConfIndexLabel *)params->top_k_elimination_temp;
    ConfIndexLabel *top_k_iterator = top_k_elimination;

    for (s32 class_i = 0; class_i < num_classes; ++class_i)
    {
        s32 current_num_det = num_nms_detections[class_i];
        if(class_i == layer_params.background_label || current_num_det <= 0)
        {
            // Ignore background class.
            // Ignore classes that have no detections left after NMS.
            continue;
        }

        dma::Task(dmaUser).start(
                (u8 *)(ddr_conf_idx_data + class_i * num_priors),
                (u8 *)(top_k_iterator),
                current_num_det * sizeof(ConfIndex),
                current_num_det * sizeof(ConfIndex),
                sizeof(ConfIndex),
                current_num_det * sizeof(ConfIndex),
                sizeof(ConfIndexLabel));

        // Write the class label.
        u16 label = (u16)class_i;
        dma::Task(dmaUser).start(
                (u8 *)(&label),
                (u8 *)(&top_k_iterator[0].label),
                current_num_det * sizeof(top_k_iterator[0].label),
                sizeof(top_k_iterator[0].label),
                sizeof(top_k_iterator[0].label),
                0,
                sizeof(ConfIndexLabel));

        top_k_iterator += current_num_det;
    }

    if(num_detections_total > keep_top_k)
    {
        sort(top_k_elimination, top_k_elimination + num_detections_total,
                sort_conf_index_label);
        num_detections_total = keep_top_k;
    }

    // Output the data.
    fp16 *output = params->output;
    // Write the number of detections. If there are 0 detections we will
    // generate one fake detection.
    output[0] = num_detections_total > 0 ? num_detections_total : 1;
    output = output + 7;

    if(num_detections_total > 0)
    {
        s32 num_outputed = 0;

        // Write the output in order of the class.
        for(s32 class_i = 0; class_i < num_classes; ++class_i)
        {
            if(class_i == layer_params.background_label)
            {
                // Ignore background label class.
                continue;
            }

            for(s32 det_i = 0; det_i < num_detections_total; ++det_i)
            {
                if(top_k_elimination[det_i].label == class_i)
                {
                    // Output detection.
                    s32 idx = top_k_elimination[det_i].conf_index.index;
                    // We support only one image and we set it's id to 0.
                    output[num_outputed * 7 + 0] = 0;
                    output[num_outputed * 7 + 1] = class_i;
                    output[num_outputed * 7 + 2] =
                        top_k_elimination[det_i].conf_index.conf;

                    SizedBBox *decoded_bboxes = share_location ?
                        params->all_decoded_bboxes_temp :
                        params->all_decoded_bboxes_temp + (num_priors * class_i);

                    output[num_outputed * 7 + 3] = decoded_bboxes[idx].bbox.xmin;
                    output[num_outputed * 7 + 4] = decoded_bboxes[idx].bbox.ymin;
                    output[num_outputed * 7 + 5] = decoded_bboxes[idx].bbox.xmax;
                    output[num_outputed * 7 + 6] = decoded_bboxes[idx].bbox.ymax;

                    ++num_outputed;
                }
            }
        }
    }
    else
    {
        // Generate fake detection.
        output[0] =  0;
        output[1] = -1;
        output[2] = -1;
        output[3] = -1;
        output[4] = -1;
        output[5] = -1;
        output[6] = -1;
    }
}

