///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Functions declarations for input re-layout.
///

#ifndef _MV_PERMUTE_CORE_H_
#define _MV_PERMUTE_CORE_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include "../priv/mvPermuteParam.h"

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------

// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif

void mvPermute(t_MvPermuteParams *params);

#ifdef __cplusplus
}
#endif

#endif /* _MV_PERMUTE_CORE_H_ */
