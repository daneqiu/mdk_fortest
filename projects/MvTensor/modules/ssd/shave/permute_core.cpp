/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief    Transforms an input volume V in channel minor to V' such that:
///           a kxl filter operation applied on V can be performed through
///           a 1x1 filter operation on V'.
///

#include <mv_types.h>
#include <moviVectorTypes.h>
#include "permute_core.h"
#include <mvTensorDma.h>
#include <mvTensorConfig.h>

#define INPUT_BPP sizeof(half)

void mvPermute(t_MvPermuteParams *params)
{
    using namespace mv::tensor;
    dma::Config dmaConfig = { 1, params->dmaLinkAgent };
    dma::User dmaUser(dmaConfig);

    fp16                 *p_input, *p_output;
    s32                  in_height, in_width, in_channels;
    s32                  out_height, out_width, out_channels;
    u32                  *permute_order;
    s32                  i, j, elems, in_dim[3], out_steps[3];

    /* Input dimensions */
    in_height   = params->in_height;
    in_width    = params->in_width;
    in_channels = params->in_channels;
    in_dim[0]   = in_height;
    in_dim[1]   = in_width;
    in_dim[2]   = in_channels;

    /* Output dimensions */
    permute_order = params->permute_order;
    out_height    = in_dim[permute_order[0]];
    out_width     = in_dim[permute_order[1]];
    out_channels  = in_dim[permute_order[2]];

    out_steps[permute_order[0]] = out_width * out_channels;
    out_steps[permute_order[1]] = out_channels;
    out_steps[permute_order[2]] = 1;
    elems = params->start_height * in_width * in_channels;

    for(i = params->start_height; i < params->end_height; i++)
    {
        for(j = 0; j < in_width; j++)
        {
            p_input  = params->input  + elems;
            p_output = params->output + i*out_steps[0] + j*out_steps[1];
            elems    = elems + in_channels;

            dma::Task(dmaUser).start(
                (u8*)p_input,              // src
                (u8*)p_output,             // dst
                in_channels * INPUT_BPP,   // byte length
                in_channels * INPUT_BPP,   // src width
                INPUT_BPP,                 // dst width
                in_channels * INPUT_BPP,   // src stride
                out_steps[2] * INPUT_BPP); // dst stride
        }
    }
}
