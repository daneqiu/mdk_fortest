///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef _MVNORMALIZE_CORE_H_
#define _MVNORMALIZE_CORE_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include "../priv/mvNormalizeParam.h"

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------

// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif

void mvNormalize(t_MvNormalizeParam *p);

#ifdef __cplusplus
}
#endif

#endif//__MVNORMALIZE_CORE_H__
