///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Functions declarations detection output.
///

#ifndef _MV_DETECTIONOUTPUT_CORE_H_
#define _MV_DETECTIONOUTPUT_CORE_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include "../priv/mvDetectionOutputParam.h"

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------

// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif

void detection_output_core(t_DetectionOutputParam *params);
void det_out_apply_nms(t_DetectionOutputParam *params);
void det_out_decode_bboxes(t_DetectionOutputParam *params);

#ifdef __cplusplus
}
#endif

#endif /* _MV_DETECTIONOUTPUT_CORE_H_ */
