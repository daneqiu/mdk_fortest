BASE_PATH := $(MVTENSOR_MODULES_PATH)/ssd

# Dependencies

MVMODULE_NEED_MATMUL := 0

# Flags

# Headers

# do not add $(BASE_PATH)/priv here
# it should be used only by internal sources, like this
# include "../priv/header_name.h"

# do not add $(BASE_PATH)/leon here
# it should be used only by sources in the same folder, like this
# include "header_name.h"

# do not add $(BASE_PATH)/shave here
# it should be used only by sources in the same folder, like this
# include "header_name.h"

# Sources

MVMODULE_LEON_COMPONENT_PATHS := $(BASE_PATH)/leon
MVMODULE_SHAVE_COMPONENT_PATHS := $(BASE_PATH)/shave

# Linker options

# Shave entry points

MVMODULE_ENTRY_POINTS := \
	-u detection_output_core \
	-u det_out_apply_nms \
	-u det_out_decode_bboxes \
	-u mvNormalize \
	-u mvPermute \
	-u mvPriorBox

# Enable the module

include $(MVTENSOR_MODULES_PATH)/enable_module.mk
