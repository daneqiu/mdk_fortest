///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
///
/// \details
///

#ifndef _MV_DETECTION_OUTPUT_PARAM_H_
#define _MV_DETECTION_OUTPUT_PARAM_H_

#include <mv_types.h>
#include "mvTensor.h"

#ifdef __MOVICOMPILE__
#include <moviVectorTypes.h>
#else
typedef fp16 half;
#endif

enum PriorBox_CodeType {CORNER = 1, CENTER_SIZE, CORNER_SIZE};

typedef fp16 Confidence;

typedef struct
{
    Confidence conf;
    u16  index;
} ConfIndex;

typedef struct
{
    ConfIndex conf_index;
    u16       label;
} ConfIndexLabel;

typedef struct
{
    fp16 xmin;
    fp16 ymin;
    fp16 xmax;
    fp16 ymax;
} BBox;

typedef struct
{
    BBox bbox;
    fp16 size;
    // TODO: Difficult not used in this implementation.
    // Decide if it is required in the future.
    // fp16 difficult;
} SizedBBox;

typedef struct
{
    fp16 v0;
    fp16 v1;
    fp16 v2;
    fp16 v3;
} BBoxVariance;

/// Detection output layer parameters.
typedef struct
{
    s32  num_classes;
    s32  share_location;
    s32  background_label;
    fp32 nms_threshold;
    s32  nms_top_k;
    s32  code_type;
    s32  keep_top_k;
    fp32 confidence_threshold;
    s32  variance_encoded_in_target;
    fp32 eta;
} t_DetOutputLayerParams;

/// Detection output parameters.
typedef struct
{
    half *mbox_loc_data;
    half *mbox_conf_data;
    half *mbox_priorbox_data;
    half *output;
    u8   *cmxslice;
    //s32  first_class;
    //s32  last_class;
    //s32  no_bboxes_to_decode;
    //s32  first_bbox_index;

    s32 num_shaves;
    s32 shave_num;

    // Temporary buffers.
    ConfIndex *all_indices_temp;
    s32 *num_detections_temp;
    SizedBBox *all_decoded_bboxes_temp;
    ConfIndexLabel *top_k_elimination_temp;

    s32 num_priors;
    s32 num_predictions;
    u32 dmaLinkAgent;

    t_DetOutputLayerParams layer_parameters;

} t_DetectionOutputParam;
#endif /* _MV_DETECTION_OUTPUT_PARAM_H_ */
