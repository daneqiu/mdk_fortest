///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
///
/// \details
///

#ifndef _MV_PERMUTEPARAM_H_
#define _MV_PERMUTEPARAM_H_

#include <mv_types.h>

#ifdef __MOVICOMPILE__
    #include <moviVectorTypes.h>
#else
    typedef fp16 half;
#endif

/// Permute parameters
typedef struct
{
  fp16 *input;
  fp16 *output;
  s32  in_width;
  s32  in_height;
  s32  in_channels;
  s32  start_height;
  s32  end_height;
  u32  *permute_order;
  u32  dmaLinkAgent;
} t_MvPermuteParams;

#endif /* _MV_PERMUTEPARAM_H_ */
