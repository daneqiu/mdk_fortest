///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
/// 
/// \details
///


#ifndef _MV_PRIORBOXPARAM_H
#define _MV_PRIORBOXPARAM_H

#include <mv_types.h>

#ifdef __MOVICOMPILE__
    #include <moviVectorTypes.h>
#else
    typedef fp16 half;
#endif

/// MvPriorBox global parameters structure
typedef struct
{
    half* output;
    fp32* params;
    u32 width;
    u32 height;
    u32 imgW;
    u32 imgH;
    half *cmxslice;
    u32 dmaLinkAgent;
} t_MvPriorBoxParam;

#endif // _MV_PRIORBOXPARAM_H
