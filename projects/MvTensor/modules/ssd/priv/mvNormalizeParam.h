///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
/// 
/// \details
///


#ifndef _MV_NORMALIZEPARAM_H
#define _MV_NORMALIZEPARAM_H

#include <mv_types.h>

#ifdef __MOVICOMPILE__
    #include <moviVectorTypes.h>
#else
    typedef fp16 half;
#endif

/// MvNormalize global parameters structure
typedef struct
{
    half* input;
    half* output;
    half* scale;
    u32 channels;
    u32 numlines;
    u32 across_spatial;
    u32 channel_shared;
    half *cmxslice;
    u32 dmaLinkAgent;
} t_MvNormalizeParam;

#endif // _MV_NORMALIZEPARAM_H
