# Dependencies

ifneq ($(MVMODULE_NEED_MATMUL),)
MVTENSOR_NEED_MATMUL := 1
endif

# Flags

# Headers

MVTENSOR_LEON_HEADER_PATHS := $(MVTENSOR_LEON_HEADER_PATHS) $(MVMODULE_LEON_HEADER_PATHS)
MVTENSOR_SHAVE_HEADER_PATHS := $(MVTENSOR_SHAVE_HEADER_PATHS) $(MVMODULE_SHAVE_HEADER_PATHS)

# do not add $(BASE_PATH)/priv here
# it should be used only by internal sources, like this
# include "../priv/header_name.h"

# do not add $(BASE_PATH)/leon here
# it should be used only by sources in the same folder, like this
# include "header_name.h"

# do not add $(BASE_PATH)/shave here
# it should be used only by sources in the same folder, like this
# include "header_name.h"

# Sources

MVTENSOR_LEON_COMPONENT_PATHS := $(MVTENSOR_LEON_COMPONENT_PATHS) $(MVMODULE_LEON_COMPONENT_PATHS)
MVTENSOR_SHAVE_COMPONENT_PATHS := $(MVTENSOR_SHAVE_COMPONENT_PATHS) $(MVMODULE_SHAVE_COMPONENT_PATHS)

# Linker options

# Shave entry points

MVTENSOR_ENTRY_POINTS := $(MVTENSOR_ENTRY_POINTS) $(MVMODULE_ENTRY_POINTS)
