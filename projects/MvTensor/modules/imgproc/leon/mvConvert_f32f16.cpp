///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief Leon wrappers for CNN post operations.
///

// 1: Includes
// ----------------------------------------------------------------------------
#include "mvConvert_f32f16.h"

#include <mvTensorInternal.h>
#include "../priv/mvConvert_f32f16Param.h"

/* TODO
 * Add support to run in multiple shaves
 */

#define MAX_SHAVES_TO_USE 4

void convert_f32f16(fp32               *input,
                    fp16               *output,
                    s32                numpix,
                    float              *convert_params,
                    t_MvTensorMyriadResources   *myriadResources,
                    const mv::tensor::Resources &res)
{
    u32 dmaLinkAgent;
    s32 first_shave, last_shave;
    s32 shave_idx;

    first_shave = myriadResources->firstShave;
    last_shave = myriadResources->lastShave;
    int num_shaves = ((last_shave + 1 - first_shave) > MAX_SHAVES_TO_USE) ? MAX_SHAVES_TO_USE : (last_shave + 1 - first_shave);
    last_shave = first_shave + num_shaves - 1;

    s32 step_size = numpix / (last_shave + 1 - first_shave);
    step_size = ((step_size * (last_shave + 1 - first_shave)) != numpix) ? step_size + 1 : step_size;

    dmaLinkAgent = myriadResources->dmaLinkAgent;

    int s = 0;
    for(shave_idx = first_shave; shave_idx < last_shave + 1; shave_idx++, s += step_size)
    {
        t_MvConvert_f32f16Params *convert_f32f16Param = useShaveParam<t_MvConvert_f32f16Params>(shave_idx);
        convert_f32f16Param->input         = input + s;
        convert_f32f16Param->output        = output + s;

        convert_f32f16Param->numpix        = (step_size > numpix - s) ? numpix - s : step_size;

        convert_f32f16Param->convert_params = convert_params;
        convert_f32f16Param->dmaLinkAgent  = dmaLinkAgent;

        convert_f32f16Param->cmxslice = getCMXSliceDataSection(shave_idx);

        MODULE_ENTRY_DECL(mvConvert_f32f16);
        startShave(shave_idx, ((u32)&MODULE_ENTRY(mvConvert_f32f16)), (u32)convert_f32f16Param);
    }
    waitShaves(first_shave, last_shave);

    res.shaveCache.writeback();
}
