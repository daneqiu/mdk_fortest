#include <mvModuleHandle.h>
#include "mvConvert_u8f16.h"
#include "mvConvert_f32f16.h"

namespace
{
    using namespace mv::tensor;

    void imgproc_entry_point(const t_MvTensorParam *mvTensorParam, const t_MvTensorOp &op, const mv::tensor::Optimization &, const mv::tensor::Resources &res)
    {
        switch (op.type)
        {
            case kConvert_u8f16:
            {
                convert_u8f16((u8*)mvTensorParam->input->data,
                        (fp16*)mvTensorParam->output->data,
                         mvTensorParam->input->dimX * mvTensorParam->input->dimY*mvTensorParam->input->dimZ,
                         (float*)op.params,
                         mvTensorParam->myriadResources,
                         res);

                break;
            }
            case kConvert_f32f16:
            {
                convert_f32f16((fp32*)mvTensorParam->input->data,
                        (fp16*)mvTensorParam->output->data,
                         mvTensorParam->input->dimX * mvTensorParam->input->dimY*mvTensorParam->input->dimZ,
                         (float*)op.params,
                         mvTensorParam->myriadResources,
                         res);

                break;
            }

            default: break;
        }
    }

    const mv::tensor::ModuleHandle handles[] =
    {
        mv::tensor::ModuleHandle(kConvert_u8f16, &imgproc_entry_point),
        mv::tensor::ModuleHandle(kConvert_f32f16, &imgproc_entry_point),
    };
}
