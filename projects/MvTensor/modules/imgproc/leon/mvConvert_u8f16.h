///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef CONVERT_U8F16_LEON_H_
#define CONVERT_U8F16_LEON_H_

#include <mv_types.h>
#include <mvTensor.h>
#include <mvTensorResources.h>

void convert_u8f16( u8                 *input,
                    fp16               *output,
                    s32                numpix,
                    float              *convert_params,
                    t_MvTensorMyriadResources   *myriadResources,
                    const mv::tensor::Resources &res);

#endif // CONVERT_U8F16_LEON_H_
