///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
///
/// \details
///

#ifndef _MV_CONVERTF32F16PARAM_H_
#define _MV_CONVERTF32F16PARAM_H_

#include <mv_types.h>

#ifdef __MOVICOMPILE__
    #include <moviVectorTypes.h>
#else
    typedef fp16 half;
#endif

#define INPUT_BPP  (sizeof(fp32))
#define OUTPUT_BPP (sizeof(fp16))

typedef struct
{
    fp32 *  input;
    fp16 *  output;
    s32     numpix;
    float*  convert_params;
    u32     dmaLinkAgent;
    u8   *  cmxslice;
} t_MvConvert_f32f16Params;

#endif /* _MV_CONVERTF32F16PARAM_H_ */
