///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef CONVERT_U8F16_SHAVE_H_
#define CONVERT_U8F16_SHAVE_H_

#include "mvTensorConfig.h"
#include "../priv/mvConvert_u8f16Param.h"

#ifdef __cplusplus
extern "C"
{
#endif

void mvConvert_u8f16(t_MvConvert_u8f16Params *params);

#ifdef __cplusplus
}
#endif

#endif // CONVERT_U8F16_SHAVE_H_
