/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///

// 1: Includes
// ----------------------------------------------------------------------------
#include "convert_u8f16_core.h"

#include <mvTensorDma.h>
#include <moviVectorConvert.h>

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------

#define WORK_BUFFER_SIZE (MVTENSOR_HEAP_DATA_SIZE/2)

__attribute__ ((noinline)) static void calculate(u8* p_input, fp16* p_output, int numpix, float scale, float bias)
{
    /*
     *  Right shifter int to float conversion *
        typedef union
        {
            short i;
            fp16 f;
        } fi16;

        fi16 sn;
        sn.f = (fp16)(1<<10);
        int i = 200;
        {
            fi16 n;
            n.i = (short)i;
            n.i = n.i + sn.i;
            n.f = n.f - sn.f;
            printf("%d %f\n", i, (double)n.f);
        }
        *
        */

    // constants for right shifter
    half8 vfs = (fp16)(1<<10);
    short8 vns = (short8)vfs;

    uchar8* p_in  = (uchar8*)p_input;
    half8* p_out = (half8*)p_output;
    half8 vscale = scale;
    half8 vbias  = bias;

    ushort8 unr0 = mvuConvert_ushort8(p_in[0]);
    ushort8 unr1 = mvuConvert_ushort8(p_in[1]);
    ushort8 unr2 = mvuConvert_ushort8(p_in[2]);
    short8 nr0 = __builtin_shave_vau_add_i16_rr((short8)unr0, vns);
    short8 nr1 = __builtin_shave_vau_add_i16_rr((short8)unr1, vns);
    short8 nr2 = __builtin_shave_vau_add_i16_rr((short8)unr2, vns);

    half8 rr0 = __builtin_shave_vau_mul_f16_rr (__builtin_shave_vau_sub_f16_rr((half8)nr0, vfs), vscale);
    half8 rr1 = __builtin_shave_vau_mul_f16_rr (__builtin_shave_vau_sub_f16_rr((half8)nr1, vfs), vscale);
    half8 rr2 = __builtin_shave_vau_mul_f16_rr (__builtin_shave_vau_sub_f16_rr((half8)nr2, vfs), vscale);
    ushort8 rr_0 = mvuConvert_ushort8(p_in[0 + 3]);
    ushort8 rr_1 = mvuConvert_ushort8(p_in[0 + 4]);
    ushort8 rr_2 = mvuConvert_ushort8(p_in[0 + 5]);
    for(int i = 0; i <= numpix / 8; i += 3)
    {
        uchar8 rr__0 = p_in[i + 6];
        uchar8 rr__1 = p_in[i + 7];
        uchar8 rr__2 = p_in[i + 8];
        p_out[i + 0] = __builtin_shave_vau_add_f16_rr (rr0, vbias);
        p_out[i + 1] = __builtin_shave_vau_add_f16_rr (rr1, vbias);
        p_out[i + 2] = __builtin_shave_vau_add_f16_rr (rr2, vbias);

        short8 nr0 = __builtin_shave_vau_add_i16_rr((short8)rr_0, vns);
        short8 nr1 = __builtin_shave_vau_add_i16_rr((short8)rr_1, vns);
        short8 nr2 = __builtin_shave_vau_add_i16_rr((short8)rr_2, vns);
        rr0 = __builtin_shave_vau_mul_f16_rr (__builtin_shave_vau_sub_f16_rr((half8)nr0, vfs), vscale);
        rr1 = __builtin_shave_vau_mul_f16_rr (__builtin_shave_vau_sub_f16_rr((half8)nr1, vfs), vscale);
        rr2 = __builtin_shave_vau_mul_f16_rr (__builtin_shave_vau_sub_f16_rr((half8)nr2, vfs), vscale);

        rr_0 = mvuConvert_ushort8(rr__0);
        rr_1 = mvuConvert_ushort8(rr__1);
        rr_2 = mvuConvert_ushort8(rr__2);
    }
}

void mvConvert_u8f16(t_MvConvert_u8f16Params *params)
{
    using namespace mv::tensor;
    dma::Config dmaConfig = { 1, params->dmaLinkAgent };
    dma::User dmaUser(dmaConfig);
    dma::Task dmaTask(dmaUser);

    u8                   *p_input0,  *p_input1, *p_input2;
    fp16                 *p_output0, *p_output1, *p_output2;
    float                *convert_params;

    /* Input dimensions */
    if(params->numpix > 0)
    {
        int numpix = params->numpix;
        convert_params = params->convert_params;
        float scale = convert_params[0];
        float bias = convert_params[1];

        s32 step_size = (WORK_BUFFER_SIZE - 8 * (sizeof(u8) + sizeof(fp16)) - 9 * 8 * sizeof(u8) - 6 * 8 * sizeof(fp16)) / (sizeof(u8) + sizeof(fp16));

        if(numpix / step_size > 2)
        {
            int cnumpix0, cnumpix1, cnumpix2, s;

            p_output0 = (fp16*)(params->cmxslice + 0 * WORK_BUFFER_SIZE);
            p_input0 = (u8*)(p_output0 + step_size + 24);
            p_output1 = (fp16*)(params->cmxslice + 1 * WORK_BUFFER_SIZE);
            p_input1 = (u8*)(p_output1 + step_size + 24);

            {
                cnumpix0 = (step_size > numpix - 0) ? numpix - 0 : step_size;
                dmaTask.start((u8*)(params->input + 0), p_input0, cnumpix0 * INPUT_BPP);

                p_output2 = p_output0;
                p_input2 = p_input0;
                p_output0 = p_output1;
                p_input0 = p_input1;
                p_output1 = p_output2;
                p_input1 = p_input2;
            }

            {
                cnumpix1 = (step_size > numpix - cnumpix0) ? numpix - cnumpix0 : step_size;
                int nbytes = __builtin_shave_cmu_max_i32_rr_int (cnumpix1, 1);
                int shift  = (cnumpix1 < 1) ? 1 : 0;
                dmaTask.start((u8*)(params->input + cnumpix0 - shift), p_input0, nbytes * INPUT_BPP);

                calculate(p_input1, p_output1, cnumpix0, scale, bias);

                p_output2 = p_output0;
                p_input2 = p_input0;
                p_output0 = p_output1;
                p_input0 = p_input1;
                p_output1 = p_output2;
                p_input1 = p_input2;
            }

            for(s = cnumpix1 + cnumpix0; s < numpix; )
            {
                cnumpix2 = __builtin_shave_cmu_min_i32_rr_int (numpix - s, step_size);
                int nbytes = __builtin_shave_cmu_max_i32_rr_int (cnumpix2, 1);
                int shift  = (cnumpix2 < 1) ? 1 : 0;
                dmaTask.start((u8*)(p_output0), (u8*)(params->output + s - cnumpix0 - cnumpix1), cnumpix0 * OUTPUT_BPP);

                calculate(p_input1, p_output1, cnumpix0, scale, bias);

                dmaTask.start((u8*)(params->input + s - shift), p_input0, nbytes * INPUT_BPP);

                p_output2 = p_output0;
                p_input2 = p_input0;
                p_output0 = p_output1;
                p_input0 = p_input1;
                p_output1 = p_output2;
                p_input1 = p_input2;

                cnumpix0 = cnumpix1;
                cnumpix1 = cnumpix2;
                s += cnumpix2;
            }
            {
                dmaTask.start((u8*)(p_output0), (u8*)(params->output + s - cnumpix0 - cnumpix1), cnumpix0 * OUTPUT_BPP);

                calculate(p_input1, p_output1, cnumpix1, scale, bias);

                p_output2 = p_output0;
                p_input2 = p_input0;
                p_output0 = p_output1;
                p_input0 = p_input1;
                p_output1 = p_output2;
                p_input1 = p_input2;
                cnumpix0 = cnumpix1;
            }
            {
                dmaTask.start((u8*)(p_output0), (u8*)(params->output + s - cnumpix0), cnumpix0 * OUTPUT_BPP);
            }
        }
        else
        {
            int s;
            step_size = (2 * WORK_BUFFER_SIZE - 8 * (sizeof(u8) + sizeof(fp16)) - 9 * 8 * sizeof(u8) - 6 * 8 * sizeof(fp16)) / (sizeof(u8) + sizeof(fp16));
            p_output0 = (fp16*)(params->cmxslice + 0 * WORK_BUFFER_SIZE);
            p_input0 = (u8*)(p_output0 + step_size + 24);

            {
                dmaTask.start((u8*)(params->input), p_input0, numpix * INPUT_BPP);
                dmaTask.wait();

                calculate(p_input0, p_output0, numpix, scale, bias);
                dmaTask.start((u8*)(p_output0), (u8*)(params->output), numpix * OUTPUT_BPP);
            }
        }
    }
}
