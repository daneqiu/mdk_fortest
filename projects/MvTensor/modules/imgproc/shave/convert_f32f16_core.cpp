/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///


// 1: Includes
// ----------------------------------------------------------------------------
#include "convert_f32f16_core.h"

#include <mvTensorDma.h>
#include <moviVectorConvert.h>

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
#define WORK_BUFFER_SIZE (MVTENSOR_HEAP_DATA_SIZE/2)

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------

__attribute__ ((noinline)) static void calculate(fp32* p_input, fp16* p_output, int numpix, float scale, float bias)
{
    float4 * p_in  = (float4*)p_input;
    half4  * p_out = (half4*) p_output;
    float4 vscale = scale;
    float4 vbias = bias;
    float4 rr0 = p_in[0 + 0];
    float4 rr1 = p_in[0 + 1];
    float4 rr2 = p_in[0 + 2];
    for(int i = 0; i <= numpix / 4; i += 6)
    {
        float4 rr3 = p_in[i + 3];
        float4 rr4 = p_in[i + 4];
        float4 rr5 = p_in[i + 5];
        float4 rr6 = p_in[i + 6];
        float4 rr7 = p_in[i + 7];
        float4 rr8 = p_in[i + 8];

        p_out[i + 0] = mvuConvert_half4(__builtin_shave_vau_add_f32_rr (__builtin_shave_vau_mul_f32_rr (rr0, vscale), vbias));
        p_out[i + 1] = mvuConvert_half4(__builtin_shave_vau_add_f32_rr (__builtin_shave_vau_mul_f32_rr (rr1, vscale), vbias));
        p_out[i + 2] = mvuConvert_half4(__builtin_shave_vau_add_f32_rr (__builtin_shave_vau_mul_f32_rr (rr2, vscale), vbias));
        p_out[i + 3] = mvuConvert_half4(__builtin_shave_vau_add_f32_rr (__builtin_shave_vau_mul_f32_rr (rr3, vscale), vbias));
        p_out[i + 4] = mvuConvert_half4(__builtin_shave_vau_add_f32_rr (__builtin_shave_vau_mul_f32_rr (rr4, vscale), vbias));
        p_out[i + 5] = mvuConvert_half4(__builtin_shave_vau_add_f32_rr (__builtin_shave_vau_mul_f32_rr (rr5, vscale), vbias));

        rr0 = rr6;
        rr1 = rr7;
        rr2 = rr8;
    }
}

void mvConvert_f32f16(t_MvConvert_f32f16Params *params)
{
    using namespace mv::tensor;
    dma::Config dmaConfig = { 1, params->dmaLinkAgent };
    dma::User dmaUser(dmaConfig);
    dma::Task dmaTask(dmaUser);

    fp32                 *p_input0,  *p_input1,  *p_input2;
    fp16                 *p_output0, *p_output1, *p_output2;
    float                *convert_params;

    /* Input dimensions */
    if(params->numpix > 0)
    {
        int numpix = params->numpix;
        convert_params = params->convert_params;
        float scale = convert_params[0];
        float bias  = convert_params[1];

        s32 step_size = (WORK_BUFFER_SIZE - 4 * (sizeof(fp32) + sizeof(fp16)) - 9 * 4 * sizeof(fp32) - 6 * 4 * sizeof(fp16)) / (sizeof(fp32) + sizeof(fp16));

        p_output0 = (fp16*)(params->cmxslice + 0 * WORK_BUFFER_SIZE);
        p_output1 = (fp16*)(params->cmxslice + 1 * WORK_BUFFER_SIZE);

        // buffers are allocated with lags between them to not thinking about cycle tails after unrolling and sw pipelining
        p_input0  = (fp32*)(p_output0 + step_size + 4 + 6 * 4);
        p_input1  = (fp32*)(p_output1 + step_size + 4 + 6 * 4);

        if((numpix / step_size) >= 2)
        {
            int cnumpix0, cnumpix1, cnumpix2, s;
            {
                cnumpix0 = (step_size > numpix - 0) ? numpix - 0 : step_size;
                dmaTask.start((u8*)(params->input + 0), (u8*)p_input0, cnumpix0 * INPUT_BPP);

                p_output2 = p_output0;
                p_input2 = p_input0;
                p_output0 = p_output1;
                p_input0 = p_input1;
                p_output1 = p_output2;
                p_input1 = p_input2;
            }

            {
                cnumpix1 = (step_size > numpix - cnumpix0) ? numpix - cnumpix0 : step_size;
                int npix = __builtin_shave_cmu_max_i32_rr_int (cnumpix1, 1);
                int shift  = (cnumpix1 < 1) ? 1 : 0;
                dmaTask.start((u8*)(params->input + cnumpix0 - shift), (u8*)p_input0, npix * INPUT_BPP);

                calculate(p_input1, p_output1, cnumpix1, scale, bias);

                p_output2 = p_output0;
                p_input2 = p_input0;
                p_output0 = p_output1;
                p_input0 = p_input1;
                p_output1 = p_output2;
                p_input1 = p_input2;
            }

            for(s = cnumpix1 + cnumpix0; s < numpix; )
            {
                cnumpix2 = __builtin_shave_cmu_min_i32_rr_int (numpix - s, step_size);
                int npix = __builtin_shave_cmu_max_i32_rr_int (cnumpix2, 1);
                int shift  = (cnumpix2 < 1) ? 1 : 0;
                dmaTask.start((u8*)(params->input + s - shift), (u8*)p_input0, npix * INPUT_BPP);

                calculate(p_input1, p_output1, cnumpix1, scale, bias);

                dmaTask.start((u8*)(p_output0), (u8*)(params->output + s - cnumpix0 - cnumpix1), cnumpix0 * OUTPUT_BPP);
                p_output2 = p_output0;
                p_input2 = p_input0;
                p_output0 = p_output1;
                p_input0 = p_input1;
                p_output1 = p_output2;
                p_input1 = p_input2;
                cnumpix0 = cnumpix1;
                cnumpix1 = cnumpix2;
                s += cnumpix2;
            }
            {
                dmaTask.start((u8*)(p_output0), (u8*)(params->output + s - cnumpix0 - cnumpix1), cnumpix0 * OUTPUT_BPP);

                calculate(p_input1, p_output1, cnumpix1, scale, bias);

                p_output2 = p_output0;
                p_input2 = p_input0;
                p_output0 = p_output1;
                p_input0 = p_input1;
                p_output1 = p_output2;
                p_input1 = p_input2;
                cnumpix0 = cnumpix1;
            }
            {
                dmaTask.start((u8*)(p_output0), (u8*)(params->output + s - cnumpix0), cnumpix0 * OUTPUT_BPP);
            }
        }
        else
        {
            step_size = (2 * WORK_BUFFER_SIZE - 4 * (sizeof(fp32) + sizeof(fp16)) - 9 * 4 * sizeof(fp32) - 6 * 4 * sizeof(fp16)) / (sizeof(fp32) + sizeof(fp16));
            p_output0 = (fp16*)(params->cmxslice);
            p_input0  = (fp32*)(p_output0 + step_size + 4 + 6 * 4);
            for(int s = 0; s < numpix; s += step_size)
            {
                int cnumpix1 = (step_size > numpix - s) ? numpix - s : step_size;
                dmaTask.start((u8*)(params->input + s), (u8*)p_input0, cnumpix1 * INPUT_BPP);
                dmaTask.wait();

                calculate(p_input0, p_output0, cnumpix1, scale, bias);
                dmaTask.start((u8*)(p_output0), (u8*)(params->output + s), cnumpix1 * OUTPUT_BPP);
            }
        }
    }
}

