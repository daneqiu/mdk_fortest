///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef CONVERT_F32F16_SHAVE_H_
#define CONVERT_F32F16_SHAVE_H_

#include "mvTensorConfig.h"
#include "../priv/mvConvert_f32f16Param.h"

#ifdef __cplusplus
extern "C"
{
#endif

void mvConvert_f32f16(t_MvConvert_f32f16Params *params);

#ifdef __cplusplus
}
#endif

#endif // CONVERT_F32F16_SHAVE_H_
