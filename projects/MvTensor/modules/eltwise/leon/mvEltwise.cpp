///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

#include "../priv/mvEltwiseParam.h"
#include <mvTensorInternal.h>
#include <mvModuleHandle.h>

MODULE_ENTRY_DECL(mvEltwise);

namespace
{
    using namespace mv::tensor;

    class Eltwise : public Layer
    {
    private:
        static void eltwise(u32 op, fp16* input, u32 channels, u32 height, u32 width,
              fp16* input2, u32 channels2, fp16* output,
              u32 istrideX, u32 i2strideX, u32 ostrideX,
              u32 firstShave, u32 lastShave, u32 dmaLinkAgent)
        {
            u32 nShaves = lastShave - firstShave + 1;
            // Divide between shaves
            u32 nelem = (width * height + nShaves - 1) / nShaves;
            for(u32 i = 0; i < nShaves; i++)
            {
                // Square input and multiply by band matrix
                t_MvEltwiseParam *param = useShaveParam<t_MvEltwiseParam>(i + firstShave);
                param->op = op == kProd ? Eltwise_prod : op == kMax ? Eltwise_max : Eltwise_sum;
                param->input = (half *)((u8 *)input + i * nelem * istrideX);
                param->channels = channels;
                param->channels2 = channels2;
                param->input2 = (half *)((u8 *)input2 + i * nelem * i2strideX);
                param->output = (half *)((u8 *)output + i * nelem * ostrideX);
                param->nelements = (i+1) * nelem > width * height ? width * height - i*nelem : nelem;
                param->istrideX = istrideX;
                param->i2strideX = i2strideX;
                param->ostrideX = ostrideX;
                param->dmaLinkAgent = dmaLinkAgent;
                param->cmxslice = (half *)getCMXSliceDataSection(firstShave + i);

                u32 readBackAddr = (u32)(&(param->cmxslice));
                GET_REG_WORD_VAL(readBackAddr);

                startShave(firstShave + i, (u32)&MODULE_ENTRY(mvEltwise), (u32)param);

                if(param->nelements + i*nelem == width*height)
                {
                    lastShave = firstShave + i;
                    break;
                }
            }

            waitShaves(firstShave, lastShave);
        }

        virtual void run_(const t_MvTensorParam *mvTensorParam, const t_MvTensorOp &op, const Optimization &, const Resources &)
        {
            eltwise(op.type, (half *) mvTensorParam->input->data, mvTensorParam->input->dimZ, mvTensorParam->input->dimY,
                    mvTensorParam->input->dimX, (half *) mvTensorParam->weights->data, mvTensorParam->weights->dimZ, (half *) mvTensorParam->output->data,
                    mvTensorParam->input->dimXStride, mvTensorParam->weights->dimXStride, mvTensorParam->output->dimXStride,
                    mvTensorParam->myriadResources->firstShave, mvTensorParam->myriadResources->lastShave, mvTensorParam->myriadResources->dmaLinkAgent);
        }
    };

    const ModuleHandle handles[] =
    {
        ModuleHandle().add<Eltwise>(kMax),
        ModuleHandle().add<Eltwise>(kProd),
        ModuleHandle().add<Eltwise>(kSum),
    };
}
