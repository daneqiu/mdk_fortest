///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect code
///

#include <mvTensorDma.h>
#include "../priv/mvEltwiseParam.h"

#define INPUT_BPP 2

extern "C"
void mvEltwise(t_MvEltwiseParam *p)
{
    using namespace mv::tensor;
    dma::Config dmaConfig = { 1, p->dmaLinkAgent };
    dma::User dmaUser(dmaConfig);

    u32 max_slice = 41976 / (p->channels + p->channels2);   // Number of "pixels" to process at each stage
    half *cmxslice2 = p->cmxslice + (max_slice * p->channels + 7) / 8 * 8;
    for(u32 i = 0; i < p->nelements; i += max_slice)
    {
        u32 nelem = p->nelements - i;
        if(nelem > max_slice)
            nelem = max_slice;

        dma::Task(dmaUser).start((u8 *)p->input + i * p->istrideX, // src
                                 (u8 *)p->cmxslice,                // dst
                                 nelem * p->channels * INPUT_BPP,  // byte length
                                 p->channels * INPUT_BPP,          // src width
                                 p->channels * INPUT_BPP,          // dst width
                                 p->istrideX,                      // src stride
                                 p->channels * INPUT_BPP);         // dst stride


        dma::Task(dmaUser).start((u8 *)p->input2 + i * p->i2strideX, // src
                                 (u8 *)cmxslice2,                    // dst
                                 nelem * p->channels2 * INPUT_BPP,   // byte length
                                 p->channels2 * INPUT_BPP,           // src width
                                 p->channels2 * INPUT_BPP,           // dst width
                                 p->i2strideX,                       // src stride
                                 p->channels2 * INPUT_BPP);          // dst stride
        switch(p->op)
        {
        case Eltwise_sum:
            for(u32 j = 0; j < nelem; j++)
            {
                u32 k;
                for(k = 0; k+7 < p->channels2; k += 8)
                    *(half8 *)&p->cmxslice[j * p->channels + k] += *(half8 *)&cmxslice2[j * p->channels2 + k];
                for(; k < p->channels2; k++)
                    p->cmxslice[j * p->channels + k] += cmxslice2[j * p->channels2 + k];
            }
            break;
        case Eltwise_prod:
            for(u32 j = 0; j < nelem; j++)
            {
                u32 k;
                for(k = 0; k+7 < p->channels2; k += 8)
                    *(half8 *)&p->cmxslice[j * p->channels + k] *= *(half8 *)&cmxslice2[j * p->channels2 + k];
                for(; k < p->channels2; k++)
                    p->cmxslice[j * p->channels + k] *= cmxslice2[j * p->channels2 + k];
            }
            break;
        case Eltwise_max:
            for(u32 j = 0; j < nelem; j++)
            {
                u32 k;
                for(k = 0; k+7 < p->channels2; k += 8)
                    *(half8 *)&p->cmxslice[j * p->channels + k] =
                        __builtin_shave_cmu_max_f16_rr_half8 (*(half8 *)&p->cmxslice[j * p->channels + k], *(half8 *)&cmxslice2[j * p->channels2 + k]);
                for(; k < p->channels2; k++)
                    p->cmxslice[j * p->channels + k] =
                        __builtin_shave_cmu_max_f16_rr_half(p->cmxslice[j * p->channels + k], cmxslice2[j * p->channels2 + k]);
            }
            break;
        }

        dma::Task(dmaUser).start((u8 *)p->cmxslice,                 // src
                                 (u8 *)p->output + i * p->ostrideX, // dst
                                 nelem * p->channels * INPUT_BPP,   // byte length
                                 p->channels * INPUT_BPP,           // src width
                                 p->channels * INPUT_BPP,           // dst width
                                 p->channels * INPUT_BPP,           // src stride
                                 p->ostrideX);                      // dst stride
    }
}
