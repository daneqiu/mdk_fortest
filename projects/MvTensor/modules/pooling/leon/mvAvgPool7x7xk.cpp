///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

#include "mvAvgPool7x7xk.h"
#include "../priv/mvAvgPool7x7xkParam.h"
#include <mvTensorInternal.h>

void avgPool7x7xk(fp16* input, u32 channels, fp16* output,
             u32 dmaLinkAgent,
             u32 firstShave, u32 lastShave)
{
    u32 i;
    u32 sliceC; // channels to process per SHAVE
    u32 remainingC;
    u32 offsetIO = 0;
    u32 channelsMaxToProcess;
    u32 max_sliceC = 209;
    u32 shaves_no =(lastShave - firstShave + 1);

    channelsMaxToProcess = channels / 4;

    do{
        sliceC = channelsMaxToProcess / shaves_no;
        if(sliceC > max_sliceC)
        {
            sliceC = max_sliceC;
            remainingC = 0;
            channelsMaxToProcess -= sliceC * shaves_no;
        }
        else
        {
            remainingC = channelsMaxToProcess % shaves_no;
            channelsMaxToProcess = 0;
        }
        u32 at_leastC = sliceC;
        for (i = firstShave; i <= lastShave; i++)
        {
            sliceC = at_leastC;
            if(remainingC)
            {
                sliceC++;
                remainingC--;
            }

            //initialize AvgPool7x7xk SHAVE param
            t_MvAvgPool7x7xkParam *avgPool7x7xkParam = useShaveParam<t_MvAvgPool7x7xkParam>(i);
            avgPool7x7xkParam->input = input + offsetIO;
            avgPool7x7xkParam->channels = channels;
            avgPool7x7xkParam->sliceC = sliceC * 4;
            avgPool7x7xkParam->cmxslice = getCMXSliceDataSection(i);
            avgPool7x7xkParam->dmaLinkAgent = dmaLinkAgent;
            avgPool7x7xkParam->output = output + offsetIO;

            u32 readBackAddr = (u32)(&(avgPool7x7xkParam->output));
            GET_REG_WORD_VAL(readBackAddr);

            MODULE_ENTRY_DECL(mvAvgPool7x7xk);
            startShave(i, (u32)&MODULE_ENTRY(mvAvgPool7x7xk), (u32)avgPool7x7xkParam);

            offsetIO += sliceC * 4;
        }
        waitShaves(firstShave, lastShave);

    }while(channelsMaxToProcess > 0);
}
