///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

#include "mvMaxPool2x2.h"
#include "../priv/mvMaxPool2x2Param.h"
#include <mvTensorInternal.h>

void maxPool2x2(fp16* input, u32 channels, u32 height,
        u32 width, u32 paddStyle, u32 stride, u32 pad, u32 ostrideX, fp16* output,
        u32 dmaLinkAgent,
        u32 firstShave, u32 lastShave)
{
    u32 i;
    u32 sliceC; // channels to process per SHAVE
    u32 remainingC;
    u32 offsetIO = 0;
    u32 channelsMaxToProcess;
    // If the channels number is a multiple of 8, group channels in groups of 8
    u32 group = channels % 8 == 0 ? 8 : 1;
    // allocate 7 words extra, in order to allow the Shave function to enlarge the
    // input/output buffers to the next multiple of 8
    u32 max_sliceC = u32((CMX_DATA_SIZE/INPUT_BPP/2 - 7)/(3*(width + 2*pad + 2))) / group;
    u32 shaves_no =(lastShave - firstShave + 1);

    channelsMaxToProcess = channels / group;

    do{
        sliceC = channelsMaxToProcess / shaves_no;
        if(sliceC > max_sliceC)
        {
            sliceC = max_sliceC;
            remainingC = 0;
            channelsMaxToProcess -= sliceC * shaves_no;
        }
        else
        {
            remainingC = channelsMaxToProcess % shaves_no;
            channelsMaxToProcess = 0;
        }
        u32 at_leastC = sliceC;
        for (i = firstShave; i <= lastShave; i++)
        {
            sliceC = at_leastC;
            if(remainingC)
            {
                sliceC++;
                remainingC--;
            }

            if (sliceC == 0)
                break;

            //initialize MaxPool2x2 SHAVE param
            t_MvMaxPool2x2Param *maxPool2x2Param = useShaveParam<t_MvMaxPool2x2Param>(i);
            maxPool2x2Param->input    = input + offsetIO;
            maxPool2x2Param->channels = channels;
            maxPool2x2Param->sliceC   = sliceC * group;
            maxPool2x2Param->ostrideX = ostrideX;
            maxPool2x2Param->height   = height;
            maxPool2x2Param->width    = width;
            maxPool2x2Param->stride   = stride;
            maxPool2x2Param->pad      = pad;
            maxPool2x2Param->cmxslice = getCMXSliceDataSection(i);
            maxPool2x2Param->dmaLinkAgent = dmaLinkAgent;
            maxPool2x2Param->paddStyle    = paddStyle;
            maxPool2x2Param->output       = output + offsetIO;

            u32 readBackAddr = (u32)(&(maxPool2x2Param->output));
            GET_REG_WORD_VAL(readBackAddr);

            MODULE_ENTRY_DECL(mvMaxPool2x2);
            startShave(i, (u32)&MODULE_ENTRY(mvMaxPool2x2), (u32)maxPool2x2Param);

            offsetIO += sliceC * group;
        }
    
        waitShaves(firstShave, lastShave);

    }while(channelsMaxToProcess > 0);
}
