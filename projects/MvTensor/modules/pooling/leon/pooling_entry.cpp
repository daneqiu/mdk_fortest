#include "mvAvgPool3x3.h"
#include "mvAvgPool7x7xk.h"
#include "mvAvgPoolMxN.h"
#include "mvMaxPool2x2.h"
#include "mvMaxPool3x3.h"
#include "mvMaxPoolMxN.h"

#include <mvModuleHandle.h>
#include <mvTensor.h>
#include <mvTensorUtil.h>

#include <algorithm>

#define DEBUG 0
#if DEBUG
#define MVT_DPRINTF(...) printf(__VA_ARGS__)
#else
#define MVT_DPRINTF(...)
#endif

namespace
{
    using namespace mv::tensor;

    class Pooling : public Layer
    {
    protected:
        static void fixOutputDim(const t_MvTensorParam *mvTensorParam, const t_MvTensorOp &op);
    };

    void Pooling::fixOutputDim(const t_MvTensorParam *mvTensorParam, const t_MvTensorOp &op)
    {
        int outDimX = mvTensorParam->output->dimX;
        int outDimY = mvTensorParam->output->dimY;
        int outDimZ = mvTensorParam->output->dimZ;

        switch(op.paddStyle)
        {
            case paddStyleTFSame:
                MVT_DPRINTF("Padding: TF-SAME\n");
                outDimX = (mvTensorParam->input->dimX + op.strideX - 1)/op.strideX;
                outDimY = (mvTensorParam->input->dimY + op.strideY - 1)/op.strideY;
                break;

            case paddStyleCaffe:
                MVT_DPRINTF("Padding: CAFFE\n");
                if(op.type == kAvgPool || op.type == kMaxPool)
                {
                    outDimX = (mvTensorParam->input->dimX + 2*op.padX - op.radixX + op.strideX - 1)/op.strideX + 1;
                    outDimY = (mvTensorParam->input->dimY + 2*op.padY - op.radixY + op.strideY - 1)/op.strideY + 1;
                    outDimX = std::min(outDimX, (int)((mvTensorParam->input->dimX + op.padX + op.strideX - 1)/ op.strideX));
                    outDimY = std::min(outDimY, (int)((mvTensorParam->input->dimY + op.padY + op.strideY - 1)/ op.strideY));
                }
                else
                {
                    outDimX = (mvTensorParam->input->dimX + 2*op.padX - op.radixX )/op.strideX + 1;
                    outDimY = (mvTensorParam->input->dimY + 2*op.padY - op.radixY )/op.strideY + 1;
                }
                break;

            default:
                MVT_DPRINTF("Padding: TF-VALID/NONE\n");
                outDimX = (mvTensorParam->input->dimX - op.radixX + op.strideX)/op.strideX;
                outDimY = (mvTensorParam->input->dimY - op.radixY + op.strideY)/op.strideY;
        }

        mvTensorParam->output->dimX = outDimX;
        mvTensorParam->output->dimY = outDimY;
        mvTensorParam->output->dimZ = outDimZ;
    }

    class AvgPool : public Pooling
    {
        virtual void run_(const t_MvTensorParam *mvTensorParam, const t_MvTensorOp &op, const Optimization &, const Resources &);
    };

    void AvgPool::run_(const t_MvTensorParam *mvTensorParam, const t_MvTensorOp &op, const Optimization &, const Resources &)
    {
        Pooling::fixOutputDim(mvTensorParam, op);
        const u32 outputStride = util::getMidStride(mvTensorParam->output);

        if(op.radixX == 3 && op.radixY == 3)
        {
            MVT_DPRINTF("AvgPool 3x3\n");

            avgPool3x3((fp16*)mvTensorParam->input->data,
                         mvTensorParam->input->dimZ,
                         mvTensorParam->input->dimY,
                         mvTensorParam->input->dimX,
                         op.paddStyle,
                         op.strideX,
                         op.padX,
                         outputStride,
                         (fp16*)mvTensorParam->output->data,
                         mvTensorParam->myriadResources->dmaLinkAgent,
                         mvTensorParam->myriadResources->firstShave,
                         mvTensorParam->myriadResources->lastShave);
        }
        else if(op.radixX == 7 && op.radixY == 7 &&
           op.strideX == 1 && op.strideY == 1 &&
           mvTensorParam->input->dimX == 7 && mvTensorParam->input->dimY == 7)
        // global pooling
        {
            MVT_DPRINTF("AvgPool 7x7 \n");

            avgPool7x7xk((fp16*)mvTensorParam->input->data,
                         mvTensorParam->input->dimZ,
                         (fp16*)mvTensorParam->output->data,
                         mvTensorParam->myriadResources->dmaLinkAgent,
                         mvTensorParam->myriadResources->firstShave,
                         mvTensorParam->myriadResources->lastShave);
        }
        else
        {
            MVT_DPRINTF("AvgPool MxN \n");

            avgPoolMxN((fp16*)mvTensorParam->input->data,
                    mvTensorParam->input->dimZ,
                    mvTensorParam->input->dimY,
                    mvTensorParam->input->dimX,
                    op.paddStyle,
                    op.radixX,
                    op.radixY,
                    op.strideX,
                    op.strideY,
                    op.padX,
                    op.padY,
                    outputStride,
                    (fp16*)mvTensorParam->output->data,
                    mvTensorParam->myriadResources->dmaLinkAgent,
                    mvTensorParam->myriadResources->firstShave,
                    mvTensorParam->myriadResources->lastShave);
        }
    }

    class MaxPool : public Pooling
    {
        virtual void run_(const t_MvTensorParam *mvTensorParam, const t_MvTensorOp &op, const Optimization &optimization, const Resources &);
    };

    void MaxPool::run_(const t_MvTensorParam *mvTensorParam, const t_MvTensorOp &op, const Optimization &optimization, const Resources &)
    {
        Pooling::fixOutputDim(mvTensorParam, op);
        const u32 outputStride = util::getMidStride(mvTensorParam->output);

        if(op.radixX == 2 && op.radixY == 2 &&
           optimization.isSet(Optimization::OPT_MAXPOOL2X2))
           // 2x2s2 SAME EVEN
        {
            MVT_DPRINTF("MaxPool 2x2\n");

            maxPool2x2((fp16*)mvTensorParam->input->data,
                         mvTensorParam->input->dimZ,
                         mvTensorParam->input->dimY,
                         mvTensorParam->input->dimX,
                         op.paddStyle,
                         op.strideX,
                         op.padX,
                         outputStride,
                         (fp16*)mvTensorParam->output->data,
                         mvTensorParam->myriadResources->dmaLinkAgent,
                         mvTensorParam->myriadResources->firstShave,
                         mvTensorParam->myriadResources->lastShave);
        }
        else if(op.radixX == 3 && op.radixY == 3
                 && optimization.isSet(Optimization::OPT_MAXPOOL3X3))
        {
            MVT_DPRINTF("MaxPool 3x3\n");

            maxPool3x3((fp16*)mvTensorParam->input->data,
                         mvTensorParam->input->dimZ,
                         mvTensorParam->input->dimY,
                         mvTensorParam->input->dimX,
                         op.paddStyle,
                         op.strideX,
                         op.padX,
                         outputStride,
                         (fp16*)mvTensorParam->output->data,
                         mvTensorParam->myriadResources->dmaLinkAgent,
                         mvTensorParam->myriadResources->firstShave,
                         mvTensorParam->myriadResources->lastShave);

            MVT_DPRINTF("out_H %d\n", (int)mvTensorParam->output->dimY);
            MVT_DPRINTF("out_W %d\n", (int)mvTensorParam->output->dimX);
        }
        else
        {
            MVT_DPRINTF("MaxPool MxN\n");
            MVT_DPRINTF("dimZ: %d\n",   (int)mvTensorParam->input->dimZ);
            MVT_DPRINTF("dimY: %d\n",   (int)mvTensorParam->input->dimY);
            MVT_DPRINTF("dimX: %d\n",   (int)mvTensorParam->input->dimX);
            MVT_DPRINTF("pad: %d\n",    (int)(op.paddStyle));
            MVT_DPRINTF("radixX: %d\n", (int)op.radixX);
            MVT_DPRINTF("radixY: %d\n", (int)op.radixY);
            MVT_DPRINTF("strideX: %d\n", (int)op.strideX);
            MVT_DPRINTF("strideY: %d\n", (int)op.strideY);
            MVT_DPRINTF("padX: %d\n",   (int)op.padX);
            MVT_DPRINTF("padY: %d\n",   (int)op.padY);

            maxPoolMxN((fp16*)mvTensorParam->input->data,
                    mvTensorParam->input->dimZ,
                    mvTensorParam->input->dimY,
                    mvTensorParam->input->dimX,
                    op.paddStyle,
                    op.radixX,
                    op.radixY,
                    op.strideX,
                    op.strideY,
                    op.padX,
                    op.padY,
                    outputStride,
                    (fp16*)mvTensorParam->output->data,
                    mvTensorParam->myriadResources->dmaLinkAgent,
                    mvTensorParam->myriadResources->firstShave,
                    mvTensorParam->myriadResources->lastShave);

            MVT_DPRINTF("out_H %d\n", (int)mvTensorParam->output->dimY);
            MVT_DPRINTF("out_W %d\n", (int)mvTensorParam->output->dimX);
        }
    }

    const ModuleHandle handles[] =
    {
        ModuleHandle().add<AvgPool>(kAvgPool),
        ModuleHandle().add<MaxPool>(kMaxPool),
    };
}
