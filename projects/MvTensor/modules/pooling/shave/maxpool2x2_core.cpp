///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect code
///

#include "maxpool2x2_core.h"
#include <maximumV2.h>
#include "mvTensor.h"
#include <mvTensorDma.h>

#define MIN(a, b)       ((a)<(b)?(a):(b))
#define MAX(a, b)       ((a)>(b)?(a):(b))
#define PAD_VALUE       ((half)-65504.0)

void mvMaxPool2x2(t_MvMaxPool2x2Param *p)
{
    u32 H = p->height, W = p->width, WP, HP, C = p->channels;
    u32 Hout=0, Wout=0;
    u32 sliceC = p->sliceC;
    u32 strideX = p->stride, strideY = p->stride;
    const u32 radix = 2;
    u32 ostrideX = p->ostrideX;
    u32 i, j;
    u8* inAddress = (u8*) ((u32) p->input);
    u8* linesBuffer;
    u8* outputBuffer;
    half* inLines[3];
    half* kernelInLines[2];
    half* kernelOutLines[1];
    u32 writtenElems = 0 , readElems = 0;
    u32 line = 0;
    // user specified padding (used only for CAFFE-style padding)
    u32 padUserX = p->pad, padUserY = p->pad;
    // convenience padding; used to simplify computations
    s32 padConvLeft, padConvRight, padConvTop, padConvBottom;
    s32 padLeft, padRight, padTop, padBottom, Hpad, Wpad;
    u32 numElem;

    // fix dmaLinkAgent
    using namespace mv::tensor;
    dma::Config dmaConfig = { 1, p->dmaLinkAgent };
    dma::User dmaUser(dmaConfig);
    dma::Task dmaTask(dmaUser);

    // set buffers to point to locations relative to cmxslice
    linesBuffer = p->cmxslice;
    outputBuffer = p->cmxslice + CMX_DATA_SIZE/2;
    kernelOutLines[0] = (half*)outputBuffer;

    if (p->paddStyle == paddStyleTFSame)
    {
        // rules for TensorFlow SAME padding
        Hout = (H + strideY - 1) / strideY;
        Wout = (W + strideX - 1) / strideX;
        Hpad = ((Hout - 1) * strideY + radix - H);
        Wpad = ((Wout - 1) * strideX + radix - W);

        padConvLeft   = Wpad/2;
        padConvRight  = Wpad - padConvLeft;
        padConvTop    = Hpad/2;
        padConvBottom = Hpad - padConvTop;
        padUserX = 0;
        padUserY = 0;
    }
    else if (p->paddStyle == paddStyleCaffe)
    {
        // rules for CAFFE padding
        Hout = ((H + 2*padUserY - radix + strideY - 1) / strideY) + 1;
        Wout = ((W + 2*padUserX - radix + strideX - 1) / strideX) + 1;
        Hout = MIN(Hout, (H + padUserY + strideY - 1) / strideY);
        Wout = MIN(Wout, (W + padUserX + strideX - 1) / strideX);

        Hpad = ((Hout - 1) * strideY + radix - H - 2*padUserY);
        Wpad = ((Wout - 1) * strideX + radix - W - 2*padUserX);
        Hpad = MAX(0, Hpad);
        Wpad = MAX(0, Wpad);

        padConvLeft   = 0;
        padConvRight  = Wpad - padConvLeft;
        padConvTop    = 0;
        padConvBottom = Hpad - padConvTop;
    }
    else
    {
        // rules for TensorFlow VALID padding
        // treat unknown padding schemes as TF-VALID
        Hout = (H - radix + 1 + strideY - 1) / strideY;
        Wout = (W - radix + 1 + strideX - 1) / strideX;
        Hpad = Wpad = 0;
        padConvTop  = padConvBottom = 0;
        padConvLeft = padConvRight  = 0;
        padUserX = 0;
        padUserY = 0;
    }

    padLeft   = padUserX + padConvLeft;
    padRight  = padUserX + padConvRight;
    padTop    = padUserY + padConvTop;
    padBottom = padUserY + padConvBottom;

    HP = H + padTop + padBottom;
    WP = W + padLeft + padRight;

    for (i = padTop; i < radix; i++)
    {
//      TODO : add dma ext ? or maybe not ... ?
        dma::Task(dmaUser).start(inAddress + readElems * C * INPUT_BPP,                 // src
                                 linesBuffer + (i*WP + padLeft) *  sliceC * INPUT_BPP,  // dst
                                 W * sliceC * INPUT_BPP,                                // byte length
                                 sliceC * INPUT_BPP,                                    // src width
                                 sliceC * INPUT_BPP,                                    // dst width
                                 C * INPUT_BPP,                                         // src stride
                                 sliceC * INPUT_BPP);                                   // dst stride
        readElems += W;
    }

    inLines[0] = (half*) linesBuffer + (       padLeft) * sliceC;
    inLines[1] = (half*) linesBuffer + (1*WP + padLeft) * sliceC;

    // Padding: padding one row in column-major is equivalent with padding one column in row-major
    //          top row -> leftmost column, bottom row -> rightmost column

    // left padding;
    for (i = 0; i < sliceC*padLeft; i++)
    {
        *(inLines[0] - padLeft * sliceC + i) = PAD_VALUE;
        *(inLines[1] - padLeft * sliceC + i) = PAD_VALUE;
    }

    // right padding
    for (i = 0; i < sliceC*padRight; i++)
    {
        *(inLines[0] + W * sliceC + i) = PAD_VALUE;
        *(inLines[1] + W * sliceC + i) = PAD_VALUE;
    }

    // top padding; user pad is zero for any padding scheme
    for (j = 0; j < (u32)padTop; j++)
    {
        for(i = 0; i < sliceC*WP; i++)
            *(inLines[j] + -padLeft*sliceC + i) = PAD_VALUE;
    }


    kernelInLines[0] = (half*)linesBuffer;
    kernelInLines[1] = (half*)linesBuffer + 1 * sliceC;

    // align numElem to the next multiple of 8, as this is a constraint of the
    // asm implementation of mvcvMaximumV2_asm
    // this works because sliceC was chosen such that there are at least
    // 7*INPUT_BPP bytes at the end of the input/output buffers
    numElem = (sliceC * radix * WP + 7) & ~0x7;
    mvcvMaximumV2_asm((half**)kernelInLines, (half**)kernelOutLines, numElem);

    kernelInLines[0] = (half*)outputBuffer;
    kernelInLines[1] = (half*)outputBuffer + WP*sliceC;
    // again, align numElem to the next multiple of 8
    numElem = (sliceC * WP + 7) & ~0x7;
    mvcvMaximumV2_asm((half**)kernelInLines, (half**)kernelOutLines, numElem);

//  TODO : add dma ext ? or maybe not ... ?
    dmaTask.start(outputBuffer,                   // src
                  (u8*)p->output,                 // dst
                  Wout * sliceC * INPUT_BPP,      // byte length
                  sliceC * INPUT_BPP,             // src width
                  sliceC * INPUT_BPP,             // dst width
                  strideX * sliceC * INPUT_BPP,   // src stride
                  ostrideX);                      // dst stride
    dmaTask.wait();

    writtenElems += Wout;

    line = 1;
    do
    {
        u32 bufferIdx = (line - 1);
        if(readElems < W*H)
        {
//          TODO : add dma ext ? or maybe not ... ?
            dmaTask.start(inAddress + readElems * C * INPUT_BPP,                                  // src
                          linesBuffer + ((bufferIdx % radix) * WP + padLeft)* sliceC * INPUT_BPP, // dst
                          W * sliceC * INPUT_BPP,                                                 // byte length
                          sliceC * INPUT_BPP,                                                     // src width
                          sliceC * INPUT_BPP,                                                     // dst width
                          C * INPUT_BPP,                                                          // src stride
                          sliceC * INPUT_BPP);                                                    // dst stride
            dmaTask.wait();

            readElems += W;
            inLines[2] = (half*)(linesBuffer + ((bufferIdx % radix) * WP + padLeft)* sliceC * INPUT_BPP);

            // left padding;
            for (i = 0; i < sliceC*padLeft; i++)
            {
                *(inLines[2] - padLeft * sliceC + i) = PAD_VALUE;
            }

            //right padding
            for (i = 0; i < sliceC*padRight; i++)
            {
                *(inLines[2] + W * sliceC + i) = PAD_VALUE;
            }
        }
        else if (padBottom)
        {
            s32 padLine = line + radix - 1 - padTop - H;
            // bottom padding; user pad is zero for any padding scheme
            if (padLine < padBottom)
            {
                for(i = 0; i < sliceC*WP; i++)
                {
                    *((half*)linesBuffer + (bufferIdx % radix) * WP * sliceC + i) = PAD_VALUE;
                }
            }
        }

        if(line%strideY == 0)
        {
            kernelInLines[0] = (half*)linesBuffer;
            kernelInLines[1] = (half*)linesBuffer + 1 * sliceC;

            // again, align numElem to the next multiple of 8
            numElem = (sliceC * 2 * WP + 7) & ~0x7;
            mvcvMaximumV2_asm((half**)kernelInLines, (half**)kernelOutLines, numElem);

            kernelInLines[0] = (half*)outputBuffer;
            kernelInLines[1] = (half*)outputBuffer +   WP*sliceC;

            // again, align numElem to the next multiple of 8
            numElem = (sliceC * WP + 7) & ~0x7;
            mvcvMaximumV2_asm((half**)kernelInLines, (half**)kernelOutLines, numElem);

//          TODO : add dma ext ? or maybe not ... ?
            dma::Task(dmaUser).start(outputBuffer,                                            // src
                                     (u8*)(p->output + writtenElems * (ostrideX/INPUT_BPP)),  // dst
                                     Wout * sliceC * INPUT_BPP,                               // byte length
                                     sliceC * INPUT_BPP,                                      // src width
                                     sliceC * INPUT_BPP,                                      // dst width
                                     strideX * sliceC * INPUT_BPP,                            // src stride
                                     ostrideX);                                               // dst stride
            writtenElems += Wout;
        }
        line++;
    } while (writtenElems < (Wout * Hout));
}
