///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect code
///

#include "avgpool3x3_core.h"
#include "averageV3.h"
#include "mvTensor.h"
#include <mvTensorDma.h>

#define MIN(a, b)       ((a)<(b)?(a):(b))
#define MAX(a, b)       ((a)>(b)?(a):(b))

void mvAvgPool3x3(t_MvAvgPool3x3Param *p)
{
    u32 H = p->height, W = p->width, WP, HP, C = p->channels;
    u32 Hout=0, Wout=0;
    u32 sliceC = p->sliceC;
    u32 strideX = p->stride, strideY = p->stride;
    u32 ostrideX = p->ostrideX;
    u32 i, j;
    u8* inAddress = (u8*) ((u32) p->input);
    u8* linesBuffer;
    u8* outputBuffer;
    half* inLines[3];
    half* kernelInLines[3];
    half* kernelOutLines[1];
    u32 writtenElems = 0 , readElems = 0;
    u32 line = 0;
    // user specified padding (used only for CAFFE-style padding)
    u32 padUserX = p->pad, padUserY = p->pad;
    // convenience padding; used to simplify computations
    s32 padConvLeft, padConvRight, padConvTop, padConvBottom;
    s32 padLeft, padRight, padTop, padBottom, Hpad, Wpad;
    u32 numElem;

    // fix dmaLinkAgent
    using namespace mv::tensor;
    dma::Config dmaConfig = { 1, p->dmaLinkAgent };
    dma::User dmaUser(dmaConfig);
    dma::Task dmaTask(dmaUser);

    // set buffers to point to locations relative to cmxslice
    // the memory allocation is done in MatMul (84000 bytes/slice)
    linesBuffer = p->cmxslice;
    outputBuffer = p->cmxslice + CMX_DATA_SIZE/2;

    if (p->paddStyle == paddStyleTFSame)
    {
        // rules for TensorFlow SAME padding
        Hout = (H + strideY - 1) / strideY;
        Wout = (W + strideX - 1) / strideX;
        Hpad = ((Hout - 1) * strideY + 3 - H);
        Wpad = ((Wout - 1) * strideX + 3 - W);

        padConvLeft   = Wpad/2;
        padConvRight  = Wpad - padConvLeft;
        padConvTop    = Hpad/2;
        padConvBottom = Hpad - padConvTop;
        padUserX = 0;
        padUserY = 0;
    }
    else if (p->paddStyle == paddStyleCaffe)
    {
        // rules for CAFFE padding
        Hout = ((H + 2*padUserY - 3 + strideY - 1) / strideY) + 1;
        Wout = ((W + 2*padUserX - 3 + strideX - 1) / strideX) + 1;
        Hout = MIN(Hout, (H + padUserY + strideY - 1) / strideY);
        Wout = MIN(Wout, (W + padUserX + strideX - 1) / strideX);

        Hpad = ((Hout - 1) * strideY + 3 - H - 2*padUserY);
        Wpad = ((Wout - 1) * strideX + 3 - W - 2*padUserX);
        Hpad = MAX(0, Hpad);
        Wpad = MAX(0, Wpad);

        padConvLeft   = 0;
        padConvRight  = Wpad - padConvLeft;
        padConvTop    = 0;
        padConvBottom = Hpad - padConvTop;
    }
    else
    {
        // rules for TensorFlow VALID padding
        // treat unknown padding schemes as TF-VALID
        Hout = (H - 3 + 1 + strideY - 1) / strideY;
        Wout = (W - 3 + 1 + strideX - 1) / strideX;
        Hpad = Wpad = 0;
        padConvTop  = padConvBottom = 0;
        padConvLeft = padConvRight  = 0;
        padUserX = 0;
        padUserY = 0;
    }

    padLeft   = padUserX + padConvLeft;
    padRight  = padUserX + padConvRight;
    padTop    = padUserY + padConvTop;
    padBottom = padUserY + padConvBottom;

    HP = H + padTop + padBottom;
    WP = W + padLeft + padRight;

    for (i = padTop; i < 3; i++)
    {
         dma::Task(dmaUser).start(inAddress + readElems * C * INPUT_BPP,                 // src
                                  linesBuffer + (i*WP + padLeft) *  sliceC * INPUT_BPP,  // dst
                                  W * sliceC * INPUT_BPP,                                // byte length
                                  sliceC * INPUT_BPP,                                    // src width
                                  sliceC * INPUT_BPP,                                    // dst width
                                  C * INPUT_BPP,                                         // src stride
                                  sliceC * INPUT_BPP);                                   // dst stride
        readElems += W;
    }

    inLines[0] = (half*) linesBuffer + padLeft * sliceC;
    inLines[1] = (half*) linesBuffer + (WP + padLeft) * sliceC;
    inLines[2] = (half*) linesBuffer + (2 * WP + padLeft) * sliceC;

    // Padding: padding one row in column-major is equivalent with padding one column in row-major
    //          top row -> leftmost column, bottom row -> rightmost column

    // left padding;
    for (i = 0; i < sliceC*padUserX; i++)
    {
        *(inLines[0] - padUserX * sliceC + i) = 0;
        *(inLines[1] - padUserX * sliceC + i) = 0;
        *(inLines[2] - padUserX * sliceC + i) = 0;
    }
    // padConvLeft can only be 0 or 1
    for (i = 0; i < padConvLeft*sliceC; i++)
    {
        *(inLines[0] - padLeft * sliceC + i) =
                (*(inLines[0] + (-padLeft + 1) * sliceC + i) + *(inLines[0] + (-padLeft + 2) * sliceC + i))/2;
        *(inLines[1] - padLeft * sliceC + i) =
                (*(inLines[1] + (-padLeft + 1) * sliceC + i) + *(inLines[1] + (-padLeft + 2) * sliceC + i))/2;
        *(inLines[2] - padLeft * sliceC + i) =
                (*(inLines[2] + (-padLeft + 1) * sliceC + i) + *(inLines[2] + (-padLeft + 2) * sliceC + i))/2;
    }

    // right padding
    for (i = 0; i < sliceC*padUserX; i++)
    {
        *(inLines[0] + W * sliceC + i) = 0;
        *(inLines[1] + W * sliceC + i) = 0;
        *(inLines[2] + W * sliceC + i) = 0;
    }
    // padConvRight can only be 0 or 1
    for (i = 0; i < padConvRight*sliceC; i++)
    {
        *(inLines[0] + (W + padRight - 1) * sliceC + i) =
                (*(inLines[0] + (W + padRight - 2) * sliceC + i) + *(inLines[0] + (W + padRight - 3) * sliceC + i))/2;
        *(inLines[1] + (W + padRight - 1) * sliceC + i) =
                (*(inLines[1] + (W + padRight - 2) * sliceC + i) + *(inLines[1] + (W + padRight - 3) * sliceC + i))/2;
        *(inLines[2] + (W + padRight - 1) * sliceC + i) =
                (*(inLines[2] + (W + padRight - 2) * sliceC + i) + *(inLines[2] + (W + padRight - 3) * sliceC + i))/2;
    }

    // top padding; user pad is zero for any padding scheme
    for (j = (u32)padConvTop; j < (u32)padTop; j++)
    {
        for(i = 0; i < sliceC*WP; i++)
            *(inLines[j] + -padLeft*sliceC + i) = 0;
    }
    // padConvTop is either 0 or 1
    if (padConvTop == 1)
    {
        for(i = 0; i < sliceC*WP; i++)
        {
            *(inLines[0] - padLeft*sliceC + i) = (*(inLines[1] - padLeft*sliceC + i) + *(inLines[2] - padLeft*sliceC + i))/2;
        }
    }

    kernelInLines[0] = (half*)linesBuffer;
    kernelInLines[1] = (half*)linesBuffer + 1 * sliceC;
    kernelInLines[2] = (half*)linesBuffer + 2 * sliceC;

    kernelOutLines[0] = (half*)outputBuffer;

    // align numElem to the next multiple of 8, as this is a constraint of the
    // asm implementation of mvcvMaximumV3_asm
    // this works because sliceC was chosen such that there are at least
    // 7*INPUT_BPP bytes at the end of the input/output buffers
    numElem = (sliceC * 3 * WP + 7) & ~0x7;
    mvcvAverageV3_asm((half**)kernelInLines, (half**)kernelOutLines, numElem);

    kernelInLines[0] = (half*)outputBuffer;
    kernelInLines[1] = (half*)outputBuffer +   WP*sliceC;
    kernelInLines[2] = (half*)outputBuffer + 2*WP*sliceC;

    // again, align numElem to the next multiple of 8
    numElem = (sliceC * W + 7) & ~0x7;
    mvcvAverageV3_asm((half**)kernelInLines, (half**)kernelOutLines, numElem);

    dmaTask.start(outputBuffer,                   // src
                  (u8*)p->output,                 // dst
                  Wout * sliceC * INPUT_BPP,      // byte length
                  sliceC * INPUT_BPP,             // src width
                  sliceC * INPUT_BPP,             // dst width
                  strideX * sliceC * INPUT_BPP,   // src stride
                  ostrideX);                      // dst stride
    dmaTask.wait();

    writtenElems += Wout;

    line = 1;
    do
    {
        u32 bufferIdx = (line - 1);
        if(readElems < W*H)
        {
            dmaTask.start(inAddress + readElems * C * INPUT_BPP,                              // src
                          linesBuffer + ((bufferIdx % 3) * WP + padLeft)* sliceC * INPUT_BPP, // dst
                          W * sliceC * INPUT_BPP,                                             // byte length
                          sliceC * INPUT_BPP,                                                 // src width
                          sliceC * INPUT_BPP,                                                 // dst width
                          C * INPUT_BPP,                                                      // src stride
                          sliceC * INPUT_BPP);                                                // dst stride
            dmaTask.wait();

            readElems += W;
            inLines[2] = (half*)(linesBuffer + ((bufferIdx % 3) * WP + padLeft)* sliceC * INPUT_BPP);

            // left padding;
            for (i = 0; i < sliceC*padUserX; i++)
            {
                *(inLines[2] - padUserX * sliceC + i) = 0;
            }
            // padConvLeft can only be 0 or 1
            for (i = 0; i < padConvLeft*sliceC; i++)
            {
                *(inLines[2] - padLeft * sliceC + i) =
                        (*(inLines[2] + (-padLeft + 1) * sliceC + i) + *(inLines[2] + (-padLeft + 2) * sliceC + i))/2;
            }

            //right padding
            for (i = 0; i < sliceC*padUserX; i++)
            {
                *(inLines[2] + W * sliceC + i) = 0;
            }
            // padConvRight can only be 0 or 1
            for (i = 0; i < padConvRight*sliceC; i++)
            {
                *(inLines[2] + (W + padRight - 1) * sliceC + i) =
                        (*(inLines[2] + (W + padRight - 2) * sliceC + i) + *(inLines[2] + (W + padRight - 3) * sliceC + i))/2;
            }
        }
        else if (padBottom)
        {
            s32 padLine = line + 3 - 1 - padTop - H;
            // bottom padding; user pad is zero for any padding scheme
            if (padLine < (s32)padUserY)
            {
                for(i = 0; i < sliceC*WP; i++)
                {
                    *((half*)linesBuffer + (bufferIdx % 3) * WP * sliceC + i) = 0;
                }
            }
            else if (padLine == (s32)padUserY)
            {
                // padConvBottom is either 0 or 1
                for(i = 0; i < sliceC*WP; i++)
                {
                    *((half*)linesBuffer + ((bufferIdx % 3) * WP) * sliceC + i) =
                            (*((half*)linesBuffer + (((bufferIdx+1) % 3) * WP) * sliceC + i) +
                             *((half*)linesBuffer + (((bufferIdx+2) % 3) * WP) * sliceC + i))/2;
                }
            }
        }

        if(line%strideY == 0)
        {
            kernelInLines[0] = (half*)linesBuffer;
            kernelInLines[1] = (half*)linesBuffer + 1 * sliceC;
            kernelInLines[2] = (half*)linesBuffer + 2 * sliceC;
            // again, align numElem to the next multiple of 8
            numElem = (sliceC * 3 * WP + 7) & ~0x7;
            mvcvAverageV3_asm((half**)kernelInLines, (half**)kernelOutLines, numElem);

            kernelInLines[0] = (half*)outputBuffer;
            kernelInLines[1] = (half*)outputBuffer +   WP*sliceC;
            kernelInLines[2] = (half*)outputBuffer + 2*WP*sliceC;

            // again, align numElem to the next multiple of 8
            numElem = (sliceC * W + 7) & ~0x7;
            mvcvAverageV3_asm((half**)kernelInLines, (half**)kernelOutLines, numElem);

            dma::Task(dmaUser).start(outputBuffer,                                           // src
                                     (u8*)(p->output + writtenElems * (ostrideX/INPUT_BPP)), // dst
                                     Wout * sliceC * INPUT_BPP,                              // byte length
                                     sliceC * INPUT_BPP,                                     // src width
                                     sliceC * INPUT_BPP,                                     // dst width
                                     strideX * sliceC * INPUT_BPP,                           // src stride
                                     ostrideX);                                              // dst stride
            writtenElems += Wout;
        }
        line++;
    } while (writtenElems < (Wout * Hout));
}
