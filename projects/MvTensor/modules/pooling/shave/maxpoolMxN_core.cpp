///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect code
///

#include "maxpoolMxN_core.h"
#include <maximumV2.h>
#include "mvTensor.h"
#include <mvTensorDma.h>

#define MIN(a, b) ((a)<(b)?(a):(b))
#define MAX(a, b) ((a)>(b)?(a):(b))

using namespace mv::tensor;

static void maxMx1_kernel(dma::User &dmaUser,
                          u8* linesBuffer, u8* outputBuffer, u32 radix, u32 elemDist, u32 numElem)
{
    u32 x, span, hasIntermediateOut;
    half* inputLines[2];
    half* outputLines[1];

    x = 1;
    span = radix;
    hasIntermediateOut = 0;

    do
    {
        if (span%2 == 1)
        {
            inputLines[0] = (half*)linesBuffer + x * (span-1) * elemDist;
            inputLines[1] = (half*)outputBuffer;
            if (hasIntermediateOut == 0)
            {
                hasIntermediateOut = 1;
                // this is the first element written to the output buffer -> a simple copy is sufficient
                dma::Task(dmaUser).start((u8*)inputLines[0], (u8*)outputBuffer, numElem * INPUT_BPP);
            }
            else
            {
                outputLines[0] = (half*)outputBuffer;

                mvcvMaximumV2_asm((half**)inputLines, (half**)outputLines, numElem);
            }
        }
         if (radix == 1)
         {
             hasIntermediateOut = 0;
             break;
         }
        inputLines[0] = (half*)linesBuffer;
        inputLines[1] = (half*)linesBuffer + x * elemDist;
        outputLines[0] = (half*)linesBuffer;

        x = x*2;
        span = span/2;

        // last iteration writes to output buffer
        if (span == 1 && !hasIntermediateOut)
        {
            outputLines[0] = (half*)outputBuffer;
        }

        mvcvMaximumV2_asm((half**)inputLines, (half**)outputLines, numElem);
    } while (span > 1);

    if (hasIntermediateOut)
    {
        inputLines[0] = (half*)linesBuffer;
        inputLines[1] = (half*)outputBuffer;
        outputLines[0] = (half*)outputBuffer;

        mvcvMaximumV2_asm((half**)inputLines, (half**)outputLines, numElem);
    }
}

void mvMaxPoolMxN(t_MvMaxPoolMxNParam *p)
{
    u32 radixX = p->radixX, radixY = p->radixY;
    u32 strideX = p->strideX, strideY = p->strideY;
    // convenience padding; used to simplify computations
    u32 padConvLeft = radixX/2, padConvRight = radixX/2, padConvTop = radixY/2, padConvBottom = radixY/2;
    u32 H = p->height, HP = p->height + padConvTop + padConvBottom;
    u32 Hout, Wout;
    u32 W = p->width, WP;
    // user specified padding (used only for CAFFE-style padding)
    u32 padUserX = p->padX, padUserY = p->padY;
    // total padding: user + convenience padding
    u32 padLeft, padRight, padTop, padBottom;
    u32 C = p->channels;
    u32 sliceC = p->sliceC;
    u32 ostrideX = p->ostrideX;
    u8* inAddr = (u8*)p->input;
    u8* outAddr = (u8*)p->output;
    u8* interimAddr = (u8*)p->output;
    u32 i;
    u8* linesBuffer;
    u8* outputBuffer;
    u32 writtenElems = 0 , readElems = 0;
    u32 procLines, procCols;
    s32 Hpad, Wpad;
    u32 numElem;

    // dmaLinkAgent
    dma::Config dmaConfig = { 1, p->dmaLinkAgent };
    dma::User dmaUser(dmaConfig);

    // set buffers to point to locations relative to cmxslice
    linesBuffer      = p->cmxslice;
    outputBuffer     = p->cmxslice + CMX_DATA_SIZE/2;

    if (p->paddStyle == paddStyleTFSame)
    {
        // rules for TensorFlow SAME padding
        Hout = (H + strideY - 1) / strideY;
        Wout = (W + strideX - 1) / strideX;
        Hpad = ((Hout - 1) * strideY + radixY - H);
        Wpad = ((Wout - 1) * strideX + radixX - W);
        Hpad = MAX(0, Hpad);
        Wpad = MAX(0, Wpad);

        padConvLeft   = Wpad/2;
        padConvRight  = Wpad - padConvLeft;
        padConvTop    = Hpad/2;
        padConvBottom = Hpad - padConvTop;
        padUserX = 0;
        padUserY = 0;
    }
    else if (p->paddStyle == paddStyleCaffe)
    {
        // rules for CAFFE padding
        Hout = ((H + 2*padUserY - radixY + strideY - 1) / strideY) + 1;
        Wout = ((W + 2*padUserX - radixX + strideX - 1) / strideX) + 1;
        Hout = MIN(Hout, (H + padUserY + strideY - 1) / strideY);
        Wout = MIN(Wout, (W + padUserX + strideX - 1) / strideX);

        Hpad = ((Hout - 1) * strideY + radixY - H - 2*padUserY);
        Wpad = ((Wout - 1) * strideX + radixX - W - 2*padUserX);
        Hpad = MAX(0, Hpad);
        Wpad = MAX(0, Wpad);

        padConvLeft   = 0;
        padConvRight  = Wpad - padConvLeft;
        padConvTop    = 0;
        padConvBottom = Hpad - padConvTop;
    }
    else
    {
        // rules for TensorFlow VALID padding
        // treat unknown padding schemes as TF-VALID
        Hout = (H - radixY + 1 + strideY - 1) / strideY;
        Wout = (W - radixX + 1 + strideX - 1) / strideX;
        Hpad = Wpad = 0;
        padConvTop  = padConvBottom = 0;
        padConvLeft = padConvRight  = 0;
        padUserX = 0;
        padUserY = 0;
    }

    padLeft   = padUserX + padConvLeft;
    padRight  = padUserX + padConvRight;
    padTop    = padUserY + padConvTop;
    padBottom = padUserY + padConvBottom;

    HP = p->height + padTop + padBottom;
    WP = p->width + padLeft + padRight;

    // Compute how many rows/cols we can store in the available memory
    // memory needed to store one row (+padding): WP*sliceC
    procLines = (CMX_DATA_SIZE/2 - 7*INPUT_BPP) / (WP*sliceC*INPUT_BPP);
    // memory needed to store one column (+padding): HP*sliceC
    procCols  = (CMX_DATA_SIZE/2 - 7*INPUT_BPP) / (HP*sliceC*INPUT_BPP);

    interimAddr = inAddr;
    if (Wout > W)
    {
        interimAddr = outAddr;
    }
    // apply horizontal kernel
    readElems = 0;
    writtenElems = 0;
    do
    {
        procLines = MIN(procLines, H-readElems);
        for (i=0; i<procLines; i++)
        {
            dma::Task(dmaUser).start(inAddr + readElems * W * C * INPUT_BPP,                 // src
                                     linesBuffer + (i * WP + padLeft ) * sliceC * INPUT_BPP, // dst
                                     W * sliceC * INPUT_BPP,                                 // byte length
                                     sliceC * INPUT_BPP,                                     // src width
                                     sliceC * INPUT_BPP,                                     // dst width
                                     C * INPUT_BPP,                                          // src stride
                                     sliceC * INPUT_BPP);                                    // dst stride
            readElems += 1;
        }

        for (u32 line = 0; line<procLines; line++)
        {
            half* inLine = (half*) linesBuffer + line * WP * sliceC;

            // padding is faster when using the DMA when the number of shaves is low;
            // if there are many shaves, the DMA gets overbooked and the performance decreases
#ifndef USE_DMA_PADDING
            u32 j;
            // left padding
            for (j=0; j<padLeft; j++)
            {
                for (i = 0; i < sliceC; i++)
                {
                    *(inLine + j * sliceC + i) = *(inLine + padLeft * sliceC + i);
                }
            }

            // right padding
            for (j=0; j<padRight; j++)
            {
                for (i = 0; i < sliceC; i++)
                {
                    *(inLine + (padLeft+W+j) * sliceC + i) = *(inLine + (padLeft+W-1) * sliceC + i);
                }
            }
#else
            if (padLeft > 0)
            {
                dma::Task(dmaUser).start((u8*)(inLine + padLeft * sliceC),  // src
                                         (u8*)(inLine),                     // dst
                                         padLeft * sliceC * INPUT_BPP,      // byte length
                                         sliceC * INPUT_BPP,                // src width
                                         sliceC * INPUT_BPP,                // dst width
                                         0,                                 // src stride
                                         sliceC * INPUT_BPP);               // dst stride
            }
            if (padRight > 0)
            {
                dma::Task(dmaUser).start((u8*)(inLine + (padLeft+W-1) * sliceC),// src
                                         (u8*)(inLine + (padLeft+W) * sliceC),  // dst
                                         padRight * sliceC * INPUT_BPP,         // byte length
                                         sliceC * INPUT_BPP,                    // src width
                                         sliceC * INPUT_BPP,                    // dst width
                                         0,                                     // src stride
                                         sliceC * INPUT_BPP);                   // dst stride
            }
#endif
        }

        // align the processing width to the next multiple of 8 (constraint of the asm implementation)
        numElem = (sliceC * WP * procLines + 7) & ~7;
        maxMx1_kernel(dmaUser, linesBuffer, outputBuffer, radixX, sliceC, numElem);

        for (i=0; i<procLines; i++)
        {
            dma::Task(dmaUser).start(outputBuffer + (i * WP * sliceC) * INPUT_BPP,      // src
                                     interimAddr + writtenElems * Wout * C * INPUT_BPP, // dst
                                     Wout * sliceC * INPUT_BPP,                         // byte length
                                     sliceC * INPUT_BPP,                                // src width
                                     sliceC * INPUT_BPP,                                // dst width
                                     strideX * sliceC * INPUT_BPP,                      // src stride
                                     C * INPUT_BPP);                                    // dst stride

           writtenElems += 1 ;
        }
    } while (readElems < H);

    // apply vertical kernel
    readElems = 0;
    writtenElems = 0;

    do
    {
        procCols = MIN(procCols, Wout - readElems);
        for (i=0; i<procCols; i++)
        {
            dma::Task(dmaUser).start(interimAddr + readElems * C * INPUT_BPP,               // src
                                     linesBuffer + (i * HP + padTop ) * sliceC * INPUT_BPP, //dst
                                     H * sliceC * INPUT_BPP,                                // byte length
                                     sliceC     * INPUT_BPP,                                // src width
                                     sliceC     * INPUT_BPP,                                // dst width
                                     Wout * C   * INPUT_BPP,                                // src stride
                                     sliceC     * INPUT_BPP);                               // dst stride






            readElems += 1;
        }

        for (u32 col = 0; col<procCols; col++)
        {
            half* inCol = (half*) linesBuffer + col * HP * sliceC;

            // padding is faster when using the DMA when the number of shaves is low;
            // if there are many shaves, the DMA gets overbooked and the performance decreases
#ifndef USE_DMA_PADDING
            u32 j;
            // top padding
            for (j=0; j<padTop; j++)
            {
                for (i = 0; i < sliceC; i++)
                {
                    *(inCol + j * sliceC + i) = *(inCol + padTop * sliceC + i);
                }
            }

            for (j=0; j<padBottom; j++)
            {
                for (i = 0; i < sliceC; i++)
                {
                    // bottom padding
                    *(inCol + (padTop+H+j) * sliceC + i) = *(inCol + (padTop+H-1) * sliceC + i);
                }
            }
#else
            if (padTop > 0)
            {
                dma::Task(dmaUser).start((u8*)(inCol + padTop * sliceC),// src
                                         (u8*)(inCol),                  // dst
                                          padTop * sliceC * INPUT_BPP,  // byte length
                                          sliceC * INPUT_BPP,           // src width
                                          sliceC * INPUT_BPP,           // dst width
                                          0,                            // src stride
                                          sliceC * INPUT_BPP);          // dst stride
            }
            if (padBottom > 0)
            {
                dma::Task(dmaUser).start((u8*)(inCol + (padTop+H-1) * sliceC), // src
                                         (u8*)(inCol + (padTop+H) * sliceC),   // dst
                                         padBottom * sliceC * INPUT_BPP,       // byte length
                                         sliceC * INPUT_BPP,                   // src width
                                         sliceC * INPUT_BPP,                   // dst width
                                         0,                                    // src stride
                                         sliceC * INPUT_BPP);                  // dst stride
            }
#endif
        }

        // align the processing width to the next multiple of 8 (constraint of the asm implementation)
        numElem = (sliceC * HP * procCols + 7) & ~7;
        maxMx1_kernel(dmaUser, linesBuffer, outputBuffer, radixY, sliceC, numElem);

        for (i=0; i<procCols; i++)
        {
            dma::Task(dmaUser).start(outputBuffer + (i * HP * sliceC) * INPUT_BPP, // src
                                     outAddr + writtenElems * ostrideX,            // dst
                                     Hout * sliceC * INPUT_BPP,                    // byte length
                                     sliceC * INPUT_BPP,                           // src width
                                     sliceC * INPUT_BPP,                           // dst width
                                     strideY * sliceC * INPUT_BPP,                 // src stride
                                     Wout * ostrideX);                             // dst stride

            writtenElems += 1;
        }
    } while (readElems < Wout);
}
