///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect code
///

#include "avgpool7x7xk_core.h"
#include <mvTensorDma.h>

#define INPUT_BPP 2

static void avgPool7x7xk(half** in, half **out, u32 k)
{
    u32 i, j;
    float sum;
    const float f49 = (float)0.02040816326530612;

    for( j = 0; j < k; j++)
    {
        sum = 0;
        for( i = 0; i < 49; i++)
            sum += in[i][j];
        out[0][j] = (half) (sum * f49);
    }
}

void mvAvgPool7x7xk(t_MvAvgPool7x7xkParam *p)
{
    u32 C = p->channels;
    u32 sliceC = p->sliceC;
    u32 i;
    u8* inAddress = (u8*) ((u32) p->input);
    u8* inputBuffer;
    u8* outputBuffer;
    half* inputLines[49];
    half* outputLine[1];

    // fix dmaLinkAgent
    using namespace mv::tensor;
    dma::Config dmaConfig = { 1, p->dmaLinkAgent };
    dma::User dmaUser(dmaConfig);
    dma::Task dmaTask(dmaUser);
                    ;
    // set buffers to point to locations relative to cmxslice
    // the memory allocation is done in MatMul (88000 bytes/slice)
    inputBuffer = p->cmxslice;
    outputBuffer = p->cmxslice + 81928;
    outputLine[0] = (half*)outputBuffer;

    dmaTask.start(inAddress,                  // src
                  inputBuffer,                // dst
                  7 * 7 * sliceC * INPUT_BPP, // byte length
                  sliceC * INPUT_BPP,         // src width
                  sliceC * INPUT_BPP,         // dst width
                  C * INPUT_BPP,              // src stride
                  sliceC * INPUT_BPP);        // dst stride
    dmaTask.wait();

    for(i = 0 ; i < 49; i++)
        inputLines[i] = (half*)inputBuffer + i * sliceC;

    avgPool7x7xk(inputLines, outputLine, sliceC);

    dmaTask.start(outputBuffer,               // src
                  (u8*)p->output,             // dst
                  1 * 1 * sliceC * INPUT_BPP, // byte length
                  sliceC * INPUT_BPP,         // src width
                  sliceC * INPUT_BPP,         // dst width
                  sliceC * INPUT_BPP,         // src stride
                  C * INPUT_BPP);             // dst stride

    dmaTask.wait();
}
