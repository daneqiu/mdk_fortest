BASE_PATH := $(MVTENSOR_MODULES_PATH)/pooling

# Dependencies

MVMODULE_NEED_MATMUL :=

# Flags

# Headers

MVMODULE_LEON_HEADER_PATHS :=
MVMODULE_SHAVE_HEADER_PATHS :=

# do not add $(BASE_PATH)/priv here
# it should be used only by internal sources, like this
# include "../priv/header_name.h"

# do not add $(BASE_PATH)/leon here
# it should be used only by sources in the same folder, like this
# include "header_name.h"

# do not add $(BASE_PATH)/shave here
# it should be used only by sources in the same folder, like this
# include "header_name.h"

# Sources

MVMODULE_LEON_COMPONENT_PATHS := $(BASE_PATH)/leon
MVMODULE_SHAVE_COMPONENT_PATHS := $(BASE_PATH)/shave

# Linker options

# Shave entry points

MVMODULE_ENTRY_POINTS := \
	-u mvAvgPool3x3 \
	-u mvAvgPool7x7xk \
	-u mvAvgPoolMxN \
	-u mvMaxPool2x2 \
	-u mvMaxPool3x3 \
	-u mvMaxPoolMxN

# Enable the module

include $(MVTENSOR_MODULES_PATH)/enable_module.mk
