#include <stdio.h>
#include <stdlib.h>

#include <DrvSvu.h>
#include <DrvTimer.h>
#include <DrvShaveL2Cache.h>
#include <mv_types.h>

//#include <mvROIPoolingParam.h>
#include <mvTensorInternal.h>
#include "mvResize.h"
#include <mvMacros.h>
#include "../priv/mvResizeParam.h"
#include <mvTensorInternal.h>
#include <mvModuleHandle.h>

// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------
// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
t_MvResizeParam resizeParam[MVTENSOR_MAX_SHAVES] NOCACHE;
#define DMA_DESCRIPTORS_SECTION __attribute__((section(".cmx.cdmaDescriptors")))
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------

const u32 ALIGN_VALUE3 = 64;
namespace
{
    using namespace mv::tensor;
    class Resize : public Layer
    {
	virtual void run_(const t_MvTensorParam *mvTensorParam,
                const t_MvTensorOp &op,
                const mv::tensor::Optimization &,
                const Resources &)
    {
        ReSize((fp16*)mvTensorParam->input->data,
                mvTensorParam->input->dimY, mvTensorParam->input->dimX, mvTensorParam->input->dimZ,
                mvTensorParam->weights->dimX, mvTensorParam->weights->dimZ,
                (fp16*)mvTensorParam->output->data, mvTensorParam->myriadResources);

				
	} 
    };

	const ModuleHandle handles[] =
    {
        ModuleHandle().add<Resize>(kResize)
    };
}

MODULE_ENTRY_DECL(mvResize);
void ReSize(fp16* input, u32 in_height, u32 in_width, u32 in_channels,
                 u32 type, u32 ratio,
                 fp16* output, t_MvTensorMyriadResources *myriadResources) 
{	  
	//printf("the value of type %d, ratio %d...\n", type, ratio);
	u8 number_shave = myriadResources->lastShave - myriadResources->firstShave + 1;
	u32 total = in_height * in_width * in_channels;
	u32 each_sum = total/number_shave;
	
	  for (int i = myriadResources->firstShave; i <= myriadResources->lastShave; i++) {
	        resizeParam[i].input = input;
	        resizeParam[i].in_height = in_height;
	        resizeParam[i].in_width = in_width;
	        resizeParam[i].in_channels = in_channels;
	        resizeParam[i].ratio = ratio;
	        resizeParam[i].type = type;
	        resizeParam[i].temp_data = NULL;
	        resizeParam[i].output = output;
	        resizeParam[i].dmaLinkAgent = myriadResources->dmaLinkAgent;
	        resizeParam[i].cmxslice = getCMXSliceDataSection(i);
            resizeParam[i].start_point = i*each_sum;
			resizeParam[i].end_point = i*each_sum + each_sum;
			if (i == myriadResources->lastShave)
				resizeParam[i].end_point = i*each_sum + each_sum + total%number_shave;
	        startShave(i, (u32)&MODULE_ENTRY(mvResize), (u32)&resizeParam[i]);
	    }		
		waitShaves(myriadResources->firstShave, myriadResources->lastShave);

    DrvShaveL2CachePartitionFlushAndInvalidate(myriadResources->dataPartitionNo);	
}
