#ifndef _RESIZE_H_
#define _RESIZE_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include <mv_types.h>
#include <mvTensor.h>

#ifdef __cplusplus
extern "C"
{
#endif

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------
// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------

void ReSize (fp16* input, u32 in_height, u32 in_width, u32 in_channels,
                 u32 type, u32 ratio,
                 fp16* output, t_MvTensorMyriadResources *myriadResources);

#ifdef __cplusplus
}
#endif

#endif  //_RESIZE_H_

