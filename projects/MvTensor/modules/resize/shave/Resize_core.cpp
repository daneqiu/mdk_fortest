#include <stdio.h>
#include <string.h>
#include <math.h>

#include <moviVectorUtils.h>
#include <swcCdma.h>
#include <svuCommonShave.h>
#include <mv_types.h>
#include <math.h>
#include <mvMacros.h>

#include <accumulateFp16.h>
#include "../priv/mvResizeParam.h"
#include "Resize_core.h"

void count_to_xyz(int count, int* x, int* y, int* z, int w, int h,int k) 
{
	//*x = *y =*z = 0;
	*z = count%k;
    int temp = count/k;
	*x = temp%w;
	*y = temp/w; 
}

void mvResize(t_MvResizeParam*p) {
	fp16* input = p->input;
	u32 in_height = p->in_height;
	u32 in_width = p->in_width;
	u32 in_channels = p->in_channels;
    u32 type = p->type;
	u32 ratio = p->ratio;
    fp16* output = p->output;

	if(type == 0) {
		int x,y,z,tmp;
		for(int i=p->start_point; i<p->end_point;i++) {
			//count_to_xyz(i, &x, &y,&z, in_width, in_height,in_channels);
			z = i%in_channels;
			tmp = i/in_channels;
			x = tmp%in_width;
			y = tmp/in_width;
			fp16 temp_d = input[i];
			for(int j=0;j<ratio*ratio;j++) {
				int x1,y1,z1, addr;
				z1 = z;
				x1 = ratio*x + j%ratio;
				y1 = ratio*y + j/ratio;
				addr = (x1 + y1*in_width*ratio)*in_channels + z;
				output[addr] = temp_d;
			}
			
		}
	}else {
		printf("we don't support the type %d yet", type);
	}
#if 0
	if(type == 0) {
		for(int i=0;i<in_channels;i++) {
		  	fp16* addr_s = input + in_width*in_height*i;
			fp16* addr_d = output +in_width*in_height*i*ratio*ratio; 
			for(int j=0;j<in_height;j++) {
				for(int k=0;k<in_width;k++) {
					fp16 tmp = addr_s[in_width*j + k];
					addr_d[in_width*ratio*j *ratio + k*ratio] = tmp;
				    addr_d[in_width*ratio*j *ratio + k*ratio + 1] = tmp;
					addr_d[in_width*ratio*j *ratio + k*ratio + in_width*ratio] = tmp;
					addr_d[in_width*ratio*j *ratio + k*ratio + in_width*ratio + 1] = tmp;
				}
			}

		  }
	} else {

		printf("we don't support the type %d yet", type);
	}
#endif
}
