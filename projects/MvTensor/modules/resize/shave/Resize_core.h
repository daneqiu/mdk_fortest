#ifndef _MVRESIZE_CORE_H_
#define _MVRESIZE_CORE_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include "../priv/mvResizeParam.h"

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------

// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif

void mvResize(t_MvResizeParam *p);

#ifdef __cplusplus
}
#endif

#endif //__MVROIPOOLING_CORE_H__

