#ifndef _MV_RESIZEPARAM_H_
#define _MV_RESIZEPARAM_H_
#include <mv_types.h>

#ifdef __MOVICOMPILE__
    #include <moviVectorTypes.h>
#else
    typedef fp16 half;
#endif
#define INPUT_BPP       2

/// mvResize global parameters structure
typedef struct
{
    half* input;
    u32 in_height;
    u32 in_width;
    u32 in_channels;
    u32 ratio;
    u32 type;
    half* output;	
    u8* temp_data;
    u8* cmxslice;
    u32 dmaLinkAgent;
	u32 start_point;
	u32 end_point;
} t_MvResizeParam;

#endif  //_MV_RESIZEPARAM_H_

