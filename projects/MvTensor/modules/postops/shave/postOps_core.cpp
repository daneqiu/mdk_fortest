///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief    Code for CNN post operations: relu, prelu and bias.
///

#include <cstring>
#include <math.h>
#include "postOps_core.h"
#include <mvTensorDma.h>
#include <mvTensorConfig.h>

#define CMX_SIZE              MVTENSOR_HEAP_DATA_SIZE
#define X_MAX                 65000
#define UNROLL_SIZE           8 // Changes to this should be reflected in the code as well.
#define MIN_DMA_TRANSFER_SIZE 512
#define USE_DMA               1

// Tools older than 00.50.79.2 have a different define
#ifndef pows
  #define pows(a, b) __hpow(a, b)
#endif

namespace
{
    void reluFp16(half8 * __restrict__ data_in,
                  half8 * __restrict__ data_out,
                  half8 * __restrict__ weights,
                  half8 * __restrict__ bias,
                  s32 no_lines, s32 line_size, half x,
                  void *parameters)
    {
        UNUSED(weights);
        UNUSED(bias);
        UNUSED(parameters);
        // todo: see if std::numeric_limits<half>::max(); works here
        const half8  x_vec = (x <= half(0.0)) ? (half8)(X_MAX) : (half8)x;

        s32 i = 0;
        for(i = 0; i < (((no_lines * line_size) / UNROLL_SIZE) * UNROLL_SIZE); i += UNROLL_SIZE)
        {
            data_out[i + 0] = __builtin_shave_cmu_clamp0_f16_rr_half8(data_in[i + 0], x_vec);
            data_out[i + 1] = __builtin_shave_cmu_clamp0_f16_rr_half8(data_in[i + 1], x_vec);
            data_out[i + 2] = __builtin_shave_cmu_clamp0_f16_rr_half8(data_in[i + 2], x_vec);
            data_out[i + 3] = __builtin_shave_cmu_clamp0_f16_rr_half8(data_in[i + 3], x_vec);
            data_out[i + 4] = __builtin_shave_cmu_clamp0_f16_rr_half8(data_in[i + 4], x_vec);
            data_out[i + 5] = __builtin_shave_cmu_clamp0_f16_rr_half8(data_in[i + 5], x_vec);
            data_out[i + 6] = __builtin_shave_cmu_clamp0_f16_rr_half8(data_in[i + 6], x_vec);
            data_out[i + 7] = __builtin_shave_cmu_clamp0_f16_rr_half8(data_in[i + 7], x_vec);
        }

        for(; i < (no_lines * line_size); ++i)
            data_out[i] = __builtin_shave_cmu_clamp0_f16_rr_half8(data_in[i], x_vec);
    }

    void reluFp16WithBias(half8 * __restrict__ data_in,
                          half8 * __restrict__ data_out,
                          half8 * __restrict__ weights,
                          half8 * __restrict__ bias,
                          s32 no_lines, s32 line_size, half x,
                          void *parameters)
    {
        UNUSED(weights);
        UNUSED(x);
        UNUSED(parameters);

        const half8  x_vec = (x <= half(0.0)) ? (half8)(X_MAX) : (half8)x;

        s32 i = 0;
        for(s32 bias_i = 0; bias_i < line_size; ++bias_i)
        {
            for(i = 0; i < (no_lines / UNROLL_SIZE) * (UNROLL_SIZE); i += UNROLL_SIZE)
            {
                data_out[(i + 0) * line_size + bias_i] = __builtin_shave_cmu_clamp0_f16_rr_half8(data_in[(i + 0) * line_size + bias_i] + bias[bias_i], x_vec);
                data_out[(i + 1) * line_size + bias_i] = __builtin_shave_cmu_clamp0_f16_rr_half8(data_in[(i + 1) * line_size + bias_i] + bias[bias_i], x_vec);
                data_out[(i + 2) * line_size + bias_i] = __builtin_shave_cmu_clamp0_f16_rr_half8(data_in[(i + 2) * line_size + bias_i] + bias[bias_i], x_vec);
                data_out[(i + 3) * line_size + bias_i] = __builtin_shave_cmu_clamp0_f16_rr_half8(data_in[(i + 3) * line_size + bias_i] + bias[bias_i], x_vec);
                data_out[(i + 4) * line_size + bias_i] = __builtin_shave_cmu_clamp0_f16_rr_half8(data_in[(i + 4) * line_size + bias_i] + bias[bias_i], x_vec);
                data_out[(i + 5) * line_size + bias_i] = __builtin_shave_cmu_clamp0_f16_rr_half8(data_in[(i + 5) * line_size + bias_i] + bias[bias_i], x_vec);
                data_out[(i + 6) * line_size + bias_i] = __builtin_shave_cmu_clamp0_f16_rr_half8(data_in[(i + 6) * line_size + bias_i] + bias[bias_i], x_vec);
                data_out[(i + 7) * line_size + bias_i] = __builtin_shave_cmu_clamp0_f16_rr_half8(data_in[(i + 7) * line_size + bias_i] + bias[bias_i], x_vec);
            }

            for(; i < no_lines; ++i)
                data_out[i * line_size + bias_i] = __builtin_shave_cmu_clamp0_f16_rr_half8(data_in[i * line_size + bias_i] + bias[bias_i], x_vec);
        }
    }

    void reluNegSlopeFp16(half8 * __restrict__ data_in,
                          half8 * __restrict__ data_out,
                          half8 * __restrict__ weights,
                          half8 * __restrict__ bias,
                          s32 no_lines, s32 line_size, half x,
                          void *parameters)
    {
        UNUSED(bias);
        UNUSED(weights);
        UNUSED(parameters);

        const half8 neg_slope = (half8)x;

        s32 i = 0;
        for(s32 bias_i = 0; bias_i < line_size; ++bias_i)
        {
            for(i = 0; i < (no_lines / UNROLL_SIZE) * (UNROLL_SIZE); i += UNROLL_SIZE)
            {
                data_out[(i + 0) * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 0) * line_size + bias_i], neg_slope * (data_in[(i + 0) * line_size + bias_i]));
                data_out[(i + 1) * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 1) * line_size + bias_i], neg_slope * (data_in[(i + 1) * line_size + bias_i]));
                data_out[(i + 2) * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 2) * line_size + bias_i], neg_slope * (data_in[(i + 2) * line_size + bias_i]));
                data_out[(i + 3) * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 3) * line_size + bias_i], neg_slope * (data_in[(i + 3) * line_size + bias_i]));
                data_out[(i + 4) * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 4) * line_size + bias_i], neg_slope * (data_in[(i + 4) * line_size + bias_i]));
                data_out[(i + 5) * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 5) * line_size + bias_i], neg_slope * (data_in[(i + 5) * line_size + bias_i]));
                data_out[(i + 6) * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 6) * line_size + bias_i], neg_slope * (data_in[(i + 6) * line_size + bias_i]));
                data_out[(i + 7) * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 7) * line_size + bias_i], neg_slope * (data_in[(i + 7) * line_size + bias_i]));
            }

            for(; i < no_lines; ++i)
                data_out[i * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[i * line_size + bias_i], neg_slope * (data_in[i * line_size + bias_i]));
        }
    }

    void reluNegSlopeFp16WithBias(half8 * __restrict__ data_in,
                          half8 * __restrict__ data_out,
                          half8 * __restrict__ weights,
                          half8 * __restrict__ bias,
                          s32 no_lines, s32 line_size, half x,
                          void *parameters)
    {
        UNUSED(weights);
        UNUSED(parameters);

        const half8 neg_slope = (half8)x;

        s32 i = 0;
        for(s32 bias_i = 0; bias_i < line_size; ++bias_i)
        {
            half8 current_bias = bias[bias_i];
            for(i = 0; i < (no_lines / UNROLL_SIZE) * (UNROLL_SIZE); i += UNROLL_SIZE)
            {
                data_out[(i + 0) * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 0) * line_size + bias_i] + current_bias, neg_slope * (data_in[(i + 0) * line_size + bias_i] + current_bias));
                data_out[(i + 1) * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 1) * line_size + bias_i] + current_bias, neg_slope * (data_in[(i + 1) * line_size + bias_i] + current_bias));
                data_out[(i + 2) * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 2) * line_size + bias_i] + current_bias, neg_slope * (data_in[(i + 2) * line_size + bias_i] + current_bias));
                data_out[(i + 3) * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 3) * line_size + bias_i] + current_bias, neg_slope * (data_in[(i + 3) * line_size + bias_i] + current_bias));
                data_out[(i + 4) * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 4) * line_size + bias_i] + current_bias, neg_slope * (data_in[(i + 4) * line_size + bias_i] + current_bias));
                data_out[(i + 5) * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 5) * line_size + bias_i] + current_bias, neg_slope * (data_in[(i + 5) * line_size + bias_i] + current_bias));
                data_out[(i + 6) * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 6) * line_size + bias_i] + current_bias, neg_slope * (data_in[(i + 6) * line_size + bias_i] + current_bias));
                data_out[(i + 7) * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 7) * line_size + bias_i] + current_bias, neg_slope * (data_in[(i + 7) * line_size + bias_i] + current_bias));
            }

            for(; i < no_lines; ++i)
                data_out[i * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[i * line_size + bias_i] + current_bias, neg_slope * (data_in[i * line_size + bias_i] + current_bias));
        }
    }

    void reluSatFp16(half8 * __restrict__ data_in,
                          half8 * __restrict__ data_out,
                          half8 * __restrict__ weights,
                          half8 * __restrict__ bias,
                          s32 no_lines, s32 line_size, half x,
                          void *parameters)
    {
        UNUSED(bias);
        UNUSED(weights);
        UNUSED(parameters);

        const half8 zeros   = (half8)0.0;
        const half8 max_val = (half8)x;

        s32 i = 0;
        for(s32 bias_i = 0; bias_i < line_size; ++bias_i)
        {
            half8 current_bias = bias[bias_i];
            for(i = 0; i < (no_lines / UNROLL_SIZE) * (UNROLL_SIZE); i += UNROLL_SIZE)
            {
                data_out[(i + 0) * line_size + bias_i] = __builtin_shave_cmu_min_f16_rr_half8(__builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 0) * line_size + bias_i] + current_bias, zeros), max_val);
                data_out[(i + 1) * line_size + bias_i] = __builtin_shave_cmu_min_f16_rr_half8(__builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 1) * line_size + bias_i] + current_bias, zeros), max_val);
                data_out[(i + 2) * line_size + bias_i] = __builtin_shave_cmu_min_f16_rr_half8(__builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 2) * line_size + bias_i] + current_bias, zeros), max_val);
                data_out[(i + 3) * line_size + bias_i] = __builtin_shave_cmu_min_f16_rr_half8(__builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 3) * line_size + bias_i] + current_bias, zeros), max_val);
                data_out[(i + 4) * line_size + bias_i] = __builtin_shave_cmu_min_f16_rr_half8(__builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 4) * line_size + bias_i] + current_bias, zeros), max_val);
                data_out[(i + 5) * line_size + bias_i] = __builtin_shave_cmu_min_f16_rr_half8(__builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 5) * line_size + bias_i] + current_bias, zeros), max_val);
                data_out[(i + 6) * line_size + bias_i] = __builtin_shave_cmu_min_f16_rr_half8(__builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 6) * line_size + bias_i] + current_bias, zeros), max_val);
                data_out[(i + 7) * line_size + bias_i] = __builtin_shave_cmu_min_f16_rr_half8(__builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 7) * line_size + bias_i] + current_bias, zeros), max_val);
            }

            for(; i < no_lines; ++i)
                data_out[i * line_size + bias_i] = __builtin_shave_cmu_min_f16_rr_half8(__builtin_shave_cmu_max_f16_rr_half8(data_in[i * line_size + bias_i] + current_bias, zeros), max_val);

        }
    }

    void preluFp16(half8 * __restrict__ data_in,
                   half8 * __restrict__ data_out,
                   half8 * __restrict__ weights,
                   half8 * __restrict__ bias,
                   s32 no_lines, s32 line_size, half x,
                   void *parameters)
    {
        UNUSED(bias);
        UNUSED(x);
        UNUSED(parameters);

        const half8 zeros   = (half8)0.0;

        s32 i = 0;
        for(s32 weight_i = 0; weight_i < line_size; ++weight_i)
        {
            half8 current_weights = weights[weight_i];
            for(i = 0; i < ((no_lines / UNROLL_SIZE) * UNROLL_SIZE); i += UNROLL_SIZE)
            {
                data_out[(i + 0) * line_size + weight_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 0) * line_size + weight_i], zeros) + current_weights * __builtin_shave_cmu_min_f16_rr_half8(data_in[(i + 0) * line_size + weight_i], zeros);
                data_out[(i + 1) * line_size + weight_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 1) * line_size + weight_i], zeros) + current_weights * __builtin_shave_cmu_min_f16_rr_half8(data_in[(i + 1) * line_size + weight_i], zeros);
                data_out[(i + 2) * line_size + weight_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 2) * line_size + weight_i], zeros) + current_weights * __builtin_shave_cmu_min_f16_rr_half8(data_in[(i + 2) * line_size + weight_i], zeros);
                data_out[(i + 3) * line_size + weight_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 3) * line_size + weight_i], zeros) + current_weights * __builtin_shave_cmu_min_f16_rr_half8(data_in[(i + 3) * line_size + weight_i], zeros);
                data_out[(i + 4) * line_size + weight_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 4) * line_size + weight_i], zeros) + current_weights * __builtin_shave_cmu_min_f16_rr_half8(data_in[(i + 4) * line_size + weight_i], zeros);
                data_out[(i + 5) * line_size + weight_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 5) * line_size + weight_i], zeros) + current_weights * __builtin_shave_cmu_min_f16_rr_half8(data_in[(i + 5) * line_size + weight_i], zeros);
                data_out[(i + 6) * line_size + weight_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 6) * line_size + weight_i], zeros) + current_weights * __builtin_shave_cmu_min_f16_rr_half8(data_in[(i + 6) * line_size + weight_i], zeros);
                data_out[(i + 7) * line_size + weight_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 7) * line_size + weight_i], zeros) + current_weights * __builtin_shave_cmu_min_f16_rr_half8(data_in[(i + 7) * line_size + weight_i], zeros);
            }

            for(; i < no_lines; ++i)
                data_out[i * line_size + weight_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[i * line_size + weight_i], zeros) + current_weights * __builtin_shave_cmu_min_f16_rr_half8(data_in[i * line_size + weight_i], zeros);
        }

    }

    void biasFp16(half8 * __restrict__ data_in,
                  half8 * __restrict__ data_out,
                  half8 * __restrict__ weights,
                  half8 * __restrict__ bias,
                  s32 no_lines, s32 line_size, half x,
                  void *parameters)
    {
        UNUSED(weights);
        UNUSED(x);
        UNUSED(parameters);

        s32 i = 0;
        for(s32 bias_i = 0; bias_i < line_size; ++bias_i)
        {
            for(i = 0; i < ((no_lines / UNROLL_SIZE) * UNROLL_SIZE); i += UNROLL_SIZE)
            {
                data_out[(i + 0) * line_size + bias_i] = data_in[(i + 0) * line_size + bias_i] + bias[bias_i];
                data_out[(i + 1) * line_size + bias_i] = data_in[(i + 1) * line_size + bias_i] + bias[bias_i];
                data_out[(i + 2) * line_size + bias_i] = data_in[(i + 2) * line_size + bias_i] + bias[bias_i];
                data_out[(i + 3) * line_size + bias_i] = data_in[(i + 3) * line_size + bias_i] + bias[bias_i];
                data_out[(i + 4) * line_size + bias_i] = data_in[(i + 4) * line_size + bias_i] + bias[bias_i];
                data_out[(i + 5) * line_size + bias_i] = data_in[(i + 5) * line_size + bias_i] + bias[bias_i];
                data_out[(i + 6) * line_size + bias_i] = data_in[(i + 6) * line_size + bias_i] + bias[bias_i];
                data_out[(i + 7) * line_size + bias_i] = data_in[(i + 7) * line_size + bias_i] + bias[bias_i];
            }

            for(; i < no_lines; ++i)
                data_out[i * line_size + bias_i] = data_in[i * line_size + bias_i] + bias[bias_i];
        }
    }

    void scaleFp16(half8 * __restrict__ data_in,
                   half8 * __restrict__ data_out,
                   half8 * __restrict__ weights,
                   half8 * __restrict__ bias,
                   s32 no_lines, s32 line_size, half x,
                   void *parameters)
    {
        UNUSED(x);
        UNUSED(bias);
        UNUSED(parameters);

        s32 i = 0;
        for(s32 bias_i = 0; bias_i < line_size; ++bias_i)
        {
            for(i = 0; i < ((no_lines / UNROLL_SIZE) * UNROLL_SIZE); i += UNROLL_SIZE)
            {
                data_out[(i + 0) * line_size + bias_i] = data_in[(i + 0) * line_size + bias_i] * weights[bias_i];
                data_out[(i + 1) * line_size + bias_i] = data_in[(i + 1) * line_size + bias_i] * weights[bias_i];
                data_out[(i + 2) * line_size + bias_i] = data_in[(i + 2) * line_size + bias_i] * weights[bias_i];
                data_out[(i + 3) * line_size + bias_i] = data_in[(i + 3) * line_size + bias_i] * weights[bias_i];
                data_out[(i + 4) * line_size + bias_i] = data_in[(i + 4) * line_size + bias_i] * weights[bias_i];
                data_out[(i + 5) * line_size + bias_i] = data_in[(i + 5) * line_size + bias_i] * weights[bias_i];
                data_out[(i + 6) * line_size + bias_i] = data_in[(i + 6) * line_size + bias_i] * weights[bias_i];
                data_out[(i + 7) * line_size + bias_i] = data_in[(i + 7) * line_size + bias_i] * weights[bias_i];
            }

            for(; i < no_lines; ++i)
                data_out[i * line_size + bias_i] = data_in[i * line_size + bias_i] * weights[bias_i];
        }
    }

    void scaleFp16WithBias(half8 * __restrict__ data_in,
                           half8 * __restrict__ data_out,
                           half8 * __restrict__ weights,
                           half8 * __restrict__ bias,
                           s32 no_lines, s32 line_size, half x,
                           void *parameters)
    {
        UNUSED(x);
        UNUSED(parameters);

        s32 i = 0;
        for(s32 bias_i = 0; bias_i < line_size; ++bias_i)
        {
            for(i = 0; i < ((no_lines / UNROLL_SIZE) * UNROLL_SIZE); i += UNROLL_SIZE)
            {
                data_out[(i + 0) * line_size + bias_i] = data_in[(i + 0) * line_size + bias_i] * weights[bias_i] + bias[bias_i];
                data_out[(i + 1) * line_size + bias_i] = data_in[(i + 1) * line_size + bias_i] * weights[bias_i] + bias[bias_i];
                data_out[(i + 2) * line_size + bias_i] = data_in[(i + 2) * line_size + bias_i] * weights[bias_i] + bias[bias_i];
                data_out[(i + 3) * line_size + bias_i] = data_in[(i + 3) * line_size + bias_i] * weights[bias_i] + bias[bias_i];
                data_out[(i + 4) * line_size + bias_i] = data_in[(i + 4) * line_size + bias_i] * weights[bias_i] + bias[bias_i];
                data_out[(i + 5) * line_size + bias_i] = data_in[(i + 5) * line_size + bias_i] * weights[bias_i] + bias[bias_i];
                data_out[(i + 6) * line_size + bias_i] = data_in[(i + 6) * line_size + bias_i] * weights[bias_i] + bias[bias_i];
                data_out[(i + 7) * line_size + bias_i] = data_in[(i + 7) * line_size + bias_i] * weights[bias_i] + bias[bias_i];
            }

            for(; i < no_lines; ++i)
                data_out[i * line_size + bias_i] = data_in[i * line_size + bias_i] * weights[bias_i] + bias[bias_i];
        }
    }

    void scaleFp16WithScalar(half8 * __restrict__ data_in,
                   half8 * __restrict__ data_out,
                   half8 * __restrict__ weights,
                   half8 * __restrict__ bias,
                   s32 no_lines, s32 line_size, half x,
                   void *parameters)
    {
        UNUSED(x);
        UNUSED(bias);
        UNUSED(parameters);

        half8 scale_value = (half)((*weights)[0]);
        s32 i = 0;
        for(s32 bias_i = 0; bias_i < line_size; ++bias_i)
        {
            for(i = 0; i < ((no_lines / UNROLL_SIZE) * UNROLL_SIZE); i += UNROLL_SIZE)
            {
                data_out[(i + 0) * line_size + bias_i] = data_in[(i + 0) * line_size + bias_i] * scale_value;
                data_out[(i + 1) * line_size + bias_i] = data_in[(i + 1) * line_size + bias_i] * scale_value;
                data_out[(i + 2) * line_size + bias_i] = data_in[(i + 2) * line_size + bias_i] * scale_value;
                data_out[(i + 3) * line_size + bias_i] = data_in[(i + 3) * line_size + bias_i] * scale_value;
                data_out[(i + 4) * line_size + bias_i] = data_in[(i + 4) * line_size + bias_i] * scale_value;
                data_out[(i + 5) * line_size + bias_i] = data_in[(i + 5) * line_size + bias_i] * scale_value;
                data_out[(i + 6) * line_size + bias_i] = data_in[(i + 6) * line_size + bias_i] * scale_value;
                data_out[(i + 7) * line_size + bias_i] = data_in[(i + 7) * line_size + bias_i] * scale_value;
            }
            for(; i < no_lines; ++i)
                data_out[i * line_size + bias_i] = data_in[i * line_size + bias_i] * scale_value;
        }
    }

    void squareFp16(half8 * __restrict__ data_in,
                    half8 * __restrict__ data_out,
                    half8 * __restrict__ weights,
                    half8 * __restrict__ bias,
                    s32 no_lines, s32 line_size, half x,
                    void *parameters)
    {
        UNUSED(x);
        UNUSED(weights);
        UNUSED(bias);
        UNUSED(parameters);

        s32 i = 0;
        for(s32 bias_i = 0; bias_i < line_size; ++bias_i)
        {
            for(i = 0; i < ((no_lines / UNROLL_SIZE) * UNROLL_SIZE); i += UNROLL_SIZE)
            {
                data_out[(i + 0) * line_size + bias_i] = data_in[(i + 0) * line_size + bias_i] * data_in[(i + 0) * line_size + bias_i];
                data_out[(i + 1) * line_size + bias_i] = data_in[(i + 1) * line_size + bias_i] * data_in[(i + 1) * line_size + bias_i];
                data_out[(i + 2) * line_size + bias_i] = data_in[(i + 2) * line_size + bias_i] * data_in[(i + 2) * line_size + bias_i];
                data_out[(i + 3) * line_size + bias_i] = data_in[(i + 3) * line_size + bias_i] * data_in[(i + 3) * line_size + bias_i];
                data_out[(i + 4) * line_size + bias_i] = data_in[(i + 4) * line_size + bias_i] * data_in[(i + 4) * line_size + bias_i];
                data_out[(i + 5) * line_size + bias_i] = data_in[(i + 5) * line_size + bias_i] * data_in[(i + 5) * line_size + bias_i];
                data_out[(i + 6) * line_size + bias_i] = data_in[(i + 6) * line_size + bias_i] * data_in[(i + 6) * line_size + bias_i];
                data_out[(i + 7) * line_size + bias_i] = data_in[(i + 7) * line_size + bias_i] * data_in[(i + 7) * line_size + bias_i];
            }

            for(; i < no_lines; ++i)
                data_out[i * line_size + bias_i] = data_in[i * line_size + bias_i] * data_in[i * line_size + bias_i];
        }
    }

    void innerLRNFp16(half8 * __restrict__ data_in, half8 * __restrict__ data_out, s32 no_lines, s32 line_size, half alpha, half beta)
    {
        s32 i, j;
        for(s32 bias_i = 0; bias_i < line_size; ++bias_i)
        {
            for(i = 0; i < ((no_lines / UNROLL_SIZE) * UNROLL_SIZE); i += UNROLL_SIZE)
            {
                data_out[(i + 0) * line_size + bias_i] = 1.0 + alpha * data_in[(i + 0) * line_size + bias_i];
                data_out[(i + 1) * line_size + bias_i] = 1.0 + alpha * data_in[(i + 1) * line_size + bias_i];
                data_out[(i + 2) * line_size + bias_i] = 1.0 + alpha * data_in[(i + 2) * line_size + bias_i];
                data_out[(i + 3) * line_size + bias_i] = 1.0 + alpha * data_in[(i + 3) * line_size + bias_i];
                data_out[(i + 4) * line_size + bias_i] = 1.0 + alpha * data_in[(i + 4) * line_size + bias_i];
                data_out[(i + 5) * line_size + bias_i] = 1.0 + alpha * data_in[(i + 5) * line_size + bias_i];
                data_out[(i + 6) * line_size + bias_i] = 1.0 + alpha * data_in[(i + 6) * line_size + bias_i];
                data_out[(i + 7) * line_size + bias_i] = 1.0 + alpha * data_in[(i + 7) * line_size + bias_i];
            }

            for(; i < no_lines; ++i)
                data_out[i * line_size + bias_i] = 1.0 + alpha * data_in[(i + 0) * line_size + bias_i];

            for(i = 0; i < no_lines; ++i)
            {
                half *p = (half *)&data_out[i * line_size + bias_i];
                for(j = 0; j < 8; j++)
                    p[j] = pows(p[j], beta);
            }
        }
    }

    void sigmoid_accurate(half8 * __restrict__ data_in,
                          half8 * __restrict__ data_out,
                          half8 * __restrict__ weights,
                          half8 * __restrict__ bias,
                          s32 no_lines, s32 line_size, half x,
                          void *parameters)
    {
        UNUSED(weights);
        UNUSED(bias);
        UNUSED(x);
        UNUSED(parameters);

        // Compute sigmoid(x) = 1 / (1 + exp(-x))
        for(s32 i = 0; i < (no_lines * line_size); ++i)
        {
            data_out[i][0] = exp((double)-data_in[i][0]);
            data_out[i][1] = exp((double)-data_in[i][1]);
            data_out[i][2] = exp((double)-data_in[i][2]);
            data_out[i][3] = exp((double)-data_in[i][3]);
            data_out[i][4] = exp((double)-data_in[i][4]);
            data_out[i][5] = exp((double)-data_in[i][5]);
            data_out[i][6] = exp((double)-data_in[i][6]);
            data_out[i][7] = exp((double)-data_in[i][7]);

            data_out[i] = (half8)1.0 / ((half8)1.0 + data_out[i]);
        }
    }

    void sigmoid_fast(half8 * __restrict__ data_in,
                      half8 * __restrict__ data_out,
                      half8 * __restrict__ weights,
                      half8 * __restrict__ bias,
                      s32 no_lines, s32 line_size, half x,
                      void *parameters)
    {
        UNUSED(weights);
        UNUSED(bias);
        UNUSED(x);
        UNUSED(parameters);

        // Compute sigmoid(x) = 1 / (1 + exp(-x)) = 1 / (1 + 2^(-x/ln(2)))
        const unsigned short negative_inv_ln2 = 0xbdc6;
        const half negative_inv_ln2_h = *reinterpret_cast<const half *>(&negative_inv_ln2);
        for(s32 i = 0; i < (no_lines * line_size); ++i)
        {
            data_in[i] = data_in[i] * (half8)negative_inv_ln2_h;

            data_out[i][0] = __builtin_shave_sau_exp2_f16_l_r(data_in[i][0]);
            data_out[i][1] = __builtin_shave_sau_exp2_f16_l_r(data_in[i][1]);
            data_out[i][2] = __builtin_shave_sau_exp2_f16_l_r(data_in[i][2]);
            data_out[i][3] = __builtin_shave_sau_exp2_f16_l_r(data_in[i][3]);
            data_out[i][4] = __builtin_shave_sau_exp2_f16_l_r(data_in[i][4]);
            data_out[i][5] = __builtin_shave_sau_exp2_f16_l_r(data_in[i][5]);
            data_out[i][6] = __builtin_shave_sau_exp2_f16_l_r(data_in[i][6]);
            data_out[i][7] = __builtin_shave_sau_exp2_f16_l_r(data_in[i][7]);

            data_out[i] = (half8)1.0 / ((half8)1.0 + data_out[i]);
        }
    }

    void tanh_accurate(half8 * __restrict__ data_in,
                       half8 * __restrict__ data_out,
                       half8 * __restrict__ weights,
                       half8 * __restrict__ bias,
                       s32 no_lines, s32 line_size, half x,
                       void *parameters)
    {
        UNUSED(weights);
        UNUSED(bias);
        UNUSED(x);
        UNUSED(parameters);

        // Compute tanh(x)
        for(s32 i = 0; i < (no_lines * line_size); ++i)
        {
            data_out[i][0] = tanh((double)data_in[i][0]);
            data_out[i][1] = tanh((double)data_in[i][1]);
            data_out[i][2] = tanh((double)data_in[i][2]);
            data_out[i][3] = tanh((double)data_in[i][3]);
            data_out[i][4] = tanh((double)data_in[i][4]);
            data_out[i][5] = tanh((double)data_in[i][5]);
            data_out[i][6] = tanh((double)data_in[i][6]);
            data_out[i][7] = tanh((double)data_in[i][7]);
        }
    }

    void tanh_fast(half8 * __restrict__ data_in,
                   half8 * __restrict__ data_out,
                   half8 * __restrict__ weights,
                   half8 * __restrict__ bias,
                   s32 no_lines, s32 line_size, half x,
                   void *parameters)
    {
        UNUSED(weights);
        UNUSED(bias);
        UNUSED(x);
        UNUSED(parameters);
        // Clamp the input to avoid fp16 precision overflow when computing exp.
        // This should not affect the results
        half8 minus_five = -10.5f;
        half8 plus_five  = 5.5f;

        // Compute tanh(x) = (exp(2x) - 1) / (exp(2x) + 1)
        // = (2^(2x/ln(2)) - 1) / (2^(2x/ln(2)) + 1)
        const unsigned short inv_ln2_mul_2 = 0x41c5;
        const half inv_ln2_mul_2_h = *reinterpret_cast<const half *>(&inv_ln2_mul_2);
        for(s32 i = 0; i < (no_lines * line_size); ++i)
        {
            data_in[i] = __builtin_shave_cmu_clampab_f16_rrr_half8(data_in[i], minus_five, plus_five);
            data_in[i] = data_in[i] * (half8)inv_ln2_mul_2_h;

            data_out[i][0] = __builtin_shave_sau_exp2_f16_l_r(data_in[i][0]);
            data_out[i][1] = __builtin_shave_sau_exp2_f16_l_r(data_in[i][1]);
            data_out[i][2] = __builtin_shave_sau_exp2_f16_l_r(data_in[i][2]);
            data_out[i][3] = __builtin_shave_sau_exp2_f16_l_r(data_in[i][3]);
            data_out[i][4] = __builtin_shave_sau_exp2_f16_l_r(data_in[i][4]);
            data_out[i][5] = __builtin_shave_sau_exp2_f16_l_r(data_in[i][5]);
            data_out[i][6] = __builtin_shave_sau_exp2_f16_l_r(data_in[i][6]);
            data_out[i][7] = __builtin_shave_sau_exp2_f16_l_r(data_in[i][7]);

            data_out[i] = (data_out[i] - (half8)1.0f) / (data_out[i] + (half8)1.0);
        }
    }
    void eluFp16(half8 * __restrict__ data_in,
                 half8 * __restrict__ data_out,
                 half8 * __restrict__ weights,
                 half8 * __restrict__ bias,
                 s32 no_lines, s32 line_size, half x,
                 void *parameters)
    {
        UNUSED(weights);
        UNUSED(bias);
        UNUSED(parameters);

        // Compute elu(x) = x for                    x >  0
        //                = alpha * (exp(x) - 1) for x <= 0
        // using exp(x) = 2^(x/ln(2))

        const half alpha = x;
        const half8 one  = (half8)1.0f;
        const half8 zero = (half8)0.0f;

        const unsigned short inv_ln2 = 0x3dc6;
        const half inv_ln2_h = *reinterpret_cast<const half *>(&inv_ln2);

        for(s32 i = 0; i < (no_lines * line_size); ++i)
        {
            half8 temp_in = data_in[i] * (half8)inv_ln2_h;
            temp_in = __builtin_shave_cmu_min_f16_rr_half8(temp_in, zero);
            half8 exp_x;

            exp_x[0] = __builtin_shave_sau_exp2_f16_l_r(temp_in[0]);
            exp_x[1] = __builtin_shave_sau_exp2_f16_l_r(temp_in[1]);
            exp_x[2] = __builtin_shave_sau_exp2_f16_l_r(temp_in[2]);
            exp_x[3] = __builtin_shave_sau_exp2_f16_l_r(temp_in[3]);
            exp_x[4] = __builtin_shave_sau_exp2_f16_l_r(temp_in[4]);
            exp_x[5] = __builtin_shave_sau_exp2_f16_l_r(temp_in[5]);
            exp_x[6] = __builtin_shave_sau_exp2_f16_l_r(temp_in[6]);
            exp_x[7] = __builtin_shave_sau_exp2_f16_l_r(temp_in[7]);

            data_out[i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[i], zero) +
                    alpha * (exp_x - one);
        }
    }

    void powerFp16_accurate(half8 * __restrict__ data_in,
            half8 * __restrict__ data_out,
            half8 * __restrict__ weights,
            half8 * __restrict__ bias,
            s32 no_lines,
            s32 line_size,
            half x,
            void *parameters)
    {
        UNUSED(weights);
        UNUSED(bias);
        UNUSED(x);

        // Compute power(x) = (shift + scale * x)^power
        const half8 shift = (half8)reinterpret_cast<t_PowerLayerParams *>(parameters)->shift;
        const half8 scale = (half8)reinterpret_cast<t_PowerLayerParams *>(parameters)->scale;
        const float power = reinterpret_cast<t_PowerLayerParams *>(parameters)->power;

        for(s32 i = 0; i < (no_lines * line_size); ++i)
        {
            half8 base = shift + scale * data_in[i];

            data_out[i][0] = powf((float)base[0], (float)power);
            data_out[i][1] = powf((float)base[1], (float)power);
            data_out[i][2] = powf((float)base[2], (float)power);
            data_out[i][3] = powf((float)base[3], (float)power);
            data_out[i][4] = powf((float)base[4], (float)power);
            data_out[i][5] = powf((float)base[5], (float)power);
            data_out[i][6] = powf((float)base[6], (float)power);
            data_out[i][7] = powf((float)base[7], (float)power);
        }
    }

    void powerFp16_fast(half8 * __restrict__ data_in,
            half8 * __restrict__ data_out,
            half8 * __restrict__ weights,
            half8 * __restrict__ bias,
            s32 no_lines,
            s32 line_size,
            half x,
            void *parameters)
    {

        UNUSED(weights);
        UNUSED(bias);
        UNUSED(x);

        const half8 shift = (half8)reinterpret_cast<t_PowerLayerParams *>(parameters)->shift;
        const float shift_scal = reinterpret_cast<t_PowerLayerParams *>(parameters)->shift;

        const half8 scale = (half8)reinterpret_cast<t_PowerLayerParams *>(parameters)->scale;
        const float scale_scal = reinterpret_cast<t_PowerLayerParams *>(parameters)->scale;

        const half8 power = (half8)reinterpret_cast<t_PowerLayerParams *>(parameters)->power;
        const float power_scal = reinterpret_cast<t_PowerLayerParams *>(parameters)->power;

    // Compute power(x) = (shift + scale * x)^power
        int num_elements = no_lines * line_size;

        if ((scale_scal == 1.0f) && (power_scal == 1.0f) && (shift_scal == 0.0f)) // out = in (copying)
        {
            // Do not do anything; src is already copied to dst CMX tile
            return;
        }

        if(power_scal == 0.0f) // power == 0
        {
            half8 vfill = (half8)1.0f;
            #pragma clang loop unroll_count(8)
            for(s32 i = 0; i < (num_elements); ++i)
            {
                data_out[i] = vfill;
            }
        }
        else
        {
            bool is_power_integer = floorf(fabs(power_scal)) == fabs(power_scal);
            const s32 integer_power = fabs(power_scal);

            if (is_power_integer) // power is integer
            {
                if (integer_power == 1) // power == 1
                {
                    if ((scale_scal == -1.0f) && (shift_scal == 0.0f)) // out = -in
                    {
                        half8 vec;
                        if (num_elements)
                        {
                            vec = -data_in[0];
                        }
                        #pragma clang loop unroll_count(4)
                        for (int i = 1; i < (num_elements); i++)
                        {
                            data_out[i-1] = vec;
                            vec = -data_in[i];
                        }
                        if (num_elements)
                        {
                            data_out[num_elements-1] = vec;
                        }
                    }
                    else
                    {
                        half8 vres;
                        half8 v;

                        if (num_elements)
                        {
                            v = scale * data_in[0];
                        }
                        #pragma clang loop unroll_count(8)
                        for (s32 i = 1; i < num_elements; i++)
                        {
                            half8 vres = v + shift;
                            v = scale * data_in[i];
                            data_out[i-1] = vres;
                        }
                        if (num_elements)
                        {
                            data_out[num_elements-1] = v + shift;
                        }
                    }

                }
                else if (integer_power == 2) // power == 2
                {
                    half8 base;

                    if (num_elements)
                    {
                        base = shift + scale * data_in[0];
                    }
                    #pragma clang loop unroll_count(8)
                    for (s32 i = 1; i < (num_elements); i++)
                    {
                        half8 vres = (base) * (base);
                        base = shift + scale * data_in[i];
                        data_out[i-1] = vres;
                    }
                    if (num_elements)
                    {
                        data_out[num_elements-1] = (base)*(base);
                    }
                }
                else if (integer_power == 3) // power == 3
                {
                    half8 vin;
                    half8 base;
                    half8 vres;

                    // 0 iteration
                    if (num_elements)
                    {
                        half8 vin = data_in[0];
                        base = shift + scale * vin;
                        vres = base * base * base;
                    }
                    // 1 iteration
                    if (num_elements > 1)
                    {
                        vin = data_in[1];
                        base = shift + scale * vin;
                    }
                    #pragma clang loop unroll_count(8)
                    for (s32 i = 2; i < (num_elements); i++)
                    {
                        data_out[i-2] = vres;
                        vres = base * base * base;
                        half8 vin = data_in[i];
                        base = shift + scale * vin;
                    }
                    // (num_elements-2) iteration
                    if (num_elements > 1)
                    {
                        data_out[num_elements-2] = vres;
                    }
                    // (num_elements-1) iteration
                    if (num_elements > 0)
                    {
                        data_out[num_elements-1] = (base)*(base)*(base);
                    }
                }
                else // general integer power
                {
                    s32 i = 0;
                    #pragma clang loop unroll_count(1)
                    for (; i < num_elements-7; i+=8)
                    {
                        half8 base0 = shift + scale * data_in[i];
                        half8 base1 = shift + scale * data_in[i+1];
                        half8 base2 = shift + scale * data_in[i+2];
                        half8 base3 = shift + scale * data_in[i+3];
                        half8 base4 = shift + scale * data_in[i+4];
                        half8 base5 = shift + scale * data_in[i+5];
                        half8 base6 = shift + scale * data_in[i+6];
                        half8 base7 = shift + scale * data_in[i+7];

                        half8 res0 = base0;
                        half8 res1 = base1;
                        half8 res2 = base2;
                        half8 res3 = base3;
                        half8 res4 = base4;
                        half8 res5 = base5;
                        half8 res6 = base6;
                        half8 res7 = base7;

                        int p = 1;
                        for (; (p << 1) <= integer_power; p<<=1)
                        {
                            res0 = res0 * res0;
                            res1 = res1 * res1;
                            res2 = res2 * res2;
                            res3 = res3 * res3;
                            res4 = res4 * res4;
                            res5 = res5 * res5;
                            res6 = res6 * res6;
                            res7 = res7 * res7;
                        }

                        for (; p < integer_power; p++)
                        {
                            res0 = res0 * base0;
                            res1 = res1 * base1;
                            res2 = res2 * base2;
                            res3 = res3 * base3;
                            res4 = res4 * base4;
                            res5 = res5 * base5;
                            res6 = res6 * base6;
                            res7 = res7 * base7;
                        }
                        data_out[i+0] = res0;
                        data_out[i+1] = res1;
                        data_out[i+2] = res2;
                        data_out[i+3] = res3;
                        data_out[i+4] = res4;
                        data_out[i+5] = res5;
                        data_out[i+6] = res6;
                        data_out[i+7] = res7;
                    }

                    for (; i < num_elements; i++)
                    {
                        half8 base = shift + scale * data_in[i];

                        half8 res = base;
                        for (int p = 0; p < integer_power-1; p++)
                        {
                            res = res * base;
                        }
                        data_out[i] = res;
                    }
                }

                if (power_scal < 0.0f)
                {
                    half8 v;
                    if (num_elements)
                    {
                        v = data_in[0];
                    }
                    #pragma clang loop unroll_count(8)
                    for (s32 i = 1; i < (num_elements); i++)
                    {
                        data_out[i-1] = 1.f / v;
                        v = data_in[i];
                    }
                    if (num_elements)
                    {
                        data_out[num_elements-1] = 1.f / v;
                    }
                }
            }
            else // general case
            {

    #define intrinsic_vec(intrinsic, vin, vout) \
        vout[0] = intrinsic(vin[0]); \
        vout[1] = intrinsic(vin[1]); \
        vout[2] = intrinsic(vin[2]); \
        vout[3] = intrinsic(vin[3]); \
        vout[4] = intrinsic(vin[4]); \
        vout[5] = intrinsic(vin[5]); \
        vout[6] = intrinsic(vin[6]); \
        vout[7] = intrinsic(vin[7]);

    #define log2_vec(vin, vout) intrinsic_vec(__builtin_shave_sau_log2_f16_l_r, vin, vout)
    #define exp2_vec(vin, vout) intrinsic_vec(__builtin_shave_sau_exp2_f16_l_r, vin, vout)

                half8 base_log, base_mult, base;

                // 0 iteration
                if (num_elements > 0)
                {
                    base = shift + scale * data_in[0];
                    log2_vec(base, base_log);
                }
                // 1 iteration
                if (num_elements > 1)
                {
                    base = shift + scale * data_in[1];
                }

                #pragma clang loop unroll_count(8)
                for (s32 i = 2; i < (num_elements); ++i)
                {
                    base_mult = base_log * power;// 2 stage
                    exp2_vec(base_mult, data_out[i-2]);

                    log2_vec(base, base_log);// 1 stage

                    base = shift + scale * data_in[i];// 0 stage
                }

                // (num_elements-2) iteration
                if (num_elements > 1)
                {
                    base_mult = base_log * power;
                    exp2_vec(base_mult, data_out[num_elements-2]);
                }
                // (num_elements-1) iteration
                if (num_elements > 0)
                {
                    log2_vec(base, base_log);
                    base_mult = base_log * power;
                    exp2_vec(base_mult, data_out[num_elements-1]);
                }
            }
    #undef intrinsic_vec
    #undef log2_vec
    #undef exp2_vec

        }

    }
}

void maxWithConstant(half8 * __restrict__ data_in,
                       half8 * __restrict__ data_out,
                       half8 * __restrict__ weights,
                       half8 * __restrict__ bias,
                       s32 no_lines, s32 line_size, half x,
                       void *parameters)
{
    UNUSED(bias);
    UNUSED(parameters);
    UNUSED(x);

    const half8 max_val = (half8)weights[0][0];

    s32 i = 0;
    for(s32 bias_i = 0; bias_i < line_size; ++bias_i)
    {
        for(i = 0; i < (no_lines / UNROLL_SIZE) * (UNROLL_SIZE); i += UNROLL_SIZE)
        {
            data_out[(i + 0) * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 0) * line_size + bias_i], max_val);
            data_out[(i + 1) * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 1) * line_size + bias_i], max_val);
            data_out[(i + 2) * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 2) * line_size + bias_i], max_val);
            data_out[(i + 3) * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 3) * line_size + bias_i], max_val);
            data_out[(i + 4) * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 4) * line_size + bias_i], max_val);
            data_out[(i + 5) * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 5) * line_size + bias_i], max_val);
            data_out[(i + 6) * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 6) * line_size + bias_i], max_val);
            data_out[(i + 7) * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[(i + 7) * line_size + bias_i], max_val);
        }

        for(; i < no_lines; ++i)
            data_out[i * line_size + bias_i] = __builtin_shave_cmu_max_f16_rr_half8(data_in[i * line_size + bias_i], max_val);

    }
}

void rsqrtFp16(half8 * __restrict__ data_in,
                half8 * __restrict__ data_out,
                half8 * __restrict__ weights,
                half8 * __restrict__ bias,
                s32 no_lines, s32 line_size, half x,
                void *parameters)
{
    UNUSED(x);
    UNUSED(weights);
    UNUSED(bias);
    UNUSED(parameters);

    half beta = -0.5;
    s32 i = 0;
    u32 j = 0;
    for(s32 bias_i = 0; bias_i < line_size; ++bias_i)
    {
        for(i = 0; i < no_lines; ++i)
        {
        	half *src = (half*)&data_in[i * line_size + bias_i];
        	half *dst = (half*)&data_out[i * line_size + bias_i];
            for(j = 0; j < 8; j++)
            {
            	dst[j] = pows(src[j],beta);
            }
        }
    }
}

void postOps_core(t_PostOpsParams *params)
{
    using namespace mv::tensor;
    dma::Config dmaConfig = { 1, params->dmaLinkAgent };
    dma::User dmaUser(dmaConfig);

    half *input        = params->input;
    half *output       = params->output;
    half *weights      = params->weights;
    half *bias         = params->bias;
    u32  width         = params->width;
    u32  height        = params->height;
    u32  stride        = params->stride;
    half alpha, beta;
    bool innerlrn = false;

    void (* postOp)(half8 *, half8 *, half8 *, half8 *, s32, s32, half, void *);
    const s32 g_useDMA = USE_DMA;

    switch(params->postOpType)
    {
    case RELU:
        if(bias == 0)
            postOp = &reluFp16;
        else
            postOp = &reluFp16WithBias;
        break;
    case RELU_NEG_SLOPE:
        if(bias == 0)
            postOp = &reluNegSlopeFp16;
        else
            postOp = &reluNegSlopeFp16WithBias;
        break;
    case RELU_SAT:
        postOp = &reluSatFp16;
        break;
    case PRELU:
        postOp = &preluFp16;
        break;
    case BIAS:
        postOp = &biasFp16;
        break;
    case SCALE:
    	if(bias == 0)
            postOp = &scaleFp16;
        else
            postOp = &scaleFp16WithBias;
        break;
    case SQUARE:
        postOp = squareFp16;
        break;
    case INNERLRN:
        alpha = *weights;
        beta = -*bias;
        weights = 0;
        bias = 0;
        innerlrn = true;
        break;
    case SIGMOID_ACCURATE:
        postOp = &sigmoid_accurate;
        break;
    case SIGMOID_FAST:
        postOp = &sigmoid_fast;
        break;
    case TANH_ACCURATE:
        postOp = &tanh_accurate;
        break;
    case TANH_FAST:
        postOp = &tanh_fast;
        break;
    case ELU:
        postOp = &eluFp16;
        break;
    case POWER_ACCURATE:
        postOp = &powerFp16_accurate;
        break;
    case POWER_FAST:
        postOp = &powerFp16_fast;
        break;
    case MAX_WITH_CONSTANT:
    	postOp = maxWithConstant;
    	break;
    case RSQRT:
        postOp = rsqrtFp16;
        break;
    case SCALE_WITH_SCALAR:
    	postOp = scaleFp16WithScalar;
        break;
    }

    s32 cmx_bl8h    = (CMX_SIZE >> 3) / sizeof(half); // Size of cmx in blocks of 8 halfs.
    s32 w_bl8h      = width >> 3; // Size of width in blocks of 8 halfs.
    s32 w_remainder = width % 8;  // Number of halfs in the last block of 8 halfs.

    // "Padd" width to a round number of blocks of 8 halfs.
    if(w_remainder)
        ++w_bl8h; // There is an incomplete block of 8 halfs.

    // Split the biases in equal sized chunks of blocks of 8 half.
    // We have to be able to fit at least 2 chunks in cmx when we have bias and
    // 1 chunk when we don't have bias. Best case scenario chunk size in blocks of
    // 8 half = width size in blocks of 8 half (w_bl8h).
    s32 no_chunks     = 0; // This will be 1 in most cases.
    s32 no_cmx_chunks = 0; // Number of chunks that can fit in the CMX.

    no_chunks = ((!!bias + !!weights + 1) * w_bl8h + cmx_bl8h - 1) / cmx_bl8h;
    no_cmx_chunks = cmx_bl8h * no_chunks / w_bl8h;

    if(bias != 0)
        --no_cmx_chunks; // One cmx chunk reserved for bias;
    if(weights != 0)
        --no_cmx_chunks; // One cmx chunk reserved for weight;

    s32 chunk_size_bl8h = w_bl8h / no_chunks;
    s32 chunk_remainder = w_bl8h % no_chunks;
    if(chunk_remainder)
        ++no_chunks; // There is an incomplete chunk.

    half8 *v_bias = (half8 *)(params->cmxslice);
    half8 *v_weights = (half8 *)(params->cmxslice);
    half8 *v_in_out = (half8 *)(params->cmxslice);
    if(bias != 0 && weights != 0)
    {
        // Make space for weights and bias
        v_bias = (half8 *)(params->cmxslice + (chunk_size_bl8h << 3) * sizeof(half));
        v_in_out = (half8 *)(params->cmxslice + 2 * (chunk_size_bl8h << 3) * sizeof(half));
    }
    else
        if(weights != 0 || bias != 0) // Make space for one of the two
            v_in_out = (half8 *)(params->cmxslice + (chunk_size_bl8h << 3) * sizeof(half));

    s32 transfer_in_width = (chunk_size_bl8h << 3) * sizeof(half); // Transfer width in bytes.
    s32 transfer_out_width = transfer_in_width; // Transfer width in bytes.
    s32 last_chunk_transfer_out_width = transfer_out_width;

    // Make sure we transfer out the proper amount.
    if(chunk_remainder)
        last_chunk_transfer_out_width = (((chunk_remainder -1) << 3) + w_remainder) * sizeof(half);
    else
        if(w_remainder)
            last_chunk_transfer_out_width = (((chunk_size_bl8h - 1) << 3) + w_remainder) * sizeof(half);

    s32 no_transfers_per_chunk = height / no_cmx_chunks;
    s32 no_transfers_remainder = height % no_cmx_chunks;

    if(no_transfers_remainder)
        ++no_transfers_per_chunk;

    s32 in_out_offset = stride;
    if(no_chunks > 1)
        // If no_chunks > 1. Than chunk_size < width < stride, Hence the offset in the output buffer should be
        // chunk size. Otherwise the offset should be stride.
        in_out_offset = (chunk_size_bl8h << 3);

    for(s32 chunk_i = 0; chunk_i < no_chunks; ++chunk_i)
    {
        // Transfer chunk_i of bias.
        if(weights != 0)
        {
            if(g_useDMA && ((chunk_size_bl8h << 3) * sizeof(half)) > MIN_DMA_TRANSFER_SIZE)
            {
                dma::Task(dmaUser).start(
                        (u8 *)(weights + chunk_i * (chunk_size_bl8h << 3)),
                        (u8 *)v_weights,
                        (chunk_size_bl8h << 3) * sizeof(half));
            }
            else
            {
                memcpy((void *)(v_weights), (void *)((u8 *)(weights + chunk_i * (chunk_size_bl8h << 3))),
                        (size_t)((chunk_size_bl8h << 3) * sizeof(half)));
            }
        }

        // Transfer chunk_i of bias.
        if(bias != 0)
        {
            if(g_useDMA && ((chunk_size_bl8h << 3) * sizeof(half)) > MIN_DMA_TRANSFER_SIZE)
            {
                dma::Task(dmaUser).start(
                        (u8 *)(bias + chunk_i * (chunk_size_bl8h << 3)),
                        (u8 *)v_bias,
                        (chunk_size_bl8h << 3) * sizeof(half));
            }
            else
            {
                memcpy((void *)(v_bias), (void *)((u8 *)(bias + chunk_i * (chunk_size_bl8h << 3))),
                        (size_t)((chunk_size_bl8h << 3) * sizeof(half)));
            }
        }

        if(chunk_i == (no_chunks -1))
            transfer_out_width = last_chunk_transfer_out_width;

        s32 no_chunks_to_process = no_cmx_chunks;

        for(s32 transfer_i = 0; transfer_i < no_transfers_per_chunk; ++transfer_i)
        {
            if(transfer_i == (no_transfers_per_chunk - 1))
            {
                if(no_transfers_remainder)
                {
                    no_chunks_to_process = no_transfers_remainder;
                }
            }

            s32 transfer_in_size = transfer_in_width * no_chunks_to_process;
            s32 transfer_out_size = transfer_out_width * no_chunks_to_process;

            // Transfer no_cmx_chunks corresponding to bias chunk_i.
            if(g_useDMA && transfer_out_size > MIN_DMA_TRANSFER_SIZE)
            {
                dma::Task(dmaUser).start(
                        (u8 *)(input + (chunk_i * in_out_offset + in_out_offset * no_cmx_chunks * transfer_i)),
                        (u8 *)v_in_out,
                        transfer_in_size,
                        transfer_in_width,
                        transfer_in_size,
                        stride * sizeof(half),
                        transfer_in_size);
            }
            else
            {
                for(s32 i = 0; i < no_chunks_to_process; ++i)
                    memcpy(
                            (void *)(&v_in_out[i * chunk_size_bl8h]),
                            (void *)((u8 *)(input + (chunk_i + no_cmx_chunks * transfer_i + i) * in_out_offset)),
                            (size_t)transfer_in_width);
            }

            if(innerlrn)
                innerLRNFp16(v_in_out, v_in_out, no_chunks_to_process, chunk_size_bl8h, alpha, beta);
            else
                postOp(v_in_out, v_in_out, v_weights, v_bias, no_chunks_to_process, chunk_size_bl8h, (half)params->x, params->params);

            if(g_useDMA && transfer_out_size > MIN_DMA_TRANSFER_SIZE)
            {
                dma::Task(dmaUser).start(
                        (u8 *)(v_in_out),
                        (u8 *)(output + (chunk_i * in_out_offset + in_out_offset * no_cmx_chunks * transfer_i)),
                        transfer_out_size,
                        transfer_out_width,
                        transfer_out_width,
                        (chunk_size_bl8h << 3) * sizeof(half),
                        stride * sizeof(half));
            }
            else
            {
                for(s32 i = 0; i < no_chunks_to_process; ++i)
                    memcpy(
                            (void *)((u8 *)(output + (chunk_i + no_cmx_chunks * transfer_i + i) * in_out_offset)),
                            (void *)(&v_in_out[i * chunk_size_bl8h]),
                            (size_t)transfer_out_width);
            }
        }
    }
}
