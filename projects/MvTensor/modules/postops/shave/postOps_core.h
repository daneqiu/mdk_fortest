///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef _MV_POSTOPS_CORE_H_
#define _MV_POSTOPS_CORE_H_

#include <mvPostOpsParam.h>

#ifdef __cplusplus
extern "C"
{
#endif

void postOps_core(t_PostOpsParams *params);

#ifdef __cplusplus
}
#endif

#endif /* _MV_PRELUFP16_CORE_H_ */
