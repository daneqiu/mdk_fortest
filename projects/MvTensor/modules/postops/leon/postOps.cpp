///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief Leon wrappers for CNN post operations.
///

#include <mvPostOpsParam.h>
#include <mvModuleHandle.h>
#include <mvTensorInternal.h>

#if DEBUG
#define MVT_DPRINTF(...) printf(__VA_ARGS__)
#else
#define MVT_DPRINTF(...)
#endif

MODULE_ENTRY_DECL(postOps_core);

namespace
{
    using namespace mv::tensor;

    class PostOps : public Layer
    {
        static void postOps(fp16 *input, fp16 *output, fp16 *weights, fp16 *bias, u32 width,
                u32 height, u32 stride, float x, void *params,
                t_MvTensorMyriadResources *myriadResources, PostOpType postOpType);

        virtual void run_(const t_MvTensorParam *mvTensorParam, const t_MvTensorOp &op, const Optimization &optimization, const Resources &res);
    };

    void PostOps::postOps(fp16 *input, fp16 *output, fp16 *weights, fp16 *bias, u32 width,
            u32 height, u32 stride, float x, void *params,
            t_MvTensorMyriadResources *myriadResources, PostOpType postOpType)
    {
        int shave_i;
        u32 no_shaves = myriadResources->lastShave - myriadResources->firstShave + 1;
        u32 h_per_shave = height / no_shaves;
        u32 h_per_shave_remainder = height % no_shaves;
        s32 in_out_offset = 0;

        // Nothing to process.
        if(width == 0)
            return;
        for(shave_i = myriadResources->firstShave; shave_i <= myriadResources->lastShave; ++shave_i)
        {
            t_PostOpsParams *postOpsParams = useShaveParam<t_PostOpsParams>(shave_i);
            postOpsParams->input        = (input  + in_out_offset);
            postOpsParams->output       = (output + in_out_offset);
            postOpsParams->weights      = weights;
            postOpsParams->bias         = bias;
            postOpsParams->width        = width;
            postOpsParams->height       = h_per_shave;
            postOpsParams->stride       = stride;
            postOpsParams->cmxslice     = getCMXSliceDataSection(shave_i);
            postOpsParams->dmaLinkAgent = myriadResources->dmaLinkAgent;
            postOpsParams->postOpType   = postOpType;
            postOpsParams->x            = x;
            postOpsParams->params       = params;

            // Distribute one line of width to the first h_per_shave_remainder shave.
            in_out_offset += h_per_shave * stride;

            if(h_per_shave_remainder != 0)
            {
                postOpsParams->height += 1;
                in_out_offset += stride;
                --h_per_shave_remainder;
            }

            u32 readBackAddr = (u32)(&(postOpsParams->x));
            GET_REG_WORD_VAL(readBackAddr);

            startShave(shave_i, (u32)&MODULE_ENTRY(postOps_core), (u32)postOpsParams);
        }

        waitShaves(myriadResources->firstShave, myriadResources->lastShave);
    }

    void PostOps::run_(const t_MvTensorParam *mvTensorParam, const t_MvTensorOp &op, const Optimization &optimization, const Resources &res)
    {
        switch (op.type)
        {
            case kBias:
            {
                postOps(
                    (fp16 *)mvTensorParam->output->data,
                    (fp16 *)mvTensorParam->output->data,
                    nullptr,
                    (fp16 *)mvTensorParam->biases->data,
                    mvTensorParam->output->dimZ,
                    mvTensorParam->output->dimX * mvTensorParam->output->dimY,
                    mvTensorParam->output->dimXStride/sizeof(half),
                    0.0f,
                    nullptr,
                    mvTensorParam->myriadResources,
                    BIAS
                );

                break;
            }

            case kElu:
            {
                postOps(
                    (fp16 *)mvTensorParam->output->data,
                    (fp16 *)mvTensorParam->output->data,
                    nullptr,
                    nullptr,
                    mvTensorParam->output->dimZ,
                    mvTensorParam->output->dimX * mvTensorParam->output->dimY,
                    mvTensorParam->output->dimXStride/sizeof(half),
                    op.opX, // opX = alpha
                    nullptr,
                    mvTensorParam->myriadResources,
                    ELU
                );

                break;
            }

            case kInnerLRN:
            {
                postOps(
                    (fp16 *)mvTensorParam->input->data,
                    (fp16 *)mvTensorParam->output->data,
                    ((fp16 *)mvTensorParam->biases->data) + 1,
                    ((fp16 *)mvTensorParam->biases->data) + 2,
                    mvTensorParam->output->dimZ,
                    mvTensorParam->output->dimX * mvTensorParam->output->dimY,
                    mvTensorParam->output->dimXStride/sizeof(half),
                    0.0f,
                    nullptr,
                    mvTensorParam->myriadResources,
                    INNERLRN
                );

                break;
            }

            case kPower:
            {
                postOps(
                    (fp16 *)mvTensorParam->input->data,
                    (fp16 *)mvTensorParam->output->data,
                    nullptr,
                    nullptr,
                    mvTensorParam->input->dimZ,
                    mvTensorParam->input->dimX * mvTensorParam->input->dimY,
                    mvTensorParam->input->dimXStride/sizeof(half),
                    0.0f,
                    op.params,
                    mvTensorParam->myriadResources,
                    optimization.isSet(Optimization::OPT_POWER) ? POWER_ACCURATE : POWER_FAST
                );

                break;
            }

            case kPRelu:
            {
                postOps(
                    (fp16 *)mvTensorParam->input->data,
                    (fp16 *)mvTensorParam->output->data,
                    (fp16 *)mvTensorParam->biases->data, // We put them in bias, because they are 1D, so they fit there better
                    nullptr,
                    mvTensorParam->output->dimZ,
                    mvTensorParam->output->dimX * mvTensorParam->output->dimY,
                    mvTensorParam->output->dimXStride/sizeof(half),
                    0.0f,
                    nullptr,
                    mvTensorParam->myriadResources,
                    PRELU
                );

                break;
            }

            case kRelu:
            case kReluX:
            case kLeakyRelu:
            {
                PostOpType postOpType;
                switch (op.type)
                {
                case kRelu:      postOpType = RELU;           break;
                case kReluX:     postOpType = RELU_SAT;       break;
                case kLeakyRelu: postOpType = RELU_NEG_SLOPE; break;
                default:         postOpType = RELU;           break;  // This is to quiet the compiler warnings
                }

                postOps(
                    (fp16 *)mvTensorParam->output->data,
                    (fp16 *)mvTensorParam->output->data,
                    nullptr,
                    (fp16 *)(mvTensorParam->biases ? mvTensorParam->biases->data : nullptr),
                    mvTensorParam->output->dimZ,
                    mvTensorParam->output->dimX * mvTensorParam->output->dimY,
                    mvTensorParam->output->dimXStride/sizeof(half),
                    op.opX,
                    nullptr,
                    mvTensorParam->myriadResources,
                    postOpType
                );

                break;
            }

            case kScale:
            {
                postOps(
                    (fp16 *)mvTensorParam->input->data,
                    (fp16 *)mvTensorParam->output->data,
                    (fp16 *)mvTensorParam->weights->data,
                    op.type == kBias ? (fp16 *)mvTensorParam->biases->data : nullptr,
                    mvTensorParam->output->dimZ,
                    mvTensorParam->output->dimX * mvTensorParam->output->dimY,
                    mvTensorParam->output->dimXStride/sizeof(half),
                    0.0f,
                    nullptr,
                    mvTensorParam->myriadResources,
                    SCALE
                );

                break;
            }

            case kSigmoid:
            {
                //true  = Use the accurate method (slower).
                //false = Use the fast method (less accurate).
                const bool be_accurate = false;

                postOps(
                    (fp16 *)mvTensorParam->input->data,
                    (fp16 *)mvTensorParam->output->data,
                    nullptr,
                    nullptr,
                    mvTensorParam->input->dimZ,
                    mvTensorParam->input->dimX * mvTensorParam->input->dimY,
                    mvTensorParam->input->dimXStride/sizeof(half),
                    0.0f,
                    nullptr,
                    mvTensorParam->myriadResources,
                    be_accurate ? SIGMOID_ACCURATE : SIGMOID_FAST
                );

                break;
            }

            case kSquare:
            {
                postOps(
                    (fp16 *)mvTensorParam->input->data,
                    (fp16 *)mvTensorParam->output->data,
                    nullptr,
                    nullptr,
                    mvTensorParam->output->dimZ,
                    mvTensorParam->output->dimX * mvTensorParam->output->dimY,
                    mvTensorParam->output->dimXStride/sizeof(half),
                    0.0f,
                    nullptr,
                    mvTensorParam->myriadResources,
                    SQUARE);

                break;
            }

            case kTanh:
            {
                //true  = Use the accurate method (slower).
                //false = Use the fast method (less accurate).
                const bool be_accurate = false;

                postOps(
                    (fp16 *)mvTensorParam->input->data,
                    (fp16 *)mvTensorParam->output->data,
                    nullptr,
                    nullptr,
                    mvTensorParam->input->dimZ,
                    mvTensorParam->input->dimX * mvTensorParam->input->dimY,
                    mvTensorParam->input->dimXStride/sizeof(half),
                    0.0f,
                    nullptr,
                    mvTensorParam->myriadResources,
                    be_accurate ? TANH_ACCURATE : TANH_FAST
                );

                break;
            }

            case kMaxWithConstant:
            {
                postOps(
                    (fp16 *)mvTensorParam->input->data,
                    (fp16 *)mvTensorParam->output->data,
                    (fp16 *)mvTensorParam->weights->data,
                    nullptr,
                    mvTensorParam->output->dimZ,
                    mvTensorParam->output->dimX * mvTensorParam->output->dimY,
                    mvTensorParam->output->dimXStride/sizeof(half),
                    0.0f,
                    nullptr,
                    mvTensorParam->myriadResources,
                    MAX_WITH_CONSTANT
                );

                break;
            }

            case kRsqrt:
            {
                postOps(
                    (fp16 *)mvTensorParam->input->data,
                    (fp16 *)mvTensorParam->output->data,
                    nullptr,
                    nullptr,
                    mvTensorParam->output->dimZ,
                    mvTensorParam->output->dimX * mvTensorParam->output->dimY,
                    mvTensorParam->output->dimXStride/sizeof(half),
                    0.0f,
                    nullptr,
                    mvTensorParam->myriadResources,
                    RSQRT);

                break;
            }

            case kScaleWithScalar:
            {
                postOps(
                    (fp16 *)mvTensorParam->input->data,
                    (fp16 *)mvTensorParam->output->data,
                    (fp16 *)mvTensorParam->weights->data,
                    nullptr,
                    mvTensorParam->output->dimZ,
                    mvTensorParam->output->dimX * mvTensorParam->output->dimY,
                    mvTensorParam->output->dimXStride/sizeof(half),
                    0.0f,
                    nullptr,
                    mvTensorParam->myriadResources,
                    SCALE_WITH_SCALAR
                );

                break;
            }

            default: break;
        }

        res.shaveCache.writeback();
        res.shaveCache.invalidate();
    }

	const ModuleHandle handles[] =
	{
        ModuleHandle().add<PostOps>(kBias),
        ModuleHandle().add<PostOps>(kElu),
        ModuleHandle().add<PostOps>(kInnerLRN),
        ModuleHandle().add<PostOps>(kPower),
        ModuleHandle().add<PostOps>(kPRelu),
        ModuleHandle().add<PostOps>(kRelu),
        ModuleHandle().add<PostOps>(kReluX),
        ModuleHandle().add<PostOps>(kLeakyRelu),
        ModuleHandle().add<PostOps>(kScale),
        ModuleHandle().add<PostOps>(kSigmoid),
        ModuleHandle().add<PostOps>(kSquare),
        ModuleHandle().add<PostOps>(kTanh),
        ModuleHandle().add<PostOps>(kMaxWithConstant),
        ModuleHandle().add<PostOps>(kRsqrt),
        ModuleHandle().add<PostOps>(kScaleWithScalar),
	};
}
