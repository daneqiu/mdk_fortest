///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
///
///
#include <stdio.h>
#include <time.h>
#include "fastmemDevice.h"

int main()
{
    struct fastmemDevice *a;
    u8 *ptr = 0;
    int length;
    int counter = 0;

    fastmemInit(&a, "v4l", "/dev/video1");

    clock_t t1, t2;
    double t3;

    t1 = clock();
    while (1)
    {
        length = fastmemRead(a, &ptr, 0);
        if (length)
        {
            counter++;
            if (counter == 400)
                break;
        }
    }

    t2 = clock();
    t3 = (((double) t2) - ((double) t1)) / CLOCKS_PER_SEC;
    printf("%f %d %f\n", 1.0 * counter / t3, counter, t3);

    return 0;
}
