///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main file
///
#include <stdio.h>
#include "fastmemDevice.h"

#define USB_ENDPOINT_IN     (0x81)   /* endpoint address */
#define USB_ENDPOINT_OUT (0x01)   /* endpoint address */
#define USB_TIMEOUT         4000        /* Connection timeout (in ms) */

#define FRAME_WIDTH         640
#define FRAME_HEIGHT        480

u8 wb[FRAME_WIDTH * FRAME_HEIGHT * 3];
u8 rb[FRAME_WIDTH * FRAME_HEIGHT * 3];

int main()
{
    struct fastmemDevice *a;
    int transferLength;

    fastmemInit(&a, "usb", 0, 0); //0 for default pId and vId
    while (1)
    {
        transferLength = fastmemWrite(a, USB_ENDPOINT_OUT, wb,
        FRAME_WIDTH * FRAME_HEIGHT, USB_TIMEOUT);
        printf("%d\n", transferLength);

        transferLength = fastmemRead(a, USB_ENDPOINT_IN, rb,
        FRAME_WIDTH * FRAME_HEIGHT, USB_TIMEOUT);
        printf("%d\n", transferLength);
    }

    return 0;
}
