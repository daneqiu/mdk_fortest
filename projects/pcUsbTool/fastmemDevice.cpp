///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
///
///
#include <iostream>
#include <cstdio>
#include <cstdarg>

#include "fastmemDevice.hpp"

//usb
static int32_t fastmemInitUsb(fastmemDevice *obj, va_list args)
{
    uint16_t pId = va_arg(args, uint32_t);
    uint16_t vId = va_arg(args, uint32_t);

    if (pId)
        return obj->fastmemInit(vId, pId);
    if (vId)
        return obj->fastmemInit(vId);
    return obj->fastmemInit();
}

static int32_t fastmemReadUsb(fastmemDevice *obj, va_list args)
{
    uint8_t ep = va_arg(args, uint32_t);
    uint8_t *buffer = va_arg(args, uint8_t*);
    uint32_t length = va_arg(args, uint32_t);
    uint32_t timeout = va_arg(args, uint32_t);

    return obj->fastmemRead(ep, buffer, length, timeout);
}

static int32_t fastmemWriteUsb(fastmemDevice *obj, va_list args)
{
    uint8_t ep = va_arg(args, uint32_t);
    uint8_t *buffer = va_arg(args, uint8_t*);
    uint32_t length = va_arg(args, uint32_t);
    uint32_t timeout = va_arg(args, uint32_t);

    return obj->fastmemWrite(ep, buffer, length, timeout);
}

#ifdef __unix__
static int32_t fastmemInitV4L(fastmemDevice *obj, va_list args)
{
    char *p = va_arg(args, char*);
    if (p == NULL)
    {
        std::cerr << "V4L device is invalid!\n";
        return -1;
    }
    return obj->fastmemInit(p);
}

static int32_t fastmemReadV4L(fastmemDevice *obj, va_list args)
{
    uint8_t **buffer = va_arg(args, uint8_t**);
    uint32_t timeout = va_arg(args, uint32_t);

    return obj->fastmemRead(buffer, timeout);
}

#endif //__unix__

//#ifdef _WIN32
//static int fastmemInitDShow(fastmemDevice *obj, va_list args)
//{
//    int dev = va_arg(args, int);
//    int width = va_arg(args, int);
//    int height = va_arg(args, int);

//    return obj->fastmemInit(dev, width, height);
//}

//static int fastmemReadDShow(fastmemDevice *obj, va_list args)
//{
//    uint8_t **buffer = va_arg(args, uint8_t**);
//    uint32_t timeout = va_arg(args, uint32_t);

//    return obj->fastmemRead(buffer, timeout);
//}

//#endif //_WIN32

//general
void fastmemUninit(fastmemDevice **obj)
{
    delete (*obj);
}

int32_t fastmemInit(fastmemDevice **obj, const char *type, ...)
{
    int32_t ret = 0;

    *obj = new (std::nothrow) fastmemDevice(type);
    if (!obj)
    {
        printf("Could not allocate memory for device: %s!\n", type);
        return -1;
    }

    va_list args;
    va_start(args, type);

    if ((*obj)->getTypeString() == "usb")
        ret = fastmemInitUsb((*obj), args);
#ifdef __unix__
    if ((*obj)->getTypeString() == "v4l")
    ret = fastmemInitV4L((*obj), args);
#endif //__unix__
//#ifdef _WIN32
//    if ((*obj)->getTypeString() == "dshow")
//    ret = fastmemInitDShow((*obj), args);
//#endif //_WIN32

    va_end(args);
    return ret;
}

int32_t fastmemRead(fastmemDevice *obj, ...)
{
    int32_t ret = 0;
    va_list args;
    va_start(args, obj);

    if (obj->getTypeString() == "usb")
        ret = fastmemReadUsb(obj, args);
#ifdef __unix__
    if (obj->getTypeString() == "v4l")
    ret = fastmemReadV4L(obj, args);
#endif //__unix__
//#ifdef _WIN32
//    if (obj->getTypeString() == "dshow")
//    ret = fastmemReadDShow(obj, args);
//#endif //_WIN32

    va_end(args);
    return ret;
}

int32_t fastmemWrite(fastmemDevice *obj, ...)
{
    int32_t ret = 0;
    va_list args;
    va_start(args, obj);

    if (obj->getTypeString() == "usb")
        ret = fastmemWriteUsb(obj, args);

    va_end(args);
    return ret;
}
