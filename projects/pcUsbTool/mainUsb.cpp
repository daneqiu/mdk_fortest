///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main file
///
#include <iostream>
#include <mv_types.h>
#include "fastmemDevice.hpp"

#define USB_ENDPOINT_IN     (0x81)   /* endpoint address */
#define USB_ENDPOINT_OUT    (0x01)   /* endpoint address */
#define USB_TIMEOUT         4000

using namespace std;

u8 buf[640*480];

int main(int argc, char **argv)
{
    fastmemDevice a("usb"); //type of device as constructor parameter

    a.fastmemInit(); //default vId and pId
    //a.fastmemInit(0x040e); //specified vId, default pId
    //a.fastmemInit(0x040e, 0xf63b); //specified vId and pId

    while (1)
    {
        //fastmemWrite works the same as fastmemRead
        int b = a.fastmemRead(USB_ENDPOINT_IN, buf, 640*480, USB_TIMEOUT);
        if (b)
            cout << b << "\n";
    }
    return 0;
}

