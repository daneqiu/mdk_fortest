///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
///
///
#include <iostream>
#include "fastmemUsb.hpp"

int32_t fastmemUsb::fastmemInit(uint16_t vId, uint16_t pId)
{
    int32_t resp;

    libusb_init(&ctx);
    libusb_set_debug(ctx, 3);
    vendorId = vId;
    productId = pId;
    try
    {
        handle = libusb_open_device_with_vid_pid(ctx, vendorId, productId);
        if (!handle)
            throw LIBUSB_ERROR_OTHER;

        resp = libusb_claim_interface(handle, 0);

        if (resp < 0)
            throw resp;
    }
    catch (libusb_error e) {
        if (e == LIBUSB_ERROR_OTHER)
            std::cerr << "libusb could not be initialized!\n";
        return (int)e;
    }
    catch (int e) {
        if (e < 0)
            std::cerr << "usb_claim_interface error " << e << "\n";
        return e;
    }
    return 0;
}

fastmemUsb::~fastmemUsb(){
    fastmemUninit();
}

void fastmemUsb::fastmemUninit()
{
    if (!handle)
        return;
    libusb_release_interface(handle, 0);
    libusb_close(handle);
    libusb_exit(ctx);
    vendorId = productId = 0;
}

int32_t fastmemUsb::fastmemRead(uint8_t ep, uint8_t *buffer, uint32_t length, uint32_t timeout)
{
    int32_t bytesRead, resp;

    resp = libusb_bulk_transfer(handle, ep, buffer, length, &bytesRead,
            timeout);

    if (resp)
    {
        std::cerr << "ERROR in bulk read: " << resp << "\n";
        return resp;
    }

    return bytesRead;
}

int32_t fastmemUsb::fastmemWrite(uint8_t ep, uint8_t *buffer, uint32_t length, uint32_t timeout)
{
    int32_t bytesWritten, resp;

    resp = libusb_bulk_transfer(handle, ep, buffer, length, &bytesWritten,
            timeout);

    if (resp)
    {
        std::cerr << "ERROR in bulk write: " << resp << "\n";
        return resp;
    }

    return bytesWritten;
}
