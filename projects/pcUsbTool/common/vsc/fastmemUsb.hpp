///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
///
///
#ifndef FASTMEM_USB_HPP
#define FASTMEM_USB_HPP

#include "fastmemAbstractDevice.hpp"
#include <vector>

class fastmemUsb: public fastmemAbstractDevice
{
    private:
        libusb_context *ctx;
        libusb_device_handle *handle;
        uint16_t vendorId, productId;
    public:
        ~fastmemUsb();
        int32_t fastmemInit(uint16_t vId = 0x040e, uint16_t pId = 0xf63b);
        void fastmemUninit();
        int32_t fastmemRead(uint8_t ep, uint8_t *buffer, uint32_t length, uint32_t timeout);
        int32_t fastmemWrite(uint8_t ep, uint8_t *buffer, uint32_t length, uint32_t timeout);
};

#endif //FASTMEM_USB_HPP
