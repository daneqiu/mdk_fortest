///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
///
///
#include <mv_types.h>
#include <iostream>
#include "fastmemDevice.hpp"

using namespace std;

int main(int argc, char **argv)
{
    u8 *ptr;

    fastmemDevice a("v4l");  //type of device as constructor parameter
    a.fastmemInit("/dev/video1"); //device name is necessary for v4l

    while (1)
    {
        int b = a.fastmemRead(&ptr, 0); // the buffer is automatically created
                                        // 0 for infinite timeout
        if (b)
            cout << b << "\n";
    }
    return 0;
}

