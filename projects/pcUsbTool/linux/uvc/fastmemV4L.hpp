///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
///
///
#ifndef FASTMEM_V4L_HPP
#define FASTMEM_V4L_HPP

#include "fastmemAbstractDevice.hpp"
#include <sys/time.h>

struct deviceBuffer
{
        void *start;
        uint32_t length;
};

class fastmemV4L: public fastmemAbstractDevice
{
    private:
        deviceBuffer *buffers;
        std::string devName;
        int32_t fd;
        uint32_t n_buffers;
        struct timeval timeBefore, timeAfter;
        int64_t timeoutExec, elapsedTime;

        void errno_exit(const char *s);
        int32_t xioctl(int32_t fh, int32_t request, void *arg);
        void close_device();
        void open_device(const char *dev_name);
        void stop_capturing();
        void start_capturing(void);
        deviceBuffer* init_mmap(const char *dev_name);
        void capabilitiesCheck(const char *dev_name);
        void cropCheck();
        void formatCheck();

    public:
        ~fastmemV4L();
        int32_t fastmemInit(const char *deviceName);
        void fastmemUninit();
        int32_t fastmemRead(uint8_t **destBuf, int64_t timeout);
};


typedef struct deviceCapture
{
    char *devName;
    deviceBuffer *buffers;
} deviceCapture;

void init_start(void *deviceSrc);
int32_t stop_uninit(deviceBuffer **buffers);
int32_t read_frame(deviceCapture *cam, deviceBuffer *destBuf, uint64_t timeout);

#endif //FASTMEM_V4L_HPP
