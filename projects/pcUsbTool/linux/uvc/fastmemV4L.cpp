///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
///
///
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <string>
#include <iostream>

#include <fcntl.h>              /* low-level i/o */
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <linux/videodev2.h>
#include "fastmemV4L.hpp"

#define CLEAR(x) memset(&(x), 0, sizeof(x))

void fastmemV4L::errno_exit(const char *s)
{
    fprintf(stderr, "%s error %d, %s\n", s, errno, strerror(errno));
    exit (EXIT_FAILURE);
}

int32_t fastmemV4L::xioctl(int32_t fh, int32_t request, void *arg)
{
    int32_t r;

    gettimeofday(&timeBefore, NULL);
    do
    {
        r = ioctl(fh, request, arg);
        //timeout check
        if (timeoutExec > 0)
        {
            gettimeofday(&timeAfter, NULL);
            elapsedTime = (timeAfter.tv_sec - timeBefore.tv_sec) * 1000000LL
                    + (timeAfter.tv_usec - timeAfter.tv_usec);
            if (elapsedTime > timeoutExec)
                return -1;
        }
    } while (-1 == r && EINTR == errno);

    return r;
}

void fastmemV4L::close_device()
{
    if (-1 == close(fd))
        errno_exit("close");
    fd = -1;
}

void fastmemV4L::open_device(const char *dev_name)
{
    struct stat st;

    if (-1 == stat(dev_name, &st))
    {
        fprintf(stderr, "Cannot identify '%s': %d, %s\n", dev_name, errno,
                strerror(errno));
        exit (EXIT_FAILURE);
    }

    if (!S_ISCHR(st.st_mode))
    {
        fprintf(stderr, "%s is no device\n", dev_name);
        exit (EXIT_FAILURE);
    }

    fd = open(dev_name, O_RDWR /* required */| O_NONBLOCK, 0);

    if (-1 == fd)
    {
        fprintf(stderr, "Cannot open '%s': %d, %s\n", dev_name, errno,
                strerror(errno));
        exit (EXIT_FAILURE);
    }
}

void fastmemV4L::stop_capturing()
{
    enum v4l2_buf_type type;

    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (-1 == xioctl(fd, VIDIOC_STREAMOFF, &type))
        errno_exit("VIDIOC_STREAMOFF");
}

void fastmemV4L::start_capturing()
{
    uint32_t i;
    enum v4l2_buf_type type;

    for (i = 0; i < n_buffers; ++i)
    {
        struct v4l2_buffer buf;

        CLEAR(buf);
        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        buf.index = i;

        if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
            errno_exit("VIDIOC_QBUF");
    }
    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (-1 == xioctl(fd, VIDIOC_STREAMON, &type))
        errno_exit("VIDIOC_STREAMON");
}

deviceBuffer* fastmemV4L::init_mmap(const char *dev_name)
{
    struct v4l2_requestbuffers req;
    deviceBuffer *buffers;

    CLEAR(req);

    req.count = 4;
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;

    if (-1 == xioctl(fd, VIDIOC_REQBUFS, &req))
    {
        if (EINVAL == errno)
        {
            fprintf(stderr, "%s does not support "
                    "memory mapping\n", dev_name);
            exit (EXIT_FAILURE);
        }
        else
        {
            errno_exit("VIDIOC_REQBUFS");
        }
    }

    if (req.count < 2)
    {
        fprintf(stderr, "Insufficient buffer memory on %s\n", dev_name);
        exit (EXIT_FAILURE);
    }

    buffers = (deviceBuffer*) calloc(req.count, sizeof(*buffers));

    if (!buffers)
    {
        fprintf(stderr, "Out of memory\n");
        exit (EXIT_FAILURE);
    }

    for (n_buffers = 0; n_buffers < req.count; ++n_buffers)
    {
        struct v4l2_buffer buf;

        CLEAR(buf);

        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        buf.index = n_buffers;

        if (-1 == xioctl(fd, VIDIOC_QUERYBUF, &buf))
            errno_exit("VIDIOC_QUERYBUF");

        buffers[n_buffers].length = buf.length;
        buffers[n_buffers].start = mmap(NULL /* start anywhere */, buf.length,
                PROT_READ | PROT_WRITE /* required */,
                MAP_SHARED /* recommended */, fd, buf.m.offset);

        if (MAP_FAILED == buffers[n_buffers].start)
            errno_exit("mmap");
    }
    return buffers;
}

void fastmemV4L::capabilitiesCheck(const char *dev_name)
{
    struct v4l2_capability cap;

    if (-1 == xioctl(fd, VIDIOC_QUERYCAP, &cap))
    {
        if (EINVAL == errno)
        {
            fprintf(stderr, "%s is no V4L2 device\n", dev_name);
            exit (EXIT_FAILURE);
        }
        else
        {
            errno_exit("VIDIOC_QUERYCAP");
        }
    }

    if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE))
    {
        fprintf(stderr, "%s is no video capture device\n", dev_name);
        exit (EXIT_FAILURE);
    }

    if (!(cap.capabilities & V4L2_CAP_STREAMING))
    {
        fprintf(stderr, "%s does not support streaming i/o\n", dev_name);
        exit (EXIT_FAILURE);
    }
}

void fastmemV4L::cropCheck()
{
    struct v4l2_cropcap cropcap;
    struct v4l2_crop crop;

    CLEAR(cropcap);
    cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (0 == xioctl(fd, VIDIOC_CROPCAP, &cropcap))
    {
        crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        crop.c = cropcap.defrect; /* reset to default */

        if (-1 == xioctl(fd, VIDIOC_S_CROP, &crop))
        {
            switch (errno)
            {
                case EINVAL:
                    /* Cropping not supported. */
                    break;
                default:
                    /* Errors ignored. */
                    break;
            }
        }
    }
}

void fastmemV4L::formatCheck()
{
    struct v4l2_format format;

    CLEAR(format);
    format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    format.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;

    if (-1 == xioctl(fd, VIDIOC_G_FMT, &format))
        errno_exit("VIDIOC_G_FMT");
}

fastmemV4L::~fastmemV4L()
{
    fastmemUninit();
}

int32_t fastmemV4L::fastmemInit(const char *deviceName)
{
    devName = deviceName;
    fd = timeoutExec = -1;

    open_device(devName.c_str());
    capabilitiesCheck(devName.c_str());
    cropCheck();
    formatCheck();

    buffers = init_mmap(devName.c_str());
    start_capturing();

    return 1;
}

void fastmemV4L::fastmemUninit()
{
    unsigned int i;

    stop_capturing();
    //uninit
    for (i = 0; i < n_buffers; ++i)
        if (-1 == munmap(buffers[i].start, buffers[i].length))
            errno_exit("munmap");
    free(buffers);

    close_device();
}
int32_t fastmemV4L::fastmemRead(uint8_t **destBuf, int64_t timeout)
{
    struct v4l2_buffer buf;

    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;
    timeoutExec = timeout * 1000LL;

    if (-1 == xioctl(fd, VIDIOC_DQBUF, &buf))
    {
        switch (errno)
        {
            case EAGAIN:
                return 0;

            default:
                errno_exit("VIDIOC_DQBUF");
        }
    }
    assert(buf.index < n_buffers);
    *destBuf = (uint8_t*) buffers[buf.index].start;

    if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
        errno_exit("VIDIOC_QBUF");

    timeoutExec = -1;
    return buf.bytesused;
}
