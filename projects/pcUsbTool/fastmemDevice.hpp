///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
///
///
#ifndef FASTMEM_HPP
#define FASTMEM_HPP

#define FASTMEMDEVICE_TYPE "usb"

#include <string>

#include "fastmemUsb.hpp"

#ifdef __unix__
#include "fastmemV4L.hpp"
#endif //__unix__

//#ifdef _WIN32
//#include "fastmemDShow.hpp"
//#endif //_WIN32

class fastmemDevice
{
    private:
        fastmemAbstractDevice *dev;
        std::string type;
    public:
        fastmemDevice(std::string type)
        {
            if (type == "usb" || type == "USB")
            {
                dev = new fastmemUsb();
                this->type = "usb";
            }
#ifdef __unix__
            if (type == "v4l" || type == "V4L")
            {
                dev = new fastmemV4L();
                this->type = "v4l";
            }
#endif //__unix__

//#ifdef _WIN32
//            if (type == "dshow" || type == "DSHOW")
//            {
//                dev = new fastmemDShow();
//                this->type = "dshow";
//            }
//#endif //_WIN32
        }

        ~fastmemDevice()
        {
            fastmemUninit();
        }

        const char *getType()
        {
            return type.c_str();
        }

        std::string getTypeString()
        {
            return type;
        }

        int32_t fastmemInit()
        {
            if (type == "usb")
                return ((fastmemUsb*) dev)->fastmemInit(0x040e, 0xf63b);

            return -1;
        }

        void fastmemUninit()
        {
            if (!dev)
                return;
            if (type == "usb")
                ((fastmemUsb*) dev)->fastmemUninit();
#ifdef __unix__
            if (type == "v4l")
            ((fastmemV4L*) dev)->fastmemUninit();
#endif //__unix__

//#ifdef _WIN32
//            if (type == "dshow")
//            ((fastmemDShow*) dev)->fastmemUninit();
//#endif //_WIN32

            delete dev;
            dev = 0;
        }

        //usb
        int32_t fastmemInit(uint16_t vId, uint16_t pId = 0xf63b)
        {
            return ((fastmemUsb*) dev)->fastmemInit(vId, pId);
        }

        int32_t fastmemRead(uint8_t ep, uint8_t *buffer, uint32_t length, uint32_t timeout)
        {
            return ((fastmemUsb*) dev)->fastmemRead(ep, buffer, length, timeout);
        }

        int32_t fastmemWrite(uint8_t ep, uint8_t *buffer, uint32_t length, uint32_t timeout)
        {
            return ((fastmemUsb*) dev)->fastmemWrite(ep, buffer, length,
                    timeout);
        }
#ifdef __unix__
        //v4l
        int32_t fastmemInit(std::string deviceName)
        {
            return ((fastmemV4L*) dev)->fastmemInit(deviceName.c_str());
        }
#endif //__unix__

//#ifdef _WIN32
//        //dshow
//        int fastmemInit(int dev, int w, int h)
//        {
//            return ((fastmemDShow*) dev)->fastmemInit(dev, w, h);
//        }
//#endif //_WIN32

        //v4l and dshow
        int32_t fastmemRead(uint8_t **buffer, uint32_t timeout)
        {
#ifdef __unix__
            if (type == "v4l")
            return ((fastmemV4L*) dev)->fastmemRead(buffer, timeout);
#endif

//#ifdef _WIN32
//            if (type == "dshow")
//            return ((fastmemDShow*) dev)->fastmemRead(buffer, timeout);
//#endif //_WIN32

            throw -1; //never reached
            return 0;
        }

};

extern "C"
{
int32_t fastmemInit(fastmemDevice **obj, const char *type, ...);
int32_t fastmemRead(fastmemDevice *obj, ...);
int32_t fastmemWrite(fastmemDevice *obj, ...);
}

#endif //FASTMEM_HPP
