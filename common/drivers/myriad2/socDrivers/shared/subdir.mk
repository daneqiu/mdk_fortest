

srcs-los-$(CONFIG_SOC_2x5x) += \
	src/CmxDma.c \
	arch/ma2x5x/sgl/src/SglResMgr.c

srcs-los-$(CONFIG_SOC_2x8x) += \
	arch/ma2x8x/hgl/src/HglCmxDma.c \
	arch/ma2x8x/hgl/src/HglFreeRunCnt.c \
	arch/ma2x8x/sgl/src/SglCmxDma.c \
	arch/ma2x8x/sgl/src/SglResMgr.c

srcs-lrt-y = $(srcs-los-y)

srcs-shave-y += $(srcs-los-y)

