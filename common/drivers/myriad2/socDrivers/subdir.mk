
subdirs-los-y += leon/hgl
subdirs-los-$(CONFIG_SOC_LOS_MODE_BM) += leon/bm
subdirs-los-$(CONFIG_SOC_LOS_MODE_RTEMS) += leon/rtems leon/bm
subdirs-los-y += shared

subdirs-lrt-y += leon/hgl
subdirs-lrt-$(CONFIG_SOC_LRT_MODE_BM) += leon/bm
subdirs-lrt-$(CONFIG_SOC_LRT_MODE_RTEMS) += leon/rtems leon/bm
subdirs-lrt-y += shared

subdirs-shave-y += shared

# IMPORTANT NOTE:
# Please maintain drivers include dirs here and not further down into the
# directory hierarchy; this will make our lives simpler, esp in this
# transition phae to the new HGL and no BM for RTEMS
include-dirs-los-$(CONFIG_SOC_2x5x) +=\
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/shared/arch/ma2x5x/sgl/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/hgl/arch/ma2x5x/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/bm/arch/ma2x5x/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/shared/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/bm/src

include-dirs-los-$(CONFIG_SOC_2x8x) +=\
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/shared/arch/ma2x8x/sgl/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/shared/arch/ma2x8x/hgl/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/shared/arch/ma2x8x/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/shared/include \
 	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/hgl/arch/ma2x8x/include

include-dirs-los-$(CONFIG_SOC_LOS_MODE_RTEMS) += \
  $(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/rtems/include

ifeq "y" "$(CONFIG_SOC_LOS_MODE_RTEMS)"
include-dirs-los-$(CONFIG_SOC_2x5x) += \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/rtems/arch/ma2x5x/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/rtems/src
include-dirs-los-$(CONFIG_SOC_2x8x) += \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/rtems/arch/ma2x8x/include
else
include-dirs-los-$(CONFIG_SOC_2x5x) += \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/cross/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/rtems/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/hgl/include
endif

include-dirs-lrt-$(CONFIG_SOC_2x5x) +=\
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/shared/arch/ma2x5x/sgl/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/hgl/arch/ma2x5x/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/bm/arch/ma2x5x/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/shared/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/bm/src

include-dirs-lrt-$(CONFIG_SOC_2x8x) +=\
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/shared/arch/ma2x8x/sgl/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/shared/arch/ma2x8x/hgl/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/shared/arch/ma2x8x/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/shared/include \
 	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/hgl/arch/ma2x8x/include

include-dirs-lrt-$(CONFIG_SOC_LRT_MODE_RTEMS) += \
  $(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/rtems/include

ifeq "y" "$(CONFIG_SOC_LRT_MODE_RTEMS)"
include-dirs-lrt-$(CONFIG_SOC_2x5x) += \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/rtems/arch/ma2x5x/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/rtems/src
include-dirs-lrt-$(CONFIG_SOC_2x8x) += \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/rtems/arch/ma2x8x/include
else
include-dirs-lrt-$(CONFIG_SOC_2x5x) += \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/cross/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/rtems/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/hgl/include
endif

include-dirs-shave-y += \
	$(MDK_INSTALL_DIR_ABS)/drivers/$(MV_SOC_PLATFORM)/socDrivers/shared/include

include-dirs-shave-$(CONFIG_SOC_2x5x)+=\
	$(MDK_INSTALL_DIR_ABS)/drivers/$(MV_SOC_PLATFORM)/socDrivers/shared/arch/ma2x5x/sgl/include \
 	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/shave/arch/ma2x5x/include \
 	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/shave/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/hgl/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/bm/arch/ma2x5x/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/bm/include

include-dirs-shave-$(CONFIG_SOC_2x8x)+=\
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/shared/arch/ma2x8x/hgl/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/shared/arch/ma2x8x/sgl/include \
 	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/shared/arch/ma2x8x/include \
 	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/shave/arch/ma2x8x/include \
 	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/bm/arch/ma2x8x/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/hgl/arch/ma2x8x/include

asm-include-dirs-shave-y=\
	$(MDK_INSTALL_DIR_ABS)/drivers/$(MV_SOC_PLATFORM)/socDrivers/shave/arch/$(CONFIG_SOC_DIR)/include

