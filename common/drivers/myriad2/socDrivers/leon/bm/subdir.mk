# Build rules for the BM drivers

srcs-bm +=	src/DrvAhbDma.c \
	src/DrvCdma.c \
	src/DrvCif.c \
	src/DrvCmxDma.c \
	src/DrvCmxDmaArch.h \
	src/DrvCommon.c \
	src/DrvCpr.c \
	src/DrvDdr.c \
	src/DrvDdrArch.h \
	src/DrvEfuse.c \
	src/DrvEfuseIO.c \
	src/DrvEth.c \
	src/DrvGpio.c \
	src/DrvGpioArch.h \
	src/DrvI2c.c \
	src/DrvI2cMaster.c \
	src/DrvI2s.c \
	src/DrvIcb.c \
	src/DrvLcd.c \
	src/DrvLeon.c \
	src/DrvLeonL2C.c \
	src/DrvMipi.c \
	src/DrvMss.c \
	src/DrvMutex.c \
	src/DrvRegUtils.c \
	src/DrvResMgr.c \
	src/DrvRoic.c \
	src/DrvSdio.c \
	src/DrvShaveL2Cache.c \
	src/DrvSipp.c \
	src/DrvSpiMaster.c \
	src/DrvSpiSlave.c \
	src/DrvSpiSlaveCP.c \
	src/DrvSvu.c \
	src/DrvTimer.c \
	src/DrvUart.c \
	src/OsCross.c

srcs-bm-ma2150 += \
	arch/ma215x/src/DrvDdrArchMa215x.c \
	arch/ma2x5x/src/DrvCmxDmaArchMa2x5x.c \
	arch/ma2x5x/src/DrvDdrArchMa2x5x.c \
	arch/ma2x5x/src/DrvDdrMa2x5x.c \
	arch/ma2x5x/src/DrvGpioMa2x5x.c \
	arch/ma2x5x/src/DrvTempSensor.c

srcs-bm-ma2x8x += \
	arch/ma2x8x/src/DrvAxiDma.c \
	arch/ma2x8x/src/DrvCif.c \
	arch/ma2x8x/src/DrvCmxDma.c \
	arch/ma2x8x/src/DrvCommon.c \
	arch/ma2x8x/src/DrvCpr.c \
	arch/ma2x8x/src/DrvCprHgl.c \
	arch/ma2x8x/src/DrvDdr.c \
	arch/ma2x8x/src/DrvEfuse.c \
	arch/ma2x8x/src/DrvEfuseIO.c \
	arch/ma2x8x/src/DrvGpio.c \
	arch/ma2x8x/src/DrvGpioMa2x8x.c \
	arch/ma2x8x/src/DrvI2c.c \
	arch/ma2x8x/src/DrvI2cMaster.c \
	arch/ma2x8x/src/DrvI2s.c \
	arch/ma2x8x/src/DrvIcb.c \
	arch/ma2x8x/src/DrvLcd.c \
	arch/ma2x8x/src/DrvLeon.c \
	arch/ma2x8x/src/DrvLeonL2C.c \
	arch/ma2x8x/src/DrvMipi.c \
	arch/ma2x8x/src/DrvMutex.c \
	arch/ma2x8x/src/DrvNoc.c \
	arch/ma2x8x/src/DrvRegUtils.c \
	arch/ma2x8x/src/DrvSdio.c \
	arch/ma2x8x/src/DrvShaveL2Cache.c \
	arch/ma2x8x/src/DrvSpiMaster.c \
	arch/ma2x8x/src/DrvSpiSlave.c \
	arch/ma2x8x/src/DrvSpiSlaveCP.c \
	arch/ma2x8x/src/DrvSvu.c \
	arch/ma2x8x/src/DrvTimer.c \
	arch/ma2x8x/src/DrvTsens.c \
	arch/ma2x8x/src/DrvUart.c

srcs-los-$(CONFIG_SOC_2x5x) += \
	$(srcs-bm) \
	src/DrvLeon.c \
	$(srcs-bm-ma2150)

srcs-los-$(CONFIG_SOC_2x8x) += $(srcs-bm-ma2x8x)

# do not include src/DrvLeon.c on LRT
srcs-lrt-$(CONFIG_SOC_2x5x) = \
	$(srcs-bm) \
	$(srcs-bm-ma2150)

srcs-lrt-$(CONFIG_SOC_2x5x) += $(srcs-bm-ma2150)
srcs-lrt-$(CONFIG_SOC_2x8x) += $(srcs-bm-ma2x8x)

