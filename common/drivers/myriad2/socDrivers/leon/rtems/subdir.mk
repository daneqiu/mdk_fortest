
ccopt-los-y += -D__RTEMS__ \
	-isystem $(MV_RTEMS_LOS_LIB)/include \
  -I $(MV_TOOLS_BASE)/$(DETECTED_PLATFORM)/$(SPARC_DIR)/$(MV_GCC_TOOLS)/include \
	-I $(MV_TOOLS_BASE)/$(DETECTED_PLATFORM)/$(SPARC_DIR)/lib/gcc/$(MV_GCC_TOOLS)/$(GCCVERSION)/include \
	-I $(MV_TOOLS_BASE)/$(DETECTED_PLATFORM)/$(SPARC_DIR)/lib/gcc/$(MV_GCC_TOOLS)/$(GCCVERSION)/include/c++

ccopt-lrt-y += -D__RTEMS__ \
	-isystem $(MV_RTEMS_LRT_LIB)/include \
  -I $(MV_TOOLS_BASE)/$(DETECTED_PLATFORM)/$(SPARC_DIR)/$(MV_GCC_TOOLS)/include \
	-I $(MV_TOOLS_BASE)/$(DETECTED_PLATFORM)/$(SPARC_DIR)/lib/gcc/$(MV_GCC_TOOLS)/$(GCCVERSION)/include \
	-I $(MV_TOOLS_BASE)/$(DETECTED_PLATFORM)/$(SPARC_DIR)/lib/gcc/$(MV_GCC_TOOLS)/$(GCCVERSION)/include/c++

subdirs-los-$(CONFIG_SOC_2x5x) += arch/ma2x5x/src src
subdirs-los-$(CONFIG_SOC_2x8x) += arch/ma2x8x/src

subdirs-lrt-y = $(subdirs-los-y)

