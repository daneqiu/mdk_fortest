

srcs-los-$(CONFIG_SOC_2x5x) += \
	OsCommon.c \
	OsDrvAhbDma.c \
	OsDrvCmxDma.c \
	OsDrvCpr.c \
	OsDrvGpio.c \
	OsDrvI2cBus.c \
	OsDrvI2cMyr2.c \
	OsDrvMutex.c \
	OsDrvSdio.c \
	OsDrvShaveL2Cache.c \
	OsDrvSpiBus.c \
	OsDrvSvu.c \
	OsDrvTimer.c \
	OsDrvUart.c \
	OsDrvUsbPhy.c \
	OsDrvUsbPhyArch.h \
	OsSpiCommon.c \
	OsSpiSlave.c \
	OsSpiSlaveFifo.c

srcs-lrt-y = $(srcs-los-y)
