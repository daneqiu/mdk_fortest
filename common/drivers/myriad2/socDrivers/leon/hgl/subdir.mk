
# include-dirs-los-$(CONFIG_SOC_2x5x) += \
# 	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/hgl/arch/ma2x5x/include \
# 	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/bm/arch/ma2x5x/include
# include-dirs-los-$(CONFIG_SOC_2x8x) += \
# 	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/hgl/arch/ma2x8x/include \
# 	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/bm/arch/ma2x8x/include
# include-dirs-los-$(CONFIG_SOC_LOS_MODE_BM) +=	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/bm/include
# include-dirs-los-y += $(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/hgl/include
#
#
#
# include-dirs-lrt-$(CONFIG_SOC_2x5x) += \
# 	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/hgl/arch/ma2x5x/include \
# 	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/bm/arch/ma2x5x/include
# include-dirs-lrt-$(CONFIG_SOC_2x8x) += \
# 	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/hgl/arch/ma2x8x/include \
# 	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/bm/arch/ma2x8x/include
# include-dirs-lrt-$(CONFIG_SOC_LRT_MODE_BM) +=	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/bm/include
# include-dirs-lrt-y += $(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/hgl/include

srcs-los-$(CONFIG_SOC_2x5x)+=\
	src/HglCommon.c \
	src/HglCpr.c \
	src/HglMutex.c \
	src/HglSpi.c \
	src/HglGpioTimer.c \
	arch/ma2x5x/src/HglCprMa2x5x.c \
	arch/ma2x5x/src/HglMutexMa2x5x.c \
	arch/ma2x5x/src/HglTempSensor.c

srcs-los-$(CONFIG_SOC_2x8x)+=\
	arch/ma2x8x/src/HglAxiDma.c \
	arch/ma2x8x/src/HglCommon.c \
	arch/ma2x8x/src/HglCpr.c \
	arch/ma2x8x/src/HglI2c.c \
	arch/ma2x8x/src/HglLeonMss.c \
	arch/ma2x8x/src/HglMipi.c \
	arch/ma2x8x/src/HglMmcSdio.c \
	arch/ma2x8x/src/HglMutex.c \
	arch/ma2x8x/src/HglShave.c \
	arch/ma2x8x/src/HglShaveL2c.c \
	arch/ma2x8x/src/HglSpi.c \
	arch/ma2x8x/src/HglTempSensor.c \
	arch/ma2x8x/src/HglUart.c

srcs-lrt-y = $(srcs-los-y)

