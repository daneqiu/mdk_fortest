
include-dirs-los-y += \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/brdDrivers/osDrivers/include
include-dirs-lrt-y = $(include-dirs-los-y)

srcs-los-$(CONFIG_BSP_MODEL_MV0182) += OsBrdMv0182/OsBrdMv0182.c
srcs-los-$(CONFIG_BSP_MODEL_MV0212) += OsBrdMv0212/OsBrdMv0212.c

srcs-lrt-$(CONFIG_BSP_MODEL_MV0182) += OsBrdMv0182/OsBrdMv0182.c
srcs-lrt-$(CONFIG_BSP_MODEL_MV0212) += OsBrdMv0212/OsBrdMv0212.c
