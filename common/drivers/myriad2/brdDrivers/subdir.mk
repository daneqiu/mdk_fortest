
subdirs-los-$(CONFIG_SOC_LOS_MODE_RTEMS) += osDrivers
subdirs-lrt-$(CONFIG_SOC_LOS_MODE_RTEMS) += osDrivers

include-dirs-los-y += \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/brdDrivers/include
include-dirs-lrt-y = $(include-dirs-los-y)

srcs-los-$(CONFIG_BSP_MODEL_MV0182) += \
	src/brdMv0182/brdMv0182.c \
	src/brdMv0182/brdMv0182GpioDefaults.c
srcs-los-$(CONFIG_BSP_MODEL_MV0212) += \
	src/brdMv0212/brdMv0212.c \
	src/brdMv0212/brdMv0212GpuiDefaults.c
srcs-los-y += src/brdMv0198/brdMv0198.c

srcs-lrt-$(CONFIG_BSP_MODEL_MV0182) = $(srcs-los-$(CONFIG_BSP_MODEL_MV0182))
srcs-lrt-$(CONFIG_BSP_MODEL_MV0212) = $(srcs-los-$(CONFIG_BSP_MODEL_MV0212))

