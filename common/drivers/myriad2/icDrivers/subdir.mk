
subdirs-los-$(CONFIG_SOC_LOS_MODE_RTEMS) += osDrivers
subdirs-lrt-$(CONFIG_SOC_LRT_MODE_RTEMS) += osDrivers

include-dirs-los-y += \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/icDrivers/include
include-dirs-lrt-y = $(include-dirs-los-y)

srcs-los-$(CONFIG_SOC_2x5x) += \
	src/DrvADV7513.c \
	src/DrvCDCEL.c \
	src/DrvMcp3424.c \
	src/DrvWm8325.c

srcs-lrt-y = $(srcs-los-y)

