
include-dirs-los-y += \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/icDrivers/osDrivers/include
include-dirs-los-$(CONFIG_SOC_2x8x) += \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/icDrivers/osDrivers/arch/ma2x8x/include

include-dirs-lrt-y = $(include-dirs-los-y)

srcs-los-$(CONFIG_SOC_2x5x) += \
	OsADV7513.c \
	OsBmx055.c \
	OsCDCEL.c \
	OsEEPROM.c \
	OsOv7750.c \
	OsWm8325.c \
	src/SpiFlashN25QDevice.c

srcs-los-$(CONFIG_SOC_2x8x) += \
	arch/ma2x8x/src/OsDrvAdcMcp3424.c \
	arch/ma2x8x/src/OsDrvEEPROM_24AA32A.c \
	arch/ma2x8x/src/OsDrvIOExpander_PCAL6416A.c

srcs-lrt-y = $(srcs-los-y)

