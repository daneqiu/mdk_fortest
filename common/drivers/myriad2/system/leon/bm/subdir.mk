#

srcs-los-$(CONFIG_TARGET_SOC_MA2150) += arch/ma2x5x/src/SystemInit.c
srcs-lrt-$(CONFIG_TARGET_SOC_MA2150) += arch/ma2x5x/src/SystemInit.c

srcs-los-$(CONFIG_TARGET_SOC_MA2480) += arch/ma2x8x/src/SystemInit.c
srcs-lrt-$(CONFIG_TARGET_SOC_MA2480) += arch/ma2x8x/src/SystemInit.c

subdirs-los-$(CONFIG_SOC_LOS_MODE_BM) += asm
subdirs-lrt-$(CONFIG_SOC_LRT_MODE_BM) += asm

