################################################################################
#
# Setup MDK Targets
#
# Created: 07/11/2014
#
# Note:
# * setup target is using only getSupportedToolChains for now. Its current
# purpose is to allow Jenkins to grab Tools in an elegant way while automating
# regression testing. It may be extended to do other MDK setup also.
#
# Creator: Catalin Curcanu (catalin.curcanu@movidius.com)  
#
################################################################################

MDK_PROJ_STRING ?= MDK_FALSE
MV_COMMON_BASE = .

include generic.mk

setup: getSupportedToolChains setGitHooks
