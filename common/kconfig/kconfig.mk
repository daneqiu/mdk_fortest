
# TODO add MDK version information into the configuration files


define KCONF_WARNING

			$(t_red)WARNING!$(col_rst)

	The MDK uses kconfig frontends with custom patches.
	You do not seem to have these installed.
  You can download kconfig-frontends from here
	https://github.com/movidius/kconfig-frontends

endef


HAVE_KCONFIG=$(shell which kconfig-conf 2>/dev/null || echo no)

ifeq ($(HAVE_KCONFIG),no)
$(info $(KCONF_WARNING))
$(error Sorry)
endif


# TODO check here for the presence of other required tools if necessary
# However, keep in mind that the check will be done with each MDK build system
# invocation, so perhaps we'll only state the needed GNU tools in the doc

Kconfig = $(TMPDIR)/kcnf

define frontend_template
.ONESHELL:
$(1): collectkcnf
	$$(ECHO)kconfig-$(2) $(Kconfig)
ifneq "" "$(CONFIG_FILE)"
	$$(ECHO)if [[ -f .config ]] ; then
		$$(SILENT_INFO)Updating user specified file $(CONFIG_FILE)
		$$(ECHO)cp .config $(CONFIG_FILE)
	fi
endif
	$$(ECHO)echo "MDK configuration is now complete."
	$$(ECHO)echo "Run 'make all' to build everything now"
endef

$(eval $(call frontend_template,menuconfig,mconf))
$(eval $(call frontend_template,gconfig,gconf))
$(eval $(call frontend_template,qconfig,qconf))
$(eval $(call frontend_template,nconfig,nconf))

CONFIG_GOALS = menuconfig gconfig qconfig nconfig olddefconfig oldconfig config defconfig alldefconfig

$(APPDIR)/include/config/auto.conf: $(TMPDIR)/.configured
	$(ECHO)echo > /dev/null

$(APPDIR)/include/generated/autoconf.h: $(TMPDIR)/.configured
	$(ECHO)echo > /dev/null


config: collectkcnf
	$(ECHO)kconfig-conf --oldaskconfig $(Kconfig)
	$(ECHO)popd

oldconfig: collectkcnf
	$(ECHO)kconfig-conf --$@ $(Kconfig)

# run this target to get the application's existing .config file updated with
# the new configuration values set to their default values
olddefconfig: collectkcnf
	$(ECHO)kconfig-conf --$@ $(Kconfig)

alldefconfig: collectkcnf
	$(ECHO)kconfig-conf --$@ $(Kconfig)

defconfig: collectkcnf
	$(ECHO)kconfig-conf --$@ $(Kconfig)

$(TMPDIR)/.configured: .config collectkcnf
	$(SILENT_INFO) "Config changed, running silentoldconfig"
	$(ECHO)$(MAKE) -f $(MDK_INSTALL_DIR_ABS)/make/mdk-collect.mk silentoldconfig
	$(ECHO)touch $(@)

silentoldconfig: collectkcnf
	$(ECHO)mkdir -p include/generated
	$(ECHO)mkdir -p include/config
	$(ECHO)kconfig-conf --$@ $(Kconfig)

# $(APPDIR)/.config :
# 	$(SILENT_ERROR) "Please run 'make menuconfig' first"
# 	@exit 1

