# MDK main make file
# This file should be included in any MDK-based application
#
# The MDK_INSTALL_DIR must be known before including this file. That variable
# must point to the location of this file and can be relative or absolute

# FIXME make -j do not correctly work FTM

ifeq (,$(MDK_INSTALL_DIR))
$(error Please define MDK_INSTALL_DIR before including this file. Stop.)
endif

.PHONY: all
.DEFAULT_GOAL=all
all:

# let the user specify the name of the configuration from/to where get/put the
# configuration values
ifdef CONFIG_FILE

$(info Replacing .config with $(CONFIG_FILE) as specified by the user)
$(shell cp $(CONFIG_FILE) .config)

endif

# this is the only directory that could be defined in this file. all the rest
# of the directories MUST be defined into dirs.mk, below
APPDIR ?= $(abspath .)
export APPDIR
# include settings generated by `make menuconfig`
-include $(APPDIR)/.config

include $(MDK_INSTALL_DIR)/make/dirs.mk
include $(MDK_INSTALL_DIR)/make/lib.mk

# cancel all the implicit build-ins
.SUFFIXES:

ifeq ($(CONFIG_BUILD_VERBOSE),y)
$(info MDK_INSTALL_DIR_ABS=$(MDK_INSTALL_DIR_ABS))
endif

# don't make build target .PHONY as it'll no longer build for some obscure
# reason
build: collect

all: build
	@echo "Make all"
	@echo "" > /dev/null


# use this make target to tell if your application uses the next gen build
# system and so identify the already migrated applications
.PHONY: isNextGen
isNextGen:
	@echo This make file uses next gen build system

%config:
	$(ECHO)$(MAKE) $(MAKE_TRACE) -f $(MDK_INSTALL_DIR_ABS)/make/mdk-collect.mk $@

# catch-all target that invokes the makefiles that implement the target
# requested by the user

# FIXME pervert user may specify several goals, so figure-out how to treat
# them later
-include $(MDK_INSTALL_DIR_ABS)/make/mdk-$(firstword $(MAKECMDGOALS)).mk

# redirect some targets to the makefiles implementing them
FIRST_GOAL=$(firstword $(MAKECMDGOALS))
MDK_MAKEFILE?=$(if $(filter report,$(FIRST_GOAL)),mdk-build.mk,$(if $(filter load,$(FIRST_GOAL)),mdk-run.mk,$(if $(filter start_server,$(FIRST_GOAL)),mdk-run.mk)))

# Catch-all target redirecting GNU make to the specialized makefile for that
# target
# For example:
#   $ make debug
# This will call the Makefile named mdk-debug.mk from the MDK directory
#
# If you'd rather re-use the file for a different target, then inside
# mdk-debug.mk define MDK_MAKEFILE with the precise name of that file. The
# include clause above will introduce that variable here so that Makefile will
# be called
%:
ifeq "" "$(MDK_MAKEFILE)"
	$(ECHO)$(MAKE) $(MAKE_TRACE) -f $(MDK_INSTALL_DIR_ABS)/make/mdk-$@.mk $@
else
	$(ECHO)$(MAKE) $(MAKE_TRACE) -f $(MDK_INSTALL_DIR_ABS)/make/$(MDK_MAKEFILE) $@
endif

