# Main MDK makefile, called during the build by build.mk

subdirs-los-y += swCommon
subdirs-los-y += drivers/myriad2
subdirs-los-$(CONFIG_SOC_LOS_MODE_BM) += libc/myriad2/leon
subdirs-los-y += tmp/components
subdirs-los-y += shared

subdirs-lrt-y += swCommon
subdirs-lrt-y += drivers/myriad2
subdirs-lrt-$(CONFIG_SOC_LRT_MODE_BM) += libc/myriad2/leon
subdirs-lrt-y += tmp/components
subdirs-lrt-y += shared

subdirs-shave-y += swCommon
subdirs-shave-y += shared
subdirs-shave-y += drivers/myriad2
subdirs-shave-y += tmp/components
subdirs-shave-y += shared

subdirs-los-y += scripts/ld

