#!/bin/bash

RTEMS_BSP="ma2100 ma2x5x ma2x8x"
MV_RTEMS_ARCH="myriad2-sparc-rtems"
CURRENT_DIR="$PWD"
TOOLS_INSTALL_DIR="$PWD/StagingArea/tools"
RTEMS_INSTALL_DIR="$PWD/StagingArea/output"
RTEMS_BUILD_DIR="$PWD/StagingArea/RTEMS/Build"
RTEMS_TESTS_DIR="$PWD/StagingArea/RTEMS/Tests"
REPOSITORIES_DIR="$PWD/StagingArea/repos"
LOGGING_DIR="$PWD/StagingArea/logs"

function find_tools {
    #Get path of gcc toolchain
    if [ -z "${MV_TOOLS_DIR}" ]; then
        MV_TOOLS_DIR=${HOME}/WORK/Tools/Releases/General
    fi

    if [ -z "${MV_TOOLS_VERSION}" ]; then
        MV_TOOLS_VERSION=`cat ../../toolssettings.mk |\
            grep "^MV_TOOLS_VERSION" |\
            awk '{print $3}'`
    fi

    tools_base="${MV_TOOLS_DIR}/${MV_TOOLS_VERSION}"

    if [ ! -d ${tools_base} ]; then
        echo "Tools do not exist at ${tools_base}";
        exit 1;
    fi

    platform='unknown'
    unamestr=`uname`
    if [[ "$unamestr" == 'Linux' ]]; then
        platform='linux64'
    else
        platform='win32'
    fi

    MV_GCC_TOOLS=`cat $tools_base/common/toolspaths.mk|grep MV_GCC_TOOLS|head -n 1|awk -F= '{print $2}'`
    gcc_version=`cat $tools_base/common/toolspaths.mk|grep GCCVERSION|head -n 1|awk -F= '{print $2}'`
    gcc_dir=$MV_GCC_TOOLS-$gcc_version

    MV_GCC_PATH=${tools_base}/$platform/$gcc_dir
}

function create_links {
    for i in `ls ${MV_GCC_PATH}/bin`; do
        j=`echo $i|sed -r "s/${MV_GCC_TOOLS}/${MV_RTEMS_ARCH}/"`
        ln -sf ${MV_GCC_PATH}/bin/$i ${TOOLS_INSTALL_DIR}/bin/$j
    done;
}

#Compile auto-tools
function build_autotools {
    pushd ${REPOSITORIES_DIR}
    if [ ! -d rtems-source-builder ]; then
        git clone https://github.com/RTEMS/rtems-source-builder.git
    fi
    popd

    pushd ${REPOSITORIES_DIR}/rtems-source-builder/rtems &&
        ../source-builder/sb-check > /dev/null &&
        ../source-builder/sb-set-builder --prefix=${TOOLS_INSTALL_DIR} 4.12/rtems-autotools 2>&1 | tee ${LOGGING_DIR}/autotools.log
    if [ ${PIPESTATUS[0]} -eq 1 ]; then
        echo "Autotools failed to setup";
        exit 1;
    fi

    echo "*******************************"
    echo "* Autotools compiled properly *"
    echo "*******************************"
    popd
}

#Compile rtems-tools
function build_rtemstools {
    pushd ${REPOSITORIES_DIR}
    if [ ! -d MovidiusRTEMS-tools ]; then
        git clone git@github.com:movidius/MovidiusRTEMS-tools.git
    fi
    popd

    pushd ${REPOSITORIES_DIR}/MovidiusRTEMS-tools &&
        ./waf distclean | tee ${LOGGING_DIR}/rtemstools.log &&
        ./waf configure --prefix=${TOOLS_INSTALL_DIR} | tee -a ${LOGGING_DIR}/rtemstools.log &&
        ./waf build install | tee -a ${LOGGING_DIR}/rtemstools.log
    if [ ${PIPESTATUS[0]} -eq 1 ]; then
        echo "RTEMS tools failed to setup";
        exit 1;
    fi

    echo "*********************************"
    echo "* RTEMS tools compiled properly *"
    echo "*********************************"
    popd
}

#Compile RTEMS
function build_rtems {
    pushd ${REPOSITORIES_DIR}
    if [ ! -d MovidiusRTEMS ]; then
        git clone git@github.com:movidius/MovidiusRTEMS.git
    fi
    popd

    pushd ${REPOSITORIES_DIR}/MovidiusRTEMS

    if [ ! -f cpukit/Makefile.in ]; then
        ./bootstrap -c;
        ./bootstrap -p;
        ./bootstrap;
    fi
    popd

    for i in $RTEMS_BSP; do
        if [ ! -d ${RTEMS_BUILD_DIR}/$i ]; then
            mkdir -p ${RTEMS_BUILD_DIR}/$i
            pushd ${RTEMS_BUILD_DIR}/$i
            param="--prefix=${RTEMS_INSTALL_DIR} --target=${MV_RTEMS_ARCH} --enable-drvmgr=no --enable-posix --enable-cxx --disable-tests"
            if [ "$i" == "ma2x8x" ]; then
                param="$param --disable-networking"
            else
                param="$param --enable-networking"
            fi
            ${REPOSITORIES_DIR}/MovidiusRTEMS/configure ${param}
        else
            pushd ${RTEMS_BUILD_DIR}/$i
        fi
        make all install RTEMS_BSP=$i
        if [ $? -eq 1 ]; then
            echo "RTEMS for $i failed to setup";
            exit 1;
        fi
        popd
    done

    echo "***************************"
    echo "* RTEMS compiled properly *"
    echo "***************************"
}

#Compile RTEMS-libbsd
function build_rtems_libbsd {
    pushd ${REPOSITORIES_DIR}
    if [ ! -d MovidiusRTEMS-libbsd ]; then
        git clone git@github.com:movidius/MovidiusRTEMS-libbsd.git
        pushd MovidiusRTEMS-libbsd
        git submodule init
        git submodule update rtems_waf
        popd
    fi
    popd

    tools_waf=${REPOSITORIES_DIR}/MovidiusRTEMS-tools/waf
    if [ ! -e $tools_waf ]; then
        echo "Couldn't find tools waf binary."
        exit 1
    fi

    # Remove last element of arch tuple.
    # eg. myriad2-sparc-rtems -> myriad2-sparc
    libbsd_arch=${MV_RTEMS_ARCH%-*}

    for i in $RTEMS_BSP; do
        if [ "$i" == "ma2x8x" ]; then
            pushd ${REPOSITORIES_DIR}/MovidiusRTEMS-libbsd

            $tools_waf distclean | tee ${LOGGING_DIR}/rtems_libbsd.log
            if [ ${PIPESTATUS[0]} -eq 1 ]; then
                echo "RTEMS libbsd distclean failed";
                exit 1;
            fi

            #############################
            # <workarounds> To be removed.
            #############################
            # These CLAGS are workarounds until RTEMS (x1)/toolchain (x5) issues resolved
            LIBBSD_CFLAGS="-D__NULLABILITY_PRAGMA_PUSH=\
                -D__NULLABILITY_PRAGMA_POP=\
                -D_Nonnull=\
                -DTAILQ_FOREACH_FROM(var,head,field)=for((var)=((var)?(var):TAILQ_FIRST((head)));(var);(var)=TAILQ_NEXT((var),field))\
                -DEVFILT_EMPTY=-13\
                -Dreallocarray(optr,nmemb,size)=realloc(optr,size*nmemb)"
            export CFLAGS="$LIBBSD_CFLAGS"

            # This copy is a workaround until RTEMS is updated
            cp ${REPOSITORIES_DIR}/MovidiusRTEMS/c/src/lib/libbsp/shared/include/fdt.h \
               ${RTEMS_INSTALL_DIR}/${MV_RTEMS_ARCH}/${i}/lib/include/bsp/fdt.h
            #############################
            # </workarounds>
            #############################

            $tools_waf configure --prefix=${RTEMS_INSTALL_DIR} \
                --rtems-tools=${TOOLS_INSTALL_DIR} \
                --rtems-bsps=${libbsd_arch}/${i} \
                --rtems-version="" | tee -a ${LOGGING_DIR}/rtems_libbsd.log
            if [ ${PIPESTATUS[0]} -eq 1 ]; then
                echo "RTEMS libbsd configure failed";
                exit 1;
            fi

            $tools_waf | tee -a ${LOGGING_DIR}/rtems_libbsd.log
            if [ ${PIPESTATUS[0]} -eq 1 ]; then
                echo "RTEMS libbsd build failed";
                exit 1;
            fi

            $tools_waf install | tee -a ${LOGGING_DIR}/rtems_libbsd.log
            if [ ${PIPESTATUS[0]} -eq 1 ]; then
                echo "RTEMS libbsd install failed";
                exit 1;
            fi

            unset CFLAGS
            popd
        fi
    done

    echo "*********************************"
    echo "* RTEMS libbsd compiled properly *"
    echo "*********************************"
}

#Build RTEMS tests
function build_rtems_tests {
    pushd ${REPOSITORIES_DIR}
    if [ ! -d MovidiusRTEMS ]; then
        echo "RTEMS repo doesn't exist";
        exit 1;
    fi
    popd

    pushd ${REPOSITORIES_DIR}/MovidiusRTEMS

    if [ ! -f cpukit/Makefile.in ]; then
        echo "RTEMS repo isn't bootstrapped";
        exit 1;
    fi
    popd

    for i in $RTEMS_BSP; do
        if [ ! -d ${RTEMS_BUILD_DIR}/$i ]; then
            echo "RTEMS build folder doesn't exist for $i";
            exit 1;
        fi
    done

    for i in $RTEMS_BSP; do
        if [ ! -d ${RTEMS_TESTS_DIR}/$i ]; then
            mkdir -p ${RTEMS_TESTS_DIR}/$i
            pushd ${RTEMS_TESTS_DIR}/$i
            export CFLAGS=`cat ${RTEMS_BUILD_DIR}/$i/${MV_RTEMS_ARCH}/c/$i/make/target.cfg | grep ^CFLAGS |cut -d "=" -f 2-`
            param="--enable-drvmgr=no --enable-posix --enable-cxx --enable-tests --cache-file=/dev/null \
                --host=${MV_RTEMS_ARCH} --target=${MV_RTEMS_ARCH} RTEMS_BSP=$i                          \
                --enable-rtems-root=${RTEMS_BUILD_DIR}/$i/${MV_RTEMS_ARCH}/c/$i/                        \
                --enable-project-root=${RTEMS_BUILD_DIR}/$i/${MV_RTEMS_ARCH}/$i/                        \
                --with-project-top=${RTEMS_BUILD_DIR}/$i/${MV_RTEMS_ARCH}/c/"
            if [ "$i" == "ma2x8x" ]; then
                param="--disable-networking $param"
            else
                param="--enable-networking $param"
            fi

            ${REPOSITORIES_DIR}/MovidiusRTEMS/testsuites/configure ${param}

        else
            pushd ${RTEMS_TESTS_DIR}/$i
        fi
        make all RTEMS_BSP=$i
        if [ $? -eq 1 ]; then
            echo "RTEMS tests for $i failed to setup";
            exit 1;
        fi

        unset CFLAGS;
        popd
    done

    echo "*********************************"
    echo "* RTEMS tests compiled properly *"
    echo "********************************"
}

mkdir -p "${RTEMS_INSTALL_DIR}"
mkdir -p "${TOOLS_INSTALL_DIR}/bin"
mkdir -p "${REPOSITORIES_DIR}"
mkdir -p "${LOGGING_DIR}"

find_tools;
create_links;

export PATH=${TOOLS_INSTALL_DIR}/bin:${PATH}

if [ -f ${LOGGING_DIR}/autotools_complete ]; then
    echo "Found Autotools; skipping..."
else
    build_autotools;
    touch ${LOGGING_DIR}/autotools_complete
fi

if [ -f ${LOGGING_DIR}/rtemstools_complete ]; then
    echo "Found Autotools; skipping..."
else
    build_rtemstools;
    touch ${LOGGING_DIR}/rtemstools_complete
fi

if [ -f ${LOGGING_DIR}/rtems_complete ]; then
    echo "Found RTEMS; skipping..."
else
    build_rtems;
    touch ${LOGGING_DIR}/rtems_complete
fi

if [ -f ${LOGGING_DIR}/rtems_libbsd_complete ]; then
    echo "Found RTEMS-libbsd; skipping..."
else
    build_rtems_libbsd;
    touch ${LOGGING_DIR}/rtems_libbsd_complete
fi

#if [ -f ${LOGGING_DIR}/rtems_tests_complete ]; then
#    echo "Found RTEMS TESTS; skipping..."
#else
#    build_rtems_tests;
#    touch ${LOGGING_DIR}/rtems_tests_complete
#fi
