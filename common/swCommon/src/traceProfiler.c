// why is this file here and not in common/components/profile:
// right now is not easy to provide dummy stubs when a component
// is not enabled.
#include <stdarg.h>
#include <stdint.h>
#include <dbgLogEvents.h> // Event_t
#ifdef TRACE_PROFILER_ENABLED
#include <string.h> // strlen

#include <dbgTracerApi.h>

//TODO: all functions should be __attribute__((no_instrument_function))

// TODO: merge with the one from functionProfiler.h and compute delta between timers
#include <DrvRegUtils.h>
#include <registersMyriad.h>
#include <swcWhoAmI.h>
//#ifdef LEON_RT
//#define TIMER_ADDR TIM1_BASE_ADR
//#else
#define TIMER_ADDR TIM0_BASE_ADR
//#endif

#ifdef MA2X8X
#define FP_TIME_READ() ({ \
    uint64_t ___low  = (uint64_t)GET_REG_WORD_VAL(TIMER_ADDR + TIM_FREE_CNT0_OFFSET); \
    uint64_t ___high  = (uint64_t)GET_REG_WORD_VAL(TIMER_ADDR + TIM_FREE_CNT1_OFFSET); \
    u64 ___full = (___high  << 32) | ___low; \
    ___full; \
})
#else
#define FP_TIME_READ() ({ \
    uint64_t ___high  = (uint64_t)GET_REG_WORD_VAL(TIMER_ADDR + TIM_FREE_CNT1_OFFSET); \
    uint64_t ___low  = (uint64_t)GET_REG_WORD_VAL(TIMER_ADDR + TIM_FREE_CNT0_OFFSET); \
    u64 ___full = (___high  << 32) | ___low; \
    ___full; \
})
#endif

typedef struct  __attribute__ ((packed)) ProfileEntry_t {
    // if eventId is -1, then is variable length
    // for variable length, the data is the payload, and the following 'data' bytes
    // from the following records represent the message
    uint32_t eventId;
    uint32_t data;
    uint32_t caller;
    uint64_t timestamp; // TODO: use delta
} ProfileEntry_t;
#define PROFILE_ENTRY_DEFINED
// IMPORTANT: ProfileEntry_t must be defined before profile.h is included
#include "profile.h"

#include <stdio.h>

static ProfileBufferEntry_t tProfile = {NULL, NULL, NULL};
static uint32_t tracingLevel = 0;

// TODO: disable Wframe-address warning
__attribute__((no_instrument_function))
void dbgLogEvent(Event_t eventId, uint32_t data, uint32_t eventLevel)
{
    if (tProfile.head && eventLevel <= tracingLevel) {
        profilePushEntry(tProfile, eventId, data,
            (uint32_t)__builtin_extract_return_addr(__builtin_return_address(0)),
            FP_TIME_READ())
    }
}

// TODO: add eventLevel exactly as in the original printk
__attribute__((no_instrument_function))
void dbgLogMessage(const char* fmt, ...)
{
    va_list args;
    char* msg = NULL;
    size_t msgLen = 0;

    if (!tProfile.head || !tracingLevel) return;
    if (!fmt) return;

    va_start(args, fmt);
    msgLen = vsnprintf (0, 0, fmt, args);
    msgLen++;
    msg = __builtin_alloca(msgLen);
    vsnprintf (msg, msgLen, fmt, args);
    va_end(args);

    // variable length
    profilePushEntry(tProfile, (Event_t)-1, (uint32_t)msgLen,
        (uint32_t)__builtin_extract_return_addr(__builtin_return_address(0)),
        FP_TIME_READ())
    // compute mesage size that should be multiple of ProfileEntry_t
    msgLen = ((msgLen + sizeof(ProfileEntry_t) - 1) / sizeof(ProfileEntry_t)) *
        sizeof(ProfileEntry_t);
    while (msgLen) {
        profilePushEntry(tProfile,
            (uint32_t) ((uint32_t)msg[0] | (uint32_t)msg[1] << 8 | (uint32_t)msg[2] << 16 | (uint32_t)msg[3] << 24),
            (uint32_t) ((uint32_t)msg[4] | (uint32_t)msg[5] << 8 | (uint32_t)msg[6] << 16 | (uint32_t)msg[7] << 24),
            (uint32_t) ((uint32_t)msg[8] | (uint32_t)msg[9] << 8 | (uint32_t)msg[10] << 16 | (uint32_t)msg[11] << 24),
            (uint64_t) ((uint64_t)msg[12] | (uint64_t)msg[13] << 8 | (uint64_t)msg[14] << 16 | (uint64_t)msg[15] << 24 | (uint64_t)msg[16] << 32 | (uint64_t)msg[17] << 40 | (uint64_t)msg[18] << 48 | (uint64_t)msg[19] << 56)
            );
        msgLen -= sizeof(ProfileEntry_t);
        msg += sizeof(ProfileEntry_t);
    }
}

// TODO: add this in the .h with events
#define EVT_PROBE (9999u)
// TODO: inline or macro. should be as fast as possible
__attribute__((no_instrument_function))
void dbgInsertProbe(void) {
        profilePushEntry(tProfile, EVT_PROBE, 0, 0, FP_TIME_READ());
}

__attribute__((no_instrument_function))
void initTraceProfiler(void* buffer, size_t size, uint32_t level) /*_attribute__((nonnull (1)))*/
{
    if (!buffer) level = 0;
    tracingLevel = level;
    if (level == 0) {
        // reset
        return;
    }
    if ( __profileBufferMasterRecord__.tProf ) {
        // already initialized
        // // TODO: make buffer list
    }
    tProfile.buffer = tProfile.head = (ProfileEntry_t*) buffer;
    tProfile.end = &tProfile.buffer[size / sizeof(ProfileEntry_t)];
    __profileBufferMasterRecord__.tProf = &tProfile;
    __profileBufferMasterRecord__.coreId = swcWhoAmI();

    memset(buffer, 0, size);
}

// TODO: maybe in .cmx.data section???
static char traceBuffer[TRACE_BUFFER_SIZES] __attribute__((section(".ddr.data")));

void
__attribute__((no_instrument_function))
__attribute__((constructor))
__trace_profiler_init_code__() {
    // TODO: when someone will figure out that trace profiler is working for shaves as well
    // make an init function for shaves
    initTraceProfiler(traceBuffer, TRACE_BUFFER_SIZES, DEBUG_LOG_LEVEL_HIGH);
}

#else
// dummy stubs doing nothing when trace profiling is not activated at all
void initTraceProfiler(void* buffer __attribute__((unused)), int size __attribute__((unused)), int level __attribute__((unused)))
{
    // do nothing
}
void dbgLogEvent(Event_t eventId __attribute__((unused)), uint32_t data __attribute__((unused)), uint32_t eventLevel __attribute__((unused)))
{
    // do nothing
}
void dbgLogMessage(const char* fmt __attribute__((unused)), ...)
{
    // do nothing
}
void dbgInsertProbe()
{
    // do nothing
}

#endif