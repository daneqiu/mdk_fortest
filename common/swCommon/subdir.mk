# SW Common makefile

include-dirs-los-y += $(MDK_INSTALL_DIR_ABS)/swCommon/include
include-dirs-lrt-y = $(include-dirs-los-y)

# MV_SOC_PLATFORM currently is myriad2
subdirs-los-y += $(MV_SOC_PLATFORM)

subdirs-los-$(CONFIG_APP_USES_SHAVEAPPS) += \
	dynamicStructure \
	shave_code/$(MV_SOC_PLATFORM)

subdirs-lrt-y += $(MV_SOC_PLATFORM)

srcs-los-y += \
	src/swcCrc.c \
	src/swcFifo.c \
	src/swcLeonMath.c \
	src/swcLeonUtils.c \
	src/swcRandom.c
srcs-lrt-y = $(srcs-los-y)

include-dirs-shave-y += \
	$(MDK_INSTALL_DIR_ABS)/swCommon/include \
	$(MDK_INSTALL_DIR_ABS)/

# TODO pcModel should be included in case of PC builds
subdirs-shave-y += \
	shave_code/$(MV_SOC_PLATFORM)


