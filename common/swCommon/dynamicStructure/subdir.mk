
COLLECT_MISC+=$(if $(objs-$(C_PROCESSOR)-y),collect-elf-dependencies)


B_COLLECTED_INCLUDES := $(addprefix -I,$(shell [[ -f $(SHAVE_INCLUDE_DIRS_LIST) ]] && cat $(SHAVE_INCLUDE_DIRS_LIST) | sort | uniq ))

#Have a special build rule for building the global array for dyncontexts as in the future we may want to use special rules here
$(BUILDDIR_TOP)/dynContextMaster.o : $(BUILDDIR_TOP)/dynContextMaster.c
	$(SILENT_INFO) "Create global dyncontext array for the current application $@"
	$(B_SILENT_CC) -o $@ -c $(B_CCOPT) $(B_COLLECTED_CCOPT) $(B_LOCAL_CCOPT) $(B_LOCAL_CPPOPT) $(B_CCOPT_ASM_COMMA) \
		$(B_INCLUDES) $(B_COLLECTED_INCLUDES) \
		-include $(APPDIR)/include/generated/autoconf.h \
		$<
	$(call LIST_TARGET_FILE)

objs-shave-y+=$(BUILDDIR_TOP)/dynContextMaster.o

