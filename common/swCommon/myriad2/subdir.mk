
# TODO FIXME remove this section alltogether when BUG24188 is fixed
# that's not a normal situation, to have swCommon depend on the BM and HGL
# layers
# START REMOVE
include-dirs-los-$(CONFIG_SOC_2x5x) += $(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/bm/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/hgl/include
include-dirs-los-$(CONFIG_SOC_2x8x)=\
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/bm/arch/ma2x8x/include

include-dirs-los-y += $(MDK_INSTALL_DIR_ABS)/swCommon/myriad2/include

include-dirs-los-$(CONFIG_TARGET_SOC_MA2150) += \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/bm/arch/ma2x5x/include \
	$(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/hgl/arch/ma2x5x/include
# END REMOVE

include-dirs-lrt-y = $(include-dirs-los-y)

srcs-los-y += \
	src/swcMemoryTransfer.c \
	src/swcShaveLoader.c \
	src/swcSliceUtils.c \
	src/swcTestUtils.c
srcs-lrt-y = $(srcs-los-y)

