///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved
///            For License Warranty see: common/license.txt
///
/// @defgroup dbgTracer Tracer
/// @defgroup dbgTracerApi Debug Tracer
/// @ingroup  dbgTracer
/// @{
/// @brief Debug Tracer module API
///
/// Header abstract API for debug trace logging
///

#ifndef DBG_TRACER_API_H
#define DBG_TRACER_API_H

#include <dbgLogEvents.h>

#include <stdint.h>
#include <stddef.h> // size_t

#define DEBUG_LOG_LEVEL_LOW 	1
#define DEBUG_LOG_LEVEL_MIDIUM 	2
#define DEBUG_LOG_LEVEL_HIGH 	3

#ifdef TRACE_PROFILER_ENABLED
#ifdef __cplusplus
extern "C" {
#endif

////////////////////////////////////////////////////////////////////////////
/// @brief initTraceProfiler - Init function
///
/// @param buffer - trace profiler buffer. If NULL it will disable logging
/// @param size - trace buffer size
/// @param level - logging level. if set to 0, then profiling will be disabled
////////////////////////////////////////////////////////////////////////////
void initTraceProfiler(void* buffer, size_t size, uint32_t level);

/// @brief Logs Event from the system by a specific logger method
///
/// Components that are going to be logged are: Timestamp, eventID and 
/// data value (if any) to a (ring) buffer
/// @param[in] eventId    - ID of the event that will be logged
/// @param[in] data       - Data field containing custom info
/// @param[in] eventLevel - Debug Level for the current eventId to determine if
///            the event would be traced or not.
/// @return void
///
void dbgLogEvent(Event_t eventId, uint32_t data, uint32_t eventLevel);

/// @brief Log a formatted string message
/// @param as in printf family
void dbgLogMessage(const char* fmt, ...);

/// @brief Insert a marker in code used to time execution
/// Time spent between two probes is collected in profilers
void dbgInsertProbe();


#ifdef __cplusplus
}
#endif

#else // TRACE_PROFILER_ENABLED

#define eat_arg(...)

#ifndef initTraceProfiler
#define initTraceProfiler(a,b,c) (void)(a);(void)(b);(void)(c)
#endif

#ifndef dbgLogEvent
#define dbgLogEvent(a,b,c) (void)(a);(void)(b);(void)(c)
#endif

#ifndef dbgLogMessage
#define dbgLogMessage(f, ...) eat_arg(f, __VA_ARGS__)
#endif

#ifndef dbgInsertProbe
#define dbgInsertProbe()
#endif

#endif // TRACE_PROFILER_ENABLED
/// @}
#endif //_DBG_TRACER_API_H_

