
include-dirs-shave-y += $(MDK_INSTALL_DIR_ABS)/swCommon/shave_code/$(MV_SOC_PLATFORM)/include
include-dirs-shave-$(CONFIG_TARGET_SOC_MA2150) += $(MDK_INSTALL_DIR_ABS)/swCommon/shave_code/$(MV_SOC_PLATFORM)/include/arch/ma2x5x
include-dirs-shave-$(CONFIG_TARGET_SOC_MA2450) += $(MDK_INSTALL_DIR_ABS)/swCommon/shave_code/$(MV_SOC_PLATFORM)/include/arch/ma2x5x
include-dirs-shave-$(CONFIG_TARGET_SOC_MA2480) += $(MDK_INSTALL_DIR_ABS)/swCommon/shave_code/$(MV_SOC_PLATFORM)/include/arch/ma2x8x

asm-include-dirs-shave-$(CONFIG_TARGET_SOC_MA2150) += $(MDK_INSTALL_DIR_ABS)/swCommon/shave_code/$(MV_SOC_PLATFORM)/include/arch/ma2x5x
asm-include-dirs-shave-$(CONFIG_TARGET_SOC_MA2450) += $(MDK_INSTALL_DIR_ABS)/swCommon/shave_code/$(MV_SOC_PLATFORM)/include/arch/ma2x5x
asm-include-dirs-shave-$(CONFIG_TARGET_SOC_MA2480) += $(MDK_INSTALL_DIR_ABS)/swCommon/shave_code/$(MV_SOC_PLATFORM)/include/arch/ma2x8x

srcs-shave-$(CONFIG_SOC_2x5x) += \
	src/swcCdmaShaveM2.c \
	asm/ma2x5x/dummy.asm \
	asm/ma2x5x/svuCommonLUTs.asm \
	asm/ma2x5x/swcSIMDUtils.asm

srcs-shave-$(CONFIG_APP_USES_SHAVEAPPS)+=\
	src/swcShaveInit.c \
	asm/$(call unquote,$(CONFIG_SOC_DIR))/swcCRT0.asm

collect-shavegroup-lib:
	$(ECHO)( flock -n 10; \
		echo -n '$(LIB)' >> $(SHAVEGROUP_LIBRARIES_LIST) ;
	) 10>$(SHAVEGROUP_LIBRARIES_LIST).lock

collect-misc-shave-y += collect-shavegroup-lib

subdir-shave-force-ld=y

