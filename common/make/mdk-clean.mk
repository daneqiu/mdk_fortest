
ifndef MDK_INSTALL_DIR_ABS
$(error This file was not launched by the main mdk.mk)
endif

include $(MDK_INSTALL_DIR_ABS)/make/build-lib.mk

.PHONY: clean
.ONESHELL:
clean:
	@echo "Removing built artefacts"
	[[ -d $(APPDIR)/include ]] && rm -rf $(APPDIR)/include || true
	[[ -d $(TMPDIR) ]] && rm -rf $(TMPDIR) || true
	if [[ -f $(CLEAN_LIST_FILE) ]]; \
		then \
			for x in $$(cat $(CLEAN_LIST_FILE) | sort | uniq); \
				do \
					if [ -f $$x ]; \
					then \
						rm $$x; \
					fi\
				done; \
			rm $(CLEAN_LIST_FILE); \
			fi

