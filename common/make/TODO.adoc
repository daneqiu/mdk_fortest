
* Implement `clean` target in a more elegant way, e.g. without simply removing the build/MV_SOC_REV dir
* Add dependencies logic on the overall Makefiles; the list of Makefiles can be built during the collect phase
* Add dependencies logic on the sources in all the makefiles
* Give user the means to specify per/file compiler options
* Add check for overflow of text in debug mode to switch to release with debug symbols

Beware the moviCompile + moviAsm combination when using CCACHE. CCACHE will
store the resulting object file according to the moviCompile timestamp. When
the moviAsm will change, the timestamps will stay unchanged and CCACHE will
wrongly hand-over a cached object file instead of invoking the new moviAsm.

* DynamicLoading: handle SHAVE groups libraries
* DynamicLoading: add a SHAVE group libraries example

