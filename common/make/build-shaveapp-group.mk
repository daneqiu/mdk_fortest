# the default group, named SHAVE_GROUP_DEFAULT, holds the list of the
# libraries collected for all the build process

# build the different SHAVE applications dyncontexts here

# TODO ShaveApp HEAPSIZE, STACKSIZE cannot be overridden by the user FTM
# This could be changed by reading these variables from the respective
# application's subdir.mk during the collect phase perhaps
define CREATE_SHAVEAPP_DYNCONTEXT

$(1)_HEAPSIZE       ?= 1024*6
$(1)_STACKSIZE      ?= 1024*6
$(1)_MainEntry      ?= Entry
$(1)_HEAP_INIT      ?= DYNCONTEXT_HEAP_INIT
$(1)_GROUP = $(call unquote,$(CONFIG_SHAVEAPP_$(1)_GROUP))

$(1)_OtherEntries   ?=
cDynContextDefs_$(1) ?= -D'APPDYNDATASECTION=$(1)appdyndata' -D'SHAVEAPPNAME="$(1)"'
cDynContextDefs_$(1) += -D'APPHEAPSIZE=($$($(1)_HEAPSIZE))'
cDynContextDefs_$(1) += -D'APPSTACKSIZE=($$($(1)_STACKSIZE))' -D'HEAP_INIT_VAL=$$($(1)_HEAP_INIT)'
cDynContextDefs_$(1) += -D'APPGROUPDYNDATASECTION=$$($(1)_GROUP)grpdyndata'
cDynContextDefs_$(1) += -D'GRPENTRY=$$($(1)_GROUP)____AllTimeEntryPoint'

# FIXME below we support only one entry point FTM (APPENTRY)
$(BUILDDIR_TOP)/$(1)_dyncontext_shave.o : $(MDK_INSTALL_DIR_ABS)/swCommon/dynamicStructure/theDynContext.c \
			$$($(1)_GROUP_DATA) \
			$(BUILDDIR_TOP)/$(1).shvXdata \
			$(BUILDDIR_TOP)/$$(call unquote,$$(CONFIG_SHAVEAPP_$(1)_GROUP)).shvZdata
	$(SILENT_INFO) "Create dyncontext for the current application $(1)"
	$(ECHO)test -d $(@D) || mkdir -p $$(@D)
	$(ECHO) $(SILENT_MVCC) $(MVCC) -c $(filter-out -S,$(MVCCOPT)) \
		$$$$([ -f "$$(SHAVE_CCOPT_LIST)" ] && cat "$(SHAVE_CCOPT_LIST)") \
		$$(addprefix -I,$$(shell [ -f "$$(SHAVE_INCLUDE_DIRS_LIST)" ] && cat "$$(SHAVE_INCLUDE_DIRS_LIST)" | sort | uniq )) \
		$$(cDynContextDefs_$(1)) \
		-D'APPDYNCONTEXTNAME=$(1)X_ModuleData' -D'APPDYNCONTEXTPRIVATENAME=$(1)X_ModuleDataPrivate'\
		-D'APPHEAPDYNCONTEXTADR=$(1)X_heap'  -D'APPGRPDATADYNCONTEXTADR=$(1)X_grpData'\
		-D'CTORSARRAY=$(1)X___init_array_start'\
		-D'INITARRAYEND=$(1)X___init_array_end'\
		-D'DTORSARRAY=$(1)X___fini_array_start'\
		-D'FINIARRAYEND=$(1)X___fini_array_end'\
		-D'APPGROUPDYNDATASECTIONSIZE=0x0$$(shell $(OBJDUMP) -xsrtd $(BUILDDIR_TOP)/$$(call unquote,$$(CONFIG_SHAVEAPP_$(1)_GROUP)).shvZdata | grep dyn.data | grep -v section | cut -d ' ' -f 9)ul' \
		-D'APPCRITCMXTEXTSECTIONSIZE=0x0$$(shell $(OBJDUMP) -xsrtd $(BUILDDIR_TOP)/$(1).shvXdata | grep dyn.textCrit | grep -v section | grep -v : | cut -d ' ' -f 5)ul' \
		-D'APPENTRY=$(1)X_$$(call unquote,$$(CONFIG_SHAVEAPP_$(1)_ENTRY_POINTS))' \
		$$< -o $$@
	@echo $$@ >> $$(CLEAN_LIST_FILE)
	@echo $$(patsubt %.o,%.d,$$@) >> $$(CLEAN_LIST_FILE)

SHAVEAPP_DYNCONTEXT_LIST+=$(BUILDDIR_TOP)/$(1)_dyncontext_shave.o

endef

$(eval $(foreach app,$(call unquote,$(CONFIG_SHAVEAPP_LIST)),$(call CREATE_SHAVEAPP_DYNCONTEXT,$(app))))

# TODO handle the other groups libraries list here

define cGroupRules_template
#Get list of symbols to mark -u based on all apps using this library group
.ONESHELL:
$(BUILDDIR_TOP)/$(1).grpsyms :
	$$(SILENT_INFO) "Creating list of symbols to be trapped from the library group $$@"
	$$(ECHO) for lib in $$$$(cat $$(BUILDDIR_TOP)/$(1)-grpsyms-dependencies | sort | uniq) ; do \
	$$(READELF) --syms --wide $$$$lib | grep -v Symbol | grep -v Vis | grep -v File | grep -v FILE | grep UND | sed 's/\s\+/ /g' | cut -d ' ' -f 9 | grep . | sort -n | uniq  >> $$(call RESOLVE_TARGET_PATH,$$@) ; \
	done
	$$(ECHO) echo ___AllTimeEntryPoint >> $$(call RESOLVE_TARGET_PATH,$$@)
	@echo $$@ >> $$(CLEAN_LIST_FILE)
	@echo $$(BUILDDIR_TOP)/$(1)-grpsyms-dependencies >> $$(CLEAN_LIST_FILE)

#Get list of all possible symbols provided by the Group
$(1)_LIBS=$$(CompilerANSILibs)
$(1)_GROUP_LIBS=$$(shell $(MDK_INSTALL_DIR_ABS)/make/list_libraries.sh $(SHAVEGROUP_LIBRARIES_LIST) $(CONFIG_BUILD_DEBUG))
$(1)_LIBS+=$$($(1)_GROUP_LIBS)
GROUP$(1)_LIBS+=$$(call unquote,$$(CONFIG_$(1)))
$(1)_FILT_LIBS=$$(filter-out --start-group --end-group,$$($(1)_LIBS))
.ONESHELL:
#Append __fini_array_start __fini_array_end to make sure they get translated even if empty
#Append __init_array_start __init_array_end to make sure they get translated even if empty
$(BUILDDIR_TOP)/$(1).grpallsyms : $$($(1)_FILT_LIBS)
	$(SILENT_INFO) "Generate list of all symbols defined by the group $$@"
	$$(ECHO) $$(READELF) --syms --wide $$($(1)_FILT_LIBS) | grep -v Symbol | grep -v Vis | grep -v File | grep -v FILE | grep -v UND | sed 's/\s\s*/ /g' | cut -d ' ' -f 9 | grep . | sort -n | uniq  > $$(call RESOLVE_TARGET_PATH,$$@)
	$$(ECHO) echo ___AllTimeEntryPoint >> $$(call RESOLVE_TARGET_PATH,$$@)
	$$(ECHO) echo __fini_array_start >> $$(call RESOLVE_TARGET_PATH,$$@)
	$$(ECHO) echo __fini_array_end >> $$(call RESOLVE_TARGET_PATH,$$@)
	$$(ECHO) echo __init_array_start >> $$(call RESOLVE_TARGET_PATH,$$@)
	$$(ECHO) echo __init_array_end >> $$(call RESOLVE_TARGET_PATH,$$@)
	@echo $$@ >> $$(CLEAN_LIST_FILE)

#Group symbols to redefine. Make sure to take out of the redefinition rules the globalTail and ResMgr
.ONESHELL:
$(BUILDDIR_TOP)/$(1).grpredefinesyms : $(BUILDDIR_TOP)/$(1).grpallsyms
	$(SILENT_INFO) "Generate redefinitions for groups $$@"
	$$(ECHO) cat $$< | sort -n | uniq  | grep -v "__SglResMgrGlobal\|__FilterResourceManagerGlobal\|__hwFilterListGlobal\|___globalTail" | sed 's/.*/& $(1)_&/g'> $$@
	@echo $$@ >> $$(CLEAN_LIST_FILE)

ifeq "y" "$(CONFIG_HAS_LRT_SRCS)"
.ONESHELL:
$(BUILDDIR_TOP)/$(1).lrtgrpsyms: $(BUILDDIR_TOP)/$(1).grpsyms
	$$(SILENT_INFO) "Generate LeonRT symbol uniquificator for group $$(notdir $$@)"
	$$(ECHO) cat $$< | sort -n | uniq | sed 's/.*/lrt_$(1)_& $(1)_&/g' > $$(call RESOLVE_TARGET_PATH,$$@)
	$$(ECHO) echo lrt_$(1)grpdyndata $(1)grpdyndata >> $$(call RESOLVE_TARGET_PATH,$$@)
	@echo $$@ >> $$(CLEAN_LIST_FILE)
else
$(BUILDDIR_TOP)/$(1).lrtgrpsyms:
	@echo "" > /dev/null
endif

.ONESHELL:
$(BUILDDIR_TOP)/$(1).lnkcmds_tmp : $(BUILDDIR_TOP)/$(1).grpsyms $(BUILDDIR_TOP)/$(1).lrtgrpsyms
	@test -d $$(@D) || mkdir -p $$(@D)
	$$(ECHO) echo $$(LdCommandLineFileHeader) --start-group $$($(1)_LIBS) --end-group > $$@
	$$(ECHO) cat $$< | sort -n | uniq | sed 's/.*/-u &/g' >> $$@
	@echo $$@ >> $$(CLEAN_LIST_FILE)

.ONESHELL:
$(BUILDDIR_TOP)/$(1).lnkcmds : $(BUILDDIR_TOP)/$(1).lnkcmds_tmp
	$$(ECHO) tr '\n' ' ' < $$< > $$@
	@echo $$@ >> $$(CLEAN_LIST_FILE)

.ONESHELL:
$(BUILDDIR_TOP)/$(1).mvlibG : $(BUILDDIR_TOP)/$(1).lnkcmds
	$$(ECHO) $$(LD) @$$< -o $$(call RESOLVE_TARGET_PATH,$$@)
	@echo $$@ >> $$(CLEAN_LIST_FILE)

#Creating library group and making sure the global ResMgr and globalTail symbols are weakened so that the Leon ones are strongest
.ONESHELL:
$(BUILDDIR_TOP)/$(1).libgroup : $(BUILDDIR_TOP)/$(1).mvlibG $(BUILDDIR_TOP)/$(1).grpredefinesyms
	$$(ECHO) $$(OBJCOPY) --prefix-alloc-sections=.shvZ.$(1). --redefine-syms=$(BUILDDIR_TOP)/$(1).grpredefinesyms -W __SglResMgrGlobal \
	-W __hwFilterListGlobal -W __FilterResourceManagerGlobal -W ___globalTail \
	$$< $$@
	@echo $$@ >> $$(CLEAN_LIST_FILE)

.ONESHELL:
$(BUILDDIR_TOP)/$(1).shvZdata : $(BUILDDIR_TOP)/$(1).mvlibG
	$$(SILENT_INFO) "Creating dynamically loadable library group data : $$(notdir $$@)"
	$$(ECHO) $$(LD) $$(LD_ENDIAN_OPT) $$(V_GRP_LDDYNOPT) $$< -o $$@
	@echo $$@ >> $$(CLEAN_LIST_FILE)

.ONESHELL:
$(BUILDDIR_TOP)/$(1)_shvZdata.o : $(BUILDDIR_TOP)/$(1).shvZdata
	$$(ECHO) $$(OBJCOPY)  -I binary --rename-section .data=.ddr.data \
	--redefine-sym  _binary_$$(subst /,_,$$(subst .,_,$$<))_start=$$(subst .shvZdata,,$$(notdir $$<))grpdyndata \
	-O elf32-littlesparc -B sparc $$< $$@
	@echo $$@ >> $$(CLEAN_LIST_FILE)

SHAVEAPP_DYNCONTEXT_LIST+=$(BUILDDIR_TOP)/$(1)_shvZdata.o

endef

define LIST_GROUP_APPS
MV_$$(call unquote,$$(CONFIG_SHAVEAPP_$(1)_GROUP))_APPS+=$(1)
endef

$(eval $(foreach app,$(call unquote,$(CONFIG_SHAVEAPP_LIST)),$(call LIST_GROUP_APPS,$(app))))

$(eval $(foreach grp,$(call unquote,$(CONFIG_SHAVEGROUP_LIST)),$(call cGroupRules_template,$(grp))))

$(BUILDDIR_TOP)/shaveXApps.symuniq: \
				$(patsubst %,$(BUILDDIR_TOP)/%.lrtappsyms,$(call unquote,$(CONFIG_SHAVEAPP_LIST))) \
        $(patsubst %,$(BUILDDIR_TOP)/%.lrtgrpsyms,$(call unquote,$(CONFIG_SHAVEGROUP_LIST)))
	$(SILENT_INFO) "Generate SHAVE master symbol uniquificator $(notdir $@)"
	$(ECHO) cat $^ > $@
	$(call LIST_TARGET_FILE)

#Rule to create the required linkerscript for building the dynamically loaded libs
DYN_SHAVEAPP_LDSCRIPT=$(BUILDDIR_TOP)/shvDynInfrastructureBase.ldscript
ifeq "" "$(strip $(CONFIG_SHAVEAPP_LIST))"
$(error CONFIG_SHAVEAPP_LIST is empty. Abort.)
endif
.ONESHELL:
$(DYN_SHAVEAPP_LDSCRIPT) :
	$(ECHO)for appName in $(call unquote,$(CONFIG_SHAVEAPP_LIST)); \
	do cat $(MDK_INSTALL_DIR_ABS)/scripts/ld/shaveDynamicLoad/shaveDynLoadTemplate_App.ldscript | sed s/ZZZZZZZZZ/"$$appName"/g | sed s/YYYYYYYYY/"$$previous"/g >> $(BUILDDIR_TOP)/shvDynInfrastructureTemp.ldscript ; \
	previous=$$file;\
	done
	$(ECHO)for grpName in $(foreach group,$(call unquote,$(call unquote,$(CONFIG_SHAVEGROUP_LIST))),$(lastword $(subst /, ,$(subst ., ,$(group))))); \
	do cat $(MV_COMMON_BASE)/scripts/ld/shaveDynamicLoad/shaveDynLoadTemplate_Group.ldscript | sed s/ZZZZZZZZZ/"$$grpName"/g | sed s/YYYYYYYYY/"$$previous"/g >> $(BUILDDIR_TOP)/shvDynInfrastructureTemp.ldscript ; \
	previous=$$file;\
	done
	$(ECHO) cat $(BUILDDIR_TOP)/shvDynInfrastructureTemp.ldscript | sed s/"LOADADDR (S.lrt.shvCOM.cmx.data.Shave) + SIZEOF(S.lrt.shvCOM.cmx.data.Shave)"/"0xE0000000"/ > $@
	@rm -rf $(BUILDDIR_TOP)/shvDynInfrastructureTemp.ldscript
	$(call LIST_TARGET_FILE)

#generate a master C file holding the list of all applications in the project. Needed to get state of project.
# FIXME Makefile below should be the reald main application's Makefile and
# that one could have a slightly different name. Figure-out either if this is
# still needed or get here the right name
$(BUILDDIR_TOP)/dynContextMaster.c :
	@echo "//  Generated file with all application content data" > $@
	@echo "#include \"theDynContext.h\"" >> $@
	@echo "" >> $@
	@echo "#define ___APPS_DETECTED " $(words $(CONFIG_SHAVEAPP_LIST))  >> $@
	@echo ""  >> $@
	@echo -e "$(foreach app,$(call unquote,$(CONFIG_SHAVEAPP_LIST)),extern DynamicContext_t $(lastword $(subst /, ,$(app)))X_ModuleData;\n)" >> $@
	@echo ""  >>  $@
	@echo "DynamicContextInfo_t GlobalArray[___APPS_DETECTED]=" >> $@
	@echo "{"  >> $@
	@echo -e "   $(foreach app,$(call unquote,$(CONFIG_SHAVEAPP_LIST)), "{" &$(lastword $(subst /, ,$(app)))X_ModuleData, "\""$(lastword $(subst /, ,$(app)))"\"" "},"\n)" >> $@
	@echo "};"  >> $@
	@echo ""  >> $@
	@echo "DynamicContextGlobal_t GlobalContextData="  >> $@
	@echo "{"   >> $@
	@echo "     ___APPS_DETECTED, (DynamicContextInfo_t*)GlobalArray"  >> $@
	@echo "};"  >> $@
	@echo "" >> $@
	$(call LIST_TARGET_FILE)

.ONESHELL:
$(BUILDDIR_TOP)/dynContextMaster.o : $(BUILDDIR_TOP)/dynContextMaster.c
	@echo "Create global dyncontext array for the current application $@"
	$(ECHO) $(CC) -c $(CONLY_OPT) $(CCOPT) \
		$([ -f "$(LOS_CCOPT_LIST)" ] && cat "$(LOS_CCOPT_LIST)") \
		$(addprefix -I,$(shell [ -f "$(LOS_INCLUDE_DIRS_LIST)" ] && cat "$(LOS_INCLUDE_DIRS_LIST)" | sort | uniq )) \
		$< -o $@

build-shaveapp-group: $(APP_DEFAULT_SHAVE_MODULE_LIB) collect-shave \
						$(DYN_SHAVEAPP_LDSCRIPT) \
			$(foreach grp,$(call unquote,$(CONFIG_SHAVEGROUP_LIST)),$(BUILDDIR_TOP)/$(grp)_shvZdata.o $(BUILDDIR_TOP)/$(grp).libgroup) \
						$(SHAVEAPP_DYNCONTEXT_LIST) \
      $(BUILDDIR_TOP)/dynContextMaster.o
	@echo "" > /dev/null

