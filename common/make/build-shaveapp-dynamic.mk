
include $(MDK_INSTALL_DIR_ABS)/make/build-shaveapp-group.mk

#Get the Application data
.ONESHELL:
$(BUILDDIR_TOP)/$(shaveapp-id).shvXdata : $(LIB)
	$(SILENT_INFO) "Creating dynamically loadable shave application data : $(notdir $@)"
	$(SILENT_LD)$(LD) $(LD_ENDIAN_OPT) $(V_APP_LDDYNOPT) $< -o $@
	$(call LIST_TARGET_FILE)

.ONESHELL:
$(BUILDDIR_TOP)/$(shaveapp-id)_shvXdata.o : $(BUILDDIR_TOP)/$(shaveapp-id).shvXdata
	$(SILENT_OBJCOPY)$(OBJCOPY) -I binary --rename-section .data=.ddr.data \
	--redefine-sym  _binary_$(subst /,_,$(subst .,_,$<))_start=$(shaveapp-id)appdyndata \
	-O elf32-littlesparc -B sparc $< $@
	$(call LIST_TARGET_FILE)

.ONESHELL:
$(BUILDDIR_TOP)/$(shaveapp-id).shvXdatacomplete : $(LIB)
	$(SILENT_INFO) "Creating windowed library for symbol extraction of loadable shave application $(notdir $@) from $<"
	$(SILENT_LD)$(LD) $(LD_ENDIAN_OPT) $(V_APP_LDSYNOPT) $< -o $@
	$(call LIST_TARGET_FILE)

.ONESHELL:
$(BUILDDIR_TOP)/$(shaveapp-id)_shvXdata_sym.o : $(BUILDDIR_TOP)/$(shaveapp-id).shvXdatacomplete
	$(SILENT_INFO) "Creating symbols file for shave app dynamic loading section $(notdir $@)"
	$(SILENT_OBJCOPY)$(OBJCOPY) --prefix-symbols=$(shaveapp-id)_ --extract-symbol $< $@
	$(call LIST_TARGET_FILE)

# FIXME the includes in the bm directory below are for the file DrvRegUtilsDefines.h
# FIXME are these files correctly placed there as soon as it's needed by shave
# compilation?
B_INCLUDES+=\
	  -I $(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/bm/include

B_INCLUDES_2x5x=-I $(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/bm/arch/ma2x5x/include
B_INCLUDES_2x8x=-I $(MDK_INSTALL_DIR_ABS)/drivers/myriad2/socDrivers/leon/bm/arch/ma2x8x/include

B_INCLUDES_$(CONFIG_TARGET_SOC_MA2150)=$(B_INCLUDES_2x5x)
B_INCLUDES_$(CONFIG_TARGET_SOC_MA2450)=$(B_INCLUDES_2x5x)
B_INCLUDES_$(CONFIG_TARGET_SOC_MA2480)=$(B_INCLUDES_2x8x)

B_INCLUDES+=$(B_INCLUDES_y)


#$(foreach app,$(shaveXApps),$(eval $(call DYNCONTEXT_template,$(lastword $(subst /, ,$(app))),$(app))))

.ONESHELL:
$(BUILDDIR)/$(shaveapp-id).appsyms: $(LIB)
	$(SILENT_READELF) $(READELF) --syms --wide $< | grep -v Symbol | grep -v Vis | grep -v File | grep -v FILE | grep -v UND | sed 's/\s\s*/ /g' | cut -d ' ' -f 9 | grep . | sort -n | uniq  >> $(call RESOLVE_TARGET_PATH,$@)
	$(call LIST_TARGET_FILE)

lrt-appsyms-$(CONFIG_HAS_LRT_SRCS)=$(BUILDDIR_TOP)/$(shaveapp-id).lrtappsyms
# this file will be used in build.mk to delete the processor prefix from some
# shaveapp-related symbols
.ONESHELL:
$(BUILDDIR_TOP)/$(shaveapp-id).lrtappsyms: $(BUILDDIR)/$(shaveapp-id).appsyms
	$(SILENT_INFO) "Generate LeonRT symbol uniquificator for application $(notdir $@)"
	$(ECHO) cat $< | sort -n | uniq | sed 's/.*/lrt_$(shaveapp-id)X_& $(shaveapp-id)X_&/g' > $(call RESOLVE_TARGET_PATH,$@)
	$(ECHO) echo lrt_$(shaveapp-id)appdyndata $(shaveapp-id)appdyndata >> $(call RESOLVE_TARGET_PATH,$@)
	$(ECHO) echo lrt_$(shaveapp-id)grpdyndata $(shaveapp-id)grpdyndata >> $(call RESOLVE_TARGET_PATH,$@)
	$(ECHO) echo lrt_$(shaveapp-id)X_ModuleData $(shaveapp-id)X_ModuleData >> $(call RESOLVE_TARGET_PATH,$@)
	$(call LIST_TARGET_FILE)

$(BUILDDIR)/$(shaveapp-id).appredefinesyms: $(BUILDDIR)/$(shaveapp-id).appsyms $(lrt-appsyms-y)
	$(ECHO)cat $< | sort -n | uniq | grep -v "__SglResMgrGlobal\|__hwFilterListGlobal\|___globalTail" | sed 's/.*/& $(shaveapp-id)X_&/g'> $(call RESOLVE_TARGET_PATH,$@)
	$(call LIST_TARGET_FILE)

#Create a shvXlib*redefsyms file which is a collection of appredefine syms and grpredefinesyms
#To take note: the main purpose this is a separate rule is to handle linkonce symbols. These
#are for example static functions declared in headers: they are allowed but when header is included in different
#C files, they will be linked twice. In C this may look slightly odd but in C++ this is actually very much used
#because many libraries wil declare template classes in headers for example
#So what we are doing is: we are keeping the local application present linkonce data.
#"Why not the library one?" -> one might ask. Because the library one is a mirror of whoever
#built the libraries, and they may have build them with flags different than your own apps. So:
#staying with the application one. Note: this doesn't affect cases where the library itself has a need
#to call it's own linkonce cases because this does not affect libgroup creation
SHAVEAPP_GROUP=$(call unquote,$(CONFIG_SHAVEAPP_$(shaveapp-id)_GROUP))
.ONESHELL:
$(BUILDDIR)/$(shaveapp-id).$(SHVXLIB_SUFFIX)redefsyms: $(BUILDDIR)/$(shaveapp-id).appredefinesyms \
      $(BUILDDIR_TOP)/$(SHAVEAPP_GROUP).grpredefinesyms
	$(ECHO)cat $(BUILDDIR_TOP)/$(SHAVEAPP_GROUP).grpredefinesyms $(BUILDDIR)/$(shaveapp-id).appredefinesyms | \
	sed 's/$(shaveapp-id)/AA$(shaveapp-id)/g' \
	| sed 's/$(SHAVEAPP_GROUP)/ZZ$(SHAVEAPP_GROUP)/g' \
	| sort | rev | uniq -f 1 | rev | sed 's/ZZ$(SHAVEAPP_GROUP)/$(SHAVEAPP_GROUP)/g' \
	| sed 's/AA$(shaveapp-id)/$(shaveapp-id)/g' > $(call RESOLVE_TARGET_PATH,$@)
	$(call LIST_TARGET_FILE)

#shvXlib creation by using the redefine syms list for the current application and the ones from the group. Make sure the GlobalTail and SglResMgrGlobal are weakened
.ONESHELL:
$(SHVXLIB): $(BUILDDIR)/$(shaveapp-id).$(SHVXLIB_SUFFIX)redefsyms $(LIB)
	$(SILENT_INFO) "Moving SHAVE application to its own group application type $$@"
	$(SILENT_OBJCOPY) $(OBJCOPY) --prefix-alloc-sections=.shvX.$(shaveapp-id). \
	--redefine-syms=$(call RESOLVE_TARGET_PATH,$<) \
	-W __SglResMgrGlobal -W ___globalTail -W __hwFilterListGlobal \
	$(LIB) $(call RESOLVE_TARGET_PATH,$@)
	$(call LIST_TARGET_FILE)
