#
# find out which groups we should consider by looking to the used applications
# for the used groups, collect the needed files
# the default group will not be built unless at least one application's using it
#

# FIXME all the contents of this file should only be executed once, consider
# moving this upper into build-shaveapp-group.mk
define CONSIDER_SHAVEAPP_GROUP-y=
$$(eval USED_GROUPS_LIST+=$$(call unquote,$$(CONFIG_SHAVEAPP_$(1)_GROUP)))
endef

$(eval $(foreach app,$(call unquote, $(CONFIG_SHAVEAPP_LIST)),$(call CONSIDER_SHAVEAPP_GROUP-$(CONFIG_USE_SHAVEAPP_$(app)),$(app)) ))

# we now have the list of the currently used groups, following the user's
# configuration
$(call VINFO,USED_GROUPS_LIST=$(USED_GROUPS_LIST))

# TODO here we could expand the build system with features letting users
# specify group-specific options into the shaveapp.mk makefile
# For example, we could define per-group linker options like this, in that
# shaveapp.mk file:
#
# group-SHAVE_GROUP_DEFAULT-ldopt = <some linker options here>
#
# But there's no request for such a feature, FTM.

SHAVEAPP_GROUP = $(call unquote,$(CONFIG_SHAVEAPP_$(shaveapp-id)_GROUP))
ifeq "" "$(SHAVEAPP_GROUP)"
$(error "The current shaveapp named '$(shaveapp-id)' configuration does not specify a shavegroup")
endif

COLLECT_MISC+=collect-elf-dependencies

.PHONY:collect-elf-dependencies
collect-elf-dependencies:
	$(ECHO)( flock -n 9 ; \
		echo "$(patsubst %,$(BUILDDIR_TOP)/%.libgroup,$(call unquote,$(CONFIG_SHAVEGROUP_LIST))) " >> $(SHAVEAPP_ELF_DEPENDENCIES_LIST) ; \
		echo "$(BUILDDIR)/$(shaveapp-id).shvXlib$(SHAVEAPP_GROUP)" >> $(SHAVEAPP_ELF_DEPENDENCIES_LIST)
		echo "$(BUILDDIR_TOP)/$(shaveapp-id)_shvXdata_sym.o" >> $(SHAVEAPP_ELF_DEPENDENCIES_LIST)
		echo "$(BUILDDIR_TOP)/$(shaveapp-id)_shvXdata.o" >> $(SHAVEAPP_ELF_DEPENDENCIES_LIST)
		echo "$(BUILDDIR_TOP)/shvDynInfrastructureBase.ldscript" >> $(SHAVEAPP_ELF_DEPENDENCIES_LIST)
	) 9>$(SHAVEAPP_ELF_DEPENDENCIES_LIST).lock
	@echo $(SHAVEAPP_ELF_DEPENDENCIES_LIST) >> $(CLEAN_LIST_FILE)
	@echo $(SHAVEAPP_ELF_DEPENDENCIES_LIST).lock >> $(CLEAN_LIST_FILE)

