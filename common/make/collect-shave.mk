ifndef MDK_INSTALL_DIR_ABS
$(error MDK_INSTALL_DIR_ABS is not defined. This file should be invoked by the main build.mk file)
endif

include $(APPDIR)/.config
include $(MDK_INSTALL_DIR_ABS)/make/lib.mk
include $(MDK_INSTALL_DIR_ABS)/make/build-lib.mk
include $(MDK_INSTALL_DIR_ABS)/toolssettings.mk

#========================================
# collect for processor LOS
C_PROCESSOR=shave

#========================================

include $(MDK_INSTALL_DIR_ABS)/make/collect-processor.mk

