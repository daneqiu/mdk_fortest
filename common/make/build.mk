APP_LOS_LIB = $(BUILDDIR_TOP)/app-los.mvlib
APP_LRT_LIB = $(BUILDDIR_TOP)/app-lrt.mvlib
APP_DEFAULT_SHAVE_MODULE_LIB = $(BUILDDIR_TOP)/app-shave.mvlib
APP_SHAVEAPP_OBJECT_LIST = $(BUILDDIR_TOP)/app-shaveapp-objects
export APP_SHAVEAPP_OBJECT_LIST

include $(APPDIR)/.config
ifeq "y" "$(CONFIG_USE_CUSTOM_LDSCRIPT)"
LinkerScript := $(APPDIR)/$(call unquote,$(CONFIG_CUSTOM_SCRIPT_REL_PATH))
endif

DdrInitHeaderMvcmd = $(MDK_INSTALL_DIR_ABS)/utils/ddrInit/$(call unquote,$(CONFIG_DDR_INIT_HEADER_CMD))

include $(MDK_INSTALL_DIR_ABS)/toolssettings.mk
include $(MDK_INSTALL_DIR_ABS)/make/build-lib.mk

ifeq "" "$(CONFIG_OUTPUT_FILE_NAME)"
$(eval $(call ERROR_TARGET,check_output_file_name,Missing OUTPUT_FILE_NAME option in your application kcnf file. Please add it$(comma) re-run the configuration then retry the build.))
else
$(eval $(call NOP_TARGET,check_output_file_name))
endif
PHONY += check-output-file-name

SHAVE_DYN_APPS_DIR_NAME=$(call unquote,$(CONFIG_APP_SHAVE_DYN_APPS_SRCS_DIR))

# TODO create dependency files
# TODO check if there any race conditions when writing to the intermediate
# files during the collect or build phase; keep in mind that the user may have
# -j option active on their system

mdk-debug-obj-list-los-$(CONFIG_BUILD_DEBUG) = $(BUILDDIR_TOP)/.mdk_debug_obj_list_los
mdk-debug-obj-list-lrt-$(CONFIG_BUILD_DEBUG) = $(BUILDDIR_TOP)/.mdk_debug_obj_list_lrt
mdk-debug-obj-list-shave-$(CONFIG_BUILD_DEBUG) = $(BUILDDIR_TOP)/.mdk_debug_obj_list_shave

define BUILD_PROCESSOR_MDK

export $(2)_MDK_LIBRARIES_LIST

$(2)_SRCS_DIR=$$(APPDIR)/$$(call unquote,$(CONFIG_APP_$(2)_SRCS_DIR))
$(2)_DYN_APPS_DIR=$$(APPDIR)/$$(SHAVE_DYN_APPS_DIR_NAME)
build_processor_target_mdk:=build-mdk-$(1)
collect_target_$(1):=collect-$(1)
info_msg:=$(2): building default shaveapp

PHONY += $$(build_processor_target_mdk) $$(collect_target_$(1))
$$(build_processor_target_mdk): $$(collect_target_$(1))
	$$(SILENT_INFO) $(2): Building MDK libraries and components
	$$(ECHO)$$(call BUILD_MAKE_SUBDIR,$$(MDK_INSTALL_DIR_ABS),$$(BUILDDIR)/mdk/$(1),srcs-$(1)-y,$$($(2)_INCLUDE_DIRS_LIST),$$($(2)_CCOPT_LIST),$$($(2)_CPPOPT_LIST),build-$(1).mk,build-$(1))
	$$(SILENT_INFO) $(2): Done building MDK.

# remove the following 2 empty lines and MAKE will give you weird errors, further away in this makefile :/


endef

$(eval $(if $(filter y,$(CONFIG_HAS_SHAVE_SRCS)),$(call BUILD_PROCESSOR_MDK,shave,SHAVE)))
$(eval $(if $(filter y,$(CONFIG_HAS_LOS_SRCS)),$(call BUILD_PROCESSOR_MDK,los,LOS)))
$(eval $(if $(filter y,$(CONFIG_HAS_LRT_SRCS)),$(call BUILD_PROCESSOR_MDK,lrt,LRT)))

define BUILD_PROCESSOR_APP

export $(2)_APP_LIBRARIES_LIST

$(2)_SRCS_DIR=$$(APPDIR)/$$(call unquote,$(CONFIG_APP_$(2)_SRCS_DIR))
$(2)_DYN_APPS_DIR=$$(APPDIR)/$$(SHAVE_DYN_APPS_DIR_NAME)
build_processor_target_app:=build-app-$(1)
collect_target_$(1):=collect-$(1)
info_msg:=$(2): building default shaveapp

PHONY += $$(build_processor_target_app) $$(collect_target_$(1))
$$(build_processor_target_app): $$(collect_target_$(1))
	$$(SILENT_INFO) $(2): building application sources
ifneq "" "$$($(2)_SRCS_DIR)"
ifneq "" "$$(mdk-debug-obj-list-$(1)-y)"
	$$(ECHO)export MDK_DEBUG_OBJ_LIST=$$(mdk-debug-obj-list-$(1)-y)
endif
	$$(ECHO)$$(call BUILD_MAKE_SUBDIR,$$($(2)_SRCS_DIR),$$(BUILDDIR)/app/$(1),srcs-$(1)-y,$$($(2)_INCLUDE_DIRS_LIST),$$($(2)_CCOPT_LIST),$$($(2)_CPPOPT_LIST),build-$(1).mk,build-$(1))
	$$(ECHO)[[ -d $$($(2)_DYN_APPS_DIR) ]] && $$(call BUILD_MAKE_SUBDIR,$$($(2)_DYN_APPS_DIR),$$(BUILDDIR)/app/$(1)/$$(SHAVE_DYN_APPS_DIR_NAME),srcs-$(1)-y,$$($(2)_INCLUDE_DIRS_LIST),$$($(2)_CCOPT_LIST),$$($(2)_CPPOPT_LIST),build-$(1).mk,build-$(1))
endif
	$$(SILENT_INFO) $(2): Done building APP.

# remove the following 2 empty lines and MAKE will give you weird errors, further away in this makefile :/

build-$(1): build-mdk-$(1) build-app-$(1)
	$$(SILENT_INFO) $(2): Done building APP and MDK.

endef

$(eval $(if $(filter y,$(CONFIG_HAS_SHAVE_SRCS)),$(call BUILD_PROCESSOR_APP,shave,SHAVE)))
$(eval $(if $(filter y,$(CONFIG_HAS_LOS_SRCS)),$(call BUILD_PROCESSOR_APP,los,LOS)))
$(eval $(if $(filter y,$(CONFIG_HAS_LRT_SRCS)),$(call BUILD_PROCESSOR_APP,lrt,LRT)))

#
# build the LOS and LRT parts of the application, using the generated
# intermediately-linked LOS libraries and performing garbage collect on the
# standard "main" entry-point
# FIXME the --allow-multiple-definition : see BUG 24244
APP_FILE_NAME = $(call unquote,$(CONFIG_OUTPUT_FILE_NAME))
APP_LOS_MAP_FILE = $(BUILDDIR_TOP)/$(APP_FILE_NAME)-los.map
APP_LRT_MAP_FILE = $(BUILDDIR_TOP)/$(APP_FILE_NAME)-lrt.map

ld-trace-symbols-y += $(addprefix --trace-symbol=,$(call unquote,$(CONFIG_BUILD_TRACE_SYMBOL_NAMES)))
ld-trace-$(CONFIG_BUILD_TRACE_LD)=-t
ld-gc-sections-$(CONFIG_BUILD_GC_SECTIONS) = --gc-sections
ld-gc-sections-$(CONFIG_BUILD_PRINT_GC_SECTIONS) += --print-gc-sections
ld-allow-multiple-definition-$(CONFIG_BUILD_ALLOW_MULTIPLE_DEFINITIONS) = --allow-multiple-definition
ld-verbose-$(CONFIG_BUILD_VERBOSE_LD) = --verbose

# $1 - processor name in capitals
# $2 - processor name in lower caps
# $3 - y if the resulting library should have the symbols prefixed with $2
define BUILD_APP_LEON_COMPONENT
# file eventually created during the collect phase
$(1)_PREBUILD_LIBS_FILE=$(BUILDDIR_TOP)/$(2)-prebuilt-libs

ld-rtems-prologue-$(2)-$$(CONFIG_SOC_$(1)_MODE_RTEMS) = \
  $$(MV_GCC_TOOLS_BASE)/lib/gcc/$$(MV_GCC_TOOLS)/$$(GCCVERSION)/crti.o \
  $$(MV_GCC_TOOLS_BASE)/lib/gcc/$$(MV_GCC_TOOLS)/$$(GCCVERSION)/crtbegin.o \
	$$(MV_RTEMS_$(1)_LIB)/start.o
ld-rtems-opts-$(2)-$$(CONFIG_SOC_$(1)_MODE_RTEMS) = -L $$(MV_RTEMS_$(1)_LIB) -lrtemsbsp -lrtemscpu
ld-rtems-epilogue-$(2)-$$(CONFIG_SOC_$(1)_MODE_RTEMS) = \
  $$(MV_GCC_TOOLS_BASE)/lib/gcc/$$(MV_GCC_TOOLS)/$$(GCCVERSION)/crtend.o \
  $$(MV_GCC_TOOLS_BASE)/lib/gcc/$$(MV_GCC_TOOLS)/$$(GCCVERSION)/crtn.o
ifeq "y" "$$(CONFIG_HAS_LRT_SRCS)"
$$(APP_$(1)_LIB) : build-$(2) $(if $(CONFIG_APP_USES_SHAVEAPPS),$(BUILDDIR_TOP)/shaveXApps.symuniq,)
else
$$(APP_$(1)_LIB) : build-$(2)
endif
	@[[ -f "$$($(1)_PREBUILD_LIBS_FILE)" ]] && echo "$$($(1)_PREBUILD_LIBS_FILE)" >> $(CLEAN_LIST_FILE)
	$$(SILENT_INFO) "Building application $(1) component"
	$$(SILENT_LD) $$(LD) $$(ld-trace-y) -o $$@ \
		-Ur -EL -O9 -warn-common \
		$$(ld-trace-symbols-y) $$(ld-allow-multiple-definition-y) $$(ld-verbose-y) \
		-Map=$$(APP_$(1)_MAP_FILE) \
		$$(ld-rtems-prologue-$(2)-y) \
		$$(shell $(MDK_INSTALL_DIR_ABS)/make/list_libraries.sh $$($(1)_APP_LIBRARIES_LIST) $(CONFIG_BUILD_DEBUG)) \
		$$(shell $(MDK_INSTALL_DIR_ABS)/make/list_libraries.sh $$($(1)_MDK_LIBRARIES_LIST) $(CONFIG_BUILD_DEBUG)) \
		--start-group \
		$$(shell [[ -f "$$($(1)_PREBUILD_LIBS_FILE)" ]] && cat "$$($(1)_PREBUILD_LIBS_FILE)") \
		$$(ld-rtems-opts-$(2)-y) \
		-L $$(MV_GCC_TOOLS_BASE)/lib/gcc/$$(MV_GCC_TOOLS)/$$(GCCVERSION)	-lgcc -lgcov \
		-L $$(MV_GCC_TOOLS_BASE)/$$(MV_GCC_TOOLS)/lib -lc -lssp -lstdc++ -lsupc++ -lm -lg -lssp_nonshared  \
		--end-group \
		$$(ld-rtems-epilogue-$(2)-y) \
		-L $$(DirLDScrCommon) -T leon_os_partlink.ldscript
	@echo $$@ >> $$(CLEAN_LIST_FILE)
	@echo $$(APP_$(1)_MAP_FILE) >> $$(CLEAN_LIST_FILE)
ifeq (y,$(3))
	$$(SILENT_OBJCOPY) $$(OBJCOPY) --prefix-alloc-sections=.$(2) --prefix-symbols=$(2)_ $$@
	$$(SILENT_OBJCOPY) $$(OBJCOPY) --redefine-sym $(2)____globalTail=___globalTail -W ___globalTail $$@
	$$(SILENT_OBJCOPY) $$(OBJCOPY) --redefine-sym $(2)___SglResMgrGlobal=__SglResMgrGlobal -W __SglResMgrGlobal $$@
	$$(SILENT_OBJCOPY) $$(OBJCOPY) --redefine-sym $(2)___FilterResourceManagerGlobal=__FilterResourceManagerGlobal -W __FilterResourceManagerGlobal $$@
	$$(SILENT_OBJCOPY) $$(OBJCOPY) --redefine-sym $(2)___hwFilterListGlobal=__hwFilterListGlobal -W __hwFilterListGlobal $$@
ifeq "y" "$(CONFIG_APP_USES_SHAVEAPPS)"
	$$(SILENT_OBJCOPY) $$(OBJCOPY) --redefine-syms=$(BUILDDIR_TOP)/shaveXApps.symuniq --redefine-sym $(2)___SglResMgrGlobal=__SglResMgrGlobal -W __SglResMgrGlobal --redefine-sym $(2)_GlobalContextData=GlobalContextData -W GlobalDynContextData --redefine-sym $(2)____globalTail=___globalTail -W ___globalTail $$@
endif
endif

endef

$(eval $(call BUILD_APP_LEON_COMPONENT,LOS,los,))
$(eval $(call BUILD_APP_LEON_COMPONENT,LRT,lrt,y))
APP_SHAVE_MAP_FILE = $(BUILDDIR_TOP)/$(APP_FILE_NAME)-shave.map
$(eval $(call LIST_LIBRARIES,SHAVE_APP_LIBS,SHAVE_APP_LIBRARIES_LIST))
$(eval $(call LIST_LIBRARIES,SHAVE_MDK_LIBS,SHAVE_MDK_LIBRARIES_LIST))
SHAVE_LIBS+=$(SHAVE_MDK_LIBS) $(SHAVE_APP_LIBS) \
						$(shell [[ -f $(APP_SHAVEAPP_OBJECT_LIST) ]] && cat $(APP_SHAVEAPP_OBJECT_LIST))

# SHAVE applications can have multiple entry points, so give the first une
# with --entry then the others with --undef
APP_SHAVE_ENTRY_POINT=$(call unquote,$(CONFIG_APP_SHAVE_ENTRY_POINT))
APP_SHAVE_FIRST_ENTRY=$(firstword $(APP_SHAVE_ENTRY_POINT))
APP_SHAVE_OTHER_ENTRY=$(filter-out $(APP_SHAVE_FIRST_ENTRY),$(APP_SHAVE_ENTRY_POINT))
ld-entry-points-shave= --entry=$(APP_SHAVE_ENTRY_POINT) $(if $(APP_SHAVE_OTHER_ENTRY),$(addprefix --undef=,$(APP_SHAVE_OTHER_ENTRY)))

$(APP_DEFAULT_SHAVE_MODULE_LIB) : build-shave
	$(SILENT_INFO) "Building application SHAVE component"
	$(SILENT_LD) $(LD) $(ld-trace-y) -o $@ \
		$(MVLIBOPT) $(ld-entry-points-shave) \
		$(ld-allow-multiple-definition-y) $(ld-verbose-y) --gc-sections \
		-Map=$(APP_SHAVE_MAP_FILE) \
		$(shell $(MDK_INSTALL_DIR_ABS)/make/list_libraries.sh $(SHAVE_APP_LIBRARIES_LIST) $(CONFIG_BUILD_DEBUG)) \
		$(shell $(MDK_INSTALL_DIR_ABS)/make/list_libraries.sh $(SHAVE_MDK_LIBRARIES_LIST) $(CONFIG_BUILD_DEBUG)) \
		$(CompilerANSILibs)
	$(call LIST_TARGET_FILE)
	@echo $(APP_SHAVE_MAP_FILE) >> $(CLEAN_LIST_FILE)

ifeq (y,$(CONFIG_OUTPUT_TYPE_ELF))

# ok we're building an ELF
# FIXME create a variable for the default libraries instead of directly
# putting their names here?
# FIXME -t in LDOPT should be given only in verbose mode
APP_ELF_FILE = $(BUILDDIR)/$(APP_FILE_NAME).elf
APP_ELF_FILE_NODEBUG = $(APP_ELF_FILE).nodebug
APP_MAP_FILE = $(BUILDDIR)/$(APP_FILE_NAME).map
APP_MVCMD_FILE = $(BUILDDIR)/$(APP_FILE_NAME).mvcmd
LST_LEON_FILE = $(BUILDDIR_TOP)/$(APP_FILE_NAME)_leon.lst
LST_SHAVE_FILE = $(BUILDDIR_TOP)/$(APP_FILE_NAME)_shave.lst


ifeq (y,$(CONFIG_HAS_LOS_SRCS))
ELF_LIBS+=$(APP_LOS_LIB)
PHONY+=build-los-parts
build-los-parts: $(build-shave-y) $(APP_LOS_LIB)
	@echo > /dev/null

endif
ifeq (y,$(CONFIG_HAS_LRT_SRCS))
ELF_LIBS+=$(APP_LRT_LIB)
PHONY+=build-lrt-parts
build-lrt-parts: $(build-shave-y) $(APP_LRT_LIB)
	@echo > /dev/null

endif
ifeq (y,$(CONFIG_HAS_SHAVE_SRCS))
ELF_LIBS+=$(APP_SHAVE_PLACED_LIBS)

ifeq "y" "$(CONFIG_HAS_SHAVE_DEFAULT_STATIC_MODULE)"
define BUILD_PLACED_SHAVE_LIB
ifeq (y,$$(CONFIG_PLACE_SHAVE_CORE_$(1)))
APP_SHAVE_PLACED_LIB_$(1) = $(BUILDDIR)/app-shave-$(1).mvlib
$$(APP_SHAVE_PLACED_LIB_$(1)): $$(APP_DEFAULT_SHAVE_MODULE_LIB)
	$$(SILENT_INFO) Placing SHAVE library "$$@"
	$$(ECHO) $$(OBJCOPY) --prefix-alloc-sections=.shv$(1). --prefix-symbols=$(call unquote,$(CONFIG_APP_SHAVE_NAME))$(1)_ $$< $$@
	$$(ECHO) $$(OBJCOPY) --redefine-sym $(1)____globalTail=___globalTail -W ___globalTail $$@
	$$(ECHO) $$(OBJCOPY) --redefine-sym $(1)___SglResMgrGlobal=__SglResMgrGlobal -W __SglResMgrGlobal $$@
	$$(ECHO) $$(OBJCOPY) --redefine-sym $(1)___FilterResourceManagerGlobal=__FilterResourceManagerGlobal -W __FilterResourceManagerGlobal $$@
	$$(ECHO) $$(OBJCOPY) --redefine-sym $(1)___hwFilterListGlobal=__hwFilterListGlobal -W __hwFilterListGlobal $$@
	@echo $$@ >> $$(CLEAN_LIST_FILE)
APP_SHAVE_PLACED_LIBS += $$(APP_SHAVE_PLACED_LIB_$(1))
endif
endef

$(foreach idx,$(SHAVES_IDX),$(eval $(call BUILD_PLACED_SHAVE_LIB,$(idx))))
endif
ifeq "y" "$(CONFIG_APP_USES_SHAVEAPPS)"

# create the ShaveApps group libraries
include $(MDK_INSTALL_DIR_ABS)/make/build-shaveapp-group.mk

SHAVEAPP_ELF_DEPENDENCIES:=$(shell [[ -f $(SHAVEAPP_ELF_DEPENDENCIES_LIST) ]] && cat $(SHAVEAPP_ELF_DEPENDENCIES_LIST))
$(call VINFO,SHAVEAPP_ELF_DEPENDENCIES=$(sort $(SHAVEAPP_ELF_DEPENDENCIES) ))
ELF_LIBS+=$(sort $(SHAVEAPP_ELF_DEPENDENCIES))
ELF_LIBS+=$(SHAVEAPP_DYNCONTEXT_LIST)
ELF_LIBS+=$(BUILDDIR_TOP)/dynContextMaster.o
APP_SHAVEAPP_OBJECTS=$(shell [[ -f $(APP_SHAVEAPP_OBJECT_LIST) ]] && cat $(APP_SHAVEAPP_OBJECT_LIST))
# APP_SHAVEAPP_MDK_LIBRARIES=$(shell $(MDK_INSTALL_DIR_ABS)/make/list_libraries.sh $(SHAVE_MDK_LIBRARIES_LIST) $(CONFIG_BUILD_DEBUG))
ELF_LIBS+=$(APP_SHAVEAPP_OBJECTS) $(APP_SHAVEAPP_MDK_LIBRARIES)
$(call VINFO,ELF_LIBS=$(ELF_LIBS))

else

build-shaveapp-group:
	@echo > /dev/null

endif
PHONY+=build-shave-parts
build-shave-parts: $(APP_SHAVE_PLACED_LIBS) build-shaveapp-group
	@echo > /dev/null

endif

build-los-$(CONFIG_HAS_LOS_SRCS)=build-los-parts
build-lrt-$(CONFIG_HAS_LRT_SRCS)=build-lrt-parts
build-shave-$(CONFIG_HAS_SHAVE_SRCS)=build-shave-parts $(build-los-y) $(build-lrt-y)
# this and the lines above trick GNU Make into making the targets in the order we want
build-shave-first: $(build-shave-y) $(build-lrt-y) $(build-los-y)
	@echo > /dev/null

ELF_LDOPT=-L $(BUILDDIR_TOP) $(LDOPT)

$(APP_ELF_FILE) : $(LinkerScript) build-shave-first
	$(SILENT_INFO) "Building application ELF"
	$(SILENT_LD)$(LD) \
		-o $@ \
		$(filter-out -t --gc-sections,$(ELF_LDOPT)) $(ld-trace-y) \
		$(ld-gc-sections-y) $(ld-trace-symbols-y) $(ld-allow-multiple-definition-y) $(ld-verbose-y) \
		$(ELF_LIBS) \
		-Map=$(APP_MAP_FILE)
	$(call LIST_TARGET_FILE)
	@echo $(APP_MAP_FILE) >> $(CLEAN_LIST_FILE)

.ONESHELL:
$(APP_MVCMD_FILE) : $(APP_ELF_FILE)
	$(SILENT_MVCONV) $(MVCONV) $(MVCONVOPT) $(MVCMDOPT) $^ -mvcmd:$@

# disable LST file generation when building on Jenkins
ifeq "$(JENKINS_HOME)" ""
lst-files-$(CONFIG_BUILD_LST_FILES) := lst
endif
lst-file-leon-$(CONFIG_HAS_LOS_SRCS) = $(LST_LEON_FILE)
lst-file-leon-$(CONFIG_HAS_LRT_SRCS) = $(LST_LEON_FILE)
lst-file-shave-$(CONFIG_HAS_SHAVE_SRCS) = $(LST_SHAVE_FILE)

.INTERMEDIATE: $(APP_ELF_FILE_NODEBUG)

$(APP_ELF_FILE_NODEBUG): $(APP_ELF_FILE)
	$(SILENT_OBJCOPY) $(OBJCOPY) -R .debug_aranges \
			   -R .debug_ranges \
			   -R .debug_pubnames \
			   -R .debug_info \
			   -R .debug_abbrev \
			   -R .debug_asmline \
			   -R .debug_line \
			   -R .debug_str \
			   -R .debug_loc \
			   -R .debug_macinfo \
			$< $@

$(LST_LEON_FILE): $(APP_ELF_FILE_NODEBUG)
	$(SILENT_OBJDUMP) $(OBJDUMP) -xsrtd $< > $@

$(LST_SHAVE_FILE): $(APP_ELF_FILE_NODEBUG)
	$(SILENT_MVDUMP) $(MVDUMP) -cv:$(MV_SOC_REV) $< -o:$@ >/dev/null

lst: $(lst-file-leon-y) $(lst-file-shave-y)
	@echo > /dev/null

build-selected-target: $(APP_ELF_FILE) $(APP_MVCMD_FILE) $(lst-files-y)
	@echo "" > /dev/null

PHONY += build-selected-target
endif # CONFIG_OUTPUT_TYPE_ELF

ifeq (y,$(CONFIG_OUTPUT_TYPE_LIB))
# building libraries is not yet supported
build-selected-target:
	$(SILENT_ERROR) Building libraries is not yet implemented :-/
	exit 1

PHONY += build-selected-target
endif # CONFIG_OUTPUT_TYPE_LIB

.PHONY: check_tools_present
.ONESHELL:
check_tools_present:
	$(SILENT_INFO) Checking tools
	@if [ ! -f "$(lastword $(MVCC))" ] ; then
		$(SILENT_ERROR) "Seems that you don't have tools version $(MV_TOOLS_VERSION). Please download them then try again the build"
		@exit 1
	fi

ifeq "y" "$(CONFIG_BUILD_GENERATE_COMPILE_COMMANDS)"
# TODO factor this with the same definition in build-processor.mk
# TODO find a way to close the file even in case of compile errors
compile-commands-file-$(CONFIG_BUILD_GENERATE_COMPILE_COMMANDS) = $(APPDIR)/compile_commands.json
close-compile-commands-file:
	@echo "{}" >> $(compile-commands-file-y)
	@echo "]" >> $(compile-commands-file-y)
endif

# TODO include/generated should also contain MV_SOC_REV to accomodate
# per-platform settings
build: check_tools_present check-output-file-name $(APPDIR)/include/generated/autoconf.h build-selected-target $(if $(CONFIG_BUILD_GENERATE_COMPILE_COMMANDS),close-compile-commands-file)
	$(SILENT_INFO)"Done."

# TODO move this file into the `make` directory when migration is done
include $(MDK_INSTALL_DIR)/saferemove.mk
PHONY += clean
clean:
	$(SILENT_INFO) Cleaning building artefacts from $(BUILDDIR)
	$(call SAFE_RM,$(BUILDDIR))
	$(call SAFE_RM,$(MDK_TMPDIR))


.ONESHELL:
$(BUILDDIR_REPORT)/memviz.html : build
	$(SILENT_INFO)echo "Generating memory view..."
	[ ! -d $(@D) ] && mkdir $(@D)
	env python2 $(MDK_INSTALL_DIR_ABS)/utils/memviz.py \
                      -i $(BUILDDIR_TOP)/$(APP_FILE_NAME).map \
                      -p $(MV_SOC_PLATFORM)             \
	                    -o $@ \
                      -c $(MDK_INSTALL_DIR_ABS)/utils/memviz_style.css
	$(call LIST_TARGET_FILE)

.ONESHELL:
report: $(BUILDDIR_REPORT)/memviz.html
	$(SILENT_INFO)Opening report in your favourite browser
	which xdg-open > /dev/null && xdg-open $^
	$(call LIST_TARGET_FILE)


.PHONY: $(PHONY)
