# LRT build rules
# This is the main LEON OS makefile. It produces the LRT .mvlib that'll be
# linked into the final ELF file.

include $(APPDIR)/.config
include $(MDK_INSTALL_DIR_ABS)/make/lib.mk
include $(MDK_INSTALL_DIR_ABS)/make/build-lib.mk
include $(MDK_INSTALL_DIR_ABS)/toolssettings.mk

#========================
# build character LRT

# always use lower case for defining this
B_PROCESSOR=lrt
B_INCLUDE_DEFAULT=$(MDK_INSTALL_DIR_ABS)/shared/include

B_CC=$(CC)
B_CXX=$(CXX)
B_SILENT_CC=$(SILENT_LRTCC) $(B_CC)
B_SILENT_CXX=$(SILENT_LRTCXX) $(B_CXX)
B_CCOPT=$(CCOPT_LRT) $(CONLY_OPT)
B_SILENT_LD=$(SILENT_LD) $(LD)
B_SILENT_AR=$(SILENT_AR) $(AR)
#
# FIXME the -c flag should be added with more control as user may
# occasionnally need to replace it with -S or some other similar flags
#========================

include $(MDK_INSTALL_DIR_ABS)/make/build-processor.mk

