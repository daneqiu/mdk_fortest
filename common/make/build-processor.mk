
# This makefile contain generic target processor logic. This file needs some
# variables to be defined prior to be processed. Take a look to build-los.mk
# for an example usage

$(call VINFO,build-$(B_PROCESSOR) in $(MDK_SUBDIR) with goals '$(MAKECMDGOALS)')

# Use this variable into your Makefile to get the directory where your
# Makefile lives in
MDK_SUBDIR_ABS=$(abspath $(MDK_SUBDIR))
include $(MDK_SUBDIR)/$(SUBDIR_MAKEFILE_NAME)

B_LIB_NAME=$(B_PROCESSOR).mvlib
B_COLLECTED_LIB=.$(B_PROCESSOR)-collected-lib

B_RECURSE_SUBDIRS=$(subdirs-$(B_PROCESSOR)-y)
B_RECURSE_BUILD += $(foreach dir,$(B_RECURSE_SUBDIRS),$(BUILDDIR)/$(dir)/$(B_LIB_NAME))
B_RECURSE_COLLECT += $(foreach dir,$(B_RECURSE_SUBDIRS),$(BUILDDIR)/$(dir)/$(B_COLLECTED_LIB))
B_COLLECT_MISC=$(collect-misc-$(B_PROCESSOR)-y)

B_LOCAL_CCOPT=$(local-ccopt-$(B_PROCESSOR)-y)
B_LOCAL_CPPOPT=$(local-cppopt-$(B_PROCESSOR)-y)
B_LOCAL_ASMOPT=$(local-asmopt-$(B_PROCESSOR)-y)

B_SRCS_C   = $(filter %.c,$($(SRCS)))
B_SRCS_CPP = $(filter %.cpp,$($(SRCS)))
B_SRCS_ASM = $(filter %.asm,$($(SRCS)))
B_SRCS_S   = $(filter %.S,$($(SRCS)))
B_SRCS_URC = $(filter %.urc,$($(SRCS)))

# NOTE the object file name contains the original file extension in order to
# prevent situations where the user provides the same file name with different
# extensions, leading to weird, hard to debug, linking errors
C_OBJS   = $(sort $(patsubst %.c,%_c_$(B_PROCESSOR).o,$(B_SRCS_C)))
CPP_OBJS = $(sort $(patsubst %.cpp,%_cpp_$(B_PROCESSOR).o,$(B_SRCS_CPP)))
ASM_OBJS = $(sort $(patsubst %.asm,%_asm_$(B_PROCESSOR).o,$(B_SRCS_ASM)))
S_OBJS   = $(sort $(patsubst %.S,%_S_$(B_PROCESSOR).o,$(B_SRCS_S)))
URC_OBJS = $(sort $(patsubst %.urc,%_urc.o,$(B_SRCS_URC)))
OBJS = $(C_OBJS) $(CPP_OBJS) $(ASM_OBJS) $(S_OBJS) $(URC_OBJS)
OBJS += $(objs-$(B_PROCESSOR)-y)

empty:=
comma:=,
space:=$(empty) $(empty)

B_INCLUDES = $(addprefix -I ,$(sort $(B_INCLUDE_DEFAULT)))
B_INCLUDES += $(addprefix -I ,$(sort $(include-dirs-$(B_PROCESSOR)-y)))

$(call VINFO,MDK_SUBDIR        = $(MDK_SUBDIR))
$(call VINFO,BUILDDIR          = $(BUILDDIR))
$(call VINFO,$(SRCS)           = $($(SRCS)))
$(call VINFO,OBJS              = $(OBJS))
$(call VINFO,B_RECURSE_SUBDIRS = $(B_RECURSE_SUBDIRS))
$(call VINFO,B_RECURSE_BUILD   = $(B_RECURSE_BUILD))
$(call VINFO,B_INCLUDES        = $(B_INCLUDES))
$(call VINFO,B_INCLUDE_DEFAULT = $(B_INCLUDE_DEFAULT))
$(call VINFO,INCLUDE_DIRS_LIST = $(INCLUDE_DIRS_LIST))

ifeq "" "$(MDK_SUBDIR)"
$(error MDK_SUBDIR cannot be empty)
endif

OBJ_DIRS = $(sort $(dir $(OBJS)))
OBJ_DIRS_CREATED = $(foreach objdir,$(OBJ_DIRS),$(BUILDDIR)/$(objdir).created)
# FIXME this should no longer be necessary because of the %/.created. check
# and remove if true
$(OBJ_DIRS_CREATED) :
	$(ECHO)[ -d "$(dir $@)" ] || mkdir -p "$(dir $@)"
	$(ECHO)touch $@

ifneq "" "$(INCLUDE_DIRS_LIST)"
B_COLLECTED_INCLUDES := $(addprefix -I,$(shell [[ -f $(INCLUDE_DIRS_LIST) ]] && cat $(INCLUDE_DIRS_LIST) | sort | uniq ))
B_COLLECTED_ASM_INCLUDES := $(shell [[ -f $(INCLUDE_DIRS_LIST)-asm ]] && cat $(INCLUDE_DIRS_LIST)-asm | sort | uniq)
ifneq "" "$(B_COLLECTED_ASM_INCLUDES)"
B_ASM_INCLUDES += $(addprefix -i:,$(sort $(B_COLLECTED_ASM_INCLUDES)))
# we use this internal variable in order to allow user to freely use Make
# syntax to construct B_CCOPT_ASM as standard separator is the space and not
# that incorrect specifier that's the comma, imposed by moviAsm
B_CCOPT_ASM_COMMA=$(subst $(space),$(comma),$(B_CCOPT_ASM) $(B_ASM_INCLUDES))
endif
endif
ifneq "" "$(CCOPT_LIST)"
B_COLLECTED_CCOPT := $(shell [ -f "$(CCOPT_LIST)" ] && cat "$(CCOPT_LIST)")
endif
ifneq "" "$(CPPOPT_LIST)"
B_COLLECTED_CPPOPT := $(shell [ -f "$(CPPOPT_LIST)" ] && cat "$(CPPOPT_LIST)")
endif
.ONESHELL:
$(C_OBJS) : %_c_$(B_PROCESSOR).o : $(MDK_SUBDIR)/%.c $(OBJ_DIRS_CREATED)
	@echo $(addprefix $(BUILDDIR)/,$@) >> $(CLEAN_LIST_FILE)
	$(B_SILENT_CC) -o $@ -c $(B_CCOPT) $(B_COLLECTED_CCOPT) $(B_LOCAL_CCOPT) $(B_CCOPT_ASM_COMMA) \
		$(B_INCLUDES) $(B_COLLECTED_INCLUDES) \
		-include $(APPDIR)/include/generated/autoconf.h \
		$<
ifeq "y" "$(CONFIG_BUILD_GENERATE_COMPILE_COMMANDS)"
	echo "{" >> $(compile-commands-file-y)
	echo "\"directory\": \"$(<D)\"," >> $(compile-commands-file-y)
	echo "\"command\": \"$(filter-out $(CCACHE),$(B_CC)) -o $@ -c $(B_CCOPT) $(B_COLLECTED_CCOPT) $(B_LOCAL_CCOPT) $(B_CCOPT_ASM_COMMA) $(B_INCLUDES) $(B_COLLECTED_INCLUDES) -include $(APPDIR)/include/generated/autoconf.h $<\""	>> $(compile-commands-file-y)
	echo "\"file\": \"$<\"" >> $(compile-commands-file-y)
	echo "}," >> $(compile-commands-file-y)
endif

%_urc.c : $(MDK_SUBDIR)/%.urc
	@echo "$@" >> $(CLEAN_LIST_FILE)
	@echo $(patsubst %.urc,%.h,$<) >> $(CLEAN_LIST_FILE)
	$(B_SILENT_URC) $(USBRCOTP) -Io - -o - $< > $@
	$(B_SILENT_URC) $(USBRCOTP) -o /dev/null -Ho $(patsubst %_urc.c,%.h,$@) $<

#USBRC generated object files have slightly different rules because they have to
#not generate dependency files one on another, they themselves being generated files
#in their turn. So we take all other rules but we exclude the -MD option from this one
.ONESHELL:
$(URC_OBJS) : %_urc.o : %_urc.c
	@echo $(addprefix $(BUILDDIR)/,$@) >> $(CLEAN_LIST_FILE)
	$(B_SILENT_CC)  -o $@ -c $(filter-out -MD -MP,$(B_CCOPT)) \
		$(B_INCLUDES) $(B_COLLECTED_INCLUDES) \
		$<

compile-commands-file-$(CONFIG_BUILD_GENERATE_COMPILE_COMMANDS) = $(APPDIR)/compile_commands.json

$(compile-commands-file-y):
	echo "[" >> $@
	echo "$@" >> $(CLEAN_LIST_FILE)

.ONESHELL:
$(CPP_OBJS) : %_cpp_$(B_PROCESSOR).o : $(MDK_SUBDIR)/%.cpp $(OBJ_DIRS_CREATED) $(if $(CONFIG_BUILD_GENERATE_COMPILE_COMMANDS),$(compile-commands-file-y))
	@echo $(addprefix $(BUILDDIR)/,$@) >> $(CLEAN_LIST_FILE)
	$(B_SILENT_CXX) -o $@ -c $(filter-out -std=gnu11,$(B_CCOPT)) $(B_COLLECTED_CCOPT) $(B_COLLECTED_CPPOPT) $(B_LOCAL_CCOPT) $(B_LOCAL_CPPOPT) $(B_CCOPT_ASM_COMMA) \
		$(B_INCLUDES) $(B_COLLECTED_INCLUDES) \
		-include $(APPDIR)/include/generated/autoconf.h \
		$<
ifeq "y" "$(CONFIG_BUILD_GENERATE_COMPILE_COMMANDS)"
	echo "{" >> $(compile-commands-file-y)
	echo "\"directory\": \"$(<D)\"," >> $(compile-commands-file-y)
	echo "\"command\": \"$(filter-out $(CCACHE),$(B_CXX)) -o $@ -c $(filter-out -std=gnu11,$(B_CCOPT)) $(B_COLLECTED_CCOPT) $(B_LOCAL_CCOPT) $(B_LOCAL_CPPOPT) $(B_CCOPT_ASM_COMMA) $(B_INCLUDES) $(B_COLLECTED_INCLUDES) -include $(APPDIR)/include/generated/autoconf.h $< \"," >> $(compile-commands-file-y)
	echo "\"file\": \"$<\"" >> $(compile-commands-file-y)
	echo "}," >> $(compile-commands-file-y)
endif

.ONESHELL:
$(S_OBJS) : %_S_$(B_PROCESSOR).o : $(MDK_SUBDIR)/%.S $(OBJ_DIRS_CREATED)
	@echo $(addprefix $(BUILDDIR)/,$@) >> $(CLEAN_LIST_FILE)
	$(B_SILENT_CC) -o $@ -c $(B_CCOPT) $(B_COLLECTED_CCOPT) \
		$(B_INCLUDES) $(B_COLLECTED_INCLUDES) \
		-include $(APPDIR)/include/generated/autoconf.h \
		$<

.ONESHELL:
$(ASM_OBJS) : %_asm_$(B_PROCESSOR).o : $(MDK_SUBDIR)/%.asm $(OBJ_DIRS_CREATED)
	@echo $(addprefix $(BUILDDIR)/,$@) >> $(CLEAN_LIST_FILE)
	$(B_SILENT_ASM) $(MVASMOPT) $(B_LOCAL_ASMOPT) $(B_ASM_INCLUDES)  $< -o:$@  $(DUMP_NULL)

make-print-database-$(CONFIG_MAKE_DEBUG_PRINT_DATABASE)=-p
%/$(B_COLLECTED_LIB):
	$(ECHO)$(MAKE) $(MAKE_TRACE) $(make-print-database-y) MDK_SUBDIR=$(MDK_SUBDIR)/$(subst $(BUILDDIR),,$*) \
		BUILDDIR=$(subst ..,_,$*) \
		SRCS=srcs-$(B_PROCESSOR)-y \
		INCLUDE_DIRS_LIST=$(INCLUDE_DIRS_LIST) \
		LIBRARIES_LIST=$(LIBRARIES_LIST) \
		-f $(MDK_INSTALL_DIR_ABS)/make/build-$(B_PROCESSOR).mk \
		collect-$(B_PROCESSOR)

.ONESHELL:
%/$(B_LIB_NAME) :
	$(ECHO)$(eval BUILD_DIR=$(subst ..,_,$*))
	[ -d $(BUILD_DIR) ] || mkdir -p $(BUILD_DIR)
	$(eval SUBDIR=$(subst $(BUILDDIR),,./$*))
ifeq (y,$(CONFIG_BUILD_VERBOSE))
	$(V)In makefile $(lastword $(MAKEFILE_LIST))
	$(V)   stem     = $*
	$(V)   BUILDDIR = $(BUILDDIR)
	$(V)   SUBDIR   = $(SUBDIR)
endif
	$(call BUILD_MAKE_SUBDIR,$(abspath $(MDK_SUBDIR)/$(SUBDIR)),$(BUILD_DIR),$(SRCS),$(INCLUDE_DIRS_LIST),$(CCOPT_LIST),$(CPPOPT_LIST),build-$(B_PROCESSOR).mk,build-$(B_PROCESSOR))

ifdef shaveapp-id
ifeq "y" "$(CONFIG_USE_SHAVEAPP_$(shaveapp-id))"
ifeq "y" "$(CONFIG_SHAVEAPP_$(shaveapp-id)_TYPE_DYNAMIC)"

# locally override library build mode for shave dynamic apps
CONFIG_BUILD_SUBDIRS_USING_LD=y
CONFIG_BUILD_SUBDIRS_USING_OBJECTS=n
CONFIG_BUILD_SUBDIRS_USING_AR=n

SHAVEAPP_GROUP = $(call unquote,$(CONFIG_SHAVEAPP_$(shaveapp-id)_GROUP))
ifeq "" "$(SHAVEAPP_GROUP)"
$(error "The current shaveapp named '$(shaveapp-id)' configuration does not specify a shavegroup")
endif

SHVXLIB_SUFFIX = shvXlib$(SHAVEAPP_GROUP)
SHVXLIB=$(BUILDDIR)/$(shaveapp-id).$(SHVXLIB_SUFFIX)
# SHVXLIB+=$(BUILDDIR)/$(BUILDDIR_TOP)/$(shaveapp-id)_shvXdata_sym.o
# SHVXLIB+=$(BUILDDIR_TOP)/$(shaveapp-id)_shvXdata.o

endif
endif
endif

PHONY += collect-$(B_PROCESSOR)
ifneq "" "$(strip $(OBJS))"

# NOTE the LIB variable should only be defined here, only if there are $(OBJS)
# defined if not, you'll end-up with an infinite recursive build loop
#
# let the user override the target library rule. See th README.adoc for more on this
ifeq "" "$(strip $(LIB))"
LIB = $(BUILDDIR)/$(B_LIB_NAME)
endif

# locally override library build mode for shave dynamic apps or when user
# specifically wants it
ifneq "" "$(or $(shaveapp-id),$(filter y,$(subdir-$(B_PROCESSOR)$(C_PROCESSOR)-force-ld)))"
$(call V_INFO,Overriding build using LD in subdir $(SUBDIR))
CONFIG_BUILD_SUBDIRS_USING_LD=y
CONFIG_BUILD_SUBDIRS_USING_OBJECTS=n
CONFIG_BUILD_SUBDIRS_USING_AR=n
endif

ifndef collect-target-override
collect-$(B_PROCESSOR) : $(B_RECURSE_COLLECT) $(B_COLLECT_MISC)
ifeq "y" "$(CONFIG_BUILD_SUBDIRS_USING_LD)"
	$(ECHO)echo "L:$(if $(SHVXLIB),$(SHVXLIB),$(LIB))" >> $(LIBRARIES_LIST)
endif
ifeq "y" "$(CONFIG_BUILD_SUBDIRS_USING_OBJECTS)"
	$(ECHO)echo "O:$(LIB)" >> $(LIBRARIES_LIST)
ifneq "" "$(strip $(collect-$(B_PROCESSOR)-deps-y))"
	$(ECHO)echo "O:$(collect-$(B_PROCESSOR)-deps-y)" >> $(LIBRARIES_LIST)
endif
endif
ifeq "y" "$(CONFIG_BUILD_SUBDIRS_USING_AR)"
	$(ECHO)echo "A:$(LIB)" >> $(LIBRARIES_LIST)
endif
ifneq "" "$(libs-$(B_PROCESSOR)-y)"
	$(ECHO)echo $(libs-$(B_PROCESSOR)-y) >> $(LIBRARIES_LIST)
endif
ifeq "y" "$(CONFIG_BUILD_VERBOSE)"
	@echo "Collecting for future build $(LIB)"
endif
ifdef shaveapp-id
	$(ECHO)echo "$(LIB) " >> $(BUILDDIR_TOP)/$(strip $(call unquote,$(CONFIG_SHAVEAPP_$(shaveapp-id)_GROUP)))-grpsyms-dependencies
endif
endif

# did the user declared a shaveapp in the subdir.mk? then build it!
ifdef shaveapp-id

# Here we can see the case of the SHAVEAPP id must be exactly the same as that
# in the configuration file. We choose not to convert it to uppercase or lcase
# as kconfig preserves the case when converting the kcnf file to the .config
ifeq "y" "$(CONFIG_USE_SHAVEAPP_$(shaveapp-id))"

ifeq "y" "$(CONFIG_SHAVEAPP_$(shaveapp-id)_TYPE_DYNAMIC)"
include $(MDK_INSTALL_DIR_ABS)/make/build-shaveapp-dynamic.mk

B_LIB_LDOPT+=-T $(MDK_INSTALL_DIR_ABS)/scripts/ld/shaveDynamicLoad/dyndata_shave_first_phase.ldscript

$(APP_SHAVEAPP_OBJECT_LIST):
	[[ ! -f $@ ]] && touch $@
	$(call LIST_TARGET_FILE)

SHAVEAPP_DATA=build-shaveapp-data
.PHONY: build-shaveapp-data
.ONESHELL:
build-shaveapp-data: $(BUILDDIR_TOP)/$(shaveapp-id)_shvXdata_sym.o $(BUILDDIR_TOP)/$(shaveapp-id)_shvXdata.o
	$(ECHO)echo $(BUILDDIR_TOP)/$(shaveapp-id)_shvXdata_sym.o >> $(APP_SHAVEAPP_OBJECT_LIST)
	echo $(BUILDDIR_TOP)/$(shaveapp-id)_shvXdata.o >> $(APP_SHAVEAPP_OBJECT_LIST)
	echo $(APP_SHAVEAPP_OBJECT_LIST) >> $(CLEAN_LIST_FILE)
	$(SILENT_INFO) "Built data for shaveapp $(shaveapp-id)"

endif

ifeq "y" "$(CONFIG_SHAVEAPP_$(shaveapp-id)_TYPE_STATIC)"
include $(MDK_INSTALL_DIR_ABS)/make/build-shaveapp-static.mk
endif

else
$(call VINFO, "Ignoring shaveapp $(shaveapp-id) during build as configured by the user")

endif
endif

# some objects may already have full paths so don't prefix them
OBJS_WITH_PATH=$(foreach obj,$(OBJS),$(if $(findstring $(dir $(abspath $(obj))),$(dir $(obj))),$(obj),$(addprefix $(BUILDDIR)/,$(obj))))

# Depending on the build system configuration choice BUILD_SUBDIRS_USING
# we'll create here either an object list, an archive or a partially-linked
# library. In all cases, the generated file will have the same name. Only it's
# contents will be different and that will be taken care of in that main
# build.mk
# In the case CONFIG_BUILD_SUBDIRS_USING_OBJECTS we replace spaces with LF to
# ease the readability of the generated files. Make will revert those back to
# spaces when processing the file
# TODO the build system should check the presence of `tr` and `sort` on the
# user's computer
#
.ONESHELL:
$(LIB): $(OBJS) $(lib-$(B_PROCESSOR)-deps-y)
	$(call LIST_TARGET_FILE)
ifdef MDK_DEBUG_OBJ_LIST
	echo "$(OBJS)" | tr ' ' '\n' | sort >> $(MDK_DEBUG_OBJ_LIST)
endif
ifeq "y" "$(CONFIG_BUILD_SUBDIRS_USING_OBJECTS)"
	$(SILENT_OBJ) echo "$(OBJS_WITH_PATH)" | tr ' ' '\n' | sort > $@
endif
ifeq "y" "$(CONFIG_BUILD_SUBDIRS_USING_AR)"
	$(B_SILENT_AR) -r $@ $(OBJS)
endif
ifeq "y" "$(CONFIG_BUILD_SUBDIRS_USING_LD)"
	$(B_SILENT_LD) -o $@ -Ur -EL $(OBJS) \
		$(B_LIB_LDOPT) -Map=$@.map \
		$(shell $(MDK_INSTALL_DIR_ABS)/make/list_libraries.sh $($(B_PROCESSOR)_MDK_LIBRARIES_LIST) $(CONFIG_BUILD_DEBUG))
	# 	$(build-trace-symbols-y)
	echo $@.map >> $(CLEAN_LIST_FILE)
endif

else

# nothing to collect from here, as there are no objects to link into a library
collect-$(B_PROCESSOR) : $(B_RECURSE_COLLECT) $(B_COLLECT_MISC)
	@echo "" > /dev/null

endif

$(call VINFO,LIB               = $(LIB))
PHONY += build-$(B_PROCESSOR)
build-$(B_PROCESSOR) : $(B_RECURSE_BUILD) $(if $(SHVXLIB),$(SHVXLIB),$(LIB)) $(SHAVEAPP_DATA) $(build-$(B_PROCESSOR)-deps-y)
	@echo > /dev/null

.PHONY : $(PHONY)

