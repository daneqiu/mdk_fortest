
include $(MDK_INSTALL_DIR)/make/lib.mk

TMPDIR ?= $(BUILDDIR)/tmp
######## target SOC settings #########
# This will capture all possible SOC targets as long as they keep with the
# correct notation in kcnf, e.g. CONFIG_TARGET_SOC_xxxxx
CONFIG_TARGET_SOC_LIST=$(filter CONFIG_TARGET_SOC_%,$(.VARIABLES))
CONFIG_TARGET_SOC=$(call lc,$(patsubst CONFIG_TARGET_SOC_%,%,$(foreach var,$(CONFIG_TARGET_SOC_LIST),$(if $(filter y,$($(var))),$(var)))))
ifeq "" "$(CONFIG_TARGET_SOC)"
$(warning No target SoC was configured. Aborting build)
endif
export CONFIG_TARGET_SOC

# TODO add this to kcnf
MV_SOC_PLATFORM = myriad2
export MV_SOC_PLATFORM

# TODO remove this SECTION once old build system is gone
# SECTION START
MV_COMMON_BASE = $(MDK_INSTALL_DIR_ABS)
# SECTION END
######## ------------------- #########

# some projects/examples use the name build, so use mvbuild here
BUILDDIR_NAME = mvbuild
BUILDDIR ?= $(abspath $(APPDIR)/$(BUILDDIR_NAME)/$(CONFIG_TARGET_SOC))
BUILDDIR_TOP ?= $(BUILDDIR)
export BUILDDIR_TOP BUILDDIR

BUILDDIR_REPORT ?= $(BUILDDIR_TOP)/report

# TODO migrate all the makefiles from DirAppRoot to APPDIR var
# FTM let the old DirAppRoot exist
DirAppRoot = $(APPDIR)
DirAppOutput = $(BUILDDIR)

MDK_INSTALL_DIR_ABS = $(abspath $(MDK_INSTALL_DIR))
MDK_TMPDIR = $(MDK_INSTALL_DIR)/tmp
# FIXME should this directory be more configurable, via a kcnf entry maybe?
MDK_PACKAGES_DIR_ABS = $(abspath $(MDK_INSTALL_DIR_ABS)/../packages)
export MDK_INSTALL_DIR MDK_INSTALL_DIR_ABS MDK_PACKAGES_DIR_ABS
export TMPDIR
export APPDIR
export DirAppRoot
export DirAppOutput

SHAVE_DYN_APPS_DIR_NAME=$(call unquote,$(CONFIG_APP_SHAVE_DYN_APPS_SRCS_DIR))
export SHAVE_DYN_APPS_DIR_NAME

SHAVEAPP_ELF_DEPENDENCIES_LIST = $(BUILDDIR_TOP)/shaveapp-elf-dependencies
export SHAVEAPP_ELF_DEPENDENCIES_LIST

define DEFINE_PROCESSOR_FILES
$(1)_INCLUDE_DIRS_LIST = $$(BUILDDIR)/$(2)-include-dirs
export $(1)_INCLUDE_DIRS_LIST
$(1)_CCOPT_LIST = $$(BUILDDIR)/$(2)-ccopt
export $(1)_CCOPT_LIST
$(1)_CPPOPT_LIST = $$(BUILDDIR)/$(2)-cppopt
export $(1)_CPPOPT_LIST
endef

$(eval $(call DEFINE_PROCESSOR_FILES,SHAVE,shave))
$(eval $(call DEFINE_PROCESSOR_FILES,LOS,los))
$(eval $(call DEFINE_PROCESSOR_FILES,LRT,lrt))

LOS_MDK_LIBRARIES_LIST = $(BUILDDIR_TOP)/los-mdk-libraries
LRT_MDK_LIBRARIES_LIST = $(BUILDDIR_TOP)/lrt-mdk-libraries
SHAVE_MDK_LIBRARIES_LIST = $(BUILDDIR_TOP)/shave-mdk-libraries
LOS_APP_LIBRARIES_LIST = $(BUILDDIR_TOP)/los-app-libraries
LRT_APP_LIBRARIES_LIST = $(BUILDDIR_TOP)/lrt-app-libraries
SHAVE_APP_LIBRARIES_LIST = $(BUILDDIR_TOP)/shave-app-libraries
SHAVEGROUP_LIBRARIES_LIST = $(BUILDDIR_TOP)/shavegroup-libraries-list

export LOS_MDK_LIBRARIES_LIST LOS_APP_LIBRARIES_LIST \
	LRT_MDK_LIBRARIES_LIST LRT_APP_LIBRARIES_LIST \
	SHAVE_MDK_LIBRARIES_LIST SHAVE_APP_LIBRARIES_LIST  \
	SHAVEAPP_ELF_DEPENDENCIES SHAVEGROUP_LIBRARIES_LIST


# TODO remove this when migration's done
# add a marker variable to help migration to this NextGen build system
CONFIG_BUILD_SYSTEM_NEXTGEN=y
export CONFIG_BUILD_SYSTEM_NEXTGEN

export MV_COMMON_BASE
include $(MDK_INSTALL_DIR_ABS)/make/compat.mk

