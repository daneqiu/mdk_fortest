#!/bin/bash

for l in $( cat "$1" ) ; do
  M=$( echo "$l" | cut -d ':' -f 1 )
  P=$( echo "$l" | cut -d ':' -f 2 )
  if [[ "$M" == "O"  ]] ; then
    if [[ -f "$P" ]] ; then
      cat "$P"
    else
      if [[ "$2" == "yes" ]] ; then
        (>&2 echo "list_libraries.sh: File not found: $P")
        exit 1
      fi
    fi
  fi
  [[ "$M" != "O"  ]] && echo $P
done

