
ifndef MDK_INSTALL_DIR_ABS
$(error This file was not launched by the main mdk.mk)
endif

include $(MDK_INSTALL_DIR)/make/dirs.mk
include $(MDK_INSTALL_DIR)/make/lib.mk

include $(MDK_INSTALL_DIR_ABS)/make/readconfig.mk

# TODO remove this once the old system is gone
include $(MDK_INSTALL_DIR_ABS)/generalsettings.mk
include $(MDK_INSTALL_DIR_ABS)/toolssettings.mk
# remove until here

ifeq ($(UNAME),GNU/Linux)
 MV_HOSTNAME=$(shell hostname)
 ifeq ($(findstring dub,$(MV_HOSTNAME)),dub)
  CF_ENABLE?=no
 else
  CF_ENABLE?=yes
 endif
endif

ifeq ($(CF_ENABLE),yes)
  MV_REMOTE_TOOLS_BASE?=https://d2pvd2prfslgxf.cloudfront.net
else
  MV_REMOTE_TOOLS_BASE?=https://s3-eu-west-1.amazonaws.com/movi-tools
endif
MV_REMOTE_TOOLS_AUTH?=internal-b37c6320-f6d4-11e3-a3ac-0800200c9a66
MV_REMOTE_TOOLS_FILE?=movi-tools-$(MV_TOOLS_VERSION).tgz
MV_REMOTE_TOOLS_URL?=$(MV_REMOTE_TOOLS_BASE)/$(MV_REMOTE_TOOLS_AUTH)/$(MV_REMOTE_TOOLS_FILE)
MV_TOOLS_DOWN_TGZ=$(MV_TOOLS_DIR)/$(MV_REMOTE_TOOLS_FILE)
MV_FORCE_TOOLS?=no


getTools:
	@if [ ! -d $(MV_TOOLS_DIR) ]; then \
		echo "WARNING: MV_TOOLS_DIR:$(MV_TOOLS_DIR) does not exist, creating it ..."; \
		mkdir -p $(MV_TOOLS_DIR); \
	fi;
	@echo "Downloading toolchain $(MV_TOOLS_VERSION) to $(MV_TOOLS_DIR)"
	@if [ "$(MV_TOOLS_VERSION)" = "Latest" ] || [ "$(MV_TOOLS_VERSION)" = "Master" ] ; then \
		echo "WARNING: make getTools does not support tools version -> $(MV_TOOLS_VERSION)"; \
		echo "Please use svn update $(MV_TOOLS_DIR) to update $(MV_TOOLS_VERSION)"; \
		exit 0; \
	fi;
	@if [ "$(MV_FORCE_TOOLS)" = "yes" ]; then \
		echo "Forcing the download of fresh tools by removing existing toolchain directory: $(MV_TOOLS_BASE)"; \
		rm -rf $(MV_TOOLS_BASE); \
	fi;
	@if [ ! -d $(MV_TOOLS_BASE) ]; then \
		rm -f $(MV_TOOLS_DOWN_TGZ); \
		wget --no-check-certificate $(MV_REMOTE_TOOLS_URL) -P $(MV_TOOLS_DIR); \
		tar xzvf $(MV_TOOLS_DOWN_TGZ) -C $(MV_TOOLS_DIR); \
		rm $(MV_TOOLS_DOWN_TGZ); \
	else \
		echo "Tools directory $(MV_TOOLS_BASE) already exists. Use MV_FORCE_TOOLS=yes to overwrite"; \
	fi;

