include $(APPDIR)/.config
include $(MDK_INSTALL_DIR_ABS)/make/lib.mk
include $(MDK_INSTALL_DIR_ABS)/make/build-lib.mk
include $(MDK_INSTALL_DIR_ABS)/toolssettings.mk

#========================================
# collect for processor LOS
C_PROCESSOR=los

#========================================

include $(MDK_INSTALL_DIR_ABS)/make/collect-processor.mk

