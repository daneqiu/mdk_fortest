ifneq ($(CONFIG_NOCOLOR),y)
t_blk=$(shell echo -e '\e[0;30m')
t_red=$(shell echo -e '\e[0;31m')
t_grn=$(shell echo -e '\e[0;32m')
t_ylw=$(shell echo -e '\e[0;33m')
t_blu=$(shell echo -e '\e[0;34m')
t_pur=$(shell echo -e '\e[0;35m')
t_cyn=$(shell echo -e '\e[0;36m')
t_wht=$(shell echo -e '\e[0;37m')

tb_blk=$(shell echo -e '\e[1;30m')
tb_red=$(shell echo -e '\e[1;31m')
tb_grn=$(shell echo -e '\e[1;32m')
tb_ylw=$(shell echo -e '\e[1;33m')
tb_blu=$(shell echo -e '\e[1;34m')
tb_pur=$(shell echo -e '\e[1;35m')
tb_cyn=$(shell echo -e '\e[1;36m')
tb_wht=$(shell echo -e '\e[1;37m')

tu_blk=$(shell echo -e '\e[4;30m')
tu_red=$(shell echo -e '\e[4;31m')
tu_grn=$(shell echo -e '\e[4;32m')
tu_ylw=$(shell echo -e '\e[4;33m')
tu_blu=$(shell echo -e '\e[4;34m')
tu_pur=$(shell echo -e '\e[4;35m')
tu_cyn=$(shell echo -e '\e[4;36m')
tu_wht=$(shell echo -e '\e[4;37m')

bg_blk=$(shell echo -e '\e[40m')
bg_red=$(shell echo -e '\e[41m')
bg_grn=$(shell echo -e '\e[42m')
bg_ylw=$(shell echo -e '\e[43m')
bg_blu=$(shell echo -e '\e[44m')
bg_pur=$(shell echo -e '\e[45m')
bg_cyn=$(shell echo -e '\e[46m')
bg_wht=$(shell echo -e '\e[47m')
col_rst=$(shell echo -e '\e[0m')
endif


ifeq (0,${MAKELEVEL})
ifeq ($(CONFIG_THREADED),y)
	MAKEFLAGS+=-j$(CONFIG_THREADS)
endif
endif

ifeq "$(CONFIG_BUILD_VERBOSE)" "y"

ECHO:=
MAKE_TRACE = --trace
VMDK=$(tb_ylw)mdk $(col_rst)
V= @echo '$(VMDK)'

define VINFO
$(info $(tb_ylw)$(VMDK) $(col_rst)$(1))
endef

BASH_VECHO=echo $(1)

else

ECHO?=@
MAKE_TRACE=
V=
define VINFO
endef
BASH_VECHO=:

endif

ifneq ($(CONFIG_BUILD_VERBOSE),y)
   # TODO remove this?
   # SILENT_VER      = @echo '  $(tb_ylw)[VERSION]$(col_rst)  ' "$(VERSION_MAJOR).$(VERSION_MINOR), $(VERSION_CODENAME); git commit: $(VERSION_GIT)  ";
   SILENT_CFG      = @echo '  $(tb_cyn)[CFG]$(col_rst)       '
   SILENT_LOSCC    = @echo '  $(tb_ylw)[LOS-CC]$(col_rst)   ' $(subst $(BUILDDIR),,$(@:$(BUILDDIR)/%.o=%.c));
   SILENT_LOSCXX   = @echo '  $(tb_ylw)[LOS-CXX]$(col_rst)   ' $(subst $(BUILDDIR),,$(@:$(BUILDDIR)/%.o=%.c));
   SILENT_LRTCC    = @echo '  $(tb_ylw)[LRT-CC]$(col_rst)   ' $(subst $(BUILDDIR),,$(@:$(BUILDDIR)/%.o=%.c));
   SILENT_LRTCXX   = @echo '  $(tb_ylw)[LRT-CXX]$(col_rst)   ' $(subst $(BUILDDIR),,$(@:$(BUILDDIR)/%.o=%.c));
   SILENT_MVCC     = @echo '  $(tb_ylw)[MVCC]$(col_rst)     ' $(subst $(BUILDDIR),,$(@:$(BUILDDIR)/%.o=%.c));
   SILENT_ASM      = @echo '  $(tb_ylw)[ASM]$(col_rst)      ' $@;
   SILENT_AS       = @echo '  $(tb_ylw)[AS]$(col_rst)       ' $(subst $(BUILDDIR),,$(@:$(BUILDDIR)/%.o=%.S));
   SILENT_CXX      = @echo '  $(tb_ylw)[CXX]$(col_rst)      ' $(subst $(BUILDDIR),,$(@:$(BUILDDIR)/%.o=%.cpp));
   SILENT_MVCXX    = @echo '  $(tb_ylw)[MVCXX]$(col_rst)    ' $(subst $(BUILDDIR),,$(@:$(BUILDDIR)/%.o=%.cpp));
	 SILENT_URC      = @echo '  $(tb_pur)[USBRC]$(col_rst)    ' $(subst $(BUILDDIR),,$(@:$(BUILDDIR)/%.c=%.urc));
   SILENT_LD       = @echo '  $(tb_pur)[LD]$(col_rst)       ' $@;
	 SILENT_LDSCRIPT = @echo '  $(tb_pur)[LDSCRIPT]$(col_rst) ' $@;
   SILENT_MVLNK    = @echo '  $(tb_pur)[mvLink]$(col_rst)   ' $@;
   SILENT_AR       = @echo '  $(tb_pur)[AR]$(col_rst)       ' $@;
   SILENT_OBJ      = @echo '  $(tb_pur)[OBJ-LIST]$(col_rst) ' $@;
   SILENT_GEN      = @echo '  $(tb_cyn)[GEN]$(col_rst)      ' $(subst $(BUILDDIR),,$(@));
   SILENT_GEN2     = @echo '  $(tb_cyn)[GEN]$(col_rst)      ' $(subst $(BUILDDIR),,$$(@));
	 SILENT_MVCONV   = @echo '  $(tb_pur)[MVCONV]$(col_rst)   ' $@;

   SILENT_DL       = @echo '  $(tb_cyn)[DOWNLOAD]$(col_rst) ' $(PACKAGE);
   TILENT_PATCH    = @echo '  $(tb_cyn)[PATCH]$(col_rst)    ' $(PACKAGE);
   SILENT_CHECK    = @echo '  $(tb_cyn)[CHECK]$(col_rst)    ' $(1);
   SILENT_UPDATE   = @echo '  $(tb_cyn)[UPD]$(col_rst)      ' $(PACKAGE);
   SILENT_PARSE    = @echo '  $(tb_cyn)[PARSE]$(col_rst)    '
   SILENT_OBJCOPY  = @echo '  $(tb_blu)[OBJCOPY]$(col_rst)  ' $(@F);
   SILENT_BISON    = @echo '  $(tb_pur)[BISON]$(col_rst)    ' $(@F);
   SILENT_TAGS     = @echo '  $(tb_pur)[TAGS]$(col_rst)     ' Generating TAGS file ;
   SILENT_FLEX     = @echo '  $(tb_pur)[FLEX]$(col_rst)     ' $(@F);
   SILENT_CLEAN    = @echo '  $(tb_pur)[CLEAN]$(col_rst)    ' cleaning up build directory and temporary files;
   SILENT_MRPROPER = @echo '  $(tb_pur)[MRPROPER]$(col_rst) ' purging everything;

   SILENT_GPERF    = @echo '  $(tb_pur)[GPERF]$(col_rst)    ' $(@F);

   SILENT_EXTRA    = @echo '  $(tb_pur)[EXTRA]$(col_rst)    ' $$(@);

   SILENT_DISAS    = @echo '  $(tb_blu)[DISAS]$(col_rst)    ' $(@:$(BUILDDIR)/%.bin=%);
   SILENT_OBJDUMP  = @echo '  $(tb_blu)[OBJDUMP]$(col_rst)  ' $(OBJDUMP);
	 SILENT_READELF  = @echo '  $(tb_blu)[READELF]$(col_rst)  ' ;
   SILENT_HOSTCC   = @echo '  $(tb_ylw)[HOSTCC]$(col_rst)   ' $(@:$(BUILDDIR)/%.o=%.c);
   SILENT_HEX2BIN  = @echo '  $(tb_pur)[HEX2BIN]$(col_rst)   ' $(@);

   #Shut up this crap
   MAKEFLAGS+=--no-print-directory
endif

SILENT_INFO	   = @echo '  $(tb_grn)[INFO]$(col_rst)     '
SILENT_ERROR   = @echo '  $(tb_red)[ERR]$(col_rst)      '

define ldmakes
$(eval subdirs=$(addprefix $(1),$(filter %/,$(objects-y))))
$(eval objects+=$(addprefix $(BUILDDIR)$(1), $(filter %.o,$(objects-y))))
$(eval alien+=$(addprefix $(BUILDDIR)$(1), $(filter %.mk,$(objects-y))))
$(eval objects-y= )
$(foreach dir, $(subdirs),$(eval include $(dir)Makefile) $(call includedir,$(dir)) $(call ldmakes,$(dir)))
endef

check_root=$(if $(1),sudo,)

#Code generation helpers

define newline


endef


define c_define_or
#define $(1) ( 0 $(foreach el, $(2),| $(el)) )
endef

define c_header_start
/* autogenerated, do not edit */
#ifndef $(1)
#define $(1)
endef

define c_header_end
#endif
endef

define generate
$(subst $(newline),\n,$(call $(1),$(2),$(3),$(4)))
endef

# var, kconfig, append_if_y, append_if_n
define check_and_append
ifeq ($$(CONFIG_$(2)),y)
$(1)+=$(3)
else
$(1)+=$(4)
endif
endef

# var, kconfig, define, append_if_y, append_if_n
define check_and_define
ifeq ($$(CONFIG_$(2)),y)
$(1)+=\#define $(3) $(4)\n
else
$(1)+=\#define $(3) $(5)\n
endif
endef

dquote="
# give another double quote to help highlighting "
unquote=$(strip $(subst $(dquote),,$(1)))

define NOP_TARGET
$(1):
	@echo "" > /dev/null

endef

# TODO make this target display more conveniently multiline error messages
comma := ,
define ERROR_TARGET
$(1):
	$$(SILENT_ERROR) $(2)
	@exit 1
endef

# This macro will get you the directory where the calling makefile is located
# in. Make sure you call it right before the place where you use the returned
# value and not after calling inclluded makefiles, as these, in their turn,
# may use a variable named the same, changing it on their way
GET_CURRENT_DIR = $(abspath $(dir $(lastword $(MAKEFILE_LIST))))

lc = $(subst A,a,$(subst B,b,$(subst C,c,$(subst D,d,$(subst E,e,$(subst F,f,$(subst G,g,$(subst H,h,$(subst I,i,$(subst J,j,$(subst K,k,$(subst L,l,$(subst M,m,$(subst N,n,$(subst O,o,$(subst P,p,$(subst Q,q,$(subst R,r,$(subst S,s,$(subst T,t,$(subst U,u,$(subst V,v,$(subst W,w,$(subst X,x,$(subst Y,y,$(subst Z,z,$1))))))))))))))))))))))))))

LC = $(subst a,A,$(subst b,B,$(subst c,C,$(subst d,D,$(subst e,E,$(subst f,F,$(subst g,G,$(subst h,H,$(subst i,I,$(subst j,J,$(subst k,K,$(subst l,L,$(subst m,M,$(subst n,N,$(subst o,O,$(subst p,P,$(subst q,Q,$(subst r,R,$(subst s,S,$(subst t,T,$(subst u,U,$(subst v,V,$(subst w,W,$(subst x,X,$(subst y,Y,$(subst z,Z,$1))))))))))))))))))))))))))

