
# read-in the current project's configuration settings and also set some
# implicit variables from that. This should be included by all the other main
# makefiles.
#
# please keep this strictly config-related


# Let the user continue using the classical MV_TOOLS_VERSION but initialize it
# from the configuration file if it's not yet specified
# add a marker variable to help migration to this NextGen build system
-include $(APPDIR)/.config
CONFIG_BUILD_SYSTEM_NEXTGEN=y
export CONFIG_BUILD_SYSTEM_NEXTGEN

ifneq "y" "$(CONFIG_USE_DEFAULT_TOOLS_VERSION)"
MV_TOOLS_VERSION = $(call unquote,$(CONFIG_TOOLS_VERSION))
ifeq "" "$(or $(MV_TOOLS_VERSION),$(filter-out $(firstword $(MAKECMDGOALS)),menuconfig nconfig qconfig gconfig))"
$(error [TOOLS] Tools version overriden but with an empty value)
endif
$(info Using local tools version: $(MV_TOOLS_VERSION))
endif

