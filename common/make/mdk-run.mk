ifndef MDK_INSTALL_DIR_ABS
$(error This file was not launched by the main mdk.mk)
endif

include $(MDK_INSTALL_DIR_ABS)/make/readconfig.mk
include $(MDK_INSTALL_DIR_ABS)/generalsettings.mk


include $(MDK_INSTALL_DIR_ABS)/make/dirs.mk
include $(MDK_INSTALL_DIR_ABS)/make/ports.mk
include $(MDK_INSTALL_DIR_ABS)/make/lib.mk
include $(MDK_INSTALL_DIR_ABS)/toolssettings.mk

defaultDSCR =  $(DirDbgScrCommon)/default_debug_mdbg2.scr
defaultRSCR =  $(DirDbgScrCommon)/default_run_mdbg2.scr
defaultPSCR =  $(DirDbgScrCommon)/default_pipe_mdbg2.scr
defaultTPSCR = $(DirDbgScrCommon)/tcp_pipe_mdbg2.scr

MVDBG_SCRIPT_OPT  ?= -D:default_target=$(MV_DEFAULT_START_PROC_ID) $(MVDBGOPT) -srvIP:$(srvIP) -serverPort:$(srvPort) -D:elf=$(BUILDDIR_TOP)/$(call unquote,$(CONFIG_OUTPUT_FILE_NAME)).elf

.PHONY: start_server
start_server:
	$(ECHO) while true; \
		do $(MVSVR) $(MV_SRV_EXTRA_OPT) -tcpPort:$(srvPort); \
		[[ $$? == 0 ]] && break; \
	done

start_simulator:
	$(ECHO) while true; do $(MVSIM) $(MVSIMOPT) -tcpip:$(srvPort) -q; done

ifeq (y,$(CONFIG_OUTPUT_TYPE_ELF))

$(BUILDDIR_TOP):
	$(SILENT_ERROR) "Please run 'make all' in order to build the binaries prior to running or debugging"
	$(ECHO)false

run: $(BUILDDIR_TOP)
	@echo  $(defaultPSCR)
	@cp $(SourceDebugScript) $(debugSCR)
	@if [[ "$(SourceDebugScript)" == *"$(DirDbgScrCommon)"* ]]; \
	then \
		if [[ "$(CONFIG_USE_COMPONENT_PIPEPRINT)" == "y" ]]; \
		then \
			cat "$(defaultPSCR)" >> "$(debugSCR)"; \
		fi; \
		cat "$(defaultRSCR)" >> "$(debugSCR)"; \
	fi;
	$(MVDBG) $(MVDBG_SCRIPT_OPT) --script $(debugSCR) -D:run_opt=runw -D:exit_opt=exit  $(debugOptions)

debug: $(BUILDDIR_TOP)
	@# This target is used to load and run your application, but interactive debug is still possible during exection
	@# It performs load app; run;
	@cp $(SourceDebugScript) $(debugSCR)
	$(ECHO) if [[ "$(SourceDebugScript)" == *"$(DirDbgScrCommon)"* ]]; \
	then \
		if [[ "$(CONFIG_USE_COMPONENT_PIPEPRINT)" == "y" ]]; \
		then \
			cat "$(defaultPSCR)" >> "$(debugSCR)"; \
		fi; \
		cat "$(defaultDSCR)" >> "$(debugSCR)"; \
	fi;
	$(MVDBG) $(MVDBG_SCRIPT_OPT) --interactive --script $(debugSCR) -D:run_opt=run $(debugOptions)

# This target is used to simply start debugger and load your application ready for interactive debug
# It performs load app;
load:
	@cp $(SourceDebugScript) $(debugSCR)
	@if [[ "$(SourceDebugScript)" == *"$(DirDbgScrCommon)"* ]]; \
	then \
		if [[ "$(CONFIG_USE_COMPONENT_PIPEPRINT)" == "y" ]]; \
		then \
			cat "$(defaultPSCR)" >> "$(debugSCR)"; \
		fi; \
		cat "$(defaultDSCR)" >> "$(debugSCR)"; \
	fi;
	@cp $(debugSCR) $(debugSCR).tmp
	@echo "source $(MV_COMMON_BASE)/scripts/debug/mdkTcl/mdkTclStart.tcl" > "$(debugSCR)"; \
	 cat $(debugSCR).tmp  >> $(debugSCR)
	@rm  $(debugSCR).tmp
	$(MVDBG) $(MVDBG_SCRIPT_OPT) --script $(debugSCR) --interactive -D:run_opt=halt -D:exit_opt=" "

else

$(eval $(call ERROR_TARGET,run,"run target is not supported for this kind of build output"))
$(eval $(call ERROR_TARGET,debug,"debug target is not supported for this kind of build output"))

endif

