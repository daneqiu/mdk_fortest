
# this makefiles contains constructs meant to help transition from the current
# build system to the NextGen build system
#

# TODO remove this file when no longer needed

include $(MDK_INSTALL_DIR_ABS)/make/lib.mk

# recognize the old MV_SOC_REV option but please not this is quite dangerous
# the target SOC should only be configured via kconfig interface in order to
# get the dependencies correctly updated, if any (and you'll never know there
# are any of them before hand)
ifdef MV_SOC_REV

define MV_SOC_REV_WARNING
WARNING: MV_SOC_REV definition was defined in $(origin MV_SOC_REV)
Please consider it's use is now obsolete. Use kconfig for setting this.
endef

$(warning $(MV_SOC_REV_WARNING))

MV_SOC_REV_UCASE=$(call LC,$(MV_SOC_REV))

SOC_VARS=$(filter CONFIG_TARGET_SOC_%,$(.VARIABLES))
ifneq "" "$(SOC_VARS)"

ifneq "y" "$(CONFIG_TARGET_SOC_$(MV_SOC_REV_UCASE))"
$(warning Overriding the configured target SOC to $(MV_SOC_REV). Dependencies may not be correctly updated.)

# clear any CONFIG_TARGET_SOC_xxx variable that was configured with kconfig
$(foreach var,$(SOC_VARS),$(eval undefine $(var)))

$(eval CONFIG_TARGET_SOC_$(MV_SOC_REV_UCASE)="y")

endif

endif

endif
