#!/bin/bash

# IMPORTANT NOTE:
# this file uses real TAB characters for indentation it it should stay that way
# please use a decent text editor able to preserve the tabs if not the script here-documents will break

# TODO correctly detect and set RTEMS operating mode for LOS and eventually LRT

echo "Migrating application in directory $(pwd)"
[ ! -f Makefile ] && echo "No makefile found! Aborting." && exit 1

# TODO the new makefile should be Makefile.new and the old one should stay the same

MAIN_MAKEFILE=Makefile
OLD_MAKEFILE=oldMakefile.mk
git mv $MAIN_MAKEFILE $OLD_MAKEFILE

LOS_MAKEFILE=leon/subdir.mk
LRT_MAKEFILE=leon_rt/subdir.mk
SHAVE_MAKEFILE=shave/subdir.mk
SHAVEAPPS_MAKEFILE=shaveDynApps/subdir.mk

truncate -s 0 kcnf
truncate -s 0 $MAIN_MAKEFILE

[ -d leon ] && touch $LOS_MAKEFILE && export HAS_LOS=yes
[ -d leon_rt ] && touch $LRT_MAKEFILE && export HAS_LRT=yes
[ -d shave ] && touch $SHAVE_MAKEFILE && export HAS_SHAVE=yes
[ -d shaveDynApps ] && touch $SHAVEAPPS_MAKEFILE && export HAS_SHAVEAPPS=yes

MDK_COMMON=$(grep "^MV_COMMON_BASE" oldMakefile.mk | cut -d = -f 2 | sed "s/^[ ]*//" )

cat > Makefile <<makefile_text
MDK_INSTALL_DIR = $MDK_COMMON

include \$(MDK_INSTALL_DIR)/mdk.mk

$(grep "^TEST" $OLD_MAKEFILE)
makefile_text

if [ "$HAS_LOS" == "yes" ] ; then
  LOS_CCOPT=$(grep "^CCOPT" $OLD_MAKEFILE | perl -pe 's|^.*?=(.*)|\1|')
	if [ ! -z "LOS_CCOPT" ] ; then
		for opt in $LOS_CCOPT ; do
			cat >> $LOS_MAKEFILE <<-los_makefile_text
			ccopt-los-y+=$opt
			los_makefile_text
		done
  fi
	cat >> $LOS_MAKEFILE <<-los_makefile_text
	srcs-los-y += $(find leon -name '*.c' -o -name '*.cpp' -o -name '*.asm' | sed "s/leon\///" | xargs echo | sed "s/ / \\\\\n  /g")
	los_makefile_text
fi

if [ "$HAS_LRT" == "yes" ] ; then
	LRT_CCOPT=$(grep "^CCOPT_LRT" $OLD_MAKEFILE | cut -d "=" -f 2)
	if [ ! -z "$LRT_CCOPT" ] ; then
		for opt in $LRT_CCOPT ; do
			cat >> $LRT_MAKEFILE <<-lrt_makefile_text
			ccopt-lrt-y+=$opt
			lrt_makefile_text
		done
	fi
	cat >> $LRT_MAKEFILE <<-lrt_makefile_text
	srcs-lrt-y += $(find leon_rt -name '*.c' -o -name '*.cpp' -o -name '*.asm' | sed "s/leon_rt\///" | xargs echo | sed "s/ / \\\\\n  /g")
	lrt_makefile_text
fi

if [ "$HAS_SHAVE" == "yes" ] ; then
	SHAVE_CCOPT=$(grep "^MVCCOPT" $OLD_MAKEFILE | cut -d "=" -f 2)
	if [ ! -z $SHAVE_CCOPT  ] ; then
		for opt in $SHAVE_CCOPT ; do
			cat >> $SHAVE_MAKEFILE <<-shave_makefile_text
			ccopt-shave-y+=$opt
			shave_makefile_text
		done
	fi
	cat >> $SHAVE_MAKEFILE <<-shave_makefile_text
	srcs-shave-y += $(find shave -name '*.c' -o -name '*.cpp' -o -name '*.asm' | sed "s/shave\///" | xargs echo | sed "s/ / \\\\\n  /g")
	shave_makefile_text
fi

if [ "$HAS_SHAVEAPPS" == "yes" ] ; then
	echo "subdirs-shave-y+=\\" > $SHAVEAPPS_MAKEFILE
  SHAVEAPP_LIST=$(cd shaveDynApps && find . ! -path . -type d | sed "s/\.\///")
  for app in $SHAVEAPP_LIST ; do
		echo "  $app \\" >> $SHAVEAPPS_MAKEFILE
    pushd shaveDynApps/$app
    touch subdir.mk
    cat >> subdir.mk <<-shave_makefile_text
		shaveapp-id=$app
		srcs-shave-y += $(find . -name '*.c' -o -name '*.cpp' -o -name '*.asm' | sed "s/\.\///" | xargs echo | sed "s/ / \\\\\n  /g")
		shave_makefile_text
    git add subdir.mk
    popd

    cat >> kcnf <<-shaveapp_config
		shaveapp $app
		  prompt "$app SHAVE dynamic application"
		  shavegroup SHAVE_GROUP_DEFAULT
		  entrypoints "Entry"
		  help
		    This is the help for the $app SHAVE dynamic application

		shaveapp_config
  done
  git add $SHAVEAPPS_MAKEFILE
fi

make alldefconfig
APP_NAME=$(basename $(pwd))
sed -i "s/^\(CONFIG_OUTPUT_FILE_NAME=\).*\"/\\1\"$APP_NAME\"/" .config

if [ "$HAS_SHAVEAPPS" == "yes" ] ; then
	sed -i "s/^\(CONFIG_HAS_SHAVE_DYN_APPS=\).*/\\1y/" .config
	# TODO also set SHAVEAPP_MesgOne_TYPE here to dynamic if not application won't build
fi

# TODO check the CONFIG_HAS_LRT_SRCS and the related options if HAS_LRT is set

git add .config
git add kcnf
git add Makefile
git add $LOS_MAKEFILE
git add $LRT_MAKEFILE
git add $SHAVE_MAKEFILE

echo "Application was now migrated. Here is the git status:"
git status

read -p "Would you like me to launch 'make menuconfig'?" yn
case $yn in
	[Yy]* )
		make menuconfig
		;;
	* )
		echo "Not launching 'make menuconfig'"
esac

echo "Done."

# vim: set tw=0: set textmargin=0: set shiftwidth=2: set softtabstop=0: set noexpandtab :

