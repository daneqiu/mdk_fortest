
ifndef MDK_INSTALL_DIR_ABS
$(error This file was not launched by the main mdk.mk)
endif

include $(MDK_INSTALL_DIR_ABS)/make/readconfig.mk
include $(MDK_INSTALL_DIR_ABS)/generalsettings.mk


include $(MDK_INSTALL_DIR_ABS)/make/dirs.mk
include $(MDK_INSTALL_DIR_ABS)/make/lib.mk
include $(MDK_INSTALL_DIR_ABS)/make/build.mk

