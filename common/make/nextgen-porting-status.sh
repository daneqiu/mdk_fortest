#!env /bin/bash

# This script is intended to be run from the root of a MDK clone
# It'll output the list of applications with their ported status

MAKEFILES=$(find . -type f -name Makefile)
NEW_MK_NAME=newMakefile.mk

for mk in $MAKEFILES; do
  IS_NEXTGEN=0
  # maybe the Makefile is already next gen
  MAKE_OUTPUT=$(make -f $mk isNextGen 2>/dev/null)
  [[ $? -eq 0 ]] && IS_NEXTGEN=1
  # or perhaps into that directory we have newMakefile.mk
  NEW_MK=$(dirname $mk)/$NEW_MK_NAME
  [[ -f $NEW_MK ]] && make -C $(dirname $mk) -f $NEW_MK_NAME isNextGen 2>&1 >/dev/null && IS_NEXTGEN=1

  # output status
  echo "$(dirname $mk),$IS_NEXTGEN"
done

