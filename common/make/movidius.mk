#
# Definition of Movidius-specific targets that are not meant to be included in
# the MDK release. This is roughly equivalent to the old inhousetargets.mk
#

print-%  : ; @echo $* = $($*)

getToolsVersion:
	@echo "Movidius Tools version "$(MV_TOOLS_VERSION)
	@$(MVCC) --version
	@$(CC) --version

ifeq ($(CF_ENABLE),yes)
  MV_REMOTE_TOOLS_BASE?=https://d2pvd2prfslgxf.cloudfront.net
else
  MV_REMOTE_TOOLS_BASE?=https://s3-eu-west-1.amazonaws.com/movi-tools
endif
MV_REMOTE_TOOLS_AUTH?=internal-b37c6320-f6d4-11e3-a3ac-0800200c9a66
MV_REMOTE_TOOLS_FILE?=movi-tools-$(MV_TOOLS_VERSION).tgz
MV_REMOTE_TOOLS_URL?=$(MV_REMOTE_TOOLS_BASE)/$(MV_REMOTE_TOOLS_AUTH)/$(MV_REMOTE_TOOLS_FILE)
MV_TOOLS_DOWN_TGZ=$(MV_TOOLS_DIR)/$(MV_REMOTE_TOOLS_FILE)
MV_FORCE_TOOLS?=no

# This function is used to download a version of the tools from Amazon S3 service
# usage: make getTools  -> Downloads current MV_TOOLS_VERSION
#        make getTools MV_TOOLS_VERSION=00.50.44.3 -> Download specific version
#        make getTools MV_TOOLS_VERSION=00.50.44.2 MV_FORCE_TOOLS=yes -> delete current 00.50.44.2 and download a fresh copy


getTools:
	@if [ ! -d $(MV_TOOLS_DIR) ]; then \
		echo "WARNING: MV_TOOLS_DIR:$(MV_TOOLS_DIR) does not exist, creating it ..."; \
		mkdir -p $(MV_TOOLS_DIR); \
	fi;
	@echo "Downloading toolchain $(MV_TOOLS_VERSION) to $(MV_TOOLS_DIR)"
	@if [ "$(MV_TOOLS_VERSION)" = "Latest" ] || [ "$(MV_TOOLS_VERSION)" = "Master" ] ; then \
		echo "WARNING: make getTools does not support tools version -> $(MV_TOOLS_VERSION)"; \
		echo "Please use svn update $(MV_TOOLS_DIR) to update $(MV_TOOLS_VERSION)"; \
		exit 0; \
	fi;
	@if [ "$(MV_FORCE_TOOLS)" = "yes" ]; then \
		echo "Forcing the download of fresh tools by removing existing toolchain directory: $(MV_TOOLS_BASE)"; \
		rm -rf $(MV_TOOLS_BASE); \
	fi;
	@if [ ! -d $(MV_TOOLS_BASE) ]; then \
		rm -f $(MV_TOOLS_DOWN_TGZ); \
		wget --no-check-certificate $(MV_REMOTE_TOOLS_URL) -P $(MV_TOOLS_DIR); \
		tar xzvf $(MV_TOOLS_DOWN_TGZ) -C $(MV_TOOLS_DIR); \
		rm $(MV_TOOLS_DOWN_TGZ); \
	else \
		echo "Tools directory $(MV_TOOLS_BASE) already exists. Use MV_FORCE_TOOLS=yes to overwrite"; \
	fi;

