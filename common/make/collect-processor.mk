
# FIXME find-out why the collect or build phase creates the same identical set
# of directories into app/los and app/shave (and eventually app/lrt)

MDK_SUBDIR_ABS=$(abspath $(MDK_SUBDIR))
include $(MDK_SUBDIR)/$(SUBDIR_MAKEFILE_NAME)

COLLECTED_NAME = .$(C_PROCESSOR)-collected

C_RECURSE_SUBDIRS=$(subdirs-$(C_PROCESSOR)-y)
C_RECURSE_COLLECT=$(foreach dir,$(C_RECURSE_SUBDIRS),$(BUILDDIR)/$(dir)/$(COLLECTED_NAME))

C_INCLUDES = $(addprefix -I ,$(sort $(C_INCLUDE_DEFAULT)))
C_INCLUDES += $(addprefix -I ,$(sort $(include-dirs-$(C_PROCESSOR)-y)))

ASM_INCLUDE_DIRS_LIST=$(INCLUDE_DIRS_LIST)-asm

$(call VINFO,MDK_SUBDIR        = $(MDK_SUBDIR))
$(call VINFO,BUILDDIR          = $(BUILDDIR))
$(call VINFO,C_RECURSE_SUBDIRS = $(C_RECURSE_SUBDIRS))
$(call VINFO,C_RECURSE_COLLECT = $(C_RECURSE_COLLECT))
$(call VINFO,C_INCLUDES        = $(C_INCLUDES))
$(call VINFO,C_INCLUDE_DEFAULT = $(C_INCLUDE_DEFAULT))
# Pattern: execute some MDK .mk makefile in a MDK component or user-directory,
# specifying a target to collect defines or include dirs
# $1: a file name used as a collect marger into the build directory
# FIXME this can be factored-out with BUILD_MAKE_SUBDIR, with the slight price
# of less readability; do it later when build system will be stabilized
define BUILD_COLLECT_RECURSE_SUBDIR
.ONESHELL:
%/$(COLLECTED_NAME) :
	$(ECHO)$$(eval SUBDIR=$$(subst $$(BUILDDIR)/,,./$$*))
	$$(eval NEXT_MDK_SUBDIR=$$(MDK_SUBDIR)/$$(SUBDIR))
	$$(eval BUILD_DIR=$$(subst ..,_,$$*))
	$$(ECHO)[ -d $$(BUILD_DIR) ] || mkdir -p $$(BUILD_DIR)
	if [ -f "$$(NEXT_MDK_SUBDIR)/$(SUBDIR_MAKEFILE_NAME)" ] ; then \
		$$(MAKE) $$(MAKE_TRACE) MDK_SUBDIR=$$(NEXT_MDK_SUBDIR) \
			BUILDDIR=$$(BUILD_DIR) \
			SRCS=$(1) \
			INCLUDE_DIRS_LIST=$(2) \
			CCOPT_LIST=$(3) \
			CPPOPT_LIST=$(4) \
			-C $$(NEXT_MDK_SUBDIR) \
			-f $$(MDK_INSTALL_DIR_ABS)/make/collect-$(5).mk \
			$(6) ; \
	else \
			[[ "$(CONFIG_BUILD_DEBUG)" -eq "y" ]] && $(call BASH_VECHO,"WARNING: The directory '$$(NEXT_MDK_SUBDIR)/$(SUBDIR_MAKEFILE_NAME)' should contain a local makefile named '$(SUBDIR_MAKEFILE_NAME)'") || true ; \
	fi
endef

#$(eval $(call BUILD_COLLECT_RECURSE_SUBDIR,srcs-$(C_PROCESSOR)-y,$(INCLUDE_DIRS_LIST),$(CCOPT_LIST),$(CPPOPT_LIST),$(C_PROCESSOR),collect-makefiles))
$(eval $(call BUILD_COLLECT_RECURSE_SUBDIR,srcs-$(C_PROCESSOR)-y,$(INCLUDE_DIRS_LIST),$(CCOPT_LIST),$(CPPOPT_LIST),$(C_PROCESSOR),collect-$(C_PROCESSOR)))

# by now, we might be picked-up the name of the shaveapp from the shaveapp
# makefile, so check it and bail-out if user forgot to add it
ifdef shaveapp-id

# Here we can see the case of the SHAVEAPP id must be exactly the same as that
# in the configuration file. We choose not to convert it to uppercase or lcase
# as kconfig preserves the case when converting the kcnf file to the .config
ifeq "y" "$(CONFIG_USE_SHAVEAPP_$(shaveapp-id))"

ifeq "y" "$(CONFIG_SHAVEAPP_$(shaveapp-id)_TYPE_DYNAMIC)"
include $(MDK_INSTALL_DIR_ABS)/make/collect-shaveapp-dynamic.mk
endif

ifeq "y" "$(CONFIG_SHAVEAPP_$(shaveapp-id)_TYPE_STATIC)"
include $(MDK_INSTALL_DIR_ABS)/make/collect-shaveapp-static.mk
endif

else
$(call VINFO, "Ignoring shaveapp $(shaveapp-id) during collect as configured by the user")
endif
endif

$(BUILDDIR_TOP)/$(C_PROCESSOR)_MAKEFILE_LIST:
	$(ECHO)if [[ ! -f $@ ]] ; then \
		touch $@; \
		echo $@ >> $(CLEAN_LIST_FILE); \
	fi

# NOTE the $(C_PROCESSOR)_MAKEFILE_LIST variable is exported by collect.mk
.PHONY: collect-makefiles-$(C_PROCESSOR)
collect-makefiles-$(C_PROCESSOR): $(BUILDDIR_TOP)/$(C_PROCESSOR)_MAKEFILE_LIST
	$(ECHO)[[ -f $(MDK_SUBDIR)/$(SUBDIR_MAKEFILE_NAME) ]] && echo $(MDK_SUBDIR)/$(SUBDIR_MAKEFILE_NAME) >> $(BUILDDIR_TOP)/$(C_PROCESSOR)_MAKEFILE_LIST || true

# Cllect the include dirs and the compiler options introduced by this subdir
# the .collected file created into the BUILDDIR contains the actual values and
# and this meant to help build debugging
.PHONY: collect-$(C_PROCESSOR)
.ONESHELL:
collect-$(C_PROCESSOR): $(C_RECURSE_COLLECT) $(COLLECT_MISC)
	$(ECHO)[ -d $(BUILDDIR) ] || mkdir -p $(BUILDDIR)
	[ ! -f $(BUILDDIR)/$(COLLECTED_NAME) ] || truncate -s 0 $(BUILDDIR)/$(COLLECTED_NAME)
	echo $(BUILDDIR)/$(COLLECTED_NAME) >> $(CLEAN_LIST_FILE)
ifneq "" "$(strip $(include-dirs-$(C_PROCESSOR)-y))"
ifeq (y,$(CONFIG_BUILD_VERBOSE))
	$(V)echo "Collecting include-dirs-$(C_PROCESSOR)-y in $(BUILDDIR)"
endif
	echo -n "$(sort $(include-dirs-$(C_PROCESSOR)-y)) " | tr ' ' '\n' >> $(INCLUDE_DIRS_LIST)
	echo "$(sort $(include-dirs-$(C_PROCESSOR)-y))" >> $(BUILDDIR)/$(COLLECTED_NAME)
endif
ifneq "" "$(strip $(sort $(asm-include-dirs-$(C_PROCESSOR)-y)))"
	echo -n "$(sort $(asm-include-dirs-$(C_PROCESSOR)-y)) " | tr ' ' '\n' >> $(ASM_INCLUDE_DIRS_LIST)
	echo $(ASM_INCLUDE_DIRS_LIST) >> $(CLEAN_LIST_FILE)
	echo "$(sort $(asm-include-dirs-$(C_PROCESSOR)-y))" >> $(BUILDDIR)/$(COLLECTED_NAME)
endif
ifneq ("","$(strip $(ccopt-$(C_PROCESSOR)-y))")
ifeq (y,$(CONFIG_BUILD_VERBOSE))
	$(V)"Collecting ccopts-$(C_PROCESSOR)-y in $(SUBDIR)"
endif
	echo -n "$(ccopt-$(C_PROCESSOR)-y) " >> $(CCOPT_LIST) && echo "$(ccopt-$(C_PROCESSOR)-y)" >> $(BUILDDIR)/$(COLLECTED_NAME)
endif
ifneq ("","$(strip $(cppopt-$(C_PROCESSOR)-y))")
ifeq (y,$(CONFIG_BUILD_VERBOSE))
	$(V)"Collecting cppopts-$(C_PROCESSOR)-y in $(SUBDIR)"
endif
	echo -n "$(cppopt-$(C_PROCESSOR)-y) " >> $(CPPOPT_LIST) && echo "$(cppopt-$(C_PROCESSOR)-y)" >> $(BUILDDIR)/$(COLLECTED_NAME)
endif
ifdef libs-$(C_PROCESSOR)-y
ifeq (y,$(CONFIG_BUILD_VERBOSE))
	$(V)"Collecting prebuilt-libraries given in libs-$(C_PROCESSOR) in $(SUBDIR)"
endif
	echo -n "$(libs-$(C_PROCESSOR)-y) " >> $(BUILDDIR_TOP)/$(C_PROCESSOR)-prebuilt-libs
endif
ifeq (y,$(CONFIG_BUILD_GENERATE_MAKEFILES_LIST))
	echo "$(abspath $(MDK_SUBDIR)/$(SUBDIR_MAKEFILE_NAME))" >> $(MAKEFILES_LIST)
endif


