
# shared targets and variables for the main build.mk and the partial libraries build makefiles

# %/.created :
# 	$(ECHO)mkdir -p $* || true
# 	$(ECHO)touch $@
#

# this is the name of the "Makefiles" located under the subdirectories
SUBDIR_MAKEFILE_NAME=subdir.mk

DRAM_SIZE_MB = $(call unquote,$(CONFIG_DRAM_SIZE_MB))
SHAVE_ENTRY_POINTS_DEFS=$(BUILDDIR_TOP)/shave-entry-points
MAKEFILES_LIST = $(BUILDDIR_TOP)/.makefiles

# TODO adjust this following the current target SOC
SHAVES_IDX = 0 1 2 3 4 5 6 7 8 9 10 11

# Pattern: execute some MDK .mk makefile in a MDK component or user-directory,
# specifying a target for the build
# $1: MDK_SUBDIR is directory being currently built
# $2: BUILDDIR - where to place the output artefacts
# $3: SRCS variable, something like srcs-los-y or shave-los-y
# $4: INCLUDE_DIRS_LIST file containing the include dirs found during the
# collect phase
# $5: CCOPT_LIST : file containing the compile options found during the
# collect phase
# $6: CPPOPT_LIST : file containing the c++ compile options collected
# $7 the name of the makefile to be used for the local build
# $8 the recipe to be invoked in that makefile
#
define BUILD_MAKE_SUBDIR
	$(call BASH_VECHO,'mdk: Trying to make subdir $(1)') ; \
	[ -d $(2) ] || mkdir -p $(2) ; \
	if [ -d "$(1)" ] ; then \
	if [ -f "$(1)/$(SUBDIR_MAKEFILE_NAME)" ] ; then \
		$(MAKE) $(MAKE_TRACE) MDK_SUBDIR=$(1) \
			BUILDDIR=$(abspath $(2)) \
			SRCS=$(3) \
			INCLUDE_DIRS_LIST=$(4) \
			CCOPT_LIST=$(5) \
			CPPOPT_LIST=$(6) \
			-C $(2) \
			-f $(MDK_INSTALL_DIR_ABS)/make/$(7) \
			$(8) ; \
	else \
			[[ "$(CONFIG_BUILD_DEBUG)" -eq "y" ]] && $(call BASH_VECHO,"WARNING: The directory '$(1)/$(SUBDIR_MAKEFILE_NAME)' should contain a local makefile named '$(SUBDIR_MAKEFILE_NAME)'") || true ; \
	fi ; \
	else \
	  [[ "$(CONFIG_BUILD_DEBUG)" -eq "y" ]] && [[ ! -d "$(1)" ]] && $(call BASH_VECHO,"WARNING: directory not found '$(1)'") || true ; \
	fi
endef


define LIST_LIBRARIES
ifeq "" "$$($(2))"
$$(error Empty file name!)
endif
$(1):=$$(shell $(MDK_INSTALL_DIR_ABS)/make/list_libraries.sh $$($(2)) $(CONFIG_BUILD_DEBUG))
$$(call VINFO,"$(1)=$$($(1))  $(2)=$$($(2))")
endef

CLEAN_LIST_FILE=$(BUILDDIR_TOP)/clean-list
define LIST_TARGET_FILE
	@echo $@ >> $(CLEAN_LIST_FILE)
endef

