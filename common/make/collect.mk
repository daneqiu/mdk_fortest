#
# Logic to collect all the kcnf files from their different locations into one
# global file, suitable for inclusion into the main kcnf file. The files
# `all.kcnf` act as markers to let this makescript find the location of the
# files to be included
#
# NOTE this logic only works with the kconfig-frontends patched by us. See
# here:
# https://github.com/valentinrusu-movidius/kconfig-frontends
# The patches will eventually make to the mainstream. Until then, please
# install the frontends from my repo
#
# this file also collects the include directories used by the build phase

COMPONENTS_DIR = $(MDK_INSTALL_DIR)/components
COMPONENTS_KCNF = $(MDK_TMPDIR)/all-components.kcnf
COMPONENTS_MAKEFILE = $(MDK_TMPDIR)/components/$(SUBDIR_MAKEFILE_NAME)
COMPONENTS_KCNF_MARKERS = $(shell find $(COMPONENTS_DIR) -type f -name 'all.kcnf')
COMPONENTS_KCNF_PARENT_DIRS = $(dir $(COMPONENTS_KCNF_MARKERS))
$(foreach PARENT_DIR,$(COMPONENTS_KCNF_PARENT_DIRS),\
  $(eval COMPONENTS_KCNF_FILES += $(shell find $(PARENT_DIR) -maxdepth 2 -type f -name "kcnf" | sort)))

$(call VINFO,COMPONENTS_KCNF_FILES = $(COMPONENTS_KCNF_FILES))

include $(MDK_INSTALL_DIR_ABS)/make/build-lib.mk

# TODO this is not well protected agains parallel builds
$(COMPONENTS_KCNF): $(COMPONENTS_KCNF_FILES)
	$(ECHO)truncate -s 0 $@
	$(ECHO)echo "`echo $(abspath $^) | tr ' ' '\n' | sed -e 's/^/source /'`" > $@
	$(call LIST_TARGET_FILE)


$(COMPONENTS_MAKEFILE) : $(COMPONENTS_KCNF_FILES)
	$(ECHO)truncate -s 0 $@
	$(ECHO)for d in $(patsubst $(MDK_INSTALL_DIR_ABS)/%/kcnf,%,$(sort $(abspath $^))) ; \
		do \
			KCNF_FILE=$(@D)/../../$$d/kcnf ; \
			COMPONENT_CONFIG_NAMES=`cat $$KCNF_FILE | grep '^config[ \\t]*USE_COMPONENT' | sed -e 's/config \(.*\)/\\1/'` ; \
			for c in $$COMPONENT_CONFIG_NAMES ; do \
				echo "subdirs-los-\$$(CONFIG_$$c) += ../../$$d" >> $@ ; \
				echo "subdirs-lrt-\$$(CONFIG_$$c) += ../../$$d" >> $@ ; \
				echo "subdirs-shave-\$$(CONFIG_$$c) += ../../$$d" >> $@ ; \
				echo "" >> $@ ; \
			done \
		done
	$(call LIST_TARGET_FILE)

$(Kconfig): $(APPDIR)/kcnf $(MDK_INSTALL_DIR)/kcnf
	$(ECHO)truncate -s 0 $@
	$(ECHO)echo "source $(abspath $(APPDIR)/kcnf)" >> $@
	$(ECHO)echo "source $(abspath $(MDK_INSTALL_DIR)/kcnf)" >> $@
	$(ECHO)[ -f $(MDK_INSTALL_DIR)/kcnf-inhouse ] && echo "source $(MDK_INSTALL_DIR)/kcnf-inhouse" >> $@

# This target is called from kconfig.mk to collect our kcnf files
PHONY += collectkcnf
collectkcnf: collectdir $(Kconfig) $(COMPONENTS_KCNF) $(COMPONENTS_MAKEFILE) $(DRIVERS_KCNF)
	$(SILENT_INFO) "Configuration collected."

PHONY += collectdir
collectdir:
	$(ECHO)[ -d $(TMPDIR) ] || mkdir -p $(TMPDIR)
	$(ECHO)[ -d $(MDK_TMPDIR) ] || mkdir -p $(MDK_TMPDIR)
	$(ECHO)[ -d $(MDK_TMPDIR)/components ] || mkdir $(MDK_TMPDIR)/components

# Pattern: collect information from build makefiles in subdirectories
define BUILD_COLLECT_LIBS
	$(ECHO)if [ -d "$(1)" ] && [ -f "$(1)/$(SUBDIR_MAKEFILE_NAME)"  ] ; then \
	$(MAKE) $(MAKE_TRACE) MDK_SUBDIR=$(1) \
		BUILDDIR=$(2) \
		SRCS=$(3) \
		LIBRARIES_LIST=$(4) \
		-f $(MDK_INSTALL_DIR_ABS)/make/build-$(5).mk \
		collect-$(5) ; \
	else \
	  [[ "$(CONFIG_BUILD_DEBUG)" == "y" ]] && [[ ! -d "$(1)" ]] && echo "WARNING: directory not found '$(1)'" || true ; \
	  [[ "$(CONFIG_BUILD_DEBUG)" == "y" ]] && [[ ! -f "$(1)/$(SUBDIR_MAKEFILE_NAME)" ]] && echo "WARNING: subdirectory makefile not found '$(1)/$(SUBDIR_MAKEFILE_NAME)'" || true ; \
	fi
endef

# TODO in order to enhance build time, a dependency list can be generated
# along with the LIBRARY_LIST and included here as a dependency. This will
# allow trigerring libraries collection only when one of the recurse Makefile
# changed

.PHONY: collect-los-prepare
collect-los-prepare:
	$(ECHO)truncate -s 0 $(LOS_MDK_LIBRARIES_LIST)
	@echo $(LOS_MDK_LIBRARIES_LIST) >> $(CLEAN_LIST_FILE)

.PHONY: collect-lrt-prepare
collect-lrt-prepare:
	$(ECHO)truncate -s 0 $(LRT_MDK_LIBRARIES_LIST)
	@echo $(LRT_MDK_LIBRARIES_LIST) >> $(CLEAN_LIST_FILE)

.PHONY: collect-shave-prepare
collect-shave-prepare:
	$(ECHO)truncate -s 0 $(SHAVEGROUP_LIBRARIES_LIST)
	@echo $(SHAVEGROUP_LIBRARIES_LIST) >> $(CLEAN_LIST_FILE)
	@echo $(SHAVEGROUP_LIBRARIES_LIST).lock >> $(CLEAN_LIST_FILE)

define COLLECT_PROCESSOR_MDK
$(1)_DYN_APPS_DIR=$$(APPDIR)/$$(SHAVE_DYN_APPS_DIR_NAME)
PHONY += collect-$(2)

export $$($(1)_MDK_LIBRARIES_LIST)

$(1)_MAKEFILE_LIST=$$(BUILDDIR_TOP)/$(2)-makefile-list
export $$($(1)_MAKEFILE_LIST)
$$($(1)_MAKEFILE_LIST) :
	$$(SILENT_INFO) $(1): Collecting Makefile list
	$$(ECHO)
	$$(ECHO)$$(call BUILD_MAKE_SUBDIR,$$(APPDIR)/$$(call unquote,$$(CONFIG_APP_$(1)_SRCS_DIR)),$$(BUILDDIR)/app/$(2),srcs-$(2)-y,$$($(1)_INCLUDE_DIRS_LIST),$$($(1)_CCOPT_LIST),$$($(1)_CPPOPT_LIST),collect-$(2).mk,collect-makefiles-$(2))
	$$(ECHO)[[ -d $$($(1)_DYN_APPS_DIR) ]] && $$(call BUILD_MAKE_SUBDIR,$$($(1)_DYN_APPS_DIR),$$(BUILDDIR)/app/$(2)/$$(SHAVE_DYN_APPS_DIR_NAME),srcs-$(2)-y,$$($(1)_INCLUDE_DIRS_LIST),$$($(1)_CCOPT_LIST),$$($(1)_CPPOPT_LIST),collect-$(2).mk,collect-makefiles-$(2))
	$$(ECHO)$$(call BUILD_MAKE_SUBDIR,$$(MDK_INSTALL_DIR_ABS),$$(BUILDDIR)/mdk/$(2),srcs-$(2)-y,$$($(1)_INCLUDE_DIRS_LIST),$$($(1)_CCOPT_LIST),$$($(1)_CPPOPT_LIST),collect-$(2).mk,collect-makefiles-$(2))
	@echo $$(BUILDDIR)/$$@ >> $$(CLEAN_LIST_FILE)
	@echo $$($(1)_INCLUDE_DIRS_LIST) >> $$(CLEAN_LIST_FILE)
	@echo $$($(1)_CCOPT_LIST) >> $$(CLEAN_LIST_FILE)
	@echo $$($(1)_CPPOPT_LIST) >> $$(CLEAN_LIST_FILE)

collect-mdk-$(2) : $$(APPDIR)/include/generated/autoconf.h $$($(1)_MAKEFILE_LIST) collect-$(2)-prepare
	$$(SILENT_INFO) $(1): Collecting building information from MDK sources tree
	$$(ECHO)$$(call BUILD_MAKE_SUBDIR,$$(MDK_INSTALL_DIR_ABS),$$(BUILDDIR)/mdk/$(2),srcs-$(2)-y,$$($(1)_INCLUDE_DIRS_LIST),$$($(1)_CCOPT_LIST),$$($(1)_CPPOPT_LIST),collect-$(2).mk,collect-$(2))
ifeq "y" "$(CONFIG_USE_PACKAGES)"
	$$(SILENT_INFO) $(1): Collecting also the selected packages settings
	$$(ECHO)$$(call BUILD_MAKE_SUBDIR,$$(MDK_PACKAGES_DIR_ABS)/movidius,$$(BUILDDIR)/mdk/$(2),srcs-unused,$$($(1)_INCLUDE_DIRS_LIST),$$($(1)_CCOPT_LIST),$$($(1)_CPPOPT_LIST),collect-$(2).mk,collect-$(2))
endif
	$$(SILENT_INFO) $(1): Collecting MDK libraries list
	$$(ECHO)truncate -s 0 $$($(1)_MDK_LIBRARIES_LIST)
	@echo $$($(1)_MDK_LIBRARIES_LIST) >> $$(CLEAN_LIST_FILE)
	$$(call BUILD_COLLECT_LIBS,$$(MDK_INSTALL_DIR_ABS),$$(BUILDDIR)/mdk/$(2),srcs-$(2)-y,$$($(1)_MDK_LIBRARIES_LIST),$(2))
ifeq (y,$(CONFIG_BUILD_CHECK_INCLUDE_DIRS))
	$$(SILENT_INFO) $(1): Checking MDK include dirs
	$$(ECHO)for f in $$$$(cat $$($(1)_INCLUDE_DIRS_LIST)) ; do [ -d $$$$f ] || (echo "$$(tb_red)[ERR]$$(col_rst) Include dir not found: $$$$f" && exit 1) ; done
endif
endef

$(eval $(if $(filter y,$(CONFIG_HAS_SHAVE_SRCS)),$(call COLLECT_PROCESSOR_MDK,SHAVE,shave)))
$(eval $(if $(filter y,$(CONFIG_HAS_LOS_SRCS)),$(call COLLECT_PROCESSOR_MDK,LOS,los)))
$(eval $(if $(filter y,$(CONFIG_HAS_LRT_SRCS)),$(call COLLECT_PROCESSOR_MDK,LRT,lrt)))

define COLLECT_PROCESSOR_APP
$(1)_DYN_APPS_DIR=$$(APPDIR)/$$(SHAVE_DYN_APPS_DIR_NAME)
PHONY += collect-$(2)

export $$($(1)_APP_LIBRARIES_LIST)

collect-app-$(2) : $$(APPDIR)/include/generated/autoconf.h $$($(1)_MAKEFILE_LIST)
	$$(SILENT_INFO) $(1): Collecting building information from the application source tree
	$$(ECHO)$$(call BUILD_MAKE_SUBDIR,$$(APPDIR)/$$(call unquote,$$(CONFIG_APP_$(1)_SRCS_DIR)),$$(BUILDDIR)/app/$(2),srcs-$(2)-y,$$($(1)_INCLUDE_DIRS_LIST),$$($(1)_CCOPT_LIST),$$($(1)_CPPOPT_LIST),collect-$(2).mk,collect-$(2))
	$$(ECHO)[[ -d $$($(1)_DYN_APPS_DIR) ]] && $$(call BUILD_MAKE_SUBDIR,$$($(1)_DYN_APPS_DIR),$$(BUILDDIR)/app/$(2)/$$(SHAVE_DYN_APPS_DIR_NAME),srcs-$(2)-y,$$($(1)_INCLUDE_DIRS_LIST),$$($(1)_CCOPT_LIST),$$($(1)_CPPOPT_LIST),collect-$(2).mk,collect-$(2))
	$$(SILENT_INFO) $(1): Collecting application libraries list
	$$(ECHO)truncate -s 0 $$($(1)_APP_LIBRARIES_LIST)
	@echo $$($(1)_APP_LIBRARIES_LIST) >> $$(CLEAN_LIST_FILE)
	$$(call BUILD_COLLECT_LIBS,$$(APPDIR)/$$(call unquote,$$(CONFIG_APP_$(1)_SRCS_DIR)),$$(BUILDDIR)/app/$(2),srcs-$(2)-y,$$($(1)_APP_LIBRARIES_LIST),$(2))
	$$(call BUILD_COLLECT_LIBS,$$($(1)_DYN_APPS_DIR),$$(BUILDDIR)/app/$(2)/$(SHAVE_DYN_APPS_DIR_NAME),srcs-$(2)-y,$$($(1)_APP_LIBRARIES_LIST),$(2))
ifeq "y" "$(CONFIG_BUILD_CHECK_INCLUDE_DIRS)"
	$$(SILENT_INFO) $(1): Checking application include dirs
	$$(ECHO)for f in $$$$(cat $$($(1)_INCLUDE_DIRS_LIST)) ; do [ -d $$$$f ] || (echo "$$(tb_red)[ERR]$$(col_rst) Include dir not found: $$$$f" && exit 1) ; done
endif

collect-$(2): collect-app-$(2) collect-mdk-$(2)
	@echo "" > /dev/null

endef

$(eval $(if $(filter y,$(CONFIG_HAS_SHAVE_SRCS)),$(call COLLECT_PROCESSOR_APP,SHAVE,shave)))
$(eval $(if $(filter y,$(CONFIG_HAS_LOS_SRCS)),$(call COLLECT_PROCESSOR_APP,LOS,los)))
$(eval $(if $(filter y,$(CONFIG_HAS_LRT_SRCS)),$(call COLLECT_PROCESSOR_APP,LRT,lrt)))

# FIXME see if we could do this without performing the full build
getMakefileList:
	@$(MAKE) -f $(firstword $(MAKEFILE_LIST)) build CONFIG_BUILD_GENERATE_MAKEFILES_LIST=y >/dev/null
	@cat $(MAKEFILES_LIST)

.PHONY: $(PHONY)
