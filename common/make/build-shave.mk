
# SHAVE libraries build logic
#

include $(APPDIR)/.config
include $(MDK_INSTALL_DIR_ABS)/make/lib.mk
include $(MDK_INSTALL_DIR_ABS)/make/build-lib.mk
include $(MDK_INSTALL_DIR_ABS)/toolssettings.mk

#========================
# build character SHAVE

# always use lower case for defining this
B_PROCESSOR=shave
B_INCLUDE_DEFAULT=$(MDK_INSTALL_DIR_ABS)/shared/include

B_CC=$(MVCC)
B_CXX=$(MVCC)
B_SILENT_CC=$(SILENT_MVCC) $(B_CC)
B_SILENT_CXX=$(SILENT_MVCC) $(B_CXX)
# FIXME this cleanup won't be necessary after toolssettings will be adjusted
# to the new build-system
B_CCOPT=$(filter-out -S,$(MVCCOPT))
# B_CCOPT+=-fvisibility=hidden -fvisibility-inlines-hidden

ifneq (y,$(CONFIG_BUILD_USE_GENASM))
B_CCOPT_ASM+=-Wa,-no6thSlotCompression,-i:$(MDK_INSTALL_DIR_ABS)/swCommon/shave_code/myriad2/include,-i:$(MDK_INSTALL_DIR_ABS)/common/swCommon/shave_code/myriad2/asm
B_CCOPT += $(MV_DISABLE_ANALYSER)
else
$(error CONFIG_BUILD_USE_GENASM mode is not yet implemented)
endif


B_SILENT_LD=$(SILENT_LD) $(LD)
B_SILENT_AR=$(SILENT_AR) $(AR)
B_SILENT_ASM=$(SILENT_ASM) $(MVASM)
B_ASM_OPT=$(MVASMOPT)
# FIXME should this type of asm includes be specified into the makefiles, like
# the other includes?
B_ASM_INCLUDES=$(addprefix -i:,\
	$(MV_COMMON_BASE)/swCommon/shave_code/$(MV_SOC_PLATFORM)/include \
	$(MV_COMMON_BASE)/swCommon/shave_code/$(MV_SOC_PLATFORM)/asm)
#========================

# this implements the build-shave target and the resulting SHAVE objects
include $(MDK_INSTALL_DIR_ABS)/make/build-processor.mk

#
# Here comes the SHAVE specific logic, that places symbols on the selected
# SHAVE cores
#
ifeq (y,$(CONFIG_BUILD_SUBDIRS_USING_OBJECTS))
# in this case, SHAVE_LIBRARIES contains a list of files containing each a list
# of objects to be linked together
SHAVE_LIBRARIES = $(shell [[ -f $(SHAVE_LIBRARIES_LIST) ]] && $$(cat $(SHAVE_LIBRARIES_LIST) | xargs cat))
endif
ifeq (y,$(CONFIG_BUILD_SUBDIRS_USING_AR))
SHAVE_LIBRARIES = $(shell [[ -f $(SHAVE_LIBRARIES_LIST) ]] && cat $(SHAVE_LIBRARIES_LIST) )
endif
ifeq (y,$(CONFIG_BUILD_SUBDIRS_USING_LD))
SHAVE_LIBRARIES = $(shell [[ -f $(SHAVE_LIBRARIES_LIST) ]] && cat $(SHAVE_LIBRARIES_LIST) )
endif

