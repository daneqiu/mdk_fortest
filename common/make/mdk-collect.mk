
ifndef MDK_INSTALL_DIR_ABS
$(error This file was not launched by the main mdk.mk)
endif

include $(MDK_INSTALL_DIR_ABS)/make/readconfig.mk
include $(MDK_INSTALL_DIR_ABS)/make/dirs.mk
include $(MDK_INSTALL_DIR_ABS)/make/lib.mk
include $(MDK_INSTALL_DIR_ABS)/kconfig/kconfig.mk
include $(MDK_INSTALL_DIR_ABS)/make/collect.mk

collect: \
		$(if $(filter y,$(CONFIG_HAS_SHAVE_SRCS)),collect-shave) \
		$(if $(filter y,$(CONFIG_HAS_LOS_SRCS)),collect-los) \
		$(if $(filter y,$(CONFIG_HAS_LRT_SRCS)),collect-lrt)
	$(call VINFO,DONE $@)

