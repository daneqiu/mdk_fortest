
include $(MDK_INSTALL_DIR_ABS)/generalsettings.mk
include $(MDK_INSTALL_DIR_ABS)/make/dirs.mk
include $(APPDIR)/.config

CONFIG_MDK_PROJ_STRING ?="MDK_TRUE"

# This target is used by the build regression to detect which projects are MDK top-level projects
isMdkProject:
	@echo $(CONFIG_MDK_PROJ_STRING)

