

ifndef MDK_INSTALL_DIR_ABS
$(error This file was not launched by the main mdk.mk)
endif

-include $(APPDIR)/.config
include $(MDK_INSTALL_DIR_ABS)/make/dirs.mk
include $(MDK_INSTALL_DIR_ABS)/make/lib.mk
include $(MDK_INSTALL_DIR_ABS)/kconfig/kconfig.mk
include $(MDK_INSTALL_DIR_ABS)/make/collect.mk

