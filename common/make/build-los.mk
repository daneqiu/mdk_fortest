# LOS build rules
# This is the main LEON OS makefile. It produces the LOS .mvlib that'll be
# linked into the final ELF file.

include $(APPDIR)/.config
include $(MDK_INSTALL_DIR_ABS)/make/lib.mk
include $(MDK_INSTALL_DIR_ABS)/make/build-lib.mk
include $(MDK_INSTALL_DIR_ABS)/toolssettings.mk

#========================
# build character LOS

# always use lower case for defining this
B_PROCESSOR=los
B_INCLUDE_DEFAULT=$(MDK_INSTALL_DIR_ABS)/shared/include

B_CC=$(CC)
B_CXX=$(CXX)
B_SILENT_CC=$(SILENT_LOSCC) $(B_CC)
B_SILENT_CXX=$(SILENT_LOSCXX) $(B_CXX)
# FIXME CCOPT is inherited from the old build system. we should rename it in
# CCOPT_LOS
B_CCOPT=$(CCOPT) $(CONLY_OPT)
B_SILENT_LD=$(SILENT_LD) $(LD)
B_SILENT_AR=$(SILENT_AR) $(AR)
B_SILENT_URC=$(SILENT_URC) $(USBRC)
#
# FIXME the -c flag should be added with more control as user may
# occasionnally need to replace it with -S or some other similar flags
#========================

include $(MDK_INSTALL_DIR_ABS)/make/build-processor.mk

