///
/// @file
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Application configuration Leon header
///
#ifndef _XLINK_H
#define _XLINK_H
#include "XLinkPublicDefines.h"

#ifdef __cplusplus
extern "C"
{
#endif

// Initializes communication and pings the remote
XLinkError_t XLinkInitialize(XLinkHandler_t* handler);

// Opens a stream in the remote that can be written to by the local
// Allocates stream_write_size (aligned up to 64 bytes) for that stream
streamId_t XLinkOpenStream(const char* name, int stream_write_size);

// Currently useless
XLinkError_t XLinkGetAvailableStreams();

XLinkError_t XLinkGetDeviceName(int index, char* name, int nameSize);

// Send a package to initiate the writing of data to a remote stream
// Note that the actual size of the written data is ALIGN_UP(size, 64)
XLinkError_t XLinkWriteData(streamId_t streamId, const uint8_t* buffer, int size);

// Currently useless
XLinkError_t XLinkAsyncWriteData();

// Read data from local stream. Will only have something if it was written
// to by the remote
XLinkError_t XLinkReadData(streamId_t streamId, streamPacketDesc_t** packet);

// Release data from stream - This should be called after ReadData
XLinkError_t XLinkReleaseData(streamId_t streamId);

// Boot the remote (This is intended as an interface to boot the Myriad
// from PC)
XLinkError_t XLinkBootRemote(const char* deviceName, const char* binaryPath);

// Reset the remote and reset local as well
XLinkError_t XLinkResetRemote();

// Profiling funcs
XLinkError_t XLinkProfStart();
XLinkError_t XLinkProfStop();
XLinkError_t XLinkProfPrint();

#ifdef __cplusplus
}
#endif

#endif
