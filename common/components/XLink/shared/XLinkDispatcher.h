///
/// @file
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Application configuration Leon header
///
#ifndef _XLINKDISPATCHER_H
#define _XLINKDISPATCHER_H
#define _USBLINK_ENABLE_PRIVATE_INCLUDE_
#include "XLinkPrivateDefines.h"

#ifdef __cplusplus
extern "C"
{
#endif

///Adds a new event with parameters and returns event.header.id
xLinkEvent_t* dispatcherAddEvent(xLinkEventOrigin_t origin, 
									xLinkEvent_t *event);

int dispatcherWaitEventComplete();
int dispatcherUnblockEvent(eventId_t id, 
							xLinkEventType_t type,
							streamId_t stream);
void dispatcherBlockEvent(eventId_t id, 
							xLinkEventType_t type);
void dispatcherFindCorrespondingWriteRtsAndFillData(xLinkEvent_t* event);

struct dispatcherControlFunctions {
								int (*eventSend) (xLinkEvent_t*);
								int (*eventReceive) (xLinkEvent_t*);
								int (*localGetResponse) (xLinkEvent_t*,
								                     		xLinkEvent_t*);
								int (*remoteGetResponse) (xLinkEvent_t*,
								                      		xLinkEvent_t*);
								void (*closeLink) ();
								void (*resetDevice) ();
								};

int dispatcherInitialize(struct dispatcherControlFunctions* controlFunc);

#ifdef __cplusplus
}
#endif

#endif
