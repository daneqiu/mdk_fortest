///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Application configuration Leon header
///
#ifndef _XLINK_USBLINKPLATFORM_H
#define _XLINK_USBLINKPLATFORM_H
#include <stdint.h>
#ifdef __cplusplus
extern "C"
{
#endif

#define MAX_POOLS_ALLOC 8
#define PACKET_LENGTH (64*1024)

/*
The USB specific implementation is contained in the file called UsbLinkPlatform.c/cpp.
It implements the following functions:
*/
int USBLinkWrite(void* data, int size, unsigned int timeout);
int USBLinkRead(void* data, int size, unsigned int timeout);

int UsbLinkPlatformInit(const char* devPathRead,
						const char* devPathWrite,
                        int loglevel);

int UsbLinkPlatformGetDeviceName(   int index,
                                    char* name,
                                    int nameSize);

int UsbLinkPlatformBootRemote(const char* deviceName,
								const char* binaryPath);
int USBLinkPlatformResetRemote();

void* allocateData(uint32_t size, uint32_t alignment);

typedef enum usbLinkPlatformErrorCode {
    USB_LINK_PLATFORM_SUCCESS = 0,
    USB_LINK_PLATFORM_ERROR,
    USB_LINK_PLATFORM_DEVICE_NOT_FOUND,
    USB_LINK_PLATFORM_TIMEOUT
} usbLinkPlatformErrorCode_t;

#ifdef __cplusplus
}
#endif

#endif

/* end of include file */
