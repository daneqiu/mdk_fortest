#ifdef __cplusplus
extern "C" {
#endif

extern int usb_loglevel;

typedef enum usbBootError {
    USB_BOOT_SUCCESS = 0,
    USB_BOOT_ERROR,
    USB_BOOT_DEVICE_NOT_FOUND,
    USB_BOOT_TIMEOUT
} usbBootError_t;

usbBootError_t usb_find_device(unsigned idx, char *addr, unsigned addrsize, void **device, int vid, int pid);
int usb_boot(const char *addr, const void *mvcmd, unsigned size);


#ifdef __cplusplus
}
#endif
