///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Application configuration Leon header
///

#include "UsbLinkPlatform.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/timeb.h>
#include <sys/un.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <libusb.h>
#include "usb_boot.h"

#ifdef USE_LINK_JTAG
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#endif
#define USB_LINK_SOCKET_PORT 5678

#define USBLINK_ERROR_PRINT
#ifdef USBLINK_ERROR_PRINT
#define USBLINK_ERROR(...) printf(__VA_ARGS__)
#else
#define USBLINK_ERROR(...) (void)0
#endif

#ifdef USBLINKDEBUG
#define USBLINK_PRINT(...) printf(__VA_ARGS__)
#else
#define USBLINK_PRINT(...) (void)0
#endif


#ifndef USE_USB_VSC
int usbFdWrite = -1;
int usbFdRead = -1;
#endif

static int statuswaittimeout = 15;

#include <pthread.h>
#include <assert.h>

pthread_t readerThreadId;

void* poolsAllocated[MAX_POOLS_ALLOC];
uint32_t numPoolsAllocated = 0;
static void freeAllData();

#define USB_ENDPOINT_IN 0x81
#define USB_ENDPOINT_OUT 0x01

libusb_device_handle* f;

static double seconds()
{
    static double s;
    struct timespec ts;

    clock_gettime(CLOCK_MONOTONIC, &ts);
    if(!s)
        s = ts.tv_sec + ts.tv_nsec * 1e-9;
    return ts.tv_sec + ts.tv_nsec * 1e-9 - s;
}


static int usb_write(libusb_device_handle *f, const void *data, size_t size, unsigned int timeout)
{
    while(size > 0)
    {
        int bt, ss = size;
        if(ss > 1024*1024*10)
            ss = 1024*1024*10;
        int rc = libusb_bulk_transfer(f, USB_ENDPOINT_OUT, (unsigned char *)data, ss, &bt, timeout);
        if(rc)
            return rc;
        data = (char *)data + bt;
        size -= bt;
    }
    return 0;
}

static int usb_read(libusb_device_handle *f, void *data, size_t size, unsigned int timeout)
{
    while(size > 0)
    {
        int bt, ss = size;
        if(ss > 1024*1024*10)
            ss = 1024*1024*10;
        int rc = libusb_bulk_transfer(f, USB_ENDPOINT_IN,(unsigned char *)data, ss, &bt, timeout);
        if(rc)
            return rc;
        data = ((char *)data) + bt;
        size -= bt;
    }
    return 0;
}

libusb_device_handle *usblink_open(const char *path)
{
    usbBootError_t rc;
    libusb_device_handle *h = NULL;
    libusb_device *dev = NULL;
    double waittm = seconds() + statuswaittimeout;
    while(seconds() < waittm){
        rc = usb_find_device(0, (char *)path, 0, (void **)&dev, 0x040e, 0xf63b);
        if(rc == USB_BOOT_SUCCESS)
            break;
        usleep(1000);
    }
    if (rc==USB_BOOT_TIMEOUT) // Timeout
        return 0;
    int libusb_rc = libusb_open(dev, &h);
    if (libusb_rc < 0)
    {
        libusb_unref_device(dev);
        return 0;
    }
    libusb_unref_device(dev);
    libusb_rc = libusb_claim_interface(h, 0);
    if(libusb_rc < 0)
    {
        libusb_close(h);
        return 0;
    }
    return h;
}

void usblink_close(libusb_device_handle *f)
{
    libusb_release_interface(f, 0);
    libusb_close(f);
}





 int USBLinkWrite(void* data, int size, unsigned int timeout)
{
    int rc = 0;
#ifndef USE_USB_VSC
    int byteCount = 0;
#ifdef USE_LINK_JTAG
    while (byteCount < size){
        byteCount += write(usbFdWrite, &((char*)data)[byteCount], size - byteCount);
        printf("write %d %d\n", byteCount, size);
    }
#else
    if(usbFdWrite < 0)
    {
        return -1;
    }
    while(byteCount < size)
    {
       int toWrite = (PACKET_LENGTH && (size - byteCount > PACKET_LENGTH)) \
                        ? PACKET_LENGTH:size - byteCount;
       int wc = write(usbFdWrite, ((char*)data) + byteCount, toWrite);
//       printf("wwrite %x %d %x %d\n", wc, handler->commFd, ((char*)data) + byteCount, toWrite);

       if ( wc != toWrite)
       {
           return -2;
       }

       byteCount += toWrite;
       unsigned char acknowledge;
       int rc;
       rc = read(usbFdWrite, &acknowledge, sizeof(acknowledge));
       if ( rc < 0)
       {
           return -2;
       }
       if (acknowledge == 0xEF)
       {
//         printf("read %x\n", acknowledge);
       }
       else
       {
//         printf("read err %x %d\n", acknowledge, rc);
           return -2;
       }
    }
#endif  /*USE_LINK_JTAG*/
#else
    rc = usb_write(f, data, size, timeout);
#endif  /*USE_USB_VSC*/
    return rc;
}

 int USBLinkRead(void* data, int size, unsigned int timeout)
{
    int rc = 0;
#ifndef USE_USB_VSC
    int nread =  0;
#ifdef USE_LINK_JTAG
    while (nread < size){
        nread += read(usbFdWrite, &((char*)data)[nread], size - nread);
        printf("read %d %d\n", nread, size);
    }
#else
    if(usbFdRead < 0)
    {
        return -1;
    }

    while(nread < size)
    {

        int toRead = (PACKET_LENGTH && (size - nread > PACKET_LENGTH)) \
                        ? PACKET_LENGTH : size - nread;


        while(toRead > 0)
        {
            rc = read(usbFdRead, &((char*)data)[nread], toRead);
//          printf("read %x %d\n", *(int*)data, nread);
            if ( rc < 0)
            {
                return -2;
            }
            toRead -=rc;
            nread += rc;
        }
        unsigned char acknowledge = 0xEF;
        int wc = write(usbFdRead, &acknowledge, sizeof(acknowledge));
        if (wc == sizeof(acknowledge))
        {
//          printf("write %x %d\n", acknowledge, wc);
        }
        else
        {
            return -2;
        }
    }
#endif  /*USE_LINK_JTAG*/
#else
    rc = usb_read(f, data, size, timeout);
#endif  /*USE_USB_VSC*/
    return rc;
}

int UsbLinkPlatformGetDeviceName(int index, char* name, int nameSize){
    usbBootError_t rc = usb_find_device(index, name, nameSize, 0, 0, 0);
switch(rc) {
        case USB_BOOT_SUCCESS:
            return USB_LINK_PLATFORM_SUCCESS;
        case USB_BOOT_DEVICE_NOT_FOUND:
            return USB_LINK_PLATFORM_DEVICE_NOT_FOUND;
        case USB_BOOT_TIMEOUT:
            return USB_LINK_PLATFORM_TIMEOUT;
        default:
            return USB_LINK_PLATFORM_ERROR;
    }
}

int UsbLinkPlatformBootRemote(const char* deviceName, const char* binaryPath)
{
    unsigned filesize;
    FILE *fp;
    char *tx_buf;
    char subaddr[28+2];
    int rc;
#ifndef USE_USB_VSC

    if (usbFdRead != -1){
        close(usbFdRead);
        usbFdRead = -1;
    }
    if (usbFdWrite != -1){
        close(usbFdWrite);
        usbFdWrite = -1;
    }
#endif
    // Load the executable
    fp = fopen(binaryPath, "rb");
    if(fp == NULL)
    {
        if(usb_loglevel)
            perror(binaryPath);
        return -7;
    }
    fseek(fp, 0, SEEK_END);
    filesize = ftell(fp);
    rewind(fp);
    if(!(tx_buf = (char*)malloc(filesize)))
    {
        if(usb_loglevel)
            perror("buffer");
        fclose(fp);
        return -3;
    }
    if(fread(tx_buf, 1, filesize, fp) != filesize)
    {
        if(usb_loglevel)
            perror(binaryPath);
        fclose(fp);
        free(tx_buf);
        return -7;
    }
    fclose(fp);

    // This will be the string to search for in /sys/dev/char links
    int chars_to_write = snprintf(subaddr, 28, "-%s:", deviceName);
    if(chars_to_write >= 28) {
        printf("Path to your boot util is too long for the char array here!\n");
    }
    // Boot it
    rc = usb_boot(deviceName, tx_buf, filesize);
    free(tx_buf);
    if(rc)
    {
        return rc;
    }
    if(usb_loglevel > 1)
        fprintf(stderr, "Boot successful, device address %s\n", deviceName);
    return 0;
}

int USBLinkPlatformResetRemote()
{
    freeAllData();
#ifndef USE_USB_VSC
#ifdef USE_LINK_JTAG
    /*Nothing*/
#else
    if (usbFdRead != -1){
        close(usbFdRead);
        usbFdRead = -1;
    }
    if (usbFdWrite != -1){
        close(usbFdWrite);
        usbFdWrite = -1;
    }
#endif  /*USE_LINK_JTAG*/
#else
    usblink_close(f);
#endif  /*USE_USB_VSC*/
    return -1;
}

int UsbLinkPlatformInit(const char* devPathRead, const char* devPathWrite, int loglevel)
{
    usb_loglevel = loglevel;
#ifndef USE_USB_VSC
#ifdef USE_LINK_JTAG
    struct sockaddr_in serv_addr;
    usbFdWrite = socket(AF_INET, SOCK_STREAM, 0);
    usbFdRead = socket(AF_INET, SOCK_STREAM, 0);
    assert(usbFdWrite >=0);
    assert(usbFdRead >=0);
    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    serv_addr.sin_port = htons(USB_LINK_SOCKET_PORT);

    if (connect(usbFdWrite, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
    {
        perror("ERROR connecting");
        exit(1);
    }
    printf("this is working\n");
    return 0;

#else
    usbFdRead= open(devPathRead, O_RDWR);
    if(usbFdRead < 0)
    {
        return -1;
    }
    // set tty to raw mode
    struct termios  tty;
    speed_t     spd;
    int rc;
    rc = tcgetattr(usbFdRead, &tty);
    if (rc < 0) {
        usbFdRead = -1;
        return -2;
    }

    spd = B115200;
    cfsetospeed(&tty, (speed_t)spd);
    cfsetispeed(&tty, (speed_t)spd);

    cfmakeraw(&tty);

    rc = tcsetattr(usbFdRead, TCSANOW, &tty);
    if (rc < 0) {
        usbFdRead = -1;
        return -2;
    }

    usbFdWrite= open(devPathWrite, O_RDWR);
    if(usbFdWrite < 0)
    {
        usbFdWrite = -1;
        return -2;
    }
    // set tty to raw mode
    rc = tcgetattr(usbFdWrite, &tty);
    if (rc < 0) {
        usbFdWrite = -1;
        return -2;
    }

    spd = B115200;
    cfsetospeed(&tty, (speed_t)spd);
    cfsetispeed(&tty, (speed_t)spd);

    cfmakeraw(&tty);

    rc = tcsetattr(usbFdWrite, TCSANOW, &tty);
    if (rc < 0) {
        usbFdWrite = -1;
        return -2;
    }
    return 0;
#endif  /*USE_LINK_JTAG*/
#else
    f = usblink_open(devPathWrite);
    if (f == 0)
    {
       printf("fail\n");
       exit(1);
    }
//    dev = new fastmemDevice("usb");
//    int usbInit = dev->fastmemInit();
    if(f)
        return 0;
    else
        return -1;
#endif  /*USE_USB_VSC*/
}

void* allocateData(uint32_t size, uint32_t alignment)
{
   void* ret = NULL;

   if(numPoolsAllocated < MAX_POOLS_ALLOC) {
      if ((alignment > 0) && (size > 0) && ((alignment & (alignment - 1)) == 0)) {  // Check that the alignment is a Power of 2
         alignment--; // You will need maximum alignment of align-1, aka 1 byte.
         ret = malloc(size + alignment); // You will need maximum alignment of align-1, aka 1 byte.
         poolsAllocated[numPoolsAllocated++] = ret;
         ret = (void*)(((uint64_t)ret + (uint64_t)alignment) & ~(uint64_t)alignment); // add alignment and round down the result
      }
   }

   return ret;
}

/********************AUXILIARY FUNCTIONS*********************/
void freeAllData(){
    for(uint32_t i = 0; i<numPoolsAllocated; i++) {
        if(poolsAllocated[i] != 0){
            free(poolsAllocated[i]);
        }
    }
}
