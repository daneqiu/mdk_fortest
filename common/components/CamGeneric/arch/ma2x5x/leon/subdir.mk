include-dirs-los-ma2x5x-y+=\
  $(MDK_INSTALL_DIR_ABS)/components/CamGeneric/arch/ma2x5x/leon/common/include 
include-dirs-los-$(CONFIG_TARGET_SOC_MA2150) += include-dirs-los-ma2x5x-y
include-dirs-los-$(CONFIG_TARGET_SOC_MA2450) += include-dirs-los-ma2x5x-y

srcs-los-ma2x5x-y+=\
 common/src/CamGeneric_ma2x5x.c 
srcs-los-$(CONFIG_TARGET_SOC_MA2150) += srcs-los-ma2x5x-y
srcs-los-$(CONFIG_TARGET_SOC_MA2450) += srcs-los-ma2x5x-y
