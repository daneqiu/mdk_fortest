///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     RTEMS configuration
///

#include <rtems.h>
#include <fatalExtension.h>
#include <OsDrvInit.h>
#include "TestSysConfig.h"

// ask the system to generate a configuration table
#define CONFIGURE_INIT

#ifndef RTEMS_POSIX_API
#define RTEMS_POSIX_API
#endif

#define CONFIGURE_MICROSECONDS_PER_TICK             1000    // 1 millisecond
#define CONFIGURE_TICKS_PER_TIMESLICE               10      // 10 milliseconds
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_POSIX_INIT_THREAD_TABLE
#define CONFIGURE_MINIMUM_TASK_STACK_SIZE           4096
#define CONFIGURE_MAXIMUM_TASKS                     24
#define CONFIGURE_MAXIMUM_POSIX_THREADS             20
#define CONFIGURE_MAXIMUM_POSIX_MUTEXES             7
#define CONFIGURE_MAXIMUM_POSIX_KEYS                7
#define CONFIGURE_MAXIMUM_POSIX_SEMAPHORES          32
#define CONFIGURE_MAXIMUM_POSIX_MESSAGE_QUEUES      20
#define CONFIGURE_MAXIMUM_MESSAGE_QUEUES            20
#define CONFIGURE_MAXIMUM_POSIX_TIMERS              7
#define CONFIGURE_MAXIMUM_TIMERS                    7
#define CONFIGURE_MAXIMUM_SEMAPHORES                32
#define CONFIGURE_MAXIMUM_DRIVERS                   20
#define CONFIGURE_MAXIMUM_DEVICES                   20
#define CONFIGURE_MAXIMUM_REGIONS                    4


#define CONFIGURE_MAXIMUM_USER_EXTENSIONS           1
#define CONFIGURE_INITIAL_EXTENSIONS                { .fatal = Fatal_extension }

#define CONFIGURE_APPLICATION_EXTRA_DRIVERS         OS_DRV_INIT_TABLE_ENTRY


void *POSIX_Init (void *args);


#include <rtems/confdefs.h>


// program the booting clocks
BSP_SET_CLOCK(DEFAULT_OSC_CLOCK_KHZ, DEFAULT_APP_CLOCK_KHZ,   1, 1,
              APP_CSS_CLOCKS, APP_MSS_CLOCKS, APP_UPA_CLOCKS, 0, 0);

// program L2 cache behaviour
BSP_SET_L2C_CONFIG(0,
              DEFAULT_RTEMS_L2C_REPLACEMENT_POLICY,
              DEFAULT_RTEMS_L2C_LOCKED_WAYS,
              DEFAULT_RTEMS_L2C_MODE, 0, 0);
