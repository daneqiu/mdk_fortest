///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Application configuration Leon header
///

#ifndef _TEST_SYS_CONFIG_H_
#define _TEST_SYS_CONFIG_H_
#define CMX_CONFIG_SLICE_7_0        (0x11111111)
#define CMX_CONFIG_SLICE_15_8       (0x11111111)

#define DEFAULT_APP_CLOCK_KHZ       600000
#define DEFAULT_OSC_CLOCK_KHZ       24000


#define APP_CSS_CLOCKS              DEFAULT_RTEMS_CSS_LOS_CLOCKS
#define APP_MSS_CLOCKS              DEFAULT_RTEMS_MSS_LRT_CLOCKS
#define APP_UPA_CLOCKS              DEV_UPA_MTX


/// Setup all the clock configurations needed by this application and also the ddr
///
/// @return    0 on success, non-zero otherwise
int TestInitClocksAndMemory(void);

#endif


