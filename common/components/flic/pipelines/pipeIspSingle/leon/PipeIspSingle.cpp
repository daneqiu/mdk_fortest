#include <Opipe.h>

#include <Flic.h>
#include <Allocator.h>

#include <PlgPool.h>
#include "PlgSource.h"
#include "PlgIspCtrl.h"
#include "PlgIspProc.h"
#include "PlgOutItf.h"
#include "PipeIspSingle.h"

#define N_POOL_FRMS 2

PlgSource    plgSrc         SECTION(".cmx_direct.data");     //Source
PlgIspCtrl  *plgIspCtrl     SECTION(".cmx_direct.data") ; //cameraConfig interface to flic message
PlgIspProc   plgIsp         SECTION(".cmx_direct.data");     //ISP
PlgPool      plgPoolSrc     SECTION(".cmx_direct.data"); //source out pool
PlgPool      plgPoolIsp     SECTION(".cmx_direct.data"); //isp    out pool
PlgOutItf    plgOut         SECTION(".cmx_direct.data");     //out
Pipeline     p              SECTION(".cmx_direct.data");

ImgFrameFactory ff;


void pipeIspSingleCreate(GetSrcSzLimits getSrcSzLimits) {
    icSourceSetup srcSet;

    OpipeReset();

    //Create plugins
    RgnAlloc.Create(RgnBuff, DEF_POOL_SZ);
    plgIspCtrl = PlgIspCtrl::instance();
    plgIspCtrl->Create();
    plgSrc    .Create(IC_SOURCE_0);
    plgSrc    .outFmt = SIPP_FMT_16BIT;
    plgIsp    .Create(IC_SOURCE_0);
    if(0 == getSrcSzLimits(IC_SOURCE_0, &srcSet)) {
        icSourceSetup* srcLimits = &srcSet;
        srcLimits->maxBpp = 16;
        plgPoolSrc.Create(&RgnAlloc, &ff, N_POOL_FRMS, ((srcLimits->maxPixels * srcLimits->maxBpp)) >> 3); //RAW
        plgPoolIsp.Create(&RgnAlloc, &ff, N_POOL_FRMS, (srcLimits->maxPixels * 3)>>1); //YUV420
    }
    else {
        // not define max size for initialized camera
        assert(0);
    }
    plgOut    .Create();

    p.Add(plgIspCtrl);
    p.Add(&plgOut);
    p.Add(&plgSrc);
    p.Add(&plgIsp);
    p.Add(&plgPoolSrc);
    p.Add(&plgPoolIsp);

    plgPoolSrc .out                       .Link(&plgSrc.inO);
    plgSrc     .outCommand                .Link(&plgIspCtrl->inCtrlResponse);
    plgIspCtrl->outSrcCommand[IC_SOURCE_0].Link(&plgSrc.inCommand);
    plgPoolIsp .out                       .Link(&plgIsp.inO);
    plgSrc     .out                       .Link(&plgIsp.inI);
    plgIsp     .outF                      .Link(&plgOut.in );
    plgIsp     .outE                      .Link(&plgIspCtrl->inCtrlResponse);
    plgOut     .outCmd                    .Link(&plgIspCtrl->inCtrlResponse);
    plgIspCtrl->outOutCmd                 .Link(&plgOut.inCmd);
    p.Start();
}

void pipeIspSingleDestroy(void) {
    p.Stop();
    p.Wait();
    p.Delete();
    RgnAlloc.Delete();
}
