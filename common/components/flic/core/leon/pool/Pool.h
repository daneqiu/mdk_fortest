///
/// @file
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Pool & shared frame pointer (PoPtr)
///

#ifndef __POOL_H__
#define __POOL_H__

#include <stdio.h>
#include <Flic.h>
#include <Sender.h>
#include <MsgBuf.h>
#include <FlicIpc.h>

//TBD: move implementation in different file, nobody want to see it...

#if 0 //Pool debug
  #define DPRINTF(...) printf(__VA_ARGS__)
#else
  #define DPRINTF(...)
#endif

//Remote methods defs
#define RMT_MET_INC_REF 1
#define RMT_MET_DEC_REF 2

//####################################################################
class RefCnt
{
  friend class RefBase;
//private:
  public:
    int  cnt; // ref count

  public:
    void IncRef()
    {
       Atomic a;
       a.Enter();
         cnt++;
       a.Leave();
     //printf("_inc A=0x%lx V=%d\n", (uint32_t)&cnt, cnt);
    }

    int DecRef()
    {
        Atomic a;
        a.Enter();
         if(cnt>0)
            cnt--;
        a.Leave();
      //printf("_dec A=0x%lx V=%d\n", (uint32_t)&cnt, cnt);
        return cnt;
    }
};
//####################################################################
class RefBase
{
  public:
    RefCnt    *ref; // Reference cnt
    RefBase() {ref = NULL;}
    const char *name; //debug
};

template <typename T> class Pool;

//####################################################################
//Pool-Object-Ptr (a sort of smart pointer)
template <typename T> class PoPtr : public RefBase
{
public:
    T       *ptr;
    Pool<T> *pool; //need to know who I belong to

public:
    PoPtr(                  ) {pool = NULL; ptr = NULL; name = NULL  ;}
    PoPtr(const char *szName) {pool = NULL; ptr = NULL; name = szName;}

  //Cool stuff:
    T& operator*  () {return *ptr;}
    T* operator-> () {return  ptr;}

    void  RmtIncRef(); //only for Shared pools
    void  RmtDecRef();

private:
    //=================================================================
    void CpuCheck()
    {
      if ((pool->cpu != PROCESS_LEON_OS) &&
          (pool->cpu != PROCESS_LEON_RT))
      {
          printf("ERROR: invalid CPU : %d (pool @ 0x%lx)\n", pool->cpu, (uint32_t)pool);
          assert(0);
      }
    }
    //=================================================================
    void UnRefOld()
    {
     //NOTE: in case of super-frame other.ptr/ref/pool could be NULL
      if(ref && pool){
        CpuCheck();
        if(pool->cpu == ThisCpu())
        {
          if(ref->DecRef() == 0){
          //TBD: this is not robust, if freeFrm gets destroyed, this will crash!
           pool->freeFrm.Push(NULL, this);
          }
        }
        else{
          RmtDecRef();//RMI
        }
      }
    }
    //=================================================================
    void RefNew()
    {
      if(ref && pool){
       CpuCheck();
       if(pool->cpu == ThisCpu())
          ref->IncRef();
       else{
          RmtIncRef();//RMI
       }
      }
    }

public:
    //=================================================================
    PoPtr<T>(const PoPtr<T>& other)
    {
        DPRINTF("COPY_CONSTRUCTOR\n");
        ptr  = other.ptr;
        ref  = other.ref;
        pool = other.pool;
        RefNew();
    }

    //=================================================================
    PoPtr<T>& operator = (const PoPtr<T>& other) // Assignment operator
    {
        if(this != &other) // Avoid self assignment
        {
          //Ownership changes here:
          //1) release what we referenced so far
            UnRefOld();

          //2) and from now on, we'll refer what 'other' refers
            ptr  = other.ptr;
            ref  = other.ref;
            pool = other.pool;
            RefNew();
        }
        return *this;
    }
    //=================================================================
    void Reset() {
        UnRefOld();
    }
    //=================================================================
    ~PoPtr<T>()
    {
        Reset();
    }
};

//####################################################################
//Pool Sender : P should be a PoPtr<F>, where F is the user frame descriptor
template <typename P> class PoolSender : public SSender<P>
{
   private:
    MsgBuf<P> *freeFrm;

   public:
    void Init   (MsgBuf<P> *freeF)                 {       freeFrm = freeF;  }
    int  Pull   (Io *who, P *msg, IBuffCmd<P> *c)  {return freeFrm->Pop   (who, msg, c);}
    int  TryPull(Io *who, P *msg, IBuffCmd<P> *c)  {return freeFrm->TryPop(who, msg, c);}

    void Destroy() { }
    void Delete () { }
};

//####################################################################
//not cool, but want to save additional dummy members
template <typename T> class Pool : public IPlugin
{
  private:
    IAllocator *alloc;    //PoolBase
    uint32_t    nFrames;  //PoolBase
    uint32_t    frmSize;  //PoolBase
    T          *bufDesA;  //allocation
    T          *bufDes;   //use
    RefCnt     *refCnt;   //PoolBase

  public:
    swcProcessorType cpu;
    bool          shared;
    pthread_t     thread; //created only if [shared]
    sem_t          rmtOp; //created only if [shared]

    uint32_t  *bases;//backup, TBD: remove?
    MsgBuf    <PoPtr<T>> freeFrm;
    PoolSender<PoPtr<T>> out;

  public:
    void Create(IAllocator *a, uint32_t nFrm, uint32_t fSize, bool sh = false)
    {
    //printf("Pool Create\n");
      shared  = sh;
      cpu     = swcWhoAmI();
      alloc   = a;
      nFrames = nFrm;
      bufDesA = new T       [nFrm];
      refCnt  = new RefCnt  [nFrm];
      bases   = new uint32_t[nFrm];
      frmSize = fSize;
      freeFrm.Create(nFrm/*, poolBeh*/);

     //For shared pools, 'bufDes' needs to be visible on remote side.
     //Using separate BufDesA (A=Allocation so that delete matches new)
      if(shared) bufDes = (T*)NonCachedAddr(bufDesA);
      else       bufDes = bufDesA;

      for(uint32_t i=0; i<nFrm; i++)
      {
        PoPtr<T> pImg;        //block scope
        refCnt[i].cnt = 1;    //initial owner: the Pool
        pImg.ptr   = &bufDes[i];
        pImg.ref   = &refCnt[i];
        pImg->base = a->Alloc(frmSize);
        pImg->size = frmSize;
        bases[i]   = (uint32_t)pImg->base;
        pImg.pool  = this;
        freeFrm.Push(NULL, &pImg);
        assert(pImg->base); //check alloc
      //"pImg" gets destructed here !!!
      }

      out.Init(&freeFrm);
      Add(&out, "out");
    }

    void Delete()
    {
       for(uint32_t i=0; i<nFrames; i++){
          alloc->Free((void*)bases[i]);
       }
       freeFrm.Delete();

       delete[] bufDesA;
       delete[] refCnt;
       delete[] bases;
       nIO = 0;
    }

    static void * rmtEventsFunc(void *This);

    void Start(){
      if(shared)
      {
         pthread_attr_t attr;
         assert(OK == pthread_attr_init           (&attr));
         assert(OK == pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED));
         assert(OK == pthread_attr_setschedpolicy (&attr, SCHED_RR));
         assert(OK == pthread_create(&thread, &attr, Pool<T>::rmtEventsFunc, this));
         assert(OK == pthread_attr_destroy   (&attr));

         assert(OK == sem_init(&rmtOp, 0, 0));
      }
    }

    void Stop(){
        if(shared){
           assert(0 == sem_destroy(&rmtOp));
        }
        freeFrm.Destroy();
    }

    void ShowRefCounts() //debug
    {
      printf("############# Ref Counts #############\n");
      for(uint32_t i=0; i<nFrames; i++)
        printf("refCnt[%ld] = %x\n", i, refCnt[i].cnt);
      printf("\n");
    }

    int FreeCnt(){ //debug
        return freeFrm.Count();
    }
};

//====================================================================
//the "typedef" for templates:
template <typename T> using PlgPool = Pool<T>;


//########################################################################
//  _____  ______ __  __  ____ _______ ______       _          __  __
// |  __ \|  ____|  \/  |/ __ \__   __|  ____|     | |        / _|/ _|
// | |__) | |__  | \  / | |  | | | |  | |__     ___| |_ _   _| |_| |_
// |  _  /|  __| | |\/| | |  | | | |  |  __|   / __| __| | | |  _|  _|
// | | \ \| |____| |  | | |__| | | |  | |____  \__ \ |_| |_| | | | |
// |_|  \_\______|_|  |_|\____/  |_|  |______| |___/\__|\__,_|_| |_|
//
//########################################################################

//Handle remote PoPtr IncRef/DecRef, this thread only gets started
//for SHARED pools
template <typename T> void * Pool<T>::rmtEventsFunc(void *This)
{
  #if defined(FLIC_2LEONS)
   Pool<T> *me = (Pool<T>*)This;
   while(me->Alive())
   {
      sem_wait(&me->rmtOp);
      switch(inpCall->methodId)
      {
       case RMT_MET_INC_REF:
       {
            RefCnt *ref = (RefCnt*)inpCall->params[0];
            ref->IncRef();
            FlicRmt::RmiAck();
            break;
       }
       case RMT_MET_DEC_REF:
       {
            T       *ptr = (T      *)inpCall->params[0];
            RefCnt  *ref = (RefCnt *)inpCall->params[1];
            Pool<T> *pool= (Pool<T>*)inpCall->params[2];
            assert(pool == me); //paranoia check

            //TBD: name

            if(ref->DecRef() == 0)
            {
              //Local obj to be pushed
               PoPtr<T> p;
               p.pool = me;
               p.ref  = ref;
               p.ptr  = ptr;
               me->freeFrm.Push(NULL, &p);

              //Cancel local obj destruction, as point here is to do a Push,
              //not a Push followed by a Pop (thus nothing)
               p.pool = NULL;
               p.ref  = NULL;
            }
            FlicRmt::RmiAck();
            break;
       }
       default:
       {
            assert(0);
            break;
       }
      }//switch
   }//while

  #else
   UNUSED(This);
  #endif

   return NULL;
}

//####################################################################
template <typename T> void PoPtr<T>::RmtIncRef()
{
 #if defined(FLIC_2LEONS)
 //Note: "pool" is remotely defined/instantiated
 //assert(this->pool->shared == true);
   RmiCtx ctx;
   ctx.semToWake = &pool->rmtOp;
   ctx.methodId  = RMT_MET_INC_REF;
   ctx.params[0] = (uint32_t)this->ref;
   FlicRmt::RmiCall(&ctx);
 #endif
}

template <typename T> void PoPtr<T>::RmtDecRef()
{
  #if defined(FLIC_2LEONS)
 //Note: "pool" is remotely defined/instantiated
 //assert(this->pool->shared == true);
   RmiCtx ctx;
   ctx.semToWake = &pool->rmtOp;
   ctx.methodId  = RMT_MET_DEC_REF;
   ctx.params[0] = (uint32_t)ptr;
   ctx.params[1] = (uint32_t)ref;
   ctx.params[2] = (uint32_t)pool;
   FlicRmt::RmiCall(&ctx);
  #endif
}

#endif
