///
/// @file
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     MsgBuffer : Overwrite Oldest PUSH policy
///

#ifndef __BEH_QUEUE_OVR_H__
#define __BEH_QUEUE_OVR_H__

#include "rtems.h"
#include "BehQueue.h"

//##############################################################
template <typename T> class QueueCmdOvr : public QueueCmd<T>
{
  public:
    int doPush(MsgBuf<T>*q, T *m, TimeSpec *ts);

    static IBuffCmd<T> * Inst()
    {
        static QueueCmdOvr<T> inst;
        return &inst;
    }
};


//##############################################################
//TBD: factor out stuff in BufferBase
template<typename T> int QueueCmdOvr<T>::doPush(MsgBuf<T> *b, T *m, TimeSpec *ts)
{
    UNUSED(ts);

    Atomic a;
    a.Enter();
    int idx = 0;

    if((uint32_t)b->semUsedV < b->nElem){
        idx = b->GetFreeSlot();
        sem_wait(&b->semFree); //must dec free elems
    }else{
        idx = b->GetNext();
        b->mssg[idx]->~T(); //release old
        sem_wait(&b->semUsed);
        assert(b->semUsedV>0); b->semUsedV--;
    }

    assert(idx < (int)b->nElem);
     //*b->mssg[idx] = *m;  //invoke operator= !!!
    new(b->mssg[idx]) T(*m);//invoke copy-constructor
    b->MarkPush(idx);

    a.Leave();
    return OK;
}

#endif