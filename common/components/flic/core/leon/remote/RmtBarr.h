///
/// @file
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Inter Leon sync barrier
///

#if defined(FLIC_2LEONS)

#ifndef __RMT_BARRIER_H__
#define __RMT_BARRIER_H__

#include <FlicRmt.h>

class RmtBarr
{
  private:
    sem_t syncSem;
    RmtBarr *rmt;

  public:
   void Create(RmtBarr *remote)
   {
       rmt = remote;
       assert(OK == sem_init(&syncSem, 0, 0));
   }
   void Destroy()
   {
       sem_destroy(&syncSem);
   }
   void Wait()
   {
      #if defined(LEON_RT)
       RmiCtx ctx;
       ctx.semToWake = &rmt->syncSem;
       FlicRmt::RmiCall(&ctx); //waits for ACK
      #else
       sem_wait(&syncSem);
       FlicRmt::RmiAck(); //sends ACK
      #endif
   }
};

#endif
#endif