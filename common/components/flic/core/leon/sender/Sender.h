///
/// @file
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Master/Slave Sender
///

#ifndef __SENDER_H__
#define __SENDER_H__

#include <Flic.h>

template<typename T> class ISReceiver;
template<typename T> class MReceiver;

//###############################################################
// Master-Sender
//###############################################################
template<typename T> class MSender : public Io
{
   public:
        ISReceiver<T> **recs;
        uint32_t       nRecs; //actual number of recs
        uint32_t       mRecs; //max number of recs

        MSender(uint32_t maxRecs = MAX_RECS_PER_SENDER)
        {
           nRecs = 0;
           recs  = new ISReceiver<T>* [maxRecs];
           mRecs = maxRecs;
        }
        virtual ~MSender(){
            delete[] recs;
        }
        void Delete(){nRecs = 0;}

        void Link   (ISReceiver<T> *r);
        int  Send   (T  *msg);
        int  TrySend(T  *msg);
};

template <typename T> void MSender<T>::Link(ISReceiver<T> *r)
{
    CheckParent(this);
    CheckParent(r);
    assert(nRecs < mRecs);
    recs[nRecs] = r;
    nRecs++;

    if(FLIC_PLOG && par) //LOGGING
     if((par->pl) && (r->par))
      if(par->pl->log)
       par->pl->log->Print("Pipe [%.8s] MS_LINK [%.8s%.8s]->[%.8s%.8s]\n",
        (char*)&par->pl->name, (char*)&par->name, (char*)&name,
        (char*)&r->par->name, (char*)&r->name);
}

template <typename T> int MSender<T>::Send(T *msg)
{
    int rc = OK;
    for(uint32_t i=0; i<nRecs; i++){
        rc |= recs[i]->Post(this, msg);
    }
    return rc;
}
template <typename T> int MSender<T>::TrySend(T *msg)
{
    int rc = OK;
    for(uint32_t i=0; i<nRecs; i++){
        rc |= recs[i]->TryPost(this, msg);
    }
    return rc;
}

//###############################################################
//Slave-Sender (responds to Master-Receiver requests)
//###############################################################
template <typename T> class SSender : public Io
{
    friend class MReceiver<T>;

    public: void Link(MReceiver<T> *rec)
           {
              CheckParent(this);
              CheckParent(rec);
              rec->snd = this;

              if(FLIC_PLOG && par) //LOGGING
               if((par->pl) && (rec->par))
                if(par->pl->log)
                 par->pl->log->Print("Pipe [%.8s] SS_LINK [%.8s%.8s]->[%.8s%.8s]\n",
                  (char*)&par->pl->name,  (char*)&par->name,  (char*)&name,
                  (char*)&rec->par->name, (char*)&rec->name);
           }

    protected: //only Receiver can invoke "Pull"
     virtual int Pull   (Io *who, T *msg, IBuffCmd<T> *c) = 0;
     virtual int TryPull(Io *who, T *msg, IBuffCmd<T> *c) = 0;
     virtual ~SSender(){};
};
#endif
