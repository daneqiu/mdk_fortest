///
/// @file
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Master/Slave Receiver
///

#ifndef __RECEIVER_H__
#define __RECEIVER_H__

#include <Flic.h>
#include <MsgBuf.h>
#include <BehQueue.h>

template<typename T> class SSender;

//Note: one plugin migh send a notification to another plugin from an IRQ
//      (e.g. Isp::LineReached), so "Notify" must be runnable from IRQ as well
class SemSync : public ISync{
  private: sem_t sem;
  public :
          void Create() {assert(OK==sem_init(&sem,0,0));}
           int Notify() {return sem_post(&sem);}
           int Wait  () {return sem_wait(&sem);}
          void Destroy(){assert(OK== sem_destroy(&sem));}
};

//####################################################################
// Slave-Receiver interface
//####################################################################
template<typename T> class ISReceiver : public Io
{
  public:

    virtual void Create(uint32_t nMsg, IBuffCmd<T> *beh, ISync *s) = 0;
            void Create(uint32_t nMsg, IBuffCmd<T> *beh) { Create(nMsg, beh , NULL); }
            void Create(uint32_t nMsg, ISync *s)         { Create(nMsg, NULL,    s); }
            void Create(uint32_t nMsg          )         { Create(nMsg, NULL, NULL); }

    virtual void Delete () = 0;
    virtual void Destroy() = 0;

  //Receiver view:
    virtual int  Receive   (T *msg, IBuffCmd<T> *c = NULL) = 0;
    virtual int  TryReceive(T *msg, IBuffCmd<T> *c = NULL) = 0;

  //Sender view (i.e. called by sender)
    virtual int  Post      (Io *who, T *msg, IBuffCmd<T> *c = NULL) = 0;
    virtual int  TryPost   (Io *who, T *msg, IBuffCmd<T> *c = NULL) = 0;
};

//####################################################################
// Slave-Receiver default implementation
//####################################################################
template<typename T> class SReceiver : public ISReceiver<T>
{
   public:
         MsgBuf<T>   mq;  //Slave must buffer incoming messages

         ISync   *sync;   //for grouped inputs

         using ISReceiver<T>::Create; //Thanks Razvan :)
         void Create(uint32_t nMsg, IBuffCmd<T> *beh, ISync *s)
         {
            mq.Create(nMsg, beh);
            sync = s;
         }
         void Delete () { mq.Delete (); };
         void Destroy() { mq.Destroy(); };

  //Receiver view:
    virtual int  Receive   (T *msg, IBuffCmd<T> *c = NULL) { return mq.Pop   (this, msg, c);}
    virtual int  TryReceive(T *msg, IBuffCmd<T> *c = NULL) { return mq.TryPop(this, msg, c);}

  //Sender view (called by sender)
    virtual int  Post    (Io *who, T *msg, IBuffCmd<T> *c = NULL)
    {
        int rc = mq.Push(who, msg, c);
        if((rc==0) && (sync))
            sync->Notify();
        return rc;
    }
    virtual int  TryPost (Io *who, T *msg, IBuffCmd<T> *c = NULL)
    {
        int rc = mq.TryPush(who, msg, c);
        if((rc==OK) && (sync))
            sync->Notify();
        return rc;
    }
    virtual ~SReceiver<T>(){};
};

//###############################################################
//Master-Receiver
//###############################################################
template <typename T> class MReceiver: public Io
{
  public:
  //Master behaviour: 'Receive' invokes a 'Pull' from associated SSender
    virtual int  Receive   (T *m, IBuffCmd<T> *c = NULL) {return snd->Pull   (this, m, c);}
    virtual int  TryReceive(T *m, IBuffCmd<T> *c = NULL) {return snd->TryPull(this, m, c);}

  public:
    SSender<T>  *snd;
    virtual ~MReceiver(){};
};


#endif