///
/// @file      PlgIspProcOpipe.cpp
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Opipe Interface for the isp flic plugin.
///            Platform(s) supported : ma2x5x
///

#include <rtems.h>
#include "Opipe.h"
#include "OpipeApps.h"
#include <ImgFrame.h>
#include "IspCommon.h"
#include "ImgTypes.h"
#include "PlgIspProcOpipe.h"

extern void appSetParams(Opipe *p);


pthread_mutex_t PlgIspProcOpipe::lockMtx;
uint32_t        PlgIspProcOpipe::plgIspMtxInstanceCnt = 0;

//#################################################################################################
//Opipe cmx circular buffers
uint16_t PlgIspProcOpipe::cSigma  [PLG_ISP_MAX_W * I_CBUFF_H    ] SECTION(".cmx.opipe") ALIGNED(8);  //Bayer is 10bit
uint16_t PlgIspProcOpipe::cDbyrY  [PLG_ISP_MAX_W * DBYR_Y_H     ] SECTION(".cmx.opipe") ALIGNED(8);
uint16_t PlgIspProcOpipe::cSharpY [PLG_ISP_MAX_W * SHARP_Y_H    ] SECTION(".cmx.opipe") ALIGNED(8);
uint8_t  PlgIspProcOpipe::cLut    [PLG_ISP_MAX_W * O_CBUFF_H * 3] SECTION(".cmx.opipe") ALIGNED(8);
uint8_t  PlgIspProcOpipe::cUpfirDn[PLG_ISP_MAX_W * O_CBUFF_H * 3] SECTION(".cmx.opipe") ALIGNED(8);  //as for yuv444

static void opipeIspEof(Opipe *p) {
    pthread_t waitingThr = (pthread_t)p->params[0];
    rtems_event_send(waitingThr, RTEMS_EVENT_1);
}

//#################################################################################################
void PlgIspProcOpipe::IspStart(OpipeMF *opF, ImgFrame *fInp, ImgFrame *fOut, icIspConfig *ispCfg)
{
    pthread_mutex_lock(&(lockMtx));
    {//=========================================
        Opipe *p = &opF->p;
        fetchIcIspConfig(p, ispCfg); //icIspConfig -> Opipe translation
        if(ispCfg->updnCfg0.hN & ispCfg->updnCfg0.hD
                & ispCfg->updnCfg0.vN & ispCfg->updnCfg0.vD) {

        }
        else {
            //assume scale params are not set if ona of them is 0, so use default 1 values
            ispCfg->updnCfg0.hN = ispCfg->updnCfg0.hD = ispCfg->updnCfg0.vN = ispCfg->updnCfg0.vD = 1;
        }
        // option to reduce size at 1/2
        if(this->down2xOn) {
            ispCfg->updnCfg0.hD = ispCfg->updnCfg0.hD << 1;
            ispCfg->updnCfg0.vD = ispCfg->updnCfg0.vD << 1;
        }
        computePolyfirParams(&scale, ispCfg);
        //setPolyfirParams(p);//TBD: should consider scale factors here...

        //Default CFG words:
        OpipeDefaultCfg(p, SIPP_SIGMA_ID  );
        OpipeDefaultCfg(p, SIPP_RAW_ID    );
        OpipeDefaultCfg(p, SIPP_DBYR_ID   );
        OpipeDefaultCfg(p, SIPP_DOGL_ID   );
        OpipeDefaultCfg(p, SIPP_SHARPEN_ID);
        OpipeDefaultCfg(p, SIPP_CGEN_ID   );
        OpipeDefaultCfg(p, SIPP_MED_ID    );
        OpipeDefaultCfg(p, SIPP_CHROMA_ID );
        OpipeDefaultCfg(p, SIPP_CC_ID     );
        OpipeDefaultCfg(p, SIPP_LUT_ID    );
    }//=========================================

    OpipeSetSizeMF(opF, fInp->fb.spec.width, fInp->fb.spec.height);

    uint32_t outW = (ispCfg->updnCfg0.hN * opF->p.width2  - 1)/ispCfg->updnCfg0.hD + 1;
    uint32_t outH = (ispCfg->updnCfg0.vN * opF->p.height2 - 1)/ispCfg->updnCfg0.vD + 1;


    opF->pIn->ddr.base    = (uint32_t)fInp->base;
    opF->pOutY->ddr.base  = (uint32_t)fOut->base;
    //
    opF->pOutUV->ddr.base = (uint32_t)fOut->base + outW * outH;

    opF->p.params[0] = (void*)pthread_self();
    OpipeStart(&opF->p);

    fOut->fb.spec.width  = opF->p.pUpfirdn0Cfg->oW;
    fOut->fb.spec.height = opF->p.pUpfirdn0Cfg->oH;
    fOut->fb.spec.stride = fOut->fb.spec.width;


    //ptr, strides ...
    fOut->fb.p1 = (unsigned char*) fOut->base;
    fOut->fb.p2 = (unsigned char*)((uint32_t)fOut->base + outW * outH);
    fOut->fb.p3 = (unsigned char*)((uint32_t)fOut->base + outW * outH + ((outW * outH)>>2));
    fOut->fb.spec.type = YUV420p;
    fOut->fb.spec.bytesPP =  1;
    // this field can be overwrite at app level if this isp is associated with still image
    fOut->categ     = FRAME_TYPE_PREVIEW;
}


//#################################################################################################
void PlgIspProcOpipe::IspBuildPipe(OpipeMF *opF)
{
    if (0 == plgIspMtxInstanceCnt) {
        if (pthread_mutex_init(&lockMtx, NULL) != 0){
            printf("\n mutex init failed\n");
            assert(0);
        }
    }
    pthread_mutex_lock(&(lockMtx));

    plgIspMtxInstanceCnt++;

    //Must specify buffers first
    opF->in.cBufSigma.base = (uint32_t)cSigma;   opF->in.cBufSigma.h = I_CBUFF_H;
    opF->cBufDbyrY.base    = (uint32_t)cDbyrY;   opF->cBufDbyrY.h    = DBYR_Y_H;
    opF->cBufSharp.base    = (uint32_t)cSharpY;  opF->cBufSharp.h    = SHARP_Y_H;
    opF->cBufLut.base      = (uint32_t)cLut;     opF->cBufLut.h      = O_CBUFF_H;
    opF->cBufPoly.base     = (uint32_t)cUpfirDn; opF->cBufPoly.h     = O_CBUFF_H;

    OpipeCreateFull(opF, BPP(2));
    opF->p.cbEndOfFrame = opipeIspEof;


    computePolyFirInit(&scale);
    opF->p.pUpfirdn0Cfg  = &updnCfg0;
    opF->p.pUpfirdn12Cfg = &updnCfg12;
    pthread_mutex_unlock(&(lockMtx));
}

//#################################################################################################
void PlgIspProcOpipe::IspWait()
{
    rtems_event_set events;
    rtems_event_receive(RTEMS_EVENT_1, RTEMS_EVENT_ANY | RTEMS_WAIT, RTEMS_NO_TIMEOUT, &events);
    nFrmDone++;
    pthread_mutex_unlock(&(lockMtx));
}

//#################################################################################################
void PlgIspProcOpipe::IspDestroy() {
    //delete sharing resources mutex. if is not already deleted
    plgIspMtxInstanceCnt--;
    if (0 == plgIspMtxInstanceCnt) {
        if (pthread_mutex_destroy(&lockMtx) != 0){
            printf("\n mutex destroy failed\n");
            assert(0);
        }
    }
}

//#################################################################################################
void PlgIspProcOpipe::fetchIcIspConfig(Opipe *p, icIspConfig *ic)
{
    p->rawBits      = ic->pipelineBits;
    p->bayerPattern = ic->bayerOrder;

    //Filter specific
    p->pBlcCfg        = &ic->blc;
    p->pSigmaCfg      = &ic->sigma;
    p->pLscCfg        = &ic->lsc;
    p->pRawCfg        = &ic->raw;
    p->pDbyrCfg       = &ic->demosaic;
    p->pLtmCfg        = &ic->ltm;
    p->pDogCfg        = &ic->dog;
    p->pLumaDnsCfg    = &ic->lumaDenoise;
    p->pLumaDnsRefCfg = &ic->lumaDenoiseRef;
    p->pSharpCfg      = &ic->sharpen;
    p->pChrGenCfg     = &ic->chromaGen;
    p->pMedCfg        = &ic->median;
    p->pChromaDnsCfg  = &ic->chromaDenoise;
    p->pColCombCfg    = &ic->colorCombine;
    p->pLutCfg        = &ic->gamma;
    p->pColConvCfg    = &ic->colorConvert;
    p->aeCfg          = &ic->aeAwbConfig;
    p->aeStats        =  ic->aeAwbStats;
    p->afCfg          = &ic->afConfig;
    p->afStats        =  ic->afStats;
    p->pUpfirdn0Cfg   = &ic->updnCfg0;
    p->pUpfirdn12Cfg  = &ic->updnCfg12;
}

