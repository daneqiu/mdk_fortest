///
/// @file      PlgIspProcOpipe.h
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Opipe Interface for the isp flic plugin.
///            Platform(s) supported : ma2x5x
///

#ifndef COMPONENTS_FLIC_PLUGINS_PLGISPPROC_LEON_PLGISPPROCOPIPE_H_
#define COMPONENTS_FLIC_PLUGINS_PLGISPPROC_LEON_PLGISPPROCOPIPE_H_

#include <Flic.h>
#include <Sender.h>
#include <Receiver.h>
#include "ImgFrame.h"
#include "IspCommon.h"
#include "PolyFirUtils.h"

#ifndef PLG_ISP_MAX_W
#define PLG_ISP_MAX_W (2144)
#endif

class PlgIspProcOpipe {
public:
    uint32_t nFrmDone;
    static pthread_mutex_t lockMtx;
    static uint32_t plgIspMtxInstanceCnt;
    void IspStart(OpipeMF *opF, ImgFrame *fInp, ImgFrame *fOut, icIspConfig *ispCfg);
    void IspWait();
    void IspBuildPipe(OpipeMF *opF);
    void IspDestroy();
    OpipeMF     opF;
    UpfirdnCfg  updnCfg0;
    UpfirdnCfg  updnCfg12;
    uint32_t    down2xOn;
    YuvScale    scale;     //yuv420 arbitrary scale output

private:
    void setPolyfirParams(Opipe *p);
    void fetchIcIspConfig(Opipe *p, icIspConfig *ic);
    static uint16_t cSigma  [];
    static uint16_t cDbyrY  [];
    static uint16_t cSharpY [];
    static uint8_t  cLut    [];
    static uint8_t  cUpfirDn[];
    static uint8_t hCoefsL[];
    static uint8_t vCoefsL[];
};


#endif /* COMPONENTS_FLIC_PLUGINS_PLGISPPROC_LEON_PLGISPPROCOPIPE_H_ */
