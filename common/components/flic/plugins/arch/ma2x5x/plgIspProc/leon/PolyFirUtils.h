///
/// @file      PolyFirUtils.h
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Header file for PolyFirUtils.
///            Platform(s) supported : ma2x5x
///

#ifndef __ISP_COMMON_UTILS_H__
#define __ISP_COMMON_UTILS_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "IspCommon.h"
#include "ImgTypes.h"

#define MAX_PHASES 16 //polyfir max N


typedef enum
{
   FMT_420 = 0,
   FMT_422 = 1,
   FMT_444 = 2
}YuvFmt;

typedef struct
{
    //Polyfir coef (arrays need to be 4Bytes alined as they get copied
    //              to HW filter area via u32_memcopy)
    uint8_t    _hCoefsY[MAX_PHASES*8] __attribute__((aligned(4))); //horizontal  Luma    coefs
    uint8_t    _vCoefsY[MAX_PHASES*8] __attribute__((aligned(4))); //vertical    Luma    coefs
    uint8_t    _hCoefsC[MAX_PHASES*8] __attribute__((aligned(4))); //horizontal  Chroma  coefs
    uint8_t    _vCoefsC[MAX_PHASES*8] __attribute__((aligned(4))); //vertical    Chroma  coefs
    UpfirdnCfg  polyY;                            //last Luma   polyFir params
    UpfirdnCfg  polyC;                            //last Chroma polyFir params
    YuvFmt      outFmt; //0:yuv420, 1:yuv422, 2:yuv444
}YuvScale;

void computePolyFirInit  (YuvScale *s);
void computePolyfirParams(YuvScale *s, icIspConfig *ic);

#ifdef __cplusplus
}
#endif

#endif
