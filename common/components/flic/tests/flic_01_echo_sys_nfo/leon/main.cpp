///
/// @file
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Dummy echo test + RTEMS CPU/workspace stats
///

#include <stdlib.h>
#include <rtems.h>
#include <pthread.h> //to set thread names...
//#include <rtems/monitor.h>
#include <rtems/libcsupport.h>
#include <rtems/score/heap.h>
#include <rtems/cpuuse.h>
#include <rtems/posix/semaphore.h>
#include <Flic.h>
#include "mv_types.h"
#include "PlgEcho.h"

PlgEchoSend  plgSend;
PlgEchoRecv  plgRecv;
Pipeline     p;

//#########################################################
//RTEMS workspace usage
//Note: increasing number of resources in rtems_config.c
//      increases workspace !!! (e.g. 1Sem = +328 Bytes)

Heap_Information_block  wi;
rtems_resource_snapshot snap;
extern Heap_Control _Workspace_Area; //cool stuff

void rtemsInfo()
{
   rtems_workspace_get_information(&wi);
   printf("FREE total = %ld (num = %ld)\n", wi.Free.total, wi.Free.number);
   printf("USED total = %ld (num = %ld)\n", wi.Used.total, wi.Used.number);

   rtems_resource_snapshot_take(&snap);

   printf("[WorkSpace]\n");
   printf("  .area_begin  @ 0x%x\n", (unsigned int)_Workspace_Area.area_begin);
   printf("  .area_end    @ 0x%x\n", (unsigned int)_Workspace_Area.area_end  );
}

//############################################
extern "C" void *POSIX_Init (void *)
{
    #if defined(CONFIGURE_ZERO_WORKSPACE_AUTOMATICALLY)
        printf("CONFIGURE_ZERO_WORKSPACE_AUTOMATICALLY\n"); //NOT defined
    #endif

    printf("Sem size = %d\n", sizeof(      Semaphore_Control));
    printf("Sem size = %d\n", sizeof(POSIX_Semaphore_Control));


    /*DBG*/ rtemsInfo();

    plgSend.Create();
    plgRecv.Create();

    p.Add(&plgSend);
    p.Add(&plgRecv);

    plgSend.out.Link(&plgRecv.in);

    p.Start();


  //===================================================
  //name threads so it looks nice on log !!!
    rtems_interrupt_level level;
    rtems_status_code     status;

    rtems_interrupt_disable( level ); //this is a MACRO that changes "level"
      status = rtems_object_set_name(pthread_self(), "POSIX_Init");  //printf("Status=%d\n", status);
      status = rtems_object_set_name(plgSend.thread, "plgSend_thr"); //printf("Status=%d\n", status);
      status = rtems_object_set_name(plgRecv.thread, "plgRecv_thr"); //printf("Status=%d\n", status);
      UNUSED(status);
    rtems_interrupt_enable( level );
  //===================================================

  {
   rtems_interrupt_level level;
   rtems_id id = plgSend.thread;
   rtems_interrupt_disable( level ); //this is a MACRO that changes "level"
      printf("\n\n");
      printf("sizeof(pthread_t) = %d\n", sizeof(pthread_t));
      printf("id                        %lx\n", id);
      printf("rtems_object_id_get_api   %lx\n", (uint32_t)rtems_object_id_get_api  (id));
      printf("rtems_object_id_get_class %lx\n", (uint32_t)rtems_object_id_get_class(id));
      printf("rtems_object_id_get_node  %lx\n", (uint32_t)rtems_object_id_get_node (id));
      printf("rtems_object_id_get_index %lx\n", (uint32_t)rtems_object_id_get_index(id));
   rtems_interrupt_enable( level );
  }


  //cool stuff: cpu: usage
    while(1){
        sleep(1);
        rtems_cpu_usage_report();
    }

    p.Wait();
    exit(0);
}
