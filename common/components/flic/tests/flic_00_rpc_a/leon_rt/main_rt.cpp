///
/// @file
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Inter-Leon : waiting barrier
///

#include <Flic.h>
#include <FlicRmt.h>
#include <RmtBarr.h>
#include <DrvLeon.h>
#include "testConfig.h"

// LRT
extern RmtBarr bLOS[N];
       RmtBarr bLRT[N];

//####################################################################
extern "C" void* POSIX_Init(void *args)
{
   UNUSED(args);
   FlicRmt::Init();

   for(int i=0; i<N; i++)
     bLRT[i].Create(&bLOS[i]);

   DrvLeonRTSignalBootCompleted();

                   sleep(3);
   bLRT[0].Wait(); sleep(2);
   bLRT[1].Wait();
   bLRT[2].Wait();
   bLRT[3].Wait();

   DrvLeonRTSignalStop();
   FlicRmt::Destroy();

   exit(0);
}
