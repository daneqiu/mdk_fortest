///
/// @file
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Inter-Leon : waiting barrier
///

#include <Flic.h>
#include <FlicRmt.h>
#include <RmtBarr.h>
#include <DrvLeon.h>
#include <DrvCpr.h>
#include "testConfig.h"

// LOS
extern RmtBarr bLRT[N];
       RmtBarr bLOS[N];

//########################################################
extern "C" void* POSIX_Init(void *args)
{
   UNUSED(args);
   DrvCprInit();
   printf("Sys freq = %ld KHz\n", DrvCprGetSysClockKhz());

   FlicRmt::Init();

   for(int i=0; i<N; i++)
      bLOS[i].Create(&bLRT[i]);

  //Start LRT
   DrvLeonRTStartupStart();
   DrvLeonRTWaitForBoot ();

  //first 2 barriers are delayed by LRT, the last 2 by LOS
   bLOS[0].Wait(); printf("...barrier 0 \n");
   bLOS[1].Wait(); printf("...barrier 1 \n"); sleep(1);
   bLOS[2].Wait(); printf("...barrier 2 \n"); sleep(2);
   bLOS[3].Wait(); printf("...barrier 3 \n");

   DrvLeonRTWaitExecution();
   FlicRmt::Destroy();

   exit(0);
}
