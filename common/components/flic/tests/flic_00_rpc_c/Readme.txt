Roles:
 PlgA : data generator
 PlgB : buffer (propagates input on output)
 PlgC : sink
 Stub/Skel : RMI helper plugins

Example diagram for N = 2               |
     _      ____   _____                |         _      _____ _______
    | |    / __ \ / ____|               |        | |    |  __ \__   __|
    | |   | |  | | (___                 |        | |    | |__) | | |
    | |   | |  | |\___ \                |        | |    |  _  /  | |
    | |___| |__| |____) |               |        | |____| | \ \  | |
    |______\____/|_____/                |        |______|_|  \_\ |_|
                                        |
                                        |
                                        |
#############          =============    |    =============
#  PlgA     # >>>>>>>> | osStub[0] | >>>>>>> | rtSkel[0] | >>>>>>>\
#############          =============    |    =============         #############
                                        |                          #  PlgB[0]  #
                       =============    |    =============         #############
             /<<<<<<<< | osSkel[0] | <<<<<<< | rtStub[0] | <<<<<<</
#############          =============    |    =============
#  PlgB[0]  #                           |
#############          =============    |    =============
             \>>>>>>>> | osStub[1] | >>>>>>> | rtSkel[1] | >>>>>>>\
                       =============    |    =============         #############
                                        |                          #  PlgB[1]  #
#############          =============    |    =============         #############
#  PlgC     # <<<<<<<< | osSkel[1] | <<<<<<< | rtStub[1] | <<<<<<</
#############          =============    |    =============
