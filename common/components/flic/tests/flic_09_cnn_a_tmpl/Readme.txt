Notes:
 CnnFrame is a custom Frame type that contains an additional description buffer (CNN guys can define this better).
 Each of the PlgAge, PlgGender or other CNN plugin that refines the search in current app will append
 the current description list via dummy CnnFrame::DescAdd().
 Basically a plugin doesn't know about other plugs (before or after it in the chain).

 A wrapper plug could fire 2x CNNs in parallel, then wait for both. Naive example:
 if(OK == in.Receive(frame))
 {
   FathomStart(CNN_HW_1, frame, ageTraining);
   FathomStart(CNN_HW_2, frame, genderTraining);
  //have a nap :)
   FathomWait (CNN_HW_1);
   FathomWait (CNN_HW_2);
  //we're done, append resulting info
   frame.DescAdd(ageResult);
   frame.DescAdd(genderResult);
  //send to consumer or do other decision here...
   out.Send(&frame);
   frame.Release();
}


Pipe diagram:

    ####################         ####################
    # PlgSrc           #         # PlgPool          #
    ####################         ####################
    #      ==========  #         #      ==========  #
    #      |   inO  |<------------------|   out  |  #
    #      ==========  #         #      ==========  #
    #   ...            #         ####################
    #      ==========  #
    #      |   out  |  #
    #      ==========  #
    #          |       #
    ########## | #######
               |
               |
    ########## | #######
    # PlgAge   |       #
    ########## V #######
    #      ==========  #
    #      |   inO  |  #
    #      ==========  #
    #  find age ...    #
    #      ==========  #
    #      |   out  |  #
    #      ==========  #
    #          |       #
    ########## | #######
               |
               |
    ########## | #######
    # PlgGen   |       #
    ########## V #######
    #      ==========  #
    #      |   inO  |  #
    #      ==========  #
    #  find gender ... #
    #      ==========  #
    #      |   out  |  #
    #      ==========  #
    #          |       #
    ########## | #######
               |
               |
    ########## | #######
    # PlgOut   |       #
    ########## V #######
    #      ==========  #
    #      |   in   |  #
    #      ==========  #
    ####################
