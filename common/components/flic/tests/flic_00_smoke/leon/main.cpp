///
/// @file
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Smoke test
///

#include <stdlib.h>
#include <stdio.h>
#include <rtems.h>
#include <vector>
#include "mv_types.h"
#include <UnitTestApi.h>

void test1(); //create/use/destroy cycle
void test2(); //code generation estimation for MsgBuf
void test3(); //pop closest TS
void test4(); //overwrite oldest
void test5(); //mSend->sReceive
void test6(); //code generation estimation for MSnd/SRecv
void test7();
void test8();

//############################################
extern "C" void *POSIX_Init (void *args)
{
    unitTestInit();
    UNUSED(args);

    test1();
    test2();
    test3();
    test4();
    test5();
    test6();
    test7();
    test8();

    unitTestFinalReport();
    exit(0);
}
