///
/// @file
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Inter-Leon : basic IRQ test
///

#include <stdlib.h>
#include <stdio.h>
#include <rtems.h>
#include <DrvLeon.h>
#include <DrvCpr.h>
#include <DrvIcb.h>
#include <DrvTimer.h>
#include "mv_types.h"
#include "testConfig.h"
#include <FlicRmt.h>

extern sem_t lrt_waitCall;

//########################################################
extern "C" void* POSIX_Init(void *args)
{
 //uint64_t startTime, endTime, timeTaken;
   UNUSED(args);

   DrvCprInit();
   DrvTimerInit();

   FlicRmt::Init();

  //Start LRT
   DrvLeonRTStartupStart();
   DrvLeonRTWaitForBoot ();

   for(int i = 0; i < INTERRUPTS_TO_GENERATE; i++)
   {
      RmiCtx ctx;
      ctx.semToWake = &lrt_waitCall;
      FlicRmt::RmiCall(&ctx); //waits for ACK
   }

    DrvLeonRTWaitExecution();
    printf("LOS finish __\n");
    exit(0);
}
