Note: PlgA fetches frame from its pool and forwards to PlgB on the other Leon,
      then PlgB returns the frame to the initial Leon via Push

Roles: PlgA = source
       PlgB = buffer (propagates input on output)
       PlgC = sink

     _      ____   _____               |         _      _____ _______
    | |    / __ \ / ____|              |        | |    |  __ \__   __|
    | |   | |  | | (___                |        | |    | |__) | | |
    | |   | |  | |\___ \               |        | |    |  _  /  | |
    | |___| |__| |____) |              |        | |____| | \ \  | |
    |______\____/|_____/               |        |______|_|  \_\ |_|
                                       |
                                       |
##############                         |
#  pool [i]  #                         |
##############                         |
      |                                |
      |                   (stub)       |      (skeleton)
      V                                |
##############          ============   |     ============
#  plugA[i]  # >>>>>>>> | stubB[i] | ======> | skelB[i] | >>>>>>>\
##############          ============   |     ============         ##############
                                       |                          #  plugB[i]  #
##############          ============   |     ============         ##############
#  plugC[i]  # <<<<<<<< | skelC[i] | <====== | stubC[i] | <<<<<<</
##############          ============   |     ============


