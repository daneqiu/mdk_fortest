flic-dir := $(MDK_INSTALL_DIR_ABS)/components/flic
include-dirs-flic-y := \
	$(flic-dir)/types/leon \
	$(flic-dir)/common/commonTypes/leon \
	$(flic-dir)/plugins/common/commonTypes/leon \
	$(flic-dir)/core/leon/top \
	$(flic-dir)/core/leon/sender \
	$(flic-dir)/core/leon/receiver \
	$(flic-dir)/core/leon/buffer \
	$(flic-dir)/core/leon/pool \
  $(flic-dir)/core/leon/remote

include-dirs-flic-$(CONFIG_SOC_2x5x) += \
	$(flic-dir)/plugins/common/arch/ma2x5x/leon

include-dirs-flic-$(CONFIG_SOC_2x8x) += \
	$(flic-dir)/plugins/common/arch/ma2x8x/leon

include-dirs-los-y += $(include-dirs-flic-y)
include-dirs-lrt-y += $(include-dirs-flic-y)

subdirs-flic-y += \
	core/leon

subdirs-los-y += $(subdirs-flic-y)
subdirs-lrt-y += $(subdirs-flic-y)

