///
/// @file      AppSpecInterface.h
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @author    truicam
/// @created   Apr 12, 2017
/// @brief     Header file for AppSpecInterface.
///            Platform(s) supported : ma2x5x
///

#ifndef COMPONENTS_FLIC_EXAMPLES_ARCH_MA2X5X_FLIC08ISPLIVEUPTO3CAMS_LEON_APPSPECINTERFACE_H_
#define COMPONENTS_FLIC_EXAMPLES_ARCH_MA2X5X_FLIC08ISPLIVEUPTO3CAMS_LEON_APPSPECINTERFACE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "LocDbgRoutines.h"
#include "LcdHdmiSend.h"

#include "PipeIspSingle2.h"

// define pipeline create, destroy if what to be associated with camera control
#ifndef PIPE_CREATE
#define PIPE_CREATE pipeIspSingleCreate
#endif
#ifndef PIPE_DESTROY
#define PIPE_DESTROY pipeIspSingleDestroy
#endif

// define output interface
#ifndef sendOutCreate
#define sendOutCreate   lcdHdmiSendCreate
#endif
#ifndef sendOutInit
#define sendOutInit     lcdHdmiSentInit
#endif
#ifndef sendOutControl
#define sendOutControl  lcdHdmiSendControl
#endif
#ifndef sendOutSend
#define sendOutSend     lcdHdmiSend
#endif
#ifndef sendOutFini
#define sendOutFini     lcdHdmiSendFini
#endif

#ifdef __cplusplus
}
#endif


#endif /* COMPONENTS_FLIC_EXAMPLES_ARCH_MA2X5X_FLIC08ISPLIVEUPTO3CAMS_LEON_APPSPECINTERFACE_H_ */
