///
/// @file      PipeIspSingle2.h
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Header file for PipeIsp.
///            Platform(s) supported : ma2x5x
///


#ifndef _PIPE_ISP_SINGLE_2_H
#define _PIPE_ISP_SINGLE_2_H

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"

typedef int (*GetSrcSzLimits)(uint32_t srcId, icSourceSetup* srcSet);

/// Create 3 camera flic base isp pipeline interface.
void pipeIspSingleCreate(GetSrcSzLimits getSrcSzLimits);

///Destroy 3 camera flic base isp pipeline interface.
void pipeIspSingleDestroy(void);


#ifdef __cplusplus
}
#endif

#endif //_PIPE_ISP_SINGLE_H
