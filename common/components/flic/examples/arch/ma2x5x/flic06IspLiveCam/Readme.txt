flic06IspLiveCam

Supported Platform
==================
Myriad2 - This example works on Myriad2 ma2150 and ma2450 silicon versions

Overview
==========
Stream from IMX208 left sensor. Run isp, and display the output on HDMI.

Hardware needed
==================
Myriad2 - This software should run on MV212 boards for ma2450 or MV182 boards for ma2150.
imx214 on CamA socket, 2 imx208 on CamB socket.
HDMI cable connected to an FullHD Display. 

Software description
=======================
+ Source to Guzzi  (READOUT_START,END, LINE_HIT)
+ ISP    to Guzzi  (ISP PROCESSING, FRAME_SKIP)
+ GUZZI  to Source (START_SOURCE)

                    =============       ==============
                    | PlgSource | <---- | PlgFrmPool |
                    =============       ==============
                          |
                          V
=============       =============       ==============
| PlgGuzzi  | ----> | PlgISP    | <---- | PlgFrmPool |
=============       =============       ==============
                          |
                          V
                    =============
                    | PlgOut    |
                    =============

###############################################################################################
ISP input-cfg release is handled by Guzzy when it receives IC_EVENT_TYPE_ISP_END
so no "Release" to be performed on config message from Isp plugin
###############################################################################################

Build
==================
Please type "make help" if you want to learn available targets.

!!!Before cross-compiling make sure you do a "make clean"!!!

Myriad2 - To build the project please type:
     - "make clean"
     - "make all"

Setup
==================
Myriad2 silicon - To run the application:
    - open terminal and type "make start_server"
    - open another terminal and type "make debug MV_SOC_REV={Myriad_version}"

Where {Myriad_version} may be ma2150 or ma2450.
The default Myriad revision in this project is ma2450 so it is not necessary
to specify the MV_SOC_REV in the terminal.
