
#include <stdio.h>
#include "usb_vsc.h"

#include "OsDrvUsbPhy.h"

#include "usbpumpdebug.h"
#include "usbpump_application_rtems_api.h"

#include "vsc2app_outcall.h"
#include "usbpump_vsc2app.h"

#define ENDPOINT1 0

extern USBPUMP_VSC2APP_CONTEXT *    pSelf;

static USBPUMP_APPLICATION_RTEMS_CONFIGURATION sg_DataPump_AppConfig =
USBPUMP_APPLICATION_RTEMS_CONFIGURATION_INIT_V3(
    /* nEventQueue */       64,
    /* pMemoryPool */       NULL,
    /* nMemoryPool */       0,
    /* DataPumpTaskPriority */  100,
    /* DebugTaskPriority */     200,
    /* UsbInterruptPriority */  10,
    /* pDeviceSerialNumber */   NULL,
    /* pUseBusPowerFn */        NULL,
    /* fCacheEnabled */     USBPUMP_MDK_CACHE_ENABLE,
    /* DebugMask */         UDMASK_ANY | UDMASK_ERRORS,
    /* pPlatformIoctlFn */    NULL,
    /* fDoNotWaitDebugFlush */  0
    );

static int readtimeout, writetimeout;

#include <unistd.h>
void* usb_vsc_init()
{
    OsDrvUsbPhyInit(NULL);
    if (!UsbPump_Rtems_DataPump_Startup(&sg_DataPump_AppConfig))
    {
        printf("\n\nUPF_DataPump_Startup() failed!\n\n\n");
    }

    //TODO:This sleep is required as sometimes the semaphore is not created before
    //the obtain below is called. Need to investigate how to solve without sleep
    usleep(10000);

    //first read semaphore will be used to wait the interface up event
    rtems_semaphore_obtain(pSelf->semReadId[0], RTEMS_WAIT, 0);
    readtimeout = writetimeout = 0;

    //    sleep(3);
    return NULL;
}


int VSCRead(void* buffer, size_t size, int timeout, int endpoint){
    int rc;
    if(!readtimeout)
        UsbVscAppRead(pSelf, size, buffer, endpoint);
    readtimeout = 0;
    rc = rtems_semaphore_obtain(pSelf->semReadId[endpoint], RTEMS_WAIT, timeout);
    if(rc == RTEMS_SUCCESSFUL)
        return size;
    if(rc == RTEMS_TIMEOUT)
    {
        readtimeout = 1;
        return -2;
    }
    return -1;
}


int VSCWrite(void* buffer, size_t size, int timeout, int endpoint){
    int rc;
    if(!writetimeout)
        UsbVscAppWrite(pSelf, size, buffer, endpoint);
    writetimeout = 0;
    rc = rtems_semaphore_obtain(pSelf->semWriteId[endpoint], RTEMS_WAIT, timeout);
    if(rc == RTEMS_SUCCESSFUL)
        return size;
    if(rc == RTEMS_TIMEOUT)
    {
        writetimeout = 1;
        return -2;
    }
    return -1;
}
