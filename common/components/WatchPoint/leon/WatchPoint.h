///
/// @file
/// @brief Watch Points functions API
///

#ifndef _WATCH_POINT_H_
#define _WATCH_POINT_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include <DrvSvu.h>

#ifdef __cplusplus
extern "C"
{
#endif

#define MAX_LEON_WP_NUM  3 // [0,...3]

typedef enum
{
    WP_SUCCESS = 0,                                             /* Successful operation */
    WP_INVALID_PARAMETERS = 1000
} WP_STATUS_CODE;

int LeonAddHWWatchPoint(
        int WPnum,      // Watchpoint to be used, [0,...3]
        int StartAddr,  // Start address of the breakpoint range
        int Size,       // >=4 (bytes). Must be power of 2 and aligned to StartAddr
        int instr_fech, // bool
        int data_rd,    // bool
        int data_wr);   // bool

void ShaveDataAccessRangeWP(
        int ShaveNum,   // Shave Number
        int Addr,       // Start address of the watchpoint range
        int size);

int AllShaveDataAccessRangeWP(
        int Addr,       // Shart address of the watchpoint range
        int size);


/// @}
#ifdef __cplusplus
}
#endif

#endif
