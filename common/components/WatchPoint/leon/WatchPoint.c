///
/// @file
///
/// @brief     WatchPoint Functions.
///
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <WatchPoint.h>
#include <registersMyriad.h>
#include <stdio.h>
#include <string.h>

#define is_invalid_WP_number(num) ((num<0) || (num>MAX_LEON_WP_NUM))

int LeonAddHWWatchPoint(
        int WPnum,          // Watchpoint to be used, [0,...3]
        int StartAddr,      // Start address of the breakpoint range
        int Mask,           // >=4 (bytes). Must be power of 2 and aligned to StartAddr
        int instr_fetch,    // bool
        int data_rd,        // bool
        int data_wr)        // bool
{
    int asr_even = ( StartAddr & ~0x3   | (!!instr_fetch));
    int asr_odd  = ( ~(Mask-1) & ~0x3   | ((!!data_rd)<<1) | (!!data_wr));

    if( is_invalid_WP_number (WPnum))
    {
        return WP_INVALID_PARAMETERS;
    }

    switch (WPnum)
    {
    case 0:
        asm volatile ("wr %0, %%asr25" :: "r"(asr_odd));
        asm volatile ("wr %0, %%asr24" :: "r"(asr_even));
        break;
    case 1:
        asm volatile ("wr %0, %%asr27" :: "r"(asr_odd));
        asm volatile ("wr %0, %%asr26" :: "r"(asr_even));
        break;
    case 2:
        asm volatile ("wr %0, %%asr29" :: "r"(asr_odd));
        asm volatile ("wr %0, %%asr28" :: "r"(asr_even));
        break;
    case 3:
        asm volatile ("wr %0, %%asr31" :: "r"(asr_odd));
        asm volatile ("wr %0, %%asr30" :: "r"(asr_even));
        break;
    default :
        return WP_INVALID_PARAMETERS;
    }
    return WP_SUCCESS;
}


void ShaveDataAccessRangeWP(int shaveNum, int breakAddr, int size)
{
    SET_REG_BITS_MASK(DCU_DCR(shaveNum), DCR_DBGE); // Debug enable
    // Both data breakpoints are being used to work on range
    SET_REG_WORD(DCU_DBA0(shaveNum), breakAddr);
    SET_REG_WORD(DCU_DBA1(shaveNum), breakAddr+size-1);

//    printf("shave id %d DCU_DCR addr = 0x%x\n", shaveNum, DCU_DCR(shaveNum));
//    printf("shave id %d DCU_DBA0 addr = 0x%x\n", shaveNum, DCU_DBA0(shaveNum));
//    printf("shave id %d DCU_DBA1 addr = 0x%x\n", shaveNum, DCU_DBA1(shaveNum));

    int ctrlReg = 1 <<  0  // Breakpoint enable
                | 1 <<  1  // Addr comparison enable
                | 1 << 25  // Inclusive range
                ;
    SET_REG_WORD(DCU_DBC0(shaveNum), ctrlReg); // Cfg only BP0
//    printf("shave id %d DCU_DBC0 addr = 0x%x value is %x\n", shaveNum, DCU_DBC0(shaveNum), ctrlReg);
}

int AllShaveDataAccessRangeWP(int Addr, int size)
{
    int i;

    for (i=0; i< TOTAL_NUM_SHAVES; i++)
    {
        ShaveDataAccessRangeWP(i, Addr, size);
    }

    return WP_SUCCESS;
}
