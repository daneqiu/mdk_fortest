///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Home for VideoDemo_MjpegInit()
///

// 1: Includes
// ----------------------------------------------------------------------------
#ifndef USE_USB_VSC
#ifndef USE_LINK_JTAG

#include "udevice.h"
#include "uplatformapi.h"
#include "usbpumpapi.h"
#include "usbpump_usbseri.h"
#include "usbseri_oslinkage.h"
#include "usbpump_application_rtems_api.h"
#include "usbpump_rtems.h"
#include "usbpumpobjectapi.h"

#include "bsp.h"
#include "rtems.h"
#include "serialPort.h"
#include <termios.h>
#include <rtems/termiostypes.h>
#include <stdio.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>

#include <rtems/shell.h>
#include "mv_types.h"

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
#define INIT_DONE_EVENT_1 RTEMS_EVENT_1
#define INIT_DONE_EVENT_2 RTEMS_EVENT_2
#define START_PLANE_SERIAL_EVENT 1
#define DTR_ON_SERIAL_EVENT      2
#define DTR_OFF_SERIAL_EVENT     3

typedef struct {
    rtems_id readSem;
    rtems_id writeSem;
    size_t readSize;
    size_t writeSize;
} readWriteControl;

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
static RtemsEvent_t serialPortEvent;
static void* hUsbPort[2];
static int usb_uart_initialized[2];
static readWriteControl readWriteCtx[2];
static int32_t dtrOn;
static int32_t startPlane;
static rtems_id initTaskId[2];
static uint32_t initDoneEvent[2] = {INIT_DONE_EVENT_1, INIT_DONE_EVENT_2};

static const USBSERI_EVENT_CODE_MAP EventCodeMap =
{
  -1, // RxDataAvailable;
  DTR_ON_SERIAL_EVENT, // DtrChangeHigh;
  DTR_OFF_SERIAL_EVENT, // DtrChangeLow;
  -1, // EscapeDataConnect;
  -1, // TxNotFull;
  -1, // BreakReceived;
  START_PLANE_SERIAL_EVENT, // StartPlane;
  -1, // StopPlane;
};

// 4: Static Local Data
// ----------------------------------------------------------------------------

// 5: Static Function Prototypes
// ----------------------------------------------------------------------------

// 6: Functions Implementation
// ----------------------------------------------------------------------------
static void readDone(void *pDoneCtx, USBSERI_STATUS status,
              unsigned char *pBuffer, size_t nBuffer,
              size_t nActual)
{
    UNUSED(status);
    UNUSED(pBuffer);
    UNUSED(nBuffer);
    readWriteControl *readCtx = pDoneCtx;
    if (status == USBSERI_STATUS_OK)
    {
        readCtx->readSize = nActual;
        rtems_semaphore_release(readCtx->readSem);
    }
    else{
        printf("Read error: %d\n", status);
    }
}

static size_t USB_SyncRead(void *hPort, unsigned char *pBuffer, size_t nBuffer,
                           rtems_device_minor_number minor)
{
    int32_t status;

    status = Usbseri_ReadSubmit_V2(hPort, pBuffer, nBuffer,
                                   0, readDone, &readWriteCtx[minor]);
    if (status != USBSERI_STATUS_OK)
        return  0;
    status = rtems_semaphore_obtain(readWriteCtx[minor].readSem,
                                    RTEMS_WAIT, RTEMS_NO_TIMEOUT);
    if (status != RTEMS_SUCCESSFUL)
    {
        printf("Read sem obtain error: %ld\n", status);
        return 0;
    }
    return readWriteCtx[minor].readSize;
}

static void writeDone(void *pDoneCtx, USBSERI_STATUS status,
               unsigned char *pBuffer, size_t nBuffer,
               size_t nActual)
{
    UNUSED(status);
    UNUSED(pBuffer);
    UNUSED(nBuffer);
    readWriteControl *writeCtx = pDoneCtx;
    if (status == USBSERI_STATUS_OK)
    {
        writeCtx->writeSize = nActual;
        rtems_semaphore_release(writeCtx->writeSem);
    }
    else
        printf("Write error: %d\n", status);
}

static size_t USB_SyncWrite(void *hPort, unsigned char *pBuffer, size_t nBuffer,
                            rtems_device_minor_number minor)
{
    int32_t status;

    status = Usbseri_WriteSubmit_V2(hPort, pBuffer, nBuffer,
                                    0, writeDone, &readWriteCtx[minor]);
    if (status != USBSERI_STATUS_OK)
        return  0;
    status = rtems_semaphore_obtain(readWriteCtx[minor].writeSem,
                                    RTEMS_WAIT, RTEMS_NO_TIMEOUT);
    if (status != RTEMS_SUCCESSFUL)
    {
        printf("Write sem obtain error: %ld\n", status);
        return 0;
    }
    return readWriteCtx[minor].writeSize;
}

static void SerialPortEventHandler(void *hPort, void *pClientContext,
    unsigned char event, void *pEventData)
{
    UNUSED(pEventData);
    UNUSED(hPort);
    SerialPortContext *context = pClientContext;
    uint32_t idx = context->portIndex;

    switch (event) {
        case START_PLANE_SERIAL_EVENT:
            startPlane = 1;
        break;
        case DTR_ON_SERIAL_EVENT:
            dtrOn = 1;
        break;
        case DTR_OFF_SERIAL_EVENT:
            dtrOn = 0;
        break;
        default:
        break;
    }
    if (startPlane)
        rtems_event_send(initTaskId[idx], initDoneEvent[idx]);
}

SerialPortContext *SerialPortInit(UPLATFORM *pPlatform, rtems_device_minor_number minor)
{
    rtems_id taskId = 0;
    int32_t status;

    SerialPortContext *pSerialPortContext;

    initTaskId[minor] = rtems_task_self();
    // Allocate context
    pSerialPortContext = UsbPumpPlatform_MallocZero(
            pPlatform,
            sizeof(*pSerialPortContext)
    );

    if (pSerialPortContext == NULL)
    {
        TTUSB_OBJPRINTF((
                &pSerialPortContext->ObjectHeader,
                UDMASK_ERRORS,
                "?" FUNCTION ":"
                " pSerialPortContex allocation fail\n"
        ));
        return NULL;
    }

    // USB_OpenDevice must not be called in DataPump context because
    // it will block the DataPump task
    pSerialPortContext->pPlatform = pPlatform;
    pSerialPortContext->portInstanceNo = minor * 2 + SERIALPORT_INSTANCE_NO;
    pSerialPortContext->portHandle = USB_OpenDevice(minor * 2 + SERIALPORT_INSTANCE_NO);
    pSerialPortContext->portIndex = minor;
    if (pSerialPortContext->portHandle)
    {
        hUsbPort[minor] = pSerialPortContext->portHandle;
        USB_RegisterEventNotify(hUsbPort[minor],
            SerialPortEventHandler, pSerialPortContext, &EventCodeMap);
        status = rtems_semaphore_create(
            rtems_build_name('S', 'R', 'D', '0'), 0,
            RTEMS_SIMPLE_BINARY_SEMAPHORE, 0, &readWriteCtx[minor].readSem);
        if (status != RTEMS_SUCCESSFUL)
            printf("Error creating read semaphore: %ld\n", status);

        status = rtems_semaphore_create(
            rtems_build_name('S', 'W', 'R', '0'), 0,
            RTEMS_SIMPLE_BINARY_SEMAPHORE, 0, &readWriteCtx[minor].writeSem);
        if (status != RTEMS_SUCCESSFUL)
            printf("Error creating write semaphore: %ld\n", status);
//        while ( rtems_event_receive(
//            initDoneEvent[minor],
//            RTEMS_WAIT | RTEMS_EVENT_ANY,
//            100,
//            &eventOut
//        )){
//            printf("timeout\n");
//            sleep(1);
//        }
    }
    else
    {
        printf("serial port init error\n");
    }
    serialPortEvent.taskId = taskId;

    //TODO this may not be the full context, since it is also modified
    //by the init task
    return pSerialPortContext;
}

// RTEMS specific event functions needed for
// synchronization with DataPump
void Usbseri_OS_InitializeEvent(
    USBSERI_OS_ABSTRACT_EVENT *pEvent,
    unsigned sizeEvent
)
{
    (void)sizeEvent;
    (void)pEvent;
    serialPortEvent.taskId = rtems_task_self();
    UHIL_cpybuf(pEvent, &serialPortEvent, sizeof(serialPortEvent));
}

void Usbseri_OS_DeinitializeEvent(USBSERI_OS_ABSTRACT_EVENT *pEvent)
{
    (void)pEvent;
    // empty
}

int Usbseri_OS_WaitForEvent(USBSERI_OS_ABSTRACT_EVENT *pEvent)
{
    rtems_event_set eventOut;
    (void)pEvent;

    return rtems_event_receive(
        SERIALPORT_EVENT,
        RTEMS_WAIT | RTEMS_EVENT_ANY,
        RTEMS_NO_TIMEOUT,
        &eventOut
    );
}

void Usbseri_OS_SetEvent(USBSERI_OS_ABSTRACT_EVENT *pEvent)
{
    RtemsEvent_t *pSerialEvent = (RtemsEvent_t *)pEvent;
    rtems_event_send(pSerialEvent->taskId, SERIALPORT_EVENT);
}

// libio functions

rtems_device_driver usb_uart_initialize(
  rtems_device_major_number major,
  rtems_device_minor_number minor,
  void *pargp __attribute__((unused))
)
{
    char device_name[16];
    rtems_device_driver status;
    for (minor = 0; minor < 2; minor++)
    {
        sprintf(device_name,"/dev/usb%d", (int)minor);

        status = rtems_io_register_name(
                        device_name,
                        major,
                        minor
        );

        if (status != RTEMS_SUCCESSFUL)
            rtems_fatal_error_occurred(status);
    }
    return RTEMS_SUCCESSFUL;
}

rtems_device_driver usb_uart_open(
  rtems_device_major_number major __attribute__((unused)),
  rtems_device_minor_number minor __attribute__((unused)),
  void *pargp
)
{
    (void)pargp;
    if (!usb_uart_initialized[minor])
    {
        SerialPortInit((UPLATFORM*)UsbPump_Rtems_DataPump_GetPlatform(), minor);
        usb_uart_initialized[minor] = 1;
    }
    return RTEMS_SUCCESSFUL;
}

rtems_device_driver usb_uart_close(
  rtems_device_major_number major __attribute__((unused)),
  rtems_device_minor_number minor __attribute__((unused)),
  void *pargp __attribute__((unused))
)
{
    return RTEMS_SUCCESSFUL;
}

rtems_device_driver usb_uart_read(
    rtems_device_major_number  major  __attribute__((unused)),
    rtems_device_minor_number  minor  __attribute__((unused)),
    void  *pargp   )
{
  rtems_libio_rw_args_t *rw_args = (rtems_libio_rw_args_t *) pargp;

  if (rw_args)
  {
      if (rw_args->count > 0)
      {
          rw_args->bytes_moved = USB_SyncRead(hUsbPort[minor],
              (unsigned char*)(rw_args->buffer), rw_args->count, minor);
      }
  }
  if (rw_args->bytes_moved == 0)
  {
      // make sure that a read from stdin will not return EOF
      // because this will prevent subsequent reads
      rw_args->bytes_moved = RTEMS_UNSATISFIED;
  }

  return RTEMS_SUCCESSFUL;
}

rtems_device_driver usb_uart_write(
  rtems_device_major_number  major  __attribute__((unused)),
  rtems_device_minor_number  minor  __attribute__((unused)),
  void  *pargp  )
{
    rtems_libio_rw_args_t *rw_args = (rtems_libio_rw_args_t *) pargp;

    if (rw_args)
        rw_args->bytes_moved = USB_SyncWrite(hUsbPort[minor],
            (unsigned char*)(rw_args->buffer), rw_args->count, minor);

    return RTEMS_SUCCESSFUL;
}

rtems_device_driver usb_uart_control(
  rtems_device_major_number major __attribute__((unused)),
  rtems_device_minor_number minor __attribute__((unused)),
  void *pargp __attribute__((unused))
)
{
  return RTEMS_NOT_IMPLEMENTED;
}
#endif
#endif

