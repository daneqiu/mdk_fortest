///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Application configuration Leon header
///
#ifndef _USBLINKDISPATCHER_H
#define _USBLINKDISPATCHER_H
#define _USBLINK_ENABLE_PRIVATE_INCLUDE_
#include "UsbLinkPrivateDefines.h"

#ifdef __cplusplus
extern "C"
{
#endif

//adds a new event with parameters and returns event.header.id
usbLinkEvent_t* dispatcherAddEvent(usbLinkEventOrigin_t origin, usbLinkEvent_t *event);
int dispatcherWaitEventComplete();
int dispatcherUnblockEvent(eventId_t id, usbLinkEventType_t type, streamId_t stream);
void dispatcherBlockEvent(eventId_t id, usbLinkEventType_t type);
void dispatcherFindCorrespondingWriteRtsAndFillData(usbLinkEvent_t* event);
struct dispatcherControlFunctions {
    int (*eventSend) (usbLinkEvent_t*);
    int (*eventReceive) (usbLinkEvent_t*);
    int (*localGetResponse) (usbLinkEvent_t*,
                             usbLinkEvent_t*);
    int (*remoteGetResponse) (usbLinkEvent_t*,
                              usbLinkEvent_t*);
    void (*closeLink) ();
    void (*resetDevice) ();
};

int dispatcherInitialize(struct dispatcherControlFunctions* controlFunc);

#ifdef __cplusplus
}
#endif

#endif
