if(EXISTS "$ENV{MV_COMMON_BASE}")
    set(MV_COMMON_BASE $ENV{MV_COMMON_BASE})
else()
    set(MV_COMMON_BASE ${CMAKE_CURRENT_LIST_DIR}/../..)
endif(EXISTS "$ENV{MV_COMMON_BASE}")

set(USBLINK_INCLUDE_DIRECTORIES
    ${MV_COMMON_BASE}/components/USBLink/pc
    ${MV_COMMON_BASE}/components/USBLink/shared
    ${MV_COMMON_BASE}/swCommon/include
    ${MV_COMMON_BASE}/shared/include
    ${MV_COMMON_BASE}/../projects/pcUsbTool/common/vsc
    ${MV_COMMON_BASE}/../projects/pcUsbTool/linux/uvc
    ${MV_COMMON_BASE}/../projects/pcUsbTool
    /usr/include/libusb-1.0
    ${MV_COMMON_BASE}/swCommon/pcModel/half
)

set(USBLINK_SOURCES
    ${MV_COMMON_BASE}/components/USBLink/pc/UsbLinkPlatform.cpp
    ${MV_COMMON_BASE}/components/USBLink/shared/UsbLink.c
    ${MV_COMMON_BASE}/components/USBLink/shared/UsbLinkDispatcher.c
    ${MV_COMMON_BASE}/swCommon/src/swcFifo.c
    ${MV_COMMON_BASE}/../projects/pcUsbTool/fastmemDevice.cpp
    ${MV_COMMON_BASE}/../projects/pcUsbTool/common/vsc/fastmemUsb.cpp
    ${MV_COMMON_BASE}/../projects/pcUsbTool/linux/uvc/fastmemV4L.cpp
)

