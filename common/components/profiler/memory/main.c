#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "profMemory.h"

int main() {
    ProfileEntry los;
    ProfileEntry lrt;
    ProfileEntry shv0;
    ProfileEntry shv1;
    ProfileEntry shv2;
    int i;
    float f = .0;
    char c = 'a';

    // TODO: needed??
    init(&los, "LOS");
    init(&lrt, "LRT");
    void dump();
    dump();
    init(&shv0, "shave 0");
    init(&shv1, "shave 1");
    init(&shv2, "shave 2");

    for (i=0; i<32; i++){
        EntryType data = {
            .i = i,
            .f = f,
            .c = c
        };
        f += 1.1f;
        c++; if (c > 'z') c = 'a';
        sprintf(data.str, "\"%d\"", i);
        if (i%2==1) write(&los, &data);
        else if (i%3==1) write(&lrt, &data);
        else if (i%5==1) write(&shv0, &data);
        else if (i%7==1) write(&shv1, &data);
        else write(&shv2, &data);
        dump();
    }

    dump();
}
///
// TODO: outside
void lock(){}
void unlock(){}
static int __profilerEnabled = 1;
void disableProfiler(){ __profilerEnabled = 0; }
int profilerEnabled() { return __profilerEnabled; }
