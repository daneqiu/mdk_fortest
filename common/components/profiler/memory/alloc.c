#include "profMemory.h"

#include <assert.h>
#include <stdio.h> // for print purpose only

static ProfileEntry_t buffer[NB_BUFFERS][NB_ELEMENTS];
//static ProfileEntry_t* links[NB_BUFFERS] = { NULL }; // TODO: add space for ProfileBufferEntry_t
static ProfileBufferEntry_t links[NB_BUFFERS]; // TODO: add space for ProfileBufferEntry_t

MasterRecord masterRecord = {NULL};

__attribute((constructor))
__attribute__((no_instrument_function))
static void __profMemAllocInit() {
    __builtin_memset(links, 0, sizeof(links));
    __builtin_memset(buffer, 0, sizeof(buffer));
}

__attribute__((no_instrument_function))
void init(ProfileBufferEntry_t* entry, const char* name) {
    int i;
    lock();
    for (i=0; i<NB_BUFFERS; i++) {
        if (!links[i].head) {
            entry->headBufNumber = i;
            entry->next = NULL;
            __builtin_strcpy(entry->name, name);
            links[i].head = buffer[i];
            links[i].end = &buffer[i][NB_ELEMENTS];
            links[i].next = NULL;
            break;
        }
    }
    assert(i < NB_BUFFERS);
    if (masterRecord.firstEntry) {
        entry->next = masterRecord.firstEntry->next;
        masterRecord.firstEntry->next = entry;
    }
    else {
        entry->next = entry;
    }
    masterRecord.firstEntry = entry;
    unlock();

}

// TODO: move in Makefile
#define OVERWRITE 1
// TODO: reuse policy: reuse_all, reuse_own. reuse_own protect from a core in infinite loop to fill all buffers with
// waiting() entry/exit calls
// if reuse_own, pass the i as argument
static int reuseGuard = NB_BUFFERS;
__attribute__((no_instrument_function))
static int reuse() {
    int i = masterRecord.firstEntry->headBufNumber;
    int k;
    if (!--reuseGuard) {
        disableProfiler();
        return -1;
    }
    masterRecord.firstEntry = masterRecord.firstEntry->next;
    // free structure
    for (k=0; k<NB_BUFFERS; k++) if (links[i].head == buffer[k] && i != k) break;
    if ( k != NB_BUFFERS ) {
        printf("reuse buffer %d, k=%d\n", i, k);
        masterRecord.firstEntry->headBufNumber = k;
        reuseGuard = NB_BUFFERS;
        return i;
    }
    printf("go into %d ...\n", masterRecord.firstEntry->next->headBufNumber);
    masterRecord.firstEntry = masterRecord.firstEntry->next;
    return reuse();
}

__attribute__((no_instrument_function))
ProfileBufferEntry_t* alloc(ProfileBufferEntry_t* pEntry) {
    // search new index
    int i;
    lock();
    for (i=0; i<NB_BUFFERS; i++) if (!links[i].head) break;
    if (i == NB_BUFFERS) {
#if OVERWRITE
        // find oldest buffer to reuse
        i = reuse();
        if (i<0) return NULL;
#else
        disableProfiler();
        return NULL;
#endif
    }

    // set next buffer
    links[pEntry->headBufNumber].head = links[i].head = buffer[i];
    links[pEntry->headBufNumber].end = NULL; // TODO: maybe I can use a different mechanism now, like *next
    pEntry->headBufNumber = i;
    unlock();
    __builtin_memset(buffer[i], 0, NB_ELEMENTS * sizeof(ProfileEntry_t));
    printf("+ allocated buffer %d\n", i);
}

__attribute__((no_instrument_function))
void bufferFull(ProfileBufferEntry_t** entry, ProfileBufferEntry_t* (*alloc)()) {
    ProfileBufferEntry_t* nextEntry = alloc();
    if (!nextEntry) {
        disableProfiler();
        return;
    }
    // TODO: lock
    nextEntry->next = *entry;
    *entry = nextEntry;
    // TODO: unlock
//    __builtin___clear_cache(nextEntry->entry->begin, end);
}


// TODO: functionality replaced in profilePushEntry()
/*
void write(ProfileEntry* pEntry, ProfileEntry_t * data) {
    if (!profilerEnabled()) return;
    printf("writing %d ... into buffer %d\n", data->i, pEntry->headBufNumber);
    *links[pEntry->headBufNumber]++ = *data;
    if (links[pEntry->headBufNumber] == &buffer[pEntry->headBufNumber][NB_ELEMENTS]) {
        // buffer end
        alloc(pEntry);
    }
}
*/

////////////////////////////////////////////////////////////////////////////////////////
//
// print for debugging purpose
//

void printProfileEntry_tBuffer(ProfileEntry_t* start, ProfileEntry_t* end) {
    while (start < end) {
        printf("i=%d c=%c, f=%f, str=%s\n", start->i, start->c, start->f, start->str);
        start++;
    }
}

void printProfileEntry(ProfileEntry* q) {
    int i;
    for (i=0; i<NB_BUFFERS; i++) {
        if (links[i] == buffer[q->headBufNumber]) {
            // a full buffer points to us
            ProfileEntry tmp = {
                .headBufNumber = i
            };
            if ( i == q->headBufNumber ){
                printf("%d empty!\n", i);
                return;
            }
            printProfileEntry(&tmp); // TODO: check recursion end
            printf(" .... and the rest of %d\n", q->headBufNumber);
            printProfileEntry_tBuffer(buffer[q->headBufNumber], links[q->headBufNumber]);
            break;
        }
    }
    if (i == NB_BUFFERS) {
        if (buffer[q->headBufNumber] < links[q->headBufNumber] &&
            links[q->headBufNumber] < &buffer[q->headBufNumber][NB_ELEMENTS]) {
            printProfileEntry_tBuffer(buffer[q->headBufNumber], links[q->headBufNumber]);
        }
        else {
            printf("full buffer %d\n", q->headBufNumber);
            printProfileEntry_tBuffer(buffer[q->headBufNumber], &buffer[q->headBufNumber][NB_ELEMENTS]);
        }
    }
    else {
        printf("what is here?\n");
        //printProfileEntry_tBuffer(buffer[q->headBufNumber], &buffer[q->headBufNumber][NB_ELEMENTS]);
    }
}

// TODO consumer
// TODO read write lock
void dump() {
    ProfileEntry* q = masterRecord.firstEntry;
    if (!q) return;
    do {
        printf("========== %s =========\n", q->name);
        printProfileEntry(q);
        q = q->next;
    } while (q != masterRecord.firstEntry);
}
