#include <swcFrameTypes.h>
#include <svuCommonShave.h>
#include <stdio.h>
//#include <string.h>
//#include "shaveStart.h"
//#include "RANSAC.h"
#include "FundamentalMatrix.h"
#include "swcCdma.h"
#include "Sections.h"
#define printf(...)
#define EXTERN extern "C"

EXTERN void  start(RANSAC_input *Inputs,RANSAC_output *Outputs);

float  cmxp1[2*MAX_NUMPOINTS];
float  cmxp2[2*MAX_NUMPOINTS];
/*output pre pad marker*/
uint32_t cmxInliersprePad;
/*output data marker*/
int cmxInliers[MAX_NUMPOINTS];
/*output post pad marker*/
uint32_t cmxInlierspostPad;
/*output pre pad marker*/
uint32_t fmprePad;
/*output data marker*/
float  fm[9];
/*output post pad marker*/
uint32_t fmpostPad;
dmaTransactionList_t CMX_DMA_DESCRIPTORS task1, task2;


RANSAC_input RANSAC_Inputs;
RANSAC_output RANSAC_Outputs;


EXTERN int main(void)
{
    printf("SHAVE HERE\n");
    start(&RANSAC_Inputs,&RANSAC_Outputs);
    return 0;
}


EXTERN void start(RANSAC_input *Inputs,RANSAC_output *Outputs)
{
    dmaTransactionList_t *ref1;
    dmaTransactionList_t *ref2;

    u32 id1 = dmaInitRequester(1);
// DMA in the points
    ref1 = dmaCreateTransaction(id1, &task1, (u8*)Inputs->p1, (u8*)&cmxp1[0], 2 * Inputs->Params->nPoints * sizeof(float));
    ref2 = dmaCreateTransaction(id1, &task2, (u8*)Inputs->p2, (u8*)&cmxp2[0], 2 * Inputs->Params->nPoints * sizeof(float));
    printf("S2\n");
    dmaLinkTasks(ref1, 1, ref2);
    printf("S3\n");
    dmaStartListTask(ref1);

    dmaWaitTask(ref1);
// Calculate the fundamental matrix
    findFundamentalMat(&cmxp1[0], &cmxp2[0],Inputs->Params->nPoints,Outputs->fm,&cmxInliers[0],Outputs->Debug,Inputs->Params->inliers_ratio,Inputs->Params->confidence,Inputs->Params->dist_threshold,Inputs->Params->max_iterations);
    printf("S6\n");
    SHAVE_HALT;

// DMA the outputs

}


