include-dirs-shave-y+=$(MDK_INSTALL_DIR_ABS)/components/kernelLib/MvCV/kernels/bilateral5x5/shave/include
srcs-shave-y+= \
  shave/src/cpp/bilateral5x5.cpp
srcs-shave-ma2x5x-y+= \
  arch/ma2x5x/arch/ma2x5x/shave/src/bilateral5x5.asm
srcs-shave-$(CONFIG_TARGET_SOC_MA2150)=$(srcs-shave-ma2x5x-y)
srcs-shave-$(CONFIG_TARGET_SOC_MA2450)=$(srcs-shave-ma2x5x-y)
