
include-dirs-los-y+=\
	$(MDK_INSTALL_DIR_ABS)/components/kernelLib/MvCV/include \
	$(MDK_INSTALL_DIR_ABS)/components/kernelLib/MvISP/include \
	$(MDK_INSTALL_DIR_ABS)/../packages/movidius/pcModel/moviCompile/compilerVectorFunctions/include

include-dirs-lrt-y+=$(include-dirs-los-y)
include-dirs-shave-y+=$(include-dirs-los-y)

subdirs-los-$(CONFIG_USE_COMPONENT_MVCV)+=MvCV
subdirs-lrt-$(CONFIG_USE_COMPONENT_MVCV)+=MvCV
subdirs-shave-$(CONFIG_USE_COMPONENT_MVCV)+=MvCV

subdirs-los-$(CONFIG_USE_COMPONENT_LAMA)+=LAMA
subdirs-lrt-$(CONFIG_USE_COMPONENT_LAMA)+=LAMA
subdirs-shave-$(CONFIG_USE_COMPONENT_LAMA)+=LAMA

subdirs-los-$(CONFIG_USE_COMPONENT_MvISP)+=MvISP
subdirs-lrt-$(CONFIG_USE_COMPONENT_MvISP)+=MvISP
subdirs-shave-$(CONFIG_USE_COMPONENT_MvISP)+=MvISP
