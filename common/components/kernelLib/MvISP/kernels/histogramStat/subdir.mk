include-dirs-shave-y+=$(MDK_INSTALL_DIR_ABS)/components/kernelLib/MvISP/kernels/histogramStat/shave/include
srcs-shave-y+= \
  shave/src/cpp/histogramStat.cpp
srcs-shave-ma2x5x-y+= \
  arch/ma2x5x/arch/ma2x5x/shave/src/histogramStat.asm
srcs-shave-$(CONFIG_TARGET_SOC_MA2150)=$(srcs-shave-ma2x5x-y)
srcs-shave-$(CONFIG_TARGET_SOC_MA2450)=$(srcs-shave-ma2x5x-y)
