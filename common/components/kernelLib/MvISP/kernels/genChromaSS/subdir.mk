include-dirs-shave-y+=$(MDK_INSTALL_DIR_ABS)/components/kernelLib/MvISP/kernels/genChromaSS/shave/include
srcs-shave-y+= \
  shave/src/cpp/genChromaSS.cpp
srcs-shave-ma2x5x-y+= \
  arch/ma2x5x/arch/ma2x5x/shave/src/genChromaSS.asm
srcs-shave-$(CONFIG_TARGET_SOC_MA2150)=$(srcs-shave-ma2x5x-y)
srcs-shave-$(CONFIG_TARGET_SOC_MA2450)=$(srcs-shave-ma2x5x-y)
