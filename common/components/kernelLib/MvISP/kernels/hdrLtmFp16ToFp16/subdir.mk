include-dirs-shave-y+=$(MDK_INSTALL_DIR_ABS)/components/kernelLib/MvISP/kernels/hdrLtmFp16ToFp16/shave/include
srcs-shave-y+= \
  shave/src/cpp/hdrLtmFp16ToFp16.cpp
srcs-shave-ma2x5x-y+= \
  arch/ma2x5x/arch/ma2x5x/shave/src/hdrLtmFp16ToFp16.asm
srcs-shave-$(CONFIG_TARGET_SOC_MA2150)=$(srcs-shave-ma2x5x-y)
srcs-shave-$(CONFIG_TARGET_SOC_MA2450)=$(srcs-shave-ma2x5x-y)
