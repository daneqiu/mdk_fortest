///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved
///            For License Warranty see: common/license.txt
///
/// @brief     H264 Encoding Functions.
///
/// This is the implementation of a simple H264 Encoder library implementing H264 Encoding operations.
///
#include <mv_types.h>
#include "swcShaveLoader.h"
#include "swcLeonUtils.h"
#include "registersMyriad.h"
#include <OsDrvSvu.h>
#include <OsDrvShaveL2Cache.h>
#include <DrvTimer.h>

#include <H4EncoderAPI.h>
#include "Defines.h"

// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------
/* The user can change the partition number according with his cache configuration */
#define PARTITION_NR                      0
#define LeonWriteNoCache32(_addr, _value) swcLeonWriteNoCache32(&(_addr), _value)
#define LeonReadNoCache32(_addr)          swcLeonReadNoCacheU32(&(_addr))

swcShaveUnit_t CMX_DMA  H4shaveList[MAX_ENCODER_SHAVES_USED];

extern DynamicContext_t MODULE_DATA(H264Osal);


H4UInt16 nb_shaves_used = MAX_ENCODER_SHAVES_USED;
H4RetCode_t               CMX_DMA  shave_err_code[MAX_ENCODER_SHAVES_USED];
H4NALUCallback            CMX_DMA  pH4NALUCallback;
H4FrameCallback           CMX_DMA  pH4FrameCallback;
/* Structure used for exchanging information between SHAVE and Leon. */
volatile H4CallbackInfo_t CMX_DMA  H4CBInfo;

// 5:  Component Usage
// --------------------------------------------------------------------------------
//  In order to use the component the following steps are ought to be done:
//  1. User should declare variables for the configuration:
// H4EncoderUserConfig_t   cfg;
//  2. User should configure the input parameters for H264 encoder corresponding to user cfg
// Ex: image_width, image_height, etc
//     See the H4EncoderUserConfig_t description
//  3. User should open the encode library according with the user cfg, by calling H4EncoderOpen function
//     Also, the callback for NALU and Frame will be set here.
//  4. User should start applying the encode algorithm on the frame, by calling H4EncodeFrame function
//     at each input original frame
//  5. User can verify the processing result, by reading the bitstream file from SDcard
//
/// @}
#define SWU_IRR_ALL   0x3F              //all shave interrupts
#define SWU_IRR_SGIIC 0x20              //Software generated interrupt (SWIH/SWIC) interrupt clear
#define ISR_SGI(a)    ((a) & (1 << 12)) //missing definition: software generated interrupt bit in the ISR


#ifdef READSENSE_LOGIC
static int shaveNumOverwrite = -1;
static int shaveIdx[12];
H4Void H2EncodeSetShave(int shaveNum, int shvIdx[12])
{
  shaveNumOverwrite = shaveNum;
  for (int i = 0; i < shaveNum; i++)
  {
    shaveIdx[i] = shvIdx[i];
  }
}
#endif

void H4EncodeDispatchCallbacks(H4CBType_t callback_type)
{
  H4RetCode_t       err = _H4_OK;
  H4InstanceHandle_t h_encoder;
  H4UInt32          first_shave = (H4UInt32)H4shaveList[0];

  h_encoder = H4CBInfo.h_instance;
  if(callback_type == _H4_NALU_CB_TYPE)
  {
    H4NALU_t        *p_nalu;
    H4NALUCBState_t state;

    p_nalu = (H4NALU_t *)swcSolveShaveRelAddr((u32)H4CBInfo.p_nalu, first_shave);
    state  = H4CBInfo.nalu_state;
    if(pH4NALUCallback != NULL)
    {
      err = pH4NALUCallback(h_encoder, p_nalu, state);
    }
    H4CBInfo.error_return  = err;
    H4CBInfo.callback_type = _H4_UNDEF_CB_TYPE;
  }
  else if(callback_type == _H4_FRAME_CB_TYPE)
  {
    H4FrameHandle_t  *hframe;
    H4Frame_t        *p_fd;
    H4FrameCBState_t state;

    hframe = (H4FrameHandle_t *)swcSolveShaveRelAddr((u32)H4CBInfo.hframe, first_shave);
    p_fd   = (H4Frame_t *)swcSolveShaveRelAddr((u32)H4CBInfo.p_fd, first_shave);
    state  = H4CBInfo.frame_state;
    if(pH4FrameCallback != NULL)
    {
      err = pH4FrameCallback(h_encoder, hframe, p_fd, state);
    }
    H4CBInfo.error_return  = err;
    H4CBInfo.callback_type = _H4_UNDEF_CB_TYPE;
  }
  else
  {
    if(H4CBInfo.callback_mechanism == TRUE)
    {
      if(pH4FrameCallback != NULL)
      {
        err = pH4FrameCallback(h_encoder, NULL, NULL, _H4_FRAME_DONE);
        H4CBInfo.error_return = err;
      }
    }
  }
}

void H4EncodeHandler(unsigned int source)
{
  H4CBType_t callback_type;
  H4UInt32   status;
  H4UInt32   firstShave = (H4UInt32)H4shaveList[0];
  H4UInt32   sourceShave;

  sourceShave = source - IRQ_SVE_0;
  status = GET_REG_WORD_VAL(SVU_CTRL_ADDR[sourceShave] + SLC_OFFSET_SVU + SVU_ISR);

  /* Check interruption type */
  /* If other than software generated interrupt returns or can be handled by user! */
  if (!ISR_SGI(status))
  {
    SET_REG_WORD(SVU_CTRL_ADDR[sourceShave] + SLC_OFFSET_SVU + SVU_IRR, SWU_IRR_ALL & ~SWU_IRR_SGIIC);
    return;
  }
  /* This callback is not treated for other SHAVEs */
  if(sourceShave != firstShave)
  {
    return;
  }
  /* Dispatcher for H264Encoder library */
  callback_type = LeonReadNoCache32(H4CBInfo.callback_type);
  H4EncodeDispatchCallbacks(callback_type);
  if(callback_type == _H4_UNDEF_CB_TYPE)
  {
    /* If the IRQ was triggered by SWIH don't start again the SHAVE!!! */
    return;
  }

  SET_REG_WORD(SVU_CTRL_ADDR[firstShave] + SLC_OFFSET_SVU + SVU_IRR, SWU_IRR_SGIIC);

  /* Start shave without affecting other bits in OCR */
  H4UInt32 OCR_val = GET_REG_WORD_VAL(SVU_CTRL_ADDR[firstShave] + SLC_OFFSET_SVU + SVU_OCR);
  SET_REG_WORD(SVU_CTRL_ADDR[firstShave] + SLC_OFFSET_SVU + SVU_OCR, OCR_val & ~OCR_STOP_GO);
}

H4Void H4EncodeOpen(H4InstanceHandle_t    *h_encoder,
                    H4EncoderUserConfig_t *enc_config_params,
                    H4NALUCallback        nalu_cb,
                    H4FrameCallback       frame_cb,
                    H4OSALHandle_t        hOSALHandle,
                    H4RetCode_t           *err_code)
{
  H4UInt32 firstShave;
  u32      running_shaves;
  int      status;

  nb_shaves_used = enc_config_params->nb_encoding_threads;

#ifdef READSENSE_LOGIC
    if (shaveNumOverwrite > 0)
    {
      enc_config_params->nb_encoding_threads = shaveNumOverwrite;
      nb_shaves_used = shaveNumOverwrite;
      printf("Use shave ");
      for (int i = 0; i < nb_shaves_used; i++)
      {
        H4shaveList[i] = shaveIdx[i];
        printf("%d ", shaveIdx[i]);
      }
      printf(". Total %d shaves for H264.\n ", nb_shaves_used);
    }
    else
    {
      status = OsDrvRequestUnallocatedShaves(H4shaveList, nb_shaves_used);
      if (status != OS_MYR_DYN_INFR_SUCCESS)
      {
        *err_code = _H4_FATAL_ERR;
        return;
      }

      printf("Auto allocate shave ");
      for (int i = 0; i < nb_shaves_used; i++)
      {
        printf("%d ", H4shaveList[i]);
      }
      printf(". Total %d shaves for H264.\n ", nb_shaves_used);

    }
#else
  status = OsDrvRequestUnallocatedShaves(H4shaveList, nb_shaves_used);
  if (status != OS_MYR_DYN_INFR_SUCCESS)
  {
    *err_code = _H4_FATAL_ERR;
    return;
  }
#endif

  firstShave = H4shaveList[0];

  pH4NALUCallback  = nalu_cb;
  pH4FrameCallback = frame_cb;
  /* Init sharing data for polling/IRQ mechanism */
  H4CBInfo.callback_type = _H4_UNDEF_CB_TYPE;
  H4CBInfo.callback_mechanism = FALSE;  /* FALSE: polling; TRUE: IRQ handling */

  for(uint32_t shaveIdx = 0; shaveIdx < nb_shaves_used; shaveIdx++)
  {
    int shaveNr = H4shaveList[shaveIdx];
    DrvSvuL1DataCacheCtrl(shaveNr, SVUL1DATACACHE_ENABLE);
    DrvSvuL1DataCacheCtrl(shaveNr, SVUL1DATACACHE_INVALIDATE_ALL);
    /* Set WTHRU bit in SHAVE_CACHE_CTRL register */
    uint32_t regVal = GET_REG_WORD_VAL(SHAVE_CACHE_CTRL(shaveNr));
    SET_REG_WORD(SHAVE_CACHE_CTRL(shaveNr), (regVal | CACHE_CTRL_WTHRU));
  }

  status = OsDrvSvuSetupDynShaveApps(&MODULE_DATA(H264Osal), H4shaveList, nb_shaves_used);
  if (status != OS_MYR_DYN_INFR_SUCCESS)
  {
    *err_code = _H4_FATAL_ERR;
    return;
  }
  status = OsDrvSvuOpenShaves(H4shaveList, nb_shaves_used, OS_MYR_PROTECTION_SEM);
  if (status != OS_MYR_DYN_INFR_SUCCESS)
  {
    *err_code = _H4_FATAL_ERR;
    return;
  }
  printf("H4EncodeOpen3  0x%lx \n", *err_code);
  status = OsDrvSvuRunShaveAlgoOnAssignedShaveCC(&MODULE_DATA(H264Osal), firstShave, "iiiiii", (u32)_H4_OPEN_ENTRY_TYPE, \
          (u32)h_encoder, (u32)enc_config_params, (u32)&H4CBInfo, (u32)hOSALHandle, (u32)err_code);
  if (status != OS_MYR_DYN_INFR_SUCCESS)
  {
    *err_code = _H4_FATAL_ERR;
    return;
  }
  printf("H4EncodeOpen4  0x%lx \n", *err_code);
  // Waiting only for instance 1
  status = OsDrvSvuDynWaitShaves(H4shaveList, 1, OS_DRV_SVU_WAIT_FOREVER, &running_shaves);
  if (status != OS_MYR_DYN_INFR_SUCCESS)
  {
    *err_code = _H4_FATAL_ERR;
    return;
  }
  printf("H4EncodeOpen5  0x%lx \n", *err_code);
  status = OsDrvSvuCloseShaves(H4shaveList, nb_shaves_used);
  if (status != OS_MYR_DYN_INFR_SUCCESS)
  {
    *err_code = _H4_FATAL_ERR;
    return;
  }
  printf("H4EncodeOpen6  0x%lx \n", *err_code);
  OsDrvShaveL2CachePartitionFlush(PARTITION_NR, PERFORM_INVALIDATION);
  printf("H4EncodeOpen10  0x%lx \n", *err_code);
}

H4Void H4EncodeClose(H4InstanceHandle_t h_encoder,
                     H4RetCode_t       *err_code)
{
  H4UInt32 firstShave = H4shaveList[0];
  u32      running_shaves;
  int      driver_return_code;
  int      status;

  /* Init sharing data for polling/IRQ mechanism */
  H4CBInfo.callback_type = _H4_UNDEF_CB_TYPE;

  status=OsDrvSvuOpenShaves(H4shaveList, nb_shaves_used, OS_MYR_PROTECTION_SEM);
  if (status != OS_MYR_DYN_INFR_SUCCESS)
  {
    *err_code = _H4_FATAL_ERR;
    return;
  }

  status=OsDrvSvuRunShaveAlgoOnAssignedShaveCC(&MODULE_DATA(H264Osal), firstShave, "iii", (u32)_H4_CLOSE_ENTRY_TYPE,
                (u32)h_encoder, (u32)err_code);
  if (status != OS_MYR_DYN_INFR_SUCCESS)
  {
    *err_code = _H4_FATAL_ERR;
    return;
  }

  do
  {
    H4CBType_t callback_type = H4CBInfo.callback_type;

    H4EncodeDispatchCallbacks(callback_type);
    driver_return_code = OsDrvSvuDynWaitShaves(H4shaveList, 1, 1, &running_shaves);
    if(OS_MYR_DRV_TIMEOUT != driver_return_code)
    {
      if(OS_MYR_DRV_SUCCESS == driver_return_code)
      {
        break;
      }
      else
      {
        *err_code = _H4_FATAL_ERR;
        return;
      }
    }
  }
  while(1);

  status = OsDrvSvuCloseShaves(H4shaveList, nb_shaves_used);
  if (status != OS_MYR_DYN_INFR_SUCCESS)
  {
    *err_code = _H4_FATAL_ERR;
    return;
  }

  status = OsDrvSvuCleanupDynShaveListApps(&MODULE_DATA(H264Osal), H4shaveList, nb_shaves_used);
  if (status != OS_MYR_DRV_SUCCESS)
  {
    *err_code = _H4_FATAL_ERR;
    return;
  }

  OsDrvShaveL2CachePartitionFlush(PARTITION_NR, PERFORM_INVALIDATION);
}

H4Void H4EncodeFrame(H4InstanceHandle_t h_encoder,
                     H4UInt32          app_frame_dts,
                     H4EncoderStatus_t *enc_ret_status,
                     H4RetCode_t       *err_code)
{
  H4RetCode_t ret_code = 0;
  H4Int32     shaveIdx;
  u32         running_shaves;
  int         driver_return_code;
  int         status;

  /* Init sharing data for polling/IRQ mechanism */
  H4CBInfo.callback_type = _H4_UNDEF_CB_TYPE;

  status = OsDrvSvuOpenShaves(H4shaveList, nb_shaves_used, OS_MYR_PROTECTION_SEM);
  if (status != OS_MYR_DYN_INFR_SUCCESS)
  {
    *err_code = _H4_FATAL_ERR;
    return;
  }

  for(shaveIdx = 0; shaveIdx < nb_shaves_used; shaveIdx++)
  {
    status = OsDrvSvuRunShaveAlgoOnAssignedShaveCC(&MODULE_DATA(H264Osal), H4shaveList[shaveIdx], "iiiii", (u32)_H4_FRAME_ENTRY_TYPE, \
            (u32)h_encoder, (u32)app_frame_dts, (u32)enc_ret_status, (u32)&shave_err_code[shaveIdx]);
    if (status != OS_MYR_DYN_INFR_SUCCESS)
    {
      *err_code = _H4_FATAL_ERR;
      return;
    }
  }

  do
  {
    H4CBType_t callback_type = H4CBInfo.callback_type;

    H4EncodeDispatchCallbacks(callback_type);
    driver_return_code = OsDrvSvuDynWaitShaves(H4shaveList, nb_shaves_used, 1, &running_shaves);
    if(OS_MYR_DRV_TIMEOUT != driver_return_code)
    {
      if(OS_MYR_DRV_SUCCESS == driver_return_code)
      {
        break;
      }
      else
      {
        *err_code = _H4_FATAL_ERR; return;
      }
    }
  }
  while(1);

  for(shaveIdx = 0; shaveIdx < nb_shaves_used; shaveIdx++)
  {
    ret_code |= shave_err_code[shaveIdx];
  }
  *err_code = ret_code;

  status = OsDrvSvuCloseShaves(H4shaveList, nb_shaves_used);
  if (status != OS_MYR_DYN_INFR_SUCCESS)
  {
    *err_code = _H4_FATAL_ERR;
    return;
  }

  OsDrvShaveL2CachePartitionFlush(PARTITION_NR, PERFORM_INVALIDATION);
}

H4Void* H4EncodeGetUserInfo(H4InstanceHandle_t h_encoder)
{
  /* The value is in uncached CMX pool of the H264 library */
  return *((H4Void**)h_encoder);
}
