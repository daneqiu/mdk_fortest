///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved
///            For License Warranty see: common/license.txt
///
/// @brief     H264 Encoder Functions API.
///
/// CodecArt H264 Encoder library API
///
#ifndef _H4_ENCODER_API_H_
#define _H4_ENCODER_API_H_

#include "H4Defines.h"

#ifdef __cplusplus
extern "C" {
#endif

// Opens an H264 encoder instance
H4Void H4EncodeOpen(H4InstanceHandle_t    *h_encoder,
                    H4EncoderUserConfig_t *enc_config_params,
                    H4NALUCallback        nalu_cb,
                    H4FrameCallback       frame_cb,
                    H4OSALHandle_t        hOSALHandle,
                    H4RetCode_t           *err_code);

// Closes the encoder identified by the encoder handle
H4Void H4EncodeClose(H4InstanceHandle_t h_encoder,
                      H4RetCode_t      *err_code);

/// Encode next frame
H4Void H4EncodeFrame(H4InstanceHandle_t h_encoder,
                     H4UInt32          frame_ts,
                     H4EncoderStatus_t *enc_ret_status,
                     H4RetCode_t       *err_code);

H4Void* H4EncodeGetUserInfo(H4InstanceHandle_t h_encoder);

#ifdef READSENSE_LOGIC
H4Void H2EncodeSetShave(int shaveNum, int shaveIdx[12]);
#endif

#ifdef __cplusplus
}
#endif

#endif

