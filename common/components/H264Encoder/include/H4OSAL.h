///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved
///            For License Warranty see: common/license.txt
///
/// @brief     H264 OSAL API.
///
/// The H264 Operating System Abstraction Layer API Definitions.
///
#ifndef	__H4OSAL_H__
#define	__H4OSAL_H__

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#include <string.h>
#include "H4Defines.h"
#include "H4Portab.h"
#include "Defines.h"

/** Memory types */
enum
{
  DDR_MEMORY_TYPE  = 0,
  CMX0_MEMORY_TYPE = 1,
  CMX1_MEMORY_TYPE = 2,
  CMX2_MEMORY_TYPE = 3,
  CMX3_MEMORY_TYPE = 4,
  CMX4_MEMORY_TYPE = 5,
  CMX5_MEMORY_TYPE = 6,
  CMX6_MEMORY_TYPE = 7,
  CMX7_MEMORY_TYPE = 8,
  CMX8_MEMORY_TYPE = 9,
  CMX9_MEMORY_TYPE = 10,
  CMX10_MEMORY_TYPE = 11,
  CMX11_MEMORY_TYPE = 12,
  MEMORY_TYPES_NR = 13
};
typedef H4Int32 H4OSALMemoryType_t;
typedef H4Void* H4OSALSemaphore_t;

/**********************************************************************************/
/*                             DMA functions                                      */
/**********************************************************************************/
H4Void H4OSALDmaInit(H4OSALHandle_t hOSALHandle);

H4Void H4OSALDmaCreateTask(H4OSALHandle_t hOSALHandle,
                           H4UInt32       taskId,
                           H4UInt8        *pSrc,
                           H4UInt8        *pDst,
                           H4UInt32       byteLength,
                           H4UInt32       srcLineWidth,
                           H4UInt32       dstLineWidth,
                           H4Int32        srcStride,
                           H4Int32        dstStride);

H4Void H4OSALDmaLinkTasks(H4OSALHandle_t hOSALHandle,
                          H4UInt32       listHead,
                          H4UInt32       newTransaction);

H4Void H4OSALDmaStartTasks(H4OSALHandle_t hOSALHandle,
                           H4UInt32       listHead);

H4Void H4OSALDmaWaitTasks(H4OSALHandle_t hOSALHandle,
                          H4UInt32       listHead);

/**********************************************************************************/
/*                            Processor functions                                 */
/**********************************************************************************/
H4Int32 H4OSALGetProcessorIndex(H4OSALHandle_t hOSALHandle);

H4Void H4OSALProcessorInit(H4OSALHandle_t hOSALHandle);

H4Void H4OSALProcessorFinish(H4OSALHandle_t hOSALHandle);

/**********************************************************************************/
/*                             Memory management                                  */
/**********************************************************************************/
H4RetCode_t H4OSALMemInit(H4OSALHandle_t hOSALHandle);

H4RetCode_t H4OSALMemDeinit(H4OSALHandle_t hOSALHandle);

H4RetCode_t H4OSALMemAlloc(H4OSALHandle_t     hOSALHandle,
                           H4Void             **pp_mem,
                           H4UInt32           size,
                           H4OSALMemoryType_t memory_type);

H4Void H4OSALMemFree(H4OSALHandle_t hOSALHandle,
                     H4Void *p_mem);

H4Void H4OSALMemCopy(H4Void   *p_dst,
                     H4Void   *p_src,
                     H4UInt32 size);

H4Void H4OSALMemSet(H4Void   *p_dst,
                    H4UInt8  value,
                    H4UInt32 size);

/**********************************************************************************/
/*                           Semaphore Mechanism                                  */
/**********************************************************************************/

H4RetCode_t H4OSALSemaphoreCreate(H4OSALHandle_t    hOSALHandle,
                                  H4OSALSemaphore_t *p_semaphore,
                                  H4Int32           count,
                                  H4Int32           maxcount);

H4RetCode_t H4OSALSemaphoreDestroy(H4OSALSemaphore_t semaphore);

H4RetCode_t H4OSALSemaphorePost(H4OSALSemaphore_t semaphore);

H4RetCode_t H4OSALSemaphoreWait(H4OSALSemaphore_t semaphore); 

#endif /*__H4OSAL_H__*/ 
