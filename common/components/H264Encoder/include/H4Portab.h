///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved
///            For License Warranty see: common/license.txt
///
/// @brief     H264 Encoder Definitions API.
///
/// Platform portability flags and type definitions.
///
#ifndef	__PORTAB_H__
#define	__PORTAB_H__

/***************************************************************************/
/*		constants                                                            */
/***************************************************************************/
#ifndef	NULL
 #ifdef WIN32
  #define NULL  ((void*)0)
 #else
  #define NULL  0L
 #endif
#endif
#ifndef FALSE
 #define FALSE  0
 #define TRUE   !FALSE
#endif

/***************************************************************************/
/*		basic types                                                          */
/***************************************************************************/
typedef void               H4Void;
typedef unsigned char      H4Bool;
typedef signed char        H4Int8;
typedef unsigned char      H4UInt8;
typedef short              H4Int16;
typedef unsigned short     H4UInt16;
typedef long               H4Int32;
typedef unsigned long      H4UInt32;
typedef char               H4Char;
typedef float              H4Float;
#if 1 /* double type is not supported */
typedef float              H4Double;
#else
typedef double             H4Double;
#endif
typedef long long          H4Int64;
typedef unsigned long long H4UInt64;

/* PC - Myriad2 defines */
#if defined WIN32
 #define CNST                   const 
 #define INLINE                 __inline
 #define NO_INLINE
 #define ALWAYS_INLINE
 #define ALIGNED(x)
 #define DDRCACHE_TO_DDR(_addr) (_addr)

/* Sections */
 #define CMX_DMA
 #define CMX_DIRECT_DATA
 #define DDR_DIRECT_DATA
 #define DDR_BSS
 #define FAST_TEXT

#else
 #define CNST                   const 
 #define INLINE                 static
 #define NO_INLINE              __attribute__((noinline))
 #define ALWAYS_INLINE          __attribute__((always_inline))
 #define ALIGNED(x)             __attribute__((aligned(x)))
 #define DDRCACHE_TO_DDR(_addr) (((H4UInt32)(_addr)) | 0x40000000)

 /* Sections */
 #define CMX_DMA                __attribute__((section(".cmx.cdmaDescriptors")))
 #define CMX_DIRECT_DATA        __attribute__((section(".cmx_direct.data")))
 #define DDR_DIRECT_DATA        __attribute__((section(".ddr_direct.data")))
 #define DDR_BSS                __attribute__((section(".ddr.bss")))
 #define FAST_TEXT              __attribute__((section(".textCrit")))
#endif

#endif /*__PORTAB_H__*/
