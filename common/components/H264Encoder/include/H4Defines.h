///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.W
///            For License Warranty see: common/license.txt
///
/// @brief     H264 Encoder Definitions API.
///
/// The Common High Level API Definitions.
///

#ifndef _H4_DEFINES_
#define _H4_DEFINES_

#include "H4Portab.h"


/*------------------------ H264 Error Codes ------------------------------------------*/
/** H264 Error Codes */
#define _H4_INFORMATIVE 0x100
#define _H4_FATAL       0x200

enum
{
  /* Informative return codes */
  _H4_OK                    = _H4_INFORMATIVE + 0,
  _H4_FRAME_NO_DATA_YET     = _H4_INFORMATIVE + 1,
  _H4_FRAME_END_OF_STREAM   = _H4_INFORMATIVE + 2,
  _H4_BITSTREAM_NO_DATA_YET = _H4_INFORMATIVE + 3,
  _H4_FRAME_NOT_FOUND       = _H4_INFORMATIVE + 4,

  /* Fatal error codes */
  _H4_NOT_SUPPORTED         = _H4_FATAL + 1,
  _H4_FATAL_ERR             = _H4_FATAL + 2,
  _H4_ALLOCATION_FAILED     = _H4_FATAL + 3,
  _H4_INVALID_HANDLE        = _H4_FATAL + 4,
  _H4_INVALID_PARAM         = _H4_FATAL + 5,
  _H4_LOCK_FAILED           = _H4_FATAL + 6,
};
typedef H4UInt32 H4RetCode_t;

enum
{
  _H4_YUV420_PLANAR      = 0x00000000, /*!< YUV 4:2:0 planar format */
  _H4_YUV422_PLANAR      = 0x00000001, /*!< YUV 4:2:2 planar format */
  _H4_YUV420_SEMI_PLANAR = 0x00000002, /*!< YUV 4:2:0 semi-planar format */
};
typedef H4UInt32 H4YUVFormat_t;

enum
{
  _H4_RC_NONE = 0x00000000, /*!< Constant quantizer rate control */
  _H4_RC_CBR  = 0x00000001, /*!< CBR rate control (rate-oriented) */	
  _H4_RC_CVBR = 0x00000002  /*!< CVBR rate control (quality-oriented) */	
};
typedef H4UInt32 H4RateControlType_t;

enum
{
  _H4_EC_VLC = 0x00000000, /*!< VLC entropy encoder */
  _H4_EC_AC  = 0x00000001  /*!< AC entropy encoder*/
};
typedef H4UInt32 H4ECType_t;

/*---------------------------- Instance management -----------------------------*/
typedef H4UInt32 H4InstanceHandle_t; 

enum 
{
  _H4_FRAME_COMPLETE   = 0x00000000, /*!< The frame is complete and it has been sent to the Data Management */
  _H4_FRAME_INCOMPLETE = 0x00000001  /*!< The frame is incomplete and the decoder waits for more packets to complete the frame */
};
typedef H4UInt32 H4FrameStatus_t;

enum 
{
  _H4_FRAME_UNDEFINED = 0x00000000, /*!< Indicates an unknown frame */
  _H4_FRAME_I         = 0x00000001, /*!< Indicates an I frame */
  _H4_FRAME_P         = 0x00000002, /*!< Indicates a P frame */
  _H4_FRAME_B         = 0x00000003, /*!< Indicates a B frame */
  _H4_FRAME_IDR       = 0x00000004  /*!< Indicates a random access point */
};
typedef H4UInt32 H4FrameType_t;

enum
{
  _H4_NALU_UNDEFINED = 0x00000000, /*!< Unknown NALU type */
  _H4_NALU_I         = 0x00000001, /*!< NALU from I frame */
  _H4_NALU_P         = 0x00000002, /*!< NALU from P frame */
  _H4_NALU_B         = 0x00000003, /*!< NALU from B frame */
  _H4_NALU_IDR       = 0x00000004, /*!< NALU from IDR frame (random access point) */
  _H4_NALU_SPS       = 0x00000005, /*!< SPS NALU */
  _H4_NALU_PPS       = 0x00000006, /*!< PPS NALU */
  _H4_NALU_AUD       = 0x00000007  /*!< AUD NALU (access unit delimiter) */
};
typedef H4UInt32 H4NaluType_t;

enum
{
  _H4_LEVEL_UNSPECIFIED = 00, /*!< use default library level */
  _H4_LEVEL_1   = 10,
  _H4_LEVEL_1_B = 18,
  _H4_LEVEL_1_1 = 11,
  _H4_LEVEL_1_2 = 12,
  _H4_LEVEL_1_3 = 13,
  _H4_LEVEL_2   = 20,
  _H4_LEVEL_2_1 = 21,
  _H4_LEVEL_2_2 = 22,
  _H4_LEVEL_3   = 30,
  _H4_LEVEL_3_1 = 31,
  _H4_LEVEL_3_2 = 32,
  _H4_LEVEL_4   = 40,
  _H4_LEVEL_4_1 = 41,
  _H4_LEVEL_4_2 = 42,
  _H4_LEVEL_5   = 50,
  _H4_LEVEL_5_1 = 51,
  _H4_LEVEL_5_2 = 52,
};
typedef H4UInt8 H4LevelType_t;

#define _H4_NO_BITRATE_REQUEST    0      /*!< set request_bitrate to this value to keep last set value */
#define _H4_NO_QP_REQUEST        -128      /*!< set request_qp to this value to let rate-control decide the QP of the current frame */

#define _H4_GLOBAL_MOTION_NA      0x7FFF
#define _H4_INTRA_REFRESH_OFF     0

/** Encoder status */
typedef struct
{
  H4UInt32        encoded_picture_count; /*!< number of encoded frames */
  H4FrameStatus_t current_frame_status;  /*!< current frame status */
  H4FrameType_t   current_frame_type;    /*!< current frame type */
  H4FrameType_t   request_frame_type;    /*!< request the coding of this frame type */
  H4UInt32        request_bitrate;       /*!< requests a new bitrate; encoder will set to supported */
  H4Int8          request_qp;            /*!< requests a new frame QP; encoder will set to supported */
  H4UInt16        intra_refresh_period;  /*!< set intra refresh period for current frame */
  H4Int16         global_motion_x;
  H4Int16         global_motion_y;
  H4Float         average_psnr[3];
  H4Float         average_ssim;
} H4EncoderStatus_t;


/*---------------------------- Notification mechanism -----------------------------*/
typedef struct
{
  H4UInt8      *packet;
  H4UInt32     size;
  H4UInt32     cts;
  H4UInt32     dts;
  H4NaluType_t naluType;
  H4Bool       startAU;
  H4Bool       endAU;
} H4NALU_t;

enum
{
  _H4_NALU_GET     = 0x00000000,
  _H4_NALU_RELEASE = 0x00000001
};
typedef H4Int32 H4NALUCBState_t;

enum
{
  _H4_FRAME_GET_ORIG  = 0x00000000,
  _H4_FRAME_GET_EMPTY = 0x00000001,
  _H4_FRAME_READY     = 0x00000002,
  _H4_FRAME_REF_INC   = 0x00000003,
  _H4_FRAME_REF_DEC   = 0x00000004,
  _H4_FRAME_DONE      = 0x00000005
};
typedef H4Int32 H4FrameCBState_t;

typedef H4UInt32 H4FrameHandle_t; 

typedef struct
{
  H4Void        *pPlane[3];
  H4UInt32      width;
  H4UInt32      height;
  H4UInt32      padding;
  H4UInt32      stride_y;
  H4UInt32      stride_uv;
  H4UInt32      cropped_width;
  H4UInt32      cropped_height;
  H4UInt32      cropped_origin_hor;
  H4UInt32      cropped_origin_vert;
  H4FrameType_t frame_type;
  H4YUVFormat_t format;              /* the selected format of the frame */
  H4UInt32      flags;
  H4UInt32      time_stamp;
} H4Frame_t;

enum
{
  _H4_SPEED_S1 = 0x01,
  _H4_SPEED_S2 = 0x02,
  _H4_SPEED_S3 = 0x03
};
typedef H4UInt8 H4SpeedSetting_t;

enum
{
  _H4_MOTION_HPEL = 0x01,
  _H4_MOTION_QPEL = 0x02
};
typedef H4UInt8 H4MotionAccuracy_t;

#define _H4_MIN_QP   0   /*!< minimum QP value supported */
#define _H4_MAX_QP  51   /*!< maximum QP value supported */

#define _H4_P_ONLY  0x0FFFF

/** Encoder library user configuration */
typedef struct 
{
  /* Encoder configuration parameters fields - "user" parameters */
  H4UInt16            image_width;         /*!< image width */
  H4UInt16            image_height;        /*!< image height */
  H4UInt32            no_frames;           /*!< Number of frames to be encoded */
  H4UInt32            I_number;            /*!< Number of I frames between IDR frames */
  H4Int32             P_number;            /*!< Number of P frames between I frames */
  H4ECType_t          ec_type;             /*!< Entropy encoder type */
  H4RateControlType_t rate_control;        /*!< selected rate control mechanism */
  H4UInt32            bitrate;             /*!< indicated bitrate for the stream */
  H4Float             framerate;           /*!< framerate of the stream */
  H4Int16             quant_I;             /*!< Initial/max quantizer for I frames */
  H4Int16             quant_P;             /*!< Initial/max quantizer for P frames */
  H4Int8              dquant_low;          /*!< delta quantizer limits */
  H4Int8              dquant_high;         /*!< delta quantizer limits */
  H4Float             aq_strength;         /*!< AQ strength factor */
  H4YUVFormat_t       input_format;        /*!< input YUV format */
  H4Bool              external_recon;      /*!< externalize reconstructed YUV datapath */
  H4Bool              use_deblock;         /*!< enable in-loop filter */
  H4MotionAccuracy_t  motion_accuracy;     /*!< select motion accuracy */
  H4SpeedSetting_t    speed;               /*!< encoding speed setting */
  H4UInt16            slice_rows;          /*!< number of rows per slice */
  H4UInt16            nb_encoding_threads; /*!< number of parallel threads */
  H4LevelType_t       level_idc;           /*!< level to be encoded in SPS */
  H4Void              *user_info;          /*!< user info associated with the instance */
  H4Bool              perform_ssim;        /*!< enable SSIM computation (only for Win32) */
} H4EncoderUserConfig_t;

typedef H4RetCode_t(*H4NALUCallback)(H4InstanceHandle_t	h_instance,
                                     H4NALU_t           *p_nalu,
                                     H4NALUCBState_t   state);

typedef H4RetCode_t(*H4FrameCallback)(H4InstanceHandle_t h_instance,
                                      H4FrameHandle_t    *h_frame,
                                      H4Frame_t          *p_fd,
                                      H4FrameCBState_t   state);

typedef H4Void* H4OSALHandle_t;

#endif /*_H4_DEFINES_*/
