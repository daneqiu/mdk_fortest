///
/// @file
/// @copyright CodecArt Alliance
///            2015. All rights reserved
///
/// @brief     HW resouce configuration for H264 Encoder.
///
#ifndef _HW_CONFIGURATION_H_
#define _HW_CONFIGURATION_H_

#include "H4Defines.h"
#include "H4OSAL.h"

#define H4OSAL_MAX_ALLOC_TABLE (2*128)

typedef H4Int32* H4OSALMemAddr32;

typedef struct
{
  H4Char   *p_mem_address;
  H4UInt32 mem_curr_size;
  H4UInt32 mem_max_size;
} H4OSALMemPool_t;

typedef struct
{
  H4OSALMemPool_t *memoryInfo;
  H4OSALMemAddr32 memAllocTable[H4OSAL_MAX_ALLOC_TABLE];
  H4UInt32        memAllocMaxIdx;
  H4UInt32        memAllocIdx;
  H4UInt32        memAllocMaxMem;
  H4UInt32        useOneCMXMemBlock;
} H4OSALMemManager_t;

typedef struct
{
  H4OSALMemManager_t *pMemManager;
  H4UInt32           *pShavesList;
  H4UInt32           maxShavesUsed;
  H4UInt32           mutexNr;
  H4UInt32           *pDmaIDList;
  H4Void             *pDmaTransactionList;
} H4HWResourcesCfg_t;

#endif /*_HW_CONFIGURATION_H_*/
