///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved
///            For License Warranty see: common/license.txt
///
/// @brief     Shared definition.
///
#ifndef _SHARED_DEFINES_H_
#define _SHARED_DEFINES_H_

#include "H4Defines.h"
#include "H4Portab.h"

/* Number of SHAVEs used */
#define MAX_ENCODER_SHAVES_USED           12

/* DMA transaction nr for each SHAVE */
#define DMA_TRANSACTIONS_NR_PER_PROCESSOR  3

enum
{
  _H4_OPEN_ENTRY_TYPE  = 0x00000000,
  _H4_FRAME_ENTRY_TYPE = 0x00000001,
  _H4_CLOSE_ENTRY_TYPE = 0x00000002
};
typedef H4Int32 H4EntryType_t;

enum
{
  _H4_NALU_CB_TYPE  = 0x00000000,
  _H4_FRAME_CB_TYPE = 0x00000001,
  _H4_UNDEF_CB_TYPE = 0x00000002
};
typedef H4Int32 H4CBType_t;

typedef struct
{
  H4CBType_t         callback_type;
  H4RetCode_t        error_return;
  H4InstanceHandle_t h_instance;
  H4NALU_t           *p_nalu;
  H4NALUCBState_t    nalu_state;
  H4FrameHandle_t    *hframe;
  H4Frame_t          *p_fd;
  H4FrameCBState_t   frame_state;
  H4Bool             callback_mechanism; /* FALSE: polling; TRUE: IRQ handling */
} H4CallbackInfo_t;

#endif /*_SHARED_DEFINES_H_*/
