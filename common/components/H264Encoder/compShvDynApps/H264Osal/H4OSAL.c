///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved
///            For License Warranty see: common/license.txt
///
/// @brief     H264 OSAL API.
///
/// The H264 Operating System Abstraction Layer API Definitions.
///
#include <stdlib.h>
#include <stdio.h>
#include "H4OSAL.h"
#include "HWResources.h"
#include "swcCdma.h"
#include "svuCommonShave.h"


#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

//#define H4_TRACE /* Used to show the memory usages per each SHAVE */
//#define H4_DEBUG /* Used to initialize the entire memory data pool with a specific value */

#define H4_HEADER_SIZE   8
#define MEMORY_ALIGNMENT 16
#define INIT_BYTE        0xCD

#define USE_DMA_TRANSFER

typedef struct
{
  H4Int32 mutexNr;
  volatile H4Int32 count;
} svuSemaphore_t;

/**********************************************************************************/
/*                             DMA functions                                      */
/**********************************************************************************/
H4Void H4OSALDmaInit(H4OSALHandle_t hOSALHandle)
{
  H4HWResourcesCfg_t *p_OSALInfo = ((H4HWResourcesCfg_t*)hOSALHandle);

#ifdef USE_DMA_TRANSFER
  H4Int32  processor_index = H4OSALGetProcessorIndex(hOSALHandle);

  p_OSALInfo->pDmaIDList[processor_index] = dmaInitRequester(3);
#endif
}

H4Void H4OSALDmaCreateTask(H4OSALHandle_t hOSALHandle,
                           H4UInt32       taskId,
                           H4UInt8        *pSrc,
                           H4UInt8        *pDst,
                           H4UInt32       byteLength,
                           H4UInt32       srcLineWidth,
                           H4UInt32       dstLineWidth,
                           H4Int32        srcStride,
                           H4Int32        dstStride)
{
  H4HWResourcesCfg_t *p_OSALInfo = ((H4HWResourcesCfg_t*)hOSALHandle);
  H4Int32            processor_index;

#ifdef USE_DMA_TRANSFER
  processor_index = H4OSALGetProcessorIndex(hOSALHandle);

  dmaCreateTransactionFullOptions(p_OSALInfo->pDmaIDList[processor_index],
                                  &((dmaTransactionList*)p_OSALInfo->pDmaTransactionList)[taskId],
                                  pSrc, pDst, byteLength, srcLineWidth, dstLineWidth, srcStride, dstStride);
#else
  (H4Void)processor_index;
  do
  {
    H4OSALMemCopy(pDst, pSrc, srcLineWidth);
    pSrc += srcStride;
    pDst += dstStride;
    byteLength -= srcLineWidth;
  } while ((H4Int32)byteLength > 0);
#endif
}

H4Void H4OSALDmaLinkTasks(H4OSALHandle_t hOSALHandle,
                          H4UInt32       listHead,
                          H4UInt32       newTransaction)
{
  H4HWResourcesCfg_t *p_OSALInfo = ((H4HWResourcesCfg_t*)hOSALHandle);

#ifdef USE_DMA_TRANSFER
  dmaLinkTasks(&((dmaTransactionList*)p_OSALInfo->pDmaTransactionList)[listHead], 1, &((dmaTransactionList*)p_OSALInfo->pDmaTransactionList)[newTransaction]);
#endif
}

H4Void H4OSALDmaStartTasks(H4OSALHandle_t hOSALHandle,
                           H4UInt32       listHead)
{
  H4HWResourcesCfg_t *p_OSALInfo = ((H4HWResourcesCfg_t *)hOSALHandle);

#ifdef USE_DMA_TRANSFER
  dmaStartListTask(&((dmaTransactionList*)p_OSALInfo->pDmaTransactionList)[listHead]);
#endif
}

H4Void H4OSALDmaWaitTasks(H4OSALHandle_t hOSALHandle,
                          H4UInt32       listHead)
{
  H4HWResourcesCfg_t *p_OSALInfo = ((H4HWResourcesCfg_t*)hOSALHandle);

#ifdef USE_DMA_TRANSFER
  dmaWaitTask(&((dmaTransactionList*)p_OSALInfo->pDmaTransactionList)[listHead]);
#endif
}

/**********************************************************************************/
/*                            Processor functions                                 */
/**********************************************************************************/
H4Int32 H4OSALGetProcessorIndex(H4OSALHandle_t hOSALHandle)
{
  H4UInt32 *p_list = ((H4HWResourcesCfg_t*)hOSALHandle)->pShavesList;
  H4Int32  shave_number = -1;
  H4Int32  i;

  shave_number = scGetShaveNumber();
  for(i = 0; i < (H4Int32)((H4HWResourcesCfg_t*)hOSALHandle)->maxShavesUsed; i++)
  {
    if ((H4Int32)p_list[i] == shave_number)
    {
      return i;
    }
  }
  return -1;
}

H4Void H4OSALProcessorInit(H4OSALHandle_t hOSALHandle)
{
  /* Unused parameters for the moment. */
  (void)hOSALHandle;
}

H4Void H4OSALProcessorFinish(H4OSALHandle_t hOSALHandle)
{
  /* Unused parameters for the moment. */
  (void)hOSALHandle;
}

/**********************************************************************************/
/*                             Memory management                                  */
/**********************************************************************************/

H4RetCode_t H4OSALMemInit(H4OSALHandle_t hOSALHandle)
{
  H4OSALMemManager_t *p_H4MemoryInfo = ((H4HWResourcesCfg_t *)hOSALHandle)->pMemManager;

  H4OSALMemSet((H4Void*)p_H4MemoryInfo->memAllocTable, 0, H4OSAL_MAX_ALLOC_TABLE*sizeof(H4OSALMemAddr32));

  p_H4MemoryInfo->memAllocMaxIdx  = 0;
  p_H4MemoryInfo->memAllocMaxMem  = 0;
  p_H4MemoryInfo->memAllocIdx     = 0;

#ifdef H4_DEBUG
  for(H4UInt32 i = 0; i < MEMORY_TYPES_NR; i++)
  {
    p_H4MemoryInfo->memoryInfo[i].mem_curr_size = 0;
    if(p_H4MemoryInfo->memoryInfo[i].mem_max_size > 0)
    {
      H4OSALMemSet(p_H4MemoryInfo->memoryInfo[i].p_mem_address, INIT_BYTE, p_H4MemoryInfo->memoryInfo[i].mem_max_size);
    }
  }
#endif

  return _H4_OK;
}

H4RetCode_t H4OSALMemDeinit(H4OSALHandle_t hOSALHandle)
{
  H4UInt32 i;
  H4OSALMemManager_t *p_H4MemoryInfo = ((H4HWResourcesCfg_t *)hOSALHandle)->pMemManager;

#ifdef H4_TRACE
  printf("Max allocated memory:  %lu\n", p_H4MemoryInfo->memAllocMaxMem);
  printf("Max allocated entries: %lu\n", p_H4MemoryInfo->memAllocMaxIdx);
#endif
  for (i = 0; i < p_H4MemoryInfo->memAllocMaxIdx; i++)
  {
    if (p_H4MemoryInfo->memAllocTable[i] != NULL)
    {
      H4OSALMemFree(hOSALHandle, p_H4MemoryInfo->memAllocTable[i]);
    }
  }

#ifdef H4_TRACE
  for(i = 0; i < MEMORY_TYPES_NR; i++)
  {
    if(p_H4MemoryInfo->memoryInfo[i].mem_curr_size > 0)
    {
      printf("Total memory type %lu! %lu\n", i, p_H4MemoryInfo->memoryInfo[i].mem_curr_size);
    }
  }
  printf("Leaked memory: %lu\n", p_H4MemoryInfo->memAllocMaxMem);
#endif
  return p_H4MemoryInfo->memAllocMaxMem == 0 ? _H4_OK : _H4_ALLOCATION_FAILED;
}

/**
 ***************************************************************
 * @brief        Call the appropriate memories allocation function
 ***************************************************************
*/
H4RetCode_t H4OSALMemAlloc(H4OSALHandle_t     hOSALHandle,
                           H4Void             **pp_mem,
                           H4UInt32           size,
                           H4OSALMemoryType_t memory_type)
{
  H4OSALMemManager_t *p_H4MemoryInfo = ((H4HWResourcesCfg_t *)hOSALHandle)->pMemManager;
  H4UInt32           curr_size, rest, i;
  H4OSALMemAddr32    p_mem_alloc;
  H4OSALMemAddr32    p_mem;
  H4OSALMemAddr32    p_mem_mod;

  if(p_H4MemoryInfo->useOneCMXMemBlock)
  {
    if(memory_type > DDR_MEMORY_TYPE)
      memory_type = CMX0_MEMORY_TYPE;
  }
  *pp_mem = NULL;

#ifdef H4_TRACE
  printf("OSAL: Alloc %d from pool %d\n", size, memory_type);
#endif

  for(i = 0; i < H4OSAL_MAX_ALLOC_TABLE; i++)
  {
    if (p_H4MemoryInfo->memAllocTable[i] == NULL)
      break;
  }
  if(i == H4OSAL_MAX_ALLOC_TABLE)
  {
    return _H4_ALLOCATION_FAILED;
  }

  if(memory_type > MEMORY_TYPES_NR - 1)
  {
    return _H4_ALLOCATION_FAILED;
  }
  curr_size = p_H4MemoryInfo->memoryInfo[memory_type].mem_curr_size;
  p_mem_alloc = (H4OSALMemAddr32)&(p_H4MemoryInfo->memoryInfo[memory_type].p_mem_address[curr_size]);
  curr_size += size + MEMORY_ALIGNMENT + H4_HEADER_SIZE;
  if(curr_size >= p_H4MemoryInfo->memoryInfo[memory_type].mem_max_size)
  {
#ifdef H4_TRACE
    printf("Size exceeded for memory type %lu! %lu\n", memory_type, curr_size);
#endif
    return _H4_ALLOCATION_FAILED;
  }
  p_H4MemoryInfo->memoryInfo[memory_type].mem_curr_size = curr_size;

  if(p_mem_alloc != NULL)
  {
    if(p_H4MemoryInfo->memAllocIdx >= H4OSAL_MAX_ALLOC_TABLE)
    {
#ifdef H4_TRACE
      printf("Err allocation index! %lu\n", p_H4MemoryInfo->memAllocIdx);
#endif
      return _H4_ALLOCATION_FAILED;
    }

    rest = ((H4UInt32)p_mem_alloc + H4_HEADER_SIZE)&(MEMORY_ALIGNMENT-1);
    p_mem = (H4OSALMemAddr32)((H4UInt8*)(p_mem_alloc) + H4_HEADER_SIZE + MEMORY_ALIGNMENT - rest);
    /* Computes a pointer aligned to MEMORY_ALIGNMENT bytes */
    p_mem_mod = (H4OSALMemAddr32)((H4UInt32)p_mem & (~(MEMORY_ALIGNMENT - 1)));
    ((H4OSALMemAddr32)p_mem_mod)[-1] = (H4Int32)p_mem_alloc;
#ifdef H4_TRACE
    ((H4OSALMemAddr32)p_mem_mod)[-2] = size + MEMORY_ALIGNMENT + H4_HEADER_SIZE;
    p_H4MemoryInfo->memAllocMaxMem  += size + MEMORY_ALIGNMENT + H4_HEADER_SIZE;
    ((H4OSALMemAddr32)p_mem_mod)[-3] = p_H4MemoryInfo->memAllocIdx;
#endif
    p_H4MemoryInfo->memAllocTable[p_H4MemoryInfo->memAllocIdx] = p_mem;
    *pp_mem = p_mem;

    p_H4MemoryInfo->memAllocIdx++;

    if(p_H4MemoryInfo->memAllocIdx > p_H4MemoryInfo->memAllocMaxIdx)
    {
      p_H4MemoryInfo->memAllocMaxIdx = p_H4MemoryInfo->memAllocIdx;
    }

    return _H4_OK;
  }

  return _H4_ALLOCATION_FAILED;
}

/**
 ***************************************************************
 * @brief        Call the appropriate memories deallocation functions
 ***************************************************************
*/
H4Void H4OSALMemFree(H4OSALHandle_t hOSALHandle,
                     H4Void         *p_mem)
{
  H4OSALMemManager_t *p_H4MemoryInfo = ((H4HWResourcesCfg_t *)hOSALHandle)->pMemManager;
  H4OSALMemAddr32    p_mem_mod;
  H4OSALMemAddr32    p_mem_alloc;
  H4Int32            i;

  if(p_mem != NULL)
  {
    p_mem_mod = (H4OSALMemAddr32)((H4UInt32)p_mem&0xFFFFFFFC);
    p_mem_alloc = (H4OSALMemAddr32)p_mem_mod[-1];

    for (i = 0; i < H4OSAL_MAX_ALLOC_TABLE; i++)
    {
      if (p_H4MemoryInfo->memAllocTable[i] == p_mem)
      {
#ifdef H4_TRACE
        p_H4MemoryInfo->memAllocMaxMem -= ((H4OSALMemAddr32)p_mem_mod)[-2];
#endif
        p_H4MemoryInfo->memAllocTable[i] = 0;
      }
    }
  }
}

/**
 ***************************************************************
 * @brief        Call the appropriate memory copy function.
 ***************************************************************
*/
H4Void H4OSALMemCopy(H4Void   *p_dst,
                     H4Void   *p_src,
                     H4UInt32 size)
{
  memcpy(p_dst, p_src, size);
}

/**
 ***************************************************************
 * @brief        Call the appropriate memory set function.
 ***************************************************************
*/
H4Void H4OSALMemSet(H4Void   *p_dst,
                    H4UInt8  value,
                    H4UInt32 size)
{
  memset(p_dst, value, size);
}

/**********************************************************************************/
/*                           Semaphore Mechanism                                  */
/**********************************************************************************/

/**
 ***************************************************************
 * @brief        Create a semaphore with specified initial count.
 ***************************************************************
*/
H4RetCode_t H4OSALSemaphoreCreate(H4OSALHandle_t    hOSALHandle,
                                  H4OSALSemaphore_t *p_semaphore,
                                  H4Int32           count,
                                  H4Int32           maxcount)
{
  H4HWResourcesCfg_t *p_OSALInfo = ((H4HWResourcesCfg_t*)hOSALHandle);
  H4RetCode_t        err_code = _H4_OK;
  svuSemaphore_t *semaphore;
  H4Int32        processor_index = H4OSALGetProcessorIndex(hOSALHandle);

  /* Unused parameters for the moment. */
  (void)count;
  (void)maxcount;
  err_code = H4OSALMemAlloc(hOSALHandle, (H4Void **)&semaphore, sizeof(svuSemaphore_t), CMX0_MEMORY_TYPE + processor_index);
  if(err_code == _H4_OK)
  {
    (*p_semaphore) = (H4OSALSemaphore_t)semaphore;
    semaphore->count = 0;
    semaphore->mutexNr = p_OSALInfo->mutexNr;
  }

  return _H4_OK;
}

/**
 ***************************************************************
 * @brief        Deletes a previously created semaphore.
 ***************************************************************
*/
H4RetCode_t H4OSALSemaphoreDestroy(H4OSALSemaphore_t semaphore)
{
  /* Unused parameters for the moment. */
  (void)semaphore;
  return _H4_OK;
}

/**
 ***************************************************************
 * @brief        Attempts to decrement the semaphore count.
 ***************************************************************
*/
H4RetCode_t H4OSALSemaphoreWait(H4OSALSemaphore_t semaphore)
{
  svuSemaphore_t *sem = (svuSemaphore_t*)semaphore;

  while(1)
  {
    scMutexRequest(sem->mutexNr);
    if(sem->count == 0)
    {
      scMutexRelease(sem->mutexNr);
      continue;
    }
    sem->count--;
    scMutexRelease(sem->mutexNr);
    break;
  }

  return _H4_OK;
}

/**
 ***************************************************************
 * @brief        Attempts to increment the semaphore count.
 ***************************************************************
*/
H4RetCode_t H4OSALSemaphorePost(H4OSALSemaphore_t semaphore)
{
  svuSemaphore_t *sem = (svuSemaphore_t*)semaphore;
  scMutexRequest(sem->mutexNr);
  sem->count++;
  scMutexRelease(sem->mutexNr);
  return _H4_OK;
}

#ifdef __cplusplus
}
#endif // __cplusplus
