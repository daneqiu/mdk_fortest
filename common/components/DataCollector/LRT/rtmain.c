///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///


// 1: Includes
// ----------------------------------------------------------------------------
#include <rtems/stackchk.h>
#include <stdio.h>
#include <DrvLeon.h>

#include <DrvCpr.h>
#include <DrvTimer.h>

#include <unistd.h>

#include "aeThread.h"


int *POSIX_Init(void)
{
    DrvCprInit();
    DrvCprInitClocks(12000, 0, 0, 0, 0); // TODO: remove
    DrvTimerInit();

    aeThreadInit();

    DrvLeonRTSignalBootCompleted();

    while(1)
    {
        //rtems_stack_checker_report_usage();
        sleep(10);
    }

    return 0;
}
