#ifndef DATACOLLECTOR_H__
#define DATACOLLECTOR_H__


#ifndef MAX_CAMERA_COUNT
#define MAX_CAMERA_COUNT 2
#endif
#ifndef MAX_PROCESS_THREADS
#define MAX_PROCESS_THREADS 3
#endif
#include "plgFlicMIG.h"

typedef struct dataCollectorContext
{
    char *camName[MAX_CAMERA_COUNT];
    int camCnt;
    int processingthreadCnt;
    int camFrameRate;
    int latencyControl;
    int outputFrameDisable; // if set to 1, the output frame will be the same as input frame (no conversion)
    plgFlicMIG* processingPlugin[MAX_PROCESS_THREADS];

}dataCollectorContext;

#ifdef __cplusplus
extern "C"
#endif
void* DataCollectorStart(dataCollectorContext* ctx);

#endif
