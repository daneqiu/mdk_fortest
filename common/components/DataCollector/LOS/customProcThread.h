#ifndef CUSTOMPROCTHREAD_H__
#define CUSTOMPROCTHREAD_H__
#include "commStructs.h"

#ifdef __cplusplus
extern "C"
{
#endif
void* customProcessInit(void);
void* customProcessFrame(processMessage_t* message);

#ifdef __cplusplus
}
#endif


#endif
