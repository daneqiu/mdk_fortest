#ifndef OUTSTREAM_H__
#define OUTSTREAM_H__
#include <messages.h>

#include <Flic.h>
#include <Sender.h>
#include <Receiver.h>
#include <BehQueueOvr.h>


#include <imu.h> //TODO: we don't want dependencies. This may need to go to an IMU buffering node
#include <XLink.h>

typedef enum outStreamType_t
{
    FRAME_BUFF,
    META,
    IMU,
}outStreamType_t;

typedef struct outStreamContext_t
{
    int unused;
}outStreamContext_t;

class PlgOutStream : public ThreadedPlugin{
  public:
    int camCnt;
    int lowLatencyEnable;

    //TODO: in future we just need to add variable number of channels here, we don't want dependencies on specific types
   SReceiver<ImgFramePtr> inFrame;
   SReceiver<ImgFramePtr> inMeta;
   SReceiver<ImgFramePtr> inIMU;
   void Create();
   void *threadFunc(void *);
  private:
   void  outStreamSendOverUsb(void *data, outStreamType_t type);
   void outStreamUSBFullWrite(void* buff, u32 cnt);

   imuFrame imuPacket;
   streamId_t usbLinkStream;
   XLinkHandler_t handler;
//   OvrOldCmd ovrBeh;

};



#endif
