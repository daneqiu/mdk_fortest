#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>

#include <OsDrvTimer.h>
//TODO: get rid of FramePump dependency
#include <FramePump.h>

#include "AecNode.h"

#define MVLOG_UNIT_NAME aecNode
#include <mvLog.h>

#include <aeApi.h>

frameBuffer aeFrame;

typedef struct aeExposure {
    unsigned integrationTime;
    float gainCode;
} aeExposure_t;

void *PlgAec::threadFunc(void *){
    mvLogLevelSet(MVLOG_DEBUG);
    aecThreadFunc(&aecCtx);
    return NULL;
}

void * PlgAec::aecThreadFunc(aecContext * context)
{
    u32 camCnt = context->camCnt;
    u32 i;
    bool initialized[2] = {false};

    simpleAEpostInit();

    while(1)
    {
        int sc;
        ImgFramePtr msg[camCnt];
        for(i = 0; i < camCnt; i++)
        {
            sc = frameIn.Receive(&msg[i]);
            if (sc != 0)
            {
                mvLog(MVLOG_WARN,"AEC TIMEOUT. No frame received\n");
                continue;
            }

            if(!initialized[i])
            {
                initialized[i] = true;
                simpleAEstart(msg[0].ptr->instNo, i, "cam");
            }

            FramePumpBuffer fpbIn;
            frameBuffer fb;
            ImgFrame* frm = (ImgFrame*) msg[i].ptr;
            fb = frm->fb;
            fpbIn.fb = fb;
            fpbIn.cam_id = i;

            mvLog(MVLOG_INFO, "Received %d", frm->fb.spec.bytesPP);

            simpleAEprocessFrame(i, &fpbIn);
        }

        aeMessage_t received = simpleAEget();

        mvLog(MVLOG_INFO, "Received AE value");

        FramePumpCamExpGain camExpGainPair;

        camExpGainPair.exposureTimeUs = received.integrationTimeUs;

        for (i = 0; i < camCnt; i++)
        {
            camExpGainPair.gainMultiplier = received.gainCode[i];

            sc = ioctl(msg[i].ptr->instNo, FRAME_PUMP_CONFIG_SEND_EXPGAIN, &camExpGainPair);
            if(sc)
            {
                mvLog(MVLOG_ERROR, "failed to set exposure and gain !!!");
            }
        }
    }


    return NULL;
}

int aecInit(void)
{
    mvLogLevelSet(MVLOG_INFO);
    int status;

    status = simpleAEinit();

    return status;
}
