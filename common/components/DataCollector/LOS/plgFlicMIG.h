#ifndef CUSTOMPROCNODE_H__
#define CUSTOMPROCNODE_H__


#include <Flic.h>
#include <Sender.h>
#include <Receiver.h>
#include <ImgFrame.h>
#include <messages.h>
#include <BehQueueOvr.h>
#include <commStructs.h>
#include <customProcThread.h>

typedef struct customProcCtx
{
    u32 camCnt;

    // will produce only one output frame for every frameRateDivider input frames
    u32 frameRateDivider;
    // will produce frame only if frameCount % frameRateDivider == dropCondition
    u32 dropCondition;
}customProcCtx;



class plgFlicMIG : public ThreadedPlugin{
  public:
    int outputFrameDisable;
    customProcCtx ctx;
    MSender<ImgFramePtr> frameLeftOut;
    MSender<ImgFramePtr> frameRightOut;
    MSender<ImgFramePtr> metaLeft;
    MSender<ImgFramePtr> metaRight;

    SReceiver<ImgFramePtr> frameLeft;
    SReceiver<ImgFramePtr> frameRight;
    SReceiver<RotMatMsg> rotMatIn;

    MReceiver<ImgFramePtr> emptyFrame;
    MReceiver<ImgFramePtr> emptyMeta;
    void Create();
    void* threadFunc(void *);
    void* customProcLoop(customProcCtx * context);
  private:
    virtual void* pluginSpecificInit(void){
        customProcessInit();
        return NULL;
    }
    virtual void* pluginSpecificProcess(processMessage_t* message){
        customProcessFrame(message);
        return NULL;
    }

};



#endif
