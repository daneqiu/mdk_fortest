///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///


// 1: Includes
// ----------------------------------------------------------------------------
#include "DataCollector.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <rtems.h>
#include <rtems/status-checks.h>
#include <rtems/libio.h>
#include <sys/stat.h>        /* For mode constants */
#include <mqueue.h>
#include <fcntl.h>
#include <OsDrvCpr.h>
#include <OsDrvTimer.h>
#include <DrvLeon.h>


#include "imu.h"
#include <AecNode.h>

#include <assert.h>

#include "CameraNode.h"
#include "outStreamNode.h"
#include "CameraSync.h"
#include "rotationMatrixPlg.h"

#include <FramePump.h>


#include <Flic.h>
#include <Sender.h>
#include <Receiver.h>
#include <ImgFrame.h>
//#include <PlgPool.h>
#include <MemAllocator.h>
#include "plgFlicMIG.h"



#define MVLOG_UNIT_NAME dataCollector
#include <mvLog.h>
// 2:  Source Specific #defines and types  (typedef, enum, struct)
#define BUFF_COUNT_IN 6
#define BUFF_COUNT_OUT 6
// -----------------------------------------------------------------------------
// 3: Global Data (Only if absolutely necessary)
// -----------------------------------------------------------------------------
// 4: Static Local Data
// -----------------------------------------------------------------------------

PlgPool<ImgFrame>      plgPoolSrc[MAX_CAMERA_COUNT]     SECTION(".cmx_direct.data");
PlgPool<ImgFrame>      plgPoolOut     SECTION(".cmx_direct.data");
PlgPool<ImgFrame>      plgPoolMeta    SECTION(".cmx_direct.data");
PlgPool<ImgFrame>      plgPoolIMU    SECTION(".cmx_direct.data");
PlgFramePumpCam  plgCam[MAX_CAMERA_COUNT];
PlgOutStream  plgB;
PlgAec plgAec;
PlgCamSync plgSync;
plgFlicMIG plgCustomProc[MAX_PROCESS_THREADS];
PlgImu plgImu;
PlgRotationMat plgRot;
Pipeline p;
customProcCtx cpctx[MAX_PROCESS_THREADS];

int lrt_ae_camera_count;

pthread_barrier_t barrier;
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------

int startLLRT(void)
{
    DrvLeonRTStartupStart();
    DrvLeonRTWaitForBoot();
    return 0;
}

void* DataCollectorStart(dataCollectorContext* ctx)
{
    mvLogLevelSet(MVLOG_INFO);

    mvLog(MVLOG_INFO, "GONE WITH THE WIND !!!!!!!!!!!!!!\n");

    RgnAlloc.Create(RgnBuff, DEF_POOL_SZ);

    pthread_barrier_init(&barrier, NULL, ctx->camCnt);

    plgB.camCnt = ctx->camCnt;
    plgB.lowLatencyEnable = ctx->latencyControl;
    plgB.Create();
    p.Add(&plgB);

    plgCam[0].isMaster = 1;

    plgSync.camCtx.camCnt = ctx->camCnt;
    plgSync.Create();
    p.Add(&plgSync);

    lrt_ae_camera_count = ctx->camCnt;
    plgAec.aecCtx.camCnt = ctx->camCnt;
    plgAec.Create();
    p.Add(&plgAec);

    startLLRT();

    int i;
    plgPoolMeta.Create(&RgnAlloc, BUFF_COUNT_OUT, sizeof(frameMeta));
    plgPoolOut.Create(&RgnAlloc, BUFF_COUNT_OUT, 640*480*2);
    p.Add(&plgPoolMeta);
    p.Add(&plgPoolOut);



    for(i = 0; i < ctx->processingthreadCnt; i++){
        if (ctx->processingPlugin[i] == NULL){
            plgCustomProc[i].outputFrameDisable = ctx->outputFrameDisable;
            plgCustomProc[i].ctx.camCnt = ctx->camCnt;
            plgCustomProc[i].ctx.frameRateDivider = ctx->processingthreadCnt;
            plgCustomProc[i].ctx.dropCondition = i;
            plgCustomProc[i].Create();
            ctx->processingPlugin[i] = &plgCustomProc[i];
        }
    }

    for(i = 0; i < ctx->processingthreadCnt; i++){

        p.Add(ctx->processingPlugin[i]);
        plgPoolMeta.out.Link(&ctx->processingPlugin[i]->emptyMeta);
        plgPoolOut.out.Link(&ctx->processingPlugin[i]->emptyFrame);

        plgSync.out[0].Link(&ctx->processingPlugin[i]->frameLeft);
        ctx->processingPlugin[i]->frameLeftOut.Link(&plgB.inFrame);
        ctx->processingPlugin[i]->metaLeft.Link(&plgB.inMeta);

        if (ctx->camCnt > 1){
            plgSync.out[1].Link(&ctx->processingPlugin[i]->frameRight);
            ctx->processingPlugin[i]->frameRightOut.Link(&plgB.inFrame);
            ctx->processingPlugin[i]->metaRight.Link(&plgB.inMeta);
        }
    }
    for(i = 0; i < ctx->camCnt; i++){
        plgPoolSrc[i].Create(&RgnAlloc, BUFF_COUNT_IN, 640*480*2); //RAW
        p.Add(&plgPoolSrc[i]);
        //camera Nodes
        plgCam[i].camCtx.camName = ctx->camName[i];
        plgCam[i].camCtx.camFrameRate = ctx->camFrameRate;
        plgCam[i].camCtx.camCnt = ctx->camCnt;

        plgCam[i].camCtx.barrier = &barrier;
        plgCam[i].Create();
        p.Add(&plgCam[i]);
        //creating links between camera nodes, sync, and AEC
        plgCam[i].out.Link(&plgSync.in[i]);
        plgPoolSrc[i].out.Link(&plgCam[i].emptyIn);
        plgSync.out[i].Link(&plgAec.frameIn);
    }

//
//    //IMU specific plugins and connections
    plgImu.Create();
    plgPoolIMU.Create(&RgnAlloc, 100, sizeof(mvImuData_t));
    p.Add(&plgPoolIMU);
    p.Add(&plgImu);
    plgPoolIMU.out.Link(&plgImu.emptyIn);
    plgImu.out.Link(&plgB.inIMU);
    plgRot.Create();
    p.Add(&plgRot);
    plgImu.out.Link(&plgRot.inIMU);
    plgSync.out[0].Link(&plgRot.timeStampIn);
    for(i = 0; i < ctx->processingthreadCnt; i++){
        plgRot.out.Link(&ctx->processingPlugin[i]->rotMatIn);
    }
    p.Start();
    p.Wait();
    mvLog(MVLOG_INFO, "GONE WITH THE WIND !!!!!!!!!!!!!!\n");

    while(1)
        sleep(1);
    return NULL;
}
