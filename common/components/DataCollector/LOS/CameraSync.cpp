#include "CameraSync.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#define MVLOG_UNIT_NAME cameraSync
#include <mvLog.h>


void* PlgCamSync::threadFunc(void* context){
    UNUSED(context);
    mvLogLevelSet(MVLOG_DEBUG);
    vgaThreadFunc((cameraSyncContext*) &camCtx);
    return NULL;
}


void * PlgCamSync::vgaThreadFunc(cameraSyncContext * context)
{
    int i;
    cameraSyncContext *tc = (cameraSyncContext*)context;
    UNUSED(tc);
    while(1)
    {
        for (i = 0; i < camCtx.camCnt; i++)
        {
            in[i].Receive(&currentFrames[i]);
        }
        //TODO: do proper checks here. For now just verify the frameCount.
        // This node must ensure that the frames are synchronized and drop the ones which are breaking synchronization
        if (camCtx.camCnt > 1){
            assert(currentFrames[0].ptr->seqNo == currentFrames[1].ptr->seqNo);
        }
        for (i = 0; i < camCtx.camCnt; i++){
            out[i].Send(&currentFrames[i]);
//            currentFrames[i].Release();
        }
    }
    return NULL;
}

