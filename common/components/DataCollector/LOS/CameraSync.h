#ifndef CAMERA_SYNC_H__
#define CAMERA_SYNC_H__


#include <pthread.h>
#include <Flic.h>
#include <Sender.h>
#include <Receiver.h>
#include <ImgFrame.h>
#include <messages.h>

typedef struct cameraSyncContext
{
    int camCnt;
}cameraSyncContext;


class PlgCamSync : public ThreadedPlugin{
  public:
    int isMaster;
    cameraSyncContext camCtx;

    MSender<ImgFramePtr> out[MAX_CAMERA_COUNT];
    SReceiver<ImgFramePtr> in[MAX_CAMERA_COUNT];
    int cnt;
    void Create(){
        assert(camCtx.camCnt <= MAX_CAMERA_COUNT);
        int i;
        char outName[6] = {'.','o','u','t','0', 0};
        char inName[6] = {'.','i','n','0',0, 0};
        for(i = 0; i < camCtx.camCnt; i++){
            outName[4] = '0' + i;
            Add(&out[i], outName);
            inName[3] = '0' + i;
            in[i].Create(1);

            Add(&in[i], inName);
        }
    }
    void *threadFunc(void *);
    void *vgaThreadFunc(cameraSyncContext * context);
  private:
    ImgFramePtr currentFrames[MAX_CAMERA_COUNT];

};

#endif
