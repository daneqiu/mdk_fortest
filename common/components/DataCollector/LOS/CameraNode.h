#ifndef CAMERA_NODE_H__
#define CAMERA_NODE_H__

#include <pthread.h>

typedef struct cameraNodeContext
{
    char *camName;
    int camFrameRate;
    int camCnt;
    pthread_barrier_t *barrier;
}cameraNodeContext;

#include <Flic.h>
#include <Sender.h>
#include <Receiver.h>
#include <ImgFrame.h>

class PlgFramePumpCam : public ThreadedPlugin{
  public:
    int isMaster;
    cameraNodeContext camCtx;
    MSender<ImgFramePtr> out;
    MReceiver<ImgFramePtr> emptyIn;
    uint32_t cnt;
    void Create(){
        previousCnt = 0;
        writeIdx = 0;
        readIdx = 0;
        Add(&out, ".out");
        Add(&emptyIn, ".emptyIn");
    }
    void *threadFunc(void *);
    void *vgaThreadFunc(cameraNodeContext * context);
  private:
    static const int MAX_MESSAGES= 12;
    ImgFramePtr frameMsgList[MAX_MESSAGES];
    uint32_t writeIdx;
    uint32_t readIdx;
    uint32_t previousCnt;
};

#endif
