#include "plgFlicMIG.h"

#include <pthread.h>
#include <string.h>
#include <mvMacros.h>

#include "swcFrameTypes.h"
#include "OsDrvTimer.h"

#include <LeonIPCApi.h>
#include <DrvLeonL2C.h>

#include <DrvIcbDefines.h>

#include "commStructs.h"
#define MVLOG_UNIT_NAME customProcNode
#include <mvLog.h>

#define NUMBER_OF_SHAVE_THREADS 2

#define CMX_NOCACHE __attribute__((section(".cmx_direct.data")))

void plgFlicMIG::Create(){
    assert(ctx.camCnt <= MAX_CAMERA_COUNT);
    Add(&frameLeftOut, ".frameLeftOut");
    Add(&frameRightOut, ".frameRightOut");
    Add(&metaLeft, ".metaLeft");
    Add(&metaRight, ".metaRight");
    frameLeft.Create(1, QueueCmdOvr<ImgFramePtr>::Inst());
    frameRight.Create(1, QueueCmdOvr<ImgFramePtr>::Inst());
    rotMatIn.Create(1, QueueCmdOvr<RotMatMsg>::Inst());
//    frameLeft.Create(1);
//    frameRight.Create(1);
//    rotMatIn.Create(1);
    Add(&frameLeft, ".frameLeft");
    Add(&frameRight, ".frameRight");
    Add(&rotMatIn, ".rotMatIn");
    Add(&emptyFrame, ".emptyFrame");
    Add(&emptyMeta, ".emptyMeta");
    pluginSpecificInit();
}

void * plgFlicMIG::customProcLoop(customProcCtx * ctx)
{
    customProcCtx* context = (customProcCtx*) ctx;
    int camCnt = context->camCnt;
    int cam;
    UNUSED(ctx);
    processMessage_t message;
    message.noFrames = camCnt;
    while(1)
    {
        int skip = 0;
        //TODO: lot of code duplication here. Please refactor it....
        ImgFramePtr frames[2];
        ImgFramePtr frameOut[2];
        ImgFramePtr meta[2];
        RotMatMsg rotMat;
        int sc;
        sc = frameLeft.Receive(&frames[0]);
        if(camCnt > 1){
            sc += frameRight.Receive(&frames[1]);
        }
        assert(camCnt <= 2); // we support max two cameras to ease integration with MIG plugins
        assert(camCnt > 0); // we support max two cameras to ease integration with MIG plugins

        if (sc != 0)
        {
            mvLog(MVLOG_WARN, "Processing timeout. No frames to process\n");
            continue;
        }
        rotMatIn.Receive(&rotMat);
        mvLog(MVLOG_DEBUG, "Latency in customProc1\n");

        while(camCnt--)
        {
            if(outputFrameDisable == 0){
                emptyFrame.Receive(&frameOut[camCnt]);
            }
            emptyMeta.Receive(&meta[camCnt]);
        }
        mvLog(MVLOG_DEBUG, "Latency in customProc2\n");

        camCnt = context->camCnt;

        for(cam=0; cam < camCnt; cam++)
        {
            if ( frames[cam].ptr->seqNo % context->frameRateDivider != context->dropCondition)
            {
//                frames[cam].Release();
//                frameOut[cam].Release();
//                meta[cam].Release();
                skip = 1;
                continue;
            }
            struct FramePumpBuffer* fpb;
            fpb = &message.inFrames[cam];
            ImgFrame* frm = (ImgFrame*) frames[cam].ptr;
            fpb->cam_id = frm->instNo;
            fpb->frameCount = frm->seqNo;
            fpb->timestampNs = frm->ts;
            fpb->fb = frm->fb;
            if(outputFrameDisable == 0){
                message.outFrames[cam] = message.inFrames[cam];
                frameOut[cam].ptr->fb.p1 = (u8*)frameOut[cam].ptr->base;
                message.outFrames[cam].fb.p1 = (u8*) frameOut[cam].ptr->fb.p1;
            }else{
                message.outFrames[cam].fb.p1 = NULL;
            }
            meta[cam].ptr->fb.p1 = (u8*)meta[cam].ptr->base;
            message.metaBuffers[cam] = ((frameMeta*)meta[cam].ptr->fb.p1);
            message.metaSizes[cam] = meta[cam].ptr->size;
        }
        if (skip){
//            rotMat.Release();
            continue;
        }
        u64 ts;
        DrvTimerGetSystemTicks64(&ts);
        ts = 1000 * ts / (DrvCprGetSysClockKhz() / 1000);
        mvLog(MVLOG_DEBUG, "Latency in customProc %llu %x\n", ts - message.inFrames[0].timestampNs, pthread_self());
        message.rotMat[0] = rotMat.rotationMatrix[0];
        message.rotMat[1] = rotMat.rotationMatrix[1];
        message.rotMat[2] = rotMat.rotationMatrix[2];

        while(rotMat.timeStamp < (uint64_t)frames[0].ptr->ts){
            rotMatIn.Receive(&rotMat);
            message.rotMat[0] += rotMat.rotationMatrix[0];
            message.rotMat[1] += rotMat.rotationMatrix[1];
            message.rotMat[2] += rotMat.rotationMatrix[2];
            mvLog(MVLOG_WARN, "Rotation matrix ts is smaller than frame ts. Getting new rot. mat\n");

        }
        //printf("!!!%lld %lld\n", rotMat.timeStamp, (uint64_t)frames[0].ptr->ts);
        if(rotMat.timeStamp != (uint64_t)frames[0].ptr->ts){
            message.rotMat[0] = 0;
            message.rotMat[1] = 0;
            message.rotMat[2] = 0;
            mvLog(MVLOG_WARN, "Rotation matrix can't be calculated due to frame drop\n");
        }

//        rotMat.Release();

        pluginSpecificProcess(&message);
        DrvTimerGetSystemTicks64(&ts);
        ts = 1000 * ts / (DrvCprGetSysClockKhz() / 1000);
        mvLog(MVLOG_DEBUG, "Latency after customProc %llu\n %x", ts - message.inFrames[0].timestampNs, pthread_self());
        for(cam = 0; cam < camCnt; cam++)
        {
            u8* inPtr = message.inFrames[cam].fb.p1;
            frameSpec inSpec = message.inFrames[cam].fb.spec;
            assert( *((u32*)(inPtr)) == *((u32*)(inPtr + inSpec.height * inSpec.stride - sizeof(u32))) );
            message.metaBuffers[cam]->genericMeta.frameCount = message.inFrames[cam].frameCount;
            message.metaBuffers[cam]->genericMeta.timestampNs = message.inFrames[cam].timestampNs;
            message.metaBuffers[cam]->customMetaSize = message.metaSizes[cam];
            if(outputFrameDisable == 0){
                frameSpec outSpec = message.outFrames[cam].fb.spec;
                u8* outPtr = message.outFrames[cam].fb.p1;
                // propagate tearing markers
                *((u32*)outPtr) = *((u32*)(inPtr));
                *((u32*)(outPtr + outSpec.height * outSpec.stride - sizeof(u32))) =
                              *((u32*)(inPtr + inSpec.height * inSpec.stride - sizeof(u32)));

                ImgFrame* frm = (ImgFrame*) frameOut[cam].ptr;
                struct FramePumpBuffer* fpb = &message.outFrames[cam];
                frm->instNo = fpb->cam_id;
                frm->seqNo = fpb->frameCount;
                frm->ts = fpb->timestampNs;
                frm->fb.spec.height = fpb->fb.spec.height;
                frm->fb.spec.width = fpb->fb.spec.width;
                frm->fb.spec.stride = fpb->fb.spec.stride;
            }
        }
        if(outputFrameDisable){
            frameLeftOut.Send(&frames[0]);
        }else{
            frameLeftOut.Send(&frameOut[0]);
        }
        metaLeft.Send(&meta[0]);
//        frameOut[0].Release();
//        meta[0].Release();
        if (camCnt > 1){
            if(outputFrameDisable){
                frameRightOut.Send(&frames[1]);
            }else{
                frameRightOut.Send(&frameOut[1]);
            }
            metaRight.Send(&meta[1]);
//            frameOut[1].Release();
//            meta[1].Release();
        }
    }

}

void* plgFlicMIG::threadFunc(void *context)
{
    UNUSED(context);
    mvLogLevelSet(MVLOG_INFO);
    customProcLoop((customProcCtx*)&ctx);
    return 0;
}
