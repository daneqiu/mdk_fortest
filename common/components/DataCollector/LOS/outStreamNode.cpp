
#include "outStreamNode.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <superFrame.h>
#include <OsDrvTimer.h>
#include <OsDrvCpr.h>
#include "XLink.h"
#define MVLOG_UNIT_NAME outStream
#include <mvLog.h>
const char * X_LINK_STREAM_NAME ="DCStream";
//#define DISABLE_USB_SEND

void PlgOutStream::Create(){
    mvLogLevelSet(MVLOG_INFO);
    if (lowLatencyEnable){//low latency config
        //TODO: this is not correct for stereo frames. Multiple nodes writing stereo pairs here will lead to message mixup
        inFrame.Create(lowLatencyEnable, QueueCmdOvr<ImgFramePtr>::Inst());
        inMeta.Create(lowLatencyEnable, QueueCmdOvr<ImgFramePtr>::Inst());
    }else{// no drop politics
        inFrame.Create(10);
        inMeta.Create(10);
    }

    inIMU.Create(128);

    Add(&inFrame, ".inFrame");
    Add(&inMeta, ".inMeta");
    Add(&inIMU, ".inIMU");
#ifndef DISABLE_USB_SEND
    XLinkInitialize(&handler);
    usbLinkStream = XLinkOpenStream((char *)X_LINK_STREAM_NAME, (camCnt * 2 + 1) *640*480);
#endif
}

int outStreamInit(void)
{
    return 0;
}

char inputImageData[4096];


void PlgOutStream::outStreamUSBFullWrite(void* buff, u32 cnt)
{
#ifndef DISABLE_USB_SEND
    // send the actual data
    if (buff)
    {
        XLinkWriteData(usbLinkStream, (const uint8_t*) buff, cnt); //ugly const cast
    }
#else
usleep(10000);
#endif
}

void  PlgOutStream::outStreamSendOverUsb(void *data, outStreamType_t type)
{
    switch (type)
    {
    case FRAME_BUFF: {
        ImgFrame* frm = (ImgFrame* )data;
        int full_size = frm->fb.spec.height * frm->fb.spec.stride;
        outStreamUSBFullWrite(&frm->fb.spec.width,  sizeof(uint32_t));
        outStreamUSBFullWrite(&frm->fb.spec.height, sizeof(uint32_t));
        outStreamUSBFullWrite(frm->fb.p1, full_size);
    }break;
    case META: {
        frameMeta* meta = (frameMeta*)data;
        int full_size = FRAME_GENERIC_META_SIZE + meta->customMetaSize;
        outStreamUSBFullWrite(&full_size, sizeof(uint32_t));
        outStreamUSBFullWrite(meta, full_size);
    }break;
    case IMU: {
        imuFrame* imu = (imuFrame*)data;
        int full_size = imu->count * sizeof(mvImuData_t) + sizeof(uint32_t);
        outStreamUSBFullWrite(&full_size, sizeof(uint32_t));
        outStreamUSBFullWrite(data, full_size);
    }break;
    default:
        assert(0);
    }
}

void* PlgOutStream::threadFunc(void *ctx)
{
    UNUSED(ctx);
    while(1)
    {
        ImgFramePtr msg;
        ImgFramePtr msgMeta;
        int i;
        for(i = 0; i < camCnt; i++){
            inFrame.Receive(&msg);
            inMeta.Receive(&msgMeta);
            u64 ts;
            DrvTimerGetSystemTicks64(&ts);
            ts = 1000 * ts / (DrvCprGetSysClockKhz() / 1000);
            ImgFrame* frm = (ImgFrame*) msg.ptr;
            assert(((u32*)frm->fb.p1)[0] == ((u32*)(((u8*)frm->fb.p1) + frm->fb.spec.stride * frm->fb.spec.height - 4))[0]);
            outStreamSendOverUsb(frm, FRAME_BUFF);
//            msg.Release();

            frameMeta* meta =  (frameMeta*)msgMeta.ptr->fb.p1;
            meta->genericMeta.latencyNs = ts - meta->genericMeta.timestampNs;
            mvLog(MVLOG_DEBUG, "Latency in outStream %lu\n", meta->genericMeta.latencyNs);
            outStreamSendOverUsb(meta, META);
            imuPacket.count = 0;
//            msgMeta.Release();
        }
        while( inIMU.TryReceive(&msg) == 0){
            memcpy(imuPacket.data, msg.ptr->fb.p1, sizeof(mvImuData_t));
            imuPacket.count++;
//            msg.Release();
        }
        outStreamSendOverUsb(&imuPacket, IMU);

    }
    return NULL;
}




