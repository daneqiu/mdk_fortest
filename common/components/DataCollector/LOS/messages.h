#ifndef MESSAGES_H__
#define MESSAGES_H__

#ifndef MAX_CAMERA_COUNT
#define MAX_CAMERA_COUNT 2
#endif
#include <stdint.h>
class RotMatMsg /*: public IMessage*/{
  public:
    float rotationMatrix[3];
    uint64_t timeStamp;
//    TYPE_IDENT(RotMatMsg);
};

#endif
