#ifndef COMMSTRUCTS_H
#define COMMSTRUCTS_H

#include <swcFrameTypes.h>
#define MAX_FRAMES_COUNT 2
#include <superFrame.h>
#include <FramePump.h> // TODO: remove FramePump dependency

typedef struct processMessage_t
{
    FramePumpBuffer inFrames[MAX_FRAMES_COUNT]; // frames sent for processing
    FramePumpBuffer outFrames[MAX_FRAMES_COUNT]; // response buffers. LRT should set the frameSpec of these
    frameMeta* metaBuffers[MAX_FRAMES_COUNT]; // The buffers for metadata
    u32 metaSizes[MAX_FRAMES_COUNT]; // the maximum size of metadata
    u32 noFrames; // the number of frames sent. This should be 1 (monocular) or 2 (stereo)
    float rotMat[3];
}processMessage_t;
#endif
