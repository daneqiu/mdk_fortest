ifeq ($(DATACOLLECT_MK_FIRST_PART),)
DATACOLLECT_MK_FIRST_PART:=1

ifeq (,$(MV_SOC_REV))
$(info ">> INFO << MV_SOC_REV not defined in aplication Makefile, defaulting to ma2150 !!!")
MV_SOC_REV ?= ma2150
endif

MV_SOC_OS =     rtems
MV_SOC_OS_LRT = rtems

RTEMS_BUILD_NAME =b-prebuilt
LEON_RT_BUILD = yes
LEON_RT_LIB_NAME = $(DirAppOutput)/leonRTApp

ComponentList_LOS += flic/core flic/types

ComponentList_LOS += SimpleAE
ComponentList_LRT += SimpleAE

USE_USB_VSC?=yes
ifeq ($(USE_USB_VSC),yes)
LEON_APP_URC_SOURCES +=  $(wildcard $(MV_COMMON_BASE)/components/USB_VSC/*leon/*.urc)
CCOPT+= -D'USE_USB_VSC'
ComponentList_LOS+=USB_VSC
MV_USB_PROTOS = protovsc2

else
LEON_APP_URC_SOURCES +=  $(wildcard $(MV_COMMON_BASE)/components/XLink/*leon/*.urc)
MV_USB_PROTOS = protowmc

endif
SIPP_AEC_SLICE_NUMBER ?= 4
CUSTOM_AE_TARGET_BRIGHTNESS ?= 75

ComponentList += LeonIPC FramePump PipePrint
ComponentList_LOS +=XLink DataFlowFramework

#Choosing if this project has shave components or not
SHAVE_COMPONENTS = yes

#########################################################################################
# Include SIPP specifics
MV_SIPP_OS_ORIG = yes
include $(MV_COMMON_BASE)/components/sipp/arch/$(MV_SOC_REV)/build/myriad2/sippMyriad2Elf.mk
CCOPT_LRT    += -U'SIPP_CMX_POOL_SZ'
CCOPT_LRT    += -D'SIPP_CMX_POOL_SZ=84000'
CCOPT_LRT    += -D'SIPP_CDMA_AGENT_NO=0'
CCOPT_LRT    += -D'DATA_COLL_NO_USB'
CCOPT_LRT    += -DSIPP_NO_PRINTF
CCOPT_LRT    += -DAE_TARGET_BRIGHTNESS=$(CUSTOM_AE_TARGET_BRIGHTNESS)
CCOPT_LRT    += -DCVRT_SLICE=$(SIPP_AEC_SLICE_NUMBER)
CCOPT_LRT    += -D'CMX_DATA=__attribute__((section(".cmx.data")))'
CCOPT_LRT    += -DSIPP_DISABLE_INTERRUPT_SETUP
#Use asm code
MVCCOPT_LRT  += -D'SIPP_USE_MVCV'

svuSippImg = $(DirAppOutput)/svuSippImg
RAWDATAOBJECTFILES += $(svuSippImg)_sym.o

PROJECTCLEAN += $(SippSvuObj)

# ------------------------------[ Components used ]--------------------------------#
DATA_COLLECTOR_PATH = $(MV_COMMON_BASE)/components/DataCollector

#-----------------------------------[ dogFreak Components ]--------------------------#
LEON_COMPONENT_C_SOURCES_LRT   += $(wildcard $(DATA_COLLECTOR_PATH)/LRT/*.c)
LEON_COMPONENT_CPP_SOURCES_LRT += $(wildcard $(DATA_COLLECTOR_PATH)/LRT/*.cpp)
LEON_HEADERS_LRT += $(wildcard $(DATA_COLLECTOR_PATH)/LRT/*.h)
LEON_HEADERS_LRT += $(wildcard $(DATA_COLLECTOR_PATH)/shared/*.h)
LEON_HEADERS_LRT += $(wildcard $(DATA_COLLECTOR_PATH)/superFrame/shared/*.h)

LEON_COMPONENT_C_SOURCES_LOS   += $(wildcard $(DATA_COLLECTOR_PATH)/LOS/*.c)
LEON_COMPONENT_CPP_SOURCES_LOS += $(wildcard $(DATA_COLLECTOR_PATH)/LOS/*.cpp)
LEON_HEADERS += $(wildcard $(DATA_COLLECTOR_PATH)/LOS/*.h)
LEON_HEADERS += $(wildcard $(DATA_COLLECTOR_PATH)/shared/*.h)
LEON_HEADERS += $(wildcard $(DATA_COLLECTOR_PATH)/superFrame/shared/*.h)

CCOPT += -D'DEF_POOL_SZ = 38*1024*1024'
CCOPT += -D'MAX_ELEM_PER_BUFF=128'
CCOPT += -D'MAX_FRAMES=128'
CCOPT += -D'CONFIG_TEARING_CHECK' #framePump enable tearing markers
else

#No local applications build rules for this example
$(svuSippImg).mvlib : $(SippSvuObj)
	@mkdir -p $(dir $@)
	$(ECHO) $(LD) $(MVLIBOPT) $(SippSvuObj) $(CompilerANSILibs) -o $@
$(svuSippImg)_sym.o : $(svuSippImg).shvdcomplete
	$(ECHO) $(OBJCOPY) --prefix-symbols=lrt_SS_ --extract-symbol $< $@
$(DirAppObjBase)./leon_rt/appMemMap_lrt.o : $(svuSippImg).shvdlib
endif
