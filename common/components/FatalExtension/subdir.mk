
fatal-extension-srcs=\
	leon/rtems/fatalExtension.c

srcs-los-$(CONFIG_SOC_LOS_MODE_RTEMS)=$(fatal-extension-srcs)
srcs-lrt-$(CONFIG_SOC_LRT_MODE_RTEMS)=$(fatal-extension-srcs)

include-dirs-los-$(CONFIG_SOC_LOS_MODE_RTEMS)+=$(MDK_INSTALL_DIR_ABS)/components/FatalExtension/leon/rtems
include-dirs-lrt-$(CONFIG_SOC_LRT_MODE_RTEMS)+=$(MDK_INSTALL_DIR_ABS)/components/FatalExtension/leon/rtems

