#ifndef FRAMEPUMPMIPI_H__
#define FRAMEPUMPMIPI_H__

#include "DrvMipi.h"
#include "FramePumpPrivate.h"

eDrvMipiInitFuncRet InitMIPI(eDrvMipiCtrlNo controllerNb,
                             const struct FramePumpSensorDriver *cameraSpec);

#endif
