/*
 * This is a component to manage memory for dynamic allocation
 *
 * @File      memManagerApi.c
 * @Author    Cristian Garjitzky
 * @Brief     Movidius Memory Manager
 * @copyright All code copyright Movidius Ltd 2017, all rights reserved
 *            For License Warranty see: common/license.txt
 */

#include <stdio.h>
#include <string.h>
#include "mv_types.h"
#include "mvMacros.h"
#include "DrvMutex.h"

#include "memManagerApi.h"

#define MVLOG_UNIT_NAME memmanager
#include "mvLog.h"

typedef enum { LEFT = 0, RIGHT = 1} shiftDirection;

////////////////////////////////////////////////////////////////////////////
//////////////////////////////////INTERNAL//////////////////////////////////
////////////////////////////////////////////////////////////////////////////
static void shiftArray(MemArea *area, unsigned int index, shiftDirection direction)
{
    int remIdx, incr, startPoint, endPoint;

    switch(direction)
    {
        case LEFT:
            startPoint = index;
            endPoint = area->nmbOfAllocs ;
            incr = +1;
            for (remIdx = startPoint; remIdx < endPoint+1; remIdx+=incr)
            {
                area->allocators[remIdx] =
                    area->allocators[remIdx+incr];
            } break;

        case RIGHT:
        default:
            startPoint = area->nmbOfAllocs+1;
            endPoint = index;
            incr = -1;
            for (remIdx = startPoint; remIdx > endPoint+1; remIdx+=incr)
            {
                area->allocators[remIdx] =
                    area->allocators[remIdx+incr];
            } break;
    }
}

static void freeSpaceMerge(MemArea *area, unsigned int index)
{
    /*Merge two adjacent free allocations into a single one
            (called after every allocation freeing)*/
    if(index >= area->nmbOfAllocs ||
        area->allocators[index].state != FREE)
        return;                         /*Sanity checks*/

    if (area->allocators[index-1].state == FREE)
    {
        area->allocators[index-1].size +=
            area->allocators[index].size;
        area->nmbOfAllocs -= 1;
        shiftArray(area, index, LEFT);
        freeSpaceMerge(area, index);    /*in case of 3 free-allocations merge scenario*/
    }
    else if (area->allocators[index+1].state == FREE)
    {
        area->allocators[index].size +=
            area->allocators[index+1].size;
        area->nmbOfAllocs -= 1;
        shiftArray(area, index+1, LEFT);
    }
}

static unsigned int isUsingDDR(void)
{
    return (__ddrSpaceEnd - __ddrSpaceStart);
}

static void addAllocator(MemArea *area, unsigned int allocIndex,
                           void* address, size_t size, allocState state)
{
    if(size > 0)
    {  // only add allocator if it's size is grater then 0
        area->allocators[allocIndex].address   = address;
        area->allocators[allocIndex].size      = size;
        area->allocators[allocIndex].state     = state;

        if (state == USED)
            area->usage += size;

        area->nmbOfAllocs++;
    }
}

static void removeAllocator(MemArea *area, unsigned int allocIndex)
{
    area->allocators[allocIndex].state = FREE;
    area->usage -= area->allocators[allocIndex].size;
    freeSpaceMerge(area, allocIndex);
}

////////////////////////////////////////////////////////////////////////////
/////////////////////////////////EXTERNAL///////////////////////////////////
////////////////////////////////////////////////////////////////////////////
void* MemMgrAlloc(size_t size, MemMgrAreas area)
{
    unsigned int leftoverSpace;
    void* AllocAddr     = NULL;
    MemArea *localArea  = &__MemManager.areas[area];
    Allocator *localAlloc;
    MemMgrErrorCode error = NO_ERROR;

    DrvFastMutexLock(MEM_MGR_MUTEX);

    if (size <= 0)
    {
        error = MEMALLOC_INVALID_SIZE;
        mvLog(MVLOG_ERROR, "Error %d: MemArea-%d allocating size of (%d) is invalid!",
                error, area, size);
        goto exit;
    }

    /*Calculate size based on allocation-rate*/
    size = ALIGN_UP(size, localArea->allocRate);

    if (size > (localArea->size - localArea->usage))
    {
        error = MEMALLOC_SIZE_EXCEEDED;
        mvLog(MVLOG_ERROR, "Error %d: MemArea-%d size(%d) exceeded available space(%d)!",
               error, area, size,
               localArea->size - localArea->usage);
        goto exit;
    }

    for (unsigned int allocation = 0;
        allocation < localArea->nmbOfAllocs;
        allocation++)
    {
        localAlloc = &localArea->allocators[allocation];
        if (localAlloc->state == FREE && localAlloc->size >= size)
        {
            leftoverSpace = localAlloc->size - size;

            if (leftoverSpace > 0)
            {
                /*Shift allocators to the right*/
                shiftArray(localArea, allocation, RIGHT);

                /*Add new allocation with leftover space*/
                addAllocator(localArea,
                            allocation+1,
                            localAlloc->address + size,
                            leftoverSpace,
                            FREE);
            }
            addAllocator(localArea,
                        allocation,
                        localAlloc->address,
                        size,
                        USED);

            localArea->nmbOfAllocs--;

            AllocAddr = localAlloc->address;
            mvLog(MVLOG_DEBUG, "Alocated %p size %d in area %d error %d\n",
                    AllocAddr, size, area, error);
            goto exit;
        }
    }
    exit:
        DrvFastMutexUnlock(MEM_MGR_MUTEX);
        return AllocAddr;
}

MemMgrErrorCode MemMgrFree(void *AllocAddr)
{
    MemMgrErrorCode error = NO_ERROR;
    MemArea *localArea;
    Allocator *localAlloc;

    DrvFastMutexLock(MEM_MGR_MUTEX);

    for(MemMgrAreas area = CMX_AREA0; area < MEM_TOTAL_COUNT; area++)
    {
        localArea  = &__MemManager.areas[area];
        for(unsigned int allocation = 0;
            allocation < localArea->nmbOfAllocs;
            allocation++)
        {
            localAlloc = &localArea->allocators[allocation];
            if(AllocAddr == localAlloc->address && localAlloc->state == USED)
            {
                mvLog(MVLOG_DEBUG,"Matched %p for deletion! Size of %d in Area %d",
                        AllocAddr,
                        localAlloc->size,
                        area);

                removeAllocator(localArea, allocation);
                goto exit;
            }
        }
    }
    /*Invalid address if not found*/
    error = MEM_INVALID_ADDRESS;
    mvLog(MVLOG_ERROR, "Error %d: Address %p is invalid ",
            error, AllocAddr);
    exit:
        DrvFastMutexUnlock(MEM_MGR_MUTEX);
        return error;
}

MemMgrErrorCode MemMgrBufferExport(size_t bufSize, char *csvBuffer, size_t *csvLenght)
{
    const char* allocStates[] = {"FREE","USED","STATIC"};
    unsigned int nmbOfAreas;
    MemArea *localArea;
    Allocator *localAlloc;
    MemMgrErrorCode error = NO_ERROR;

    DrvFastMutexLock(MEM_MGR_MUTEX);

    *csvLenght = 0;
    *csvLenght += snprintf(csvBuffer, bufSize, "%s", "MemArea,StartAddress,Size,State");

    nmbOfAreas = isUsingDDR() ? MEM_TOTAL_COUNT : CMX_TOTAL_COUNT;

    mvLog(MVLOG_DEBUG, "--EXPORT--");
    for(MemMgrAreas area = CMX_AREA0; area < nmbOfAreas; area++)
    {
        localArea  = &__MemManager.areas[area];
        mvLog(MVLOG_DEBUG, "MemArea: %d",
            area);
        mvLog(MVLOG_DEBUG, "Addr: %p",
            localArea->address);
        mvLog(MVLOG_DEBUG, "AlocRate: %d",
            localArea->allocRate);
        mvLog(MVLOG_DEBUG, "Nmb Allocs: %d",
            localArea->nmbOfAllocs);
        mvLog(MVLOG_DEBUG, "Size: %d",
            localArea->size);

        for(unsigned int allocation = 0;
            allocation < localArea->nmbOfAllocs;
            allocation++)
        {
            localAlloc = &localArea->allocators[allocation];
            mvLog(MVLOG_DEBUG, "\tAllocAddress:%p",
                localAlloc->address);
            mvLog(MVLOG_DEBUG, "\tAllocSize:%d",
                localAlloc->size);
            mvLog(MVLOG_DEBUG, "\t%s",
                allocStates[localAlloc->state]);

            *csvLenght += snprintf(csvBuffer + *csvLenght, bufSize,
                                "\n%d,%p,%d,%d",
                                area,
                                localAlloc->address,
                                localAlloc->size,
                                localAlloc->state);
        }
    }
    if (*csvLenght >= bufSize)
    {
        error = MEM_EXPORT_ALLOCMEM;
        mvLog(MVLOG_ERROR, "Error %d: Allocate more memory for export buffer !",
            error);
    }
    DrvFastMutexUnlock(MEM_MGR_MUTEX);
    return error;
}

void MemMgrExportDraw(void)
{
    MemArea *localArea;
    Allocator *localAlloc;
    double percentage = 0;
    int nmbOfBlocks = 0;
    unsigned int nmbOfAreas, sliceSize, totalNmbOfBlocks = 0;
    const void* plotColors[] =
                    { ANSI_COLOR_GREEN, ANSI_COLOR_RED, ANSI_COLOR_MAGENTA};
    const char MEM_DRAW_STR[64] =
                    "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||";

    DrvFastMutexLock(MEM_MGR_MUTEX);

    nmbOfAreas = isUsingDDR() ? MEM_TOTAL_COUNT : CMX_TOTAL_COUNT;

    for(MemMgrAreas area = CMX_AREA0; area < nmbOfAreas; area++)
    {
        localArea  = &__MemManager.areas[area];
        totalNmbOfBlocks = 0;
        printf(ANSI_COLOR_RESET "MemArea%d-%s[",area, (area == DDR_AREA)?"DDR":"CMX");

        for(unsigned int allocation = 0;
            allocation < localArea->nmbOfAllocs;
            allocation++)
        {
            localAlloc = &localArea->allocators[allocation];
            sliceSize = (area == DDR_AREA) ?
                        localArea->size :
                        CMX_SLICE_SIZE ;

            percentage =(double)localAlloc->size /
                        (double)sliceSize *
                        100;

            nmbOfBlocks = (int)(percentage * sizeof(MEM_DRAW_STR) / 100 + 0.5);
            nmbOfBlocks = nmbOfBlocks > 0 ? nmbOfBlocks : 1;
            totalNmbOfBlocks += nmbOfBlocks;

            if (totalNmbOfBlocks > sizeof(MEM_DRAW_STR)+10)
            {
                /*max number of blocks per row tinkering*/
                printf("\n\t\t");
                printf(ANSI_COLOR_RESET);
                totalNmbOfBlocks = 0;
            }
            printf(plotColors[localAlloc->state]);
            printf("%.*s", nmbOfBlocks, MEM_DRAW_STR);
            printf(ANSI_COLOR_RESET);
        }
        printf("](%d/%d)\n", localArea->usage, localArea->size);
    }
    DrvFastMutexUnlock(MEM_MGR_MUTEX);
}
