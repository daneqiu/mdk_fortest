#ifndef _MEMMANAGERAPI_H_
#define _MEMMANAGERAPI_H_

#ifndef MEM_MGR_MUTEX
#define MEM_MGR_MUTEX 31
#endif

#include "MyriadMemInit.h"

//Error codes
typedef enum{
    NO_ERROR = 0,						/*no error*/
    MEMALLOC_INVALID_SIZE = 10,		    /*Invalid size value*/
    MEMALLOC_SIZE_EXCEEDED = 11,	    /*Size bigger than available space*/
    MEM_INVALID_ADDRESS = 12,			/*Invalid address used*/
    MEM_EXPORT_ALLOCMEM = 13,           /*Allocate more memory for export buffer*/
}MemMgrErrorCode;

////////////////////////////////////// Prototypes ////////////////////////////////////////
                                        /*APIS*/
/// Allocate memory equal to the specified size inside the given memory area
/// @param[in] size        - size of the allocation
/// @param[in] area        - memory area where to allocate
/// @return    address     - address where memory was allocated
void* MemMgrAlloc(  size_t size, 
                    MemMgrAreas area); 

/// Free up memory allocated at the inputed address
/// @param[in] address     - address where to free memory from
/// @return    error       - error code
MemMgrErrorCode MemMgrFree(void *address);

/// Get memory’s current state as a buffer structed into a .csv style format for easy export
/// @param[in]  out        - array of pointers to output lines
/// @param[out] in         - array of pointers to input lines
/// @param[out] conv       - array of values from convolution
/// @return    error       - error code
MemMgrErrorCode MemMgrBufferExport( size_t bufSize,         
                                    char *csvBuffer,        
                                    size_t *csvLenght);     

/// Graphically illustrate the current state of the memory mapped inside the memory manager 
void MemMgrExportDraw(void);
#endif
