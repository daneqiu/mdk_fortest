include-dirs-shave-y+=$(include-dirs-los-y)
include-dirs-lrt-y+=$(include-dirs-los-y)

subdirs-los-y+=leon
subdirs-shave-y+=shave

CONFIG_BUILD_SUBDIRS_USING_OBJECTS:=n
CONFIG_BUILD_SUBDIRS_USING_AR:=y
CONFIG_BUILD_SUBDIRS_USING_LD:=n

include $(MDK_INSTALL_DIR_ABS)/components/BLIS/genblislib-nextgen.mk

BLIS_LIB=blis_lib_$(B_PROCESSOR).mvlib

# let the build-$(B_PROCESSOR) invoke our recipe for BLIS_LIB
LIB=$(BLIS_LIB)

collect-target-override=y
collect-$(B_PROCESSOR) : $(B_RECURSE_COLLECT)
	$(ECHO)echo "A:$(BLIS_LIB)" >> $(LIBRARIES_LIST)

$(BLIS_LIB): $(BLIS_OBJ_LEON) $(BLIS_OBJ_SHAVE)
	$(BLIS_OBJ_LEON) $(BLIS_OBJ_SHAVE)
	mkdir -p $(dir $(BLIS_LIB_LEON))
	mkdir -p $(dir $(BLIS_LIB_SHAVE))
	$(ECHO) $(AR) rs $(BLIS_LIB_LEON) $(BLIS_OBJ_LEON)
	$(ECHO) $(AR) rs $(BLIS_LIB_SHAVE) $(BLIS_OBJ_SHAVE)
