#-------------------------------[ Local shave applications sources ]--------------------#
#Choosing C sources the BLIS application on shave
BLISTestApp = $(DirAppOutput)/BLISTest

include-dirs-shave-y+=$(DirAppRoot)/shave
include-dirs-shave-y+=$(DirAppRoot)/shared

#Generating list of required generated assembly files for the BLISKernelTest app on shave
# BUILD_USE_GENASM
SHAVE_GENASMS_C_BLISTest = $(patsubst %.c,$(DirAppObjBase)%.asmgen,$(SHAVE_C_SOURCES_BLISTest))
SHAVE_GENASMS_CPP_BLISTest = $(patsubst %.cpp,$(DirAppObjBase)%.asmgen,$(SHAVE_CPP_SOURCES_BLISTest))

#Generating required objects list from sources
BLIS_OBJ_SHAVE = $(patsubst %.asm, $(DirAppObjBase)%_shave.o, $(srcs-shave-y)) \
				 $(patsubst %.c, $(DirAppObjBase)%_shave.o,$(srcs-shave-y)) \
                 $(patsubst %.cpp, $(DirAppObjBase)%_shave.o,$(srcs-shave-y))

#Update clean rules with our generated files
PROJECTCLEAN  += $(SHAVE_GENASMS_C_BLISTest) $(SHAVE_GENASMS_CPP_BLISTest) $(BLIS_OBJ_SHAVE)
#Uncomment below to reject generated shave as intermediary files (consider them secondary)
#PROJECTINTERM += $(SHAVE_GENASMS_BLISTest)

BLIS_OBJ_LEON = $(patsubst %.c,%.o,$(srcs-los-y))
PROJECTCLEAN += $(BLIS_OBJ_LEON)

BLIS_LIB_LEON = $(MV_COMMON_BASE)/../packages/movidius/BLIS/lib/blis_lib_leon.a
BLIS_LIB_SHAVE = $(MV_COMMON_BASE)/../packages/movidius/BLIS/lib/blis_lib_shave.a
