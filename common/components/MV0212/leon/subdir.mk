include-dirs-los-y+=\
  $(MDK_INSTALL_DIR_ABS)/components/MV0212/leon/bm/include \
  $(MDK_INSTALL_DIR_ABS)/components/MV0212/leon/common/gpioDefaults \
  $(MDK_INSTALL_DIR_ABS)/components/MV0212/leon/common/include \
  $(MDK_INSTALL_DIR_ABS)/components/MV0212/leon/rtems/include 
srcs-los-y+=\
 bm/src/MV0212.c \
 common/gpioDefaults/brdMv0212GpioDefaults.c \
 rtems/src/OsMV0212.c 
