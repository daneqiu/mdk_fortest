///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     SIPP engine
///

#ifndef _SIPP_TEST_COMMON_MA2x5x_H_
#define _SIPP_TEST_COMMON_MA2x5x_H_

extern u8   mbinImgSipp[];
extern void sippPlatformInit();
extern void sippPlatformInitAsync();

#endif // _SIPP_TEST_COMMON_MA2x5x_H_
