
include-dirs-los-y+=\
	$(MDK_INSTALL_DIR_ABS)/components/sipp/include

include-dirs-lrt-y+=$(include-dirs-los-y)
include-dirs-shave-y+=$(include-dirs-los-y)

subdirs-los-$(CONFIG_USE_COMPONENT_SIPP)+=filters
subdirs-lrt-$(CONFIG_USE_COMPONENT_SIPP)+=filters
subdirs-shave-$(CONFIG_USE_COMPONENT_SIPP)+=filters
