HelloWorld_LOS-RTEMS_LRT

Supported Platform
==================
Myriad2 - This example works on Myriad2: ma2150, ma2450 silicon and
 ma2150, ma2450 simulator

Overview
==========
Just starts RTEMS on Leon OS which then starts Leon RT.

Software description
=======================
This application uses Leon OS which is running RTEMS to start Leon RT.

Hardware needed
==================
Myriad2 -This software should run on MV182 board.

Build
==================
Please type "make help" if you want to learn available targets.

Myriad2 - To build the project please type:
     - "make clean"
     - "make build"

Setup
==================
Myriad2 simulator - To run the application:
    - open terminal and type "make start_simulator"
    - open another terminal and type "make run"
Myriad2 silicon - To run the application:
    - open terminal and type "make start_server"
    - open another terminal and type "make run"

Expected output
==================
The results  consist in printf seen using the debugger:
UART: LeonOS Started.
UART: LeonRT Start Addr: 0x701C1000
UART: LeonRT Started.
UART: LeonRT Stop.
