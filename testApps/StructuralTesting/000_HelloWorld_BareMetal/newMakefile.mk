# ------------------------------[ App ]--------------------------------------------#
# Default application name is the same as the folder it is in.
# This can be overridden here if something different is required
#APPNAME ?= $(notdir $(shell pwd))

# ------------------------------[ Build overrides ]--------------------------------#
# overrides that need to be set before including generic.mk
# TODO remove this section?

# ------------------------------[ Components used ]--------------------------------#
#Component lists are set using kcnf

# ------------------------------[ Tools overrides ]--------------------------------#
# Hardcode tool version here if needed, otherwise defaults to latest stable release
#MV_TOOLS_VERSION ?=

# Verbose or not, uncomment for more verbosity
#ECHO ?=

# Set MV_COMMON_BASE relative to mdk directory location (but allow user to override in environment)
#MV_COMMON_BASE  ?= ../../../common
MDK_INSTALL_DIR ?= ../../../common

# the subdirectory `leon` contains the Makefile describing the sources to be
# compiled for the LEON OS component of the application

# Include the generic Makefile
# include $(MV_COMMON_BASE)/generic.mk
include $(MDK_INSTALL_DIR)/mdk.mk

# the build system will automatically pick-up the leon sub-directory.

#-------------------------------[ Local shave applications build rules ]------------------#
#Describe the rule for building theApp application. Simple rule specifying 
#which objects build up the said application. The application will be built into a library

# -------------------------------- [ Build Options ] ------------------------------ #
# App related build options 

# Extra app related options
#CCOPT			+= -DDEBUG

