HelloWorldLeon

Supported Platform
==================
Myriad2 - This example works on Myriad2: ma2150, ma2450 silicon and
ma2150, ma2450 simulator

Overview
==========
This is a baremetal example that just prints hello world using Leon.

Software description
=======================
This application uses only the Leon code. Basically, these are the few steps made during the application:
    1. The Clock Power reset module is initialized.
    2. The DDR is also initialized.
    3. The L2 Cache must be invalidate.
    4. On this step, the setup is complete. The desired message can now be printed.
This is the easiest example on how to write an application using just a leon code.

Hardware needed
==================
Myriad2 -This software should run on MV182 board.

Build
==================
Please type "make help" if you want to learn available targets.

Myriad2 - To build the project please type:
     - "make clean"
     - "make build"

Setup
==================
Myriad2 simulator - To run the application:
    - open terminal and type "make start_simulator"
    - open another terminal and type "make run"
Myriad2 silicon - To run the application:
    - open terminal and type "make start_server"
    - open another terminal and type "make run"

Expected output
==================
 The result  consist of a printf message seen on debugger terminal.
 Hello from LEON!

