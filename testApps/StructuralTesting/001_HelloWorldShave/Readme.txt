HelloWorldShave

Supported Platform
==================
Myriad2 - This example works on Myriad2: ma2150, ma2450 silicon and
 ma2150, ma2450 simulator

Overview
==========
Just print hello world using shave compiled code using stdio library

Software description
=======================

This application uses both Leon and Shave code. Basically, these are the few steps made during the application:
    1. Configure the sistem.
Since this application implies the shave0 using, we have to set it properly:
    2. Reset shave 0
    3. Set the absolute default stack;
    4. Start shave 0
    5. Wait  shave
    6. On this step, the setup is complete. The desired message can now be printed.
This is the easiest example on how to write a shave code. The application just prints hello world using shave compiled code.

Hardware needed
==================
Myriad2 - This software runs on MV212 board.

Build
==================
Please type "make help" if you want to learn available targets.

Myriad2 - To build the project please type:
     - "make clean"
     - "make build"

Setup
==================
Myriad2 simulator - To run the application:
    - open terminal and type "make start_simulator"
    - open another terminal and type "make run"
Myriad2 silicon - To run the application:
    - open terminal and type "make start_server"
    - open another terminal and type "make run"

Where {Myriad_version} may be ma2150 or ma2450.
The default Myriad revision in this project is ma2150 so it is not necessary
to specify the MV_SOC_REV in the terminal.

Expected output
==================
 The result consist in printf seen using the debugger.
 Hello from SHAVE!

