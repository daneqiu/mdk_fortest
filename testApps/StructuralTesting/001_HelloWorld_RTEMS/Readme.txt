HelloWorld_RTEMS

Supported Platform
==================
Myriad2 - This example works on Myriad2: ma2150, ma2450 silicon and
ma2150, ma2450 simulator

Overview
==========
Starts two threads which run in parallel.
Each thread runs for a given time slice before releasing the processor for the other thread.


Hardware needed
==================
Myriad2 -This software should run on MV182 board.

Build
==================
Please type "make help" if you want to learn available targets.

Myriad2 - To build the project please type:
     - "make clean"
     - "make build"

Setup
==================
Myriad2 simulator - To run the application:
    - open terminal and type "make start_simulator"
    - open another terminal and type "make run"
Myriad2 silicon - To run the application:
    - open terminal and type "make start_server"
    - open another terminal and type "make run"

to specify the MV_SOC_REV in the terminal.

Expected output
==================
UART: C: 1
UART: C: 2
UART: C: 3
UART: C: 4
...
UART: C: 146
UART: C: 147
UART: C: 148
UART: D: 1
UART: D: 2
UART: D: 3
UART: D: 4
UART: D: 5
...
UART: D: 41
UART: D: 42
UART: D: 43
UART: D: 44

The ouput is similar with the listing above (but the figures can vary between simulator run and board run). One of the thread prints D and the other C.
This shows that the thread switch is happening.
Each thread run in an infinite loop, so you should hit CTRL+C to close the application.


User interaction
==================

