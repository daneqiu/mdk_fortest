USB Link simple application

Supported Platform
==================
Myriad2 - This example works on Myriad2: ma2150, ma2450
MyriadX - This example works on MyriadX: ma2480 ***

***usb_boot doesn't work for MyriadX so to run on MyriadX, start the myriad application
using the debugger (make debug MV_SOC_REV=ma2480) before starting the PC application.
Then, make the pc application with line 35 of the makefile (-D'BOOT_DISABLE')
uncommented
Overview
==========
Sends specified messages from PC to the Myriad. The Myriad receives messages from PC and sends them back.
When the PC gets the messages back, it compares them to the original sent buffers.

Software description
=======================
This test comprises of a PC application and a Myriad application
1) PC Application
This includes code for the pc application that does the following:
    1. Boots the Myriad
    2. Initializes XLink (including sending a ping from the PC)
    3. Opens a named stream
    4. Do the following 100 times:
        a) Write a message to the USB
        b) Read the response from the USB
2) Myriad Application
This includes leon code for LeonOS. The steps in this application are:
    1. The Clock Power reset module is initialized
    2. Initializes XLink (including sending a ping from the Myriad)
    3. Opens a named stream
    4. A thread that receives and returns 100 messages is started:

Hardware needed
==================
Myriad2 (ma2150 or ma2450) with USB connectivity

Build
==================
Please type "make help" if you want to learn available targets.

!!!Before cross-compiling make sure you do a "make clean"!!!

Myriad2 - To build the project, please cd to myriad/ and type:
            > "make clean"
            > "make all MV_SOC_REV={Myriad_version}"

Where {Myriad_version} may be ma2150 or ma2450.
The default Myriad revision in this project is ma2450

PC      - To build the PC side make sure you have libusbx-devel-1 installed on you machine,
          please cd to pc/ and type:
            > "make"

Setup and Run
==================
1) Ensure your udev rules are set correctly. See MDK document XLink_UserGuide.pdf

2) Confire your board boot switches. 2,7 UP (USB Bootmode)
    | |-| | | | |-| |
    |-| |-|-|-|-| |-|
     1 2 3 4 5 6 7 8

3) To run the application:
    - cd to pc/ and type './XLink'

Expected output
==================

 XLink pc example application used to communicate to the Myriad USB device.
  - Reading program argument list...
Argument's correct.
 moviUsbBoot binary found.

Myriad was booted
libusb could not be initialized!
*********************************
Remaining iterations: 99
*********************************
Booting myriad... Sending some data to myriad... Data sent
Reading back data from myriad... Data read
*********************************
Remaining iterations: 98
*********************************
Booting myriad... Sending some data to myriad... Data sent
Reading back data from myriad... Data read
*********************************
Remaining iterations: 97
*********************************
Booting myriad... Sending some data to myriad... Data sent
Reading back data from myriad... Data read

(...)

*********************************
Remaining iterations: 4
*********************************
Booting myriad... Sending some data to myriad... Data sent
Reading back data from myriad... Data read
*********************************
Remaining iterations: 3
*********************************
Booting myriad... Sending some data to myriad... Data sent
Reading back data from myriad... Data read
*********************************
Remaining iterations: 2
*********************************
Booting myriad... Sending some data to myriad... Data sent
Reading back data from myriad... Data read
*********************************
Remaining iterations: 1
*********************************
Booting myriad... Sending some data to myriad... Data sent
Reading back data from myriad... Data read
*********************************
Remaining iterations: 0
*********************************
Booting myriad... Sending some data to myriad... Data sent
Reading back data from myriad... Data read
USB Link profiling results:
Average write speed: 341.536987 MB/Sec
Average read speed: 50.343376 MB/Sec

