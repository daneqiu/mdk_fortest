#!/bin/bash

# Exit if a command fails
set -e

for t in `ls | egrep "T[1-9]"`; do
    echo "test $t"
    cd $t/myriad/
    make all MV_SOC_REV=ma2450
    cd ../pc/
    make
    ./XLink
    cd ../../ 
done

