
include-dirs-usb-y += $(MDK_PACKAGES_DIR_ABS)/movidius/usb/include
include-dirs-usb-$(CONFIG_USE_PACKAGE_USB_DATAPUMP)  += $(MDK_PACKAGES_DIR_ABS)/movidius/usb/include/datapump
include-dirs-usb-$(CONFIG_USE_PACKAGE_USB_OSRTEMS)   += $(MDK_PACKAGES_DIR_ABS)/movidius/usb/include/osrtems
include-dirs-usb-$(CONFIG_USE_PACKAGE_USB_OSRTEMS)   += \
	$(MDK_PACKAGES_DIR_ABS)/movidius/usb/include/osrtems/$(call unquote,$(CONFIG_SOC_DIR))
include-dirs-usb-$(CONFIG_USE_PACKAGE_USB_PROTOVSC2) += $(MDK_PACKAGES_DIR_ABS)/movidius/usb/include/protovsc2
include-dirs-usb-$(CONFIG_USE_PACKAGE_USB_PROTOWMC)  += $(MDK_PACKAGES_DIR_ABS)/movidius/usb/include/protowmc
include-dirs-usb-$(CONFIG_USE_PACKAGE_USB_XDCI)      += $(MDK_PACKAGES_DIR_ABS)/movidius/usb/include/xdci

include-dirs-los-y += $(include-dirs-usb-y)
include-dirs-lrt-y += $(include-dirs-usb-y)

# TODO handle here debug/release configurations
objs-usb-$(CONFIG_USE_PACKAGE_USB_DATAPUMP) += $(MDK_PACKAGES_DIR_ABS)/movidius/usb/lib/release/datapump.a
objs-usb-$(CONFIG_USE_PACKAGE_USB_OSRTEMS)  += \
	$(MDK_PACKAGES_DIR_ABS)/movidius/usb/lib/release/$(call unquote,$(CONFIG_SOC_DIR))/osrtems.a
objs-usb-$(CONFIG_USE_PACKAGE_USB_PROTOWMC) += $(MDK_PACKAGES_DIR_ABS)/movidius/usb/lib/release/protovsc2.a
objs-usb-$(CONFIG_USE_PACKAGE_USB_PROTOWMC) += $(MDK_PACKAGES_DIR_ABS)/movidius/usb/lib/release/protowmc.a
objs-usb-$(CONFIG_USE_PACKAGE_USB_XDCI)     += \
	$(MDK_PACKAGES_DIR_ABS)/movidius/usb/lib/release/$(call unquote,$(CONFIG_SOC_DIR))/xdci.a

libs-los-y += $(objs-usb-y)
libs-lrt-y += $(objs-usb-y)

