
# IMPORTANT NOTE: these makefiles are only used during the collect phase
# So the srcs-XXX clauses will be ignored in this file and those this file
# includes

subdirs-movidius-$(CONFIG_USE_PACKAGE_USB) += usb

subdirs-los-y += $(subdirs-movidius-y)
subdirs-lrt-y += $(subdirs-movidius-y)
subdirs-shave-y += $(subdirs-movidius-y)

