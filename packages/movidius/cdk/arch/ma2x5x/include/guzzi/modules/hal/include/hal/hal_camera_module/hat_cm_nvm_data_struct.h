/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file hat_cm_nvm_data_struct.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __HAL_CM_NVM_DATA_STRUCT_H__
#define __HAL_CM_NVM_DATA_STRUCT_H__

#include <osal/osal_stdtypes.h>

typedef struct {
    uint32 manufacturer_id;
    uint16 sensor_id;
    uint16 camera_module_ver;
    uint16 voice_coil_motor_id;
    uint16 module_assembling_id;
    uint16 sensor_ver;
    uint16 isp_ver;
    uint16 firmware_ver;
    uint16 nvm_data_ver;
    uint16 pcb_fpc_ver;
    uint16 opt_mech_ver;
    uint16 shading_data_ver;
    uint8 register_map;
    uint8 sw_version[3];
    uint8 serial_num[16];
} nvm_version_t;

typedef struct {
    uint16 r;
    uint16 gr;
    uint16 b;
    uint16 gb;
} nvm_spot_sens_t;

typedef struct {
    uint16 color_temp;
    nvm_spot_sens_t center;
} nvm_center_spot_t;

typedef struct {
    uint32 spots_uarr_size;
    nvm_center_spot_t (*spots);
} nvm_center_spot_info_t;

typedef struct {
    uint16 valid;
    uint16 x;
    uint16 y;
} nvm_lsc_center_data_t;

typedef struct {
    uint16 color_temp;
    nvm_lsc_center_data_t center_pax_r;
    nvm_lsc_center_data_t center_pax_gr;
    nvm_lsc_center_data_t center_pax_b;
    nvm_lsc_center_data_t center_pax_gb;
    uint32 pdata_uarr_size;
    nvm_spot_sens_t (*pdata);
} nvm_lsc_spot_sens_grid_t;

typedef struct {
    uint16 xsz;
    uint16 ysz;
    uint32 grids_uarr_size;
    nvm_lsc_spot_sens_grid_t (*grids);
} nvm_lsc_data_t;

typedef struct {
    int32 start_current;
    int32 inf_pos_typ;
    int32 mac_pos_typ;
    int32 inf_pos_min;
    int32 inf_pos_max;
    int32 mac_pos_min;
    int32 mac_pos_max;
    uint8 tal;
    uint8 tah;
    uint8 tbl;
    uint8 tbh;
    uint8 period;
    uint32 pulse_time;
    uint32 pwm_per_um_fwd;
    uint32 pwm_per_um_bkwd;
} nvm_lens_pos_t;

typedef struct {
    uint16 r;
    uint16 gr;
    uint16 gb;
    uint16 b;
} black_vs_gain_t;

typedef struct {
    uint32 noise;
    uint16 x_coord;
    uint16 y_coord;
} dpix_info_t;

typedef struct {
    uint32 dpix_info_uarr_size;
    dpix_info_t (*dpix_info);
} nvm_dpc_info_t;

typedef struct {
    uint8 sensor_specific[64];
    nvm_version_t version;
    nvm_center_spot_info_t center;
    nvm_lsc_data_t lsc;
    nvm_lens_pos_t positions;
    black_vs_gain_t bl_vs_gain[8];
    uint16 pedestal;
    nvm_dpc_info_t dpc;
} nvm_info_t;

#endif // __HAL_CM_NVM_DATA_STRUCT_H__
