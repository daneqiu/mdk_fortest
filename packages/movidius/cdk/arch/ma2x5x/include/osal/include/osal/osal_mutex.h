/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file osal_mutex.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef  __OSAL_MUTEX_H__
#define  __OSAL_MUTEX_H__

#include <osal/osal_stdtypes.h>

/* ==========================================================================
 *                                OSAL MUTEX
 * ========================================================================= */

#define OSAL_MUTEX_TIMEDOUT     1
#define OSAL_MUTEX_MAXWAIT      (~(uint32)0)

struct osal_mutex;
typedef struct osal_mutex osal_mutex;

/** Create a mutex, initialized unlocked */
osal_mutex *osal_mutex_create(void);

/** Destroy a mutex */
void osal_mutex_destroy(osal_mutex *mutex);

/** Lock the mutex
 *  @return 0, or -1 on error
 */
int osal_mutex_lock(osal_mutex *mutex);

/** Lock the mutex with a timeout in milliseconds, returns 0 if
 *  the wait succeeds, SDL_MUTEX_TIMEDOUT if the wait does not succeed in
 *  the allotted time, and -1 on error.
 *
 *  On some platforms this function is implemented by looping with a delay
 *  of 1 ms, and so should be avoided if possible.
 */
int osal_mutex_lock_timeout(osal_mutex *mutex, uint32 ms);

/** Try Lock the mutex
 *  @return 0, or -1 on error
 */
int osal_mutex_trylock(osal_mutex *mutex);

/** Unlock the mutex
 *  @return 0, or -1 on error
 *
 *  It is an error to unlock a mutex that has not been locked by
 *  the current thread, and doing so results in undefined behavior.
 */
int osal_mutex_unlock(osal_mutex *mutex);

/* ==========================================================================
 *                                OSAL SEMAPHORES
 * ========================================================================= */
struct osal_sem;
typedef struct osal_sem osal_sem;

/** Create a semaphore, initialized with value, returns NULL on failure. */
osal_sem *osal_sem_create(uint32 init_value);

/** Destroy a semaphore */
void osal_sem_destroy(osal_sem *sem);

/** Initialize semaphore */
int osal_sem_init(osal_sem *sem, uint32 value);

/**
 * This function suspends the calling thread until the semaphore pointed
 * to by sem has a positive count. It then atomically decreases the semaphore
 * count.
 */
int osal_sem_wait(osal_sem *sem);

/** Non-blocking variant of SDL_SemWait().
 *  @return 0 if the wait succeeds,
 *  SDL_MUTEX_TIMEDOUT if the wait would block, and -1 on error.
 */
int osal_sem_try_wait(osal_sem *sem);

/** Variant of SDL_SemWait() with a timeout in milliseconds, returns 0 if
 *  the wait succeeds, SDL_MUTEX_TIMEDOUT if the wait does not succeed in
 *  the allotted time, and -1 on error.
 *
 *  On some platforms this function is implemented by looping with a delay
 *  of 1 ms, and so should be avoided if possible.
 */
int osal_sem_wait_timeout(osal_sem *sem, uint32 ms);

/** Atomically increases the semaphore's count (not blocking).
 *  @return 0, or -1 on error.
 */
int osal_sem_post(osal_sem *sem);

/** Returns the current count of the semaphore */
uint32 osal_sem_value(osal_sem *sem);

/* ==========================================================================
 *                                OSAL Condition
 * ========================================================================= */
struct osal_cond;
typedef struct osal_cond osal_cond;

/** Create a condition variable */
osal_cond *osal_cond_create(void);

/** Destroy a condition variable */
void osal_cond_destroy(osal_cond *cond);

/** Restart one of the threads that are waiting on the condition variable,
 *  @return 0 or -1 on error.
 */
int osal_cond_signal(osal_cond *cond);

/** Restart all threads that are waiting on the condition variable,
 *  @return 0 or -1 on error.
 */
int osal_cond_broadcast(osal_cond *cond);

/** Wait on the condition variable, unlocking the provided mutex.
 *  The mutex must be locked before entering this function!
 *  The mutex is re-locked once the condition variable is signaled.
 *  @return 0 when it is signaled, or -1 on error.
 */
int osal_cond_wait(osal_cond *cond, osal_mutex *mutx);

/** Waits for at most 'ms' milliseconds, and returns 0 if the condition
 *  variable is signaled, osal_MUTEX_TIMEDOUT if the condition is not
 *  signaled in the allotted time, and -1 on error.
 *  On some platforms this function is implemented by looping with a delay
 *  of 1 ms, and so should be avoided if possible.
 */
int osal_cond_wait_timeout(osal_cond *cond, osal_mutex *mutex, uint32 ms);

#endif /* __OSAL_MUTEX_H__ */
