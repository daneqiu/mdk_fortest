This PC application was designed for displaying video streams from CDK applications.

It has been built for Ubuntu 16.04 LTS.

IMPORTANT NOTES:

It is compatible with CDK applications implementing FLIC on ma2x5x.

Start the application (i.e. run the executable) after initializing the video device (<Movidius camera> shall be detected), otherwise it will crash.
