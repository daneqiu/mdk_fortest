/*
 * MultiTransport.c
 *
 *  Created on: Aug 31, 2017
 *      Author: apalfi
 */

//================================================================================
// INCLUDES
//================================================================================
#include <stdio.h>
#include <assert.h>
#include "MultiTransport.h"

#ifdef READSENSE_LOGIC
static MultiTransport_readyCallback_type readsense_callback = NULL;

int MultiTransport_setDataReadyCallBack(MultiTransport_readyCallback_type callback)
{
    readsense_callback = callback;
    return 0;
}
#endif

// #include <H264Codec.h>

//================================================================================
// GLOBAL FUNCTIONS
//================================================================================
void MultiTransport_Init(void)
{
// #ifdef ENABLE_H264 // Run H264 encoder
//         s32 st = H264Init();
//         if(st)
//         {
//             printf("H264Init error: %d \n",st);
//         }
// #endif
	s32 status;

    return;

}

void MultiTransport_SendFrame(SendOutElement_t *task)
{
    FrameT *frame = task->buffer;
	s32 status;

    switch (frame->type)
    {
        case FRAME_TYPE_PREVIEW:
        case FRAME_TYPE_STILL:
        case FRAME_TYPE_USER_01:
        case FRAME_TYPE_USER_02:
        {
#ifdef READSENSE_LOGIC
            if (readsense_callback != NULL)
            {
                readsense_callback(frame);
            }
#endif
            break;
        }

        default:
        {
            assert (!"MultiTransport_SendFrame: Invalid frame instance id");
            break;
        }
    }

    // This is mandatory
    task->localCallback(task);

    return;
}


