/*
 * MultiTransport.h
 *
 *  Created on: Aug 31, 2017
 *      Author: apalfi
 */

#ifndef _MULTITRANSPORT_MULTITRANSPORT_H_
#define _MULTITRANSPORT_MULTITRANSPORT_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include "mv_types.h"
#include "sendOutApi.h"

#ifdef __cplusplus
extern "C" {
#endif

//================================================================================
// DEFINES
//================================================================================

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------
// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------
void MultiTransport_Init(void);
void MultiTransport_SendFrame(SendOutElement_t *task);


#define TRANSPORT_create(...)
#define TRANSPORT_init MultiTransport_Init
#define TRANSPORT_SendFrame MultiTransport_SendFrame
#define TRANSPORT_Fini(...)

#ifdef READSENSE_LOGIC
typedef int32_t (*MultiTransport_readyCallback_type)(FrameT * frame);

int MultiTransport_setDataReadyCallBack(MultiTransport_readyCallback_type callback);
#endif

#ifdef __cplusplus
}
#endif

#endif /* _MULTITRANSPORT_MULTITRANSPORT_H_ */
