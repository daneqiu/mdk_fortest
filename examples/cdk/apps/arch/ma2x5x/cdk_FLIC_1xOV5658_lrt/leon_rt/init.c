#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <rtems.h>
#include <bsp.h>

#include <rtems/bspIo.h>

#include <semaphore.h>
#include <pthread.h>
#include <sched.h>
#include <fcntl.h>
#include <mv_types.h>
#include <rtems/cpuuse.h>
#include <DrvLeon.h>
#include "rtems_config.h"
//#define CMX_CONFIG_SLICE_7_0       (0x11111111)
//#define CMX_CONFIG_SLICE_15_8      (0x11111111)
//#define L2CACHE_NORMAL_MODE (0x6)  // In this mode the L2Cache acts as a cache for the DRAM
//#define L2CACHE_CFG     (L2CACHE_NORMAL_MODE)
//#define BIGENDIANMODE   (0x01000786)
/* Sections decoration is require here for downstream tools */
//extern CmxRamLayoutCfgType __cmx_config;
//CmxRamLayoutCfgType __attribute__((section(".cmx.ctrl"))) __cmx_config = {CMX_CONFIG_SLICE_7_0, CMX_CONFIG_SLICE_15_8};
//uint32_t __l2_config   __attribute__((section(".l2.mode")))  = L2CACHE_CFG;




int main(int argc, char **argv);

void* POSIX_Init (void *args)
{
    UNUSED(args);
    main(0, NULL);
	return NULL;
}
