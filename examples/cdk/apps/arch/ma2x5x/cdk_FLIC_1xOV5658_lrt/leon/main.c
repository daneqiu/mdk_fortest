/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <stdint.h>
// #include <version_info.h>
#include <pthread.h>

#include "rtems.h"
#ifdef _REPORT_CPU_USAGE
#include "rtems/cpuuse.h"
#endif

#include "initSystem.h"
#include <stdio.h>
#include "LeonIPCApi.h"
#include "DrvIcb.h"
#include "camera_control.h"
#include "app_guzzi_command_spi.h"
#include "app_guzzi_command_dbg.h"
#include "FrameTypes.h"

extern leonIPCChannel_t lrt_LRTtoLOSChannel;

extern leonIPCChannel_t lrt_LOStoLRTChannel;
#define MSG_QUEUE_SIZE 3
#define MSG_SIZE sizeof(app_guzzi_command_t)

uint32_t __attribute__((section(".cmx_direct.data"))) messagePool[MSG_QUEUE_SIZE * MSG_SIZE];

/*
 * ****************************************************************************
 * ** Main ********************************************************************
 * ****************************************************************************
 */
int main(int argc, char **argv)
{
	int status = -1;
	u32 msg;
	u32 msgNo;

    UNUSED(argc);
    UNUSED(argv);
    rtems_object_set_name(RTEMS_SELF, "main");


    // while (status != IPC_SUCCESS) 
    {
        status = LeonIPCTxInit(&lrt_LOStoLRTChannel, messagePool, MSG_QUEUE_SIZE, MSG_SIZE);
	}
	if (status != IPC_SUCCESS) {
		printf("LeonIPCTxInit failed, status=%u\n", status);
	} else {
		printf("LeonIPCTxInit done\n");
	}

    initSystem();

#if 1
	// while (status != IPC_SUCCESS) 
    {
		status = LeonIPCRxInit(&lrt_LRTtoLOSChannel, NULL, IRQ_DYNAMIC_5, 5);
	}
    if (status != IPC_SUCCESS) {
		printf("LeonIPCRxInit failed, status=%u\n", status);
	} else {
		printf("LeonIPCRxInit done\n");
	}
#endif


    while(1)
    {

#ifdef _REPORT_CPU_USAGE
        sleep(3);
        rtems_cpu_usage_report();
        rtems_cpu_usage_reset ();
#endif

        int ret = 5;
        //ret = getchar();
        switch (ret)
        {
            case 'c':
                printf("%c input\n",ret);
                break;
        }

        //send the camere start

        sleep(6);
        app_guzzi_command_t command;
        command.id = APP_GUZZI_COMMAND__CAM_START;
        command.cam.id = 0;
        status = LeonIPCSendMessage(&lrt_LOStoLRTChannel, &command);
        if (status != IPC_SUCCESS)
        	printf("LeonIPCSendMessage failed status = %d\n", status);
        else
        	printf("LeonIPCSendMessage success with buffPtr = 0x%x\n", &command);  


#if 1
while (1)
{
		LeonIPCNumberOfPendingMessages(&lrt_LRTtoLOSChannel, &msgNo);
		while (msgNo > 0) {
            FrameT* frame;
			status = LeonIPCReadMessage(&lrt_LRTtoLOSChannel, &frame);
			if (status == IPC_SUCCESS) {
                printf("[LeonIPCReadMessage]: Frame address: %p, %dx%d\n", frame->fbPtr[0], frame->stride[0], frame->height[0]);
				msgNo--;
			}
		}
}
#endif

        sleep(10);
#if 0
        //stop
        command.id = APP_GUZZI_COMMAND__CAM_STOP;
        command.cam.id = 0;
        status = LeonIPCSendMessage(&LOStoLRTChannel, &command);
        if (status != IPC_SUCCESS)
        	printf("LeonIPCSendMessage failed status = %d\n", status);
        else
        	printf("LeonIPCSendMessage success with buffPtr = 0x%x\n", &command);  

        sleep(10);
 #endif       

 #if 1
 
        //change the resolution
        command.id = APP_GUZZI_COMMAND__SENSOR_SIZE_MODE;
        command.cam.id = 0;
        command.cam.sensorsizemode.val = 1; // 1 ,2,3 
          status = LeonIPCSendMessage(&lrt_LOStoLRTChannel, &command);
        if (status != IPC_SUCCESS)
        	printf("LeonIPCSendMessage failed status = %d\n", status);
        else
        	printf("LeonIPCSendMessage success with buffPtr = 0x%x\n", &command);


         sleep(10);

        //change the resolution
        command.id = APP_GUZZI_COMMAND__SENSOR_SIZE_MODE;
        command.cam.id = 0;
        command.cam.sensorsizemode.val = 2; // 1 ,2,3 
          status = LeonIPCSendMessage(&lrt_LOStoLRTChannel, &command);
        if (status != IPC_SUCCESS)
        	printf("LeonIPCSendMessage failed status = %d\n", status);
        else
        	printf("LeonIPCSendMessage success with buffPtr = 0x%x\n", &command);


         sleep(10);        

        //change the resolution
        command.id = APP_GUZZI_COMMAND__SENSOR_SIZE_MODE;
        command.cam.id = 0;
        command.cam.sensorsizemode.val = 3; // 1 ,2,3 
          status = LeonIPCSendMessage(&lrt_LOStoLRTChannel, &command);
        if (status != IPC_SUCCESS)
        	printf("LeonIPCSendMessage failed status = %d\n", status);
        else
        	printf("LeonIPCSendMessage success with buffPtr = 0x%x\n", &command);


         sleep(10);         
#endif        
#if 1
		LeonIPCNumberOfPendingMessages(&lrt_LRTtoLOSChannel, &msgNo);
		while (msgNo > 0) {
			status = LeonIPCReadMessage(&lrt_LRTtoLOSChannel, &msg);
			if (status == IPC_SUCCESS) {
				printf("[LeonIPCReadMessage]: msg=0x%lx\n", msg);
				msgNo--;
			}
		}
#endif
    }
    return 0;
}
