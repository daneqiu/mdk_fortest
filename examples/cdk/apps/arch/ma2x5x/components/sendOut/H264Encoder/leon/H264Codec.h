///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved
///            For License Warranty see: common/license.txt
///
/// @brief
///
///
///
///
///

#ifndef H264_CODEC_H_
#define H264_CODEC_H_
// 1: Includes
// ----------------------------------------------------------------------------
#include "mv_types.h"
//FLIC
#include <FrameTypes.h>
//#include <IcTypes.h>

#include "H4Portab.h"
#include "H4Defines.h"
#include "H4EncoderAPI.h"

// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------
#ifndef IMG_MAX_TS
#define IMG_MAX_TS 4 // Maximum history timestamp associated with a frame
#endif
#ifndef IMG_MAX_PLANES
#define IMG_MAX_PLANES 4 // Maximum history timestamp associated with a frame
#endif

// 3:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------

// 4:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------
s32 H264Init();
s32 H264UnInit();
int H264_SendNewFrame(FrameT *frame);
int H264_ReadyToSend();
int H264_SendNewFrame2(uint8_t * data, int width, int height);

#ifdef READSENSE_LOGIC

typedef int32_t (*H264_readyCallback_type)(void * data, int size);

/* set decode cfg, called before H264Init to take effect */
int H264_setEncodeCfg(H4EncoderUserConfig_t cfg);
/* get current decode cfg */
H4EncoderUserConfig_t H264_getEncodeCfg();

int H264_setDataReadyCallBack(H264_readyCallback_type callback);
#endif

#endif
