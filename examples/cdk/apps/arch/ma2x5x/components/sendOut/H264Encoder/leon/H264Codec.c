///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "Defines.h"
#include "H4EncoderAPI.h"
#include "H4Portab.h"
#include "HWResources.h"
#include "swcLeonUtils.h"

#include <VcsHooksApi.h>

#include <OsDrvShaveL2Cache.h>
#include <sched.h>
#include <pthread.h>
#include <rtems.h>
#include <DrvShaveL2Cache.h>
#include "DrvTimer.h"
#include "H264Codec.h"
#include "OsDrvSvu.h"

#define PARTITION_NR                      0
// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
#define NALU_PARAMETERS_SIZE  256

// The current maximum frame size supported by the h264 encoder component.
#define MAX_H264_FRAME_WIDTH  1920
#define MAX_H264_FRAME_HEIGHT 1080

// With and height of the frame to be encoded. Using maximum size in this example.
// The width and height of the h264 frame does not have to be the same size as the camera
// frame size but the strides need to be set accordingly.
#define H264_FRAME_WIDTH  1920
#define H264_FRAME_HEIGHT 1080

// Maximum number of nalu buffers.
// This should be set accordingly to how fast the application uses the nalus
// (i.e. write them on storage, stream them on the network, etc.).
#define MAX_NALU_BUFFERS 16

// The size of a nalu buffer should be enough to hold an encoded frame for the
// required bit rate.
#define MAX_NALU_BUFFER_SIZE (1000 * 1024)

// #define DEBUG_CODE_ENABLED
 
// 3: Global Data (Only if absolutely necessary)

#ifdef READSENSE_LOGIC
static H264_readyCallback_type readsense_callback = NULL;

int H264_setDataReadyCallBack(H264_readyCallback_type callback)
{
    readsense_callback = callback;
    return 0;
}

H4EncoderUserConfig_t CMX_DMA  enc_cfg;
H4EncoderUserConfig_t H264_getEncodeCfg()
{
    return enc_cfg;
}
int H264_setEncodeCfg(H4EncoderUserConfig_t cfg)
{
    enc_cfg = cfg;
    return 0;
}
#endif

/* HW configuration */
H4Void* H4EncoderSetHWResourcesConfiguration();

/* Variables for HL */
H4InstanceHandle_t CMX_DMA  instance;
H4OSALHandle_t     CMX_DMA  hOSALHandle;
H4RetCode_t        CMX_DMA  ret_code;
H4EncoderStatus_t  CMX_DMA  enc_status;

static pthread_mutex_t h264FrameReadyLock;

// Typical structure used for handling frames in conjunction with H264 encoder.
typedef struct
{
  s32          width;
  s32          height;
  s32          padding;
  s32          stride_y;
  s32          stride_uv;
  volatile s32 time_stamp;
  volatile s32 references;
  volatile s32 used_by_cam;
  u8           *y;
  u8           *u;
  u8           *v;
  enum {ORIGINAL, RECONSTRUCTED} frameType; // Identifies either original or reconstructed frames
} AppFrame_t;

typedef struct
{
  H4NALU_t nalu;
  volatile u32 is_full;
  volatile u32 is_used;
} AppNALU_t;

// 4: Static Local Data
// ----------------------------------------------------------------------------
static AppNALU_t CMX_DMA  nalu_buffer[MAX_NALU_BUFFERS];
// Buffers for the nal units output by the h264 encoder.
static volatile u8 __attribute__((section(".ddr.bss"))) ALIGNED(16) nalu_packet[MAX_NALU_BUFFERS][MAX_NALU_BUFFER_SIZE];

static pthread_t thread_h264, thread_nalu_consumer;

// Index of the next nalu buffer to be written by the h264 encoder.
s32 next_nalu_index_get = 0;
s32 next_nalu_index_rel = 0;

// extern volatile u32 h264_frame_ready;
// extern volatile u32 h264_orig_ping;
// extern volatile AppFrame_t *ping_h264_orig_frame;
// extern volatile AppFrame_t *pong_h264_orig_frame;

// extern volatile u32 h264_recon_frame_ready;
// extern volatile u32 h264_recon_ping;
// extern volatile AppFrame_t *ping_h264_recon_frame;
// extern volatile AppFrame_t *pong_h264_recon_frame;

// // Marking the first and last nalu to write when in the recording state.
// extern volatile s32 first_nalu_to_write_time_stamp;
// extern volatile s32 last_nalu_to_write_time_stamp;

// extern volatile u32 lrt_signal_record_button_pressed;
// extern volatile u32 lrt_signal_stop_app_button_pressed;

// extern uint64_t lrt_totalEncodedFrames;

static s32 next_nalu_to_consume = 0;
static volatile u32 h264_frame_ready = FALSE;
static volatile u32 h264_orig_ping = FALSE;
static volatile AppFrame_t CMX_DMA  ping_h264_orig_frame_alloc;
static volatile AppFrame_t CMX_DMA  pong_h264_orig_frame_alloc;
static volatile AppFrame_t CMX_DMA  *ping_h264_orig_frame = &ping_h264_orig_frame_alloc;
static volatile AppFrame_t CMX_DMA  *pong_h264_orig_frame = &pong_h264_orig_frame_alloc;

// Marking the first and last nalu to write when in the recording state.
static volatile s32 first_nalu_to_write_time_stamp = 0;
static volatile s32 last_nalu_to_write_time_stamp = 0;

// Index of the next nalu buffer to be written by the h264 encoder.
static s32 next_nalu_index = 0;

static int started = 0;

volatile int h264OutBuffInd =0 ;
volatile int naluInUse = 0;
static u32 encodedFrames = 0;

#define H264_MAX_BUFF_SIZE (1024 * 1024 * 12)
#ifndef READSENSE_LOGIC
u8 __attribute__((section(".ddr.bss"))) ALIGNED(16) h264OutputBuffer[H264_MAX_BUFF_SIZE];
#else 
u8 __attribute__((section(".ddr.bss"))) ALIGNED(16) h264OutputBuffer[4*1024*1024];
#endif

extern char H264OsalX_LibBuildTag;

// H264 configuration.
H4EncoderUserConfig_t CMX_DMA  enc_cfg =
{
    640*2,//H264_FRAME_WIDTH, /*!< image width */
    480*2,//H264_FRAME_HEIGHT, /*!< image height */
                  0,//100, /*!< Number of frames to be encoded */
                    0, /*!< Number of I frames between IDR frames */
                   29, /*!< Number of P frames between I frames */
           _H4_EC_VLC, /*!< Entropy encoder type */
           _H4_RC_CBR, /*!< selected rate control mechanism */
            2000000,//  9000000, /*!< indicated bitrate for the stream */
                 30.0, /*!< framerate of the stream */
                    0, /*!< Initial quantizer for I frames (with RC, 0 is autodetect)*/
           _H4_MAX_QP, /*!< Max quantizer for P frames */
                   -4, /*!< delta quantizer low limit */
                    4, /*!< delta quantizer high limits */
           (float)1.5, /*!< AQ strength factor */
    _H4_YUV420_PLANAR, /*!< input YUV format */
                 FALSE, /*!< externalize reconstructed YUV datapath (set TRUE to get recon file */
                 TRUE, /*!< enable in-loop filter */
      _H4_MOTION_HPEL, /*!< Select motion accuracy */
         _H4_SPEED_S3, /*!< encoding speed setting (1 - 3) */
                    0, /*!< number of rows per slice */
                    2,//4, /*!< number of parallel threads */
_H4_LEVEL_UNSPECIFIED, /*!< level to be encoded in SPS */
        (H4Void*)NULL, /*!< user info associated with the instance */
                FALSE  /*!< enable SSIM computation (only for Win32) */
};

static void InitNALUBuffers();
H4Void* H4EncoderSetHWResourcesConfiguration();
// 6: Functions Implementation
// ----------------------------------------------------------------------------
// Initialize the nalu buffers.
static void InitNALUBuffers()
{
    int nalu_index;
    for (nalu_index = 0; nalu_index < MAX_NALU_BUFFERS; ++nalu_index)
    {
        nalu_buffer[nalu_index].nalu.packet = (H4UInt8 *) nalu_packet[nalu_index];
        nalu_buffer[nalu_index].nalu.size = 0;
        nalu_buffer[nalu_index].nalu.cts = 0;
        nalu_buffer[nalu_index].nalu.dts = 0;
        nalu_buffer[nalu_index].nalu.naluType = _H4_NALU_UNDEFINED;
        nalu_buffer[nalu_index].nalu.startAU = FALSE;
        nalu_buffer[nalu_index].nalu.endAU = FALSE;
        nalu_buffer[nalu_index].is_full = FALSE;
    }
}

void * NALUConsumer(void *args)
{
    UNUSED(args);
    int st;

    rtems_object_set_name(RTEMS_SELF, "H264_NALU_Cons");

    do
    {
        //printf("nalu_buffer full :%d\n", nalu_buffer[next_nalu_to_consume].is_full);
        if (nalu_buffer[next_nalu_to_consume].is_full)
        {
            // consume the nalu.
            int n_bytes_to_write = nalu_buffer[next_nalu_to_consume].nalu.size;

            if ((h264OutBuffInd + n_bytes_to_write) > H264_MAX_BUFF_SIZE)
            {
                printf("Buffer full, exiting...\n");
                break;
            }

            // If the current nalu has a time stamp captured in the recording process then it
            // can be written to the file. If we were able to open a new file that is.
            if ((h264OutBuffInd + n_bytes_to_write) < H264_MAX_BUFF_SIZE
                && nalu_buffer[next_nalu_to_consume].nalu.dts >= (H4UInt32) first_nalu_to_write_time_stamp
                && nalu_buffer[next_nalu_to_consume].nalu.dts <= (H4UInt32) last_nalu_to_write_time_stamp)
            {
                st = OsDrvShaveL2CachePartitionFlush(PARTITION_NR, PERFORM_INVALIDATION);
                if (st)
                {
                    printf("H264 OsDrvShaveL2CachePartitionFlush error %d \n", st);
                    pthread_exit(0);
                    break;
                }

#ifdef READSENSE_LOGIC
                if (readsense_callback != NULL)
                {
                    readsense_callback((const void *) ((H4UInt32) nalu_buffer[next_nalu_to_consume].nalu.packet | 0x40000000),
                        n_bytes_to_write);
                }
                naluInUse = 0;
#else
                // Read the nalu buffer uncached when writing to sdcard i.e. (nalu_buffer[next_nalu_to_consume].nalu.packet | 0x40000000)
                //write(stream_file, (const void *)(((H4UInt32)nalu_buffer[next_nalu_to_consume].nalu.packet | 0x40000000)), n_bytes_to_write);
                memcpy(&h264OutputBuffer[h264OutBuffInd],
                       (const void *) ((H4UInt32) nalu_buffer[next_nalu_to_consume].nalu.packet | 0x40000000),
                       n_bytes_to_write);

                h264OutBuffInd += n_bytes_to_write;
                naluInUse = 0;
                printf("h264OutputBuffer : %x h264OutBuffInd %d\n", (u32)&h264OutputBuffer[0], h264OutBuffInd);
                
#endif
                //printf("Wrote NALU %2d, size %d, total %d\n", next_nalu_to_consume, n_bytes_to_write, h264OutBuffInd);
            }
            //printf("set next_nalu_to_consume %d to FALSE\n", next_nalu_to_consume);
            nalu_buffer[next_nalu_to_consume].is_full = FALSE;
            next_nalu_to_consume = ((next_nalu_to_consume + 1) % MAX_NALU_BUFFERS);
        }
        else
            rtems_task_wake_after(1);
    }
    while (started);

    printf("H264_NALU_Cons exit\n");
    pthread_exit(0);
}


/* IMPORTANT: Do not use critical section or time-consuming calls in this callback!
It is stalling the SHAVES and may also be called from an IRQ */
H4RetCode_t EncNALUCallback(H4InstanceHandle_t h_encoder,
                            H4NALU_t          *p_nalu,
                            H4NALUCBState_t   state)
{
  (void)h_encoder;
#ifdef DEBUG_CODE_ENABLED  //Debug code
    printf("-- NALU \n");
    switch(p_nalu->naluType){
     case _H4_NALU_UNDEFINED: printf("UNDEFINED"); break;
     case _H4_NALU_I:   printf("I");   break;
     case _H4_NALU_P:   printf("P");   break;
     case _H4_NALU_B:   printf("B");   break;
     case _H4_NALU_IDR: printf("IDR"); break;
     case _H4_NALU_SPS: printf("SPS"); break;
     case _H4_NALU_PPS: printf("PPS"); break;
     case _H4_NALU_AUD: printf("AUD"); break;
     default: printf("error\n"); break;
    }
#endif
  switch (state)
  {
    case _H4_NALU_GET:
#ifdef DEBUG_CODE_ENABLED  //Debug code
        printf(" GET %ld\n", next_nalu_index_get);
#endif
        if(!nalu_buffer[next_nalu_index_get].is_full )//&& !nalu_buffer[next_nalu_index_get].is_used)
        {
          //p_nalu->packet = (void*)((uint32_t)(nalu_buffer[next_nalu_index_get].nalu.packet) | 0x40000000);
          p_nalu->packet = (void*)((uint32_t)(nalu_buffer[next_nalu_index_get].nalu.packet));
          switch (p_nalu->naluType){
            case _H4_NALU_SPS:
            case _H4_NALU_PPS:
                p_nalu->size = NALU_PARAMETERS_SIZE;
            default:
                p_nalu->size = MAX_NALU_BUFFER_SIZE;
                break;
          }
        }
        else
        {
          // There isn't a free nalu buffer. Probably they are waiting to be consumed.
          printf("_H4_BITSTREAM_NO_DATA_YET\n");
          return _H4_BITSTREAM_NO_DATA_YET;
        }
        nalu_buffer[next_nalu_index_get].is_used = TRUE;
        next_nalu_index_get = ((next_nalu_index_get + 1) % MAX_NALU_BUFFERS);
      break;

    case _H4_NALU_RELEASE:
#ifdef DEBUG_CODE_ENABLED  //Debug code
        printf(" REL %ld\n", next_nalu_index_rel);
#endif
        // Copy nalu information. It is safe to copy the packet pointer as well.
        //DrvShaveL2CachePartitionFlush(0);  //has done flush in NALUConsumer
        nalu_buffer[next_nalu_index_rel].nalu.packet   = (H4UInt8*)swcLeonReadNoCacheU32(&p_nalu->packet);
        nalu_buffer[next_nalu_index_rel].nalu.cts      = swcLeonReadNoCacheU32(&p_nalu->cts);
        nalu_buffer[next_nalu_index_rel].nalu.dts      = swcLeonReadNoCacheU32(&p_nalu->dts);
        nalu_buffer[next_nalu_index_rel].nalu.size     = swcLeonReadNoCacheI32(&p_nalu->size);
        nalu_buffer[next_nalu_index_rel].nalu.endAU    = swcLeonReadNoCacheU8(&p_nalu->endAU);
        nalu_buffer[next_nalu_index_rel].nalu.startAU  = swcLeonReadNoCacheU8(&p_nalu->startAU);
        nalu_buffer[next_nalu_index_rel].nalu.naluType = swcLeonReadNoCacheU8(&p_nalu->naluType);
        nalu_buffer[next_nalu_index_rel].is_full       = TRUE;
        nalu_buffer[next_nalu_index_rel].is_used       = FALSE;
        next_nalu_index_rel = ((next_nalu_index_rel + 1) % MAX_NALU_BUFFERS);
      break;
  }

  //printf("\n");
  return _H4_OK;
}

/* IMPORTANT: Do not use critical section or time-consuming calls in this callback!
It is stalling the SHAVES and may also be called from an IRQ */
H4RetCode_t EncFrameCallback(H4InstanceHandle_t h_encoder,
                             H4FrameHandle_t   *hframe,
                             H4Frame_t         *p_fd,
                             H4FrameCBState_t  state)
{
    AppFrame_t *p_frame;

    // Unused parameters for the moment.
    (void) h_encoder;

    switch (state)
    {
    case _H4_FRAME_GET_ORIG:
        // Set the required fields for the new frame.
        // The camera frame size is bigger than the h264 frame size.
        // We could choose to start with other pixel in the camera frame
        // by adding a proper offset to the pPlane[n].
        if (h264_orig_ping)
        {
            p_fd->pPlane[0] = (H4Void *) ping_h264_orig_frame->y;
            p_fd->pPlane[1] = (H4Void *) ping_h264_orig_frame->u;
            p_fd->pPlane[2] = (H4Void *) ping_h264_orig_frame->v;
            p_fd->width = ping_h264_orig_frame->width;
            p_fd->height = ping_h264_orig_frame->height;
            p_fd->stride_y = ping_h264_orig_frame->stride_y;
            p_fd->stride_uv = ping_h264_orig_frame->stride_uv;
            p_fd->time_stamp = ping_h264_orig_frame->time_stamp;

            *hframe = (H4FrameHandle_t) (ping_h264_orig_frame);
        }
        else
        {
            p_fd->pPlane[0] = (H4Void *) pong_h264_orig_frame->y;
            p_fd->pPlane[1] = (H4Void *) pong_h264_orig_frame->u;
            p_fd->pPlane[2] = (H4Void *) pong_h264_orig_frame->v;
            p_fd->width = pong_h264_orig_frame->width;
            p_fd->height = pong_h264_orig_frame->height;
            p_fd->stride_y = pong_h264_orig_frame->stride_y;
            p_fd->stride_uv = pong_h264_orig_frame->stride_uv;
            p_fd->time_stamp = pong_h264_orig_frame->time_stamp;

            *hframe = (H4FrameHandle_t) (pong_h264_orig_frame);
        }

        // Ask for a new frame to be prepared for encoding.
        h264_orig_ping = !h264_orig_ping;
        pthread_mutex_lock(&h264FrameReadyLock);
        h264_frame_ready = FALSE;
        pthread_mutex_unlock(&h264FrameReadyLock);
        return _H4_OK;
        break;
    case _H4_FRAME_GET_EMPTY:
        //never enter on this branch
        printf("__H4_FRAME_GET_EMPTY error \n");
        return _H4_FATAL_ERR;
    case _H4_FRAME_READY:
        //never enter on this branch
        printf("_H4_FRAME_READY error \n");
        return _H4_FATAL_ERR;
    case _H4_FRAME_REF_INC:
        return _H4_OK;
        p_frame = (AppFrame_t*) (*hframe);
        if (NULL != p_frame)
        {
            p_frame->references++;
            return _H4_OK;
        }
        else
        {
            printf("_H4_FATAL_ERR inc\n");
            return _H4_FATAL_ERR;
        }
    case _H4_FRAME_REF_DEC:
        return _H4_OK;
        p_frame = (AppFrame_t *) (*hframe);
        if ((p_frame != NULL) && (p_frame->references > 0))
        {
            p_frame->references--;
            return _H4_OK;
        }
        else
        {
            printf("_H4_FATAL_ERR dec\n");
            return _H4_FATAL_ERR;
        }
    }

  return _H4_OK;
}

void* H264StartEncoder(void *user_cfg)
{
  H4EncoderUserConfig_t *p_enc_cfg = (H4EncoderUserConfig_t *)user_cfg;
  s32                   frame_time_stamp;                 // Time stamp of the next frame to be encoded.
  u32                   init_first_nalu_to_write = TRUE;

  rtems_object_set_name(RTEMS_SELF, "H264_Encoder");
  

  hOSALHandle = (H4OSALHandle_t)H4EncoderSetHWResourcesConfiguration();
  // Open the H264 encoder instance
  H4EncodeOpen(&instance, p_enc_cfg, EncNALUCallback, EncFrameCallback, hOSALHandle, &ret_code);
  if (ret_code != _H4_OK)
  {
    printf("\nError 0x%lx in H4EncodeOpen\n", ret_code);
    pthread_exit(NULL);
  }

  /* Init the encoder status */
  enc_status.encoded_picture_count = 0;
  enc_status.request_frame_type    = _H4_FRAME_UNDEFINED;
  enc_status.request_bitrate       = _H4_NO_BITRATE_REQUEST;
  enc_status.request_qp            = _H4_NO_QP_REQUEST;
  enc_status.intra_refresh_period  = _H4_INTRA_REFRESH_OFF;
  enc_status.global_motion_x       = _H4_GLOBAL_MOTION_NA;
  enc_status.global_motion_y       = _H4_GLOBAL_MOTION_NA;

  // If the encoder instance was opened _H4_OK? Start encoding loop.
  while (ret_code == _H4_OK
      || ret_code == _H4_FRAME_NO_DATA_YET
      //|| ret_code == _H4_BITSTREAM_NO_DATA_YET
      || ret_code == _H4_FRAME_NOT_FOUND
      && (p_enc_cfg->no_frames > encodedFrames))
  {
    if (started <= 0)
    {
        break;
    }
    if (h264_frame_ready)
        {
            if (h264_orig_ping)
                frame_time_stamp = ping_h264_orig_frame->time_stamp;
            else
                // pong
                frame_time_stamp = pong_h264_orig_frame->time_stamp;

            // Do we need to set the time stamp for the first nalu to write?
            if (init_first_nalu_to_write)
            {
                // Set the time stamp.
                first_nalu_to_write_time_stamp = frame_time_stamp;
                // Mark it as set.
                init_first_nalu_to_write = FALSE;
            }

            // If we are still recording, increase the time stamp of the last nalu to write.
            last_nalu_to_write_time_stamp = frame_time_stamp;

            // Start encoding the frame.
            encodedFrames++;
            H4EncodeFrame(instance, frame_time_stamp, &enc_status,
                          &ret_code);
            // printf("encode frame %d\n", encodedFrames);
        }
        else
        {
            // If there isn't a new frame ready, give the processor time to other thread.
            //sched_yield();
            rtems_task_wake_after(1); // FIXME: find better mechanism
        }
  }
  /* Free encoder structures */
  H4EncodeClose(instance, &ret_code);
  if (ret_code != _H4_OK)
  {
    printf("\nError 0x%x H4EncodeClose\n", (unsigned int)ret_code);
    pthread_exit(NULL);
  }

  printf("H264Encoder exit\n");
  // Just to be sure we exit properly.
  pthread_exit(NULL);
  return 0;
}

s32 H264Init()
{
    s32 rc;
    pthread_attr_t attr;
    printf("Initialize nalu buffers\n");
    InitNALUBuffers();

    h264FrameReadyLock = PTHREAD_MUTEX_INITIALIZER;

    // Index of the next nalu buffer to be written by the h264 encoder.
    next_nalu_index_get = 0;
    next_nalu_index_rel = 0;
    next_nalu_to_consume = 0;
    h264_frame_ready = FALSE;
    h264_orig_ping = FALSE;
    // Marking the first and last nalu to write when in the recording state.
    first_nalu_to_write_time_stamp = 0;
    last_nalu_to_write_time_stamp = 0;
    // Index of the next nalu buffer to be written by the h264 encoder.
    next_nalu_index = 0;
    h264OutBuffInd =0 ;
    naluInUse = 0;
    encodedFrames = 0;

    started = 1;
    // rc = OsDrvSvuInit();
    // if(rc != OS_MYR_DRV_ALREADY_INITIALIZED
    //     && rc != OS_MYR_DRV_SUCCESS)
    // {
    //     printf("H264 error shave init: %d \n", rc);
    // }
#ifndef READSENSE_LOGIC
    memset(h264OutputBuffer, 0xFF, H264_MAX_BUFF_SIZE);
#endif    
    if (pthread_attr_init(&attr) != 0)
    {
        printf("pthread_attr_init error");
        started = 0;
        return -1;
    }

    if (pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED) != 0)
    {
        printf("pthread_attr_setinheritsched error");
        started = 0;
        return -2;
    }

    if (pthread_attr_setschedpolicy(&attr, SCHED_RR) != 0)
    {
        printf("pthread_attr_setschedpolicy error");
        started = 0;
        return -3;
    }

    if ((rc = pthread_create(&thread_nalu_consumer, &attr, &NALUConsumer, NULL)))
    {
        printf("Thread NALUConsumer() thread creation failed: %d\n", (int) rc);
        started = 0;
        return -4;
    }
    else
    {
        printf("Thread NALUConsumer() created\n");
    }

    if ((rc = pthread_create(&thread_h264, &attr, &H264StartEncoder, &enc_cfg)))
    {
        printf("Thread H264StartEncoder() thread creation failed: %d\n",
               (int) rc);
        started = 0;
        return -5;
    }
    else
    {
        printf("Thread H264StartEncoder() created\n");
    }

    return 0;
}

s32 H264UnInit()
{
    started = 0;
    void *status;
    pthread_join(thread_nalu_consumer, &status);
    pthread_join(thread_h264, &status);
    return 0;
}

int H264_SendNewFrame2(uint8_t * data, int width, int height)
{
    uint8_t* buffPtrP0 = data;
    uint8_t* buffPtrP1 = data + width*height;
    uint8_t* buffPtrP2 = data + width*height + width*height/4;
    
    if (h264_frame_ready)
    {   // last frame is not decoded
        return -1;
    }
    
    static u32 timeStampDebug  = 0;
    if (h264_orig_ping)
    {
       ping_h264_orig_frame->y = buffPtrP0;
       ping_h264_orig_frame->u = buffPtrP1;
       ping_h264_orig_frame->v = buffPtrP2;
       ping_h264_orig_frame->width = width;
       ping_h264_orig_frame->height = height;
       ping_h264_orig_frame->stride_y = width;
       ping_h264_orig_frame->stride_uv = width/2;
       ping_h264_orig_frame->time_stamp = timeStampDebug;
    }
    else
    {
       pong_h264_orig_frame->y = buffPtrP0;
       pong_h264_orig_frame->u = buffPtrP1;
       pong_h264_orig_frame->v = buffPtrP2;
       pong_h264_orig_frame->width = width;
       pong_h264_orig_frame->height = height;
       pong_h264_orig_frame->stride_y = width;
       pong_h264_orig_frame->stride_uv = width/2;
       pong_h264_orig_frame->time_stamp = timeStampDebug;
    }
    pthread_mutex_lock(&h264FrameReadyLock);
    h264_frame_ready = TRUE;
    pthread_mutex_unlock(&h264FrameReadyLock);
    timeStampDebug++;

    return 0;
}


int H264_SendNewFrame(FrameT *frame)
{
    uint8_t* buffPtrP0 = frame->fbPtr[0];
    uint8_t* buffPtrP1 = frame->fbPtr[1];
    uint8_t* buffPtrP2 = frame->fbPtr[1]+frame->tSize[1]/2;

    if (h264_frame_ready)
    {   // last frame is not decoded
        return -1;
    }

    static u32 timeStampDebug  = 0;
    if (h264_orig_ping)
    {
       ping_h264_orig_frame->y = buffPtrP0;
       ping_h264_orig_frame->u = buffPtrP1;
       ping_h264_orig_frame->v = buffPtrP2;
       ping_h264_orig_frame->width = frame->width[0];
       ping_h264_orig_frame->height = frame->height[0];
       ping_h264_orig_frame->stride_y = frame->stride[0];
       ping_h264_orig_frame->stride_uv = frame->stride[1]/2;
       ping_h264_orig_frame->time_stamp = timeStampDebug;
    }
    else
    {
       pong_h264_orig_frame->y = buffPtrP0;
       pong_h264_orig_frame->u = buffPtrP1;
       pong_h264_orig_frame->v = buffPtrP2;
       pong_h264_orig_frame->width = frame->width[0];
       pong_h264_orig_frame->height = frame->height[0];
       pong_h264_orig_frame->stride_y = frame->stride[0];
       pong_h264_orig_frame->stride_uv = frame->stride[1]/2;
       pong_h264_orig_frame->time_stamp = timeStampDebug;
    }
    pthread_mutex_lock(&h264FrameReadyLock);
    h264_frame_ready = TRUE;
    pthread_mutex_unlock(&h264FrameReadyLock);
    timeStampDebug++;

    return 0;
}

int H264_ReadyToSend()
{
    return h264_frame_ready == FALSE;
}