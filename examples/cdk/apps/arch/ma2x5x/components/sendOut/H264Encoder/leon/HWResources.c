///
/// @file
/// @copyright CodecArt Alliance
///            2015. All rights reserved
///
/// @brief     HW resources configuration functions.
///
///
#include <string.h>
#include "Defines.h"
#include "H4Portab.h"
#include "HWResources.h"
#include "swcShaveLoader.h"
#include "swcCdma.h"

// 2:  Source Specific #defines and types  (typedef, enum, struct)
/* Main DDR memory used in H4CodecLib library */
/* For internal DM of reconstructed frames */
#define DDR_ENC_GLOBAL_MEMORY_SIZE (12*1024*1024)
#define CMX_ENC_GLOBAL_MEMORY_SIZE ( 100*1024)

uint8_t CMX_DMA cmxBuffer[CMX_ENC_GLOBAL_MEMORY_SIZE];

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
extern swcShaveUnit_t CMX_DMA  H4shaveList[MAX_ENCODER_SHAVES_USED];

H4Char DDR_BSS ALIGNED(16) globalEncoderMemoryDDR[DDR_ENC_GLOBAL_MEMORY_SIZE];

H4OSALMemPool_t      CMX_DMA  memInfo[MEMORY_TYPES_NR];
H4OSALMemManager_t   CMX_DMA  memManager = { memInfo, {0, }, 0, 0, 0, 0};
H4UInt32             CMX_DMA  dmaEncoderIDList[MAX_ENCODER_SHAVES_USED];
dmaTransactionList_t CMX_DMA         dmaEncoderListTransaction[MAX_ENCODER_SHAVES_USED*DMA_TRANSACTIONS_NR_PER_PROCESSOR];
H4HWResourcesCfg_t   CMX_DMA  hwResourcesCfg;

H4Void* H4EncoderSetHWResourcesConfiguration()
{
  H4Int32 i;

  /* Send the SHAVE list to OSAL */
  hwResourcesCfg.pShavesList   = (H4UInt32*)H4shaveList;
  hwResourcesCfg.maxShavesUsed = MAX_ENCODER_SHAVES_USED;

  /* Send the MUTEX list to OSAL */
  /* NOTE: in the current OSAL there is only one MUTEX used */
  hwResourcesCfg.mutexNr = 0;

  /* Send the DMA id list to OSAL */
  hwResourcesCfg.pDmaIDList = dmaEncoderIDList;

  /* Send the DMA transaction list to OSAL */
  hwResourcesCfg.pDmaTransactionList = dmaEncoderListTransaction;

  /* Memory configuration */
  hwResourcesCfg.pMemManager = &memManager;
  memManager.useOneCMXMemBlock = 1;

  /* Set to 0 all the members */
  for(i = 0; i < MEMORY_TYPES_NR; i++)
  {
    memInfo[i].p_mem_address = NULL;
    memInfo[i].mem_curr_size = 0;
    memInfo[i].mem_max_size  = 0;
  }

  memInfo[DDR_MEMORY_TYPE].p_mem_address = globalEncoderMemoryDDR;
  memInfo[DDR_MEMORY_TYPE].mem_max_size  = DDR_ENC_GLOBAL_MEMORY_SIZE;

  /* NOTE: The cache coherency is an important issue that should be managed by application.
   * The recommendation is to use SHAVE CMX data and CMX shared data between LEON and SHAVE as uncacheable memory.
   * Also, the LEON data cache should be configured as write-through cache memory to avoid any other possible cache-coherency issues.
   */
  memInfo[CMX0_MEMORY_TYPE].p_mem_address = (H4Char*)(cmxBuffer);
  memInfo[CMX0_MEMORY_TYPE].mem_max_size  = sizeof(cmxBuffer);

  return (H4Void*)&hwResourcesCfg;
}

