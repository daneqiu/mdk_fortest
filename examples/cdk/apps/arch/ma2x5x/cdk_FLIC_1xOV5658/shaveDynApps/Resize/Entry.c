///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect code
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <mv_types.h>
#include <math.h>
#include <stdio.h>
#include <svuCommonShave.h>
#include <swcCdma.h>
#include <swcWhoAmI.h>

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// 4: Static Local Data
// dmaTransactionList_t __attribute__((section(".cmx.cdmaDescriptors"))) *ref1,*ref2;

// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------
void __attribute__((section(".text.entrypoint"))) __attribute__((aligned(16)))
Entry(u8* input, u8* output, int *src_w, int *src_h, int *dst_w, int *dst_h, int *pixSize)
{
         float cbufy0, cbufy1;
	 float cbufx0, cbufx1;
	 int startX, startY;
	 float sv, su;
	 int i, j;
	 int widthStart = 0;
	 int heightStart = 0;
	 int widthEnd = (*src_w);
	 int heightEnd = (*src_h);
	 //int srcWidth = inPut->nWidth;
	 //int srcHeight = inPut->nHeight;
	 int srcPitch = (*src_w)*(*pixSize);
	 int dstWidth = (*dst_w);
	 int dstHeight = (*dst_h);
	 int dstPitch = (*dst_w) * (*pixSize);
         printf("widthStart %d, widthEnd %d, heightStart %d, heightEnd %d\n", widthStart, widthEnd, heightStart, heightEnd);
         printf("srcPitch %d, dstPitch %d, dstWidth %d\n", srcPitch, dstPitch, dstWidth);
	 float xScale = (widthEnd-widthStart)/(float)dstWidth;
	 float yScale = (heightEnd-heightStart)/(float)dstHeight;
	 unsigned char* inputFrame = input;
	 unsigned char* outputFrame = output;
 
	 for(j=0; j<dstHeight; j++) {
		 startY = floor((j+0.5)*yScale-0.5);
		 su = (j+0.5)*yScale-0.5-startY;
		 if(startY<0){
		 su=0,startY=0;
		 }
		 if(startY>=heightEnd-heightStart-1){
		 su=0,startY=heightEnd-heightStart-2;
		 }
 
		 startY = startY+heightStart;
		 cbufy0 = 1.0f-su;
		 cbufy1 = su;
 
		 for(i=0; i<dstWidth*(*pixSize) ; i++) {
			 startX = floor((i/(*pixSize)+0.5)*xScale-0.5);
			 sv = (i/(*pixSize)+0.5)*xScale-0.5-startX;
			 if(startX<0) {
				 sv=0,startX=0;
			 }
			 if(startX>=widthEnd-widthStart-1) {
				 sv=0,startX=widthEnd-widthStart-2;
			 }
 
			 startX = startX+widthStart;
			 cbufx0 = 1.0f-sv;
			 cbufx1 = sv;
 
			 float fvalue = (float)(inputFrame[startY*srcPitch+startX*(*pixSize)+i%(*pixSize)]*cbufy0*cbufx0+
			 inputFrame[(startY+1)*srcPitch+startX*(*pixSize)+i%(*pixSize)]*cbufy1*cbufx0+
			 inputFrame[startY*srcPitch+(startX+1)*(*pixSize)+i%(*pixSize)]*cbufy0*cbufx1+
			 inputFrame[(startY+1)*srcPitch+(startX+1)*(*pixSize)+i%(*pixSize)]*cbufy1*cbufx1);
			 outputFrame[j*dstPitch+i] = (unsigned char)(fvalue);
		 }
	 }
 
    SHAVE_HALT;
    return;
}

