///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect code
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <mv_types.h>
#include <stdio.h>
#include <svuCommonShave.h>
#include <swcCdma.h>
#include <swcWhoAmI.h>

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
// 3: Global Data (Only if absolutely necessary)
static float YUV2RGB_CONVERT_MATRIX[3][3] = { { 1, 0, 1.4022 }, 
	                                      { 1, -0.3456, -0.7145 }, 
	                                      { 1, 1.771, 0 } 
                                            };
// ----------------------------------------------------------------------------
// 4: Static Local Data
// dmaTransactionList_t __attribute__((section(".cmx.cdmaDescriptors"))) *ref1,*ref2;

// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------
void __attribute__((section(".text.entrypoint"))) __attribute__((aligned(16)))
Entry(u8* input, u8* output, int *w, int *h)
{
    int width = *w;
    int height = *h;
    printf("%d x %d\n", width, height);
    int uIndex = width * height;
    int vIndex = uIndex + ((width * height) >> 2);
//           int gIndex = width * height;
//           int rIndex = gIndex * 2;  //Modify for BGR format
//           int bIndex = gIndex * 2;

    int temp = 0;

    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            // R
            temp = (int)(input[y * width + x] + (input[vIndex + (y / 2) * (width / 2) + x / 2] - 128) * YUV2RGB_CONVERT_MATRIX[0][2]);
            output[3*(y * width + x) + 2] = (unsigned char)(temp < 0 ? 0 : (temp > 255 ? 255 : temp));

            // G
            temp = (int)(input[y * width + x] + (input[uIndex + (y / 2) * (width / 2) + x / 2] - 128) * YUV2RGB_CONVERT_MATRIX[1][1] + (input[vIndex + (y / 2) * (width / 2) + x / 2] - 128) * YUV2RGB_CONVERT_MATRIX[1][2]);
            output[3*(y * width + x) + 1] = (unsigned char)(temp < 0 ? 0 : (temp > 255 ? 255 : temp));

            // B
            temp = (int)(input[y * width + x] + (input[uIndex + (y / 2) * (width / 2) + x / 2] - 128) * YUV2RGB_CONVERT_MATRIX[2][1]);
            output[3*(y * width + x)+ 0] = (unsigned char)(temp < 0 ? 0 : (temp > 255 ? 255 : temp));
        }
    }
    SHAVE_HALT;
    return;
}

