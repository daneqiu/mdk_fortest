/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <stdint.h>
// #include <version_info.h>

#include "rtems.h"
#ifdef _REPORT_CPU_USAGE
#include "rtems/cpuuse.h"
#endif

#include "initSystem.h"

#include "LeonIPCApi.h"
#include "DrvIcb.h"

extern leonIPCChannel_t lrt_LRTtoLOSChannel;

/*
 * ****************************************************************************
 * ** Main ********************************************************************
 * ****************************************************************************
 */
int main(int argc, char **argv)
{
    UNUSED(argc);
    UNUSED(argv);
    rtems_object_set_name(RTEMS_SELF, "main");
    initSystem();

	int status = -1;
	u32 msg;
	u32 msgNo;

	while (status != IPC_SUCCESS) {
		status = LeonIPCRxInit(&lrt_LRTtoLOSChannel, NULL, IRQ_DYNAMIC_5, 5);
	}
	printf("[LeonIPCRxInit] done\n");

    while(1)
    {

#ifdef _REPORT_CPU_USAGE
        sleep(3);
        rtems_cpu_usage_report();
        rtems_cpu_usage_reset ();
#endif

		LeonIPCNumberOfPendingMessages(&lrt_LRTtoLOSChannel, &msgNo);
		while (msgNo > 0) {
			status = LeonIPCReadMessage(&lrt_LRTtoLOSChannel, &msg);
			if (status == IPC_SUCCESS) {
				printf("[LeonIPCReadMessage]: msg=0x%lx\n", msg);
				msgNo--;
			}
		}
    }
    return 0;
}
