/******************************************************************************

 @File         : initSystem.c
 @Author       : MT
 @Brief        : IPIPE Hw configuration on Los side
 Date          : 02 - Sep - 2014
 E-mail        : xxx.xx@movidius.com
 Copyright     : � Movidius Srl 2013, � Movidius Ltd 2014

 Description :


******************************************************************************/


/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include <OsDrvCpr.h>
#include <DrvGpio.h>
#include <OsDrvTimer.h>
#include <DrvShaveL2Cache.h>
#include "initSystem.h"
#include <brdDefines.h>
#include <OsDrvCmxDma.h>
#include <OsDrvSvu.h>
#include <OsDrvCmxDma.h>
#include <DrvDdr.h>
#include <OsDrvShaveL2Cache.h>
#include "brdGpioCfgs/brdMv0182GpioDefaults.h"


/**************************************************************************************************
 ~~~  Specific #defines and types (typedef,enum,struct)
**************************************************************************************************/
#define CMX_CONFIG_SLICE_7_0       (0x11111111)
#define CMX_CONFIG_SLICE_15_8      (0x11111111)
#define L2CACHE_CFG                (SHAVE_L2CACHE_NORMAL_MODE)

// #define CLOCKS_MIPICFG (AUX_CLK_MASK_MIPI_ECFG | AUX_CLK_MASK_MIPI_CFG)
// #define REM_CLOCKS        (    AUX_CLK_MASK_I2S0  |     \
//                                 AUX_CLK_MASK_I2S1  |     \
//                                 AUX_CLK_MASK_I2S2  )

// #define DESIRED_USB_FREQ_KHZ        (20000)
// #if (DEFAULT_APP_CLOCK_KHZ%DESIRED_USB_FREQ_KHZ)
// #error "Can not achieve USB Reference frequency. Aborting."
// #endif

// #define SHAVES_USED                (12)

/**************************************************************************************************
 ~~~  Global Data (Only if absolutely necessary)
**************************************************************************************************/
CmxRamLayoutCfgType __attribute__((section(".cmx.ctrl"))) __cmx_config = {
        CMX_CONFIG_SLICE_7_0, CMX_CONFIG_SLICE_15_8};

/**************************************************************************************************
 ~~~  Static Local Data
 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Functions Implementation
**************************************************************************************************/

// assumed host will update all this parameters
void initSystem_lrt(void)
{
    OsDrvCprInit();
    OsDrvCprOpen();
    OsDrvTimerInit();
    OsDrvSvuInit();
    // Set the shave L2 Cache mode
    OsDrvShaveL2CacheInit(L2CACHE_CFG);

    // DrvGpioIrqResetAll();
    // DrvGpioInitialiseRange(brdMV0182GpioCfgDefault);
}

