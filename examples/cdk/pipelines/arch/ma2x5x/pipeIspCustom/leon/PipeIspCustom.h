///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Create and Destroy camera flic base isp pipeline interface.
///
///
///


#ifndef _PIPE_ISP_CUSTOM_H
#define _PIPE_ISP_CUSTOM_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef CUSTOM_FLIC_PRIORITY
#define CUSTOM_FLIC_PRIORITY 211
#endif

#ifndef FLIC_DEFAULT_PTHREAD_PRIO_CFG	
#define FLIC_DEFAULT_PTHREAD_PRIO_CFG	(150)
#endif	

#ifndef N_POOL_FRMS
#define N_POOL_FRMS 3
#endif

#ifndef N_POOL_FRMS_SRC
#define N_POOL_FRMS_SRC 4
#endif

// by changing this define this pipeline can run any number of cams.
// it can run and any number of cams smaller that this
#ifndef APP_NR_OF_CAMS
#define APP_NR_OF_CAMS 1
#endif

typedef int (*GetSrcSzLimits)(uint32_t srcId, icSourceSetup* srcSet);

/// Create 1 camera flic base isp pipeline interface.
void pipeIspSingleCreate(GetSrcSzLimits getSrcSzLimits);

///Destroy 1 camera flic base isp pipeline interface.
void pipeIspSingleDestroy(void);


#ifdef __cplusplus
}
#endif

#endif
