#include <Opipe.h>

#include <Flic.h>
#include <MemAllocator.h>

#include <Pool.h>
#include "PlgSource.h"
#include "PlgIspCtrl.h"
#include "PlgIspProc.h"
#include "PlgOutItf.h"
#include "PipeIspCustom.h"

PlgSource               plgSrc         SECTION(".cmx_direct.data");     //Source
PlgIspCtrl              *plgIspCtrl     SECTION(".cmx_direct.data") ; //cameraConfig interface to flic message
PlgIspProc              plgIsp         SECTION(".cmx_direct.data");     //ISP
PlgPool<ImgFrame>       plgPoolSrc     SECTION(".cmx_direct.data"); //source out pool
PlgPool<ImgFrame>       plgPoolIsp     SECTION(".cmx_direct.data"); //isp    out pool
PlgOutItf               plgOut         SECTION(".cmx_direct.data");     //out
Pipeline                p              SECTION(".cmx_direct.data");


void pipeIspSingleCreate(GetSrcSzLimits getSrcSzLimits) {
    icSourceSetup srcSet;

    OpipeReset();

    //Create plugins
    RgnAlloc.Create(RgnBuff, DEF_POOL_SZ);
    plgIspCtrl = PlgIspCtrl::instance();
    plgIspCtrl->Create();
    plgIspCtrl->schParam.sched_priority = FLIC_DEFAULT_PTHREAD_PRIO_CFG;

    plgSrc    .Create(IC_SOURCE_0);
    plgSrc    .outFmt = SIPP_FMT_16BIT;
    plgSrc.schParam.sched_priority = FLIC_DEFAULT_PTHREAD_PRIO_CFG;
    
    if(0 == getSrcSzLimits(IC_SOURCE_0, &srcSet)) {
        icSourceSetup* srcLimits = &srcSet;
        srcLimits->maxBpp = 16;
        plgPoolSrc.Create(&RgnAlloc, N_POOL_FRMS_SRC, ((srcLimits->maxPixels * srcLimits->maxBpp)) >> 3); //RAW
        plgPoolIsp.Create(&RgnAlloc, N_POOL_FRMS, (srcLimits->maxPixels * 3)>>1); //YUV420
    }
    else {
        // not define max size for initialized camera
        assert(0);
    }
    
    plgIsp.Create(IC_SOURCE_0, srcSet.appSpecificInfo);
    plgIsp.schParam.sched_priority = FLIC_DEFAULT_PTHREAD_PRIO_CFG;

    plgOut    .Create();
    plgOut.schParam.sched_priority = FLIC_DEFAULT_PTHREAD_PRIO_CFG;

    p.Add(plgIspCtrl);
    p.Add(&plgOut);
    p.Add(&plgSrc);
    p.Add(&plgIsp);
    p.Add(&plgPoolSrc);
    p.Add(&plgPoolIsp);

    plgPoolSrc .out                       .Link(&plgSrc.inO);
    plgSrc     .outCommand                .Link(&plgIspCtrl->inCtrlResponse);
    plgIspCtrl->outSrcCommand[IC_SOURCE_0].Link(&plgSrc.inCommand);
    plgPoolIsp .out                       .Link(&plgIsp.inO);
    plgSrc     .out                       .Link(&plgIsp.inI);
    plgIsp     .outF                      .Link(&plgOut.in );
    plgIsp     .outE                      .Link(&plgIspCtrl->inCtrlResponse);
    plgOut     .outCmd                    .Link(&plgIspCtrl->inCtrlResponse);
    plgIspCtrl->outOutCmd                 .Link(&plgOut.inCmd);
    p.Start();
}

void pipeIspSingleDestroy(void) {
    p.Stop();
    p.Wait();
    p.Delete();
    RgnAlloc.Delete();
}
