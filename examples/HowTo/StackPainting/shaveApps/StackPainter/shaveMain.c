///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>

#include <mv_types.h>
#include <svuCommonShave.h>

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------

int Entry(u32 *stackStart, u32 *testPattern, u32 fillSize)
{
    u32 *stackTop = stackStart;
    u32 *stackEnd = stackStart - fillSize;

    while (stackEnd != stackTop)
    {
        *stackTop-- = *testPattern++;
    }

    SHAVE_HALT;
    return 0;
}
