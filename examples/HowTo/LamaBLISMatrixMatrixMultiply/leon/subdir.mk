VERS_STR = \\\"0.1.0-34\\\"

ccopt-los-y+= -DMYRIAD2
ccopt-los-y+= -DBLIS
ccopt-los-y+= -DBLIS_VERSION_STRING=$(VERS_STR)
ccopt-los-y+= -DVERSION=BLIS_VERSION_STRING=$(VERS_STR) 
ccopt-los-y+= -DLEON_USE_REAL_NUMBERS_ONLY
include-dirs-los-y+= $(MV_COMMON_BASE)/components/BLIS/leon/config/reference
include-dirs-los-y+= $(APPDIR)/shared
include-dirs-los-y+= $(APPDIR)/leon
srcs-los-y += Helpers.c \
  app_config.c \
  main.c
