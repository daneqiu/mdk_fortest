/*
 * nopGenerator.cpp
 *
 *  Created on: Feb 29, 2016
 *      Author: ovidiuandoniu
 */

#include "mv_types.h"
// doing nothing with grace

template <int howManyTimes>
inline __attribute__((always_inline)) __attribute__((no_instrument_function)) void nop(void) {
	asm volatile ("nop");
	nop<howManyTimes-1>();
}

template<>
inline __attribute__((always_inline)) __attribute__((no_instrument_function)) void nop<0>(void) { }

extern "C"
void exquisiteWorkOf(unsigned count) {
	for (unsigned i=0; i<count; ++i) {
		nop<20>();
	}
}

inline __attribute__((always_inline))
void escape(void* p) {
	asm volatile("" : : "g"(p) : "memory");
}

inline __attribute__((always_inline))
void clobber(void*) {
	asm volatile("" : : : "memory");
}
