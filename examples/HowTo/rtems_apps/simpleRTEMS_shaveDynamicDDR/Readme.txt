simpleRTEMS_shaveDynamic

Supported Platform
==================
Myriad2 - This example works on Myriad2: ma2150, ma2450 silicon and
ma2150, ma2450 simulator

Overview
==========
LeonOS running RTEMS loads and starts SHAVE binary dynamically.

Software description
=======================
This application starts two RTEMS threads with same thread function and start SHAVE entry function

Hardware needed
==================
Myriad2 -This software should run on MV182 or MV212 boards.

Build
==================
Please type "make help" if you want to learn available targets.

!!!Before cross-compiling make sure you do a "make clean"!!!

Myriad2 - To build the project please type:
     - "make clean"
     - "make all MV_SOC_REV={Myriad_version}"

Where {Myriad_version} may be ma2150 or ma2450.
The default Myriad revision in this project is ma2450 so it is not necessary
to specify the MV_SOC_REV in the terminal.

Setup
==================
Myriad2 simulator - To run the application:
    - open terminal and type "make start_simulator MV_SOC_REV={Myriad_version}"
    - open another terminal and type "make debug MV_SOC_REV={Myriad_version}"
Myriad2 silicon - To run the application:
    - open terminal and type "make start_server"
    - open another terminal and type "make debug MV_SOC_REV={Myriad_version}"

Where {Myriad_version} may be ma2150 or ma2450.
The default Myriad revision in this project is ma2450 so it is not necessary
to specify the MV_SOC_REV in the terminal.

Expected output
==================
The application output should be similar to:

UART:
UART: RTEMS POSIX
UART: Started
UART: Add thread created
UART: Sub thread created
UART: S0 Data set 1 at adress 0x80000000 : 4 25 313 13
UART: S2 Data set 1 at adress 0x80040000 : 4 25 313 13
UART: S0 Data set 2 at adress 0x80000020 : 4 30 400 500
UART: S2 Data set 2 at adress 0x80040020 : 4 30 400 500
UART: S0 Start at addr:0x1D000010
UART: S2 Start at addr:0x1D000010
UART: S1 Data set 1 at adress 0x80020000 : 4 25 313 13
UART: S3 Data set 1 at adress 0x80060000 : 4 25 313 13
UART: S1 Data set 2 at adress 0x80020020 : 4 30 400 500
UART: S3 Data set 2 at adress 0x80060020 : 4 30 400 500
UART: S1 Start at addr:0x1D000010
UART: S3 Start at addr:0x1D000010
UART: S 0 res from adress 0x80000050: 8 55 713 513
UART: S 2 res from adress 0x80040050: 0 -5 -87 -487
UART: S 1 res from adress 0x80020050: 8 55 713 513
UART: S 3 res from adress 0x80060050: 0 -5 -87 -487
LOS: LeonOS (P0:ALOS) suspended at 0x70195104 (Application terminated successfully)

