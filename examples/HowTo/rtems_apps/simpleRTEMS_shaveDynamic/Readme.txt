simpleRTEMS_shaveDynamic

Supported Platform
==================
Myriad2 - This example works on Myriad2: ma2150, ma2450 silicon and
ma2150, ma2450 simulator

Overview
==========
LeonOS running RTEMS loads and starts SHAVE binary dynamically.

Software description
=======================
This application starts two RTEMS threads with same thread function and start SHAVE entry function

Hardware needed
==================
Myriad2 -This software should run on MV182 or MV212 boards.

Build
==================
Please type "make help" if you want to learn available targets.

!!!Before cross-compiling make sure you do a "make clean"!!!

Myriad2 - To build the project please type:
     - "make clean"
     - "make all MV_SOC_REV={Myriad_version}"

Where {Myriad_version} may be ma2150 or ma2450.
The default Myriad revision in this project is ma2450 so it is not necessary
to specify the MV_SOC_REV in the terminal.

Setup
==================
Myriad2 simulator - To run the application:
    - open terminal and type "make start_simulator MV_SOC_REV={Myriad_version}"
    - open another terminal and type "make debug MV_SOC_REV={Myriad_version}"
Myriad2 silicon - To run the application:
    - open terminal and type "make start_server"
    - open another terminal and type "make debug MV_SOC_REV={Myriad_version}"

Where {Myriad_version} may be ma2150 or ma2450.
The default Myriad revision in this project is ma2450 so it is not necessary
to specify the MV_SOC_REV in the terminal.

Expected output
==================
The application output should be similar to:

UART:
UART: RTEMS POSIX Started
UART: Thread 1 created
UART: Data set 1 : 4 25 313 13
UART: Data set 2 : 4 30 400 500
UART: Start Shave at addr:0x1D000010
UART: res  : 8 55 713 513
UART: Thread 2 created
UART: Data set 1 : 4 25 313 13
UART: Data set 2 : 4 30 400 500
UART: Start Shave at addr:0x1D000010
UART: res  : 0 -5 -87 -487
LOS: Application terminated successfully.
