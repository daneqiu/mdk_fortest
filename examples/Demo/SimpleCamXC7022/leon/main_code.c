///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief
///

// 1: Includes
// ----------------------------------------------------------------------------

#include "stdio.h"
#include "string.h"
#include "time.h"

#include "app_config.h"
#ifdef MV0212
#include "MV0212.h"
#include "brdGpioCfgs/brdMv0182R5GpioDefaults.h"
#elif defined(MV0182)
#include <Board182Api.h>
#endif
#include "DrvCDCEL.h"
#include "DrvCpr.h"
#include "DrvGpioDefines.h"
#include "registersMyriad.h"
#include "DrvADV7513.h"
#include "CamGenericApi.h"
#include "LcdApi.h"
#include "DrvMss.h"
#include "DrvRegUtilsDefines.h"

#include "OsDrvTimer.h"
#include "LcdCEA1080p60.h"
#include "XC7022_1920x1080_yuv422.h"
//#include "imx214_2L_2104x1560_Raw10_30Hz_binning.h"

#include "brdMv0182.h"
#include <VcsHooksApi.h>

static u8 camWriteProto8b[]  = {S_ADDR_WR, R_ADDR_H, R_ADDR_L, DATAW, LOOP_MINUS_1};		 //register addr on 16bits, data on 8 bits
static u8 camWriteProto16b[] = {S_ADDR_WR, R_ADDR_H, R_ADDR_L, DATAW, DATAW, LOOP_MINUS_1};  //register addr on 16bits, data on 16 bits


static uint8_t protocolReadSample2[] = I2C_PROTO_READ_16BA;
static uint8_t * protocolWriteSample2 = camWriteProto8b;//camWriteProto8b;


// 2:  Source specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------

#define MAX_USED_BUF             10   //minimum 2 for ping-pong
#define FIRST_INCOMING_BUF_ID    1   //this is in fact 2, because CamGeneric call once the getFrame fc. before start
#define FIRST_OUTGOING_BUF_ID    0

#define CAM_WINDOW_START_COLUMN  0
#define CAM_WINDOW_START_ROW     0   //cut the embedded info of the first 8 lines

#define WINDOW_WIDTH             1920
#define WINDOW_HEIGHT            1080

#define CAM_BPP 2                //RAW10 cut down to 8 bits by the SIPP filter : a simple solution available for this black&white sensor,
                                 //which avoid the usage of a debayer filter
#define LCD_BPP 1                //YUV422p: the planar YUV422p type has 1 bpp for EVERY PLANE (indication to every plane DMAs,
                                 //not an indicator for global storage)

#define CAM_FRAME_SIZE_BYTES     (WINDOW_WIDTH * WINDOW_HEIGHT * 3/2)
#define LCD_CHROMA_SIZE_BYTES   (WINDOW_WIDTH * WINDOW_HEIGHT * LCD_BPP )
#define CAM_OUT_LUMA_SIZE_BYTES    (WINDOW_WIDTH * WINDOW_HEIGHT * LCD_BPP )  //size of a single chroma plane
#define CAM_OUT_CHROMA_SIZE_BYTES    (WINDOW_WIDTH * WINDOW_HEIGHT * LCD_BPP/4 )  //size of a single chroma plane


#define DDR_AREA                 __attribute__((section(".ddr.bss")))

#ifdef MV0212
#define NUM_I2C_DEVS 3
#endif
// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
static I2CM_Device * i2c0Handle;
static I2CM_Device * i2c2Handle;

// 4: Static Local Data
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
GenericCamSpec *camConfig = &XC7022_1920x1080_YUV422_camCfg;
ADV7513ContfigMode_t ADV7513Config = ADV7513_1080P60;


volatile s32 newCamFrameCtr = FIRST_INCOMING_BUF_ID;
volatile s32 oldCamFrameCtr = FIRST_INCOMING_BUF_ID;


u8 DDR_AREA  camBuf[MAX_USED_BUF][CAM_FRAME_SIZE_BYTES];



u8 DDR_AREA  lcdBuf[CAM_OUT_LUMA_SIZE_BYTES];
u8 DDR_AREA  lcdDummyChroma[LCD_CHROMA_SIZE_BYTES];

GenericCameraHandle  camHndl;
CamUserSpec          userConfig;
frameSpec            camFrameSpec;
frameBuffer          camFrame[MAX_USED_BUF];
frameSpec            lcdFrameSpec;
frameBuffer          lcdFrame[MAX_USED_BUF];
LCDHandle            lcdHndl;

callbacksListStruct          callbacks = {0};
interruptsCallbacksListType  isrCallbacks = {0};

//time stamp
u64 time_clk;
u64 time_ns;

// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
frameBuffer* AllocateNextCamFrameBuf(void);

// 6: Functions Implementation
// ----------------------------------------------------------------------------

void prepareDriverData(void)
{
   int ibuf;


   //user extra settings
   userConfig.mipiControllerNb  = CAM_A1_MIPICTRL;//CAM_B1_MIPICTRL;                     //the SIPP camera connected to 1st position of CAMB connector
   userConfig.receiverId        = CIF_DEVICE0;//CIF_DEVICE0;//SIPP_DEVICE1;//CIF_DEVICE0;//                        //
printf("test receiverId %d,(CIF_DEVICE0 %d,SIPP_DEVICE1 %d )\n",userConfig.receiverId,CIF_DEVICE0,SIPP_DEVICE1 );


#ifdef MV0212
   userConfig.sensorResetPin    = MV0212_MV0200_SENSOR_RIGHT_RST_GPIO;
#elif defined(MV0182)
   userConfig.sensorResetPin    = MV0182_MV0200_SENSOR_RIGHT_RST_GPIO;   //
#endif

   //userConfig.stereoPairIndex   = CAM_A_ADDR;                    //
   userConfig.windowColumnStart = CAM_WINDOW_START_COLUMN;
   userConfig.windowRowStart    = CAM_WINDOW_START_ROW;
   userConfig.windowWidth       = WINDOW_WIDTH;
   userConfig.windowHeight      = WINDOW_HEIGHT;
   userConfig.generateSync      = NULL;

   //synchronize local buffers spec with driver configuration
   camFrameSpec.width   = WINDOW_WIDTH;
   camFrameSpec.height  = WINDOW_HEIGHT;
   camFrameSpec.bytesPP = 1;
   camFrameSpec.stride  = WINDOW_WIDTH * 1;
   camFrameSpec.type    = YUV420p;//RAW16;//YUV422p;//YUV422i; //YUV422i

   //prepare output buffers
   for (ibuf = 0; ibuf < MAX_USED_BUF;ibuf++)
   {
      camFrame[ibuf].p1    = (unsigned char*)&camBuf[ibuf][0];
      camFrame[ibuf].p2    = &camBuf[ibuf][WINDOW_WIDTH * WINDOW_HEIGHT];    //use dummy UV planes
      camFrame[ibuf].p3    = &camBuf[ibuf][WINDOW_WIDTH * WINDOW_HEIGHT+WINDOW_WIDTH * WINDOW_HEIGHT/4];    //
      camFrame[ibuf].spec  = camFrameSpec;
   }

   isrCallbacks.getBlock     = NULL;
   isrCallbacks.getFrame     = AllocateNextCamFrameBuf;
   isrCallbacks.notification = NULL;
   callbacks.isrCbfList      = &isrCallbacks;
   callbacks.sensorCbfList   = NULL;

#if 1 //test
   //initialize the LCD data
   lcdFrameSpec.width   = WINDOW_WIDTH;
   lcdFrameSpec.height  = WINDOW_HEIGHT;
   lcdFrameSpec.stride  = WINDOW_WIDTH * LCD_BPP;
   lcdFrameSpec.bytesPP = LCD_BPP;
   lcdFrameSpec.type    = YUV420p;

   for (ibuf = 0; ibuf < MAX_USED_BUF;ibuf++)
   {
       lcdFrame[ibuf].spec = lcdFrameSpec;
       lcdFrame[ibuf].p1   = lcdBuf;  //use for lcd directly the camera buffers, as Y plane
       lcdFrame[ibuf].p2   = lcdDummyChroma;    //use dummy UV planes
       lcdFrame[ibuf].p3   = lcdDummyChroma;    //
   }
#endif

   return;
}


u64 time_ms;
u64 time_ms_last;

frameBuffer* AllocateNextCamFrameBuf(void)
{
	u32 clksPerUs;
	u64 ticks;
	int status;
	rtems_interrupt_level level;

	rtems_interrupt_disable(level);
	status = DrvTimerGetSystemTicks64(&ticks);
	clksPerUs = DrvCprGetSysClocksPerUs();
	rtems_interrupt_enable(level);

	//ms
	time_ms = ticks / clksPerUs / 1000;


	newCamFrameCtr++;
	printf("==== get %d frame in XC7022 ==== ,clk %lld, %lld ms, %d fps\n",newCamFrameCtr,ticks,time_ms,1000/(time_ms-time_ms_last));

	time_ms_last = time_ms;

	return ( &camFrame[newCamFrameCtr % MAX_USED_BUF] );
   
}

frameBuffer* allocateLcdFrame(int layer)
{
    (void) layer;// "use" the variables to hush the compiler warning.

   return ( &lcdFrame[(newCamFrameCtr - 1)  % MAX_USED_BUF] );
}


int main()
{
    s32 status;
    camErrorType camStatus;
    LCDLayerOffset lcdLayerStartOffset = {0, 0};
    s32 boardStatus;
	
	u32 camID	= 0;
	s32 statusI2c=0;
	u8 bytes[2];
	u32 i;

    status = initClocksAndMemory();
    if(status)
        return status;

#ifdef MV0212
    int32_t rc;
    uint32_t rev;
    BoardI2CInfo info[NUM_I2C_DEVS];
    BoardConfigDesc config[] =
    {
        {
            BRDCONFIG_GPIO,
            // use this for the sake of testing as it has the same gpio config as the MV0212 R0
            (void *)brdMV0182R5GpioCfgDefault
        },
        {
            BRDCONFIG_END,
            NULL
        }
    };

    rc = BoardInit(config);
    if (rc!=BRDCONFIG_SUCCESS)
    {
    	printf("Error: board initialization failed with %ld status\n",
    	    			rc);
        return rc;
    }

    rc = BoardGetPCBRevision(&rev);
    if (rc!=BRDCONFIG_SUCCESS)
    {
    	printf("Error: board configuration read failed with %ld status\n",
    	    			rc);
        return rc;
    }
    printf("Board Mv0212 initialized, revision = %lu \n", rev);

    boardStatus = BoardInitExtPll(EXT_PLL_CFG_148_24_24MHZ);
    if (boardStatus != BRDCONFIG_SUCCESS)
    {
    	printf("Error: board initialization failed with %ld status\n",
    			boardStatus);
    	return -1;
    }
    rc = BoardGetI2CInfo(info, NUM_I2C_DEVS);
    i2c0Handle=info[0].handler;
    i2c2Handle=info[2].handler;
#elif defined(MV0182)

    boardStatus = BoardInitialise(EXT_PLL_CFG_148_24_24MHZ);
    i2c0Handle=gAppDevHndls.i2c0Handle;
    i2c2Handle=gAppDevHndls.i2c2Handle;
    if (boardStatus != B_SUCCESS)
    {
    	printf("Error: board initialization failed with %ld status\n",
    			boardStatus);
    	return -1;
    }
#endif
    printf("BoardInitialise Done\n");



	//i2c read xc id
	bytes[0] = 0xff;
	bytes[1] = 0xff;
	statusI2c=DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_RD, XC7022_REG_ID, protocolReadSample2, bytes, 1);
	camID|=bytes[0]<<8;
	statusI2c=DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_RD, XC7022_REG_ID+1, protocolReadSample2, bytes, 1);
	camID|=bytes[0];
	printf("status 0x%x,xc7022 id: 0x%x\n",statusI2c,camID);


    printf("Configuring camera and datapath\n");
    prepareDriverData();

#if 1 //test
    //prefill the common UV buffer of the output YUV422p with constant neutral pattern(luma Y plane will be overwritten by the SIPP DMA);
    //this is acceptable for the imx208 sensor, which output grayscale image, so only luma is valuable, the chroma can be neutral.
    memset(lcdDummyChroma, 0x80, sizeof(lcdDummyChroma));

    //connect the LCD to output parallel bus (by GPIO) instead of MIPI Tx
    DrvMssConnectLcdToGpio();
#endif

    camStatus = CamInit( &camHndl, camConfig, &userConfig, &callbacks, i2c0Handle );
    if (camStatus != CAM_SUCCESS)
    {
        printf("\n Camera configuration failed (%d).\n", camStatus);
        return -1;
    }
    printf("CamInit OK\n");


    camStatus = CamStart( &camHndl );
    if (camStatus != CAM_SUCCESS)
    {
        printf("\n Camera failed to start (%d).\n", camStatus);
        return -1;
    }
    printf("CamStart OK\n");

#if 0 //test
    printf("Configuring the HDMI chip ADV7513\n");
    status = initADV7513reg(i2c2Handle, ADV7513Config);

    if (status != 0)
    {
        printf("\n ADV7513 chip configuration failed with code %ld.\n", status);
        return -1;
    }
#endif

#if 1 //test
    printf("Configuring the LCD\n");
    LCDInit(&lcdHndl, &lcdSpec1080p60, NULL, LCD1);
    LCDInitLayer(&lcdHndl, VL1, &lcdFrameSpec, lcdLayerStartOffset);
    LCDSetupCallbacks(&lcdHndl, &allocateLcdFrame, NULL, NULL, NULL);
    LCDStart(&lcdHndl);
#endif


#if 1
	//i2c wrtite xc 
	//0x36 ffff ff : enable bypass i2c to access 2716
	//DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_WR, 0xFFFd ,protocolWriteSample2, 0x80 , 1);
	//DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_WR, 0xFFFe ,protocolWriteSample2, 0x50 , 1);
	//DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_WR, 0x004d ,protocolWriteSample2, 0x01 , 1);
	for(i=0;i<COUNT_OF(XC7022_bypass_on);i++)
	{
		printf("i2c 0x%x : write addr 0x%x , 0x%x\n",XC7022_I2C_ADDRESS_WR<<1,XC7022_bypass_on[i][0],XC7022_bypass_on[i][1]);
		statusI2c = DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_WR, XC7022_bypass_on[i][0] ,protocolWriteSample2, (u8*)(&XC7022_bypass_on[i][1]) , 1);
		printf("write status %d\n",statusI2c);
		//DrvTimerSleepMs(100);
	}

#if 1	
	//i2c read 2718 id
	statusI2c=DrvI2cMTransaction(i2c0Handle, OV2716_I2C_ADDRESS, OV2716_REG_ID, protocolReadSample2, bytes, 2);
	printf("ov2716 id: 0x%x%x\n",bytes[0],bytes[1]);
	statusI2c=DrvI2cMTransaction(i2c0Handle, OV2716_I2C_ADDRESS, OV2716_REG_ID, protocolReadSample2, bytes, 1);
	printf("0x300a 0x%x\n",bytes[0]);
	statusI2c=DrvI2cMTransaction(i2c0Handle, OV2716_I2C_ADDRESS, OV2716_REG_ID+1, protocolReadSample2, bytes, 1);
	printf("0x300b 0x%x\n",bytes[0]);
	
#endif
	
#if 1
	//i2c write 2718 setting
	//statusI2c=DrvI2cMTransaction(i2c0Handle, OV2716_I2C_ADDRESS, 0xFFFF, protocolWriteSample2, 0xff, 1);
	printf("[write/read/check] ov2718 setting\n");
	for(i=0;i<COUNT_OF(OV2718MIPI_default_regs);i++)
	{
		//printf("i2c 0x%x : write addr 0x%x , 0x%x\n",OV2716_I2C_ADDRESS<<1,OV2718MIPI_default_regs[i][0],OV2718MIPI_default_regs[i][1]);

		//write
		DrvI2cMTransaction(i2c0Handle, OV2716_I2C_ADDRESS, OV2718MIPI_default_regs[i][0], protocolWriteSample2, &OV2718MIPI_default_regs[i][1], 1);
		//read
		statusI2c=DrvI2cMTransaction(i2c0Handle, OV2716_I2C_ADDRESS, OV2718MIPI_default_regs[i][0], protocolReadSample2, bytes, 1);
		//check
		if(bytes[0]!=OV2718MIPI_default_regs[i][1])
		{
			printf("!!!! check error, 0x%x , write 0x%x, read out 0x%x\n",OV2718MIPI_default_regs[i][0],OV2718MIPI_default_regs[i][1],bytes[0]);
		}
	}
#endif


	//i2c wrtite xc 
	//0x36 ffff ff : disable bypass i2c to access 2716
	//DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_WR, 0xFFFd ,protocolWriteSample2, 0x80 , 1);
	//DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_WR, 0xFFFe ,protocolWriteSample2, 0x50 , 1);
	//DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_WR, 0x004d ,protocolWriteSample2, 0x00 , 1);
	for(i=0;i<COUNT_OF(XC7022_bypass_off);i++)
	{
		printf("i2c 0x%x : write addr 0x%x , 0x%x\n",XC7022_I2C_ADDRESS_WR<<1,(XC7022_bypass_off)[i][0],(XC7022_bypass_off)[i][1]);
		DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_WR, (XC7022_bypass_off)[i][0] ,protocolWriteSample2, &(XC7022_bypass_off)[i][1] , 1);
	}
	
#endif

//setting ov2716 setting
#if 0

	//i2c wrtite xc 
	//0x36 ffff ff : enable bypass i2c to access 2716
	//DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_WR, 0xFFFd ,protocolWriteSample2, 0x80 , 1);
	//DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_WR, 0xFFFe ,protocolWriteSample2, 0x50 , 1);
	//DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_WR, 0x004d ,protocolWriteSample2, 0x01 , 1);
	for(i=0;i<COUNT_OF(XC7022_bypass_on);i++)
	{
		printf("i2c 0x%x : write addr 0x%x , 0x%x\n",XC7022_I2C_ADDRESS_WR<<1,XC7022_bypass_on[i][0],XC7022_bypass_on[i][1]);
		DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_WR, XC7022_bypass_on[i][0] ,protocolWriteSample2, &XC7022_bypass_on[i][1] , 1);
	}
#if 1
	//i2c write 2716
	//statusI2c=DrvI2cMTransaction(i2c0Handle, OV2716_I2C_ADDRESS, 0xFFFF, protocolWriteSample2, 0xff, 1);
	for(i=0;i<COUNT_OF(OV2718MIPI_default_regs);i++)
	{
		printf("i2c 0x%x : write addr 0x%x , 0x%x\n",OV2716_I2C_ADDRESS<<1,OV2718MIPI_default_regs[i][0],OV2718MIPI_default_regs[i][1]);
		DrvI2cMTransaction(i2c0Handle, OV2716_I2C_ADDRESS, OV2718MIPI_default_regs[i][0], protocolWriteSample2, &OV2718MIPI_default_regs[i][1], 1);
	}
#endif
	//i2c wrtite xc 
	//0x36 ffff ff : disable bypass i2c to access 2716
	//DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_WR, 0xFFFd ,protocolWriteSample2, 0x80 , 1);
	//DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_WR, 0xFFFe ,protocolWriteSample2, 0x50 , 1);
	//DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_WR, 0x004d ,protocolWriteSample2, 0x00 , 1);
	for(i=0;i<COUNT_OF(XC7022_bypass_off);i++)
	{
		printf("i2c 0x%x : write addr 0x%x , 0x%x\n",XC7022_I2C_ADDRESS_WR<<1,(XC7022_bypass_off)[i][0],(XC7022_bypass_off)[i][1]);
		DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_WR, (XC7022_bypass_off)[i][0] ,protocolWriteSample2, &(XC7022_bypass_off)[i][1] , 1);
	}

#endif



#if 1
	printf("~~~~ colorbar ~~~~\n");
	//XC7022 colorbar
	#if 0
	for(i=0;i<COUNT_OF(XC7022_colorbar_on);i++)
	{
		printf("i2c 0x%x : write addr 0x%x , 0x%x\n",XC7022_I2C_ADDRESS_WR<<1,XC7022_colorbar_on[i][0],XC7022_colorbar_on[i][1]);
		DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_WR, XC7022_colorbar_on[i][0] ,protocolWriteSample2, &XC7022_colorbar_on[i][1] , 1);
		
		statusI2c=DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_RD, XC7022_colorbar_on[i][0], protocolReadSample2, bytes, 1);
		printf("i2c 0x%x : read addr 0x%x , 0x%x\n",XC7022_I2C_ADDRESS_RD<<1,XC7022_colorbar_on[i][0],bytes[0]);
	}
	#else
	for(i=0;i<COUNT_OF(XC7022_colorbar_off);i++)
	{
		printf("i2c 0x%x : write addr 0x%x , 0x%x\n",XC7022_I2C_ADDRESS_WR<<1,(XC7022_colorbar_off)[i][0],(XC7022_colorbar_off)[i][1]);
		DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_WR, (XC7022_colorbar_off)[i][0] ,protocolWriteSample2, &(XC7022_colorbar_off)[i][1] , 1);
	}
	#endif
#endif

#if 1
	u16 test_reg,page_reg,page;
	page_reg = 0xfffe;
	page = 0x26;
	test_reg = 0x2015;
	DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_WR, page_reg ,protocolWriteSample2, &page , 1);
		
	statusI2c=DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_RD, test_reg, protocolReadSample2, bytes, 1);
	printf("[TEST] i2c 0x%x : read addr 0x%x , 0x%x\n",XC7022_I2C_ADDRESS_RD<<1,test_reg,bytes[0]);

#endif


#if 0
	
		//i2c read test
		printf("~~~~ read test ~~~~\n");
		
		printf("[XC7022_0xfff0~0xffff]\n");
		for(i=0;i<16;i++)
		{
			statusI2c=DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_RD, 0xfff0+i, protocolReadSample2, bytes, 1);
			printf("i2c 0x%x : read addr 0x%x , 0x%x\n",XC7022_I2C_ADDRESS_RD<<1,0xfff0+i,bytes[0]);
		}
		
		printf("[XC7022_bypass_on]\n");
		for(i=0;i<COUNT_OF(XC7022_bypass_on);i++)
		{
			if((XC7022_bypass_on[i][0] == 0xfffd)||(XC7022_bypass_on[i][0] == 0xfffe))
			{
				statusI2c = DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_WR, XC7022_bypass_on[i][0] ,protocolWriteSample2, (u8*)(&XC7022_bypass_on[i][1]) , 1);
				printf("%d:write 0x%x 0x%x\n",statusI2c,XC7022_bypass_on[i][0],XC7022_bypass_on[i][1]);
			}
			statusI2c=DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_RD, XC7022_bypass_on[i][0], protocolReadSample2, bytes, 1);
			printf("i2c 0x%x : read addr 0x%x , 0x%x\n",XC7022_I2C_ADDRESS_RD<<1,XC7022_bypass_on[i][0],bytes[0]);
		}
	
		printf("[XC7022_1920x1080_YUV422]\n");
		for(i=0;i<100;/*COUNT_OF(XC7022_1920x1080_YUV422)*/i++)
		{
			if((XC7022_1920x1080_YUV422[i][0] == 0xfffd)||(XC7022_1920x1080_YUV422[i][0] == 0xfffe))
			{
				statusI2c = DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_WR, XC7022_1920x1080_YUV422[i][0] ,protocolWriteSample2, (u8*)(&XC7022_1920x1080_YUV422[i][1]) , 1);
				printf("%d:write 0x%x 0x%x\n",statusI2c,XC7022_1920x1080_YUV422[i][0],XC7022_1920x1080_YUV422[i][1]);
			}
			statusI2c=DrvI2cMTransaction(i2c0Handle, XC7022_I2C_ADDRESS_RD, XC7022_1920x1080_YUV422[i][0], protocolReadSample2, bytes, 1);
			printf("i2c 0x%x : read addr 0x%x , 0x%x\n",XC7022_I2C_ADDRESS_RD<<1,XC7022_1920x1080_YUV422[i][0],bytes[0]);
		}
		printf("~~~~ read test end ~~~~\n");
#endif


    printf("\n  Streaming ... \n");

	printf("~~~ cambuf 0x%x\n",camBuf[0]);

	//memset(camBuf[0],0x55,1920*1080*2);
	//memset(camBuf[0],0xaa,1920*1080*2);
	//memset(camBuf[0],0xff,1920*1080*2);
	
	rtems_interrupt_level level;
    while(1)
	{
		//outNum++;
		if(newCamFrameCtr==100)
		{
			printf("save start ... 1920*1080 = 0x%x\n",1920*1080);
			printf("buf0 ... 0x%x \n",camBuf[0]);
			printf("buf1 ... 0x%x \n",camBuf[1]);
			printf("buf2 ... 0x%x \n",camBuf[2]);
			
			rtems_interrupt_disable(level);

			//saveMemoryToFile((unsigned int)camBuf[0], 1920*1080*2, "output_1920x1080_P400_8bpp-buf0.yuv");
			//saveMemoryToFile((unsigned int)camBuf[1], 1920*1080*2, "output_1920x1080_P400_8bpp-buf1.yuv");
			saveMemoryToFile((unsigned int)camBuf[0],CAM_FRAME_SIZE_BYTES, "output_1920x1080_P400_8bpp-buf2_0.yuv");
            saveMemoryToFile((unsigned int)camBuf[1],CAM_FRAME_SIZE_BYTES, "output_1920x1080_P400_8bpp-buf2_1.yuv");
            saveMemoryToFile((unsigned int)camBuf[2],CAM_FRAME_SIZE_BYTES, "output_1920x1080_P400_8bpp-buf2_2.yuv");
            saveMemoryToFile((unsigned int)camBuf[3],CAM_FRAME_SIZE_BYTES, "output_1920x1080_P400_8bpp-buf2_3.yuv");
			rtems_interrupt_enable(level);

			printf("save done\n");
		}

		if(oldCamFrameCtr!=newCamFrameCtr)
		{
			oldCamFrameCtr = newCamFrameCtr;
		}
		
	}

    return 0;
}
